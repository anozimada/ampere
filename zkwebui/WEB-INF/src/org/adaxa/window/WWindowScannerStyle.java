package org.adaxa.window;

public class WWindowScannerStyle {
	public static final String STATUS_NORMAL_BACKGROUND_STYLE = "background-color: #FFFFFF;" +
			"-moz-border-radius: 3px; -webkit-border-radius: 3px; border: 1px solid #262626; border-radius: 3px; text-align : center;";
	public static final String STATUS_MESSAGE_BACKGROUND_STYLE = "background-color: transparent;" +
			" -moz-border-radius: 3px; -webkit-border-radius: 3px; border: 1px solid red; width:500px;" +
			" padding:1px 5px; border-radius: 3px; text-align : left; height:50px;";
	public static final String STATUS_HIGHLIGHTED_BACKGROUND_STYLE = "background-color: #C51111;" +
			" -moz-border-radius: 3px; -webkit-border-radius: 3px; border:" +
			" 1px solid #8B0000; border-radius: 3px; text-align : center; ";
	public static final String STATUS_TEXT_STYLE = "color: black; background-color: transparent;" +
			" font-size: 10px; font-weight:bold; position: relative; -moz-box-shadow: 0px 0px 0px #000;" +
			"-webkit-box-shadow: 0px 0px 0px #000;box-shadow: 0px 0px 0px #000; padding: 5px; " +
			"min-height: 20px; line-height:2.5em;";
	public static final String MESSAGE_TEXT_STYLE = "color: #C51111; background-color: transparent;" +
			" font-size: 14px; font-weight:bold; position: relative; ";
	public static final String DOCUMENT_TEXT_STYLE = "color: darkblue; font-size: 16px;";
	public static final String PRODUCT_TEXT_STYLE = "color: darkred; font-size: 16px;";
	public static final String BOLD_TEXT_STYLE = "color: darkred; font-size: 18px; font-weight: bolder";
	
	public static final String PANEL_SCANNED_STYLE = "background-color: green;" +
			" -moz-border-radius: 3px; -webkit-border-radius: 3px; border: 1px solid black;" +
			" padding-top:15px; border-radius: 3px; text-align : center; height:50px;";
	public static final String PANEL_SHORT_STYLE = "background-color: red;" +
			" -moz-border-radius: 3px; -webkit-border-radius: 3px; border: 1px solid black;" +
			" padding-top:15px; border-radius: 3px; text-align : center; height:50px;";
	public static final String PANEL_EXCESS_STYLE = "background-color: orangered;" +
			" -moz-border-radius: 3px; -webkit-border-radius: 3px; border: 1px solid black;" +
			" padding-top:15px; border-radius: 3px; text-align : center; height:50px;";
	public static final String PANEL_TEXT_STYLE = "color: white; font-size: 20px; font-weight:bold;";
	public static final String PANEL_STATUS_STYLE = "background-color: white;" +
			" -moz-border-radius: 3px; -webkit-border-radius: 3px; border: 1px solid black;" +
			" padding-top:15px; border-radius: 3px; text-align : center; height:25px; width:40px;";
}
