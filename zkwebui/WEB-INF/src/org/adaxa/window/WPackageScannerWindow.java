package org.adaxa.window;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adaxa.window.IOLineEditorDialog.AttSelections;
import org.adempiere.util.Callback;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.WNumberEditor;
import org.adempiere.webui.editor.WStringEditor;
import org.adempiere.webui.editor.WYesNoEditor;
import org.adempiere.webui.event.DialogEvents;
import org.adempiere.webui.panel.StatusBarPanel;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.adempiere.webui.window.SimplePDFViewer;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MAttributeSetInstance;
import org.compiere.model.MBPartner;
import org.compiere.model.MClientInfo;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MLocator;
import org.compiere.model.MOrder;
import org.compiere.model.MPInstance;
import org.compiere.model.MPackage;
import org.compiere.model.MPackageLine;
import org.compiere.model.MPrintConfig;
import org.compiere.model.MProduct;
import org.compiere.model.MSysConfig;
import org.compiere.model.Query;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportCtl;
import org.compiere.print.ReportEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.compiere.util.ValueNamePair;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.North;
import org.zkoss.zul.South;
import org.zkoss.zul.Space;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vbox;

public class WPackageScannerWindow extends Window implements EventListener<Event> {

	private static final int IDX_COLUMN_NOTES = 9;

	private static final int IDX_COLUMN_LOCATOR = 3;

	private static final int IDX_COLUMN_ATTNO = 6;

	public static final int PARTNO_MAXLENGTH = 10;
	
	private static final String				SHIPMENT_WINDOW_BUTTON1			= "SHIPMENT_WINDOW_BUTTON1";
	private static final String				SHIPMENT_WINDOW_BUTTON2			= "SHIPMENT_WINDOW_BUTTON2";
	private static final String				SHIPMENT_WINDOW_BUTTON3			= "SHIPMENT_WINDOW_BUTTON3";

	/**
	 * 
	 */
	private static final long serialVersionUID = -849734609756031494L;

	private static CLogger log = CLogger.getCLogger(WPackageScannerWindow.class);

	/** Window No */
	private int m_WindowNo = 0;

	//m_product_id, Total QtyScanned
	private HashMap<Integer, BigDecimal> mapProductScanned = new HashMap<Integer, BigDecimal>();
	//m_product_id, Total QtyToScan 
	private HashMap<Integer, BigDecimal> mapProductToScan = new HashMap<Integer, BigDecimal>();
	//MProduct.Value-SerNo, null  PCode and SerNo is the key, value is M_InOutLine_ID for quick lookup
	private HashMap<String, Integer> mapProductSerial = new HashMap<String, Integer>();
	
	private int m_PackageIndex = 0;
	private ArrayList<Integer> scannedASI = new ArrayList<Integer>();
	private MInOut inout = null;
	private Properties ctx = null;
	private MProduct m_product = null;
	private boolean isRemoveMode = false;
	private boolean isCrossDock = false;
	List<MPackage> packList = new ArrayList<MPackage>();
	List<MPackageLine> packLines = new ArrayList<MPackageLine>();
	MInOutLine ioLines[] = null;
	private MInOutLine ioLine = null;
	private String m_trxName = null;
	private int totalItem = 0;

	MPrintConfig 							m_PrintConfig1 			= null;
	MPrintConfig 							m_PrintConfig2 			= null;
	MPrintConfig 							m_PrintConfig3 			= null;
	// GUI
	private WListbox table = ListboxFactory.newDataTable();
	private Borderlayout mainPanel = new Borderlayout();
	private Grid northPanel = GridFactory.newGridLayout();
	private Grid southPanel = GridFactory.newGridLayout();
	private Grid edgePanel = GridFactory.newGridLayout();
	
	private Label lShipmentInfo = new Label();
	private Label lPackageInfo = new Label();
	private Label lProductInfo = new Label();
	private Label lUPC = new Label();
	private Label lLocator = new Label();
	private Label lSerial = new Label();
	private Label lNoOfPackages = new Label();
	private Label lmode = new Label();
	private Label lProductKey = new Label();
	private Label lCrossDock = new Label("X");

	private Label lMessage = new Label();
	private Label lScanned = new Label();
	private Label lShort = new Label();
	private Label lExcess = new Label();
	private Div panelScanned = new Div();
	private Div panelShort = new Div();
	private Div panelExcess = new Div();
	private Div panelMessage = new Div();
	
	private WStringEditor fUPC = new WStringEditor();
	private WStringEditor fLocator = new WStringEditor();
	private WStringEditor fSerial = new WStringEditor();
	private WStringEditor fProductKey = new WStringEditor();
	private WNumberEditor fNoofPackages = new WNumberEditor();
	
	private Button bNext = null;
	private Button bBack = null;
	private Div modePanel = new Div();
	private WYesNoEditor cRemoveMode = null;
	private Button	bPrintButton1 = null;
	private Button	bPrintButton2 = null;
	private Button	bPrintButton3 = null;
	
	private Button bDelete = null;
	private Button bSave = null;
	private Button bComplete = null;
	
	private StatusBarPanel statusBar = new StatusBarPanel();

	private MPInstance pInstance;

	private ProcessInfo pInfo;
	private int AD_Process_ID = 0;
	private int m_Locator_ID = 0;
	private boolean m_IsPreLoad = false;
	
	private ArrayList<MInOutLine> m_deleteCandidates = null;
	private ArrayList<MInOutLine> m_insertCandidates = null;
	private ArrayList<MInOutLine> m_updateCandidates = null;
	private boolean isAllowOverShipping = false;  //always the case but may change - jobriant 
	private int m_M_Locator_ID;
	
	public WPackageScannerWindow(int windowNo, MInOut inout) {
		m_WindowNo = windowNo;
		this.inout = inout;
		
		m_IsPreLoad = MSysConfig.getBooleanValue("PRELOAD_SHIPMENT", false, 
				inout.getAD_Client_ID(), inout.getAD_Org_ID());
				
		isCrossDock = inout.get_ValueAsInt("M_Locator_ID") != 0; 
		ioLines = inout.getLines(true);
		ctx = inout.getCtx();
		// Creating transaction for importing order
		m_trxName = Trx.createTrxName("PackageScanner");

		try {
			init();
			dynInit();
		} catch (Exception ex) {
			log.log(Level.SEVERE, ex.getMessage());
		}
	}

	/**
	 * Static Init
	 * 
	 * @throws Exception
	 */

	void init() throws Exception {
		ZKUpdateUtil.setWidth(this, "1350px");
		this.setBorder("normal");
		ZKUpdateUtil.setHeight(this, "825px");
		this.setClosable(true);
		this.setTitle("Package Contents & Serial#");
		this.setAttribute("mode", "modal");
		this.setSizable(true);
		this.appendChild(mainPanel);

		// North
		North north = new North();
		north.appendChild(northPanel);
		ZKUpdateUtil.setHeight(northPanel, "220px");
		mainPanel.appendChild(north);
		Rows rows = northPanel.newRows();
		// First Row
		Row row = rows.newRow();
		row.appendChild(new Space());
		row.appendChild(lShipmentInfo);

		bNext = createButton("Next", "Detail", "Next Package");
		bBack = createButton("Previous", "Parent", "Previous Package");
		bNext.setDisabled(false);
		row.appendChild(bBack);
		//row.appendChild(new Space());
		row.appendChild(lCrossDock);
		row.appendChild(bNext);
		row.setSpans("1,4");
		// Second Row
		row = rows.newRow();
		row.appendChild(new Space());
		row.appendChild(lPackageInfo);
		row.appendChild(new Space());
		row.setSpans("1,4");

		// Third Row

		row = rows.newRow();
		row.appendChild(new Space());
//		row.appendChild(lLocator.rightAlign());
//		row.appendChild(fLocator.getComponent());
		row.appendChild(lUPC.rightAlign());
		row.appendChild(fUPC.getComponent());
		row.appendChild(lProductKey.rightAlign());
		row.appendChild(fProductKey.getComponent());
		// row.appendChild(new Space());
		row.appendChild(lSerial.rightAlign());
		row.appendChild(fSerial.getComponent());
		row.appendChild(new Space());
//		row.setSpans("1,1,1,1,1,2");

		// Second Row
		row = rows.newRow();
		row.appendChild(new Space());
		row.appendChild(lProductInfo);
		row.appendChild(new Space());
		row.setSpans("1,4");

		
		// Fourth Row
		cRemoveMode = new WYesNoEditor("RemoveMode", "Remove Mode",
				"Scanned Items are removed", false, false, true);
		row = rows.newRow();
		row.appendChild(new Space());
		row.appendChild(modePanel);
		row.appendChild(cRemoveMode.getComponent());
		modePanel.appendChild(lmode);
		lmode.setStyle(WWindowScannerStyle.STATUS_TEXT_STYLE);
		row.appendChild(lNoOfPackages.rightAlign());
		row.appendChild(fNoofPackages.getComponent());		
		lNoOfPackages.setValue(Msg.translate(Env.getCtx(), "NoofPackages"));
		fNoofPackages.getComponent().setFormat(DisplayType.getNumberFormat(DisplayType.Integer));

		if (isCrossDock) {
			lCrossDock.setStyle(WWindowScannerStyle.BOLD_TEXT_STYLE);
			String loc = MLocator.get(ctx, inout.get_ValueAsInt("M_Locator_ID")).getValue();
			lCrossDock.setText("XDock [" + loc + "]");
		}
		lCrossDock.setVisible(isCrossDock);
		

		modePanel.setStyle(WWindowScannerStyle.STATUS_NORMAL_BACKGROUND_STYLE);
		cRemoveMode.getComponent().addActionListener(this);
		row.setSpans("1,4,2");
		ZKUpdateUtil.setHeight(modePanel, "30px");

		// Center
		Center center = new Center();
		center.appendChild(table);
		ZKUpdateUtil.setVflex(table, "1");
		mainPanel.appendChild(center);

		// South
		South south = new South();
		Vbox vbox = new Vbox();
		ZKUpdateUtil.setHeight(south, "130px");
		ZKUpdateUtil.setWidth(vbox, "100%");
		vbox.appendChild(southPanel);
		south.appendChild(vbox);

		rows = southPanel.newRows();
		row = rows.newRow();
//		row.appendChild(new Space());
		Hbox buttonPanel = new Hbox();
		ZKUpdateUtil.setWidth(buttonPanel, "600px");
		ZKUpdateUtil.setWidth(row, "600px");
//		ZKUpdateUtil.setHflex(row, "1");
		ZKUpdateUtil.setHeight(buttonPanel, "50px");
		bDelete = createButton("Delete", "Delete", "Delete");

		bDelete.addActionListener(this);
		buttonPanel.appendChild(bDelete);
		
		m_PrintConfig1 = MPrintConfig.get(SHIPMENT_WINDOW_BUTTON1);
		m_PrintConfig2 = MPrintConfig.get(SHIPMENT_WINDOW_BUTTON2);
		m_PrintConfig3 = MPrintConfig.get(SHIPMENT_WINDOW_BUTTON3);
		if (m_PrintConfig1.isVisible()) {
			bPrintButton1 = createButton(m_PrintConfig1.getValue(), "Print1", m_PrintConfig1.getName());
			buttonPanel.appendChild(bPrintButton1);
			bPrintButton1.setEnabled(m_PrintConfig1.getAD_PrintFormat_ID() != 0);
				
		}
		if (m_PrintConfig2.isVisible()) {
			bPrintButton2 = createButton(m_PrintConfig2.getValue(), "Print2", m_PrintConfig2.getName());
			buttonPanel.appendChild(bPrintButton2);
			bPrintButton2.setEnabled(m_PrintConfig2.getAD_PrintFormat_ID() != 0);
		}
		if (m_PrintConfig3.isVisible()) {
			bPrintButton3 = createButton(m_PrintConfig3.getValue(), "Print3", m_PrintConfig3.getName());
			buttonPanel.appendChild(bPrintButton3);
			bPrintButton3.setEnabled(m_PrintConfig3.getAD_PrintFormat_ID() != 0);
		}

		row.appendChild(buttonPanel);
		ZKUpdateUtil.setHeight(row, "60px");

		lMessage.setStyle(WWindowScannerStyle.MESSAGE_TEXT_STYLE);
		panelMessage.setStyle(WWindowScannerStyle.STATUS_MESSAGE_BACKGROUND_STYLE);
		row.appendChild(panelMessage);
		panelMessage.appendChild(lMessage);
		ZKUpdateUtil.setHflex(panelMessage, "1");

		Hbox rightHbox = new Hbox();
		rightHbox.setPack("end");
		ZKUpdateUtil.setHflex(rightHbox, "1");
		bComplete = createButton("Complete", "Process", "Complete Shipment");
		bComplete.addActionListener(this);

		bSave = createButton("Exit", "End", "Exit");
		bSave.addActionListener(this);

		bSave.setDisabled(false);
		bComplete.setDisabled(false);
		bDelete.setDisabled(false);

		rightHbox.appendChild(bComplete);
		rightHbox.appendChild(bSave);
		row.appendChild(rightHbox);
		
//		row = rows.newRow();
//		row.appendChild(new Space());
//		row.appendChild(statusBar);
//		row.setSpans("1,9");

		mainPanel.appendChild(south);
		
		//printer settings
		String sql="SELECT t.ad_process_id FROM ad_tab t  INNER JOIN ad_table l ON l.ad_table_id = t.ad_table_id WHERE l.tablename = 'M_Package'";
		AD_Process_ID = DB.getSQLValue(null, sql);
		
		lScanned.setValue("Scanned=3");
		lShort.setValue("Short=0");
		lExcess.setValue("Excess=1");
		lScanned.setStyle(WWindowScannerStyle.PANEL_TEXT_STYLE);
		lShort.setStyle(WWindowScannerStyle.PANEL_TEXT_STYLE);
		lExcess.setStyle(WWindowScannerStyle.PANEL_TEXT_STYLE);


		panelScanned.setStyle(WWindowScannerStyle.PANEL_SCANNED_STYLE);
		panelScanned.appendChild(lScanned);

		//TODO LAYOUT
		panelShort.setStyle(WWindowScannerStyle.PANEL_SHORT_STYLE);
		panelShort.appendChild(lShort);

		panelExcess.setStyle(WWindowScannerStyle.PANEL_EXCESS_STYLE);
		panelExcess.appendChild(lExcess);

		rows = edgePanel.newRows();
		// First Row
		row = rows.newRow();
		ZKUpdateUtil.setHeight(row, "50px");

		row.appendChild(panelScanned);
		row.appendChild(panelShort);
		row.appendChild(panelExcess);

		ZKUpdateUtil.setHflex(panelScanned, "1");
		ZKUpdateUtil.setHflex(panelShort, "1");
		ZKUpdateUtil.setHflex(panelExcess, "1");

		vbox.appendChild(edgePanel);
		mainPanel.appendChild(south);
		
//		ZKUpdateUtil.setWidth(lMessage, "400px");
	}

	private void dynInit() {

		StringBuilder shipmentInfo = new StringBuilder();
		MBPartner partner = inout.getBPartner();

		shipmentInfo.append(partner.getName()).append(" - Order# ");
		int C_Order_ID = inout.getC_Order_ID();
		if (C_Order_ID > 0) {
			MOrder order = new MOrder(ctx, C_Order_ID, null);
			shipmentInfo.append(order.getDocumentNo());
		}

		int noofPackages = inout.getNoPackages()==0 ? 1 : inout.getNoPackages();
		fNoofPackages.setValue(noofPackages);
		
		this.addEventListener(Events.ON_CLOSE, this);

		shipmentInfo.append(" - Shipment# ").append(inout.getDocumentNo());

		lShipmentInfo.setText(shipmentInfo.toString());
		lShipmentInfo.setStyle(WWindowScannerStyle.DOCUMENT_TEXT_STYLE);
		lPackageInfo.setStyle(WWindowScannerStyle.DOCUMENT_TEXT_STYLE);
		
		lProductInfo.setStyle(WWindowScannerStyle.PRODUCT_TEXT_STYLE);
		lUPC.setText(Msg.translate(ctx, "UPC"));
		lLocator.setText(Msg.translate(ctx, "Locator"));
		lSerial.setText(Msg.translate(ctx, "Serial"));
		lProductKey.setText("Product Key");

		cRemoveMode.setValue(false);
		lmode.setText("ITEMS SCANNED WILL BE ADDED");

		fLocator.getComponent().addEventListener(Events.ON_OK, this);
		fUPC.getComponent().addEventListener(Events.ON_OK, this);
		fSerial.getComponent().addEventListener(Events.ON_OK, this);
		fProductKey.getComponent().addEventListener(Events.ON_OK, this);

		String colLocatorInfo = "Locator";
		if (isCrossDock) {
			colLocatorInfo = "Package No";
		}
		ColumnInfo[] layout = new ColumnInfo[] {
				new ColumnInfo(" ", ".", IDColumn.class, false, false, ""),
				new ColumnInfo(Msg.translate(Env.getCtx(), "M_Product_ID"),
						".", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "PCode"), ".",
						String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), colLocatorInfo), ".", //Locator
						String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Status"), ".",
						String.class),
				new ColumnInfo("Scanned", ".",
						String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Att No"), ".",
						String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "PO No"), ".",
						String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Remarks"), ".",
								String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Notes"), ".",
						String.class) };

		table.prepareTable(layout, "", "", false, "");		
		table.setSclass("scannerlist");
		table.addEventListener(Events.ON_SELECT, this);
		layout[4].setColWidth(30);
		statusBar.setStatusDB("");

		String sql = "select sum(qtyentered) from M_InoutLine where isActive='Y' and c_charge_id IS NULL and M_Inout_Id = ?";
		totalItem = DB.getSQLValue(null, sql, inout.getM_InOut_ID());

		loadPackages(m_PackageIndex);
		setActivePackage(m_PackageIndex);

		WListItemRenderer renderer = (WListItemRenderer) table.getItemRenderer();
		ArrayList<ListHeader> headers = renderer.getHeaders();
		ZKUpdateUtil.setWidth(headers.get(1), "70px");
		ZKUpdateUtil.setWidth(headers.get(2), "150px");
		ZKUpdateUtil.setWidth(headers.get(3), "70px");
		ZKUpdateUtil.setWidth(headers.get(4), "40px");
		ZKUpdateUtil.setWidth(headers.get(5), "40px");
		ZKUpdateUtil.setWidth(headers.get(6), "60px");
		ZKUpdateUtil.setWidth(headers.get(7), "60px");
		ZKUpdateUtil.setWidth(headers.get(8), "120px");
		ZKUpdateUtil.setWidth(headers.get(9), "120px");
		
		fUPC.getComponent().focus();
		lMessage.setValue("");

	}

	//reload table from database
	private void reloadTable() {


		table.setRowCount(0);

		String sql = "SELECT pl.m_packageline_id, p.value as PCode, p.name as PName, l.value AS Locator, COALESCE(asi.serno, asi.lot) as idno, iol.movementqty  " +
	             "  , pl.qty as qtyscanned, COALESCE(o.documentno, 'NIL') AS OrderNo " +
	             "  , case when xpl.instances > 1  " +
	             "      then 'LineNo ' || iol.Line || ', PartNo=' || p.value || ' split into ' ||  xpl.instances    " +
	             "      else '' " +
	             "    end as remarks " +
	             ", pl.description as notes " + 
	             "FROM m_inoutline iol  " +
	             "INNER JOIN m_packageline pl on pl.m_inoutline_id = iol.m_inoutline_id " +
	             "  LEFT JOIN c_orderline ol ON iol.c_orderline_id = ol.c_orderline_id  " +
	             "  LEFT JOIN c_order o ON ol.c_order_id = o.c_order_id  " +
	             "  INNER JOIN m_product p ON iol.m_product_id = p.m_product_id  " +
	             "  INNER JOIN m_locator l ON pl.m_locator_id = l.m_locator_id  " +
	             "  LEFT JOIN m_attributesetinstance asi ON iol.m_attributesetinstance_id = asi.m_attributesetinstance_id  " +
	             "  LEFT JOIN (SELECT pl.m_inoutline_id, count(pl.qty) as instances FROM m_packageline pl GROUP BY pl.m_inoutline_id) xpl " +
	             "  		ON xpl.m_inoutline_id = iol.m_inoutline_id " +
	             "WHERE pl.m_package_id = ? " +
	             "ORDER BY iol.Line, p.value, pl.m_packageline_id";
		
		PreparedStatement pstmt = DB.prepareStatement(sql, m_trxName);
		ResultSet rs = null;
		try {
			pstmt.setInt(1, packList.get(m_PackageIndex).getM_Package_ID());
			rs = pstmt.executeQuery();
			
			while (rs.next())
			{
				        //int m_inoutline_id, String pCode, String pName, String locator
						// String serNo, BigDecimal QtyToScan, BigDecimal QtyScanned, String orderNo, boolean noCheck
				String loc = isCrossDock ? (rs.getString(10) == null ? "" : rs.getString(10)) : rs.getString(4) ; 
				addRow(rs.getInt(1), rs.getString(2),  rs.getString(3),  loc
						,  rs.getString(5), rs.getBigDecimal(6),  rs.getBigDecimal(7), rs.getString(8), rs.getString(9),rs.getString(10), true);
			}

		}
		catch (Exception e) {
			log.severe(e.getMessage());
		}
		finally {
			DB.close(rs, pstmt);
		}
		
		updateItemCount();
		resetHashMap();
		table.repaint();
	}
	
	
	/**
	 * Add Receipt Line to table, if exists, just update with new value
	 * @param m_inoutline_id
	 * @param pCode
	 * @param pName
	 * @param locator
	 * @param serNo
	 * @param QtyToScan
	 * @param QtyScanned
	 * @param orderNo
	 * @param noCheck - for efficiency, no need to check when loading for the first time
	 * @return row Position
	 */

	private int addRow(int m_inoutline_id, String pCode, String pName,
			String locator, String serNo, BigDecimal QtyToScan, BigDecimal QtyScanned,
			String orderNo, String remarks, String notes, boolean noCheck) {
		int numrows = table.getItemCount();
		int row = 0;
		if (!noCheck) {
			for (int i= 0; i < numrows; i++)
			{
				if (m_inoutline_id == ((IDColumn)table.getValueAt(i, 0)).getRecord_ID())
				{
					//row already exists - just update
					row = i;
					break;
				}
			}
			
		}
		if (row == 0) {
			//nothing found, create new
			row = numrows ;
			table.setRowCount(row+1);
			table.setValueAt(new IDColumn(m_inoutline_id), row, 0);
		}
		
		table.setValueAt(pCode, row, 1);
		table.setValueAt(pName, row, 2);
		table.setValueAt(locator, row, 3);  //locator		
		table.setValueAt(QtyToScan, row, 4);
		table.setValueAt(QtyScanned, row, 5);
		table.setValueAt(serNo, row, 6);
		table.setValueAt(orderNo, row, 7);
		table.setValueAt(remarks, row, 8);
		table.setValueAt(notes, row, 9);
		table.repaint();
		return row;

	}
	private void loadPackages(int packageIndex) {
		packList.clear();

		packList = new Query(ctx, MPackage.Table_Name, "M_Inout_ID=?", m_trxName)
				.setParameters(inout.getM_InOut_ID())
				.setOnlyActiveRecords(true).list();

		if (packList.size() == packageIndex) { //missing package, list is always + 1
			MPackage pack = createPackage();
			if (pack != null) 
			{
				packList.add(pack);
			}
		}
		packLines = new Query(ctx, MPackageLine.Table_Name,
				"M_Package_ID=?", m_trxName).setParameters(
						packList.get(m_PackageIndex).getM_Package_ID()).setOnlyActiveRecords(true)
						.list();
			
		if (packLines.size() == 0 && m_IsPreLoad) {
			if (m_IsPreLoad && packageIndex == 0) {
				//only preload on the first package
				createPackageLines(packList.get(0));
			}
		}
		
		reloadTable();

	}

	/**
	 * Automatically load items for shipment as if items has been scanned
	 * In this way, people save time from scanning and just verifying if all items are correct
	 * Assumption - no serialized product, and in one package only 
	 * @param pack
	 */
	private void createPackageLines(MPackage pack) {
		for (MInOutLine line : ioLines)
		{
			if (isCrossDock) {
				assignPackageNo(line, pack);
			}
			else {
				MPackageLine packLine = new MPackageLine(pack);
				packLine.setInOutLine(line);
				packLine.set_ValueOfColumn("M_Locator_ID", line.getM_Locator_ID());
				if (packLine.save(inout.get_TrxName())) {
					packLines.add(packLine);
					//appendTableRow(packLine, line, line.getProduct(), null);
				}
				else {
					showLogError("Error while saving Package Line");
					return;
				}
			}	
		}
	}

	private void assignPackageNo(MInOutLine sLine, MPackage pack) {
		String sql = "SELECT m_inoutline_id, packageno, movementqty - qtyscanned - putqty as availqty " +
	             "FROM m_inout_line_crossdock_v " +
	             "WHERE isputaway = 'N' AND movementqty - qtyscanned - putqty > 0 AND m_product_id = ? " +
	             "ORDER BY movementqty - qtyscanned - putqty DESC";
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		BigDecimal qtyToShip = sLine.getQtyEntered();
		try {
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, sLine.getM_Product_ID());
			rs = pstmt.executeQuery();
			while (rs.next() && qtyToShip.compareTo(Env.ZERO) > 0) {
				BigDecimal availQty = rs.getBigDecimal(3);
				BigDecimal qty = qtyToShip;
				if (availQty.compareTo(qtyToShip) < 0) {
					qty = availQty;
					qtyToShip = qtyToShip.subtract(availQty);
				}
				else {
					qtyToShip = Env.ZERO;
				}
				MPackageLine packLine = new MPackageLine(pack);
				
				packLine.set_ValueOfColumn("ReceiptLine_ID", rs.getInt(1));
				packLine.set_ValueOfColumn("PackageNo", rs.getString(2));
				packLine.setQty(qty);
				packLine.setInOutLine(sLine);
				packLine.set_ValueOfColumn("M_Locator_ID", sLine.getM_Locator_ID());
				if (packLine.save(inout.get_TrxName())) {
					packLines.add(packLine);
				}
			}
		} catch (SQLException e) {
			log.severe(e.getMessage());
		}
		finally {
			rs = null;
			pstmt = null;
		}
		
	
	}

	private MPackage createPackage() {
//		MShipper shipper = null;
//		if (inout.getM_Shipper_ID() > 0) {
//			shipper = new MShipper(ctx, inout.getM_Shipper_ID(), null);
//		}

		MPackage pack = new MPackage(ctx, 0, null);
		pack.setM_InOut_ID(inout.getM_InOut_ID());
		pack.set_ValueOfColumn(MInOut.COLUMNNAME_AD_Client_ID, inout
				.getAD_Client_ID());
		pack.setAD_Org_ID(inout.getAD_Org_ID());
		pack.setShipDate(inout.getShipDate());
		pack.setM_Shipper_ID(inout.getM_Shipper_ID());
		if (!pack.save()) {
			showLogError("Can not create package");
			return null;
		}
		return pack;
	}

	private void setActivePackage(int packageIndex) {
		MPackage mPack = packList.get(packageIndex);
		StringBuilder packInfo = new StringBuilder("Package #");
		packInfo.append(m_PackageIndex + 1).append(" - ").append(
				mPack.getDocumentNo()).append(" Track# ");

		packLines = new Query(ctx, MPackageLine.Table_Name, "M_Package_ID=?",
				m_trxName).setParameters(mPack.getM_Package_ID())
				.setOnlyActiveRecords(true).list();

		//refreshTable();

		lPackageInfo.setText(packInfo.toString());

		pInstance = new MPInstance(Env.getCtx(),0, "");
		pInstance.setAD_Process_ID(AD_Process_ID);
		pInstance.setRecord_ID(mPack.getM_Package_ID());
		pInstance.setAD_Client_ID(Env.getAD_Client_ID(ctx));
		pInstance.save();
		
		pInfo = new ProcessInfo("Print Label", AD_Process_ID);
		pInfo.setRecord_ID(mPack.getM_Package_ID());
		pInfo.setAD_PInstance_ID(pInstance.getAD_PInstance_ID());
		pInfo.setAD_Client_ID(Env.getAD_Client_ID(ctx));


	}


	private Button createButton(String name, String image, String tooltip)
	{
		Button btn = new Button("");
		btn.setName("btn" + name);
		btn.setImage(ThemeUtils.resolveImageURL( image + "24.png"));
		btn.setTooltiptext(tooltip);

		LayoutUtils.addSclass("action-button", btn);
		ZKUpdateUtil.setHeight(btn, "50px");
		ZKUpdateUtil.setWidth(btn, "60px");
		btn.setTabindex(0);
		btn.addEventListener(Events.ON_CLICK, this);
		btn.addActionListener(this);
		btn.setDisabled(true);

		return btn;
	}


	public void onEvent(Event event) throws Exception {
		if (Events.ON_CLICK.equals(event.getName())) {
			if (event.getTarget() == bNext) {
				onNext();
			} else if (event.getTarget() == bBack) {
				onPrevious();
			} else if (event.getTarget() == bSave) {
				onExit();
			} else if (event.getTarget() == bComplete) {
				onComplete();
			} else if (event.getTarget() == bDelete) {
				onDelete();
			} 
			else if (event.getTarget() == bPrintButton1) {
				print(m_PrintConfig1);
			}
			else if (event.getTarget() == bPrintButton2) {
				print(m_PrintConfig2);
			}
			else if (event.getTarget() == bPrintButton3) {
				print(m_PrintConfig3);
			}

		}
		else if (Events.ON_SELECT.equals(event.getName())) {
			if (event.getTarget() == this.table) {
			onEditRow();
			table.clearSelection();
		}
		} else if (Events.ON_OK.equals(event.getName())) {
			if (event.getTarget() == fSerial.getComponent()) {
				onSerial();
			} else if (event.getTarget() == fUPC.getComponent()) {
				onProductScan(fUPC.getComponent().getTextbox());
			} else if (event.getTarget() == fLocator.getComponent()) {
				onLocator();
			} else if (event.getTarget() == fProductKey.getComponent()) {
				onProductScan(fProductKey.getComponent().getTextbox());
			}
		} else if (Events.ON_CHECK.equals(event.getName())) {
			if (event.getTarget() == cRemoveMode.getComponent()) {
				onModeTogle();
			}
		} else if (Events.ON_CLOSE.equals(event.getName())) {
			onExit();
		}
	}

	private void onModeTogle() {
		isRemoveMode = (Boolean) cRemoveMode.getValue();
		if (isRemoveMode) {
			modePanel.setStyle(WWindowScannerStyle.STATUS_HIGHLIGHTED_BACKGROUND_STYLE);
			lmode.setText("ITEMS SCANNED WILL BE REMOVED FROM THE LIST");
		} else {
			modePanel.setStyle(WWindowScannerStyle.STATUS_NORMAL_BACKGROUND_STYLE);
			lmode.setText("ITEMS SCANNED WILL BE ADDED");
			//setInoutLine();
		}
	}

	private void onEditRow() {
		showError("");
		int r = table.getSelectedRow();
		int M_PackageLine_ID = ((IDColumn)table.getValueAt(r, 0)).getRecord_ID();
		MPackageLine line = null;
		for (MPackageLine search : packLines) {
			if (search.getM_PackageLine_ID() == M_PackageLine_ID) {
				line = search;
				break;
			}
		}
		if (line == null) {
			log.severe("This is not possible. We're just editing existing line.");
			return;
		}
		final MPackageLine lineF = line;
		MProduct product = (MProduct) line.getM_InOutLine().getM_Product();

		String serial = (String) table.getValueAt(r, 6);
		String locator = (String) table.getValueAt(r, 3);
		String notes = (String) table.getValueAt(r, 9);
		BigDecimal startQty = (BigDecimal) line.getQty();
		boolean isSerial = product.getM_AttributeSet() != null && (product.getM_AttributeSet().isSerNo() || product.getM_AttributeSet().isLot());
		
		IOLineEditorDialog dlg = new IOLineEditorDialog(this.inout.getM_Warehouse_ID(), product.toDisplayString()
				, startQty, isSerial, 100, !isSerial, IOLineEditorDialog.Mode.EDIT, product, isCrossDock, notes);
		
		dlg.setLocator(locator);
		dlg.setM_PackageLine_ID(M_PackageLine_ID);
		dlg.setAttribute(Window.MODE_KEY, Window.MODE_HIGHLIGHTED);
		dlg.setSerial(serial);
		
		
		StringBuffer s = new StringBuffer(product.getValue() + "-" + product.getName() + "\n");
		s.append("Total Qty that must be shipped=" + mapProductToScan.get(product.getM_Product_ID()) + "\n");
		s.append("Qty Scanned outside this dialog =" + mapProductScanned.get(product.getM_Product_ID()).subtract(startQty) ); 
		dlg.setStatus(s.toString());
		
		dlg.setSummary();
		dlg.setAsiChanged(false);
		dlg.addEventListener(DialogEvents.ON_WINDOW_CLOSE, new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				if (!dlg.isCancelled()) {
					if (isCrossDock) {
						lineF.set_ValueOfColumn("PackageNo", dlg.getLocator());
						lineF.set_ValueOfColumn("ReceiptLine_ID", dlg.getReceiptLine_ID());
					} else {
						if (dlg.getM_Locator_ID() == 0) {
							showError("This is unusual, you shouldn't get Locator=0");
							return;
						}
						lineF.set_ValueOfColumn("M_Locator_ID", dlg.getM_Locator_ID());
					}
					lineF.setDescription(dlg.getNotes());
					
					if (isSerial) {
						boolean refresh = false;
						//serial changed
//						if (serial == null || (!serial.equals(dlg.getSerial()))) {
						if (dlg.isAsiChanged()) {
		/*
		a					if (isSerialOK(product.getValue(), dlg.getSerial())) {
								MAttributeSetInstance asi = getAttributeSetInstance(product, dlg.getSerial());
								if (asi == null) {
									FDialog.error(m_WindowNo, "Invalid Serial No being assigned to the line. Changes are ignored");
									return;
								}
								MInOutLine ioLine = (MInOutLine) line.getM_InOutLine();
								ioLine.setM_AttributeSetInstance_ID(asi.getM_AttributeSetInstance_ID());
								
								table.setValueAt(dlg.getSerial(), r, IDX_COLUMN_ATTNO);
							}
						}
						if (line.save(m_trxName)) {
							table.setValueAt(dlg.getLocator(), r, IDX_COLUMN_LOCATOR);  //locator
							table.setValueAt(dlg.getNotes(), r, IDX_COLUMN_NOTES);
							refresh = true;
						}
						*/

						//prevent from detecting as duplicate	
						if (serial != null)
							deleteSerialProductMap(product.getValue(), serial);
						
						int n = 1;
						MInOutLine shipmentLine = null; 
						AttSelections[] selectedASI = dlg.getSelectedASI();
						if (selectedASI.length == 0) { //nothing to pick - qty must be 0
							lineF.setQty(Env.ZERO);
							lineF.save();
							
							shipmentLine = (MInOutLine) lineF.getM_InOutLine();
							shipmentLine.setM_AttributeSetInstance_ID(0);
							shipmentLine.setQty(Env.ZERO);
							shipmentLine.save(m_trxName);
							

							refresh = true;
						}
						for (IOLineEditorDialog.AttSelections sel : selectedASI) {
							if (isSerialOK(product.getValue(), sel.AttNo)) {
								if (n == 1) {
									shipmentLine = (MInOutLine) lineF.getM_InOutLine();
									lineF.setQty(sel.qty);
									lineF.setM_Locator_ID(sel.M_Locator_ID);
									lineF.save();
								} else {
									shipmentLine = new MInOutLine((MInOut) lineF.getM_InOutLine().getM_InOut());
									MInOutLine.copyValues((MInOutLine) lineF.getM_InOutLine(), shipmentLine);
									shipmentLine.setLine(shipmentLine.getLine() + n);
									//copy original c_invoiceline_id reference - jtrinidad
									shipmentLine.setC_InvoiceLine_ID(lineF.getM_InOutLine().getC_InvoiceLine_ID());
									shipmentLine.save();
									MPackageLine newLine = new MPackageLine((MPackage) lineF.getM_Package());
									newLine.setM_InOutLine_ID(shipmentLine.getM_InOutLine_ID());
									newLine.setQty(sel.qty);
									newLine.set_ValueOfColumn("M_Locator_ID", sel.M_Locator_ID);
									newLine.save(m_trxName);

									//TODO must check it's been added jtrinidad 11/05/2018
									packLines.add(newLine);
//									packLineCandidates.add(newLine);
								}
								n++;
								refresh = true;
								shipmentLine.setM_AttributeSetInstance_ID(sel.M_AttributeSetInstance_ID);
								shipmentLine.setM_Locator_ID(sel.M_Locator_ID);
								shipmentLine.setQty(sel.qty);
								shipmentLine.save(m_trxName);
								
								table.setValueAt(sel.AttNo, r, IDX_COLUMN_ATTNO);
								addSerialToProductMap(product.getValue(), sel.AttNo, shipmentLine.getM_InOutLine_ID());
							}
							
							//must create new shipment line/package
								
							}
						}
						if (refresh) {
							loadPackages(m_PackageIndex); //just reload the whole stuff
//							table.repaint();
//							updateItemCount();
						}
						return;
					}
					//non serial
					BigDecimal changed = dlg.getQty().subtract(startQty);
//					if (changed.signum() > 0) {
						//check if qty limit permits
						if (updateScannedQtyProductMap(product.getM_Product_ID(), changed, true)) {
							lineF.setQty(dlg.getQty());
							table.setValueAt(dlg.getQty(), r, 5);
						}
						else {
							showError("Update not possible. Over Shipment.");
						}
					//}
					if (lineF.save(m_trxName)) {
						table.setValueAt(dlg.getLocator(), r, 3);  //locator		
						table.setValueAt(dlg.getNotes(), r, 9);
						table.repaint();
						updateItemCount();
					}
				}
			}
			
		});
		SessionManager.getAppDesktop().showWindow(dlg);
	}


	private void onNext() {
		m_PackageIndex++;
		if (m_PackageIndex >= packList.size()){
			packLines = new Query(ctx, MPackageLine.Table_Name,
					"M_Package_ID=?", null).setParameters(
							packList.get(m_PackageIndex-1).getM_Package_ID()).setOnlyActiveRecords(true)
							.list();
			if(packLines.size()==0){
				FDialog.ask(m_WindowNo, null, 
						"the previous package is still empty <br/>do you want to keep creating new packages? ", 
						new Callback<Boolean>() {

					@Override
					public void onCallback(Boolean result) {
						if (result) {
							//createPackage();
							loadPackages(m_PackageIndex);
						} else {
							m_PackageIndex--;
						}
						
						setActivePackage(m_PackageIndex);
						bBack.setDisabled(false);
						fUPC.getComponent().focus();
					}
					
				});
			}
		}
		else {
			reloadTable();
			
			setActivePackage(m_PackageIndex);
			bBack.setDisabled(false);
			fUPC.getComponent().focus();
		}
	}

	private void onPrevious() {
		if (m_PackageIndex == 0) {
			bBack.setDisabled(true);
			return;
		}

		m_PackageIndex--;
		if (m_PackageIndex == 0) {
			bBack.setDisabled(true);
			
		}
		reloadTable();
		setActivePackage(m_PackageIndex);
		fUPC.getComponent().focus();
	}


	private void onSerial() {
		
		String strSerial = (String) fSerial.getValue();
		processScan(m_product, strSerial);
	}
	

	private void onLocator() {
		String strLocator = ((String) fLocator.getValue()).toUpperCase();
		String sql = "SELECT M_Locator_ID from M_Locator WHERE UPPER(value) = ? AND AD_Client_ID = ?"; 
		m_Locator_ID = DB.getSQLValue(m_trxName,sql, strLocator, Env.getAD_Client_ID(ctx));
		if (m_Locator_ID <= 0) {
			Textbox textField = fLocator.getComponent().getTextbox();
			showError("Unknown Locator");
			textField.select();
			fLocator.getComponent().focus();
			textField.select();
			return;
		}
		//fLocator.setValue("");
		fUPC.setHasFocus(true);
		fUPC.getComponent().focus();
	}


	private void onExit() {
		Trx trx = Trx.get(m_trxName, false);
		if (trx != null) {
			trx.close();
		}
		this.detach();
	}

	private void onComplete() {
		try {
			DB.commit(true, m_trxName);
			
			// Deleting lines not scanned
			m_deleteCandidates = new ArrayList<MInOutLine>();
			m_insertCandidates = new ArrayList<MInOutLine>();
			m_updateCandidates = new ArrayList<MInOutLine>();
			MClientInfo ci = MClientInfo.get(ctx, inout.getAD_Client_ID());
			int M_ProductFreight_ID = ci.getM_ProductFreight_ID();
			for (MInOutLine iLine : ioLines) {
				if (iLine.getC_Charge_ID() > 0 || iLine.getM_Product_ID() == 0) {
					continue;
				}
				if (iLine.getProduct().getM_AttributeSet_ID() == 0) {
					assignPackageLines(iLine);
				} else {
					String sql = "Select count(*) from M_PackageLine where M_InoutLine_ID=?";
					int count = DB.getSQLValue(m_trxName, sql, iLine
							.getM_InOutLine_ID());
					if (count == 0) {
						if (iLine.getM_Product_ID() > 0
								&& M_ProductFreight_ID != iLine
										.getM_Product_ID()) {
							m_deleteCandidates.add(iLine);
						}
					}
				}
			}

			if (m_deleteCandidates.size() > 0 || m_updateCandidates.size()>0) {
				if (FDialog
						.ask(
								m_WindowNo,
								null,
								"Pressing OK will complete shipment.<BR/>" +
								"There are shipment lines that were updated<br/> " +
								"Do you want to proceed?")) {
					for (MInOutLine iLine : m_deleteCandidates) {
						if (!iLine.delete(true, m_trxName)) {
							showLogError("Error while deleting shipment line");
							try {
								DB.rollback(true, m_trxName);
							} catch (Exception e) {
								showError("Rollback Error -" + e.getMessage());
							}
							return;
						}
					}
					
					for (MInOutLine iLine : m_updateCandidates) {
						if (!iLine.save( m_trxName)) {
							showLogError("Error while updating shipment line");
							try {
								DB.rollback(true, m_trxName);
							} catch (Exception e) {
								showError("Rollback Error -" + e.getMessage());
							}
							return;
						}
					}
					for (MInOutLine iLine : m_insertCandidates) {
						if (!iLine.save( m_trxName)) {
							showLogError("Error while creating new shipment line");
							try {
								DB.rollback(true, m_trxName);
							} catch (Exception e) {
								showError("Rollback Error -" + e.getMessage());
							}
							return;
						}
					}
					
					String sql = "UPDATE M_packageline " +
				             "SET m_inoutline_id = COALESCE((SELECT m_inoutline_id " +
				             "  FROM m_inoutline WHERE Holder_InOutLine_ID = M_packageline.m_inoutline_id AND m_locator_id = M_packageline.m_locator_id),  m_inoutline_id) " +
				             "FROM M_Package p " +
				             "WHERE p.m_package_id = m_packageline.m_package_id AND p.M_InOut_ID=?";
					
					DB.executeUpdate(sql, inout.get_ID(), m_trxName);

				} else {
					fUPC.getComponent().focus();
					return;

				}
			}

			inout.set_TrxName(m_trxName);
			if ("IN".equals(inout.getDocStatus())) {
				if (!inout.processIt(MInOut.DOCACTION_Prepare)) {
					showError(Msg.parseTranslation(ctx, inout.getProcessMsg()));
					try {
						DB.rollback(false, m_trxName);
					} catch (Exception ex) {
					}
					inout.set_TrxName(null);
					return;
				}
			}

			
			if (!inout.processIt(MInOut.DOCACTION_Complete)) {
				showError(Msg.parseTranslation(ctx, inout.getProcessMsg()));
				try {
					DB.rollback(false, m_trxName);
				} catch (Exception ex) {
				}
				inout.set_TrxName(null);
				return;
			}
			//additional shipment status here - jobrian
			//inout.set_ValueOfColumn("ShipmentStatus", WShipmentScannerForm.STATUS_AWAITING_SHIPPING);
			int noofPackages = ((BigDecimal)fNoofPackages.getValue()).intValue();
			inout.setNoPackages(noofPackages);
			if (!inout.save(m_trxName)) {
				showLogError("Can not save shipment");
				try {
					DB.rollback(false, m_trxName);
				} catch (Exception ex) {
				}
				inout.set_TrxName(null);
				return;
			}
			try {
				DB.commit(true, m_trxName);
			} catch (Exception e) {
				showError("Rollback Error -" + e.getMessage());
				inout.set_TrxName(null);
				return;
			}

			//prompt to print invoice if available
			MInvoice invoice = (MInvoice) inout.getC_Invoice();
			if (invoice != null && inout.getC_DocType().isCreateInvoice()) {
//				if (FDialog.ask(m_WindowNo, this, "Do you want to print invoice?"))
//				{
					printInvoice(invoice.getC_Invoice_ID());
//				}	//	OK to print

			}
			onExit();
		} catch (Exception e) {
			showLogError(e.getMessage());
			try {
				DB.rollback(false, m_trxName);
			} catch (Exception ex) {
			}
		}
	}

	private void print(MPrintConfig config) {
		
		if (config == null || config.getAD_PrintFormat_ID() == 0) {
			return;
		}
		MPrintFormat pf = new MPrintFormat(ctx, config.getAD_PrintFormat_ID(), null);

		ReportEngine re = ReportEngine.get(ctx, pInfo);
		re.setPrintFormat(pf);

		if (config.isDirectPrint()) {
			ReportCtl.createOutput(re, false, pf.getPrinterName());
		}
		else {
			Clients.showBusy("Preparing PDF");
			try {
				//String path = System.getProperty("java.io.tmpdir");
				//File file = File.createTempFile("MOVEMENT", ".pdf", new File(path));
				//file = re.getPDF();
				Window win = new SimplePDFViewer(config.getName(), new FileInputStream(re.getPDF()));
				SessionManager.getAppDesktop().showWindow(win, "center");
			} catch (Exception e) {
				log.severe(e.getMessage());
			}
		}
	}
	
	private void printInvoice(int c_invoice_id) throws Exception {
		ReportEngine re = ReportEngine.get(ctx, ReportEngine.INVOICE, c_invoice_id, null);
		if (re == null) {
			return;
		}

		try {
			String path = System.getProperty("java.io.tmpdir");
			File file = File.createTempFile("INVOICE", ".pdf", new File(path));
			
			file = re.getPDF();
			
			Clients.clearBusy();
			Window win = new SimplePDFViewer("Invoice", new FileInputStream(file));
			SessionManager.getAppDesktop().showWindow(win, "center");
			
		} catch (Exception e) {
			log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		}


	}
	
	private void onDelete() {
		if(packList.size()>1){
			FDialog.ask(m_WindowNo, null, 
					"Do you want to delete the Package?", 
					new Callback<Boolean>() {

				@Override
				public void onCallback(Boolean result) {
					boolean deletePackage = false;
					boolean deleteLines = false;
					if (result) {
						deletePackage = true;
						deleteLines = true;
					}
					onDelete0(deletePackage, deleteLines);
				}
				
			});
		}
	}
	
	private void onDelete0(boolean deletePackage, boolean deleteLines) {
		if(!deletePackage && packList.size()>0){
			FDialog.ask(m_WindowNo, null, 
					"Selecting Ok will delete all package line.<br/> Do you want to delete all package line?", 
					new Callback<Boolean>() {

				@Override
				public void onCallback(Boolean result) {
					if (result) {
						for (MPackageLine line : packLines) {
//							MInOutLine iol = new MInOutLine(ctx, line.getM_InOutLine_ID(),
//									m_trxName);
							//jobriant
							//fix cache problem
							MInOutLine iol = null;
							for (MInOutLine ioLine : ioLines) {
								if (line.getM_InOutLine_ID() == ioLine.getM_InOutLine_ID()) {
									iol = ioLine;
									break;
								}
							}
								MProduct product = iol.getProduct();
							if (product.getM_AttributeSet().isSerNo()) {
								
								scannedASI.remove(iol.getM_AttributeSetInstance_ID());
								iol.setM_AttributeSetInstance_ID(0);
								if (!iol.save()) {
									showLogError("Can not update shipment line");
									try {
										DB.rollback(false, m_trxName);
									} catch (Exception e) {
										log.severe(e.toString());
									}
								}
							}
							if (!line.delete(true, m_trxName)) {
								showLogError("Can not update shipment line");
								try {
									DB.rollback(false, m_trxName);
								} catch (Exception e) {
									log.severe(e.toString());
								}
							}

						}
						
						if(deletePackage){
							MPackage packToDel = packList.remove(m_PackageIndex);
							if(m_PackageIndex>=packList.size())
								m_PackageIndex--;
							if (!packToDel.delete(true,m_trxName)){
								showLogError("Can not delete package");
								try {
									DB.rollback(false, m_trxName);
								} catch (Exception e) {
									log.severe(e.toString());
								}
							}
						}
						
						try {
							DB.commit(true, m_trxName);
						} catch (Exception e) {
							showError("Commit Error " + e.getMessage());
						}
						
						reloadTable();
					}
				}
				
			});
		}
	}

	private void updateItemCount() {
//		String sql = "Select sum(pline.qty) from m_package p inner join m_packageLine pline on (p.m_package_id=pline.m_package_id) where p.m_inout_id=?";
//		int cnt = DB.getSQLValue(null, sql,inout.getM_InOut_ID());
//		lItemCount.setText("Total Items = " + cnt + "/"
//				+ totalItem);
		String sql = "SELECT COALESCE(SUM(Qty),0) FROM M_InOutLine iol INNER JOIN M_PackageLine pl ON iol.m_inoutline_id = pl.m_inoutline_id WHERE iol.m_product_id IS NOT NULL AND iol.M_InOut_ID = ?";
		int cnt = DB.getSQLValue(m_trxName, sql,inout.getM_InOut_ID());
		lScanned.setValue("Scanned=" + cnt);
		int excess = cnt - totalItem;
		lShort.setValue("Short=" + (excess<0?excess*-1:0));
		lExcess.setValue("Excess=" + (excess>0?excess:0));
			//lMessage.setText("Total Items Scanned = " + cnt);
	}

	private void showLogError(String msgDefault) {
		String msg = null;
		ValueNamePair err = CLogger.retrieveError();
		if (err != null)
			msg = err.getName();
		if (msg == null || msg.length() == 0)
			msg = msgDefault;

		showError(msg);
	}

	private void assignPackageLines(MInOutLine ioLine) {
		String sql = " " +
	             "SELECT m_locator_id, SUM(qty) FROM m_packageline  " +
	             "WHERE m_inoutline_id = ? " +
	             "GROUP BY m_locator_id ORDER BY 2 DESC";
		PreparedStatement pstmt = DB.prepareStatement(sql, null);
		ResultSet rs = null;
		int idx = 1;
		try {
			pstmt.setInt(1, ioLine.getM_InOutLine_ID());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				if (idx == 1) {
					ioLine.setMovementQty(rs.getBigDecimal(2));
					ioLine.setQty(rs.getBigDecimal(2));
					ioLine.setM_Locator_ID(rs.getInt(1));
					ioLine.set_ValueOfColumn("Holder_InOutLine_ID", ioLine.getM_InOutLine_ID());
					m_updateCandidates.add(ioLine);
				}
				else { // create new line
					MInOutLine newIOLine = new MInOutLine(ctx, 0, m_trxName);
					MInOutLine.copyValues(ioLine, newIOLine);
					newIOLine.setMovementQty(rs.getBigDecimal(2));
					newIOLine.setAD_Org_ID(ioLine.getAD_Org_ID());
					newIOLine.setQty(rs.getBigDecimal(2));
					newIOLine.setM_Locator_ID(rs.getInt(1));
					newIOLine.set_ValueOfColumn("Holder_InOutLine_ID", ioLine.getM_InOutLine_ID());
					newIOLine.setLine(newIOLine.getLine() + idx);
					m_insertCandidates.add(newIOLine);
				}
				idx++;
			}
		}
		catch (Exception e) {
			
		} 
		finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
	}
	
	/*
	private void showDBError(String msg) {
		statusBar.setStatusDB(msg);
	}
*/
	private void showError(String msg) {
		//statusBar.setStatusLine(msg, true, true);
		lMessage.setValue(msg);
	}

	/*
	private void removePackLine(MPackageLine packLine, int M_Product_ID) {

		int status = statusCount.get(packLine.getM_PackageLine_ID());
		int count = productCount.get(M_Product_ID);
		if (status > 0 && count > 0) {
			for (MPackageLine pline : packLines) {

				int tmp = statusCount.get(pline.getM_PackageLine_ID());
				if (tmp > status) {
					MInOutLine iol = new MInOutLine(ctx, pline
							.getM_InOutLine_ID(), null);
					if (iol.getM_Product_ID() == M_Product_ID) {
						tmp--;
						statusCount.put(pline.getM_PackageLine_ID(), tmp);
					}
				}
			}

			status--;
			count--;
			productCount.put(M_Product_ID, count);

		}

		packLines.remove(packLine);
		//refreshTable();
	}
	private Integer getTotalProduct(int M_Product_ID) {
		if (ioProdCount.get(M_Product_ID) != null) {
			return ioProdCount.get(M_Product_ID);
		}

		int count = DB
				.getSQLValue(
						null,
						"select sum(qtyEntered) from M_InoutLine where M_Inout_ID=? and M_Product_ID=?",
						inout.getM_InOut_ID(), M_Product_ID);
		ioProdCount.put(M_Product_ID, count);
		return count;
	}
	
	 */
	/**
	 * Serial OK to add - no duplicate
	 * @param pCode
	 * @param serNo
	 * @return
	 */
	private boolean isSerialOK(String pCode, String serNo)
	{
		String  key  = pCode + "-" + serNo;
		boolean isExists = mapProductSerial.containsKey(key);
		if (isRemoveMode) {
			// we are trying to remove this item, OK if you can find it
			if (!isExists) {
				showError("You are trying to remove AttNo=" + serNo + " that don't exists.");
			}
			return isExists;
		}	
		else {
			// this is the opposite, we only allow Item to add if serial don't exists
//			if (isExists) {
//				showError("You are trying to add AttNo=" + serNo + " that already exists in the shipment.");
//			}
//			return !isExists;
			return true;
		}	
	}
	
	private MAttributeSetInstance getAttributeSetInstance(MProduct product, String serial) {
		return MAttributeSetInstance.getWithSerialNo(ctx, serial, product);
	}
	
	
	/**
	 * 
	 * @param product_ID
	 * @param qty
	 * @param add - flag to add or deduct qty
	 * @return
	 */
	private boolean updateScannedQtyProductMap(int product_ID, BigDecimal qty, boolean add)
	{
		BigDecimal qtyAllowed = mapProductToScan.get(product_ID);
		BigDecimal newQty = Env.ZERO;
		
		if (mapProductScanned.containsKey(product_ID)) {
			newQty = mapProductScanned.get(product_ID);
			if (add) {
				newQty = newQty.add(qty);
			}
			else {
				newQty = newQty.subtract(qty);
			}
		}
		
		if (newQty.signum() < 0) {
			return false;  //you're taking more than possible
		}
		if  (newQty.compareTo(qtyAllowed) > 0 && !isAllowOverShipping) {
			return false;  // will result to over receipt
		}
		mapProductScanned.put(product_ID, newQty);
		return true; //mapping updated
	}
	
	private boolean isQtyOK(int product_ID, BigDecimal qty, boolean add)
	{
		BigDecimal qtyAllowed = mapProductToScan.get(product_ID);
		BigDecimal newQty = Env.ZERO;
		
		if (qtyAllowed == null) {
			qtyAllowed = Env.ZERO;
		}
		if (mapProductScanned.containsKey(product_ID)) {
			newQty = mapProductScanned.get(product_ID);
			if (add) {
				newQty = newQty.add(qty);
			}
			else {
				newQty = newQty.subtract(qty);
			}
		}
		
		if (newQty.signum() < 0) {
			showError("Process will result in negative receive qty.");
			return false;  //you're taking more than possible
		}
		if  (newQty.compareTo(qtyAllowed) > 0 && !isAllowOverShipping) {
			showError("Process will result in Over Shipping.");
			return false;  // will result to over receipt
		}
		return true; //mapping updated
	}
	
	private void resetHashMap()
	{
		mapProductToScan.clear();
		mapProductScanned.clear();
		mapProductSerial.clear();
		
		String sql = "SELECT m_product_id, SUM(qtyentered) " +
	              "FROM m_inoutline " +
		          "WHERE m_inout_id = ?" +
		          "GROUP BY m_product_id";

		try (PreparedStatement pstmt = DB.prepareStatement(sql, m_trxName)) {
			pstmt.setInt(1, inout.getM_InOut_ID());
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				mapProductToScan.put(rs.getInt(1), rs.getBigDecimal(2));
			}
		}
		catch (Exception e) {
			log.severe(e.getMessage());
		}

		sql = "SELECT il.m_product_id, SUM(pl.qty) " +
				"FROM m_inoutline il INNER JOIN m_packageline pl ON il.m_inoutline_id = pl.m_inoutline_id " +
				"WHERE il.m_inout_id = ? " +
				"GROUP BY il.m_product_id";

		try (PreparedStatement pstmt = DB.prepareStatement(sql, m_trxName)) {
			pstmt.setInt(1, inout.getM_InOut_ID());
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				mapProductScanned.put(rs.getInt(1), rs.getBigDecimal(2));
			}
		}
		catch (Exception e) {
			log.severe(e.getMessage());
		}

		sql = "SELECT p.value || '-' || asi.serno, iol.m_inoutline_id " +
				"FROM m_inoutline iol " +
				"  INNER JOIN m_attributesetinstance asi ON asi.m_attributesetinstance_id = iol.m_attributesetinstance_id " +
				"  INNER JOIN m_product p ON iol.m_product_id = p.m_product_id " +
				"WHERE asi.m_attributesetinstance_id <> 0 " +
				"AND   iol.m_inout_id = ?";
		try (PreparedStatement pstmt = DB.prepareStatement(sql, m_trxName)) {
			pstmt.setInt(1, inout.getM_InOut_ID());
			ResultSet rs = pstmt.executeQuery();
				
			while (rs.next()) {
				mapProductSerial.put(rs.getString(1), rs.getInt(2));
			}
							
		}
		catch (Exception e){
			log.severe(e.getMessage());
		}		
	}

	private MProduct findProduct(String barcode, boolean isUPC)
	{
		String key = barcode;
		if ((barcode.length() > WPackageScannerWindow.PARTNO_MAXLENGTH) ) {		
			key = barcode.substring(0,  WPackageScannerWindow.PARTNO_MAXLENGTH) + "%";
		}
		List<MProduct> productsFound = null;

		if (isUPC) {
			productsFound = MProduct.getByUPC(ctx, key, m_trxName);
		}
		else {
			productsFound = new Query(ctx, MProduct.Table_Name, "value LIKE ?",
					m_trxName).setParameters(key).list();
		}
		
		if (productsFound.size() == 0) {
			showError("Invalid Product code : " + key);
			return null;
		}
		
		if (productsFound.size() > 1) {
			showError("Multiple products found with trucated Bar Code: " + key);
			return null;
		}
		
		MProduct product = productsFound.get(0);
		
		if (!isAllowOverShipping) {
			if (!mapProductToScan.containsKey(product.getM_Product_ID())) {
				showError( product.toString() + "is not on list.");
				return null;
			}
		}
		
		return product;
		
	}
	
	/**
	 * At this stage, product is valid but needs to check qty
	 * @param product
	 */
	private void processScan(MProduct product, String serialNo)
	{
		
		boolean isSerial = false;
		BigDecimal initialValue = Env.ONE;
		boolean isSerialOK = false;
		BigDecimal qtyScan = null;
		String sql = "SELECT value " +
	             "FROM m_locator " +
	             "WHERE m_locator_id IN (SELECT MAX(iol.m_locator_id) " +
	             "                       FROM m_inoutline iol " +
	             "                       WHERE iol.m_product_id = ? " +
	             "						 AND iol.holder_inoutline_id = iol.m_inoutline_id " + 
	             "						 AND iol.isExcessReceipt = 'N'	" +
	             "                       AND   iol.m_inout_id = " + inout.getM_InOut_ID() +
	             "                       )";		
		
		//List
		String defaultLocator = DB.getSQLValueString(m_trxName, sql, product.getM_Product_ID());
		
		if (serialNo != null) {
			isSerial = true;
			qtyScan = getQtyReceived(isSerial, initialValue, true, product.toDisplayString(), defaultLocator, product);
			if (!isQtyOK(product.getM_Product_ID(), qtyScan, !isRemoveMode)) {
				return;
			}
			
			isSerialOK = isSerialOK(product.getValue(), serialNo);
			if (!isSerialOK) {
				return;
			}
		}
		else {
			initialValue = mapProductToScan.get(product.getM_Product_ID());
			BigDecimal productScanned = mapProductScanned.get(product.getM_Product_ID());
			boolean isNotOnList = false;
			
			if (initialValue == null) {
				//defaultLocator = MLocator.get(ctx, excessDeliveryLocator_ID).getValue(); 
				initialValue = Env.ZERO;  //excess receipt
				isNotOnList = true;
			}
			if (productScanned == null) {
				productScanned = Env.ZERO;  //excess receipt
			}

			initialValue = initialValue.subtract(productScanned);
			qtyScan = getQtyReceived(isSerial, initialValue, false, product.toDisplayString(), defaultLocator, product);
			
			// not on the list just save it to db
			if (isNotOnList) {
				//addExcessReceiptLine(product, qtyScan);
				return;
			}
			
			if (!isQtyOK(product.getM_Product_ID(), qtyScan, !isRemoveMode)) {
				return;
			}
		}
			
		// this shouldn't happen - dialog shouldn't allow an invalid locator
		if (m_M_Locator_ID == 0) {
			return;
		}
		
		//everything works find, we can update the user of the new status and proceed to next 
		if (loadItemToMaterialReceipt(product, serialNo, qtyScan, m_M_Locator_ID, !isRemoveMode))
		{
			updateScannedQtyProductMap(product.getM_Product_ID(), qtyScan, !isRemoveMode);
			reloadTable();
//			packLines = new Query(ctx, MPackageLine.Table_Name,
//					"M_Package_ID=?", null).setParameters(
//					packList.get(curPackage).getM_Package_ID()).setOnlyActiveRecords(true)
//					.list();
		}
	}

	private BigDecimal getQtyReceived(boolean isSerial, BigDecimal initialValue
			, boolean readOnly, String title, String defaultLocator, MProduct product) 
	{
		
		IOLineEditorDialog.Mode mode = IOLineEditorDialog.Mode.ADD;
		if (isRemoveMode) {
			mode = IOLineEditorDialog.Mode.REMOVE;
		}
		m_M_Locator_ID = 0; //initialise
		
		IOLineEditorDialog dialog = new IOLineEditorDialog(this.inout.getM_Warehouse_ID(),
				title, initialValue,
				isSerial, 100, !readOnly, mode, product, isCrossDock, null);
		
		dialog.setLocator(defaultLocator );
		dialog.setM_PackageLine_ID(0);
		dialog.setAttribute(Window.MODE_KEY, Window.MODE_HIGHLIGHTED);
		SessionManager.getAppDesktop().showWindow(dialog);
		
		if (!dialog.isCancelled()) {
			if (isCrossDock) {
				
			} else {
				m_M_Locator_ID = dialog.getM_Locator_ID();
			}
			return dialog.getQty();
		}

		return BigDecimal.ZERO;
	}

	private boolean loadItemToMaterialReceipt(MProduct product, String serNo,
			BigDecimal qtyScanned, int M_Locator_ID, boolean add)
	{
		List<MInOutLine> ioLineCandidates = new ArrayList<MInOutLine>();
		List<MPackageLine> packLineCandidates = new ArrayList<MPackageLine>();
		for (MInOutLine line : ioLines) {
			if (line.getM_Product_ID() == product.getM_Product_ID()) {
				//just list all lines that have this product
				ioLineCandidates.add(line);
			}
		}
		
		for (MPackageLine pline : packLines)
		{
			if (pline.getM_InOutLine().getM_Product_ID() == product.getM_Product_ID())
			{
				packLineCandidates.add(pline);
			}
		
		}
		if (!m_IsPreLoad && packLineCandidates.size() == 0) {
			MPackageLine newLine = new MPackageLine(packList.get(m_PackageIndex));
			MInOutLine iol = ioLineCandidates.get(0);
			newLine.setM_InOutLine_ID(iol.getM_InOutLine_ID());
			newLine.setQty(Env.ZERO);
			newLine.set_ValueOfColumn("M_Locator_ID", M_Locator_ID);
			newLine.save(m_trxName);

			packLines.add(newLine);
			packLineCandidates.add(newLine);
		}
	
		
		if (ioLineCandidates.size() == 0 && !isAllowOverShipping) {
			log.severe("loadItemToMaterialReceipt - Various checks should prevent this to happen.");
			return false;
		}


		//deduct 1 item for the list - alway 1 item at time when removing
		if (!add) 	{
			if (serNo != null) {
				//it's serial, just look in the map
				return processRemoveSerial(product, serNo);
			}
			return processRemoveProduct(qtyScanned, M_Locator_ID, packLineCandidates);
		}

		if (serNo != null) 	{
			return processAddSerial(product, serNo, M_Locator_ID, ioLineCandidates, packLineCandidates);
		}
		
		return processAddProduct(qtyScanned, M_Locator_ID, packLineCandidates);
	}

	/**
	 * This is the heart of shipment process - make sure the scanned items are allocated properly
	 * @param qtyScanned
	 * @param packLineCandidates
	 * @return
	 */
	private boolean processAddProduct(BigDecimal qtyScanned, int M_Locator_ID,
			List<MPackageLine> packLineCandidates) {
		//at this stage, we expect shipment qtyScanned to fit in
		BigDecimal qtyLeftToDistribute = qtyScanned;


		int lastM_InoutLine_ID = 0;
		BigDecimal runningQtyScanned = null;
		BigDecimal allowedQty= null;
		List<MPackageLine> activeLines = null; //they contain all lines having same holder_inoutline_id
		for (MPackageLine line : packLineCandidates) {
			if (lastM_InoutLine_ID != line.getM_InOutLine_ID())
			{
				if (lastM_InoutLine_ID != 0) {
					qtyLeftToDistribute = distributeToReceiptLines(
							M_Locator_ID, qtyLeftToDistribute,
							lastM_InoutLine_ID, runningQtyScanned,
							allowedQty, activeLines);

					if (qtyLeftToDistribute.signum() == 0) {
						//log.severe("Unable to distribute " + product.toDisplayString() + "=" + qtyLeftToDistribute);
						return true;
					}
				}

				//reset and start all over again
				lastM_InoutLine_ID = line.getM_InOutLine_ID();
				activeLines = new ArrayList<MPackageLine>();
				runningQtyScanned = Env.ZERO;
				allowedQty = line.getM_InOutLine().getQtyEntered();
			}
			
			activeLines.add(line);
			runningQtyScanned = runningQtyScanned.add(line.getQty());
			

		}
		if (runningQtyScanned == null) {
			runningQtyScanned = Env.ZERO;
		}
		if (allowedQty == null) {
			allowedQty = Env.ZERO;
		}
		if (activeLines == null) {
			activeLines = new ArrayList<MPackageLine>();
		}
		if  (!m_IsPreLoad && allowedQty.signum() == 0) {
			allowedQty = qtyScanned;
		}
	
		qtyLeftToDistribute = distributeToReceiptLines(
				M_Locator_ID, qtyLeftToDistribute,
				lastM_InoutLine_ID, runningQtyScanned,
				allowedQty, activeLines);

		if (qtyLeftToDistribute.signum() != 0) {
			log.severe("Unable to distribute " + m_product.toDisplayString() + "=" + qtyLeftToDistribute);
			return false;
		}

		return true;
	}

	

	/**
	 * @param M_Locator_ID
	 * @param qtyLeftToDistribute
	 * @param M_InOutLine_ID
	 * @param runningQtyScanned
	 * @param allowedQty
	 * @param activeLines
	 * @param line
	 * @return
	 */
	private BigDecimal distributeToReceiptLines(int M_Locator_ID,
			BigDecimal qtyLeftToDistribute, int M_InOutLine_ID,
			BigDecimal runningQtyScanned, BigDecimal allowedQty,
			List<MPackageLine> activeLines) 
	{
		BigDecimal distributeQty = allowedQty.subtract(runningQtyScanned);

		if (qtyLeftToDistribute.compareTo(distributeQty) < 1) {
			distributeQty = qtyLeftToDistribute; // don't distribute more than you're asked to
		}
		// Collections.sort(activeLines, ascM_InOutLine_ID);

		int nrow = 0;
		// process activelines
		for (MPackageLine activeLine : activeLines) {
			if (qtyLeftToDistribute.signum() == 0) {
				// nothing to distribute
				return Env.ZERO;
			}

			if (activeLine.get_ValueAsInt("M_Locator_ID") == M_Locator_ID) {
				BigDecimal qty = distributeQty;
				BigDecimal scanned = activeLine.getQty(); 
				activeLine.setQty(scanned.add(qty));
				activeLine.saveEx(m_trxName);
				distributeQty = distributeQty.subtract(qty);
				qtyLeftToDistribute = qtyLeftToDistribute.subtract(qty);

			}

			nrow++;
		}
		
		// you reached this part - still have qty to distribute
		// create new line with this as holder
		// create new line - same but different locator
		if (distributeQty.signum() > 0) {
			MPackageLine newLine = new MPackageLine(packList.get(m_PackageIndex));
			newLine.setM_InOutLine_ID(M_InOutLine_ID);
			newLine.setQty(distributeQty);
			newLine.set_ValueOfColumn("M_Locator_ID", M_Locator_ID);
			newLine.save(m_trxName);
			qtyLeftToDistribute = qtyLeftToDistribute
					.subtract(distributeQty);
			packLines.add(newLine);
			//ioLines.add(newLine);
		}

		if (qtyLeftToDistribute.signum() == 0) {
			// exit immediately - no need to process other stuff
			return Env.ZERO;
		}
		
		//No over shipping - leave it for the meantime in case something changed - jobriant
//		// create new line - excess
//		int excess_locator_id = ((MWarehouse)inout.getM_Warehouse()).get_ValueAsInt("ExcessDelivery_Locator_ID");
//		if (excess_locator_id == 0) {
//			showError("Excess Delivery Locator not set in the warehouse");
//			return new BigDecimal(-1);
//		}
//		m_M_Locator_ID = excess_locator_id;
//		//addExcessReceiptLine(originalLine.getProduct(), qtyLeftToDistribute);
//
//		return Env.ZERO;
		
		return (qtyLeftToDistribute);
	}


	/**
	 * @param product
	 * @param serNo
	 * @param M_Locator_ID
	 * @param ioLineCandidates
	 */
	private boolean processAddSerial(MProduct product, String serNo,
			int M_Locator_ID, List<MInOutLine> ioLineCandidates, List<MPackageLine> packLineCandidates) 
	{
		
		MAttributeSetInstance asi = getAttributeSetInstance(product, serNo);
		if (asi == null) {
			showError("Invalid. SerialNo=" + serNo);
			return false;
		}
		//it's Serial, we only need to add 1 item, so just chuck it in
		//to the first receipt line available
		for (MInOutLine line : ioLineCandidates)
		{
			MPackageLine ioPLine = null;
			for (MPackageLine pline : packLineCandidates) {
				if (line.getM_InOutLine_ID() == pline.getM_InOutLine_ID())
				{
					//has package assigned
					ioPLine = pline;
					break;
				}
			}
			
			if (ioPLine == null) {
				//nothing assigned to this ioLine, use this to ship
				line.setM_Locator_ID(M_Locator_ID);
				line.setM_AttributeSetInstance_ID(asi.getM_AttributeSetInstance_ID());
				line.save(m_trxName);
				
				MPackageLine newLine = new MPackageLine(packList.get(m_PackageIndex));
				newLine.setInOutLine(line);
				newLine.set_ValueOfColumn("M_Locator_ID", M_Locator_ID);
				newLine.save(m_trxName);
				
				addSerialToProductMap(product.getValue(), serNo, newLine.getM_PackageLine_ID());
				return true;
			}
			
		}
		return false;
	}
	
	/**
	 * @param product
	 * @param serNo
	 * @param ioLineCandidates
	 */
	private boolean processRemoveSerial(MProduct product, String serNo) {
		int m_packageline_id = getLineFromProductSerial(product.getValue(), serNo);
		MPackageLine line = new MPackageLine(ctx, m_packageline_id, m_trxName);
		line.delete(true);
		line.save(m_trxName);
		deleteSerialProductMap(product.getValue(), serNo);
		return true;
	}

	

	/**
	 * @param qtyScanned
	 * @param M_Locator_ID
	 * @param packLineCandidates
	 */
	private boolean processRemoveProduct(BigDecimal qtyScanned, int M_Locator_ID,
			List<MPackageLine> packLineCandidates) {
		for (MPackageLine line : packLineCandidates) {
			if (line.get_ValueAsInt("M_Locator_ID") == M_Locator_ID) {
				BigDecimal qty = line.getQty();
				qty = qty.subtract(qtyScanned);
				if (qty.signum() >= 0) 
				{
					//we are able to remove back the scanned item from the receipt
					line.setQty(qty);
					line.save(m_trxName);
					return true;
				}
			}
		}
		return false;
	}
	

	/**
	 * Add Serial No to mapping table
	 * @param pCode
	 * @param serNo
	 * @return 
	 * 		true = added to map
	 * 		false = already SerNo already exists
	 */
	private boolean addSerialToProductMap(String pCode, String serNo, int M_InOutLine_ID)
	{
		String  key  = pCode + "-" + serNo;
		mapProductSerial.put(key, M_InOutLine_ID);
		return true;
	}
	
	/**
	 * 
	 * @param pCode
	 * @param serNo 
	 * @return M_PackageLine_ID
	 */
	private int getLineFromProductSerial(String pCode, String serNo)
	{
		String  key  = pCode + "-" + serNo;
		return  mapProductSerial.get(key);
	}
	
	
	/**
	 * Remove the Serial from the mapping
	 * @param pCode
	 * @param serNo
	 * @return
	 */
	private boolean deleteSerialProductMap(String pCode, String serNo)
	{
		String  key  = pCode + "-" + serNo;
		mapProductSerial.remove(key);
		return true;
	}
	
	private void onProductScan(Textbox text)
	{
		boolean isUPC = false;
		if (text == fUPC.getComponent().getTextbox()) {
			isUPC = true;
		}
		
		MProduct product = findProduct(text.getValue(),isUPC);
		
		if (product == null)
		{
			lProductInfo.setText("");
			text.setValue("");
			return;
		}

		lProductInfo.setText(product.toDisplayString());
		m_product = product;
		boolean isSerial = isProductSerialised(product);
		
		//statusBar.setStatusLine("", false, false);
		lMessage.setValue("");
		//still need to check the serial
		if (isSerial) {
			fSerial.setValue("");
			fSerial.setHasFocus(true);
			fSerial.getComponent().focus();
		}
		
		processScan(product, null);
		
		updateItemCount();
	}
	
	private boolean isProductSerialised(MProduct product) {
		if (product.getAttributeSet() != null) {
			return product.getAttributeSet().isSerNo();
		}
		return false;
	}
	
}
