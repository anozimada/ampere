package org.adaxa.form;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.exceptions.DBException;
import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.exceptions.ValueChangeListener;
import org.adempiere.util.Callback;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Combobox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.editor.WTableDirEditor;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.MActivity;
import org.compiere.model.MColumn;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MProduct;
import org.compiere.model.MProjectTask;
import org.compiere.model.MRole;
import org.compiere.model.MTimesheetEntry;
import org.compiere.model.MUOM;
import org.compiere.model.Query;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;
import org.json.JSONArray;
import org.json.JSONObject;
import org.zkoss.zk.au.out.AuScript;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.North;
import org.zkoss.zul.South;

public class WProjectTaskTimer extends ADForm implements IFormController, ValueChangeListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7875645507898640830L;

	private CustomForm form = new CustomForm();
	
	private static CLogger log = CLogger.getCLogger(WProjectTaskTimer.class);
	
	//
	private Borderlayout mainLayout = new Borderlayout();
	private Panel parameterPanel = new Panel();
	private Grid parameterLayout = GridFactory.newGridLayout();
	private Label userLabel = new Label();
	private WTableDirEditor userField = null;
	private Label projectLabel = new Label();
	private WTableDirEditor projectField = null;
	private Label bpartnerLabel = new Label();
	private WSearchEditor bpartnerField = null;
	private WListbox taskTable = ListboxFactory.newDataTable();
	private WListbox timerTable = ListboxFactory.newDataTable();
	private WListItemRenderer timerRenderer = null;
	private Borderlayout infoPanel = new Borderlayout();
	private Panel taskPanel = new Panel();
	private Panel timerPanel = new Panel();
	private Label taskLabel = new Label();
	private Label timerLabel = new Label();
	private Borderlayout taskLayout = new Borderlayout();
	private Borderlayout timerLayout = new Borderlayout();
	private Label taskInfo = new Label();
	private Label timerInfo = new Label();
	
	private Combobox activityField = new Combobox();
	
	private int userID = 0;
	private int projectID = 0;
	private int bpartnerID = 0;
	
	private List<Button> startTimerButtonList = new ArrayList<Button>();
	private Map<Integer, Button> startButtonMap = new HashMap<Integer, Button>();
	private Map<Integer, Button> pauseButtonMap = new HashMap<Integer, Button>();
	private Map<Integer, Button> stopButtonMap = new HashMap<Integer, Button>();
	private Map<Integer, Button> deleteButtonMap = new HashMap<Integer, Button>();
	private Map<Integer, Label> durationLabelMap = new HashMap<Integer, Label>();
	
	private Map<Integer, TimerState> timerStateMap = new HashMap<Integer, TimerState>();
	
	public WProjectTaskTimer() {
		super();
	}
	
	@Override
	protected void initForm() {
		if (Env.getAD_Org_ID(Env.getCtx()) == 0)
			throw new AdempiereException("Please pick Organization on login");
		
		try
		{
			dynInit();
			zkInit();
			
			timerRenderer = (WListItemRenderer) timerTable.getItemRenderer();
			
			refreshTasks();
			refreshTimers();
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
		}
	}
	
	@Override
	public void onPageAttached(Page newpage, Page oldpage) {
		super.onPageAttached(newpage, oldpage);
		
		// update active timer values on interval, stop the interval loop when no element is found
		String script = "timer = setInterval(function() {"
				+ "labels = document.querySelectorAll(\"#" + form.getUuid() + " .duration-label.active-timer\");"
				+ "for (const label of labels) {"
				+ "	var duration = label.innerHTML.split(':');"
				+ "	var s = (+duration[0]) * 60 * 60 + (+duration[1]) * 60 + (+duration[2]) + 1;" // increment the seconds
				+ " dateObj = new Date(s * 1000);\r\n" 
				+ "	hours = dateObj.getUTCHours();" 
				+ "	minutes = dateObj.getUTCMinutes();" 
				+ "	seconds = dateObj.getSeconds();"
				+ " timeString = hours.toString().padStart(2, '0') + ':' + "
				+ " 	minutes.toString().padStart(2, '0') + ':' +"
				+ " 	seconds.toString().padStart(2, '0');"
				+ "	label.innerHTML = timeString;"
				+ "}"
				+ "durationLabels = document.querySelectorAll(\"#" + form.getUuid() + " .duration-label\");"
				+ "if(durationLabels.length == 0) {"
				+ "	clearInterval(timer);"
				+ "}"
				+ "}, 1000);";
		AuScript aus = new AuScript(form, script);
		Clients.response(aus);
	}

	/**
	 *  Static Init
	 *  @throws Exception
	 */
	private void zkInit() throws Exception
	{
		//
		form.appendChild(mainLayout);
		ZKUpdateUtil.setWidth(mainLayout, "99%");
		ZKUpdateUtil.setHeight(mainLayout, "100%");
		//
		ZKUpdateUtil.setHeight(parameterPanel, "100%");
		parameterPanel.appendChild(parameterLayout);
		ZKUpdateUtil.setHeight(parameterPanel, "100%");
		parameterLayout.setStyle("overflow: auto;");
		userLabel.setText(Msg.translate(Env.getCtx(), "AD_User_ID"));
		projectLabel.setText(Msg.translate(Env.getCtx(), "C_Project_ID"));
		bpartnerLabel.setText(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		taskLabel.setText(Msg.translate(Env.getCtx(), "Available Tasks"));
		taskLabel.setStyle("font-size: 125%;");
		timerLabel.setText(Msg.translate(Env.getCtx(), "Active Timers"));
		timerLabel.setStyle("font-size: 125%;");
		taskPanel.appendChild(taskLayout);
		timerPanel.appendChild(timerLayout);
		timerInfo.setText(".");
		taskInfo.setText(".");
		
		North north = new North();
		north.setStyle("border: none");
		mainLayout.appendChild(north);
		north.appendChild(parameterPanel);
		
		Rows rows = null;
		Row row = null;
		
		ZKUpdateUtil.setWidth(parameterLayout, "100%");
		rows = parameterLayout.newRows();
		row = rows.newRow();
		
		row.appendCellChild(userLabel.rightAlign());
		row.appendCellChild(userField.getComponent(), 2);
		ZKUpdateUtil.setWidth(userField.getComponent(), "99%");
		
		row.appendCellChild(projectLabel.rightAlign());
		row.appendCellChild(projectField.getComponent(), 2);
		ZKUpdateUtil.setWidth(projectField.getComponent(), "99%");
		
		row.appendCellChild(bpartnerLabel.rightAlign());
		row.appendCellChild(bpartnerField.getComponent(), 2);
		ZKUpdateUtil.setWidth(bpartnerField.getComponent(), "99%");
		
		taskPanel.appendChild(taskLayout);
		ZKUpdateUtil.setWidth(taskPanel, "100%");
		ZKUpdateUtil.setHeight(taskPanel, "100%");
		ZKUpdateUtil.setWidth(taskLayout, "100%");
		ZKUpdateUtil.setHeight(taskLayout, "100%");
		taskLayout.setStyle("border: none");
		
		timerPanel.appendChild(timerLayout);
		ZKUpdateUtil.setWidth(timerPanel, "100%");
		ZKUpdateUtil.setHeight(timerPanel, "100%");
		ZKUpdateUtil.setWidth(timerLayout, "100%");
		ZKUpdateUtil.setHeight(timerLayout, "100%");
		timerLayout.setStyle("border: none");
		
		north = new North();
		north.setStyle("border: none");
		taskLayout.appendChild(north);
		north.appendChild(taskLabel);
		South south = new South();
		south.setStyle("border: none");
		taskLayout.appendChild(south);
		south.appendChild(taskInfo.rightAlign());
		Center center = new Center();
		taskLayout.appendChild(center);
		center.appendChild(taskTable);
//		ZKUpdateUtil.setWidth(taskTable, "99%");
//		ZKUpdateUtil.setHeight(taskTable, "99%");
		taskTable.setSclass("form-task-timer task-table");
		center.setStyle("border: none");
		
		north = new North();
		north.setStyle("border: none");
		timerLayout.appendChild(north);
		north.appendChild(timerLabel);
		south = new South();
		south.setStyle("border: none");
		timerLayout.appendChild(south);
		south.appendChild(timerInfo.rightAlign());
		center = new Center();
		timerLayout.appendChild(center);
		center.appendChild(timerTable);
//		ZKUpdateUtil.setWidth(timerTable, "99%");
//		ZKUpdateUtil.setHeight(timerTable, "99%");
		timerTable.setSclass("form-task-timer timer-table");
		center.setStyle("border: none");
		//
		center = new Center();
		ZKUpdateUtil.setVflex(center, "flex");
		mainLayout.appendChild(center);
		center.appendChild(infoPanel);
		
		infoPanel.setStyle("border: none");
		ZKUpdateUtil.setWidth(infoPanel, "100%");
		ZKUpdateUtil.setHeight(infoPanel, "100%");
		
		north = new North();
		north.setStyle("border: none");
		ZKUpdateUtil.setHeight(north, "49%");
		infoPanel.appendChild(north);
		north.appendChild(taskPanel);
		north.setSplittable(true);
		center = new Center();
		center.setStyle("border: none");
		ZKUpdateUtil.setVflex(center, "flex");
		infoPanel.appendChild(center);
		center.appendChild(timerPanel);
		
		this.appendChild(form);
	}   //  jbInit

	/**
	 *  Dynamic Init (prepare dynamic fields)
	 *  @throws Exception if Lookups cannot be initialized
	 */
	public void dynInit() throws Exception
	{
		//  User
		int columnID = MColumn.get(Env.getCtx(), "S_Resource", "AD_User_ID").get_ID();
		MLookup lookupUser = MLookupFactory.get (Env.getCtx(), form.getWindowNo(), 0, columnID, DisplayType.TableDir);
		userField = new WTableDirEditor("AD_User_ID", true, true, true, lookupUser);
		userID = Env.getAD_User_ID(Env.getCtx());
		userField.setValue(userID);
		userField.addValueChangeListener(this);
		
		MRole role = MRole.get(Env.getCtx(), Env.getAD_Role_ID(Env.getCtx()));
		if (role.isClientAccess(0, true))
			userField.setReadWrite(true);
		else
			userField.setReadWrite(false);
		
		//  Project
		columnID = MColumn.get(Env.getCtx(), "S_TimesheetEntry", "C_Project_ID").get_ID();
//		MLookup lookupProject = MLookupFactory.get (Env.getCtx(), form.getWindowNo(), 0, columnID, DisplayType.TableDir);
		MLookup lookupProject = MLookupFactory.get(Env.getCtx(), 
				form.getWindowNo(), 
				columnID, 
				DisplayType.TableDir, 
				Env.getLanguage(Env.getCtx()), 
				"C_Project_ID", 
				0, 
				false, 
				"C_Project.IsActive='Y' AND C_Project.C_Project_ID IN "
				+ "(SELECT pr.C_Project_ID FROM C_ProjectResource pr WHERE pr.IsActive='Y')");
		projectField = new WTableDirEditor("C_Project_ID", false, false, true, lookupProject);
		projectField.addValueChangeListener(this);
		
		//  BPartner
		columnID = MColumn.get(Env.getCtx(), "C_Project", "C_BPartner_ID").get_ID();
		MLookup lookupBP = MLookupFactory.get (Env.getCtx(), form.getWindowNo(), 0, columnID, DisplayType.Search);
		bpartnerField = new WSearchEditor("C_BPartner_ID", false, false, true, lookupBP);
		bpartnerField.addValueChangeListener(this);
		
		// Activity
		List<MActivity> activityList = new Query(Env.getCtx(), MActivity.Table_Name, "", null)
				.setClient_ID()
				.setOnlyActiveRecords(true)
				.list();
		for (MActivity a : activityList) {
			activityField.appendItem(a.getValue(), a.get_ID());
		}
	}   //  dynInit
	
	private Vector<String> getTaskColumnNames() {	
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "C_Project_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_ProjectPhase_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_ProjectTask_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "Description"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "Start Timer"));
		return columnNames;
	}
	
	private void setTaskColumnClass(IMiniTable table) {
		int i = 0;
		table.setColumnClass(i++, String.class, true);	// Project
		table.setColumnClass(i++, String.class, true);	// Phase
		table.setColumnClass(i++, String.class, true);	// Task
		table.setColumnClass(i++, String.class, true);	// Description
		table.setColumnClass(i++, String.class, true);	// BP
		table.setColumnClass(i++, Button.class, false);	// Start Timer

		//  Table UI
		table.autoSize();
	}
	
	private void clearTasks() {
		for (Button b : startTimerButtonList) {
			b.removeEventListener(Events.ON_CLICK, b.getEventListeners(Events.ON_CLICK).iterator().next());
		}
		startTimerButtonList.clear();
		taskTable.clear();
	}
	
	private void refreshTasks() {
		clearTasks();
		
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT pj.Value ProjectValue, ph.Name PhaseName, pt.C_ProjectTask_ID, pt.Name TaskName, pt.Description TaskDescription, bp.Name BPName, r.S_Resource_ID ");
		sql.append("FROM C_Project pj ");
		sql.append("INNER JOIN C_ProjectPhase ph ON (ph.C_Project_ID = pj.C_Project_ID) ");
		sql.append("INNER JOIN C_ProjectTask pt ON (pt.C_ProjectPhase_ID = ph.C_ProjectPhase_ID) ");
		sql.append("INNER JOIN C_ProjectResource pr ON (pr.C_Project_ID = pj.C_Project_ID) ");
		sql.append("INNER JOIN S_Resource r ON (r.S_Resource_ID = pr.S_Resource_ID) ");
		sql.append("LEFT JOIN C_BPartner bp ON (bp.C_BPartner_ID = pj.C_BPartner_ID) ");
		sql.append("WHERE pj.Processed='N' AND pj.IsActive='Y' AND ph.IsActive='Y' AND pt.IsActive='Y' ");
		sql.append("AND r.AD_User_ID = ? ");
		params.add(userID);
		if (projectID > 0) {
			sql.append("AND pj.C_Project_ID = ? ");
			params.add(projectID);
		}
		if (bpartnerID > 0) {
			sql.append("AND bp.C_BPartner_ID = ? ");
			params.add(bpartnerID);
		}
		sql.append("ORDER BY pj.Value, ph.Name, pt.Name");
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			DB.setParameters(pstmt, params);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector<Object> line = new Vector<Object>();
				line.add(rs.getString("ProjectValue"));
				line.add(rs.getString("PhaseName"));
				line.add(rs.getString("TaskName"));
				line.add(rs.getString("TaskDescription"));
				line.add(rs.getString("BPName"));
				
				Button startButton = new Button();
				startButton.setAttribute("C_ProjectTask_ID", rs.getInt("C_ProjectTask_ID"));
				startButton.setAttribute("S_Resource_ID", rs.getInt("S_Resource_ID"));
				startButton.setIconSclass("z-icon-Play");
				startButton.addActionListener(new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						Button button = (Button) event.getTarget();
						int taskID = (Integer) button.getAttribute("C_ProjectTask_ID");
						int resourceID = (Integer) button.getAttribute("S_Resource_ID");
						onCreateTimer(taskID, resourceID);
					}
					
				});
				line.add(startButton);
				startTimerButtonList.add(startButton);
				
				data.add(line);
			}
		} catch (SQLException e) {
			throw new DBException(e, sql.toString());
		} finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		ListModelTable model = new ListModelTable(data);
		taskTable.setData(model, getTaskColumnNames());
		setTaskColumnClass(taskTable);
		taskInfo.setText(taskTable.getRowCount() + " result(s)");
	}
	
	private Vector<String> getTimerColumnNames() {	
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "C_Project_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_ProjectTask_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "Description"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_Activity_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "StartDate"));
		columnNames.add(Msg.translate(Env.getCtx(), "Notes"));
		columnNames.add(Msg.translate(Env.getCtx(), "Duration"));
		columnNames.add(Msg.translate(Env.getCtx(), "Start"));
		columnNames.add(Msg.translate(Env.getCtx(), "Pause"));
		columnNames.add(Msg.translate(Env.getCtx(), "Stop"));
		columnNames.add(Msg.translate(Env.getCtx(), "Delete"));
		return columnNames;
	}
	
	private void setTimerColumnClass(IMiniTable table) {
		int i = 0;
		table.setColumnClass(i++, String.class, true);	// Project
		table.setColumnClass(i++, String.class, true);	// Task
		table.setColumnClass(i++, String.class, true);	// Description
		table.setColumnClass(i++, String.class, true);	// BP
		table.setColumnClass(i++, Combobox.class, false);	// Activity
		table.setColumnClass(i++, String.class, true);	// StartDate
		table.setColumnClass(i++, String.class, false);	// Notes
		table.setColumnClass(i++, Label.class, true);	// Duration
		table.setColumnClass(i++, Button.class, false);	// Start
		table.setColumnClass(i++, Button.class, false);	// Pause
		table.setColumnClass(i++, Button.class, false);	// Stop
		table.setColumnClass(i++, Button.class, false);	// Delete

		//  Table UI
		table.autoSize();
	}
	
	private void clearTimers() {
		for (Integer key : startButtonMap.keySet()) {
			Button b = startButtonMap.get(key);
			b.removeEventListener(Events.ON_CLICK, b.getEventListeners(Events.ON_CLICK).iterator().next());
		}
		startButtonMap.clear();
		
		for (Integer key : pauseButtonMap.keySet()) {
			Button b = pauseButtonMap.get(key);
			b.removeEventListener(Events.ON_CLICK, b.getEventListeners(Events.ON_CLICK).iterator().next());
		}
		pauseButtonMap.clear();
		
		for (Integer key : stopButtonMap.keySet()) {
			Button b = stopButtonMap.get(key);
			b.removeEventListener(Events.ON_CLICK, b.getEventListeners(Events.ON_CLICK).iterator().next());
		}
		stopButtonMap.clear();
		
		for (Integer key : deleteButtonMap.keySet()) {
			Button b = deleteButtonMap.get(key);
			b.removeEventListener(Events.ON_CLICK, b.getEventListeners(Events.ON_CLICK).iterator().next());
		}
		deleteButtonMap.clear();
		durationLabelMap.clear();
		timerStateMap.clear();
		timerTable.clear();
	}
	
	private void refreshTimers() {
		clearTimers();
		
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT te.S_TimesheetEntry_ID, pj.Value ProjectValue, pt.Name TaskName, pt.Description TaskDescription, bp.Name BPName, ");
		sql.append("te.C_Activity_ID, a.Name ActivityName, te.StartDate, COALESCE(te.Comments,'') Notes, te.TimerData ");
		sql.append("FROM S_TimesheetEntry te ");
		sql.append("INNER JOIN C_Project pj ON (pj.C_Project_ID = te.C_Project_ID) ");
		sql.append("INNER JOIN C_ProjectTask pt ON (pt.C_ProjectTask_ID = te.C_ProjectTask_ID) ");
		sql.append("INNER JOIN C_BPartner bp ON (bp.C_BPartner_ID = pj.C_BPartner_ID) ");
		sql.append("LEFT JOIN C_Activity a ON (a.C_Activity_ID = te.C_Activity_ID) ");
		sql.append("WHERE te.IsActive='Y' AND te.Processed='N' AND te.EndDate IS NULL ");
		sql.append("AND te.AD_User_ID = ? ");
		sql.append("ORDER BY te.StartDate DESC");
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			DB.setParameters(pstmt, new Object[]{userID});
			rs = pstmt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("S_TimesheetEntry_ID");
				Vector<Object> line = new Vector<Object>();
				line.add(rs.getString("ProjectValue"));
				line.add(rs.getString("TaskName"));
				line.add(rs.getString("TaskDescription"));
				line.add(rs.getString("BPName"));
				
				Combobox activity = (Combobox) activityField.clone();
				activity.setAttribute("S_TimesheetEntry_ID", id);
				if (rs.getInt("C_Activity_ID") > 0)
					activity.setValue(rs.getInt("C_Activity_ID"));
				line.add(activity);
				
				line.add(rs.getString("StartDate"));
				line.add(rs.getString("Notes"));
				
				Label durationLabel = new Label();
				durationLabel.setSclass("duration-label");
				JSONObject jo = null;
				if (Util.isEmpty(rs.getString("TimerData"),true)) {
					durationLabel.setValue("00:00:00");
					line.add(durationLabel);
				} else {
					jo = new JSONObject(rs.getString("TimerData"));
					durationLabel.setValue(getFormattedTime(jo.getLong("TotalDuration")));
					line.add(durationLabel);
				}
				durationLabelMap.put(id, durationLabel);
				
				Button startButton = new Button();
				startButton.setAttribute("S_TimesheetEntry_ID", id);
				startButton.setIconSclass("z-icon-Play");
				startButton.addActionListener(new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						onStart(id);
					}
					
				});
				line.add(startButton);
				startButtonMap.put(id, startButton);
				
				Button pauseButton = new Button();
				pauseButton.setAttribute("S_TimesheetEntry_ID", id);
				pauseButton.setIconSclass("z-icon-Pause");
				pauseButton.addActionListener(new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						onPause(id);
					}
					
				});
				line.add(pauseButton);
				pauseButtonMap.put(id, pauseButton);
				
				Button stopButton = new Button();
				stopButton.setAttribute("S_TimesheetEntry_ID", id);
				stopButton.setIconSclass("z-icon-Stop");
				stopButton.addActionListener(new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						onStop(id, event.getTarget());
					}
					
				});
				line.add(stopButton);
				stopButtonMap.put(id, stopButton);
				
				Button deleteButton = new Button();
				deleteButton.setAttribute("S_TimesheetEntry_ID", id);
				deleteButton.setIconSclass("z-icon-XMark");
				deleteButton.addActionListener(new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						onDelete(id);
					}
					
				});
				line.add(deleteButton);
				deleteButtonMap.put(id, deleteButton);
				
				// set buttons state based on status
				if (jo != null) {
					String status = jo.getString("Status");
					if (status.equals("STARTED")) {
						startButton.setEnabled(false);
						durationLabel.setSclass("duration-label active-timer");
					} else if (status.equals("PAUSED")) {
						pauseButton.setEnabled(false);
					} else {
						startButton.setEnabled(false);
						pauseButton.setEnabled(false);
						stopButton.setEnabled(false);
						deleteButton.setEnabled(false);
					}
					
					TimerState state = new TimerState();
					state.status = status;
					state.totalDuration = jo.getLong("TotalDuration");
					state.latestStartTimeMillis = jo.getLong("LatestStartTimeMillis");
					timerStateMap.put(id, state);
				} else {
					pauseButton.setEnabled(false);
					stopButton.setEnabled(false);
				}
				
				data.add(line);
			}
		} catch (SQLException e) {
			throw new DBException(e, sql.toString());
		} finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		ListModelTable model = new ListModelTable(data);
		timerTable.setData(model, getTimerColumnNames());
		setTimerColumnClass(timerTable);
		timerInfo.setText(timerTable.getRowCount() + " result(s)");
		timerTable.repaint();
	}
	
	private void onCreateTimer(int taskID, int resourceID) {
		MProjectTask task = new MProjectTask(Env.getCtx(), taskID, null);
		
		MTimesheetEntry te = new MTimesheetEntry(Env.getCtx(), 0, null);
		te.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
		te.setAD_User_ID(userID);
		te.setC_ProjectTask_ID(task.get_ID());
		te.setC_ProjectPhase_ID(task.getC_ProjectPhase_ID());
		te.setC_Project_ID(task.getC_ProjectPhase().getC_Project_ID());
		te.setS_Resource_ID(resourceID);
		te.setM_Product_ID(MProduct.forS_Resource_ID(Env.getCtx(), resourceID, null).get_ID());
		te.setC_BPartner_ID(task.getC_ProjectPhase().getC_Project().getC_BPartner_ID());
		te.setStartDate(new Timestamp(System.currentTimeMillis()));
		te.setDateAcct(TimeUtil.addDays(te.getStartDate(), 0));
		te.saveEx();
		
		refreshTimers();
	}
	
	private void onStart(int id) {
		Button startButton = startButtonMap.get(id);
		Button pauseButton = pauseButtonMap.get(id);
		Button stopButton = stopButtonMap.get(id);
		startButton.setEnabled(false);
		pauseButton.setEnabled(true);
		stopButton.setEnabled(true);
		
		MTimesheetEntry te = new MTimesheetEntry(Env.getCtx(), id, null);
		JSONObject jo = te.getTimerData();
		if ( jo == null )
		{
			jo = new JSONObject();
			jo.put("TotalDuration", 0);
		}
		
		JSONArray ja = null;
		if (jo.isNull("Durations"))
			ja = new JSONArray();
		else
			ja = jo.getJSONArray("Durations");
		
		JSONObject duration = new JSONObject();
		duration.put("StartTimeMillis", System.currentTimeMillis());
		duration.put("StartTime", new Timestamp(duration.getLong("StartTimeMillis")));
		ja.put(duration);
		jo.put("Durations", ja);
		jo.put("LatestStartTimeMillis", duration.getLong("StartTimeMillis"));
		jo.put("LatestStartTime", duration.get("StartTime"));
		jo.put("Status", "STARTED");
		
		te.setTimerData(jo);
		te.saveEx();
		
		TimerState state;
		if (timerStateMap.containsKey(id))
			state = timerStateMap.get(id);
		else
			state = new TimerState();
		state.status = "STARTED";
		state.totalDuration = jo.getLong("TotalDuration");
		state.latestStartTimeMillis = jo.getLong("LatestStartTimeMillis");
		timerStateMap.put(id, state);
		
		Label durationLabel = durationLabelMap.get(id);
		durationLabel.setSclass("duration-label active-timer");
	}
	
	private void onPause(int id) {
		Button startButton = startButtonMap.get(id);
		Button pauseButton = pauseButtonMap.get(id);
		Button stopButton = stopButtonMap.get(id);
		startButton.setEnabled(true);
		pauseButton.setEnabled(false);
		stopButton.setEnabled(true);
		
		MTimesheetEntry te = new MTimesheetEntry(Env.getCtx(), id, null);
		JSONObject jo = te.getTimerData();
		JSONArray ja = jo.getJSONArray("Durations");
		long totalDuration = 0;
		for (int i = 0; i < ja.length(); i++) {
			 JSONObject duration = ja.getJSONObject(i);
			 if (duration.isNull("EndTime")) {
				 duration.put("EndTimeMillis", System.currentTimeMillis());
				 duration.put("EndTime", new Timestamp(duration.getLong("EndTimeMillis")));
				 long diff = duration.getLong("EndTimeMillis") - duration.getLong("StartTimeMillis");
				 duration.put("Diff", diff);
				 totalDuration += diff;
			 } else {
				 totalDuration += duration.getLong("Diff");
			 }
		}
		jo.put("TotalDuration", totalDuration);
		jo.put("Status", "PAUSED");
		
		te.setTimerData(jo);
		te.saveEx();
		
		TimerState state;
		if (timerStateMap.containsKey(id))
			state = timerStateMap.get(id);
		else
			state = new TimerState();
		state.status = "PAUSED";
		state.totalDuration = totalDuration;
		timerStateMap.put(id, state);
		
		Label durationLabel = durationLabelMap.get(id);
		durationLabel.setSclass("duration-label");
	}

	private void onStop(int id, Component source) {
		int row = timerRenderer.getRowPosition(source);
		Combobox activityField = (Combobox) timerTable.getValueAt(row, 4); // activity
		String notes = timerTable.getValueAt(row, 6).toString(); // notes
		
		int activityID = 0;
		if (!Util.isEmpty(activityField.getValue(), true)) {
			MActivity activity = new Query(Env.getCtx(), MActivity.Table_Name, "Value = ?", null)
					.setClient_ID()
					.setOnlyActiveRecords(true)
					.setParameters(activityField.getValue())
					.first();
			if (activity != null)
				activityID = activity.get_ID();
		}
		
		String errorMessage = "";
		if (activityID <= 0)
			errorMessage = "@FillMandatory@ @C_Activity_ID@";
		if (notes.isEmpty())
			errorMessage = "@FillMandatory@ @Note@";
		if (errorMessage.isEmpty()) {
			MTimesheetEntry te = new MTimesheetEntry(Env.getCtx(), id, null);
			JSONObject jo = te.getTimerData();
			JSONArray ja = jo.getJSONArray("Durations");
			long totalDuration = 0;
			for (int i = 0; i < ja.length(); i++) {
				 JSONObject duration = ja.getJSONObject(i);
				 if (duration.isNull("EndTime")) {
					 duration.put("EndTimeMillis", System.currentTimeMillis());
					 duration.put("EndTime", new Timestamp(duration.getLong("EndTimeMillis")));
					 long diff = duration.getLong("EndTimeMillis") - duration.getLong("StartTimeMillis");
					 duration.put("Diff", diff);
					 totalDuration += diff;
				 } else {
					 totalDuration += duration.getLong("Diff");
				 }
			}
			jo.put("TotalDuration", totalDuration);
			jo.put("Status", "ENDED");
			
			Duration duration = Duration.ofMillis(totalDuration);
			long seconds = duration.getSeconds();
			BigDecimal qtyHour = BigDecimal.valueOf(seconds / 3600f);
			MUOM uomHour = MUOM.get(Env.getCtx(), "Hour", null);
			qtyHour = qtyHour.setScale(uomHour.getStdPrecision(), RoundingMode.HALF_UP);
			te.setQtyEntered(qtyHour);
			te.setTimerData(jo);
			te.setEndDate(new Timestamp(System.currentTimeMillis()));
			te.setC_Activity_ID(activityID);
			te.setComments(notes);
			te.saveEx();
			
			refreshTimers();
		} else {
			FDialog.warn(m_WindowNo, Msg.parseTranslation(Env.getCtx(), errorMessage));
		}
	}

	private void onDelete(int id) {
		FDialog.ask(m_WindowNo, this, "DeleteRecord?", new Callback<Boolean>() {

			@Override
			public void onCallback(Boolean result) 
			{
				if (result)
				{
					MTimesheetEntry te = new MTimesheetEntry(Env.getCtx(), id, null);
					te.deleteEx(true);
					refreshTimers();
				}					
			}
		});
	}
	
	private String getFormattedTime(long millis) {
		Duration duration = Duration.ofMillis(millis);
		long seconds = duration.getSeconds();
		long HH = seconds / 3600;
		long MM = (seconds % 3600) / 60;
		long SS = seconds % 60;
		String timeInHHMMSS = String.format("%02d:%02d:%02d", HH, MM, SS);
		return timeInHHMMSS;
	}
	
	@Override
	public void valueChange(ValueChangeEvent e) {
		String name = e.getPropertyName();
		Object value = e.getNewValue();
		log.config(name + "=" + value);
		if (value == null)
			value = Integer.valueOf(0);

		if (name.equals("AD_User_ID"))
			userID = (Integer) value;
		else if (name.equals("C_Project_ID"))
			projectID = (Integer) value;
		else if (name.equals("C_BPartner_ID"))
			bpartnerID = (Integer) value;
		
		refreshTasks();
	}
	
	/**
	 * Called by org.adempiere.webui.panel.ADForm.openForm(int)
	 * @return
	 */
	public ADForm getForm() {
		return form;
	}
	
	class TimerState {
		String status;
		Long totalDuration;
		Long latestStartTimeMillis;
		
		public String toString() {
			return "TimerState[Status=" + status + ", TotalDuration=" + totalDuration + ", LatestStartTimeMillis=" + latestStartTimeMillis + "]";
		}
	}
}
