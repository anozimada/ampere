/******************************************************************************
 * Product: Posterita Ajax UI 												  *
 * Copyright (C) 2007 Posterita Ltd.  All Rights Reserved.                    *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Posterita Ltd., 3, Draper Avenue, Quatre Bornes, Mauritius                 *
 * or via info@posterita.org or http://www.posterita.org/                     *
 *****************************************************************************/

package org.adempiere.webui.panel;

import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.AboutWindow;
import org.compiere.util.Ini;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;

/**
 *
 * @author  <a href="mailto:agramdass@gmail.com">Ashley G Ramdass</a>
 * @author  <a href="mailto:hengsin@gmail.com">Low Heng Sin</a>
 * @date    Mar 2, 2007
 * @date    July 7, 2007
 * @version $Revision: 0.20 $
 */

public class HeaderPanel extends Panel implements EventListener<Event>
{
	private static final long serialVersionUID = -2351317624519209484L;

	private Image image = new Image();

	private boolean hasHeadline = false;
	
    public HeaderPanel()
    {
        super();
        init();
    }

    private void init()
    {
    	LayoutUtils.addSclass("desktop-header", this);
    	Div div = new Div();

    	MenuBarPanel menuBarPanel = new MenuBarPanel();
    	SearchMenuBarPanel searchMenuBarPanel = new SearchMenuBarPanel();
    	UserPanel userPanel = new UserPanel();

    	image.setSrc(ThemeUtils.resolveImageURL(ThemeUtils.HEADER_LOGO_IMAGE));
    	image.addEventListener(Events.ON_CLICK, this);
    	image.setSclass("desktop-header-logo");

    	Hlayout layout = new Hlayout();
    	layout.setParent(this);

        image.setParent(div);
        div.setParent(layout);
    	LayoutUtils.addSclass("desktop-header-left", div);
        
    	div = new Div();
    	menuBarPanel.setParent(div);
        div.setParent(layout);
    	LayoutUtils.addSclass("desktop-header-menu", div);
    	ZKUpdateUtil.setHflex(div, "2");
    	
        hasHeadline = Ini.isHeadlineSet();
    	if (hasHeadline) {
    		div = new Div();
    		Label text = new Label();
    		text.setValue(Ini.getHeadLineText());
    		text.setSclass("desktop-header-headline");
    		text.setParent(div);
            div.setParent(layout);
    		LayoutUtils.addSclass("desktop-header-center", div);
    	}

		div = new Div();
		Hlayout rightLayout = new Hlayout();
		rightLayout.setParent(div);
		searchMenuBarPanel.setParent(rightLayout);
    	userPanel.setParent(rightLayout);
        ZKUpdateUtil.setHflex(div, "1");
        div.setParent(layout);
    	LayoutUtils.addSclass("desktop-header-right", div);
    }

	public void onEvent(Event event) throws Exception {
		if (Events.ON_CLICK.equals(event.getName())) {
			if(event.getTarget() == image)
			{
				AboutWindow w = new AboutWindow();
				w.setPage(this.getPage());
				w.doModal();
			}
		}

	}
}
