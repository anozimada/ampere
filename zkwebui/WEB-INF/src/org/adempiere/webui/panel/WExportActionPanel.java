package org.adempiere.webui.panel;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.impexp.GridTabCSVExporter;
import org.adempiere.impexp.GridTabExcelExporter;
import org.adempiere.impexp.GridTabXmlExporter;
import org.adempiere.impexp.IGridTabExporter;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.IADTab;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.compiere.model.GridTab;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Label;
import org.zkoss.zul.Space;
import org.zkoss.zul.Vbox;

public class WExportActionPanel extends Window implements EventListener<Event>
{

	/**
	 * 
	 */
	private static final long		serialVersionUID			= -8204115573769710694L;

	private AbstractADWindowPanel	panel;
	private ConfirmPanel			confirmPanel				= new ConfirmPanel(true);
	private Borderlayout			selPanel					= new Borderlayout();
	private Listbox					cboType						= new Listbox();
	private Checkbox				chkCurrentRow				= new Checkbox();
	private Row						selectionTabRow				= null;
	private IGridTabExporter		exporter;
	private List<GridTab>			childs;
	private List<Checkbox>			chkSelectionTabForExport	= null;
	private int						indxDetailSelected			= 0;

	/**
	 * @param panel
	 */
	public WExportActionPanel(AbstractADWindowPanel panel)
	{
		this.panel = panel;
		init();
	}

	private void init()
	{
		ZKUpdateUtil.setWidth(selPanel, "450px");
		ZKUpdateUtil.setHeight(selPanel, "285px");

		Grid grid = GridFactory.newGridLayout();

		selPanel.appendCenter(grid);
		ZKUpdateUtil.setHeight(grid, "230px");

		Rows rows = new Rows();
		grid.appendChild(rows);

		cboType.setMold("select");

		cboType.getItems().clear();

		cboType.appendItem(Msg.getMsg(Env.getCtx(), "FileXLS"), Msg.getMsg(Env.getCtx(), "FileXLS"));
		cboType.appendItem(Msg.getMsg(Env.getCtx(), "FileCSV"), Msg.getMsg(Env.getCtx(), "FileCSV"));
		cboType.appendItem(Msg.getMsg(Env.getCtx(), "FileXML"), Msg.getMsg(Env.getCtx(), "FileXML"));
		cboType.appendItem(Msg.getMsg(Env.getCtx(), "FileXLSX"), Msg.getMsg(Env.getCtx(), "FileXLSX"));

		cboType.setSelectedIndex(0);
		cboType.addActionListener(this);

		Row row = new Row();
		rows.appendChild(row);
		row.appendCellChild(new Label(Msg.getMsg(Env.getCtx(), "FilesOfType")));
		row.appendCellChild(cboType, 2);
		ZKUpdateUtil.setWidth(cboType, "99%");

		row = new Row();
		rows.appendChild(row);
		row.appendCellChild(new Space());

		chkCurrentRow.setLabel(Msg.getMsg(Env.getCtx(), "ExportCurrentRowOnly"));
		chkCurrentRow.setSelected(true);
		row.appendCellChild(chkCurrentRow, 2);

		selectionTabRow = new Row();
		rows.appendChild(selectionTabRow);

		ZKUpdateUtil.setHeight(confirmPanel, "40px");
		LayoutUtils.addSclass("dialog-footer", confirmPanel);
		selPanel.appendSouth(confirmPanel);

		confirmPanel.addActionListener(this);
		this.appendChild(selPanel);

		displayExportTabSelection();

		this.addEventListener("onExporterException", this);
	}

	private void displayExportTabSelection()
	{
		initTabInfo();
		ListItem select = cboType.getSelectedItem();
		if (select.getValue().equals(Msg.getMsg(Env.getCtx(), "FileXLS"))
				|| select.getValue().equals(Msg.getMsg(Env.getCtx(), "FileXLSX")))
			exporter = new GridTabExcelExporter(Env.getCtx(), panel.getActiveGridTab(),
					select.getValue().equals(Msg.getMsg(Env.getCtx(), "FileXLSX")));
		else if (select.getValue().equals(Msg.getMsg(Env.getCtx(), "FileCSV")))
			exporter = new GridTabCSVExporter();
		else if (select.getValue().equals(Msg.getMsg(Env.getCtx(), "FileXML")))
			exporter = new GridTabXmlExporter();
		if (exporter == null)
		{
			Events.echoEvent("onExporterException", this, null);
		}

		// clear list checkbox selection to recreate with new reporter
		selectionTabRow.getChildren().clear();
		Vbox vlayout = new Vbox();
		selectionTabRow.appendCellChild(new Space());
		selectionTabRow.appendCellChild(vlayout, 2);
		ZKUpdateUtil.setWidth(vlayout, "99%");
		vlayout.appendChild(new Label(Msg.getMsg(Env.getCtx(), "SelectTabToExport")));
		chkSelectionTabForExport = new ArrayList<Checkbox>();
		boolean isHasSelectionTab = false;
		boolean isSelectTabDefault = false;
		// for to make each export tab with one checkbox
		for (GridTab child : childs)
		{
			Checkbox chkSelectionTab = new Checkbox();
			chkSelectionTab.setLabel(child.getName());
			// just allow selection tab can export
			if (!exporter.isExportableTab(child))
			{
				continue;
			}
			if (child.getTabNo() == indxDetailSelected || isSelectTabDefault)
			{
				chkSelectionTab.setSelected(true);
			}
			chkSelectionTab.setAttribute("tabBinding", child);
			vlayout.appendChild(chkSelectionTab);
			chkSelectionTabForExport.add(chkSelectionTab);
			isHasSelectionTab = true;
		}

		// in case no child tab can export. clear selection area
		if (isHasSelectionTab == false)
		{
			selectionTabRow.getChildren().clear();
		}
	}

	private void initTabInfo()
	{
		IADTab adTab = panel.getADTab();
		int selected = adTab.getSelectedIndex();
		int tabLevel = panel.getActiveGridTab().getTabLevel();
		Set<String> tables = new HashSet<String>();
		childs = new ArrayList<GridTab>();
		List<GridTab> includedList = panel.getActiveGridTab().getIncludedTabs();
		for (GridTab included : includedList)
		{
			String tableName = included.getTableName();
			if (tables.contains(tableName))
				continue;
			tables.add(tableName);
			childs.add(included);
		}
		for (int i = selected + 1; i < adTab.getTabCount(); i++)
		{
			IADTabpanel adTabPanel = adTab.getADTabpanel(i);
			if (adTabPanel.getGridTab().isSortTab())
				continue;
			if (adTabPanel.getGridTab().getTabLevel() <= tabLevel)
				break;
			String tableName = adTabPanel.getGridTab().getTableName();
			if (tables.contains(tableName))
				continue;
			tables.add(tableName);
			childs.add(adTabPanel.getGridTab());
		}

		indxDetailSelected = 0;
		if (adTab.getSelectedTabpanel() != null)
			indxDetailSelected = adTab.getSelectedTabpanel().getGridTab().getTabNo();

	}

	@Override
	public void onEvent(Event event) throws Exception
	{
		if (event.getTarget().getId().equals(ConfirmPanel.A_CANCEL))
			this.onClose();
		else if (event.getTarget().getId().equals(ConfirmPanel.A_OK))
		{
			exportFile();
		}
		else if (event.getTarget().equals(cboType) && event.getName().equals(Events.ON_SELECT))
		{
			displayExportTabSelection();
		}
		else if (event.getName().equals("onExporterException"))
		{
			FDialog.error(0, this, "FileInvalidExtension");
			this.onClose();
		}
	}

	private void exportFile()
	{
		try
		{
			boolean currentRowOnly = chkCurrentRow.isSelected();
			File file = File.createTempFile("Export", "." + cboType.getSelectedItem().getValue().toString());
			childs.clear();
			for (Checkbox chkSeletionTab : chkSelectionTabForExport)
			{
				if (chkSeletionTab.isChecked())
				{
					childs.add((GridTab) chkSeletionTab.getAttribute("tabBinding"));
				}
			}

			exporter.export(panel.getActiveGridTab(), childs, currentRowOnly, file, indxDetailSelected);

			this.onClose();
			AMedia media = null;
			media = new AMedia(exporter.getSuggestedFileName(panel.getActiveGridTab()), null, exporter.getContentType(),
					file, true);
			Filedownload.save(media);
		}
		catch (Exception e)
		{
			throw new AdempiereException(e);
		}
		finally
		{
			if (this != null)
				this.onClose();
		}
	}

}
