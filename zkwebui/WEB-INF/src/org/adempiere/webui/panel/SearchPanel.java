/******************************************************************************
 * Product: Posterita Ajax UI 												  *
 * Copyright (C) 2007 Posterita Ltd.  All Rights Reserved.                    *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Posterita Ltd., 3, Draper Avenue, Quatre Bornes, Mauritius                 *
 * or via info@posterita.org or http://www.posterita.org/                     *
 *****************************************************************************/

package org.adempiere.webui.panel;

import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.AutoComplete;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.DocumentSearch;
import org.adempiere.webui.util.TreeItemAction;
import org.adempiere.webui.util.TreeNodeAction;
import org.adempiere.webui.util.TreeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.MTreeNode;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.zkforge.keylistener.Keylistener;
import org.zkoss.zk.au.out.AuFocus;
import org.zkoss.zk.au.out.AuScript;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.DefaultTreeNode;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.event.TreeDataEvent;
import org.zkoss.zul.event.TreeDataListener;
import org.zkoss.zul.impl.LabelElement;


/**
 *
 * @author  <a href="mailto:agramdass@gmail.com">Ashley G Ramdass</a>
 * @date    Mar 3, 2007
 * @version $Revision: 0.10 $
 */
public class SearchPanel extends Panel implements EventListener<Event>, TreeDataListener, IdSpace
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5200523300068844754L;
	private static final String ON_COMBO_SELECT_ECHO_EVENT = "onComboSelectEcho";
	private static final String ON_POST_SELECT_TREEITEM_EVENT = "onPostSelectTreeitem";
	protected static final String ON_POST_FIRE_TREE_EVENT = "onPostFireTreeEvent";
	
	protected TreeMap<String, Object> treeNodeItemMap = new TreeMap<String, Object>();
	private String[] treeValues;
	private String[] treeDescription;
    
    protected AutoComplete cmbSearch;

	protected Tree tree;

	protected String eventToFire;
	@SuppressWarnings("unused")
	private int m_windowno = 0;
	@SuppressWarnings("unused")
	private int m_tabno = 0;

	private Treeitem selectedItem;
	private String CNTRL_KEYS = "@#slash";

	private static final String PREFIX_DOCUMENT_SEARCH = "/";

	/**
     * @param tree
     */
    public SearchPanel(Tree tree)
    {
    	this(tree, Events.ON_CLICK);
    }

    /**
     * @param tree
     * @param event
     */
    public SearchPanel(Tree tree, String event)
    {
        super();
        this.tree = tree;
        this.eventToFire = event;
        init();
    }

    /**
     * @param tree
     * @param event
     */
    public SearchPanel(Tree tree, String event, int windowno, int tabno)
    {
        super();
        this.tree = tree;
        this.eventToFire = event;
        m_windowno = windowno;
        m_tabno = tabno;
        init();
    }

    private static final String onComboSelectEchoScript = "var combo=zk('@combo').$();"
    		+ "var panel=zk('@this').$();"
    		+ "var comboitem=zk('@item').$();"
    		+ "var popupheight=combo.getPopupNode_().offsetHeight;"
    		+ "var evt = new zk.Event(panel, 'onComboSelectEcho', [comboitem.uuid, popupheight], {toServer: true});"
    		+ "zAu.send(evt);";
	
    protected void init()
    {
        cmbSearch = new AutoComplete();
        cmbSearch.setAutodrop(true);
        cmbSearch.setAutocomplete(true);
       	cmbSearch.setId("treeSearchCombo");
       	cmbSearch.setSclass("tree-search-combo");
       	cmbSearch.setPlaceholder(Msg.getMsg(Env.getCtx(),"Search Menu"));
        
       	//cmbSearch.setAttribute(AdempiereIdGenerator.ZK_COMPONENT_PREFIX_ATTRIBUTE, "unqCmbSearch" + "_" + m_windowno + "_" + m_tabno);
      
        cmbSearch.addEventListener(Events.ON_OK, this);
        cmbSearch.addEventListener(Events.ON_SELECT, new EventListener<SelectEvent<Comboitem,Object>>() {
			@Override
			public void onEvent(SelectEvent<Comboitem, Object> event)
					throws Exception {
				Set<Comboitem> set = event.getSelectedItems();
        		if (set.size() > 0) {
        			Comboitem item = set.iterator().next();
        			String script = onComboSelectEchoScript.replaceFirst("@combo", cmbSearch.getUuid())
        				.replaceFirst("@this", SearchPanel.this.getUuid())
        				.replaceFirst("@item", item.getUuid());
        			AuScript response = new AuScript(script);
        			Clients.response(response);
        		}				
			}
		});
        
        addEventListener(ON_COMBO_SELECT_ECHO_EVENT, this);
        addEventListener(ON_POST_SELECT_TREEITEM_EVENT, this);
        
        ZKUpdateUtil.setHflex(cmbSearch, "true");

        this.appendChild(cmbSearch);

        addEventListener(ON_POST_FIRE_TREE_EVENT, this);

        Keylistener keyListener = SessionManager.getSessionApplication().getKeylistener();
        keyListener.setCtrlKeys(keyListener.getCtrlKeys() + CNTRL_KEYS );
        keyListener.addEventListener(Events.ON_CTRL_KEY, this);
    }

    protected void addTreeItem(Treeitem treeItem)
    {
        String key = treeItem.getLabel();
        treeNodeItemMap.put(key, treeItem);
    }

    protected void addTreeItem(DefaultTreeNode<?> node) {
    	Object data = node.getData();
    	if (data instanceof MTreeNode) {
    		MTreeNode mNode = (MTreeNode) data;
    		treeNodeItemMap.put(mNode.getName(), node);
    	}
	}

    /**
     * populate the searchable list
     */
    public void initialise()
    {
    	refreshSearchList();

        if (tree.getModel() != null)
        {
        	tree.getModel().addTreeDataListener(this);
        }
    }

	public void refreshSearchList() {
		treeNodeItemMap.clear();
		if (tree.getModel() == null) {
	    	TreeUtils.traverse(tree, new TreeItemAction() {
				public void run(Treeitem treeItem) {
					if (treeItem.isVisible() && treeItem.getTreechildren() == null)
						addTreeItem(treeItem);
				}
	    	});
		} else {
			TreeUtils.traverse(tree.getModel(), new TreeNodeAction() {
				public void run(DefaultTreeNode<?> treeNode) {
					addTreeItem(treeNode);
				}
	    	});
		}

    	treeValues = new String[treeNodeItemMap.size()];
    	treeDescription = new String[treeNodeItemMap.size()];

    	int i = -1;

        for (Object value : treeNodeItemMap.values())
        {
        	i++;
        	if (value instanceof Treeitem)
        	{
        		Treeitem treeItem = (Treeitem) value;
        		treeValues[i] = treeItem.getLabel();
        		treeDescription[i] = treeItem.getTooltiptext();
        	}
        	else if (value instanceof DefaultTreeNode)
        	{
        		DefaultTreeNode<?> sNode = (DefaultTreeNode<?>) value;
        		MTreeNode mNode = (MTreeNode) sNode.getData();
        		treeValues[i] = mNode.getName();
        		treeDescription[i] = mNode.getDescription();
        	}
        }

        cmbSearch.setDescription(treeDescription);
        cmbSearch.setDict(treeValues);
	}

	protected String getLabel(Treeitem treeItem) {
		String label = treeItem.getLabel();
		if (label == null || label.trim().length() == 0) 
		{
        	if (treeItem.getTreerow().getFirstChild().getFirstChild() != null &&
        		treeItem.getTreerow().getFirstChild().getFirstChild() instanceof LabelElement)
        	{
        		LabelElement element = (LabelElement) treeItem.getTreerow().getFirstChild().getFirstChild();
        		label = element.getLabel();
        	}
        }
        return label;
	}
	/**
     * @param event
     * @see EventListener#onEvent(Event)
     */
    public void onEvent(Event event)
    {
        if (cmbSearch.equals(event.getTarget()))
        {
        	if (!cmbSearch.isEnabled())
        		return;
        	
        	if (event.getName().equals(Events.ON_OK))
        	{
	        	selectedItem = null;
	            String value = cmbSearch.getValue();
	
	            if (value != null && value.trim().length() > 0
	            		&& value.substring(0, 1).equals(PREFIX_DOCUMENT_SEARCH))
	            {
	            	DocumentSearch search = new DocumentSearch();
	            	if (search.openDocumentsByDocumentNo(value.substring(1)))
	    				cmbSearch.setText(null);
	            	return;
	            }

	            for(Component comp : cmbSearch.getChildren())
        		{
	            	Comboitem item = (Comboitem) comp;
	            	if (item.getLabel().equals(value))
	            	{
	    				String type = cmbSearch.getContent(item);
	    				if (!Util.isEmpty(type))
	    					selectTreeitem(value+"."+type);
	    				else
	    					selectTreeitem(value);
	    				return;
					}
        		}
        	} 
        } 
        else if (event.getName().equals(ON_POST_FIRE_TREE_EVENT)) 
        {
        	cmbSearch.setEnabled(true);        	
        	cmbSearch.clearLastSel();
        }
        else if (event.getName().equals(ON_COMBO_SELECT_ECHO_EVENT))
        {
        	Object[] data = (Object[]) event.getData();
        	String uuid = (String) data[0];
        	int height = (Integer) data[1];
        	if (height == 0) 
        	{
        		List<Component> childs = cmbSearch.getChildren();
        		for(Component comp : childs)
        		{
        			if (comp.getUuid().equals(uuid))
        			{
        				Comboitem item = (Comboitem) comp;
        				String value = item.getLabel();
        				String type = cmbSearch.getContent(item);
        				if (!Util.isEmpty(type))
        					selectTreeitem(value+"."+type);
        				else
        					selectTreeitem(value);
        			}
        		}	        	
        	}
        	else
        	{
        		cmbSearch.clearLastSel();
        	}
        }
        else if (event.getName().equals(ON_POST_SELECT_TREEITEM_EVENT))
        {
        	onPostSelectTreeitem();
        }
        else if(event.getName().equals(Events.ON_CTRL_KEY))
		{
			KeyEvent keyEvent = (KeyEvent) event;
			
			if ( keyEvent.getKeyCode() == 191)
			{
				
				Component c = cmbSearch;
				Clients.response(new AuFocus(c ));
			}
		}
    }

	private void selectTreeitem(String value) {
		if (Executions.getCurrent().getAttribute(getUuid()+".selectTreeitem") != null)
			return;
		
		Object node = treeNodeItemMap.get(value);
		Treeitem treeItem = null;
		if (node == null) {
			return;
		} else if (node instanceof Treeitem) {
		    treeItem = (Treeitem) node;
		} else {
			DefaultTreeNode<?> sNode = (DefaultTreeNode<?>) node;
			int[] path = tree.getModel().getPath(sNode);
			treeItem = tree.renderItemByPath(path);
			tree.setSelectedItem(treeItem);
		}
		if (treeItem != null)
		{
			selectedItem = treeItem;
			Executions.getCurrent().setAttribute(getUuid()+".selectTreeitem", Boolean.TRUE);
		    cmbSearch.setEnabled(false);
		    
		    select(treeItem);		    
		    Clients.showBusy(Msg.getMsg(Env.getCtx(), "Loading"));
		    Events.echoEvent(ON_POST_SELECT_TREEITEM_EVENT, this, null);
		    Event event2=new Event(Events.ON_CLICK, ((Component)(treeItem.getTreerow().getChildren().get(0))));
            Events.postEvent(event2);
			cmbSearch.setText(null);
		}
	}
	
	
    /**
     * don't call this directly, use internally for post selection event
     */
    protected void onPostSelectTreeitem() {
    	Clients.clearBusy();
    	Event event = null;
    	if (eventToFire.equals(Events.ON_CLICK))
    	{
    		if (tree.getSelectedItem().getTreerow().getFirstChild().getFirstChild() instanceof A)
    		{
    			event = new Event(Events.ON_CLICK, tree.getSelectedItem().getTreerow().getFirstChild().getFirstChild());
    		}
    		else
    		{
    			event = new Event(Events.ON_CLICK, tree.getSelectedItem().getTreerow());
    		}
    	}
    	else
    		event = new Event(eventToFire, tree);
    	Events.postEvent(event);
    	Events.echoEvent(ON_POST_FIRE_TREE_EVENT, this, null);
    }

	public static void select(Treeitem selectedItem) {
		Treeitem parent = selectedItem.getParentItem();
		while (parent != null) {
			if (!parent.isOpen())
				parent.setOpen(true);

			parent = parent.getParentItem();
		}
		selectedItem.getTree().setSelectedItem(selectedItem);
	}

	/**
	 * @param event
	 * @see TreeDataListener#onChange(TreeDataEvent)
	 */
	public void onChange(TreeDataEvent event) {
		refreshSearchList();
	}
	
	public Treeitem getSelectedItem() {
		return selectedItem;
	}
}
