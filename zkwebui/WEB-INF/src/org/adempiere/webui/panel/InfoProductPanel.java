/******************************************************************************
 * Product: Posterita Ajax UI                                                 *
 * Copyright (C) 2007 Posterita Ltd.  All Rights Reserved.                    *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Posterita Ltd., 3, Draper Avenue, Quatre Bornes, Mauritius                 *
 * or via info@posterita.org or http://www.posterita.org/                     *
 *****************************************************************************/

package org.adempiere.webui.panel;

/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Listbox.YES_NO_OPTION;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Tab;
import org.adempiere.webui.component.Tabbox;
import org.adempiere.webui.component.Tabpanel;
import org.adempiere.webui.component.Tabpanels;
import org.adempiere.webui.component.Tabs;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.event.DialogEvents;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.apache.commons.lang.StringUtils;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MDocType;
import org.compiere.model.MProduct;
import org.compiere.model.MQuery;
import org.compiere.model.MRole;
import org.compiere.model.MTable;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.North;
import org.zkoss.zul.South;
import org.zkoss.zul.Space;

/**
 * Search Product and return selection
 * This class is based on org.compiere.apps.search.InfoPAttribute written by Jorg Janke
 * @author Elaine
 *
 * Zk Port
 * @author Elaine
 * @version	InfoPayment.java Adempiere Swing UI 3.4.1
 */
public class InfoProductPanel extends InfoPanel 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6804975825156657866L;
	
	private static final String PRODUCTION_TRX = "PRODUCTION_TRX";
	private static final String ORDERS_TRX = "ORDERS_TRX";
	
	private Label lblTokenSearch = new Label();
	private Textbox fieldTokenSearch = new Textbox();
	private Label lblValue = new Label();
	private Textbox fieldValue = new Textbox();
	private Label lblName = new Label();
	private Textbox fieldName = new Textbox();
	private Label lblUPC = new Label();
	private Textbox fieldUPC = new Textbox();
	private Label lblSKU = new Label();
	private Textbox fieldSKU = new Textbox();
	private Label lblPriceList = new Label();
	private Listbox pickPriceList = new Listbox();
	private Label lblWarehouse = new Label();
	private Listbox pickWarehouse = new Listbox();
	private WSearchEditor onlyVendor = null; 
	private Label onlyVendorLabel = new Label();

	private Label lblVendorProductNo = new Label();
	private Textbox fieldVendorProductNo = new Textbox();

	private Label lblPhantom = new Label();
	private Listbox cmbPhantom = new Listbox(YES_NO_OPTION.ALL_YES_NO); 
	private Label lblBOM = new Label();
	private Listbox cmbBOM = new Listbox(YES_NO_OPTION.ALL_YES_NO); 
	private Label lblPurchased = new Label();
	private Listbox cmbPurchased = new Listbox(YES_NO_OPTION.ALL_YES_NO); 

	// Elaine 2008/11/21
	private Label lblProductCategory = new Label();
	private Listbox pickProductCategory = new Listbox();
	//
	private Label lblAS = new Label();
	private Listbox pickAS = new Listbox();

	// Elaine 2008/11/25
	private Borderlayout borderlayout = new Borderlayout();
	private Textbox fieldDescription = new Textbox();
	Tabbox tabbedPane = new Tabbox();
	WListbox warehouseTbl = ListboxFactory.newDataTable();
    String m_sqlWarehouse;
    WListbox substituteTbl = ListboxFactory.newDataTable();
    String m_sqlSubstitute;
    WListbox relatedTbl = ListboxFactory.newDataTable();
    String m_sqlRelated;
    WListbox vendorTbl = ListboxFactory.newDataTable();
    String m_sqlVendor;
    WListbox pricelistTbl = ListboxFactory.newDataTable();
    String m_sqlPriceList;

    
    //Available to Promise Tab
	private WListbox 			m_tableAtp = ListboxFactory.newDataTable();
	private int 				m_M_Product_ID = 0;
    int mWindowNo = 0;
    //

	/**	Search Button				*/
	private Button	m_InfoPAttributeButton = new Button();
	/** Instance Button				*/
	private Button	m_PAttributeButton = null;
	/** SQL From				*/
	private static final String s_productFrom =
		"M_Product p"		
		+ " LEFT OUTER JOIN M_AttributeSet pa ON (p.M_AttributeSet_ID=pa.M_AttributeSet_ID)"
		;
	/**  Array of Column Info    */
	private static ColumnInfo[] s_productLayout = null;
	private static int s_productLayoutRole = -1;
	private static int INDEX_NAME = 0;
	private static int INDEX_PATTRIBUTE = 0;


	/** ASI							*/
	private int			m_M_AttributeSetInstance_ID = -1;
	/** Locator						*/
	private int			m_M_Locator_ID = 0;

	private String		m_pAttributeWhere = null;
	private int			m_C_BPartner_ID = 0;

	/**
	 *	Standard Constructor
	 * 	@param WindowNo window no
	 * 	@param M_Warehouse_ID warehouse
	 * 	@param M_PriceList_ID price list
	 * 	@param value    Query Value or Name if enclosed in @
	 * 	@param whereClause where clause
	 */
	public InfoProductPanel(int windowNo,
		int M_Warehouse_ID, int M_PriceList_ID, boolean multipleSelection,String value,
		 String whereClause)
	{
		this(windowNo, M_Warehouse_ID, M_PriceList_ID, multipleSelection, value, whereClause, true);
	}

	/**
	 *	Standard Constructor
	 * 	@param WindowNo window no
	 * 	@param M_Warehouse_ID warehouse
	 * 	@param M_PriceList_ID price list
	 * 	@param value    Query Value or Name if enclosed in @
	 * 	@param whereClause where clause
	 */
	public InfoProductPanel(int windowNo,
		int M_Warehouse_ID, int M_PriceList_ID, boolean multipleSelection,String value,
		 String whereClause, boolean lookup)
	{
		super (windowNo, "p", "M_Product_ID",multipleSelection, whereClause, lookup);
		log.info(value + ", Wh=" + M_Warehouse_ID + ", PL=" + M_PriceList_ID + ", WHERE=" + whereClause);
		setTitle(Msg.getMsg(Env.getCtx(), "InfoProduct"));
		
		Env.setContext(Env.getCtx(), windowNo, "IsSOTrx", "N");
		onlyVendor = WSearchEditor.createBPartner(windowNo); 
		//
		initComponents();
		init();
		initInfo (value, M_Warehouse_ID, M_PriceList_ID);
		m_C_BPartner_ID = Env.getContextAsInt(Env.getCtx(), windowNo, "C_BPartner_ID");

        int no = contentPanel.getRowCount();
        setStatusLine(Integer.toString(no) + " " + Msg.getMsg(Env.getCtx(), "SearchRows_EnterQuery"), false);
        setStatusDB(Integer.toString(no));
		//	AutoQuery
		if (value != null && value.length() > 0)
        {
			executeQuery();
            renderItems();
        }
		else 
		{
			if (!lookup) {
				initialId = Env.getContextAsInt(Env.getCtx(), windowNo, "M_Product_ID");
			}
			executeQuery();
            renderItems();
		}
		tabbedPane.setSelectedIndex(0);

		p_loadedOK = true;
		if (initialId != 0) {
			fieldValue.setValue(MProduct.get(Env.getCtx(), initialId).getValue());
			initialId = 0;
			
		}
		
		
		//Begin - fer_luck @ centuryon
		mWindowNo = windowNo; // Elaine 2008/12/16
		//End - fer_luck @ centuryon

	}	//	InfoProductPanel

	/**
	 *	initialize fields
	 */
	private void initComponents()
	{
		lblTokenSearch.setValue("Token Search");
		ZKUpdateUtil.setHflex(fieldTokenSearch, "true");
		fieldTokenSearch.addEventListener(Events.ON_CHANGING, this);
		
		lblValue = new Label();
		lblValue.setValue(Util.cleanAmp(Msg.translate(Env.getCtx(), "Value")));
		lblName = new Label();

		lblName.setValue(Util.cleanAmp(Msg.translate(Env.getCtx(), "Name")));
//		lblUPC =  new Label();
//		lblUPC.setValue(Msg.translate(Env.getCtx(), "UPC"));
//		lblSKU = new Label();
//		lblSKU.setValue(Msg.translate(Env.getCtx(), "SKU"));
		lblPriceList = new Label();
		lblPriceList.setValue(Msg.getMsg(Env.getCtx(), "PriceListVersion"));
		// Elaine 2008/11/21
		lblProductCategory = new Label();
		lblProductCategory.setValue(Msg.translate(Env.getCtx(), "M_Product_Category_ID"));
		//
		lblAS = new Label();
		lblAS.setValue(Msg.translate(Env.getCtx(), "M_AttributeSet_ID"));
		lblWarehouse = new Label();
		lblWarehouse.setValue(Util.cleanAmp(Msg.getMsg(Env.getCtx(), "Warehouse")));
		onlyVendorLabel = new Label();
		onlyVendorLabel.setValue(Msg.translate(Env.getCtx(), "Vendor"));
		lblVendorProductNo = new Label();
		lblVendorProductNo.setValue(Msg.translate(Env.getCtx(), "VendorProductNo"));

		
		lblPhantom = new Label();
		lblPhantom.setValue(Msg.translate(Env.getCtx(), "IsPhantom"));
		lblBOM = new Label();
		lblBOM.setValue(Msg.translate(Env.getCtx(), "IsBOM"));
		lblPurchased = new Label();
		lblPurchased.setValue(Msg.translate(Env.getCtx(), "IsPurchased"));

		m_InfoPAttributeButton.setIconSclass("z-icon-PAttribute");
		m_InfoPAttributeButton.setTooltiptext(Msg.getMsg(Env.getCtx(), "PAttribute"));
		m_InfoPAttributeButton.addEventListener(Events.ON_CLICK,this);

		fieldValue = new Textbox();
		fieldValue.setMaxlength(40);
		ZKUpdateUtil.setWidth(fieldValue, "100%");
		fieldName = new Textbox();
		ZKUpdateUtil.setWidth(fieldName, "100%");
		fieldUPC = new Textbox();
		fieldUPC.setMaxlength(40);
		ZKUpdateUtil.setWidth(fieldUPC, "100%");
		fieldSKU = new Textbox();
		fieldSKU.setMaxlength(40);
		ZKUpdateUtil.setWidth(fieldSKU, "100%");
		pickPriceList = new Listbox();
		pickPriceList.setRows(0);
		pickPriceList.setMultiple(false);
		pickPriceList.setMold("select");
		ZKUpdateUtil.setWidth(pickPriceList, "100%");
		pickPriceList.addEventListener(Events.ON_SELECT, this);


		cmbPhantom.setMultiple(false);
		cmbPhantom.setMold("select");
		ZKUpdateUtil.setWidth(cmbPhantom, "100%");
		cmbPhantom.addEventListener(Events.ON_SELECT, this);
		cmbBOM.setMultiple(false);
		cmbBOM.setMold("select");
		ZKUpdateUtil.setWidth(cmbBOM, "100%");
		cmbBOM.addEventListener(Events.ON_SELECT, this);
		cmbPurchased.setMultiple(false);
		cmbPurchased.setMold("select");
		ZKUpdateUtil.setWidth(cmbPurchased, "100%");
		cmbPurchased.addEventListener(Events.ON_SELECT, this);

		
		// Elaine 2008/11/21
		pickProductCategory = new Listbox();
		pickProductCategory.setRows(0);
		pickProductCategory.setMultiple(false);
		pickProductCategory.setMold("select");
		ZKUpdateUtil.setWidth(pickProductCategory, "100%");
		pickProductCategory.addEventListener(Events.ON_SELECT, this);
		//
		pickAS = new Listbox();
		pickAS.setRows(0);
		pickAS.setMultiple(false);
		pickAS.setMold("select");
		ZKUpdateUtil.setWidth(pickAS, "100%");
		pickAS.addEventListener(Events.ON_SELECT, this);

		pickWarehouse = new Listbox();
		pickWarehouse.setRows(0);
		pickWarehouse.setMultiple(false);
		pickWarehouse.setMold("select");
		ZKUpdateUtil.setWidth(pickWarehouse, "100%");
		pickWarehouse.addEventListener(Events.ON_SELECT, this);

		fieldVendorProductNo = new Textbox();
		fieldVendorProductNo.setMaxlength(40);
//		ZKUpdateUtil.setWidth(fieldVendorProductNo, "100%");
//		ZKUpdateUtil.setWidth(onlyVendor.getComponent(), "100%");
		
		ZKUpdateUtil.setVflex(contentPanel, true);
	}	//	initComponents

	private void init()
	{
    	Grid grid = GridFactory.newGridLayout();

		Rows rows = new Rows();
		grid.appendChild(rows);

		Row row = new Row();
		row.setSpans("1, 3, 1, 1, 1, 1");
		rows.appendChild(row);
		row.appendChild(lblTokenSearch.rightAlign());
		row.appendChild(fieldTokenSearch);
		row.appendChild(lblWarehouse.rightAlign());
		row.appendChild(pickWarehouse);
		row.appendChild(new Space());
		row.appendChild(m_InfoPAttributeButton);

		row = new Row();
		row.setSpans("1, 1, 1, 1, 1, 1, 1, 1");
		rows.appendChild(row);
		row.appendChild(lblValue.rightAlign());
		row.appendChild(fieldValue);
		row.appendChild(lblName.rightAlign());
		row.appendChild(fieldName);
		
//		row.appendChild(lblUPC.rightAlign());
//		row.appendChild(fieldUPC);
//		row.appendChild(lblSKU.rightAlign());
//		row.appendChild(fieldSKU);
//		row.appendChild(onlyVendorLabel.rightAlign());
//		ZKUpdateUtil.setHflex(onlyVendorLabel, "true");
//		row.appendChild(onlyVendor.getComponent());
		//

		row = new Row();
		row.setSpans("1, 1, 1, 1, 1, 1, 1, 1");
		rows.appendChild(row);
		row.appendChild(lblPriceList.rightAlign());
		row.appendChild(pickPriceList);
		row.appendChild(lblProductCategory.rightAlign());
		row.appendChild(pickProductCategory);
		row.appendChild(lblAS.rightAlign());
		row.appendChild(pickAS);
//		row.appendChild(lblVendorProductNo.rightAlign());
//		row.appendChild(fieldVendorProductNo);
		
		row = new Row();
		rows.appendChild(row);
		row.appendChild(lblPhantom.rightAlign());
		row.appendChild(cmbPhantom);
		row.appendChild(lblBOM.rightAlign());
		row.appendChild(cmbBOM);
		row.appendChild(lblPurchased.rightAlign());
		row.appendChild(cmbPurchased);

		
		row = new Row();
		rows.appendChild(row);
		row.appendChild(statusBar);
		row.setSpans("6");
		statusBar.setEastVisibility(false);

		// Product Attribute Instance
		m_PAttributeButton = confirmPanel.createButton(ConfirmPanel.A_PATTRIBUTE);
		confirmPanel.addComponentsLeft(m_PAttributeButton);
		m_PAttributeButton.addActionListener(this);
		m_PAttributeButton.setEnabled(false);

        // Elaine 2008/11/25
        fieldDescription.setMultiline(true);
		fieldDescription.setReadonly(true);

		//
        ColumnInfo[] s_layoutWarehouse = new ColumnInfo[]{
        		new ColumnInfo(Msg.translate(Env.getCtx(), "Warehouse"), "Warehouse", String.class),
        		new ColumnInfo(Msg.translate(Env.getCtx(), "QtyAvailable"), "sum(QtyAvailable)", Double.class),
        		new ColumnInfo(Msg.translate(Env.getCtx(), "QtyOnHand"), "sum(QtyOnHand)", Double.class),
        		new ColumnInfo(Msg.translate(Env.getCtx(), "QtyReserved"), "sum(QtyReserved)", Double.class),
        		new ColumnInfo(Msg.translate(Env.getCtx(), "QtyOrdered"), "sum(QtyOrdered)", Double.class)};
        /**	From Clause							*/
        String s_sqlFrom = " M_PRODUCT_STOCK_V ";
        /** Where Clause						*/
        String s_sqlWhere = "Value = ?";
        m_sqlWarehouse = warehouseTbl.prepareTable(s_layoutWarehouse, s_sqlFrom, s_sqlWhere, false, "M_PRODUCT_STOCK_V");
		m_sqlWarehouse += " Group By Warehouse, documentnote ";
		warehouseTbl.setMultiSelection(false);
        warehouseTbl.autoSize();
        warehouseTbl.getModel().addTableModelListener(this);

        ColumnInfo[] s_layoutSubstitute = new ColumnInfo[]{
        		new ColumnInfo(Msg.translate(Env.getCtx(), "Warehouse"), "orgname", String.class),
        		new ColumnInfo(
    					Msg.translate(Env.getCtx(), "Value"),
    					"(Select Value from M_Product p where p.M_Product_ID=M_PRODUCT_SUBSTITUTERELATED_V.Substitute_ID)",
    					String.class),
    			new ColumnInfo(Msg.translate(Env.getCtx(), "Name"), "Name", String.class),
    			new ColumnInfo(Msg.translate(Env.getCtx(), "QtyAvailable"), "QtyAvailable", Double.class),
  	        	new ColumnInfo(Msg.translate(Env.getCtx(), "QtyOnHand"), "QtyOnHand", Double.class),
    	        new ColumnInfo(Msg.translate(Env.getCtx(), "QtyReserved"), "QtyReserved", Double.class),
  	        	new ColumnInfo(Msg.translate(Env.getCtx(), "PriceStd"), "PriceStd", Double.class)};
        s_sqlFrom = "M_PRODUCT_SUBSTITUTERELATED_V";
        s_sqlWhere = "M_Product_ID = ? AND M_PriceList_Version_ID = ? and RowType = 'S'";
        m_sqlSubstitute = substituteTbl.prepareTable(s_layoutSubstitute, s_sqlFrom, s_sqlWhere, false, "M_PRODUCT_SUBSTITUTERELATED_V");
        substituteTbl.setMultiSelection(false);
        substituteTbl.autoSize();
        substituteTbl.getModel().addTableModelListener(this);

        ColumnInfo[] s_layoutRelated = new ColumnInfo[]{
        		new ColumnInfo(Msg.translate(Env.getCtx(), "Warehouse"), "orgname", String.class),
        		new ColumnInfo(
    					Msg.translate(Env.getCtx(), "Value"),
    					"(Select Value from M_Product p where p.M_Product_ID=M_PRODUCT_SUBSTITUTERELATED_V.Substitute_ID)",
    					String.class),
    			new ColumnInfo(Msg.translate(Env.getCtx(), "Name"), "Name", String.class),
    			new ColumnInfo(Msg.translate(Env.getCtx(), "QtyAvailable"), "QtyAvailable", Double.class),
  	        	new ColumnInfo(Msg.translate(Env.getCtx(), "QtyOnHand"), "QtyOnHand", Double.class),
    	        new ColumnInfo(Msg.translate(Env.getCtx(), "QtyReserved"), "QtyReserved", Double.class),
  	        	new ColumnInfo(Msg.translate(Env.getCtx(), "PriceStd"), "PriceStd", Double.class)};
        s_sqlFrom = "M_PRODUCT_SUBSTITUTERELATED_V";
        s_sqlWhere = "M_Product_ID = ? AND M_PriceList_Version_ID = ? and RowType = 'R'";
        m_sqlRelated = relatedTbl.prepareTable(s_layoutRelated, s_sqlFrom, s_sqlWhere, false, "M_PRODUCT_SUBSTITUTERELATED_V");
        relatedTbl.setMultiSelection(false);
        relatedTbl.autoSize();
        relatedTbl.getModel().addTableModelListener(this);

        //Available to Promise Tab
        m_tableAtp.setMultiSelection(false);

		ZKUpdateUtil.setHeight(tabbedPane, "250px");
//        ZKUpdateUtil.setVflex(tabbedPane, "true");
		Tabpanels tabPanels = new Tabpanels();
		tabbedPane.appendChild(tabPanels);
		Tabs tabs = new Tabs();
		tabbedPane.appendChild(tabs);

		Tab tab = new Tab(Util.cleanAmp(Msg.translate(Env.getCtx(), "Warehouse")));
		tabs.appendChild(tab);
		Tabpanel desktopTabPanel = new Tabpanel();
		ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
		desktopTabPanel.appendChild(warehouseTbl);
		tabPanels.appendChild(desktopTabPanel);
		
		tab = new Tab(Msg.getMsg(Env.getCtx(), "Price List"));
		tabs.appendChild(tab);
		desktopTabPanel = new Tabpanel();
		ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
		desktopTabPanel.appendChild(pricelistTbl);
		tabPanels.appendChild(desktopTabPanel);

		tab = new Tab(Msg.getMsg(Env.getCtx(), "ATP"));
		tabs.appendChild(tab);
		desktopTabPanel = new Tabpanel();
		ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
		desktopTabPanel.appendChild(m_tableAtp);
		tabPanels.appendChild(desktopTabPanel);

		tab = new Tab(Msg.getMsg(Env.getCtx(), "Vendor"));
		tabs.appendChild(tab);
		desktopTabPanel = new Tabpanel();
		ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
		desktopTabPanel.appendChild(vendorTbl);
		tabPanels.appendChild(desktopTabPanel);
		
		tab = new Tab(Msg.translate(Env.getCtx(), "Description"));
		tabs.appendChild(tab);
		desktopTabPanel = new Tabpanel();
		ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
		ZKUpdateUtil.setWidth(fieldDescription, "99%");
		ZKUpdateUtil.setHeight(fieldDescription, "99%");
		desktopTabPanel.appendChild(fieldDescription);
		tabPanels.appendChild(desktopTabPanel);

		tab = new Tab(Msg.translate(Env.getCtx(), "Substitute_ID"));
		tabs.appendChild(tab);
		desktopTabPanel = new Tabpanel();
		ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
		desktopTabPanel.appendChild(substituteTbl);
		tabPanels.appendChild(desktopTabPanel);

		tab = new Tab(Msg.translate(Env.getCtx(), "RelatedProduct_ID"));
		tabs.appendChild(tab);
		desktopTabPanel = new Tabpanel();
		ZKUpdateUtil.setHeight(desktopTabPanel, "100%");
		desktopTabPanel.appendChild(relatedTbl);
		tabPanels.appendChild(desktopTabPanel);

		//
		int height = SessionManager.getAppDesktop().getClientInfo().desktopHeight * 90 / 100;
		int width = SessionManager.getAppDesktop().getClientInfo().desktopWidth * 80 / 100;

		ZKUpdateUtil.setWidth(borderlayout, "100%");
		ZKUpdateUtil.setHeight(borderlayout, "100%");
        if (isLookup())
        	borderlayout.setStyle("border: none; position: relative");
        else
        	borderlayout.setStyle("border: none; position: absolute");
        Center center = new Center();
        center.setAutoscroll(true);
        //ZKUpdateUtil.setHeight(contentPanel, "300px");
		ZKUpdateUtil.setVflex(contentPanel, "true");
		borderlayout.appendChild(center);
		center.appendChild(contentPanel);
		South south = new South();
		int detailHeight = (height * 40 / 100);
//		ZKUpdateUtil.setHeight(south, detailHeight + "px");
		south.setCollapsible(true);
		south.setSplittable(true);
		ZKUpdateUtil.setVflex(south, "true");
		south.setTitle(Msg.translate(Env.getCtx(), "WarehouseStock"));
		south.setTooltiptext(Msg.translate(Env.getCtx(), "WarehouseStock"));
		borderlayout.appendChild(south);
		south.appendChild(tabbedPane);

        Borderlayout mainPanel = new Borderlayout();
//        ZKUpdateUtil.setWidth(mainPanel, "100%");
//        ZKUpdateUtil.setHeight(mainPanel, "100%");
        ZKUpdateUtil.setVflex(mainPanel, "true");
        ZKUpdateUtil.setHflex(mainPanel, "true");
        North north = new North();
        mainPanel.appendChild(north);
        north.appendChild(grid);
        center = new Center();
        mainPanel.appendChild(center);
        center.appendChild(borderlayout);
        south = new South();
        mainPanel.appendChild(south);
        south.appendChild(confirmPanel);
        if (!isLookup())
        {
        	mainPanel.setStyle("position: absolute");
        }

		this.appendChild(mainPanel);
		if (isLookup())
		{
			ZKUpdateUtil.setWindowWidthX(this, width);
			ZKUpdateUtil.setWindowHeightX(this, height);
		}

		contentPanel.addActionListener(new EventListener<Event>() {
			public void onEvent(Event event) throws Exception {
				int row = contentPanel.getSelectedRow();
				if (row >= 0) {
					int M_Warehouse_ID = 0;
					ListItem listitem = pickWarehouse.getSelectedItem();
					if (listitem != null)
						M_Warehouse_ID = (Integer)listitem.getValue();

					int M_PriceList_Version_ID = 0;
					listitem = pickPriceList.getSelectedItem();
					if (listitem != null)
						M_PriceList_Version_ID = (Integer)listitem.getValue();
				
        			refresh(contentPanel.getValueAt(row,2), M_Warehouse_ID, M_PriceList_Version_ID);
        			borderlayout.getSouth().setOpen(true);
				}
			}
		});
	}

	@Override
	protected void insertPagingComponent() {
		North north = new North();
		north.appendChild(paging);
		borderlayout.appendChild(north);
	}

	/**
	 * 	Refresh Query
	 */
	private void refresh(Object obj, int M_Warehouse_ID, int M_PriceList_Version_ID)
	{
		//int M_Product_ID = 0;
		String sql = m_sqlWarehouse;
		//Add description to the query
		sql = sql.replace(" FROM", ", DocumentNote FROM");
		log.finest(sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setString(1, (String)obj);
			rs = pstmt.executeQuery();
			fieldDescription.setText("");
			warehouseTbl.loadTable(rs);
			rs = pstmt.executeQuery();
			if(rs.next())
				if(rs.getString("DocumentNote") != null)
					fieldDescription.setText(rs.getString("DocumentNote"));
		}
		catch (Exception e)
		{
			log.log(Level.WARNING, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}

		m_M_Product_ID = getSelectedRowKey();

		sql = m_sqlSubstitute;
		log.finest(sql);
		try {
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, m_M_Product_ID);
			pstmt.setInt(2, M_PriceList_Version_ID);
			rs = pstmt.executeQuery();
			substituteTbl.loadTable(rs);
			rs.close();
		} catch (Exception e) {
			log.log(Level.WARNING, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}

		sql = m_sqlRelated;
		log.finest(sql);
		try {
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, m_M_Product_ID);
			pstmt.setInt(2, M_PriceList_Version_ID);
			rs = pstmt.executeQuery();
			relatedTbl.loadTable(rs);
			rs.close();
		} catch (Exception e) {
			log.log(Level.WARNING, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		initAtpTab(M_Warehouse_ID);
		initPriceListTab(m_M_Product_ID);
		initVendorTab(m_M_Product_ID);
	}	//	refresh

	/**
	 *	Dynamic Init
	 *
	 * @param value value
	 * @param M_Warehouse_ID warehouse
	 * @param M_PriceList_ID price list
	 */
	private void initInfo (String value, int M_Warehouse_ID, int M_PriceList_ID)
	{
		//	Pick init
		fillPicks(M_PriceList_ID);
		int M_PriceList_Version_ID = findPLV (M_PriceList_ID);
		//	Set Value
		if (value != null && value.length() > 0 && value.startsWith("@") && value.endsWith("@")) {
			String values[] = value.substring(1,value.length()-1).split("_");
//			fieldValue.setText(values[0]);
				fieldName.setText(values[0]);
		}
		else {
				fieldName.setText(value);
			
		}
		//	Set Warehouse
		//jtrinidad - temporarily start with no selected warehouse
//		if (M_Warehouse_ID == 0)
//			M_Warehouse_ID = Env.getContextAsInt(Env.getCtx(), "#M_Warehouse_ID");
		if (M_Warehouse_ID != 0)
			setWarehouse (M_Warehouse_ID);
		// 	Set PriceList Version
		
		//jtrinidad - temporarily start with no selected warehouse
//		if (M_PriceList_Version_ID != 0)
//			setPriceListVersion (M_PriceList_Version_ID);

		//	Create Grid
		StringBuffer where = new StringBuffer();
		where.append("p.IsActive='Y'");
		if (M_Warehouse_ID != 0)
			where.append(" AND p.IsSummary='N'");
		//  dynamic Where Clause
		if (p_whereClause != null && p_whereClause.length() > 0)
			where.append(" AND ")   //  replace fully qalified name with alias
				.append(Util.replace(p_whereClause, "M_Product.", "p."));
		//
		prepareTable(getProductLayout(),
			s_productFrom,
			where.toString(),
			"p.Value ASC");

		//
		pickWarehouse.addEventListener(Events.ON_SELECT,this);
		pickPriceList.addEventListener(Events.ON_SELECT,this);
		pickProductCategory.addEventListener(Events.ON_SELECT, this); // Elaine 2008/11/21
		pickAS.addEventListener(Events.ON_SELECT, this);
	}	//	initInfo

	/**
	 *	Fill Picks with values
	 *
	 * @param M_PriceList_ID price list
	 */
	private void fillPicks (int M_PriceList_ID)
	{
		//	Price List
		String SQL = "SELECT M_PriceList_Version.M_PriceList_Version_ID,"
			+ " M_PriceList_Version.Name || ' (' || c.Iso_Code || ')' AS ValueName "
			+ "FROM M_PriceList_Version, M_PriceList pl, C_Currency c "
			+ "WHERE M_PriceList_Version.M_PriceList_ID=pl.M_PriceList_ID"
			+ " AND pl.C_Currency_ID=c.C_Currency_ID"
			+ " AND M_PriceList_Version.IsActive='Y' AND pl.IsActive='Y'";
		//	Same PL currency as original one
		if (M_PriceList_ID != 0)
			SQL += " AND EXISTS (SELECT * FROM M_PriceList xp WHERE xp.M_PriceList_ID=" + M_PriceList_ID
				+ " AND pl.C_Currency_ID=xp.C_Currency_ID)";
		//	Add Access & Order
		SQL = MRole.getDefault().addAccessSQL (SQL, "M_PriceList_Version", true, false)	// fully qualidfied - RO
			+ " ORDER BY M_PriceList_Version.Name";
		try
		{
			pickPriceList.appendItem("",0);
			PreparedStatement pstmt = DB.prepareStatement(SQL, null);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				pickPriceList.appendItem(rs.getString(2),rs.getInt(1));
			}
			rs.close();
			pstmt.close();

			//	Warehouse
			SQL = MRole.getDefault().addAccessSQL (
				"SELECT M_Warehouse_ID, Value || ' - ' || Name AS ValueName "
				+ "FROM M_Warehouse "
				+ "WHERE IsActive='Y'",
					"M_Warehouse", MRole.SQL_NOTQUALIFIED, MRole.SQL_RO)
				+ " ORDER BY Value";
			pickWarehouse.appendItem("", 0);
			pstmt = DB.prepareStatement(SQL, null);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				pickWarehouse.appendItem(rs.getString("ValueName"), rs.getInt("M_Warehouse_ID"));
			}
			rs.close();
			pstmt.close();

			// Elaine 2008/11/21
			//	Product Category
			SQL = MRole.getDefault().addAccessSQL (
				"SELECT M_Product_Category_ID, Value || ' - ' || Name FROM M_Product_Category WHERE IsActive='Y'",
					"M_Product_Category", MRole.SQL_NOTQUALIFIED, MRole.SQL_RO)
				+ " ORDER BY Value";
			for (KeyNamePair kn : DB.getKeyNamePairs(SQL, true)) {
				pickProductCategory.addItem(kn);
			}

			//	Attribute Sets
			SQL = MRole.getDefault().addAccessSQL (
				"SELECT M_AttributeSet_ID, Name FROM M_AttributeSet WHERE IsActive='Y'",
					"M_AttributeSet", MRole.SQL_NOTQUALIFIED, MRole.SQL_RO)
				+ " ORDER BY Name";
			for (KeyNamePair kn : DB.getKeyNamePairs(SQL, true)) {
				pickAS.addItem(kn);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, SQL, e);
			setStatusLine(e.getLocalizedMessage(), true);
		}
	}	//	fillPicks

	/**
	 *	Set Warehouse
	 *
	 * 	@param M_Warehouse_ID warehouse
	 */
	private void setWarehouse(int M_Warehouse_ID)
	{
		for (int i = 0; i < pickWarehouse.getItemCount(); i++)
		{
			 Integer key = (Integer) pickWarehouse.getItemAtIndex(i).getValue();
			if (key == M_Warehouse_ID)
			{
				pickWarehouse.setSelectedIndex(i);
				return;
			}
		}
	}	//	setWarehouse

	/**
	 *	Set PriceList
	 *
	 * @param M_PriceList_Version_ID price list
	 */
	private void setPriceListVersion(int M_PriceList_Version_ID)
	{
		log.config("M_PriceList_Version_ID=" + M_PriceList_Version_ID);
		for (int i = 0; i < pickPriceList.getItemCount(); i++)
		{
			Integer key = (Integer) pickPriceList.getItemAtIndex(i).getValue();
			if (key == M_PriceList_Version_ID)
			{
				pickPriceList.setSelectedIndex(i);
				return;
			}
		}
		log.fine("NOT found");
	}	//	setPriceList

	/**
	 *	Find Price List Version and update context
	 *
	 * @param M_PriceList_ID price list
	 * @return M_PriceList_Version_ID price list version
	 */
	private int findPLV (int M_PriceList_ID)
	{
		Timestamp priceDate = null;
		//	Sales Order Date
		String dateStr = Env.getContext(Env.getCtx(), p_WindowNo, "DateOrdered");
		if (dateStr != null && dateStr.length() > 0)
			priceDate = Env.getContextAsDate(Env.getCtx(), p_WindowNo, "DateOrdered");
		else	//	Invoice Date
		{
			dateStr = Env.getContext(Env.getCtx(), p_WindowNo, "DateInvoiced");
			if (dateStr != null && dateStr.length() > 0)
				priceDate = Env.getContextAsDate(Env.getCtx(), p_WindowNo, "DateInvoiced");
		}
		//	Today
		if (priceDate == null)
			priceDate = new Timestamp(System.currentTimeMillis());
		//
		log.config("M_PriceList_ID=" + M_PriceList_ID + " - " + priceDate);
		int retValue = 0;
		String sql = "SELECT plv.M_PriceList_Version_ID, plv.ValidFrom "
			+ "FROM M_PriceList pl, M_PriceList_Version plv "
			+ "WHERE pl.M_PriceList_ID=plv.M_PriceList_ID"
			+ " AND plv.IsActive='Y'"
			+ " AND pl.M_PriceList_ID=? "					//	1
			+ "ORDER BY plv.ValidFrom DESC";
		//	find newest one
		try
		{
			PreparedStatement pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, M_PriceList_ID);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next() && retValue == 0)
			{
				Timestamp plDate = rs.getTimestamp(2);
				if (!priceDate.before(plDate))
					retValue = rs.getInt(1);
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		Env.setContext(Env.getCtx(), p_WindowNo, "M_PriceList_Version_ID", retValue);
		return retValue;
	}	//	findPLV


	/**************************************************************************
	 *	Construct SQL Where Clause and define parameters
	 *  (setParameters needs to set parameters)
	 *  Includes first AND
	 *  @return SQL WHERE clause
	 */
	public String getSQLWhere()
	{
		StringBuffer where = new StringBuffer();

		//initial query
		if ( initialId > 0 )
		{
			where.append(" AND p.M_Product_ID=?");
			//return where.toString();
		}
		
		//	Optional PLV
		int M_PriceList_Version_ID = 0;
		ListItem listitem = pickPriceList.getSelectedItem();
		if (listitem != null)
			M_PriceList_Version_ID = (Integer)listitem.getValue();
		if (M_PriceList_Version_ID != 0) {
			where.append(" AND EXISTS (select * from m_productprice  where m_product_id = p.m_product_id and m_pricelist_version_id = ?) ");
			
		}
		// Elaine 2008/11/29
		//  Optional Product Category
		if (getM_Product_Category_ID() > 0) {
			where.append(" AND p.M_Product_Category_ID=?");
		}
		//

		//  Optional Attribute Set
		if (getM_AttributeSet_ID() > 0) {
			where.append(" AND p.M_AttributeSet_ID=?");
		}

		//	Product Attribute Search
		if (m_pAttributeWhere != null)
		{
			where.append(m_pAttributeWhere);
			return where.toString();
		}
		//  => Token Search
		if (m_TSQuery != null) {
			where.append(" AND findkeyword(tscolumn, ?) ");
		}
		
		//  => Value
		String value = fieldValue.getText().toUpperCase();
		if (!(value.equals("") || value.equals("%")))
			where.append(" AND UPPER(p.Value) LIKE ?");

		//  => Name
		String name = fieldName.getText();
		
		if (!(name.equals("") || name.equals("%"))) {
			where.append(" AND UPPER(p.Name) LIKE ?");
		}

		//  => UPC
		String upc = fieldUPC.getText().toUpperCase();
		if (!(upc.equals("") || upc.equals("%")))
//			where.append(" AND UPPER(p.UPC) LIKE ?");
			//jobriant - multiple UPC per product
			where.append(" AND p.m_product_id IN (SELECT m_product_id FROM m_product_upctable WHERE upc LIKE ? AND isactive = 'Y' and ad_client_id=p.ad_client_id)");
		//  => SKU
		String sku = fieldSKU.getText().toUpperCase();
		if (!(sku.equals("") || sku.equals("%")))
			where.append(" AND UPPER(p.SKU) LIKE ?");
		//	=> Vendor

		if (cmbPhantom.getSelectedItem() != null &&
				!cmbPhantom.getSelectedItem().getValue().equals("*")) {
			where.append(" AND p.isPhantom = ?");
		}
		if (cmbBOM.getSelectedItem() != null &&
				!cmbBOM.getSelectedItem().getValue().equals("*")) {
			where.append(" AND p.isBOM = ?");
		}
		if (cmbPurchased.getSelectedItem() != null &&
				!cmbPurchased.getSelectedItem().getValue().equals("*")) {
			where.append(" AND p.isPurchased = ?");
		}
	
		Integer vendor = onlyVendor.getValue()!=null?(Integer)onlyVendor.getValue():null;

		if (vendor != null) {
			where.append(" AND EXISTS (select * from m_product_po  where m_product_id = p.m_product_id and c_bpartner_id = ?) ");
		}
		
		if (fieldVendorProductNo.getValue() != null && fieldVendorProductNo.getValue().replace("%", "").length() > 0) {
			where.append(" AND EXISTS (select * from m_product_po  where m_product_id = p.m_product_id and vendorproductno like ? ) ");
		}

		return where.toString();
	}	//	getSQLWhere

	/**
	 *  Set Parameters for Query
	 *  (as defined in getSQLWhere)	 *
	 * @param pstmt pstmt
	 *  @param forCount for counting records
	 * @throws SQLException
	 */
	protected void setParameters(PreparedStatement pstmt, boolean forCount) throws SQLException
	{
		int index = 1;

		//  => Warehouse
		int M_Warehouse_ID = 0;
		ListItem listitem = pickWarehouse.getSelectedItem();
		if (listitem != null)
			M_Warehouse_ID = (Integer)listitem.getValue();
		if (!forCount)	//	parameters in select
		{
			for (int i = 0; i < p_layout.length; i++)
			{
				if (p_layout[i].getColSQL().indexOf('?') != -1)
					pstmt.setInt(index++, M_Warehouse_ID);
			}
		}
		log.fine("M_Warehouse_ID=" + M_Warehouse_ID + " (" + (index-1) + "*)");

		//initial query
		if ( initialId > 0 )
		{
			pstmt.setInt(index++, initialId);
			log.fine("M_Product_ID=" + initialId);
			//return;
		}
		
		//  => PriceList
		int M_PriceList_Version_ID = 0; 
		ListItem lstitem = pickPriceList.getSelectedItem();
		if (lstitem != null)
			M_PriceList_Version_ID = (Integer)lstitem.getValue();
		if (M_PriceList_Version_ID != 0)
		{
			pstmt.setInt(index++, M_PriceList_Version_ID);
			log.fine("M_PriceList_Version_ID=" + M_PriceList_Version_ID);
		}
		// Elaine 2008/11/29
		//  => Product Category
		int M_Product_Category_ID = getM_Product_Category_ID();
		if (M_Product_Category_ID > 0) {
			pstmt.setInt(index++, M_Product_Category_ID);
			log.fine("M_Product_Category_ID=" + M_Product_Category_ID);
		}
		//
		int M_AttributeSet_ID = getM_AttributeSet_ID();
		if (M_AttributeSet_ID > 0) {
			pstmt.setInt(index++, M_AttributeSet_ID);
			log.fine("M_AttributeSet_ID=" + M_AttributeSet_ID);
		}
		//	Rest of Parameter in Query for Attribute Search
		if (m_pAttributeWhere != null)
			return;

		// => Token Search
		if (m_TSQuery != null) {
			pstmt.setString(index++, m_TSQuery);
//			m_TSQuery = null;  //reset it immediately?
		}
		//  => Value
		String value = fieldValue.getText().toUpperCase();
		if (!(value.equals("") || value.equals("%")))
		{
			if (!value.endsWith("%"))
				value += "%";
			pstmt.setString(index++, value);
			log.fine("Value: " + value);
		}

		//  => Name
		String name = fieldName.getText();
		if (!(name.equals("") || name.equals("%")))
		{
				if (!name.endsWith("%"))
					name += "%";
				pstmt.setString(index++, name.toUpperCase());
			log.fine("Name: " + name);
		}

		//  => UPC
		String upc = fieldUPC.getText().toUpperCase();
		if (!(upc.equals("") || upc.equals("%")))
		{
//			very slow on indexed column			
//			expected to have an exact upc as we use scanner 						
//			if (!upc.endsWith("%"))
//				upc += "%";
			pstmt.setString(index++, upc);
			log.fine("UPC: " + upc);
		}

		//  => SKU
		String sku = fieldSKU.getText().toUpperCase();
		if (!(sku.equals("") || sku.equals("%")))
		{
			if (!sku.endsWith("%"))
				sku += "%";
			pstmt.setString(index++, sku);
			log.fine("SKU: " + sku);
		}

		String isPhantom = null;
		ListItem lstPhantom = cmbPhantom.getSelectedItem();
		if (lstPhantom !=null &&
				!lstPhantom.getValue().toString().equals("*"))
			isPhantom = (String) lstPhantom.getValue();
		if (isPhantom != null)
		{
			pstmt.setString(index++, isPhantom);
			log.fine("IsPhantom=" + isPhantom);
		}

		String isBOM = null;
		ListItem lstBOM = cmbBOM.getSelectedItem();
		if (lstBOM != null &&
				!lstBOM.getValue().toString().equals("*"))
			isBOM = (String) lstBOM.getValue() ;
		if (isBOM != null)
		{
			pstmt.setString(index++, isBOM);
			log.fine("IsBOM=" + isBOM);
		}

		String isPurchased = null;
		ListItem lstPurchased = cmbPurchased.getSelectedItem();
		if (lstPurchased != null &&
				!lstPurchased.getValue().toString().equals("*"))
			isPurchased = (String) lstPurchased.getValue() ;
		if (isPurchased != null)
		{
			pstmt.setString(index++, isPurchased);
			log.fine("IsBOM=" + isPurchased);
		}

		Integer vendor = onlyVendor.getValue()!=null?(Integer)onlyVendor.getValue():null;
		if (vendor != null) {
			pstmt.setInt(index++, vendor.intValue());
			log.fine("Vendor C_BPartner_ID=" + vendor.intValue());

		}
		String vendorProductNo = fieldVendorProductNo.getValue();
		
		if (vendorProductNo != null && vendorProductNo.replace("%", "").length() > 0) {
			vendorProductNo = "%" + vendorProductNo.toUpperCase().replace("%", "") + "%";
			pstmt.setString(index++, vendorProductNo);
			log.fine("Vendor Product No=" + vendorProductNo);

		}


	}   //  setParameters

	/**
	 * 	Query per Product Attribute.
	 *  <code>
	 * 	Available synonyms:
	 *		M_Product p
	 *		M_ProductPrice pr
	 *		M_AttributeSet pa
	 *	</code>
	 */
	private void cmd_InfoPAttribute()
	{
		final InfoPAttributePanel ia = new InfoPAttributePanel(this);
		ia.addEventListener(DialogEvents.ON_WINDOW_CLOSE, new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				m_pAttributeWhere = ia.getWhereClause();
				if (m_pAttributeWhere != null)
				{
					executeQuery();
					renderItems();
				}
			}
		});
	}	//	cmdInfoAttribute

	/**
	 *	Show History
	 */
	protected void showHistory()
	{
		log.info("");
		Integer M_Product_ID = getSelectedRowKey();
		if (M_Product_ID == null)
			return;
		int M_Warehouse_ID = 0;
		ListItem listitem = pickWarehouse.getSelectedItem();
		if (listitem != null)
			M_Warehouse_ID = (Integer)listitem.getValue();
		int M_AttributeSetInstance_ID = m_M_AttributeSetInstance_ID;
		if (m_M_AttributeSetInstance_ID < -1)	//	not selected
			M_AttributeSetInstance_ID = 0;
		//
		InvoiceHistory ih = new InvoiceHistory (this, 0,
			M_Product_ID.intValue(), M_Warehouse_ID, M_AttributeSetInstance_ID);
		ih.setVisible(true);
		ih = null;
	}	//	showHistory

	/**
	 *	Has History
	 *
	 * @return true (has history)
	 */
	protected boolean hasHistory()
	{
		return true;
	}	//	hasHistory

	// Elaine 2008/12/16
	/**
	 *	Zoom
	 */
	public void zoom()
	{
		log.info("");
		Integer M_Product_ID = getSelectedRowKey();
		if (M_Product_ID == null)
			return;

		MQuery query = new MQuery("M_Product");
		query.addRestriction("M_Product_ID", MQuery.EQUAL, M_Product_ID);
		query.setRecordCount(1);
		int AD_WindowNo = getAD_Window_ID("M_Product", true);	//	SO
		AEnv.zoom (AD_WindowNo, query);
	}	//	zoom
	//

	/**
	 *	Has Zoom
	 *  @return (has zoom)
	 */
	protected boolean hasZoom()
	{
		return true;
	}	//	hasZoom

	/**
	 * Customize
	 */
	protected void customize()
	{
		log.info("");
		super.customize();
	} // customize

	/**
	 * Has Customize
	 * 
	 * @return true (customize)
	 */
	protected boolean hasCustomize()
	{
		return true;
	} // hasCustomize

	/**
	 * Product info support Token search
	 */
	protected boolean isSupportTokenSearch()
	{
		return true;
	}
	/**
	 *	Save Selection Settings for PriceList
	 */
	protected void saveSelectionDetail()
	{
		//  publish for Callout to read
		Integer ID = getSelectedRowKey();
		Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, "M_Product_ID", ID == null ? "0" : ID.toString());
		ListItem pickPL = (ListItem)pickPriceList.getSelectedItem();
		if (pickPL!=null)
		{
            Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, "M_PriceList_Version_ID",pickPL.getValue().toString());
        }
		ListItem pickWH = (ListItem)pickWarehouse.getSelectedItem();
		if (pickWH != null)
        {
            Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, "M_Warehouse_ID",pickWH.getValue().toString());
        }
		//
		if (m_M_AttributeSetInstance_ID == -1)	//	not selected
		{
			Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, "M_AttributeSetInstance_ID", "0");
			Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, "M_Locator_ID", "0");
		}
		else
		{
			Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, "M_AttributeSetInstance_ID",
				String.valueOf(m_M_AttributeSetInstance_ID));
			Env.setContext(Env.getCtx(), p_WindowNo, Env.TAB_INFO, "M_Locator_ID",
				String.valueOf(m_M_Locator_ID));
		}
	}	//	saveSelectionDetail

	/**
	 *  Get Product Layout
	 *
	 * @return array of Column_Info
	 */
	protected ColumnInfo[] getProductLayout()
	{
		if (s_productLayout != null && s_productLayoutRole == MRole.getDefault().getAD_Role_ID())
			return s_productLayout;
		//
		s_productLayout = null;
		s_productLayoutRole = MRole.getDefault().getAD_Role_ID();
		ArrayList<ColumnInfo> list = new ArrayList<ColumnInfo>();
		list.add(new ColumnInfo(" ", "p.M_Product_ID", IDColumn.class, 0));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "Discontinued").substring(0, 1), "p.Discontinued", Boolean.class, 1));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "Value"), "p.Value", String.class, 2));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "Name"), "p.Name", String.class, 3));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "QtyAvailable"), "bomQtyAvailable(p.M_Product_ID,?,0) AS QtyAvailable", Double.class, true, true, null, -1, 4));
//		if (MRole.getDefault().isColumnAccess(251 /*M_ProductPrice*/, 3027/*PriceList*/, false))
//			list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "PriceList"), "bomPriceList(p.M_Product_ID, pr.M_PriceList_Version_ID) AS PriceList",  BigDecimal.class, 5));
//		if (MRole.getDefault().isColumnAccess(251 /*M_ProductPrice*/, 3028/*PriceStd*/, false))
//			list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "PriceStd"), "bomPriceStd(p.M_Product_ID, pr.M_PriceList_Version_ID) AS PriceStd", BigDecimal.class, 6));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "QtyOnHand"), "bomQtyOnHand(p.M_Product_ID,?,0) AS QtyOnHand", Double.class, 7));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "QtyReserved"), "bomQtyReserved(p.M_Product_ID,?,0) AS QtyReserved", Double.class, 8));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "QtyOrdered"), "bomQtyOrdered(p.M_Product_ID,?,0) AS QtyOrdered", Double.class, 9));
		if (isUnconfirmed())
		{
			list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "QtyUnconfirmed"), "(SELECT SUM(c.TargetQty) FROM M_InOutLineConfirm c INNER JOIN M_InOutLine il ON (c.M_InOutLine_ID=il.M_InOutLine_ID) INNER JOIN M_InOut i ON (il.M_InOut_ID=i.M_InOut_ID) WHERE c.Processed='N' AND i.M_Warehouse_ID=? AND il.M_Product_ID=p.M_Product_ID) AS QtyUnconfirmed", Double.class, 10));
			list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "QtyUnconfirmedMove"), "(SELECT SUM(c.TargetQty) FROM M_MovementLineConfirm c INNER JOIN M_MovementLine ml ON (c.M_MovementLine_ID=ml.M_MovementLine_ID) INNER JOIN M_Locator l ON (ml.M_LocatorTo_ID=l.M_Locator_ID) WHERE c.Processed='N' AND l.M_Warehouse_ID=? AND ml.M_Product_ID=p.M_Product_ID) AS QtyUnconfirmedMove", Double.class, 11));
		}
//		if (MRole.getDefault().isColumnAccess(251 /*M_ProductPrice*/, 3028/*PriceStd*/, false) && MRole.getDefault().isColumnAccess(251 /*M_ProductPrice*/, 3029/*PriceLimit*/, false))
//			list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "Margin"), "bomPriceStd(p.M_Product_ID, pr.M_PriceList_Version_ID)-bomPriceLimit(p.M_Product_ID, pr.M_PriceList_Version_ID) AS Margin", BigDecimal.class, 12));
//		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "Vendor"), "bp.Name", String.class, 13));
//		if (MRole.getDefault().isColumnAccess(251 /*M_ProductPrice*/, 3029/*PriceLimit*/, false))
//			list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "PriceLimit"), "bomPriceLimit(p.M_Product_ID, pr.M_PriceList_Version_ID) AS PriceLimit", BigDecimal.class, 14));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "IsInstanceAttribute"), "pa.IsInstanceAttribute", Boolean.class, 15));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "IsPhantom"), "p.IsPhantom", Boolean.class, 16));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "IsBOM"), "p.IsBOM", Boolean.class, 17));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "IsPurchased"), "p.IsPurchased", Boolean.class, 18));
		s_productLayout = new ColumnInfo[list.size()];
		list.toArray(s_productLayout);
		INDEX_NAME = 3;
		INDEX_PATTRIBUTE = s_productLayout.length - 1;	//	last item

		return s_productLayout;
	}   //  getProductLayout

	/**
	 * 	System has Unforfirmed records
	 *	@return true if unconfirmed
	 */
	private boolean isUnconfirmed()
	{
		int no = DB.getSQLValue(null,
			"SELECT COUNT(*) FROM M_InOutLineConfirm WHERE AD_Client_ID=?",
			Env.getAD_Client_ID(Env.getCtx()));
		if (no > 0)
			return true;
		no = DB.getSQLValue(null,
			"SELECT COUNT(*) FROM M_MovementLineConfirm WHERE AD_Client_ID=?",
			Env.getAD_Client_ID(Env.getCtx()));
		return no > 0;
	}	//	isUnconfirmed

    public void onEvent(Event e)
    {
    	Component component = e.getTarget();

    	if (e.getTarget() == fieldTokenSearch) {
    		if (e instanceof InputEvent) {
    			fieldName.setValue(null);
    			fieldValue.setValue(null);
    			InputEvent evt = (InputEvent) e;
    			String text = evt.getValue();
    			if (text.length() <= MTable.TS_MIN_CHARACTERS) {
    				m_TSQuery = null;
    				return;
    			}
    			
    			String[] tokens = StringUtils.strip(text, "&@|+%").trim().split(" ");
    			m_TSQuery = "";
    			for (String s : tokens) {
    				if (m_TSQuery.length() > 0) {
    					m_TSQuery = m_TSQuery + "&";
    				}
    				m_TSQuery = s + ":*"; //add suffix wild card for every word
    			}
    			//TODO - update query without requiring to press the refresh button
    			//onUserQuery();
    		}
    		
    		return;
    	}
    	// Elaine 2008/12/16
		//  don't requery if fieldValue and fieldName are empty
		if ((e.getTarget() == pickWarehouse || e.getTarget() == pickPriceList)
			&& (fieldValue.getText().length() == 0 && fieldName.getText().length() == 0))
			return;
		//

    	if(component == m_InfoPAttributeButton)
    	{
    		cmd_InfoPAttribute();
    		return;
    	}

    	m_pAttributeWhere = null;
    	// Query Product Attribure Instance
		int row = contentPanel.getSelectedRow();
		if (component.equals(m_PAttributeButton) && row != -1)
		{
			Integer productInteger = getSelectedRowKey();
			String productName = (String)contentPanel.getValueAt(row, INDEX_NAME);

			ListItem warehouse = pickWarehouse.getSelectedItem();
			if (productInteger == null || productInteger.intValue() == 0 || warehouse == null)
				return;

			int M_Warehouse_ID = 0;
			if(warehouse.getValue() != null)
				M_Warehouse_ID = ((Integer)warehouse.getValue()).intValue();

			String title = warehouse.getLabel() + " - " + productName;
			final InfoPAttributeInstancePanel pai = new InfoPAttributeInstancePanel(this, title,
				M_Warehouse_ID, 0, productInteger.intValue(), m_C_BPartner_ID);
			pai.addEventListener(DialogEvents.ON_WINDOW_CLOSE, new EventListener<Event>() {

				@Override
				public void onEvent(Event event) throws Exception {
					m_M_AttributeSetInstance_ID = pai.getM_AttributeSetInstance_ID();
					m_M_Locator_ID = pai.getM_Locator_ID();
					if (m_M_AttributeSetInstance_ID != -1)
						dispose(true);
				}
			});
			return;
		}

		if (e.getName().equals(Events.ON_CTRL_KEY))
		{
			if (LayoutUtils.isReallyVisible(this))
			{
				onUserQuery();
			}
		}

		super.onEvent(e);
    }

	/**
	 *  Enable PAttribute if row selected/changed
	 */
	protected void enableButtons ()
	{
		m_M_AttributeSetInstance_ID = -1;
		if (m_PAttributeButton != null)
		{
			int row = contentPanel.getSelectedRow();
			boolean enabled = false;
			if (row >= 0)
			{
				Object value = contentPanel.getValueAt(row, INDEX_PATTRIBUTE);
				enabled = Boolean.TRUE.equals(value);
			}
			m_PAttributeButton.setEnabled(enabled);
		}

		super.enableButtons();
	}   //  enableButtons

    // Elaine 2008/11/26
	/**
	 *	Query ATP
	 */
	private void initAtpTab (int  m_M_Warehouse_ID)
	{
		//	Header
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "Date"));
		columnNames.add(Msg.translate(Env.getCtx(), "QtyOnHand"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "QtyOrdered"));
		columnNames.add(Msg.translate(Env.getCtx(), "QtyReserved"));
		columnNames.add(Msg.translate(Env.getCtx(), "M_Locator_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "M_AttributeSetInstance_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "DocumentNo"));
		columnNames.add(Msg.translate(Env.getCtx(), "M_Warehouse_ID"));

		//	Fill Storage Data
//		String sql = "SELECT sum(bomqtyonhandasi(s.m_product_id, s.m_attributesetinstance_id, l.m_warehouse_id, l.m_locator_id)) as QtyonHand"
//				+ ", SUM(s.QtyReserved), SUM(s.QtyOrdered),"
//				+ " productAttribute(s.M_AttributeSetInstance_ID), 0,";
//		sql += " w.Name, l.Value "
//			+ "FROM M_Storage s"
//			+ " INNER JOIN M_Locator l ON (s.M_Locator_ID=l.M_Locator_ID)"
//			+ " INNER JOIN M_Warehouse w ON (l.M_Warehouse_ID=w.M_Warehouse_ID) "
//			+ "WHERE M_Product_ID=?";
		String sql = "with productstorage as ( " +
	             "    select QtyonHand " +
	             "    , qtyreserved " +
	             "    , qtyordered " +
	             "    , s1.m_product_id, s1.m_locator_id, s1.m_attributesetinstance_id, 'N' as issummary " +
	             "  from m_storage s1  " +
	             "  union all " +
	             "    select sum(QtyonHand) " +
	             "    , sum(qtyreserved) " +
	             "    , sum(qtyordered) " +
	             "    , s2.m_product_id, s2.m_locator_id, 0 as m_attributesetinstance_id, 'Y' as issummary " +
	             "  from m_storage s2  " +
	             "  group by s2.m_product_id, s2.m_locator_id " +
	             ") " +
	             "select  " +
	             "  sum(s.qtyonhand) as qtyonhand " +
	             "  , sum(s.qtyreserved) as qtyreserved " +
	             "  , sum(s.qtyordered) as qtyordered " +
	             ", case when s.issummary = 'Y' then 'Summary' else productattribute(s.m_attributesetinstance_id) end as productattribute " +
	             ", 0, w.name, l.value, s.issummary " +
	             "from productstorage s " +
	             "  INNER JOIN M_Locator l ON (s.M_Locator_ID = l.M_Locator_ID) " +
	             "  INNER JOIN M_Warehouse w ON (l.M_Warehouse_ID = w.M_Warehouse_ID) " +		
	             "where s.m_product_id = ? ";
		if (m_M_Warehouse_ID != 0)
			sql += " AND l.M_Warehouse_ID=?";
		if (m_M_AttributeSetInstance_ID > 0)
			sql += " AND s.M_AttributeSetInstance_ID=?";
		//sql += " AND (s.QtyOnHand<>0 OR s.QtyReserved<>0 OR s.QtyOrdered<>0)";
		sql += " GROUP BY productAttribute(s.M_AttributeSetInstance_ID), w.Name, l.Value"
					+ ", s.m_product_id, l.m_warehouse_id, l.m_locator_id, s.issummary";
		sql += " ORDER BY l.Value, s.issummary DESC, productAttribute(s.M_AttributeSetInstance_ID)";

		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		double qty = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String lastLocator = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, m_M_Product_ID);
			if (m_M_Warehouse_ID != 0)
				pstmt.setInt(2, m_M_Warehouse_ID);
			if (m_M_AttributeSetInstance_ID > 0)
				pstmt.setInt(3, m_M_AttributeSetInstance_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<Object> line = new Vector<Object>(9);
				line.add(null);							//  Date
				BigDecimal qtyOnHand = new BigDecimal(rs.getDouble(1));
				BigDecimal qtyOrdered = new BigDecimal(rs.getDouble(3));
				BigDecimal qtyReserved = new BigDecimal(rs.getDouble(2));
				//do not display
				if (qtyOnHand.signum() == 0 && qtyOrdered.signum() == 0 && qtyReserved.signum() ==0) {
					continue;
				}
					
				line.add(qtyOnHand);  		//  Qty
				line.add(null);							//  BPartner
				line.add(qtyOrdered);  //  QtyOrdered
				line.add(qtyReserved);  //  QtyReserved
				String locator = rs.getString(7);
				if (lastLocator != null && locator.equals(lastLocator)) {
					log.fine("Do not add, existing locator =" + locator);
				} else {
					qty += qtyOnHand.doubleValue();
					lastLocator = locator;
				}
				line.add(locator);      		//  Locator
				String asi = rs.getString(4);
				line.add(asi);							//  ASI
				line.add(null);							//  DocumentNo
				line.add(rs.getString(6));  			//	Warehouse
				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}

		// Orders and Production Batch Data
		sql = " SELECT DatePromised, QtyReserved, ProductAttribute, M_AttributeSetInstance_ID, DocBaseType, BPName, DocumentNo, WarehouseName, LocatorValue, IsEndProduct, OrderType FROM ( "
				+ "	SELECT	o.DatePromised, ol.QtyReserved,	productAttribute(ol.M_AttributeSetInstance_ID) AS ProductAttribute, ol.M_AttributeSetInstance_ID, "
				+ " 	dt.DocBaseType, bp.Name AS BPName, dt.PrintName || ' ' || o.DocumentNo As DocumentNo, "
				+ " 	w.Name AS WarehouseName, NULL AS LocatorValue, NULL AS IsEndProduct, '" + ORDERS_TRX + "' AS OrderType "
				+ " FROM C_Order o " 
				+ " INNER JOIN C_OrderLine ol ON (o.C_Order_ID=ol.C_Order_ID) "
				+ " INNER JOIN C_DocType dt   ON (o.C_DocType_ID=dt.C_DocType_ID) "
				+ " INNER JOIN M_Warehouse w  ON (ol.M_Warehouse_ID=w.M_Warehouse_ID) "
				+ " INNER JOIN C_BPartner bp  ON (o.C_BPartner_ID=bp.C_BPartner_ID)  "
				+ " WHERE o.DocStatus NOT IN  ('VO', 'RE', 'CL') AND ol.QtyReserved <> 0 AND ol.M_Product_ID=? ";
		
		if (m_M_Warehouse_ID != 0)
			sql += " AND ol.M_Warehouse_ID=?";
		if (m_M_AttributeSetInstance_ID > 0)
			sql += " AND ol.M_AttributeSetInstance_ID=?";
		
		sql += " UNION ALL "
				+ " SELECT	pb.MovementDate AS DatePromised, pbl.QtyReserved, NULL AS ProductAttribute, 0 AS M_AttributeSetInstance_ID, "
				+ " 	dt.DocBaseType, NULL AS BPName, dt.PrintName || ' ' || pb.DocumentNo AS DocumentNo, "
				+ " 	w.Name AS WarehouseName, l.Value AS LocatorValue, pbl.IsEndProduct, '" + PRODUCTION_TRX + "' AS OrderType " 
				+ " FROM M_Production_Batch pb "
				+ " INNER JOIN M_PBatch_Line pbl ON (pbl.M_Production_Batch_ID=pb.M_Production_Batch_ID AND pb.DocStatus='CO' AND pb.QtyReserved<>0 AND pbl.QtyReserved<>0)"
				+ " INNER JOIN C_DocType dt 	ON (pb.C_DocType_ID = dt.C_DocType_ID) "
				+ " INNER JOIN M_Locator l 		ON (l.M_Locator_ID = pb.M_Locator_ID) "
				+ " INNER JOIN M_Warehouse w 	ON (w.M_Warehouse_ID = l.M_Warehouse_ID) " 
				+ " WHERE pbl.M_Product_ID = ? ";
		
		if (m_M_Warehouse_ID != 0)
			sql += " AND w.M_Warehouse_ID = ? ";
		sql += " ) AS ATPTabData	ORDER BY DatePromised ";
		
		try
		{
			int paramCount = 1;
			pstmt = DB.prepareStatement(sql, null);
			// Order parameters
			pstmt.setInt(paramCount++, m_M_Product_ID);
			if (m_M_Warehouse_ID != 0)
				pstmt.setInt(paramCount++, m_M_Warehouse_ID);
			if (m_M_AttributeSetInstance_ID > 0)
				pstmt.setInt(paramCount++, m_M_AttributeSetInstance_ID);

			// Production Batch parameters
			pstmt.setInt(paramCount++, m_M_Product_ID);
			if (m_M_Warehouse_ID != 0)
				pstmt.setInt(paramCount++, m_M_Warehouse_ID);

			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<Object> line = new Vector<Object>(9);
				line.add(rs.getTimestamp("DatePromised"));	// Date
				double oq = rs.getDouble("QtyReserved");
				String DocBaseType = rs.getString("DocBaseType");
				String OrderType = rs.getString("OrderType");
				Double qtyReserved = null;
				Double qtyOrdered = null;
				if ((ORDERS_TRX.equals(OrderType) && MDocType.DOCBASETYPE_PurchaseOrder.equals(DocBaseType))
						|| (PRODUCTION_TRX.equals(OrderType) && rs.getString("IsEndProduct").equals("Y")))
				{
					qtyOrdered = oq;
					qty += oq;
				}
				else
				{
					qtyReserved = oq;
					qty -= oq;
				}
				line.add(qty); 				// Qty
				line.add(rs.getString("BPName")); 		// BPartner
				line.add(qtyOrdered); 					// QtyOrdered
				line.add(qtyReserved); 					// QtyReserved
				line.add(rs.getString("LocatorValue")); // Locator
				String asi = rs.getString("ProductAttribute");
				line.add(asi); 							// ASI
				line.add(rs.getString("DocumentNo")); 	// DocumentNo
				line.add(rs.getString("WarehouseName"));// Warehouse
				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		// Table
		ListModelTable model = new ListModelTable(data);
		m_tableAtp.setData(model, columnNames);
		//
		m_tableAtp.setColumnClass(0, Timestamp.class, true); // Date
		m_tableAtp.setColumnClass(1, Double.class, true); // Quantity
		m_tableAtp.setColumnClass(2, String.class, true); // Partner
		m_tableAtp.setColumnClass(3, Double.class, true); // Quantity
		m_tableAtp.setColumnClass(4, Double.class, true); // Quantity
		m_tableAtp.setColumnClass(5, String.class, true); // Locator
		m_tableAtp.setColumnClass(6, String.class, true); // ASI
		m_tableAtp.setColumnClass(7, String.class, true); // DocNo
		m_tableAtp.setColumnClass(8, String.class, true); // Warehouse
		//
		m_tableAtp.autoSize();

		// Highlight Summary Rows
		for (int i = 0; i < m_tableAtp.getModel().getNoRows(); i++)
		{
			if (model.getValueAt(i, 6) != null && model.getValueAt(i, 6).toString().equalsIgnoreCase("Summary"))
			{
				m_tableAtp.getItemAtIndex(i).setStyle("Background-color: #BBBBBB;");
			}
		}
	}	//	initAtpTab
	//

	private void initPriceListTab (int  m_Product_ID)
	{
		//	Header
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "Price List"));
		columnNames.add(Msg.translate(Env.getCtx(), "Price List Version"));
		columnNames.add(Msg.translate(Env.getCtx(), "ValidFrom"));
		columnNames.add(Msg.translate(Env.getCtx(), "PriceList"));
		columnNames.add(Msg.translate(Env.getCtx(), "PriceStd"));
		columnNames.add(Msg.translate(Env.getCtx(), "PriceLimit"));

		String sql = "select  " +
	             "pl.name as PriceListName " +
	             ", plv.name as PriceListVersion " +
	             ", plv.validfrom " +
	             ", pp.pricelist " +
	             ", pp.pricestd " +
	             ", pp.pricelimit " +
	             "from m_productprice pp " +
	             "inner join m_pricelist_version plv on pp.m_pricelist_version_id = plv.m_pricelist_version_id " +
	             "inner join m_pricelist pl on plv.m_pricelist_id = pl.m_pricelist_id " +
	             "where " +
	             "	pp.M_Product_ID=? " +
	             "	order by pl.name, plv.m_pricelist_version_id";
		
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, m_Product_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<Object> line = new Vector<Object>(5);
				line.add(rs.getString(1));
				line.add(rs.getString(2));
				line.add(rs.getTimestamp(3));
				line.add(rs.getBigDecimal(4));
				line.add(rs.getBigDecimal(5));
				line.add(rs.getBigDecimal(6));
				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}

		// Table
		ListModelTable model = new ListModelTable(data);
		pricelistTbl.setData(model, columnNames);
		//
		pricelistTbl.setColumnClass(0, String.class, true); // Price List Name
		pricelistTbl.setColumnClass(1, String.class, true); // Price List Version
		pricelistTbl.setColumnClass(2, Timestamp.class, true); // Date
		pricelistTbl.setColumnClass(3, BigDecimal.class, true); // Price List
		pricelistTbl.setColumnClass(4, BigDecimal.class, true); // Price Std
		pricelistTbl.setColumnClass(5, BigDecimal.class, true); // Price Limit
		
		WListItemRenderer renderer = (WListItemRenderer) pricelistTbl.getItemRenderer();

 		
		//jtrinidad
		//TODO review visibility depending on Role access - you don't want customer seing all sorts of prices		
//		if (!MRole.getDefault().isColumnAccess(251 /*M_ProductPrice*/, 3027/*PriceList*/, false)) {
//			renderer.getHeaders().get(3).setVisible(false);  //Price List
//		}
//		if (MRole.getDefault().isColumnAccess(251 /*M_ProductPrice*/, 3028/*PriceStd*/, false)) {
//			renderer.getHeaders().get(4).setVisible(false);  //Price Std
//		}
//		if (MRole.getDefault().isColumnAccess(251 /*M_ProductPrice*/, 3029/*PriceLimit*/, false)) {
//			renderer.getHeaders().get(5).setVisible(false);  //Price Limit
//		}
		
		//
		pricelistTbl.autoSize();
	}	//	initPriceListTab
	//

	private void initVendorTab (int  m_Product_ID)
	{
		//	Header
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "Vendor"));
		columnNames.add(Msg.translate(Env.getCtx(), "Vendor Product No"));
		columnNames.add(Msg.translate(Env.getCtx(), "Buying UOM"));

		String sql = "SELECT bp.name AS vendor, " +
	             "       po.vendorproductno, " +
	             "       u.name AS uom " +
	             "FROM m_product_po po " +
	             "  INNER JOIN c_bpartner bp ON po.c_bpartner_id = bp.c_bpartner_id " +
	             "  INNER JOIN c_uom u ON po.c_uom_id = u.c_uom_id " +
	             "WHERE po.M_Product_ID = ? " +
	             "ORDER BY bp.name";
		
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, m_Product_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<Object> line = new Vector<Object>(5);
				line.add(rs.getString(1));
				line.add(rs.getString(2));
				line.add(rs.getString(3));
				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}

		// Table
		ListModelTable model = new ListModelTable(data);
		vendorTbl.setData(model, columnNames);
		//
		vendorTbl.setColumnClass(0, String.class, true); // Vendor
		vendorTbl.setColumnClass(1, String.class, true); // Vendor Product No
		vendorTbl.setColumnClass(2, String.class, true); // Buying UOM
		//
		vendorTbl.autoSize();
	}	//	initPriceListTab
	//

    // Elaine 2008/11/21
    public int getM_Product_Category_ID()
    {
		int M_Product_Category_ID = 0;

		ListItem pickPC = (ListItem)pickProductCategory.getSelectedItem();
		if (pickPC!=null)
			M_Product_Category_ID = Integer.parseInt(pickPC.getValue().toString());

		return M_Product_Category_ID;
	}
    //
    
    public int getM_AttributeSet_ID()
    {
		int M_AttributeSet_ID = 0;

		ListItem itemAS = (ListItem)pickAS.getSelectedItem();
		if (itemAS!=null)
			M_AttributeSet_ID = Integer.parseInt(itemAS.getValue().toString());

		return M_AttributeSet_ID;
	}

	@Override
	public String getSortDirection(Comparator cmpr) {
		return "natural";
	}

}	//	InfoProduct
