package org.adempiere.webui.panel;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.adempiere.util.Callback;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.event.DropUploadEvent;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.util.ZKUtils;
import org.adempiere.webui.window.FDialog;
import org.apache.commons.io.FileUtils;
import org.compiere.model.GridTab;
import org.compiere.model.MAttachment;
import org.compiere.model.MAttachmentEntry;
import org.compiere.model.Query;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Panelchildren;
import org.zkoss.zul.Vbox;
import org.zkoss.zul.impl.FileuploadDlg;

public class RecordAttachmentPanel extends Panel implements EventListener<Event> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7665096931970010483L;
	private static CLogger log = CLogger.getCLogger(RecordAttachmentPanel.class);
	
	private Panelchildren content;
	private Grid grid;
	private Rows rows;
	private GridTab gridTab;
	private FileuploadDlg fileUploadDlg;
	private Button uploadButton;
	private Vbox uploadBox;
	
	private int tableID = 0;
	private int recordID = 0;
	private List<MAttachmentEntry> attachmentEntryList;
	
	private List<Component> onClickComponentList = new ArrayList<Component>();
	
	public RecordAttachmentPanel() {
		super();
		init();
	}
	
	public void init() {
		setSclass("record-widget record-attachment-panel");
    	setTitle(Msg.translate(Env.getCtx(), "Attachments"));
    	setMaximizable(false);
    	setCollapsible(true);
    	setOpen(true);
    	setBorder("normal");
    	
    	content = new Panelchildren();
    	this.appendChild(content);
    	
    	grid = new Grid();
    	rows = grid.newRows();
    	grid.appendChild(rows);
    	content.appendChild(grid);
    	
    	ZKUpdateUtil.setWidth(grid, "100%");
		ZKUpdateUtil.setHeight(grid, "100%");
		grid.setSizedByContent(true);
		
		uploadButton = new Button();
		uploadButton.setLabel("Drop files here or click to upload");
		uploadButton.setIconSclass("z-icon-Export");
		uploadButton.setTooltiptext(Msg.getMsg(Env.getCtx(), "Load"));
		uploadButton.setUpload("true");
		uploadButton.addEventListener(Events.ON_UPLOAD, this);
		uploadButton.addEventListener(DropUploadEvent.ON_DROP_UPLOAD_EVENT, this);
		
		fileUploadDlg = new FileuploadDlg();
		
		uploadBox = new Vbox();
		uploadBox.appendChild(fileUploadDlg);
		uploadBox.appendChild(uploadButton);
	}
	
	public void render(GridTab gridTab) {
		clearContent();
		
		this.gridTab = gridTab;
		tableID = gridTab.getAD_Table_ID();
		recordID = gridTab.getRecord_ID();
		attachmentEntryList = getAttachmentEntries(tableID, recordID);
		
		Row row;
		if (!attachmentEntryList.isEmpty()) {
			int index = 0;
			for (MAttachmentEntry entry : attachmentEntryList) {
				A linkAttachment = new A();
				linkAttachment.setAttribute("AD_Attachment_ID", entry.getAD_Attachment_ID());
				linkAttachment.setAttribute("index", index++);
				linkAttachment.setLabel(entry.getFileName());
				linkAttachment.setTooltiptext(FileUtils.byteCountToDisplaySize(entry.getFileSize()));
				
				Label comment = new Label(entry.getComments());
				comment.setSclass("comments");
				
				Vbox box = new Vbox();
				box.appendChild(linkAttachment);
				box.appendChild(comment);
				box.setSclass("attachment-info");
				
				Button downloadButton = new Button();
				downloadButton.setAttribute("AD_AttachmentEntry_ID", entry.get_ID());
				downloadButton.setIconSclass("z-icon-Import");
				downloadButton.setTooltiptext(Msg.translate(Env.getCtx(), "Download"));
				downloadButton.setSclass("download-button");
				
				Button deleteButton = new Button();
				deleteButton.setAttribute("AD_AttachmentEntry_ID", entry.get_ID());
				deleteButton.setIconSclass("z-icon-Delete");
				deleteButton.setTooltiptext(Msg.translate(Env.getCtx(), "Delete"));
				deleteButton.setSclass("delete-button");
				
				row = new Row();
				row.appendCellChild(box);
				box.setStyle("max-width: 225px");
				row.appendCellChild(downloadButton);
				row.appendCellChild(deleteButton);
				rows.appendChild(row);
				
				linkAttachment.addEventListener(Events.ON_CLICK, this);
				downloadButton.addEventListener(Events.ON_CLICK, this);
				deleteButton.addEventListener(Events.ON_CLICK, this);
				
				onClickComponentList.add(linkAttachment);
				onClickComponentList.add(downloadButton);
				onClickComponentList.add(deleteButton);
			}
			setTitle(Msg.translate(Env.getCtx(), "Attachments") + " (" + attachmentEntryList.size() + ")");
		} else {
			setTitle(Msg.translate(Env.getCtx(), "Attachments"));
		}
		
		row = new Row();
		row.appendCellChild(uploadBox,6);
		ZKUpdateUtil.setHflex(uploadBox, "1");
		ZKUpdateUtil.setHflex(uploadButton, "1");
		ZKUpdateUtil.setHflex(fileUploadDlg, "1");
		rows.appendChild(row);
		
		Clients.evalJavaScript("dropToAttachFiles.init('" + uploadButton.getUuid() + "','" + uploadBox.getUuid() + "','"
				+ fileUploadDlg.getUuid() + "','" + AEnv.getDesktop().getId() + "','','','" + ZKUtils.getMaxUploadSize() + "');");
	}
	
	private void clearContent() {
		for(Component comp : onClickComponentList) {
			comp.removeEventListener(Events.ON_CLICK, this);
		}
		onClickComponentList.clear();
		
		List<?> childs = rows.getChildren();
		int childCount = childs.size();
		for (int c = childCount - 1; c >= 0; c--) {
			Row row = (Row) childs.get(c);
			rows.removeChild(row);
		}
	}
	
	private List<MAttachmentEntry> getAttachmentEntries(int tableID, int recordID) {
		List<MAttachmentEntry> entries = new ArrayList<>();
		List<MAttachment> attachmentList = new Query(Env.getCtx(), MAttachment.Table_Name, "AD_Table_ID = ? AND Record_ID = ?", null)
				.setOnlyActiveRecords(true)
				.setParameters(tableID, recordID)
				.list();
		for (MAttachment attachment : attachmentList) {
			List<MAttachmentEntry> entryList = new Query(Env.getCtx(), MAttachmentEntry.Table_Name, "AD_Attachment_ID = ?", null)
					.setOnlyActiveRecords(true)
					.setParameters(attachment.get_ID())
					.list();
			if (!entryList.isEmpty())
				entries.addAll(entryList);
		}
		return entries;
	}

	@Override
	public void onEvent(Event event) throws Exception {
		Component comp = event.getTarget();
        String eventName = event.getName();

        if (eventName.equals(Events.ON_CLICK)) {
        	if (comp instanceof A) {
    			A linkAttachment = (A) comp;
    			int attachmentID = (Integer) linkAttachment.getAttribute("AD_Attachment_ID");
    			int index = (Integer) linkAttachment.getAttribute("index");
    			WAttachment windowAttachment =  new WAttachment (gridTab.getWindowNo(), attachmentID,
    					tableID, recordID, null, new EventListener<Event>() {

							@Override
							public void onEvent(Event event) throws Exception {
								gridTab.dataRefresh();
							}
						});
    			windowAttachment.displayData(index, false);
    		} else if (comp instanceof Button) {
    			Button button = (Button) comp;
    			if (button.getSclass().equals("download-button")) {
    				int entryID = (Integer) button.getAttribute("AD_AttachmentEntry_ID");
    				MAttachmentEntry entry = new MAttachmentEntry(Env.getCtx(), entryID, null);
    				if (entry != null && entry.getData() != null) {
    					try {
    						AMedia media = new AMedia(entry.getFileName(), null, entry.getContentType(), entry.getData());
    						Filedownload.save(media);
    					}
    					catch (Exception e) {
    						log.log(Level.SEVERE, "attachment", e);
    					}
    				}
    			} else if (button.getSclass().equals("delete-button")) {
    				FDialog.ask(gridTab.getWindowNo(), this, "AttachmentDeleteEntry?", new Callback<Boolean>() {
    					
    					@Override
    					public void onCallback(Boolean result) {
    						if (result) {
    							int entryID = (Integer) button.getAttribute("AD_AttachmentEntry_ID");
    		    				MAttachmentEntry entry = new MAttachmentEntry(Env.getCtx(), entryID, null);
    		    				if (entry.delete(false))
    		    					render(gridTab);
    						}				
    					}
    				});
    			}
    		}
        } else if (event.getName().equals(DropUploadEvent.ON_DROP_UPLOAD_EVENT)) {
        	if (fileUploadDlg.getResult() != null) {
				for (Media media : fileUploadDlg.getResult())
				{
					if (media != null)
						attachData(media);
				}
			}
		}
		else if (event instanceof UploadEvent)  {
			UploadEvent ue = (UploadEvent) event;
			attachData(ue.getMedia());
			render(gridTab);
		}
	}
	
	private void attachData(Media media) {
		String fileName = media.getName();
		
		// update
		if (attachmentEntryList!= null && !attachmentEntryList.isEmpty()) {
			for (MAttachmentEntry entry : attachmentEntryList) {
				if (entry.getFileName().equals(fileName)) {
					entry.setData(getMediaData(media));
					return;
				}
			}
		}
		
		// new
		MAttachment attachment = MAttachment.get(Env.getCtx(), tableID, recordID);
		if (attachment == null)
			attachment = new MAttachment(Env.getCtx(), tableID, recordID, null);
		attachment.addEntry(fileName, getMediaData(media));
	}

	private byte[] getMediaData(Media media) {
		byte[] bytes = null;
		
		if ( media.isBinary() )
			bytes = media.getByteData();
		else
			bytes = media.getStringData().getBytes();

		return bytes;
	}
}
