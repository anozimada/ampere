package org.adempiere.webui.panel;

import java.util.ArrayList;
import java.util.List;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.WProcessCtl;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MQuery;
import org.compiere.model.MRole;
import org.compiere.model.PrintInfo;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportCtl;
import org.compiere.print.ReportEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.A;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Panelchildren;

public class RecordReportPanel extends Panel implements EventListener<Event>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6995531780099231972L;
	
	private Panelchildren content;
	private Grid grid;
	private Rows rows;
	
	private MQuery query;
	private int windowNo;

	private int tableID;

	public RecordReportPanel() {
		super();
		setSclass("record-widget record-report-panel");
    	setTitle(Msg.getMsg(Env.getCtx(), "Reports"));
    	setMaximizable(false);
    	setCollapsible(true);
    	setOpen(true);
    	setBorder("normal");
    	
    	content = new Panelchildren();
    	this.appendChild(content);
    	
    	grid = new Grid();
    	rows = grid.newRows();
    	content.appendChild(grid);
	}
	
	public void render(GridTab gridTab) {
		clearContent();
		
		tableID = gridTab.getAD_Table_ID();
		if (MRole.getDefault().isCanReport(tableID)) {
			windowNo = gridTab.getWindowNo();
			query = buildQuery(gridTab);
			ArrayList<KeyNamePair>	printFormatList = getPrintFormats(tableID);
			for(int i = 0; i < printFormatList.size(); i++) {
				KeyNamePair pp = (KeyNamePair) printFormatList.get(i);
				A linkReport = new A();
				linkReport.setAttribute("AD_PrintFormat_ID", pp.getKey());
				linkReport.setLabel(pp.getName());
				Row row = rows.newRow();
				row.appendCellChild(linkReport);
				
				linkReport.addEventListener(Events.ON_CLICK, this);
			}
			
			A linkReport = new A();
			linkReport.setAttribute("AD_PrintFormat_ID", -1);
			linkReport.setLabel("** " + Msg.getMsg(Env.getCtx(), "NewReport") + " **");
			Row row = rows.newRow();
			row.appendCellChild(linkReport);
			
			linkReport.addEventListener(Events.ON_CLICK, this);
		}
	}
	
	private void clearContent() {
		List<?> childs = rows.getChildren();
		int childCount = childs.size();
		for (int c = childCount - 1; c >=0; c--) {
			Row row = (Row) childs.get(c);
			row.getChildren().get(0).removeEventListener(Events.ON_CLICK, this);
			rows.removeChild(row);
		}
	}
	
	private MQuery buildQuery(GridTab curTab) {
		// Query
		MQuery query = new MQuery(curTab.getTableName());
		//	Link for detail records
		String queryColumn = curTab.getLinkColumnName();
		//	Current row otherwise
		if (queryColumn.length() == 0)
			queryColumn = curTab.getKeyColumnName();
		//	Find display
		String infoName = null;
		String infoDisplay = null;
		for (int i = 0; i < curTab.getFieldCount(); i++)
		{
			GridField field = curTab.getField(i);
			if (field.isKey())
				infoName = field.getHeader();
			if ((field.getColumnName().equals("Name") || field.getColumnName().equals("DocumentNo") )
				&& field.getValue() != null)
				infoDisplay = field.getValue().toString();
			if (infoName != null && infoDisplay != null)
				break;
		}
		if (queryColumn.length() != 0)
		{
			if (queryColumn.endsWith("_ID"))
				query.addRestriction(queryColumn, MQuery.EQUAL,
					Env.getContextAsInt(Env.getCtx(), curTab.getWindowNo(), queryColumn),
					infoName, infoDisplay);
			else
				query.addRestriction(queryColumn, MQuery.EQUAL,
					Env.getContext(Env.getCtx(), curTab.getWindowNo(), queryColumn),
					infoName, infoDisplay);
		}
		if ( curTab.getWhereClause() != null && curTab.getWhereClause().length() > 0 )
		{
			StringBuffer where = new StringBuffer(Env.parseContext(Env.getCtx(), curTab.getWindowNo(), curTab.getWhereClause(), false));
			query.addRestriction("(" + where + ")");
		}
		return query;
	}

	@Override
	public void onEvent(Event event) throws Exception {
		Component comp = event.getTarget();
        String eventName = event.getName();

        if (eventName.equals(Events.ON_CLICK)) {
        	if (comp instanceof A) {
    			A linkReport = (A) comp;
    			int printFormatID = (Integer) linkReport.getAttribute("AD_PrintFormat_ID");
    			
    			MPrintFormat pf = null;
    			if ( printFormatID ==  -1 )
    			{
    				pf = createNewPrintFormat();
    			}
    			else {
    			    pf = MPrintFormat.get(Env.getCtx(), printFormatID, false);
    			}
    			
    			launchReport(pf);
    		}
        }
	}
	
	private MPrintFormat createNewPrintFormat()
	{
		MPrintFormat pf = null;
		
		List<Triple<Integer, String, Integer>> rowSet = MPrintFormat.getAccessiblePrintFormats(tableID, 0, null);
		KeyNamePair pp = null;
		for ( Triple<Integer, String, Integer> trip : rowSet ) {
			pp = new KeyNamePair (trip.getLeft(), trip.getMiddle());
		}
		
		if (pp == null)
			 pf = MPrintFormat.createFromTable(Env.getCtx(), tableID);
		else
			 pf = MPrintFormat.copyToClient(Env.getCtx(), pp.getKey(), Env.getAD_Client_ID(Env.getCtx()));
		
		return pf;
			
	}

	private ArrayList<KeyNamePair> getPrintFormats (int AD_Table_ID) {
		ArrayList<KeyNamePair>	printFormatList = new ArrayList<KeyNamePair>();
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		List<Triple<Integer, String, Integer>> rowSet = MPrintFormat.getAccessiblePrintFormats(AD_Table_ID, -1, null);
		KeyNamePair pp = null;
		for ( Triple<Integer, String, Integer> trip : rowSet ) {
			pp = new KeyNamePair (trip.getLeft(), trip.getMiddle());
			if (trip.getRight() == AD_Client_ID) {
				printFormatList.add(pp);
			}
		}

		return printFormatList;
	}	//	getPrintFormats

	/**
	 * 	Launch Report
	 * 	@param pf print format
	 */
	private void launchReport (MPrintFormat pf)
	{
		int Record_ID = 0;
		if (query.getRestrictionCount()==1 && query.getCode(0) instanceof Integer)
			Record_ID = ((Integer)query.getCode(0)).intValue();
		if (Record_ID == 0 && query.getRestrictionCount()==1 &&  !(query.getCode(0) instanceof Integer) && StringUtils.isNumeric((String) query.getCode(0))) {
			Record_ID = Integer.parseInt((String) query.getCode(0));
		}
		PrintInfo info = new PrintInfo(
			pf.getName(),
			pf.getAD_Table_ID(),
			Record_ID);
		info.setDescription(query.getInfo());
		
		if(pf != null && pf.getJasperProcess_ID() > 0)
		{
			// It's a report using the JasperReports engine
			ProcessInfo pi = new ProcessInfo ("", pf.getJasperProcess_ID(), pf.getAD_Table_ID(), Record_ID);
			
			//	Execute Process
			WProcessCtl.process(null, windowNo, pi, null, AEnv.getDesktop() );
		}
		else
		{
			// It's a default report using the standard printing engine
			ReportEngine re = new ReportEngine (Env.getCtx(), pf, query, info);
			ReportCtl.preview(re);
		}
	}	//	launchReport
}
