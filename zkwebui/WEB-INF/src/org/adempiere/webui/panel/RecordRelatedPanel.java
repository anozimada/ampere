package org.adempiere.webui.panel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.adempiere.model.ZoomInfoFactory;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.compiere.model.GridTab;
import org.compiere.model.MColumn;
import org.compiere.model.MQuery;
import org.compiere.model.MTable;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.A;
import org.zkoss.zul.Label;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Panelchildren;

public class RecordRelatedPanel extends Panel implements EventListener<Event> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6168119719051620634L;

	private Panelchildren content;
	private Grid grid;
	private Rows rows;
	
	private List<ZoomInfoFactory.ZoomInfo> zoomInfos = new ArrayList<ZoomInfoFactory.ZoomInfo>();
	private Map<Integer,String[]> identifierMap = new HashMap<Integer, String[]>();
	
	public RecordRelatedPanel() {
		super();
		setSclass("record-widget record-related-panel");
    	setTitle(Msg.getMsg(Env.getCtx(), "Related records"));
    	setMaximizable(false);
    	setCollapsible(true);
    	setOpen(true);
    	setBorder("normal");
    	
    	content = new Panelchildren();
    	this.appendChild(content);
    	
    	grid = new Grid();
    	rows = grid.newRows();
    	content.appendChild(grid);
	}
	
	public void render(GridTab gridTab) {
		clearContent();
		
		int tableID = gridTab.getAD_Table_ID();
		int recordID = gridTab.getRecord_ID();
		int windowID = gridTab.getAD_Window_ID();
		
		if (recordID > 0) {
			PO po = MTable.get(Env.getCtx(), tableID).getPO(recordID, null);
			List<ZoomInfoFactory.ZoomInfo>  zoomInfoList = ZoomInfoFactory.retrieveZoomInfos(po, windowID);
			for (ZoomInfoFactory.ZoomInfo zoomInfo : zoomInfoList) {
				if (zoomInfo.query.getRecordCount() > 0) {
					zoomInfos.add(zoomInfo);
					String label = zoomInfo.destinationDisplay + " (" + zoomInfo.query.getRecordCount() + ")";
					A linkZoom = new A();
					linkZoom.setLabel(label);
					linkZoom.setAttribute("index", zoomInfos.size()-1);
					Row row = rows.newRow();
					row.appendCellChild(linkZoom);

					linkZoom.addEventListener(Events.ON_CLICK, this);

					MTable table = MTable.get(Env.getCtx(),  zoomInfo.query.getZoomTableName());
					if ( zoomInfo.query.getRecordCount() < 4 && table.isSingleKey() )
					{
						// record detail
						List<PO> recordList = new Query(Env.getCtx(), table,
								zoomInfo.query.getWhereClause(), null).list();
						for (PO record : recordList) {
							MQuery query = zoomInfo.query.deepCopy();
							query.addRestriction(record.get_KeyColumns()[0], "=", record.get_ID());
							ZoomInfoFactory.ZoomInfo recordZoomInfo = new ZoomInfoFactory.ZoomInfo(zoomInfo.windowId, query, record.toString());
							zoomInfos.add(recordZoomInfo);

							label = getRecordDisplay(zoomInfo.query.getZoomTableName(), record.get_ID());
							Label ll = new Label(label);
							ll.setAttribute("index", zoomInfos.size()-1);
							ll.setStyle("padding-left: 20px; cursor: pointer; text-decoration: underline");
							//ll.setSclass("label-zoomable");
							row = rows.newRow();
							row.appendCellChild(ll);

							ll.addEventListener(Events.ON_CLICK, this);
						}
					}
				}
			}
		}
	}
	
	private void clearContent() {
		zoomInfos.clear();
		List<?> childs = rows.getChildren();
		int childCount = childs.size();
		for (int c = childCount - 1; c >=0; c--) {
			Row row = (Row) childs.get(c);
			row.getChildren().get(0).removeEventListener(Events.ON_CLICK, this);
			rows.removeChild(row);
		}
	}
	
	@Override
	public void onEvent(Event event) throws Exception {
		Component comp = event.getTarget();
        String eventName = event.getName();

        if (eventName.equals(Events.ON_CLICK)) {
        	if (comp instanceof A || comp instanceof Label ) {
        		int index = (Integer) comp.getAttribute("index");
        		ZoomInfoFactory.ZoomInfo zoomInfo = zoomInfos.get(index);
        		AEnv.zoom(zoomInfo.windowId, zoomInfo.query);
    		}
        }
	}
	
	private String getRecordDisplay(String tableName, int recordID) {
		String value = "";
		MTable mTable = MTable.get(Env.getCtx(), tableName);
		ArrayList<MColumn> list = new ArrayList<MColumn>();
		String[] identifiers;
		if (identifierMap.get(mTable.get_ID()) == null) {
			identifiers = mTable.getIdentifierColumns();
			identifierMap.put(mTable.get_ID(), identifiers);
		} else {
			identifiers = identifierMap.get(mTable.get_ID());
		}
		
		for (String idColumnName : identifiers) {
			MColumn column = mTable.getColumn(idColumnName);
			list.add (column);
		}
		if(list.size() > 0) {
			StringBuilder displayColumn = new StringBuilder();
			String separator = " - ";
			
			for(int i = 0; i < list.size(); i++) {
				MColumn identifierColumn = list.get(i);
				if(i > 0)
					displayColumn.append("||'").append(separator).append("'||");
				
				displayColumn.append("COALESCE(")
							.append(DB.TO_CHAR(identifierColumn.getColumnName(), 
												identifierColumn.getAD_Reference_ID(), 
												Env.getAD_Language(Env.getCtx())))
							.append(",'')");
			}
			StringBuilder sql = new StringBuilder("SELECT ");
			sql.append(displayColumn.toString());
			sql.append(" FROM ").append(tableName);
			sql.append(" WHERE ")
				.append(tableName).append(".").append(tableName).append("_ID=?");
			
			value = DB.getSQLValueStringEx(null, sql.toString(), recordID);
		}
		return value;
	}

}
