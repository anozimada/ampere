package org.adempiere.webui.panel;

import java.util.Date;
import java.util.List;

import org.adempiere.webui.apps.form.WArchiveViewer;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.session.SessionManager;
import org.compiere.model.GridTab;
import org.compiere.model.MArchive;
import org.compiere.model.Query;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.A;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Panelchildren;

public class RecordArchivePanel extends Panel implements EventListener<Event> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7630630038837292188L;
	
	private Panelchildren content;
	private Grid grid;
	private Rows rows;
	
	private int tableID = 0;
	private int recordID = 0;
	
	public RecordArchivePanel() {
		super();
		setSclass("record-widget record-archive-panel");
    	setTitle(Msg.getMsg(Env.getCtx(), "Archived Docs"));
    	setMaximizable(false);
    	setCollapsible(true);
    	setOpen(true);
    	setBorder("normal");
    	
    	content = new Panelchildren();
    	this.appendChild(content);
    	
    	grid = new Grid();
    	rows = grid.newRows();
    	content.appendChild(grid);
	}
	
	public void render(GridTab gridTab) {
		clearContent();
		
		tableID = gridTab.getAD_Table_ID();
		recordID = gridTab.getRecord_ID();
		List<MArchive> archiveList = new Query(Env.getCtx(), MArchive.Table_Name, "AD_Table_ID = ? AND Record_ID = ?", null)
				.setOnlyActiveRecords(true)
				.setParameters(tableID, recordID)
				.list();
		
		for (MArchive archive : archiveList) {
			A linkArchive = new A();
			linkArchive.setAttribute("AD_Archive_ID", archive.get_ID());
			linkArchive.setLabel(archive.getName());
			Row row = rows.newRow();
			row.appendCellChild(linkArchive);
			row.appendCellChild(new Label(DisplayType.getTimestampFormat_Default().format(new Date(archive.getCreated().getTime()))));
			row.appendCellChild(new Label(archive.getCreatedByName()));
			
			linkArchive.addEventListener(Events.ON_CLICK, this);
		}
	}
	
	private void clearContent() {
		List<?> childs = rows.getChildren();
		int childCount = childs.size();
		for (int c = childCount - 1; c >=0; c--) {
			Row row = (Row) childs.get(c);
			row.getChildren().get(0).removeEventListener(Events.ON_CLICK, this);
			rows.removeChild(row);
		}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		Component comp = event.getTarget();
        String eventName = event.getName();

        if (eventName.equals(Events.ON_CLICK)) {
        	if (comp instanceof A) {
    			A linkArchive = (A) comp;
    			int archiveID = (Integer) linkArchive.getAttribute("AD_Archive_ID");
    			int AD_Form_ID = 118;	//	ArchiveViewer
    			ADForm form = ADForm.openForm(AD_Form_ID);
    			
    			WArchiveViewer av = (WArchiveViewer) form.getICustomForm();
    			av.query(false, tableID, recordID);
    			form.setAttribute(Window.MODE_KEY, Window.MODE_EMBEDDED);
    			SessionManager.getAppDesktop().showWindow(form);
    		}
        }
	}

}
