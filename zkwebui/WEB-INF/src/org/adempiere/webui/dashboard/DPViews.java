/******************************************************************************
 * Copyright (C) 2008 Elaine Tan                                              *
 * Copyright (C) 2008 Idalica Corporation
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.webui.dashboard;

import java.util.List;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.ToolBarButton;
import org.adempiere.webui.panel.InfoPanel;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.MInfoWindow;
import org.compiere.model.MRole;
import org.compiere.model.Query;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Separator;
import org.zkoss.zul.Space;
import org.zkoss.zul.Vlayout;

/**
 * Dashboard item: Info views
 * @author Elaine
 * @date November 20, 2008
 */
public class DPViews extends DashboardPanel implements EventListener<Event> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8375414665766937581L;

	public DPViews()
	{
		super();

		Space space = new Space();
		ZKUpdateUtil.setHeight(space, "2px");
		this.appendChild(space);

		this.appendChild(createViewPanel());
		this.setClass("dp-views-items");
	}
	
	private Vlayout createViewPanel()
	{
		Vlayout vbox = new Vlayout();
		vbox.setClass("dp-views-box");
		ZKUpdateUtil.setWidth(vbox, "100%");
				
		if (MRole.getDefault().isAllow_Info_Product())
		{
			ToolBarButton btnViewItem = new ToolBarButton("InfoProduct");
			btnViewItem.setLabel(Msg.getMsg(Env.getCtx(), "InfoProduct"));
			btnViewItem.setIconSclass("z-icon-InfoProduct");
			btnViewItem.setTooltiptext(btnViewItem.getLabel());
			btnViewItem.addEventListener(Events.ON_CLICK, this);
			vbox.appendChild(btnViewItem);
		}
		if (MRole.getDefault().isAllow_Info_BPartner())
		{
			ToolBarButton btnViewItem = new ToolBarButton("InfoBPartner");
			btnViewItem.setLabel(Msg.getMsg(Env.getCtx(), "InfoBPartner"));
			btnViewItem.setIconSclass("z-icon-InfoBPartner");
			btnViewItem.setTooltiptext(btnViewItem.getLabel());
			btnViewItem.addEventListener(Events.ON_CLICK, this);
			vbox.appendChild(btnViewItem);
		}
		if (MRole.getDefault().isShowAcct() && MRole.getDefault().isAllow_Info_Account())
		{
			ToolBarButton btnViewItem = new ToolBarButton("InfoAccount");
			btnViewItem.setLabel(Msg.getMsg(Env.getCtx(), "InfoAccount"));
			btnViewItem.setIconSclass("z-icon-InfoAccount");
			btnViewItem.setTooltiptext(btnViewItem.getLabel());
			btnViewItem.addEventListener(Events.ON_CLICK, this);
			vbox.appendChild(btnViewItem);
		}
		if (MRole.getDefault().isAllow_Info_Schedule())
		{
			ToolBarButton btnViewItem = new ToolBarButton("InfoSchedule");
			btnViewItem.setLabel(Msg.getMsg(Env.getCtx(), "InfoSchedule"));
			btnViewItem.setIconSclass("z-icon-InfoSchedule");
			btnViewItem.setTooltiptext(btnViewItem.getLabel());
			btnViewItem.addEventListener(Events.ON_CLICK, this);
			vbox.appendChild(btnViewItem);
		}
		vbox.appendChild(new Separator("horizontal"));
		if (MRole.getDefault().isAllow_Info_Order())
		{
			ToolBarButton btnViewItem = new ToolBarButton("InfoOrder");
			btnViewItem.setLabel(Msg.getMsg(Env.getCtx(), "InfoOrder"));
			btnViewItem.setIconSclass("z-icon-Info");
			btnViewItem.setTooltiptext(btnViewItem.getLabel());
			btnViewItem.addEventListener(Events.ON_CLICK, this);
			vbox.appendChild(btnViewItem);
		}
		if (MRole.getDefault().isAllow_Info_Invoice())
		{
			ToolBarButton btnViewItem = new ToolBarButton("InfoInvoice");
			btnViewItem.setLabel(Msg.getMsg(Env.getCtx(), "InfoInvoice"));
			btnViewItem.setIconSclass("z-icon-Info");
			btnViewItem.setTooltiptext(btnViewItem.getLabel());
			btnViewItem.addEventListener(Events.ON_CLICK, this);
			vbox.appendChild(btnViewItem);
		}
		if (MRole.getDefault().isAllow_Info_InOut())
		{
			ToolBarButton btnViewItem = new ToolBarButton("InfoInOut");
			btnViewItem.setLabel(Msg.getMsg(Env.getCtx(), "InfoInOut"));
			btnViewItem.setIconSclass("z-icon-Info");
			btnViewItem.setTooltiptext(btnViewItem.getLabel());
			btnViewItem.addEventListener(Events.ON_CLICK, this);
			vbox.appendChild(btnViewItem);
		}
		if (MRole.getDefault().isAllow_Info_Payment())
		{
			ToolBarButton btnViewItem = new ToolBarButton("InfoPayment");
			btnViewItem.setLabel(Msg.getMsg(Env.getCtx(), "InfoPayment"));
			btnViewItem.setIconSclass("z-icon-Info");
			btnViewItem.setTooltiptext(btnViewItem.getLabel());
			btnViewItem.addEventListener(Events.ON_CLICK, this);
			vbox.appendChild(btnViewItem);
		}
		if (MRole.getDefault().isAllow_Info_CashJournal())
		{
			ToolBarButton btnViewItem = new ToolBarButton("InfoCashLine");
			btnViewItem.setLabel(Msg.getMsg(Env.getCtx(), "InfoCashLine"));
			btnViewItem.setIconSclass("z-icon-Info");
			btnViewItem.setTooltiptext(btnViewItem.getLabel());
			btnViewItem.addEventListener(Events.ON_CLICK, this);
			vbox.appendChild(btnViewItem);
		}
		if (MRole.getDefault().isAllow_Info_Resource())
		{
			ToolBarButton btnViewItem = new ToolBarButton("InfoAssignment");
			btnViewItem.setLabel(Msg.getMsg(Env.getCtx(), "InfoAssignment"));
			btnViewItem.setIconSclass("z-icon-Info");
			btnViewItem.setTooltiptext(btnViewItem.getLabel());
			btnViewItem.addEventListener(Events.ON_CLICK, this);
			vbox.appendChild(btnViewItem);
		}
		if (MRole.getDefault().isAllow_Info_Asset())
		{
			ToolBarButton btnViewItem = new ToolBarButton("InfoAsset");
			btnViewItem.setLabel(Msg.getMsg(Env.getCtx(), "InfoAsset"));
			btnViewItem.setIconSclass("z-icon-Info");
			btnViewItem.setTooltiptext(btnViewItem.getLabel());
			btnViewItem.addEventListener(Events.ON_CLICK, this);
			vbox.appendChild(btnViewItem);
		}

		List<MInfoWindow> list = new Query(Env.getCtx(), MInfoWindow.Table_Name,
				"IsValid='Y' AND IsShowInDashboard='Y'", null).setOnlyActiveRecords(true)
						.setOrderBy(MInfoWindow.COLUMNNAME_SeqNo).list();

		MInfoWindow[] infos = list.toArray(new MInfoWindow[list.size()]);

		for (int i = 0; i < infos.length; i++)
		{
			MInfoWindow info = infos[i];
			if (MInfoWindow.get(info.getAD_InfoWindow_ID(), null) != null)
			{
				ToolBarButton btnViewItem = new ToolBarButton(info.getName());
				btnViewItem.setSclass("link");
				btnViewItem.setLabel(info.get_Translation("Name"));
				btnViewItem.setIconSclass((Util.isEmpty(info.getImageURL()) ? "z-icon-Info" : ThemeUtils.getIconSclass(info.getImageURL())));
				btnViewItem.setTooltiptext(btnViewItem.getLabel());
				btnViewItem.addEventListener(Events.ON_CLICK, this);
				vbox.appendChild(btnViewItem);
			}
		}

		return vbox;
	}
        
    public void onEvent(Event event)
    {
        Component comp = event.getTarget();
        String eventName = event.getName();
        
        if(eventName.equals(Events.ON_CLICK))
        {
            if(comp instanceof ToolBarButton)
            {
            	ToolBarButton btn = (ToolBarButton) comp;
        		String actionCommand = btn.getName();
        		int WindowNo = 0;
        		
        		if (actionCommand.equals("InfoProduct") && AEnv.canAccessInfo("PRODUCT"))
        		{
        			InfoPanel.showProduct(WindowNo);
        		}
        		else if (actionCommand.equals("InfoBPartner") && AEnv.canAccessInfo("BPARTNER"))
        		{
        			InfoPanel.showBPartner(WindowNo);
        		}
        		else if (actionCommand.equals("InfoAsset") && AEnv.canAccessInfo("ASSET"))
        		{
        			InfoPanel.showAsset(WindowNo);
        		}
        		else if (actionCommand.equals("InfoAccount") && 
        				  MRole.getDefault().isShowAcct() &&
        				  AEnv.canAccessInfo("ACCOUNT"))
        		{
        			new org.adempiere.webui.acct.WAcctViewer();
        		}
        		else if (actionCommand.equals("InfoOrder") && AEnv.canAccessInfo("ORDER"))
        		{
        			InfoPanel.showOrder(WindowNo, "");
        		}
        		else if (actionCommand.equals("InfoInvoice") && AEnv.canAccessInfo("INVOICE"))
        		{
        			InfoPanel.showInvoice(WindowNo, "");
        		}
        		else if (actionCommand.equals("InfoInOut") && AEnv.canAccessInfo("INOUT"))
        		{
        			InfoPanel.showInOut(WindowNo, "");
        		}
        		else if (actionCommand.equals("InfoPayment") && AEnv.canAccessInfo("PAYMENT"))
        		{
        			InfoPanel.showPayment(WindowNo, "");
        		}
        		else if (actionCommand.equals("InfoCashLine") && AEnv.canAccessInfo("CASHJOURNAL"))
        		{
        			InfoPanel.showCashLine(WindowNo, "");
        		}
        		else if (actionCommand.equals("InfoAssignment") && AEnv.canAccessInfo("RESOURCE"))
        		{
        			InfoPanel.showAssignment(WindowNo, "");
        		}
        		else
				{
					int infoWindowID = new Query(Env.getCtx(), MInfoWindow.Table_Name, "Name = ?", null)
							.setParameters(actionCommand).setOnlyActiveRecords(true).firstIdOnly();

					if (infoWindowID <= 0)
						return;

					SessionManager.getAppDesktop().openInfo(infoWindowID);
				}
            }
        }
	}
}
