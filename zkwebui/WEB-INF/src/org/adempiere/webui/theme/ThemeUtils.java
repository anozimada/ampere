/******************************************************************************
 * Copyright (C) 2008 Low Heng Sin  All Rights Reserved.                      *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.webui.theme;

import java.util.Properties;

import org.adempiere.webui.AdempiereWebUI;
import org.adempiere.webui.component.Label;
import org.compiere.model.MSysConfig;
import org.compiere.util.Env;
import org.compiere.util.Util;
import org.zkoss.web.fn.ServletFns;
import org.zkoss.web.theme.StandardTheme.ThemeOrigin;
import org.zkoss.zhtml.impl.AbstractTag;
import org.zkoss.zk.au.out.AuScript;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Div;
import org.zkoss.zul.theme.Themes;

/**
 * 
 * @author Low Heng Sin
 * @author Michael Mckay - modified for the ZK7  theme system
 *
 */
public final class ThemeUtils {

	// theme key in MSysConfig
	public static final String ZK_THEME = "ZK_THEME";
	// default theme
	public static final String ZK_THEME_DEFAULT = "default";
	// theme resource url prefix
	public static final String THEME_PATH_PREFIX = "/theme/";

	// css for login window and box
	public static final String LOGIN_WINDOW_CLASS = "login-window";
	public static final String LOGIN_BOX_CLASS = "login-box";
	public static final String LOGIN_BOX_HEADER_CLASS = "login-box-header";
	public static final String LOGIN_BOX_HEADER_TXT_CLASS = "login-box-header-txt";
	public static final String LOGIN_BOX_HEADER_LOGO_CLASS = "login-box-header-logo";
	public static final String LOGIN_BOX_BODY_CLASS = "login-box-body";
	public static final String LOGIN_BOX_FOOTER_CLASS = "login-box-footer";
	public static final String LOGIN_BOX_FOOTER_PANEL_CLASS = "login-box-footer-pnl";

	// css for login control
	public static final String LOGIN_BUTTON_CLASS = "login-btn";
	public static final String LOGIN_LABEL_CLASS = "login-label";
	public static final String LOGIN_FIELD_CLASS = "login-field";

	// optional top, bottom, left, right content for the login page
	public static final String LOGIN_NORTH_PANEL_CLASS = "login-north-panel";
	public static final String LOGIN_SOUTH_PANEL_CLASS = "login-south-panel";
	public static final String LOGIN_WEST_PANEL_CLASS = "login-west-panel";
	public static final String LOGIN_EAST_PANEL_CLASS = "login-east-panel";

	public static final String LOGIN_TOP_PANEL_ZUL = "/login-top.zul";
	public static final String LOGIN_BOTTOM_PANEL_ZUL = "/login-bottom.zul";
	public static final String LOGIN_LEFT_PANEL_ZUL = "/login-left.zul";
	public static final String LOGIN_RIGHT_PANEL_ZUL = "/login-right.zul";

	// logo
	public static final String LOGIN_LOGO_IMAGE = "login-logo.png";
	public static final String HEADER_LOGO_IMAGE = "header-logo.png";
	public static final String BROWSER_ICON_IMAGE = "icon.png";
	// Menu image icon
	public static final String MENU_WINDOW_IMAGE = "mWindow.png";
	public static final String MENU_PROCESS_IMAGE = "mProcess.png";
	public static final String MENU_REPORT_IMAGE = "mReport.png";
	public static final String MENU_WORKFLOW_IMAGE = "mWorkFlow.png";
	public static final String MENU_INFO_IMAGE = "mInfo.png";
	// General
	public static final String WEBUI_FOLDER_IMAGE = "/webui/images/";
	
	/**
	 * @param layout
	 */
	public static void sendDeferLayoutEvent(Borderlayout layout, int timeout) {
		StringBuilder content = new StringBuilder();		
		content.append("ad_deferRenderBorderLayout('")
			   .append(layout.getUuid())
			   .append("',").append(timeout).append(");");
		
		AuScript as = new AuScript(null, content.toString());
		Clients.response("deferRenderBorderLayout", as);		
	}
	
	/**
	 * 
	 * @param cls
	 * @param target
	 */
	public static void addSclass(String cls,  Object target) {
		String sclass = getSclass(target);
		if (!hasSclass(cls, target)) {
			sclass = (sclass == null ? cls : sclass + " " + cls);
			setSclass(sclass, target);
		}
	}

	/**
	 * Removes the string cls from the Sclass if it is found.
	 * @param cls
	 * @param target
	 */
	public static void removeSclass(String cls,  Object target) {
		String sclass = " " + getSclass(target) + " ";
		if (hasSclass(cls, target)) {
			sclass = sclass.replace(cls, "").trim();
			setSclass(sclass, target);
		}
	}

	
	private static String getSclass(Object target) {
		String sclass = "";
		if (target instanceof HtmlBasedComponent) {
			sclass = ((HtmlBasedComponent) target).getSclass();
		}
		if (target instanceof AbstractTag) {
			sclass = ((AbstractTag) target).getSclass();
		}
		return sclass;
	}

	private static void setSclass(String sclass, Object target) {
		if (target instanceof HtmlBasedComponent) {
			((HtmlBasedComponent) target).setSclass(sclass);
		}
		if (target instanceof AbstractTag) {
			((AbstractTag) target).setSclass(sclass);
		}
	}

	
	/**
	 * 
	 * @param cls
	 * @param target
	 * @return boolean
	 */
	public static boolean hasSclass(String cls, Object target) {
		String sclass = getSclass(target);
		if (sclass == null)
			sclass = "";
		return cls == null
				|| ((" " + sclass + " ").indexOf(" " + cls + " ") > -1);
	}	
	
	public static Component makeRightAlign(Label label) {
		Div div = new Div();
		div.setStyle("text-align: right");
		div.appendChild(label);
		
		return div;
	}
	
	/**
	 * Set the current theme to the system default theme.
	 * @return True if a redirect has been requested.  Use this to stop loading during page setup.
	 */
	public static boolean setSystemDefaultTheme() {

		// Set the system default theme.  This will fall back to defaults
		// if the MTheme has no default themes.
		DefaultTheme dt = new DefaultTheme();		
		Execution exec = Executions.getCurrent();
		String themeName = dt.get_themeName();
		
		String currentTheme = Themes.getCurrentTheme();
		if (currentTheme.equals(themeName)) {
			//  No need to do anything
			return false;
		}

		Themes.setTheme(exec, themeName);
		Executions.sendRedirect(null);  // reload the current page
		return true;
	}

	/**
	 * Register this theme
	 */
	public static void register(String theme) {

		ThemeOrigin themeOriginEnum = ThemeOrigin.valueOf(DefaultTheme.ZK_DEFAULT_THEME_ORIGIN);
		
		Themes.register(theme, theme, 1000, themeOriginEnum);
	}
	
	/**
	 * Register all active themes
	 */
	public static void registerAllThemes(Properties ctx) {
		
		// Register the system default - needed in the case where there are no themes in
		// the AD_Theme table
		DefaultTheme dt = new DefaultTheme();
		dt.registerDefaultTheme();
		
		// Register additional themes here
		//Themes.register("themeName", "themeName", 1001, ThemeOrigin.FOLDER);
	}
	
	/**
	 *  Resolve image URLs from theme
	 */
	public static String resolveImageURL(String name)
	{
		return ServletFns.resolveThemeURL("~./images/" + name);
	}
	
	/**
	 * @return title text for the browser window
	 */
	public static String getBrowserTitle() {
	    String title = MSysConfig.getValue("ZK_BROWSER_TITLE", null, Env.getAD_Client_ID(Env.getCtx()));
	    
	    if (title != null)
	    {
	        return title;
	    }
		return MSysConfig.getValue("ZK_BROWSER_TITLE", AdempiereWebUI.APP_NAME);
	}
	
	/**
	 * @return url for browser icon
	 */
	public static String getBrowserIcon() {
		String def = ThemeUtils.resolveImageURL(ThemeUtils.BROWSER_ICON_IMAGE);
		return MSysConfig.getValue("ZK_BROWSER_ICON", def);
	}
	
	public static String getIconSclass(String imagePath) {
		String iconSclass = null;
		if (!Util.isEmpty(imagePath, true)) {
			// remove path and extension
			iconSclass = imagePath.substring(imagePath.lastIndexOf("/") + 1, imagePath.lastIndexOf("."));
			
			// remove prefix m
			if (iconSclass.length() > 2)
				iconSclass = iconSclass.startsWith("m") && Character.isUpperCase(iconSclass.charAt(1)) ? iconSclass.substring(1) : iconSclass;
			
			// remove image size
			iconSclass = iconSclass.replaceAll("(\\d\\d)$", "");
			
			iconSclass = "z-icon-" + iconSclass;
		}
		
		return iconSclass;
	}
}
