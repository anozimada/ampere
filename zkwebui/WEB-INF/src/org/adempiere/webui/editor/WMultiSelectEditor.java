package org.adempiere.webui.editor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.webui.ValuePreference;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.MultiSelectBox;
import org.adempiere.webui.event.ContextMenuEvent;
import org.adempiere.webui.event.ContextMenuListener;
import org.adempiere.webui.window.WFieldRecordInfo;
import org.compiere.model.GridField;
import org.compiere.model.Lookup;
import org.compiere.model.MRole;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.compiere.util.ValueNamePair;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.event.ListDataEvent;
import org.zkoss.zul.event.ListDataListener;

public class WMultiSelectEditor extends WEditor implements ContextMenuListener, ListDataListener, PropertyChangeListener
{

	private static final String[]	LISTENER_EVENTS	= { Events.ON_CLICK, Events.ON_CHANGE, Events.ON_OK };

	private Lookup					lookup;
	private Object					oldValue;
	protected WEditorPopupMenu		popupMenu;

	public WMultiSelectEditor(GridField gridField)
	{
		super(new MultiSelectBox(), gridField);
		((MultiSelectBox) getComponent()).editor = this;
		lookup = gridField.getLookup();
		init();
	}

	private void init()
	{
		getComponent().getPopupComponent().addEventListener(Events.ON_OPEN, this);
		getComponent().getTextbox().addEventListener(Events.ON_CLICK, this);

		if (lookup != null)
		{
			refreshList();
		}

		popupMenu = new WEditorPopupMenu(false, true, isShowPreference());
		popupMenu.addMenuListener(this);
		addChangeLogMenu(popupMenu);
		getComponent().setContext(popupMenu);

	} // init

	public MultiSelectBox getComponent()
	{
		return (MultiSelectBox) super.getComponent();
	}

	public WEditorPopupMenu getPopupMenu()
	{
		return popupMenu;
	}

	private void refreshList()
	{
		if (lookup != null)
		{
			lookup.setMandatory(true);
			lookup.refresh();

			setComponentTextValue(Msg.getMsg(Env.getCtx(), "PleaseSelect"));

			// Clear the component
			for (Checkbox cbx : getComponent().getCheckboxList())
			{
				cbx.detach();
			}
			getComponent().getCheckboxList().clear();

			// fill the component
			for (int i = 0; i < lookup.getSize(); i++)
			{
				Checkbox cbx = null;
				Object pair = lookup.getElementAt(i);
				if (pair instanceof KeyNamePair)
				{
					KeyNamePair knp = (KeyNamePair) pair;
					cbx = new Checkbox();
					cbx.setText(knp.getName());
					cbx.setAttribute(MultiSelectBox.ATTRIBUTE_MULTI_SELECT, knp.getKey());
				}
				else if (pair instanceof ValueNamePair)
				{
					ValueNamePair vnp = (ValueNamePair) pair;
					cbx = new Checkbox();
					cbx.setText(vnp.getName());
					cbx.setAttribute(MultiSelectBox.ATTRIBUTE_MULTI_SELECT, vnp.getValue());
				}
				getComponent().getCheckboxList().add(cbx);
				getComponent().getVBox().appendChild(cbx);
			}
		}
	} // refreshList

	/**
	 * @return boolean
	 */
	protected boolean isShowPreference()
	{
		return MRole.getDefault().isShowPreference() && gridField != null && !gridField.isEncrypted()
				&& !gridField.isEncryptedColumn();
	}

	/**
	 * @param popupMenu
	 */
	protected void addChangeLogMenu(WEditorPopupMenu popupMenu)
	{
		if (popupMenu != null && gridField != null && gridField.getGridTab() != null)
		{
			WFieldRecordInfo.addMenu(popupMenu);
		}
	}

	private void setComponentTextValue(String textValue)
	{
		getComponent().getTextbox().setValue(textValue.trim());
	} // setComponentTextValue

	@Override
	public void onEvent(Event event) throws Exception
	{
		if (event.getName().equals(Events.ON_CLICK))
		{
			if (event.getTarget() == getComponent().getTextbox() && isReadWrite())
				getComponent().getPopupComponent().open(getComponent().getTextbox(), "after_start");
		}
		else if (event.getName().equals(Events.ON_OPEN) && event.getTarget() == getComponent().getPopupComponent())
		{
			OpenEvent oe = (OpenEvent) event;
			if (!oe.isOpen())
			{
				isValueChange(getValue(), true);
				setComponentTextValue(getPrintableValue(oldValue));
			}
		}
	}// onEvent

	@Override
	public void setReadWrite(boolean readWrite)
	{
		getComponent().setEditable(readWrite);
	}

	@Override
	public boolean isReadWrite()
	{
		return getComponent().isEnabled();
	}

	@Override
	public void setValue(Object newValue)
	{
		if (newValue == null
				|| (DisplayType.MultiSelectTable == lookup.getDisplayType() && newValue instanceof Integer[]
						&& ((Integer[]) newValue).length == 0)
				|| (DisplayType.MultiSelectList == lookup.getDisplayType() && newValue instanceof String[]
						&& ((String[]) newValue).length == 0))
		{
			for (Checkbox cbx : getComponent().getCheckboxList())
			{
				if (cbx.isChecked())
					cbx.setChecked(false);
			}
			oldValue = null;
			setComponentTextValue(Msg.getMsg(Env.getCtx(), "PleaseSelect"));
			return;
		}

		boolean isValueSame = isValueChange(newValue, false);

		setComponentTextValue(getPrintableValue(oldValue));

		/**
		 * Fire value change event. For Example, Item X is selected and Deleting
		 * X item from the referenced table then fires value change event for
		 * the field that containing window is opened.
		 */
		if (isValueSame)
			isValueChange(getValue(), true);

	}// setValue

	@Override
	public Object getValue()
	{
		return getSortedSelectedItems(true);
	}

	@Override
	public String getDisplay()
	{
		return getComponent().getTextbox().getText();
	}

	public boolean isValueChange(Object newValue, boolean isFireChangeEvent)
	{
		if (DisplayType.MultiSelectTable == lookup.getDisplayType())
		{
			Integer values[] = (Integer[]) newValue;
			if (Arrays.equals(getSortedSelectedItems(false), values))
				return true;
			if (isFireChangeEvent)
			{
				ValueChangeEvent changeValue = new ValueChangeEvent(this, getColumnName(), oldValue, values);
				super.fireValueChange(changeValue);
			}
			oldValue = values;
		}
		else
		{
			String values[] = (String[]) newValue;
			if (Arrays.equals(getSortedSelectedItems(false), values))
				return true;
			if (isFireChangeEvent)
			{
				ValueChangeEvent changeValue = new ValueChangeEvent(this, getColumnName(), oldValue, values);
				super.fireValueChange(changeValue);
			}
			oldValue = values;
		}

		return false;
	} // isValueChange

	public String getPrintableValue(Object values)
	{
		StringBuffer sb = new StringBuffer();

		if (values == null)
			return Msg.getMsg(Env.getCtx(), "PleaseSelect");

		for (Checkbox cbx : getComponent().getCheckboxList())
		{
			String val = cbx.getAttribute(MultiSelectBox.ATTRIBUTE_MULTI_SELECT).toString();
			if (DisplayType.MultiSelectTable == lookup.getDisplayType())
			{
				for (Integer value : (Integer[]) values)
				{
					if (value.compareTo(Integer.valueOf(val)) == 0)
					{
						cbx.setChecked(true);
						sb.append(cbx.getLabel()).append("; ");
						break;
					}
					cbx.setChecked(false);
				}
			}
			else
			{
				for (String value : (String[]) values)
				{
					if (value.equals(val))
					{
						cbx.setChecked(true);
						sb.append(cbx.getLabel()).append("; ");
						break;
					}
					cbx.setChecked(false);
				}
			}
		}

		return sb.toString();
	} // getPrintableValue

	/**
	 * The sorted array items for new/old value
	 * 
	 * @param isNewValue - If true then return to new value list else old value
	 *            list
	 * @return return the sorted array items
	 */
	private Object[] getSortedSelectedItems(boolean isNewValue)
	{
		Object obj[] = null;
		List<String> itemsStr = new ArrayList<String>();
		List<Integer> itemsInt = new ArrayList<Integer>();
		if (isNewValue)
		{
			for (Checkbox cbx : getComponent().getCheckboxList())
			{
				if (cbx.isChecked())
				{
					String val = cbx.getAttribute(MultiSelectBox.ATTRIBUTE_MULTI_SELECT).toString();
					if (DisplayType.MultiSelectTable == lookup.getDisplayType())
						itemsInt.add(Integer.valueOf(val));
					else
						itemsStr.add(val);
				}
			}
		}
		else if (oldValue != null)
		{
			if (DisplayType.MultiSelectTable == lookup.getDisplayType() && oldValue instanceof Integer[])
			{
				Integer values[] = (Integer[]) oldValue;
				for (Integer value : values)
					itemsInt.add(value);
			}
			else
			{
				String values[] = (String[]) oldValue;
				for (String value : values)
					itemsStr.add(value);
			}
		}

		if (itemsInt != null && itemsInt.size() > 0)
		{
			Collections.sort(itemsInt);
			obj = itemsInt.toArray(new Integer[itemsInt.size()]);
		}
		else if (itemsStr != null && itemsStr.size() > 0)
		{
			Collections.sort(itemsStr);
			obj = itemsStr.toArray(new String[itemsStr.size()]);
		}
		return obj;
	} // getSortedSelectedItems

	public String[] getEvents()
	{
		return LISTENER_EVENTS;
	}

	@Override
	public void onMenu(ContextMenuEvent evt)
	{
		if (WEditorPopupMenu.REQUERY_EVENT.equals(evt.getContextEvent()))
		{
			actionRefresh();
		}
		else if (WEditorPopupMenu.PREFERENCE_EVENT.equals(evt.getContextEvent()))
		{
			if (isShowPreference())
				ValuePreference.start(this.getGridField(), Util.convertArrayToStringForDB(getValue()));
		}
		else if (WEditorPopupMenu.CHANGE_LOG_EVENT.equals(evt.getContextEvent()))
		{
			WFieldRecordInfo.start(gridField);
		}
	}// onMenu

	public void actionRefresh()
	{
		refreshList();

		if (oldValue != null)
			setValue(oldValue);
	} // actionRefresh

	public void propertyChange(PropertyChangeEvent evt)
	{
		if (GridField.PROPERTY.equals(evt.getPropertyName()))
		{
			setValue(evt.getNewValue());
		}
	}

	@Override
	public void onChange(ListDataEvent event)
	{
		actionRefresh();
	}

	public void dynamicDisplay()
	{
		if ((lookup != null) && (!lookup.isValidated() || !lookup.isLoaded()
				|| (isReadWrite() && lookup.getSize() != getComponent().getCheckboxList().size())))
		{
			this.actionRefresh();
		}
	}
}
