/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2007 Adempiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *
 * Copyright (C) 2007 Low Heng Sin hengsin@avantz.com
 * _____________________________________________
 *****************************************************************************/
package org.adempiere.webui.window;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.StringWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.exceptions.DBException;
import org.adempiere.pdf.Document;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.DesktopRunnable;
import org.adempiere.webui.apps.WExecute;
import org.adempiere.webui.apps.WReport;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Tab;
import org.adempiere.webui.component.Tabpanel;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.event.DialogEvents;
import org.adempiere.webui.event.DrillEvent;
import org.adempiere.webui.event.ExecuteEvent;
import org.adempiere.webui.event.ZoomEvent;
import org.adempiere.webui.panel.StatusBarPanel;
import org.adempiere.webui.report.HTMLExtension;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.GridField;
import org.compiere.model.MArchive;
import org.compiere.model.MClient;
import org.compiere.model.MColumn;
import org.compiere.model.MMailText;
import org.compiere.model.MQuery;
import org.compiere.model.MRefTable;
import org.compiere.model.MRole;
import org.compiere.model.MSysConfig;
import org.compiere.model.MTable;
import org.compiere.model.MUser;
import org.compiere.model.PO;
import org.compiere.model.PrintInfo;
import org.compiere.model.X_AD_Reference;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.ImpExpUtil;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.zkoss.io.Files;
import org.zkoss.json.JSONArray;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Div;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Fileupload;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.North;
import org.zkoss.zul.Separator;
import org.zkoss.zul.South;
import org.zkoss.zul.Toolbar;
import org.zkoss.zul.Toolbarbutton;
import org.zkoss.zul.Vbox;


/**
 *	Print View Frame
 *
 * 	@author 	Jorg Janke
 * 	@version 	$Id: Viewer.java,v 1.2 2006/07/30 00:51:28 jjanke Exp $
 * globalqss: integrate phib contribution from 
 *   http://sourceforge.net/tracker/index.php?func=detail&aid=1566335&group_id=176962&atid=879334
 * globalqss: integrate Teo Sarca bug fixing
 * Colin Rooney 2007/03/20 RFE#1670185 & BUG#1684142
 *                         Extend security to Info queries
 *
 * @author Teo Sarca, SC ARHIPAC SERVICE SRL
 * 				<li>FR [ 1762466 ] Add "Window" menu to report viewer.
 * 				<li>FR [ 1894640 ] Report Engine: Excel Export support
 * 
 * @author Low Heng Sin
 */
public class ZkReportViewer extends Window implements EventListener<Event> {
	
	private static final long serialVersionUID = 4640088641140012438L;

	// Get Find Tab Info
	public static String		SQL_GET_TABINFO_FROM_TABLEID	= "SELECT t.AD_Tab_ID	FROM AD_Tab t											"
																	+ "INNER JOIN AD_Window w ON (t.AD_Window_ID=w.AD_Window_ID)				"
																	+ "INNER JOIN AD_Table tt ON (t.AD_Table_ID=tt.AD_Table_ID) 				"
																	+ "WHERE tt.AD_Table_ID=? 													"
																	+ "ORDER BY w.IsDefault DESC, t.SeqNo, ABS (tt.AD_Window_ID-t.AD_Window_ID)	";

	/** Window No					*/
	private int                 m_WindowNo;
	/**	Print Context				*/
	private Properties			m_ctx;
	/**	Setting Values				*/
	private boolean				m_setting = false;
	/**	Report Engine				*/
	private ReportEngine 		m_reportEngine;
	/** Table ID					*/
	private int					m_AD_Table_ID = 0;
	private boolean				m_isCanExport;
	private boolean 				m_isAllowHTMLView;
	private boolean 				m_isAllowXLSView;

//	private static final Allow_HTML_View
	
	
	private MQuery 		m_ddQ = null;
	private MQuery 		m_daQ = null;
	private Menuitem 	m_ddM = null;
	private Menuitem 	m_daM = null;

	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(ZkReportViewer.class);

	//
	private StatusBarPanel statusBar = new StatusBarPanel();
	private Toolbar toolBar = new Toolbar();
	private Toolbarbutton bSendMail = new Toolbarbutton();
	private Toolbarbutton bArchive = new Toolbarbutton();
	private Toolbarbutton bCustomize = new Toolbarbutton();
	private Toolbarbutton bFind = new Toolbarbutton();
	private Toolbarbutton bExport = new Toolbarbutton();
	private Toolbarbutton bLoad = new Toolbarbutton();
	private Listbox comboReport = new Listbox();
	private Label labelDrill = new Label();
	private Listbox comboDrill = new Listbox();
	private Listbox previewType = new Listbox();
	
	private Toolbarbutton bRefresh = new Toolbarbutton();
	private Iframe iframe;
	
	private Window winExportFile = null;
	private ConfirmPanel confirmPanel = new ConfirmPanel(true);
	private Listbox cboType = new Listbox();
	private Checkbox summary = new Checkbox();
	
	private String m_title = null;
	private String m_source = null;  // from Drill Through
	private boolean hasSystemRole = true;
	
	private Desktop m_desktop = null;
	private String m_contextPath;
	private String m_uuid;
	
	/**
	 * 	Static Layout
	 * 	@throws Exception
	 */
	public ZkReportViewer(ReportEngine re, String title, Desktop desktop) {		
		super();

		log.info("");
		m_desktop = desktop;
		m_title = title;
		m_source = re.getPrintInfo().getDrillSource();
//		SessionManager.activateDesktop(m_desktop);
//		m_WindowNo = SessionManager.getAppDesktop().registerWindow(this);
		m_WindowNo = SessionManager.getAppDesktop(m_desktop).registerWindow(this);
		m_contextPath = Executions.getCurrent().getContextPath();
		m_uuid = this.getUuid();
		
//		SessionManager.releaseDesktop(m_desktop);
		Env.setContext(re.getCtx(), m_WindowNo, "_WinInfo_IsReportViewer", "Y");
		m_reportEngine = re;
		m_AD_Table_ID = re.getPrintFormat().getAD_Table_ID();
		if (!MRole.getDefault().isCanReport(m_AD_Table_ID))
		{
			SessionManager.activateDesktop(m_desktop);
			FDialog.error(m_WindowNo, this, "AccessCannotReport", m_reportEngine.getName());
			this.onClose();
			SessionManager.releaseDesktop(m_desktop);
		}
		m_isCanExport = MRole.getDefault().isCanExport(m_AD_Table_ID);
		MRole roleCurrent = MRole.get(Env.getCtx(), Env.getAD_Role_ID(Env.getCtx()));
		m_isAllowHTMLView = roleCurrent.isAllow_HTML_View();	
		m_isAllowXLSView = roleCurrent.isAllow_XLS_View();		

		try
		{
			m_ctx = m_reportEngine.getCtx();
			init();
			dynInit();
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
			SessionManager.activateDesktop(m_desktop);
			FDialog.error(m_WindowNo, this, "LoadError", e.getLocalizedMessage());
			SessionManager.releaseDesktop(m_desktop);
			this.onClose();
		}
	}

	private void init() {
		Borderlayout layout = new Borderlayout();
		layout.setStyle("position: absolute; height: 99%; width: 99%");
		this.appendChild(layout);
		this.setStyle("width: 100%; height: 100%; position: absolute");

		ZKUpdateUtil.setHeight(toolBar, "40px");
		
		previewType.setMold("select");
		previewType.appendItem("PDF", "PDF");
		if (m_isAllowHTMLView) {
			previewType.appendItem("HTML", "HTML");
		}
		if (m_isAllowXLSView) {
			previewType.appendItem("CSV", "CSV");
			previewType.appendItem("XLSX", "XLSX");
		}
		toolBar.appendChild(previewType);		
		previewType.addEventListener(Events.ON_SELECT, this);
		toolBar.appendChild(new Separator("vertical"));
		
		//set default type
		String type = m_reportEngine.getPrintFormat().isForm()
				// a42niem - provide explicit default and check on client/org specifics
				? MSysConfig.getValue("ZK_REPORT_FORM_OUTPUT_TYPE","PDF",Env.getAD_Client_ID(m_ctx),Env.getAD_Org_ID(m_ctx))
				: MSysConfig.getValue("ZK_REPORT_TABLE_OUTPUT_TYPE","PDF",Env.getAD_Client_ID(m_ctx),Env.getAD_Org_ID(m_ctx));
				
		if (("CSV".equals(type)  && !m_isAllowXLSView) ||
				("XLSX".equals(type)  && !m_isAllowXLSView)) {
				type = "HTML";
		}
		if ("HTML".equals(type)  && !m_isAllowHTMLView) {
			type = "PDF";
		}
		
		if ("PDF".equals(type))
			previewType.setValue(type);
		else if ("CSV".equals(type))
			previewType.setValue(type);
		else if ("HTML".equals(type))
			previewType.setValue(type);
		else if ("XLSX".equals(type))
			previewType.setValue(type);
		else
			// XXX - provide hint if unexpected value
			previewType.setSelectedIndex(0); // fallback to PDF
		
		if(m_reportEngine.getReportType()!=null)
		{
			if (m_reportEngine.getReportType().equals("P"))
			{
				previewType.setValue("PDF");
			}
			else if (m_reportEngine.getReportType().equals("C"))
			{
				previewType.setValue("CSV");
			}
			else if (m_reportEngine.getReportType().equals("H"))
			{
				previewType.setValue("HTML");
			}
			else if (m_reportEngine.getReportType().equals("XX"))
			{
				previewType.setValue("XLSX");
			}
		}
			
		
		labelDrill.setValue(Msg.getMsg(Env.getCtx(), "Drill") + ": ");
		toolBar.appendChild(labelDrill);
		toolBar.appendChild(new Separator("vertical"));
		
		comboDrill.setMold("select");
		comboDrill.setTooltiptext(Msg.getMsg(Env.getCtx(), "Drill"));
		toolBar.appendChild(comboDrill);
		
		toolBar.appendChild(new Separator("vertical"));
		
		comboReport.setMold("select");
		comboReport.setTooltiptext(Msg.translate(Env.getCtx(), "AD_PrintFormat_ID"));
		toolBar.appendChild(comboReport);
		toolBar.appendChild(new Separator("vertical"));
		
		summary.setText(Msg.getMsg(Env.getCtx(), "Summary"));
		//summary.setTop("9px");
		summary.setStyle("position: relative;");
		toolBar.appendChild(summary);
		toolBar.appendChild(new Separator("vertical"));
		
		bCustomize.setIconSclass("z-icon-Preference");
		bCustomize.setTooltiptext(Msg.getMsg(Env.getCtx(), "PrintCustomize"));
		toolBar.appendChild(bCustomize);
		bCustomize.addEventListener(Events.ON_CLICK, this);
		toolBar.appendChild(new Separator("vertical"));
		
		bFind.setIconSclass("z-icon-Find");
		bFind.setTooltiptext(Msg.getMsg(Env.getCtx(), "Find"));
		toolBar.appendChild(bFind);
		bFind.addEventListener(Events.ON_CLICK, this);
		if (getAD_Tab_ID(m_reportEngine.getPrintFormat().getAD_Table_ID()) <= 0)
			bFind.setVisible(false);

		toolBar.appendChild(new Separator("vertical"));
		
		bSendMail.setIconSclass("z-icon-SendMail");
		bSendMail.setTooltiptext(Msg.getMsg(Env.getCtx(), "SendMail"));
		toolBar.appendChild(bSendMail);
		bSendMail.addEventListener(Events.ON_CLICK, this);
		toolBar.appendChild(new Separator("vertical"));
		
		bArchive.setIconSclass("z-icon-Archive");
		bArchive.setTooltiptext(Msg.getMsg(Env.getCtx(), "Archive"));
		toolBar.appendChild(bArchive);
		bArchive.addEventListener(Events.ON_CLICK, this);
		toolBar.appendChild(new Separator("vertical"));
		
		if (m_isCanExport)
		{
			bExport.setIconSclass("z-icon-Export");
			bExport.setTooltiptext(Msg.getMsg(Env.getCtx(), "Export"));
			toolBar.appendChild(bExport);
			bExport.addEventListener(Events.ON_CLICK, this);
			toolBar.appendChild(new Separator("vertical"));
		}
		
		toolBar.appendChild(new Separator("vertical"));
		
		bRefresh.setIconSclass("z-icon-Refresh");
		bRefresh.setTooltiptext(Msg.getMsg(Env.getCtx(), "Refresh"));
		toolBar.appendChild(bRefresh);
		bRefresh.addEventListener(Events.ON_CLICK, this);
		toolBar.appendChild(new Separator("vertical"));

		if (hasSystemRole) {
			bLoad.setIconSclass("z-icon-Import");
			bLoad.setTooltiptext("Load New Report Definition");
			toolBar.appendChild(bLoad);
			bLoad.addEventListener(Events.ON_CLICK, this);
			toolBar.appendChild(new Separator("vertical"));
		}
		
		North north = new North();
		layout.appendChild(north);
		north.appendChild(toolBar);

		Center center = new Center();
		ZKUpdateUtil.setVflex(center, "flex");
		layout.appendChild(center);
		iframe = new Iframe();
		iframe.setId("reportFrame");
		ZKUpdateUtil.setHeight(iframe, "100%");
		ZKUpdateUtil.setWidth(iframe, "100%");
		iframe.addEventListener(Events.ON_CLICK, this);
		iframe.addEventListener(Events.ON_RIGHT_CLICK, this);
		center.appendChild(iframe);

		South south = new South();
		layout.appendChild(south);
		south.appendChild(statusBar);
		
		startReportRender();

		this.setBorder("normal");
		
		this.addEventListener(ZoomEvent.ON_ZOOM, new EventListener<Event>() {
			
			public void onEvent(Event event) throws Exception {
				if (event instanceof ZoomEvent) {
					ZoomEvent ze = (ZoomEvent) event;
					if (ze.getData() != null && ze.getData() instanceof MQuery)
					{
						MQuery query = (MQuery)ze.getData();
						
						if (query.getColumnName(0) != null && query.getColumnName(0).equals("Record_ID"))
						{
							ReportEngine rptEngine = ((ZkReportViewer)ze.getTarget()).m_reportEngine;
							MTable mTable = (MTable)rptEngine.getPrintFormat().getAD_Table();
							//mTable.getColumn("AD_Table_ID")
							if (mTable.getTableName().endsWith("Fact_Acct") || mTable.getTableName().endsWith("TrialBalance"))
							{
								int table_id = 0, record_id = 0;
								PreparedStatement pstmt = null;
								ResultSet rs = null;
								String sql = "SELECT AD_Table_ID, Record_ID FROM Fact_Acct WHERE Fact_Acct_ID = " + query.getInfoDisplay(0);
								try
								{
									pstmt = DB.prepareStatement(sql, null);
									rs = pstmt.executeQuery();
									if (rs.next())
									{
										table_id = rs.getInt(1);
										record_id = rs.getInt(2);
									}
								}
								catch (Exception e)
								{
									log.log(Level.SEVERE, sql, e);
								}
								finally
								{
									DB.close(rs, pstmt);
									rs = null;
									pstmt = null;
								}
								query = new MQuery(table_id);
								query.addRestriction(query.getTableName().trim() + "_ID = " + record_id);
							}
						} else {
							//it's not a direct table	
							if (MTable.get(m_ctx, query.getTableName()) == null) {
								query = getRevisedQuery(query);
								
							}
						}
						AEnv.zoom(query);				
					}
				}
				
			}
		});
		
		this.addEventListener(ExecuteEvent.ON_EXECUTE, new EventListener<Event>() {
			public void onEvent(Event event) throws Exception {
				if (event instanceof ExecuteEvent) {
					ExecuteEvent ee = (ExecuteEvent) event;
					JSONArray data = (JSONArray) ee.getData();
					executeAction((String)data.get(0), (String) data.get(1), Integer.parseInt((String) data.get(2)), Integer.parseInt((String) data.get(3)));
				}
			}
		});
		
		this.addEventListener(DrillEvent.ON_DRILL_ACROSS, new EventListener<Event>() {
			
			public void onEvent(Event event) throws Exception {
				if (event instanceof DrillEvent) {
					DrillEvent de = (DrillEvent) event;
					if (de.getData() != null && de.getData() instanceof MQuery) {
						MQuery query = (MQuery) de.getData();
						Listitem item = comboDrill.getSelectedItem();
						if (item != null && item.getValue() != null && item.toString().trim().length() > 0)
						{
							query.setTableName(item.getValue().toString());
							executeDrill(query, event.getTarget());
						}
					}
				}
				
			}
		});
		
		this.addEventListener(DrillEvent.ON_DRILL_DOWN, new EventListener<Event>() {
			
			public void onEvent(Event event) throws Exception {
				if (event instanceof DrillEvent) {
					DrillEvent de = (DrillEvent) event;
					if (de.getData() != null && de.getData() instanceof MQuery) {
						MQuery query = (MQuery) de.getData();
						executeDrill(query, event.getTarget());
					}
				}
				
			}
		});
	}

	private void startReportRender()
	{
		ZkReportViewer reportViewer = this;
		Runnable runnable = new Runnable() {

			@Override
			public void run()
			{
				try
				{
					renderReport(reportViewer.getUuid());
				}
				catch (Exception e)
				{
					throw new AdempiereException("Failed to render report", e);
				}
			}
		};
		DesktopRunnable desktopRunnable = new DesktopRunnable(runnable, this.getDesktop());
		AEnv.executeAsyncDesktopTask(desktopRunnable);
	}

	private void renderReport(String parentID) throws Exception {
		
		AMedia media = null;
		Listitem selected = previewType.getSelectedItem();

		String prefix = makePrefix(m_reportEngine.getName());
		String path = System.getProperty("java.io.tmpdir");
		if (log.isLoggable(Level.FINE))
			log.log(Level.FINE, "Path=" + path + " Prefix=" + prefix);

		if (selected == null || "PDF".equals(selected.getValue())) {
			File file = File.createTempFile(prefix, ".pdf", new File(path));
			m_reportEngine.createPDF(file);
			media = new AMedia(prefix, "pdf", "application/pdf", file, true);
		} else if ("HTML".equals(previewType.getSelectedItem().getValue())) {
			File file = File.createTempFile(prefix, ".html", new File(path));
			m_reportEngine.createHTML(file, false, AEnv.getLanguage(Env.getCtx()), new HTMLExtension(m_contextPath, "rp", parentID));
			media = new AMedia(prefix, "html", "text/html", file, false);
		} else if ("XLSX".equals(previewType.getSelectedItem().getValue())) {
			File file = File.createTempFile(prefix, ".xlsx", new File(path));
			m_reportEngine.createXLSX(file, AEnv.getLanguage(Env.getCtx()));
			media = new AMedia(prefix, "xlsx", "application/vnd.ms-excel", file, true);
		} else if ("CSV".equals(previewType.getSelectedItem().getValue()))
		{
			File file = File.createTempFile(prefix, ".csv", new File(path));
			m_reportEngine.createCSV(file, ',', AEnv.getLanguage(Env.getCtx()));
			media = new AMedia(prefix, "csv", "text/csv", file, true);
		}

		iframe.setContent(media);
	}

	private String makePrefix(String name) {
		StringBuffer prefix = new StringBuffer();
		char[] nameArray = name.toCharArray();
		for (char ch : nameArray) {
			if (Character.isLetterOrDigit(ch)) {
				prefix.append(ch);
			} else {
				prefix.append("_");
			}
		}
		return prefix.toString();
	}
	
	/**
	 * 	Dynamic Init
	 */
	private void dynInit()
	{
		summary.addActionListener(this);
		
		fillComboReport(m_reportEngine.getPrintFormat().get_ID());

		//	fill Drill Options (Name, TableName)
		comboDrill.appendItem("", null);
		String sql = "SELECT t.AD_Table_ID, t.TableName, e.PrintName, NULLIF(e.PO_PrintName,e.PrintName) "
			+ "FROM AD_Column c "
			+ " INNER JOIN AD_Column used ON (c.ColumnName=used.ColumnName)"
			+ " INNER JOIN AD_Table t ON (used.AD_Table_ID=t.AD_Table_ID AND t.IsView='N' AND t.AD_Table_ID <> c.AD_Table_ID)"
			+ " INNER JOIN AD_Column cKey ON (t.AD_Table_ID=cKey.AD_Table_ID AND cKey.IsKey='Y')"
			+ " INNER JOIN AD_Element e ON (cKey.ColumnName=e.ColumnName) "
			+ "WHERE c.AD_Table_ID=? AND c.IsKey='Y' "
			+ "ORDER BY 3";
		boolean trl = !Env.isBaseLanguage(Env.getCtx(), "AD_Element");
		if (trl)
			sql = "SELECT t.AD_Table_ID, t.TableName, et.PrintName, NULLIF(et.PO_PrintName,et.PrintName) "
				+ "FROM AD_Column c"
				+ " INNER JOIN AD_Column used ON (c.ColumnName=used.ColumnName)"
				+ " INNER JOIN AD_Table t ON (used.AD_Table_ID=t.AD_Table_ID AND t.IsView='N' AND t.AD_Table_ID <> c.AD_Table_ID)"
				+ " INNER JOIN AD_Column cKey ON (t.AD_Table_ID=cKey.AD_Table_ID AND cKey.IsKey='Y')"
				+ " INNER JOIN AD_Element e ON (cKey.ColumnName=e.ColumnName)"
				+ " INNER JOIN AD_Element_Trl et ON (e.AD_Element_ID=et.AD_Element_ID) "
				+ "WHERE c.AD_Table_ID=? AND c.IsKey='Y'"
				+ " AND et.AD_Language=? "
				+ "ORDER BY 3";
		try
		{
			PreparedStatement pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, m_reportEngine.getPrintFormat().getAD_Table_ID());
			if (trl)
				pstmt.setString(2, Env.getAD_Language(Env.getCtx()));
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				String tableName = rs.getString(2);
				String name = rs.getString(3);
				String poName = rs.getString(4);
				if (poName != null)
					name += "/" + poName;
				comboDrill.appendItem(name, tableName);
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		if (comboDrill.getItemCount() == 1)
		{
			labelDrill.setVisible(false);
			comboDrill.setVisible(false);
		}
		else
			comboDrill.addEventListener(Events.ON_SELECT, this);

		revalidate();
	}	//	dynInit
	
	/**
	 * 	Fill ComboBox comboReport (report options)
	 *  @param AD_PrintFormat_ID item to be selected
	 */
	private void fillComboReport(int AD_PrintFormat_ID)
	{
		comboReport.removeEventListener(Events.ON_SELECT, this);
		comboReport.getItems().clear();
		KeyNamePair selectValue = null;
		//	fill Report Options
		String sql = MRole.getDefault().addAccessSQL(
			"SELECT AD_PrintFormat_ID, Name, Description "
				+ "FROM AD_PrintFormat "
				+ "WHERE AD_Table_ID=? "
				//Added Lines by Armen
				+ "AND IsActive='Y' "
				//End of Added Lines
				+ "AND AD_Client_ID IN (0, ?) "
				+ "ORDER BY Name",
			"AD_PrintFormat", MRole.SQL_NOTQUALIFIED, MRole.SQL_RO);
		int AD_Table_ID = m_reportEngine.getPrintFormat().getAD_Table_ID();
		try
		{
			PreparedStatement pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, AD_Table_ID);
			pstmt.setInt(2, Env.getAD_Client_ID(m_ctx));
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				KeyNamePair pp = new KeyNamePair(rs.getInt(1), rs.getString(2));
				Listitem li = comboReport.appendItem(pp.getName(), pp.getKey());
				if (rs.getInt(1) == AD_PrintFormat_ID)
				{
					selectValue = pp;
					if(selectValue != null)
						comboReport.setSelectedItem(li);
				}
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		StringBuffer sb = new StringBuffer("** ").append(Msg.getMsg(Env.getCtx(), "NewReport")).append(" **");
		KeyNamePair pp = new KeyNamePair(-1, sb.toString());
		comboReport.appendItem(pp.getName(), pp.getKey());
		sb = new StringBuffer("** ").append(Msg.getMsg(m_ctx, "CopyReport")).append(" **");
		pp = new KeyNamePair(-2, sb.toString());
		comboReport.appendItem(pp.getName(), pp.getKey());
		comboReport.addEventListener(Events.ON_SELECT, this);
	}	//	fillComboReport

	/**
	 * 	Revalidate settings after change of environment
	 */
	private void revalidate()
	{
		//	Report Info
		//setTitle(Msg.getMsg(Env.getCtx(), "Report") + ": " + m_reportEngine.getName() + "  " + Env.getHeader(Env.getCtx(), 0));
		setTitle(null);
		StringBuffer sb = new StringBuffer ();
		sb.append(Msg.getMsg(Env.getCtx(), "DataCols")).append("=")
			.append(m_reportEngine.getColumnCount())
			.append(", ").append(Msg.getMsg(Env.getCtx(), "DataRows")).append("=")
			.append(m_reportEngine.getRowCount());
		statusBar.setStatusLine(sb.toString());
		//
	}	//	revalidate

	public void updateTitle()
	{
		Tabpanel parent = (Tabpanel) getParent();
		String tabLabel = "";
		if (m_source != null) {
			tabLabel =  m_source + " > ";
		}
		tabLabel = tabLabel + m_WindowNo + "-" + m_title;
		
		if (parent != null) {
			Tab tab = (Tab) parent.getTabbox().getTabs().getChildren().get(parent.getIndex());
			tab.setLabel(tabLabel);
		}

	}
	/**
	 * 	Dispose
	 */
	public void onClose()
	{
		SessionManager.getAppDesktop().unregisterWindow(m_WindowNo);
		m_reportEngine = null;
		m_ctx = null;
		super.onClose();
	}	//	dispose

	public void onEvent(Event event) throws Exception {
		
		if(event.getTarget().getId().equals(ConfirmPanel.A_CANCEL))
			winExportFile.onClose();
		else if(event.getTarget().getId().equals(ConfirmPanel.A_OK))			
			exportFile();
		else if(event.getName().equals(Events.ON_CLICK) || event.getName().equals(Events.ON_SELECT)) 
			actionPerformed(event);
		else if (event.getTarget() == summary) 
		{
			m_reportEngine.setSummary(summary.isSelected());
			cmd_report();
		}
	}

	/**************************************************************************
	 * 	Action Listener
	 * 	@param e event
	 */
	public void actionPerformed (Event e)
	{
		if (m_setting)
			return;
		if (e.getTarget() == comboReport)
			cmd_report();
		else if (e.getTarget() == bFind)
			cmd_find();
		else if (e.getTarget() == bExport)
			cmd_export();
		else if (e.getTarget() == previewType)
			cmd_render();
		else if (e.getTarget() == bSendMail)
			cmd_sendMail();
		else if (e.getTarget() == bArchive)
			cmd_archive();
		else if (e.getTarget() == bCustomize)
			cmd_customize();
		else if (e.getTarget() == bRefresh)
			cmd_report();
		else if (e.getTarget() == bLoad)
			importPrintFormat();
		//
		else if (e.getTarget() == m_ddM)
			cmd_window(m_ddQ);
		else if (e.getTarget() == m_daM)
			cmd_window(m_daQ);
	}	//	actionPerformed
	
	private void cmd_render() {
		try { 
			startReportRender();
		} catch (Exception e) {
			throw new AdempiereException("Failed to render report", e);
		}
	}

	/**
	 * 	Execute Drill to Query
	 * 	@param query query
	 *  @param component
	 */
	private void executeDrill (MQuery query, Component component)
	{
		int AD_Table_ID = MTable.getTable_ID(query.getTableName());
		if (AD_Table_ID == 0) {
			query = getRevisedQuery(query);
			if(query == null)
			{
				log.warning("Invalid Table Reference Value");
				return;
			}
			 AD_Table_ID = MTable.getTable_ID(query.getZoomTableName());
		}
		if (!MRole.getDefault().isCanReport(AD_Table_ID))
		{
			FDialog.error(m_WindowNo, this, "AccessCannotReport", query.getTableName());
			return;
		}
		if (AD_Table_ID != 0)
			new WReport (AD_Table_ID, query, component, 0, m_desktop);
		else
			log.warning("No Table found for " + query.getWhereClause(true));
	}	//	executeDrill
	
	private void executeAction(String columnName, String value, int AD_PrintFormatItem_ID, int AD_PInstance_ID)
	{
		log.fine("Execute command fired.");
		new WExecute(columnName, value, AD_PrintFormatItem_ID, AD_PInstance_ID, this, m_WindowNo, m_desktop);
	}
	
	private MQuery getRevisedQuery(MQuery query) {
		MTable table = (MTable) m_reportEngine.getPrintFormat().getAD_Table();
		String where = query.getWhereClause();
		String col = where.substring(1, where.indexOf("="));
		String val = where.substring(where.indexOf("=")+1, where.length()-1);

		MColumn column = table.getColumn(col);
		X_AD_Reference ref = (X_AD_Reference) column.getAD_Reference_Value();
		if (ref != null && ref.getValidationType() != null && ref.getValidationType().equals(X_AD_Reference.VALIDATIONTYPE_TableValidation)) {
			MRefTable refTable = new MRefTable(m_ctx, ref.getAD_Reference_ID(), null);
			MQuery query1 = new MQuery();
			query1.setZoomColumnName(MColumn.getColumnName(m_ctx, refTable.getAD_Key()));
			query1.setZoomValue(Integer.valueOf(val));
			query1.setZoomTableName(refTable.getAD_Table().getTableName());
			query1.setTableName(refTable.getAD_Table().getTableName());
			query1.setRecordCount(1);
			query1.addRestriction(query1.getZoomColumnName(),  MQuery.EQUAL, query1.getZoomValue());
			return query1;
		}
		return null;
	}

	
	/**
	 * 	Open Window
	 *	@param query query
	 */
	private void cmd_window (MQuery query)
	{
		if (query == null)
			return;
		AEnv.zoom(query);
	}	//	cmd_window
	
	/**
	 * 	Send Mail
	 */
	private void cmd_sendMail()
	{
		String to = "";
		MUser from = MUser.get(Env.getCtx(), Env.getAD_User_ID(Env.getCtx()));
		String subject = m_reportEngine.getName();
		String message = "";

		MMailText mailText = null;
		MPrintFormat format = m_reportEngine.getPrintFormat();
		if (format.getR_MailText_ID() > 0)
		{
			mailText = (MMailText) format.getR_MailText();
			PrintInfo printInfo = m_reportEngine.getPrintInfo();
			PO po = null;
			//
			mailText.setUser(from);
			//
			if (printInfo.getRecord_ID() > 0 && m_reportEngine.getPrintFormat().getAD_Table_ID() > 0) {
				po = MTable.get(m_reportEngine.getCtx(), m_reportEngine.getPrintFormat().getAD_Table_ID()).getPO(printInfo.getRecord_ID(), null);
				mailText.setPO(po);
			}
			//
			if (printInfo.getC_BPartner_ID() > 0)
				mailText.setBPartner(printInfo.getC_BPartner_ID());

			String mailHeader = mailText.getMailHeader();
			if (!Util.isEmpty(mailHeader, true))
				subject = mailHeader;
			message = mailText.getMailText(true);
		}

		File attachment = null;
		
		try
		{
			//attachment = File.createTempFile("mail", ".pdf");
			attachment = m_reportEngine.getPDF(null);
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, "", e);
		}

		boolean isHTML = false;
		if (mailText != null && mailText.isHtml())
			isHTML = true;
		WEMailDialog dlg = new WEMailDialog(this, Msg.getMsg(Env.getCtx(), "SendMail"), from, to, subject, message, attachment, isHTML);
	}	//	cmd_sendMail

	/**
	 * 	Archive Report directly
	 */
	private void cmd_archive ()
	{
		boolean success = false;
		byte[] data = Document.getPDFAsArray(m_reportEngine.getLayout().getPageable(false));	//	No Copy
		if (data != null)
		{
			MArchive archive = new MArchive (Env.getCtx(), m_reportEngine.getPrintInfo(), null);
			archive.setBinaryData(data);
			success = archive.save();
		}
		if (success)
			FDialog.info(m_WindowNo, this, "Archived");
		else
			FDialog.error(m_WindowNo, this, "ArchiveError");
	}	//	cmd_archive

	/**
	 * 	Export
	 */
	private void cmd_export()
	{		
		log.config("");
		if (!m_isCanExport)
		{
			FDialog.error(m_WindowNo, this, "AccessCannotExport", getTitle());
			return;
		}
		
		if(winExportFile == null)
		{
			winExportFile = new Window();
			winExportFile.setTitle(Msg.getMsg(Env.getCtx(), "Export") + ": " + getTitle());
			ZKUpdateUtil.setWidth(winExportFile, "450px");
			winExportFile.setClosable(true);
			winExportFile.setBorder("normal");
			winExportFile.setStyle("position:absolute");

			cboType.setMold("select");
			
			cboType.getItems().clear();			
			cboType.appendItem("ps" + " - " + Msg.getMsg(Env.getCtx(), "FilePS"), "ps");
			cboType.appendItem("xml" + " - " + Msg.getMsg(Env.getCtx(), "FileXML"), "xml");
			ListItem li = cboType.appendItem("pdf" + " - " + Msg.getMsg(Env.getCtx(), "FilePDF"), "pdf");
			cboType.appendItem("html" + " - " + Msg.getMsg(Env.getCtx(), "FileHTML"), "html");
			cboType.appendItem("txt" + " - " + Msg.getMsg(Env.getCtx(), "FileTXT"), "txt");
			cboType.appendItem("csv" + " - " + Msg.getMsg(Env.getCtx(), "FileCSV"), "csv");
			cboType.appendItem("xlsx" + " - " + Msg.getMsg(Env.getCtx(), "FileXLSX"), "xlsx");
			if (hasSystemRole) {
				cboType.appendItem("arxml" + " - Adempiere Report Definition", "arxml");
			}
			cboType.setSelectedItem(li);
			
			Hbox hb = new Hbox();
			Div div = new Div();
			div.setAlign("right");
			div.appendChild(new Label(Msg.getMsg(Env.getCtx(), "FilesOfType")));
			hb.appendChild(div);
			hb.appendChild(cboType);
			ZKUpdateUtil.setWidth(cboType, "100%");

			Vbox vb = new Vbox();
			ZKUpdateUtil.setWidth(vb, "390px");
			winExportFile.appendChild(vb);
			vb.appendChild(hb);
			vb.appendChild(confirmPanel);	
			confirmPanel.addActionListener(this);
		}
		
		winExportFile.setAttribute(Window.MODE_KEY, Window.MODE_HIGHLIGHTED);
		AEnv.showWindow(winExportFile);
		
	}	//	cmd_export
		
	private void exportFile()
	{
		try
		{
			ListItem li = cboType.getSelectedItem();
			if(li == null || li.getValue() == null)
			{
				FDialog.error(m_WindowNo, winExportFile, "FileInvalidExtension");
				return;
			}
			
			String ext = li.getValue().toString();
			
			byte[] data = null;
			File inputFile = null;
									
			if (ext.equals("pdf"))
			{
				data = m_reportEngine.createPDFData();
			}
			else if (ext.equals("ps"))
			{
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				m_reportEngine.createPS(baos);
				data = baos.toByteArray();
			}
			else if (ext.equals("xml"))
			{
				StringWriter sw = new StringWriter();							
				m_reportEngine.createXML(sw);
				data = sw.getBuffer().toString().getBytes();
			}
			else if (ext.equals("csv"))
			{
				StringWriter sw = new StringWriter();							
				m_reportEngine.createCSV(sw, ',', m_reportEngine.getPrintFormat().getLanguage());
				data = sw.getBuffer().toString().getBytes();
			}
			else if (ext.equals("txt"))
			{
				StringWriter sw = new StringWriter();							
				m_reportEngine.createCSV(sw, '\t', m_reportEngine.getPrintFormat().getLanguage());
				data = sw.getBuffer().toString().getBytes();							
			}
			else if (ext.equals("html") || ext.equals("htm"))
			{
				StringWriter sw = new StringWriter();							
				m_reportEngine.createHTML(sw, false, m_reportEngine.getPrintFormat().getLanguage(), new HTMLExtension(m_contextPath, "rp", m_uuid));
				data = sw.getBuffer().toString().getBytes();	
			}
			else if (ext.equals("xlsx"))
			{
				inputFile = File.createTempFile("Export", ".xlsx");							
				m_reportEngine.createXLSX(inputFile, m_reportEngine.getPrintFormat().getLanguage());
			}
			else if (ext.equals("arxml"))
			{
				inputFile = File.createTempFile("Export", ".arxml");							
				ImpExpUtil.exportPrintFormat(inputFile, m_reportEngine);
			}
			else
			{
				FDialog.error(m_WindowNo, winExportFile, "FileInvalidExtension");
				return;
			}

			winExportFile.onClose();
			AMedia media = null;
			if (data != null)
				media = new AMedia(m_reportEngine.getPrintFormat().getName() + "." + ext, null, "application/octet-stream", data);
			else
				media = new AMedia(m_reportEngine.getPrintFormat().getName() + "." + ext, null, "application/octet-stream", inputFile, true);
			Filedownload.save(media, m_reportEngine.getPrintFormat().getName() + "." + ext);
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, "Failed to export content.", e);
		}
	}
	
	
	/**
	 * Load a Print Format from another source
	 */
	private void importPrintFormat()
	{
		Media media = null;
		File arxml = null;
		File directory = null;
		
		try 
		{
			media = Fileupload.get(true); 
			
			if (media == null)
			{
				return;
			}
			directory = File.createTempFile("PackIn", "");
			directory.delete();
			directory.mkdir();
			arxml = new File(directory.getAbsolutePath() + File.separator +  media.getName());
			Files.copy(arxml, media.getStreamData());
		}
		catch (Exception e) 
		{
			log.log(Level.WARNING, e.getLocalizedMessage(), e);
		}
	
		if (ImpExpUtil.importPrintFormat(arxml)) {
			FDialog.info(m_WindowNo, this, "Report Definition Loaded");
			fillComboReport(m_reportEngine.getPrintFormat().get_ID());
		}
		
	}
	
	/**
	 * 	Report Combo - Start other Report or create new one
	 */
	private void cmd_report()
	{
		ListItem li = comboReport.getSelectedItem();
		if(li == null || li.getValue() == null) return;
		
		Object pp = li.getValue();
		if (pp == null)
			return;
		//
		MPrintFormat pf = null;
		int AD_PrintFormat_ID = Integer.valueOf(pp.toString());

		//	create new
		if (AD_PrintFormat_ID == -1)
		{
			int AD_ReportView_ID = m_reportEngine.getPrintFormat().getAD_ReportView_ID();
			if (AD_ReportView_ID != 0)
			{
				String name = m_reportEngine.getName();
				int index = name.lastIndexOf('_');
				if (index != -1)
					name = name.substring(0,index);
				pf = MPrintFormat.createFromReportView(m_ctx, AD_ReportView_ID, name);
			}
			else
			{
				int AD_Table_ID = m_reportEngine.getPrintFormat().getAD_Table_ID();
				pf = MPrintFormat.createFromTable(m_ctx, AD_Table_ID);
			}
			if (pf != null)
				fillComboReport(pf.get_ID());
			else
				return;
		}
		//	copy current
		else if (AD_PrintFormat_ID == -2)
		{
			MPrintFormat current = m_reportEngine.getPrintFormat();
			if (current != null)
			{
				pf = MPrintFormat.copyToClient(m_ctx, current.getAD_PrintFormat_ID(), Env.getAD_Client_ID(m_ctx));

				if (pf != null)
					fillComboReport(pf.get_ID());
				else
					return;
			}
			else
				return;
		}
		else
			pf = MPrintFormat.get (Env.getCtx(), AD_PrintFormat_ID, true);
		
		//	Get Language from previous - thanks Gunther Hoppe 
		if (m_reportEngine.getPrintFormat() != null)
		{
			pf.setLanguage(m_reportEngine.getPrintFormat().getLanguage());		//	needs to be re-set - otherwise viewer will be blank
			pf.setTranslationLanguage(m_reportEngine.getPrintFormat().getLanguage());
		}
		m_reportEngine.setPrintFormat(pf);
		
		try {
			startReportRender();
		} catch (Exception e) {
			throw new AdempiereException("Failed to render report", e);
		}

		revalidate();

	}	//	cmd_report

	/**
	 * Get the maintenance tab of the table associated to the report engine
	 * 
	 * @return AD_Tab_ID or -1 if not found
	 */
	protected int getAD_Tab_ID(int AD_Table_ID)
	{
		int AD_Tab_ID = DB.getSQLValueEx(null, SQL_GET_TABINFO_FROM_TABLEID, AD_Table_ID);
		return AD_Tab_ID;
	} // getAD_Tab_ID

	/**
	 * 	Query Report
	 */
	private void cmd_find()
	{
		String title = null; 
		String tableName = null;

		int AD_Table_ID = m_reportEngine.getPrintFormat().getAD_Table_ID();
		int AD_Tab_ID = getAD_Tab_ID(AD_Table_ID);

		
		//
		String sql = null;
		if (!Env.isBaseLanguage(Env.getCtx(), "AD_Tab"))
			sql = "SELECT Name, TableName FROM AD_Tab_vt WHERE AD_Tab_ID=? AND AD_Language='" + Env.getAD_Language(Env.getCtx()) + "' ";
		else
			sql = "SELECT Name, TableName FROM AD_Tab_v WHERE AD_Tab_ID=? ";

		try
		{
			PreparedStatement pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, AD_Tab_ID);
			ResultSet rs = pstmt.executeQuery();
			//
			if (rs.next())
			{
				title = rs.getString(1);				
				tableName = rs.getString(2);
			}
			//
			rs.close();
			pstmt.close();
		}
		catch (SQLException e)
		{
			throw new DBException(e);
		}

		GridField[] findFields = null;
		if (tableName != null)
			findFields = GridField.createFields(m_ctx, m_WindowNo, 0, AD_Tab_ID);
		
		if (findFields == null)		//	No Tab for Table exists
			bFind.setVisible(false);
		else
		{
			final FindWindow find = new FindWindow(m_WindowNo, title, AD_Table_ID, tableName,m_reportEngine.getWhereExtended(), findFields, 1, AD_Tab_ID, false);
			if (!find.isValid())
            	return;
            find.addEventListener(DialogEvents.ON_WINDOW_CLOSE, new EventListener<Event>() {
				@Override
				public void onEvent(Event event) throws Exception {
					if (!find.isCancel())
		            {
		            	m_reportEngine.setQuery(find.getQuery());
		            	try {
		            		startReportRender();
		            	} catch (Exception e) {
		        			throw new AdempiereException("Failed to render report", e);
		        		}
		            	revalidate();
		            }
				}
			});
            AEnv.showWindow(find);
		}
	}	//	cmd_find

	/**
	 * 	Call Customize
	 */
	private void cmd_customize()
	{
		int AD_Window_ID = 240;		//	hardcoded
		int AD_PrintFormat_ID = m_reportEngine.getPrintFormat().get_ID();
		AEnv.zoom(AD_Window_ID, MQuery.getEqualQuery("AD_PrintFormat_ID", AD_PrintFormat_ID));
	}	//	cmd_customize
	
}
