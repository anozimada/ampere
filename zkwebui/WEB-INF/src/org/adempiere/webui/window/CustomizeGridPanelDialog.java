package org.adempiere.webui.window;

import java.util.ArrayList;
import java.util.Map;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.GridPanel;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.panel.CustomizeGridPanel;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.util.Env;
import org.compiere.util.Msg;

public class CustomizeGridPanelDialog extends Window
{
	/**
	 * 
	 */
	private static final long		serialVersionUID	= 9077416157436132855L;

	public static final String		CUSTOMIZE_GRID		= "onCustomizeGrid";

	private CustomizeGridPanel		customizePanel;

	/**
	 * Standard Constructor
	 * 
	 * @param WindowNo window no
	 * @param AD_Tab_ID tab
	 * @param AD_User_ID user
	 * @param columnsWidth
	 * @param gridFieldIds
	 * @param isQuickForm
	 */
	public CustomizeGridPanelDialog(int windowNo, int AD_Tab_ID, int AD_User_ID, Map<Integer, String> columnsWidth,
			ArrayList<Integer> gridFieldIds)
	{
		setClosable(true);
		setSizable(true);
		setTitle(Msg.getMsg(Env.getCtx(), "Customize"));

		initComponent(windowNo, AD_Tab_ID, AD_User_ID, columnsWidth, gridFieldIds);
	}

	/**
	 * Initialize component of customize panel
	 * 
	 * @param windowNo
	 * @param AD_Tab_ID
	 * @param AD_User_ID
	 * @param columnsWidth
	 * @param gridFieldIds
	 */
	private void initComponent(int windowNo, int AD_Tab_ID, int AD_User_ID, Map<Integer, String> columnsWidth,
			ArrayList<Integer> gridFieldIds)
	{
		customizePanel = new CustomizeGridPanel(windowNo, AD_Tab_ID, AD_User_ID, columnsWidth, gridFieldIds);

		this.setStyle("position : absolute;");
		this.setBorder("normal");
		this.setSclass("popup-dialog");
		
		
		ZKUpdateUtil.setWidth(this, "800px");
		ZKUpdateUtil.setHeight(this, "600px");
		customizePanel.createUI();
		customizePanel.loadData();
		appendChild(customizePanel);

	} // initComponent

	/**
	 * @return whether change have been successfully save to DB
	 */
	public boolean isSaved()
	{
		boolean isSaved = customizePanel.isSaved();
		return isSaved;
	}

	/**
	 * @param gridPanel
	 */
	public void setGridPanel(GridPanel gridPanel)
	{
		customizePanel.setGridPanel(gridPanel);
	}

	/**
	 * Show User customize (modal)
	 * 
	 * @param WindowNo window no
	 * @param AD_Tab_ID
	 * @param columnsWidth
	 * @param gridFieldIds list fieldId current display in gridPanel
	 * @param gridPanel
	 * @param quickGridPanel
	 * @param isQuickForm
	 * @return true if save successfully
	 */
	public static boolean showCustomize(int WindowNo, int AD_Tab_ID, Map<Integer, String> columnsWidth,
			ArrayList<Integer> gridFieldIds, GridPanel gridPanel)
	{
		CustomizeGridPanelDialog customizeWindow = new CustomizeGridPanelDialog(WindowNo, AD_Tab_ID,
				Env.getAD_User_ID(Env.getCtx()), columnsWidth, gridFieldIds);


		customizeWindow.setGridPanel(gridPanel);

		AEnv.showWindow(customizeWindow);

		return customizeWindow.isSaved();
	} // showCustomize
}
