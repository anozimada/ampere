package org.adempiere.webui;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Enumeration;
import java.util.InvalidPropertiesFormatException;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.adempiere.webui.session.ServerContext;

/**
 * Static proxy for directing calls to thread local Properties instance
 * 
 */
public class ZKProperties extends Properties {

	private static final long serialVersionUID = 7360967292681986899L;

	@Override
	public synchronized void clear() {
		Properties context = ServerContext.getCurrentInstance();
		context.clear();
	}

	@Override
	public synchronized Object clone() {
		Properties context = ServerContext.getCurrentInstance();
		return context.clone();
	}

	@Override
	public synchronized Object compute(Object key, BiFunction<? super Object, ? super Object, ?> remappingFunction) {
		Properties context = ServerContext.getCurrentInstance();
		return context.compute(key, remappingFunction);
	}

	@Override
	public synchronized Object computeIfAbsent(Object key, Function<? super Object, ?> mappingFunction) {
		Properties context = ServerContext.getCurrentInstance();
		return context.computeIfAbsent(key, mappingFunction);
	}

	@Override
	public synchronized Object computeIfPresent(Object key,
			BiFunction<? super Object, ? super Object, ?> remappingFunction) {
		Properties context = ServerContext.getCurrentInstance();
		return context.computeIfPresent(key, remappingFunction);
	}

	@Override
	public boolean contains(Object value) {
		Properties context = ServerContext.getCurrentInstance();
		return context.contains(value);
	}

	@Override
	public boolean containsKey(Object key) {
		Properties context = ServerContext.getCurrentInstance();
		return context.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		Properties context = ServerContext.getCurrentInstance();
		return context.containsValue(value);
	}

	@Override
	public Enumeration<Object> elements() {
		Properties context = ServerContext.getCurrentInstance();
		return context.elements();
	}

	@Override
	public Set<java.util.Map.Entry<Object, Object>> entrySet() {
		Properties context = ServerContext.getCurrentInstance();
		return context.entrySet();
	}

	@Override
	public synchronized boolean equals(Object o) {
		Properties context = ServerContext.getCurrentInstance();
		return context.equals(o);
	}

	@Override
	public synchronized void forEach(BiConsumer<? super Object, ? super Object> action) {
		Properties context = ServerContext.getCurrentInstance();
		context.forEach(action);
	}

	@Override
	public Object get(Object key) {
		Properties context = ServerContext.getCurrentInstance();
		return context.get(key);
	}

	@Override
	public Object getOrDefault(Object key, Object defaultValue) {
		Properties context = ServerContext.getCurrentInstance();
		return context.getOrDefault(key, defaultValue);
	}

	@Override
	public String getProperty(String key, String defaultValue) {
		Properties context = ServerContext.getCurrentInstance();
		return context.getProperty(key, defaultValue);
	}

	@Override
	public String getProperty(String arg0) {
		Properties context = ServerContext.getCurrentInstance();
		return context.getProperty(arg0);
	}

	@Override
	public synchronized int hashCode() {
		Properties context = ServerContext.getCurrentInstance();
		return context.hashCode();
	}

	@Override
	public boolean isEmpty() {
		Properties context = ServerContext.getCurrentInstance();
		return context.isEmpty();
	}

	@Override
	public Set<Object> keySet() {
		Properties context = ServerContext.getCurrentInstance();
		return context.keySet();
	}

	@Override
	public Enumeration<Object> keys() {
		Properties context = ServerContext.getCurrentInstance();
		return context.keys();
	}

	@Override
	public void list(PrintStream arg0) {
		Properties context = ServerContext.getCurrentInstance();
		context.list(arg0);
	}

	@Override
	public void list(PrintWriter arg0) {
		Properties context = ServerContext.getCurrentInstance();
		context.list(arg0);
	}

	@Override
	public synchronized void load(InputStream inStream) throws IOException {
		Properties context = ServerContext.getCurrentInstance();
		context.load(inStream);
	}

	@Override
	public synchronized void load(Reader reader) throws IOException {
		Properties context = ServerContext.getCurrentInstance();
		context.load(reader);
	}

	@Override
	public synchronized void loadFromXML(InputStream in) throws IOException, InvalidPropertiesFormatException {
		Properties context = ServerContext.getCurrentInstance();
		context.loadFromXML(in);
	}

	@Override
	public synchronized Object merge(Object key, Object value,
			BiFunction<? super Object, ? super Object, ?> remappingFunction) {
		Properties context = ServerContext.getCurrentInstance();
		return context.merge(key, value, remappingFunction);
	}

	@Override
	public Enumeration<?> propertyNames() {
		Properties context = ServerContext.getCurrentInstance();
		return context.propertyNames();
	}

	@Override
	public synchronized Object put(Object key, Object value) {
		Properties context = ServerContext.getCurrentInstance();
		return context.put(key, value);
	}

	@Override
	public synchronized void putAll(Map<?, ?> t) {
		Properties context = ServerContext.getCurrentInstance();
		context.putAll(t);
	}

	@Override
	public synchronized Object putIfAbsent(Object key, Object value) {
		Properties context = ServerContext.getCurrentInstance();
		return context.putIfAbsent(key, value);
	}

	@Override
	protected void rehash() {
		throw new UnsupportedOperationException("Calling rehash directly not supported");
	}

	@Override
	public synchronized boolean remove(Object key, Object value) {
		Properties context = ServerContext.getCurrentInstance();
		return context.remove(key, value);
	}

	@Override
	public synchronized Object remove(Object key) {
		Properties context = ServerContext.getCurrentInstance();
		return context.remove(key);
	}

	@Override
	public synchronized boolean replace(Object key, Object oldValue, Object newValue) {
		Properties context = ServerContext.getCurrentInstance();
		return context.replace(key, oldValue, newValue);
	}

	@Override
	public synchronized Object replace(Object key, Object value) {
		Properties context = ServerContext.getCurrentInstance();
		return context.replace(key, value);
	}

	@Override
	public synchronized void replaceAll(BiFunction<? super Object, ? super Object, ?> function) {
		Properties context = ServerContext.getCurrentInstance();
		context.replaceAll(function);
	}

	@Override
	public void save(OutputStream out, String comments) {
		Properties context = ServerContext.getCurrentInstance();
		context.save(out, comments);
	}

	@Override
	public synchronized Object setProperty(String key, String value) {
		Properties context = ServerContext.getCurrentInstance();
		return context.setProperty(key, value);
	}

	@Override
	public int size() {
		Properties context = ServerContext.getCurrentInstance();
		return context.size();
	}

	@Override
	public void store(OutputStream out, String comments) throws IOException {
		Properties context = ServerContext.getCurrentInstance();
		context.store(out, comments);
	}

	@Override
	public void store(Writer writer, String comments) throws IOException {
		Properties context = ServerContext.getCurrentInstance();
		context.store(writer, comments);
	}

	@Override
	public void storeToXML(OutputStream os, String comment, Charset charset) throws IOException {
		Properties context = ServerContext.getCurrentInstance();
		context.storeToXML(os, comment, charset);
	}

	@Override
	public void storeToXML(OutputStream arg0, String arg1, String arg2) throws IOException {
		Properties context = ServerContext.getCurrentInstance();
		context.storeToXML(arg0, arg1, arg2);
	}

	@Override
	public void storeToXML(OutputStream os, String comment) throws IOException {
		Properties context = ServerContext.getCurrentInstance();
		context.storeToXML(os, comment);
	}

	@Override
	public Set<String> stringPropertyNames() {
		Properties context = ServerContext.getCurrentInstance();
		return context.stringPropertyNames();
	}

	@Override
	public synchronized String toString() {
		Properties context = ServerContext.getCurrentInstance();
		return context.toString();
	}

	@Override
	public Collection<Object> values() {
		Properties context = ServerContext.getCurrentInstance();
		return context.values();
	}

}
