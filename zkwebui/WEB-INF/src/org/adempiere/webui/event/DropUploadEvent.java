/*******************************************************************************
 * Copyright (C) 2017 Adaxa Pty Ltd. All Rights Reserved. This program is free
 * software; you can redistribute it and/or modify it under the terms version 2
 * of the GNU General Public License as published by the Free Software
 * Foundation. This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 ******************************************************************************/

package org.adempiere.webui.event;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;

/**
 * Drop Upload event
 * 
 * @author <a href="mailto:sachin.bhimani89@gmail.com"> Sachin Bhimani </a>
 * @since Jan 22, 2018
 */
public class DropUploadEvent extends Event
{
	/**
	 * 
	 */
	private static final long	serialVersionUID		= -1278181892192373287L;

	//
	public static final String ON_DROP_UPLOAD_EVENT = "onDropUploadEvent";

	/**
	 * @param target
	 * @param data
	 */
	public DropUploadEvent(Component target, Object data)
	{
		super(ON_DROP_UPLOAD_EVENT, target, data);
	}
}
