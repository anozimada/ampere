package org.adempiere.webui.event;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;

public class CaptureEvent extends Event
{
	public final static String	ON_CAPTURE_IMAGE	= "onCaptureImage";

	public CaptureEvent(Component target, Object data)
	{
		super(ON_CAPTURE_IMAGE, target, data);
	}
}
