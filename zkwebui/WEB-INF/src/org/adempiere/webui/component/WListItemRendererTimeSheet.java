package org.adempiere.webui.component;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import org.adempiere.util.Callback;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.form.WTimeSheet;
import org.adempiere.webui.window.FDialog;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MTimesheetEntry;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Html;
import org.zkoss.zul.Listcell;

public class WListItemRendererTimeSheet extends WListItemRenderer
{

	public WListItemRendererTimeSheet(Vector <String> columnNames)
	{
		super(columnNames);
	}

	protected Listcell getCellComponent(WListbox table, Object field, int rowIndex, int columnIndex)
	{
		ListCell listcell = new ListCell();
		boolean isCellEditable = table != null ? table.isCellEditable(rowIndex, columnIndex) : false;
		boolean isColumnVisible = Boolean.TRUE;

		if (!m_tableColumns.isEmpty())
			isColumnVisible = isColumnVisible(getColumn(columnIndex));

		// TODO put this in factory method for generating cell renderers, which
		// are assigned to Table Columns
		if (field != null && isColumnVisible)
		{
			if (field instanceof Boolean)
			{
				listcell.setValue(Boolean.valueOf(field.toString()));

				if (table != null && columnIndex == 0)
					table.setCheckmark(false);
				Checkbox checkbox = new Checkbox();
				checkbox.setChecked(Boolean.valueOf(field.toString()));

				if (isCellEditable)
				{
					checkbox.setEnabled(true);
					checkbox.addEventListener(Events.ON_CHECK, this);
				}
				else
				{
					checkbox.setEnabled(false);
				}

				listcell.appendChild(checkbox);
				ZkCssHelper.appendStyle(listcell, "text-align:center");
			}
			else if (field instanceof Number || (m_tableColumns.get(columnIndex).getColumnClass() == BigDecimal.class && !(field instanceof Button)))
			{
				DecimalFormat format = DisplayType.getNumberFormat(DisplayType.Number, AEnv.getLanguage(Env.getCtx()));
				if (field instanceof BigDecimal)
					format = DisplayType.getNumberFormat(DisplayType.Amount, AEnv.getLanguage(Env.getCtx()));
				else if (field instanceof Integer || field instanceof Long)
					format = DisplayType.getNumberFormat(DisplayType.Integer, AEnv.getLanguage(Env.getCtx()));
				else if (field instanceof Double || field instanceof Float)
					format = DisplayType.getNumberFormat(DisplayType.Quantity, AEnv.getLanguage(Env.getCtx()));

				// set cell value to allow sorting
				listcell.setValue(field.toString());

				if (isCellEditable)
				{
					Textbox textbox = new Textbox();
					if (!(field instanceof Number))
						field = new BigDecimal(field.toString());
					if (Env.ZERO.compareTo((BigDecimal) field) == 0)
						textbox.setValue(null);
					else
						textbox.setValue(field.toString());
					textbox.setWidth("98%");
					textbox.setEnabled(true);
					textbox.setStyle("text-align:right; " + listcell.getStyle());
					textbox.addEventListener(Events.ON_CHANGE, this);
					textbox.addEventListener(Events.ON_FOCUS, this);
					textbox.addEventListener(Events.ON_BLUR, this);
					textbox.setWidgetListener("onKeyPress", "if(!calc.isNumberKey(event)) event.domEvent.preventDefault();");
					listcell.appendChild(textbox);
				}
				else
				{
					listcell.setLabel(format.format(((Number) field).doubleValue()));
					ZkCssHelper.appendStyle(listcell, "text-align:right");
				}
			}
			else if (field instanceof Timestamp)
			{

				SimpleDateFormat dateFormat = DisplayType.getDateFormat(DisplayType.Date, AEnv.getLanguage(Env.getCtx()));
				listcell.setValue(dateFormat.format((Timestamp) field));
				if (isCellEditable)
				{
					Datebox datebox = new Datebox();
					datebox.setFormat(dateFormat.toPattern());
					datebox.setValue(new Date(((Timestamp) field).getTime()));
					datebox.addEventListener(Events.ON_CHANGE, this);
					listcell.appendChild(datebox);
				}
				else
				{
					listcell.setLabel(dateFormat.format((Timestamp) field));
				}
			}
			else if (field instanceof String)
			{
				listcell.setValue(field.toString());
				if (isCellEditable)
				{
					Textbox textbox = new Textbox();
					textbox.setValue(field.toString());
					textbox.addEventListener(Events.ON_CHANGE, this);
					listcell.appendChild(textbox);
				}
				else
				{
					listcell.setLabel(field.toString());
				}
			}
			else if (field instanceof org.adempiere.webui.component.Combobox)
			{
				listcell.setValue(field);
				if (isCellEditable)
				{
					Combobox combobox = (Combobox) field;
					combobox.addEventListener(Events.ON_CHANGE, this);
					listcell.appendChild(combobox);
				}
				else
				{
					Combobox combobox = (Combobox) field;
					if (combobox != null && combobox.getItemCount() > 0)
					{
						if (combobox.getSelectedIndex() >= 0)
							listcell.setLabel((String) combobox.getItemAtIndex(combobox.getSelectedIndex()).getLabel());
						else
							listcell.setLabel("");
					}
				}
			}
			else if (field instanceof org.adempiere.webui.component.Button)
			{
				listcell.setValue(field);
				if (isCellEditable)
				{
					Button button = (Button) field;
					button.addEventListener(Events.ON_CLICK, this);
					button.addEventListener(Events.ON_FOCUS, this);
					button.addEventListener(Events.ON_BLUR, this);
					button.setWidth("100%");
					listcell.appendChild(button);
					listcell.setClass("list-cell-timesheet-btn");
				}
				else
				{
					Button button = (Button) field;
					if (button != null)
					{
						listcell.setLabel("");
					}
				}
			}
			// if ID column make it invisible
			else if (field instanceof IDColumn)
			{
				listcell.setValue(((IDColumn) field).getRecord_ID());
				if (!table.isCheckmark())
				{
					table.setCheckmark(true);
					table.removeEventListener(Events.ON_SELECT, this);
					table.addEventListener(Events.ON_SELECT, this);
				}
			}
			else if (field instanceof Html)
			{
				listcell.appendChild((Html) field);
			}
			else
			{
				listcell.setLabel(field.toString());
				listcell.setValue(field.toString());
			}
		}
		else
		{
			listcell.setLabel("");
			listcell.setValue("");
		}

		listcell.setAttribute("zk_component_ID", "ListItem_Cell_" + rowIndex + "_" + columnIndex);

		return listcell;
	}

	@Override
	public void onEvent(Event event) throws Exception
	{
		super.onEvent(event);

		Component source = event.getTarget();

		if (Events.ON_CLICK.equals(event.getName()) && isWithinListCell(source) && event.getTarget() instanceof Button)
		{
			Button btn = (Button) source;
			FDialog.ask(0, btn, "Delete processed timesheet entry?", new Callback<Boolean>() {

				@Override
				public void onCallback(Boolean result) {
					if (result) {
						int projectID = (int) btn.getAttribute(WTimeSheet.BTN_ATTRIB_C_PROJECT_ID);
						int pphaseID = (int) btn.getAttribute(WTimeSheet.BTN_ATTRIB_C_PROJECTPHASE_ID);
						int ptaskID = (int) btn.getAttribute(WTimeSheet.BTN_ATTRIB_C_PROJECTTASK_ID);
						int resourceID = (int) btn.getAttribute(WTimeSheet.BTN_ATTRIB_RESOURCE_ID);
						Timestamp date = (Timestamp) btn.getAttribute(WTimeSheet.BTN_ATTRIB_DATE);

						MTimesheetEntry entry = MTimesheetEntry.get(Env.getCtx(), projectID, pphaseID, ptaskID, resourceID, date, null);
						entry.deleteEx(true);

						if (btn.getParent() instanceof ListCell)
						{
							ListCell listcell = (ListCell) btn.getParent();
							listcell.removeChild(btn);

							Component parent = listcell.getParent();
							while (parent != null && !(parent instanceof WTimeSheet))
							{
								parent = parent.getParent();
							}

							Events.postEvent(WTimeSheet.EVENT_ON_RE_RENDER, parent, null);
							Events.postEvent(WTimeSheet.EVENT_ON_ITEM_RENDERED, parent, null);
						}
					
					}
				}
				
			});
		}
	}

}
