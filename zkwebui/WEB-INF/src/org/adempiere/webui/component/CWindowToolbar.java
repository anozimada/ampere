/******************************************************************************
 * Product: Posterita Ajax UI 												  *
 * Copyright (C) 2007 Posterita Ltd.  All Rights Reserved.                    *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Posterita Ltd., 3, Draper Avenue, Quatre Bornes, Mauritius                 *
 * or via info@posterita.org or http://www.posterita.org/                     *
 *****************************************************************************/

package org.adempiere.webui.component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;

import org.adempiere.webui.AdempiereIdGenerator;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.event.ToolbarListener;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.MRole;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Space;

/**
 *
 * @author  <a href="mailto:agramdass@gmail.com">Ashley G Ramdass</a>
 * @date    Feb 25, 2007
 * @version $Revision: 0.10 $
 *
 * @author Cristina Ghita, www.arhipac.ro
 * 				<li>FR [ 2076330 ] Add new methods in CWindowToolbar class
 */
public class CWindowToolbar extends FToolbar implements EventListener<Event>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -762537218475834634L;

	private static final String TOOLBAR_BUTTON_STYLE = "";
	private static final String EMBEDDED_TOOLBAR_BUTTON_STYLE = "";

	public static final String BTN_PROCESSING = "Processing";

    private static CLogger log = CLogger.getCLogger(CWindowToolbar.class);

    private ToolBarButton btnIgnore;

    private ToolBarButton btnHelp, btnNew, btnCopy, btnDelete, btnSave;

    private ToolBarButton btnRefresh, btnFind, btnLock;

    private ToolBarButton btnGridToggle;

    private ToolBarButton btnParentRecord, btnDetailRecord;

    private ToolBarButton btnFirst, btnPrevious, btnNext, btnLast;

    private ToolBarButton btnPrint;

    private ToolBarButton btnActiveWorkflows, btnRequests, btnProductInfo, btnBPartnerInfo;

    private HashMap<String, ToolBarButton> buttons = new HashMap<String, ToolBarButton>();

    private ToolBarButton btnCustomize, btnExport;
    
    private ToolBarButton btnProcess;
    
    private ToolBarButton btnProcessing;
    
    private ArrayList<ToolbarListener> listeners = new ArrayList<ToolbarListener>();

    private Event event;

    private Map<Integer, ToolBarButton> keyMap = new HashMap<Integer, ToolBarButton>();
    private Map<Integer, ToolBarButton> altKeyMap = new HashMap<Integer, ToolBarButton>();
    private Map<Integer, ToolBarButton> ctrlKeyMap = new HashMap<Integer, ToolBarButton>();

	private boolean embedded;

	// Elaine 2008/12/04
	/** Show Personal Lock								*/
	public boolean isPersonalLock = MRole.getDefault().isPersonalLock();
	private boolean isAllowProductInfo = MRole.getDefault().isAllow_Info_Product();
	private boolean isAllowBPartnerInfo = MRole.getDefault().isAllow_Info_BPartner();

	private int windowNo = 0;

	private long prevKeyEventTime = 0;

	private KeyEvent prevKeyEvent;

	/**	Last Modifier of Action Event					*/
//	public int 				lastModifiers;
	//

    public CWindowToolbar()
    {
    	this(false);
    }

    public CWindowToolbar(boolean embedded)
    {
    	this.embedded = embedded;
        init();
    }

    public CWindowToolbar(boolean embedded, int windowNo) {
    	this.embedded = embedded;
    	setWindowNo(windowNo);
        init();
	}

	private void init()
    {
    	LayoutUtils.addSclass("adwindow-toolbar", this);

        btnIgnore = createButton("Ignore", "Ignore", "Ignore");
        addSeparator();
        btnHelp = createButton("Help", "Help","Help");
        btnNew = createButton("New", "New", "New");
        btnCopy = createButton("Copy", "Copy", "Copy");
        btnDelete = createButton("Delete", "Delete", "Delete");
        btnSave = createButton("Save", "Save", "Save");
        addSeparator();
        btnRefresh = createButton("Refresh", "Refresh", "Refresh");
        btnFind = createButton("Find", "Find", "Find");
        btnGridToggle = createButton("Toggle", "Multi", "Multi");
        addSeparator();
        btnParentRecord = createButton("ParentRecord", "Parent", "Parent");
        btnDetailRecord = createButton("DetailRecord", "Detail", "Detail");
        btnFirst = createButton("First", "FirstRecord", "First");
        btnPrevious = createButton("Previous", "PreviousRecord", "Previous");
        btnNext = createButton("Next", "NextRecord", "Next");
        btnLast = createButton("Last", "LastRecord", "Last");
        addSeparator();
        btnPrint = createButton("Print", "Print", "Print");
        addSeparator();
        btnLock = createButton("Lock", "Lock", "Lock"); // Elaine 2008/12/04
		btnLock.setVisible(isPersonalLock);
        btnActiveWorkflows = createButton("ActiveWorkflows", "WorkFlow", "WorkFlow");
        btnRequests = createButton("Requests", "Request", "Request");
        btnProductInfo = createButton("ProductInfo", "Product", "InfoProduct");
        btnProductInfo.setVisible(isAllowProductInfo);

        btnBPartnerInfo = createButton("BPartnerInfo", "BPartner", "Business Partner Info");
        btnBPartnerInfo.setVisible(isAllowBPartnerInfo);

        // Help and Exit should always be enabled
        btnHelp.setDisabled(false);
        btnGridToggle.setDisabled(false);

        btnActiveWorkflows.setDisabled(false); // Elaine 2008/07/17
        btnRequests.setDisabled(false); // Elaine 2008/07/22
        btnProductInfo.setDisabled(!isAllowProductInfo); // Elaine 2008/07/22
        btnBPartnerInfo.setDisabled(!isAllowBPartnerInfo);
        btnLock.setDisabled(!isPersonalLock); // Elaine 2008/12/04

        configureKeyMap();
        
        btnCustomize = createButton("Customize", "Customize", "Customize");
        btnCustomize.setVisible(true);
        btnCustomize.setDisabled(false);
        
        btnProcess= createButton("Process", "Process", "Process");
        btnProcess.setTooltiptext(btnProcess.getTooltiptext()+ "    Alt+O");
        btnProcess.setDisabled(false);
        
        btnExport = createButton("Export", "Export","Export");
        btnExport.setVisible(true);
        btnExport.setDisabled(false);
        
		btnProcessing = createButton(BTN_PROCESSING, BTN_PROCESSING, BTN_PROCESSING);
		btnProcessing.setStyle(btnProcessing.getStyle() + " top: 5px; position: relative;");
		btnProcessing.setVisible(false);
		String jsIcon = "$(document).ready(function(){ "
				+ "var btn = $('#" + btnProcessing.getUuid() + "');"
				+ "btn.empty(); "
				+ "btn.append('<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" class=\"lds-dash-ring\" width=\"24px\" height=\"24px\" viewBox=\"0 0 100 100\" preserveAspectRatio=\"xMidYMid\" style=\"background: none;\"><g transform=\"rotate(95.8206 50 50)\"> <animateTransform attributeName=\"transform\" type=\"rotate\" values=\"0 50 50;120 50 50\" keyTimes=\"0;1\" dur=\"1s\" repeatCount=\"indefinite\"/><circle cx=\"50\" cy=\"50\" r=\"40\" stroke=\"#456caa\" fill=\"none\" stroke-dasharray=\"41.88790204786391 251.32741228718345\" stroke-linecap=\"round\" stroke-width=\"17\" transform=\"rotate(0 50 50)\"> <animate attributeName=\"stroke\" values=\"#456caa;#88a2ce\" keyTimes=\"0;1\" dur=\"1s\" repeatCount=\"indefinite\"/> </circle><circle cx=\"50\" cy=\"50\" r=\"40\" stroke=\"#88a2ce\" fill=\"none\" stroke-dasharray=\"41.88790204786391 251.32741228718345\" stroke-linecap=\"round\" stroke-width=\"17\" transform=\"rotate(120 50 50)\"> <animate attributeName=\"stroke\" values=\"#88a2ce;#c2d2ee\" keyTimes=\"0;1\" dur=\"1s\" repeatCount=\"indefinite\"/> </circle><circle cx=\"50\" cy=\"50\" r=\"40\" stroke=\"#c2d2ee\" fill=\"none\" stroke-dasharray=\"41.88790204786391 251.32741228718345\" stroke-linecap=\"round\" stroke-width=\"17\" transform=\"rotate(240 50 50)\"> <animate attributeName=\"stroke\" values=\"#c2d2ee;#456caa\" keyTimes=\"0;1\" dur=\"1s\" repeatCount=\"indefinite\"/> </circle></g></svg>');})";
		Clients.evalJavaScript(jsIcon);

        if (embedded)
        {
        	addSeparator();

        	btnParentRecord.setVisible(false);
    		btnDetailRecord.setVisible(false);
    		btnActiveWorkflows.setVisible(false);
    		btnProductInfo.setVisible(false);
    		btnBPartnerInfo.setVisible(false);
    		setAlign("end");
    		ZKUpdateUtil.setWidth(this, "100%");
        }
        else
        {
        	ZKUpdateUtil.setWidth(this, "100%");
        	addSeparator();
        }
    }


    private ToolBarButton createButton(String name, String image, String tooltip)
    {
    	ToolBarButton btn = new ToolBarButton("");
        btn.setName("Btn"+name);
        if (windowNo > 0)
        	btn.setAttribute(AdempiereIdGenerator.ZK_COMPONENT_PREFIX_ATTRIBUTE, "unq" + btn.getName() + "_" + windowNo + (embedded ? "E" : ""));
        else
        	btn.setAttribute(AdempiereIdGenerator.ZK_COMPONENT_PREFIX_ATTRIBUTE, btn.getName());
        btn.setIconSclass("z-icon-" + image); // Ignore min size icon for embedded tab
        btn.setTooltiptext(Msg.getMsg(Env.getCtx(),tooltip));
        if (embedded)
        {
        	btn.setStyle(EMBEDDED_TOOLBAR_BUTTON_STYLE);
        	btn.setSclass("embedded-toolbar-button");
        }
        else
        {
        	btn.setStyle(TOOLBAR_BUTTON_STYLE);
        	btn.setSclass("toolbar-button");
        }
        buttons.put(name, btn);
        this.appendChild(btn);
        //make toolbar button last to receive focus
        btn.setTabindex(0);
        btn.addEventListener(Events.ON_CLICK, this);
        btn.setDisabled(true);

        return btn;
    }

    public ToolBarButton getButton(String name)
    {
    	return buttons.get(name);
    }

    /** VK_A thru VK_Z are the same as ASCII 'A' thru 'Z' (0x41 - 0x5A) */
    public static final int VK_A              = 0x41;
    public static final int VK_B              = 0x42;
    public static final int VK_C              = 0x43;
    public static final int VK_D              = 0x44;
    public static final int VK_E              = 0x45;
    public static final int VK_F              = 0x46;
    public static final int VK_G              = 0x47;
    public static final int VK_H              = 0x48;
    public static final int VK_I              = 0x49;
    public static final int VK_J              = 0x4A;
    public static final int VK_K              = 0x4B;
    public static final int VK_L              = 0x4C;
    public static final int VK_M              = 0x4D;
    public static final int VK_N              = 0x4E;
    public static final int VK_O              = 0x4F;
    public static final int VK_P              = 0x50;
    public static final int VK_Q              = 0x51;
    public static final int VK_R              = 0x52;
    public static final int VK_S              = 0x53;
    public static final int VK_T              = 0x54;
    public static final int VK_U              = 0x55;
    public static final int VK_V              = 0x56;
    public static final int VK_W              = 0x57;
    public static final int VK_X              = 0x58;
    public static final int VK_Y              = 0x59;
    public static final int VK_Z              = 0x5A;

    private void configureKeyMap()
    {
		keyMap.put(KeyEvent.F1, btnHelp);
		keyMap.put(KeyEvent.F2, btnNew);
		keyMap.put(KeyEvent.F3, btnDelete);
		keyMap.put(KeyEvent.F4, btnSave);
		keyMap.put(KeyEvent.F5, btnRefresh);
		keyMap.put(KeyEvent.F6, btnFind);
		keyMap.put(KeyEvent.F8, btnGridToggle);
		keyMap.put(KeyEvent.F12, btnPrint);

		altKeyMap.put(KeyEvent.LEFT, btnParentRecord);
		altKeyMap.put(KeyEvent.RIGHT, btnDetailRecord);
		altKeyMap.put(KeyEvent.UP, btnPrevious);
		altKeyMap.put(KeyEvent.DOWN, btnNext);
		altKeyMap.put(KeyEvent.PAGE_UP, btnFirst);
		altKeyMap.put(KeyEvent.PAGE_DOWN, btnLast);
		altKeyMap.put(VK_Z, btnIgnore);
		altKeyMap.put(VK_O, btnProcess);

		ctrlKeyMap.put(VK_I, btnProductInfo);
		ctrlKeyMap.put(VK_B, btnBPartnerInfo);
		ctrlKeyMap.put(VK_P, btnPrint);
		ctrlKeyMap.put(VK_N, btnNew);
		ctrlKeyMap.put(VK_S, btnSave);
		ctrlKeyMap.put(VK_D, btnDelete);
		ctrlKeyMap.put(VK_F, btnFind);
	}

	protected void addSeparator()
    {
		Space s = new Space();
		if (embedded)
			s.setSpacing("3px");
		else
			s.setSpacing("6px");
		s.setOrient("vertical");
		this.appendChild(s);
    }

    public void addListener(ToolbarListener toolbarListener)
    {
        listeners.add(toolbarListener);
    }

    public void removeListener(ToolbarListener toolbarListener)
    {
        listeners.remove(toolbarListener);
    }

    public void onEvent(Event event)
    {
        String eventName = event.getName();

        if(eventName.equals(Events.ON_CLICK))
        {
            if(event.getTarget() instanceof ToolBarButton)
            {
            	doOnClick(event);
            }
        } else if (eventName.equals(Events.ON_CTRL_KEY))
        {
        	KeyEvent keyEvent = (KeyEvent) event;
        	if (isRealVisible()) {
	        	//filter same key event that is too close
	        	//firefox fire key event twice when grid is visible
	        	long time = System.currentTimeMillis();
	        	if (prevKeyEvent != null && prevKeyEventTime > 0 &&
	        			prevKeyEvent.getKeyCode() == keyEvent.getKeyCode() &&
	    				prevKeyEvent.getTarget() == keyEvent.getTarget() &&
	    				prevKeyEvent.isAltKey() == keyEvent.isAltKey() &&
	    				prevKeyEvent.isCtrlKey() == keyEvent.isCtrlKey() &&
	    				prevKeyEvent.isShiftKey() == keyEvent.isShiftKey()) {
	        		if ((time - prevKeyEventTime) <= 300) {
	        			return;
	        		}
	        	}
	        	this.onCtrlKeyEvent(keyEvent);
        	}
        }
    }

	private void doOnClick(Event event) {
		this.event = event;
		ToolBarButton cComponent = (ToolBarButton) event.getTarget();
		String compName = cComponent.getName();
		String methodName = "on" + compName.substring(3);
		Iterator<ToolbarListener> listenerIter = listeners.iterator();
		while(listenerIter.hasNext())
		{
		    try
		    {
		        ToolbarListener tListener = listenerIter.next();
		        Method method = tListener.getClass().getMethod(methodName, (Class[]) null);
		        method.invoke(tListener, (Object[]) null);
		    }
		    catch(SecurityException e)
		    {
		        log.log(Level.SEVERE, "Could not invoke Toolbar listener method: " + methodName + "()", e);
		    }
		    catch(NoSuchMethodException e)
		    {
		        log.log(Level.SEVERE, "Could not invoke Toolbar listener method: " + methodName + "()", e);
		    }
		    catch(IllegalArgumentException e)
		    {
		        log.log(Level.SEVERE, "Could not invoke Toolbar listener method: " + methodName + "()", e);
		    }
		    catch(IllegalAccessException e)
		    {
		        log.log(Level.SEVERE, "Could not invoke Toolbar listener method: " + methodName + "()", e);
		    }
		    catch(InvocationTargetException e)
		    {
		        log.log(Level.SEVERE, "Could not invoke Toolbar listener method: " + methodName + "()", e);
		    }
		}
		this.event = null;
	}

    public void enableNavigation(boolean enabled)
    {
        this.btnFirst.setDisabled(!enabled);
        this.btnPrevious.setDisabled(!enabled);
        this.btnNext.setDisabled(!enabled);
        this.btnLast.setDisabled(!enabled);
    }

    public void enableTabNavigation(boolean enabled)
    {
        enableTabNavigation(enabled, enabled);
    }

    public void enableTabNavigation(boolean enableParent, boolean enableDetail)
    {
        this.btnParentRecord.setDisabled(!enableParent);
        this.btnDetailRecord.setDisabled(!enableDetail);
    }

    public void enableFirstNavigation(boolean enabled)
    {
        this.btnFirst.setDisabled(!enabled);
        this.btnPrevious.setDisabled(!enabled);
    }

    public void enableLastNavigation(boolean enabled)
    {
        this.btnLast.setDisabled(!enabled);
        this.btnNext.setDisabled(!enabled);
    }

    public void enabledNew(boolean enabled)
    {
        this.btnNew.setDisabled(!enabled);
    }

   /* public void enableEdit(boolean enabled)
    {
        this.btnEdit.setEnabled(enabled);
    }*/

    public void enableRefresh(boolean enabled)
    {
        this.btnRefresh.setDisabled(!enabled);
    }

	public void enableSave(boolean enabled)
	{
		this.btnSave.setDisabled(!enabled);
		if (enabled)
		{
			this.btnSave.setSclass(" toolbar-save-image ");
		}
		else
		{
			this.btnSave.setSclass(this.btnSave.getSclass().replaceAll(" toolbar-save-image ", ""));
		}

		this.btnSave.setIconSclass("z-icon-Save");
	}

    public boolean isSaveEnable() {
    	return !btnSave.isDisabled();
    }

    public void enableDelete(boolean enabled)
    {
        this.btnDelete.setDisabled(!enabled);
    }
    
    public boolean isDeleteEnable()
    {
    	return !btnDelete.isDisabled();
    }

    public void enableChanges(boolean enabled)
    {
        this.btnNew.setDisabled(!enabled);
        this.btnCopy.setDisabled(!enabled);
    }

    public void enableIgnore(boolean enabled)
    {
        this.btnIgnore.setDisabled(!enabled);
    }

    public void enableNew(boolean enabled)
    {
        this.btnNew.setDisabled(!enabled);
    }

    public void enablePrint(boolean enabled)
    {
    	this.btnPrint.setDisabled(!enabled);
    }

    public void enableCopy(boolean enabled)
    {
    	this.btnCopy.setDisabled(!enabled);
    }

    public void enableFind(boolean enabled)
    {
        this.btnFind.setDisabled(!enabled);
    }

    public void enableGridToggle(boolean enabled)
    {
    	btnGridToggle.setDisabled(!enabled);
    }
    
    public void enableCustomize(boolean enabled)
    {
    	btnCustomize.setDisabled(!enabled);
    }

	public void enableActiveWorkflows(boolean enabled)
	{
		this.btnActiveWorkflows.setDisabled(!enabled);
	}

	public void enableRequests(boolean enabled)
	{
		this.btnRequests.setDisabled(!enabled);
	}

	public void enableProductInfo(boolean enabled)
	{
		this.btnProductInfo.setDisabled(!enabled);
	}

	public void enableBPartnerInfo(boolean enabled)
	{
		this.btnBPartnerInfo.setDisabled(!enabled);
	}
	
	public void enableProcessButton(boolean b) {
		if (btnProcess != null) {
			btnProcess.setDisabled(!b);
		}
	}

    public void lock(boolean locked)
    {
    	this.btnLock.setPressed(locked);

    	String img = this.btnLock.isPressed() ? "z-icon-LockX" : "z-icon-Lock";
    	this.btnLock.setIconSclass(img);
    }

    public Event getEvent()
    {
    	return event;
    }

	private void onCtrlKeyEvent(KeyEvent keyEvent) {
		ToolBarButton btn = null;
		if (keyEvent.isAltKey() && !keyEvent.isCtrlKey() && !keyEvent.isShiftKey())
		{
			if (keyEvent.getKeyCode() == VK_X)
			{
				if (windowNo > 0)
				{
					prevKeyEventTime = System.currentTimeMillis();
		        	prevKeyEvent = keyEvent;
					keyEvent.stopPropagation();
					SessionManager.getAppDesktop().closeWindow(windowNo);
				}
			}
			else
			{
				btn = altKeyMap.get(keyEvent.getKeyCode());
			}
		}
		else if (!keyEvent.isAltKey() && keyEvent.isCtrlKey() && !keyEvent.isShiftKey())
		{
			if (keyEvent.getKeyCode() == KeyEvent.F2)
			{
				sendButtonClickEvent(keyEvent, btnDetailRecord);
				sendButtonClickEvent(keyEvent, btnParentRecord);
			}
			else
			{
				btn = ctrlKeyMap.get(keyEvent.getKeyCode());
			}
		}
		else if (!keyEvent.isAltKey() && !keyEvent.isCtrlKey() && !keyEvent.isShiftKey())
		{
			btn = keyMap.get(keyEvent.getKeyCode());
		}
		sendButtonClickEvent(keyEvent, btn);
	}

	/**
	 * @param keyEvent
	 * @param btn
	 */
	public void sendButtonClickEvent(KeyEvent keyEvent, ToolBarButton btn)
	{
		if (btn != null)
		{
			prevKeyEventTime = System.currentTimeMillis();
			prevKeyEvent = keyEvent;
			keyEvent.stopPropagation();
			if (!btn.isDisabled() && btn.isVisible())
			{
				Events.sendEvent(btn, new Event(Events.ON_CLICK, btn));
			}
		}
	}

	private boolean isRealVisible() {
		if (!isVisible())
			return false;
		Component parent = this.getParent();
		while (parent != null) {
			if (!parent.isVisible())
				return false;
			parent = parent.getParent();
		}
		return true;
	}

	/**
	 *
	 * @param visible
	 */
	public void setVisibleAll(boolean visible)
	{
		for (ToolBarButton btn : buttons.values())
		{
			btn.setVisible(visible);
		}
	}

	/**
	 *
	 * @param buttonName
	 * @param visible
	 */
	public void setVisible(String buttonName, boolean visible)
	{
		ToolBarButton btn = buttons.get(buttonName);
		if (btn != null)
		{
			btn.setVisible(visible);
		}
	}

	/**
	 *
	 * @param windowNo
	 */
	public void setWindowNo(int windowNo) {
		this.windowNo = windowNo;
	}
}
