/******************************************************************************
 * Copyright (C) 2008 Low Heng Sin                                            *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.webui.component;

import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.util.Util;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Iframe;

/**
 * URL Box
 * 
 * @author Low Heng Sin
 */
public class Urlbox extends EditorBox
{

	/**
	 * 
	 */
	private static final long	serialVersionUID		= -3846071364733430994L;
	
	private static final String	IFRAME_EMPTY_THUMBNAIL	= "IFRAME_EMPTY_THUMBNAIL";

	protected Iframe			iframe;
	protected Hbox				hbox;
	private boolean				isShowIFrame = true;

	public Urlbox()
	{
		super();
		setIFrameComponent(null);
	}

	/**
	 * @param url
	 */
	public Urlbox(String url)
	{
		super();
		setText(url);
		setIFrameComponent(url);
	}

	/**
	 * @param url
	 */
	public Urlbox(boolean isShowIFrame)
	{
		super();
		this.isShowIFrame = isShowIFrame;
		if (isShowIFrame)
			setIFrameComponent(null);
	}

	/**
	 * Display URL content info on the IFrame component. The below component of
	 * text URL.
	 * 
	 * @author Sachin Bhimani
	 */
	private void setIFrameComponent(String url)
	{
		if (Util.isEmpty(url, true))
			url = IFRAME_EMPTY_THUMBNAIL;
		if (iframe != null)
		{
			hbox.removeChild(iframe);
			iframe = null;
		}
		if (hbox != null)
		{
			hbox.detach();
			hbox = null;
		}

		if (!url.equalsIgnoreCase(IFRAME_EMPTY_THUMBNAIL))
		{
			hbox = new Hbox();
			ZKUpdateUtil.setWidth(hbox, "367px");
			ZKUpdateUtil.setHeight(hbox, "250px");
			hbox.setStyle("padding-top: 1px; margin-top: 5px; border:1px solid #86a4be; background-color: #eceae4;");

			iframe = new Iframe(url);
			ZKUpdateUtil.setWidth(iframe, "100%");
			ZKUpdateUtil.setHeight(iframe, "100%");
			iframe.setSrc(url);
			iframe.invalidate();
			iframe.setStyle("margin:0; border:none; overflow:hidden;");

			hbox.appendChild(iframe);
			appendChild(hbox);
		}
	}

	public Iframe getIframe()
	{
		return iframe;
	}

	public void setIframe(Iframe iframe)
	{
		this.iframe = iframe;
	}

	/*
	 * (non-Javadoc)
	 * @see org.adempiere.webui.component.EditorBox#setText(java.lang.String)
	 */
	@Override
	public void setText(String value)
	{
		super.setText(value);
		if (isShowIFrame)
			setIFrameComponent(value);
	}

	/*
	 * (non-Javadoc)
	 * @see org.zkoss.zk.ui.AbstractComponent#detach()
	 */
	@Override
	public void detach()
	{
		super.detach();
		iframe = null;
		if (hbox != null)
			hbox.detach();
	}

}