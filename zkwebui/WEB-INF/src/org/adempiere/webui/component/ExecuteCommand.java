/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2007 Adempiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/

package org.adempiere.webui.component;


import java.util.Map;
import org.adempiere.webui.event.ExecuteEvent;
import org.zkoss.lang.Objects;
import org.zkoss.json.JSONArray;
import org.zkoss.zk.au.AuRequest;
import org.zkoss.zk.au.AuService;
import org.zkoss.zk.mesg.MZk;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.Events;

/**
 * 
 * @author jobriant
 *
 */
public class ExecuteCommand implements AuService {


	public ExecuteCommand() {
	}
	
	public boolean service(AuRequest request, boolean everError) {
		if (!ExecuteEvent.ON_EXECUTE.equals(request.getCommand()))
			return false;

		Map<?, ?> map = request.getData();
		JSONArray data = (JSONArray) map.get("data");
		
		final Component comp = request.getComponent();
		if (comp == null)
			throw new UiException(MZk.ILLEGAL_REQUEST_COMPONENT_REQUIRED, this);
		
		if (data == null || data.size() < 4)
			throw new UiException(MZk.ILLEGAL_REQUEST_WRONG_DATA, new Object[] {
					Objects.toString(data), this });
		
		/*		
		String columnName = data[0];
		String tableName = MQuery.getZoomTableName(columnName);
		Object code = null; 
		if (columnName.endsWith("_ID"))
		{
			try {
				code = Integer.parseInt(data[1]);
			} catch (Exception e) {
				code = data[1];
			}
		}
		else
		{
			code = data[1];
		}
		//
		MQuery query = new MQuery(tableName);
		query.addRestriction(columnName, MQuery.EQUAL, code);
		query.setRecordCount(1);

*/		
		Events.postEvent(new ExecuteEvent(comp, data));
		return true;
	}

}
