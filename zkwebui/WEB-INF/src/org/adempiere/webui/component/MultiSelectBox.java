package org.adempiere.webui.component;

import java.util.ArrayList;

import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.exceptions.ValueChangeListener;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.editor.WMultiSelectEditor;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.util.ValueNamePair;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Vbox;

public class MultiSelectBox extends Div implements EventListener<Event>
{

	/**
	 * 
	 */
	private static final long				serialVersionUID		= 3364834897097285423L;

	public static final String				ATTRIBUTE_MULTI_SELECT	= "ATTRIBUTE_MULTI_SELECT";

	public WMultiSelectEditor				editor;

	private static String					PleaseSelect			= "(Please Select)";

	private Textbox							textbox;
	private Popup							popup;
	private Vbox							vbox;
	private boolean							editable;

	private ArrayList<Checkbox>				cbxList					= new ArrayList<Checkbox>();
	private ArrayList<ValueChangeListener>	listeners				= new ArrayList<ValueChangeListener>();

	public MultiSelectBox()
	{
		super();
		ZKUpdateUtil.setWidth(this, "100%");
		init();
	}

	public MultiSelectBox(ArrayList<ValueNamePair> list, boolean editable)
	{
		super();
		ZKUpdateUtil.setWidth(this, "100%");

		init();
		loadData(list);
		setEditable(editable);
	}

	private void init()
	{
		LayoutUtils.addSclass(".multi-select-box", this);

		popup = new Popup();
		LayoutUtils.addSclass("multi-select-popup", popup);
		appendChild(popup);
		popup.addEventListener(Events.ON_OPEN, this);
		
		vbox = new Vbox();
		LayoutUtils.addSclass("multi-select-vbox", vbox);
		popup.appendChild(vbox);

		textbox = new Textbox("") {

			/**
			 * 
			 */
			private static final long	serialVersionUID	= 1567895599198729743L;

			public void setValue(String value) throws WrongValueException
			{
				super.setValue(value);
				setTooltiptext(value);
			}
		};
		textbox.addEventListener(Events.ON_CLICK, this);
		if (isEnabled())
			textbox.setPopup("uuid(" + popup.getUuid() + "), after_start");

		textbox.setReadonly(true);
		ZKUpdateUtil.setWidth(textbox, "100%");
		LayoutUtils.addSclass("multi-select-textbox", textbox);
		appendChild(textbox);
	} // init

	public Popup getPopupComponent()
	{
		return popup;
	}

	public Textbox getTextbox()
	{
		return textbox;
	}

	public ArrayList<Checkbox> getCheckboxList()
	{
		return cbxList;
	}

	public Vbox getVBox()
	{
		return vbox;
	}

	public boolean isEnabled()
	{
		return editable;
	}

	public void setEditable(boolean editable)
	{
		this.editable = editable;

		if (textbox != null)
		{
			if (!isEnabled())
			{
				LayoutUtils.addSclass("multi-select-textbox-readonly", textbox);
				textbox.setPopup((Popup) null);
			}
			else
			{
				LayoutUtils.addSclass("multi-select-textbox", textbox);
				textbox.setPopup("uuid(" + popup.getUuid() + "), after_start");
			}
		}
	} // setEditable

	public void loadData(ArrayList<ValueNamePair> list)
	{
		textbox.setValue(PleaseSelect);

		for (Checkbox cbx : cbxList)
			cbx.detach();
		cbxList.clear();

		if (list != null)
		{
			for (ValueNamePair vnp : list)
			{
				Checkbox cbx = new Checkbox();
				cbx.setLabel(vnp.getName());
				cbx.setAttribute("msbValue", vnp.getValue());
				cbxList.add(cbx);
				vbox.appendChild(cbx);
			}
		}
	}

	public void setValues(ArrayList<ValueNamePair> values)
	{
		for (Checkbox cbx : cbxList)
			cbx.setChecked(false);

		if (values != null)
		{
			String value = "";
			for (Checkbox cbx : cbxList)
			{
				ValueNamePair vnp = new ValueNamePair((String) cbx.getAttribute("msbValue"), cbx.getLabel());
				if (values.contains(vnp))
				{
					cbx.setChecked(true);
					value += cbx.getLabel() + "; ";
				}
			}

			value = value.trim();
			if (value.length() == 0)
				value = PleaseSelect;
			textbox.setValue(value);
		}
	}

	public ArrayList<ValueNamePair> getValues()
	{
		ArrayList<ValueNamePair> values = new ArrayList<ValueNamePair>();
		for (Checkbox cbx : cbxList)
		{
			if (cbx.isChecked())
				values.add(new ValueNamePair((String) cbx.getAttribute("msbValue"), cbx.getLabel()));
		}
		return values;
	}

	@Override
	public void onEvent(Event event) throws Exception
	{
		if (event.getName().equals(Events.ON_CLICK))
		{
			if (event.getTarget() == textbox && editable)
				popup.open(textbox, "after_start");
		}
		else if (event.getName().equals(Events.ON_OPEN) && event.getTarget() == popup)
		{
			OpenEvent oe = (OpenEvent) event;
			if (!oe.isOpen())
			{
				String value = "";
				for (Checkbox cbx : cbxList)
				{
					if (cbx.isChecked())
						value += cbx.getLabel() + "; ";
				}

				value = value.trim();
				if (value.length() == 0)
					value = PleaseSelect;
				textbox.setValue(value);
				fireValueChange(new ValueChangeEvent(this, null, null, getValues()));
			}
		}
	}
	
	public void addValueChangeListener(ValueChangeListener listener)
	{
		if (listener == null)
			return;

		if (!listeners.contains(listener))
			listeners.add(listener);
	}

	public boolean removeValueChangeListener(ValueChangeListener listener)
	{
		return listeners.remove(listener);
	}

	private void fireValueChange(ValueChangeEvent event)
	{
		// copy to array to avoid concurrent modification exception
		ValueChangeListener[] vcl = new ValueChangeListener[listeners.size()];
		listeners.toArray(vcl);
		for (ValueChangeListener listener : vcl)
		{
			listener.valueChange(event);
		}
	}

}
