package org.adempiere.webui.process;

import java.util.ArrayList;
import java.util.Arrays;

import org.compiere.model.MTable;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;


/**
 * 
 * @author jtrinidad
 *
 */

public class PopulateTSColumn extends SvrProcess {

	private int tableId = 0;
	private ArrayList<String> updatedTable = new ArrayList<String>();

	@Override
	protected void prepare() {
		ProcessInfoParameter[] paras = getParameter();
		
		for ( int i = 0; i < paras.length; i++ )
		{
			if ( paras[i].getParameterName().equals("AD_Table_ID"))
				tableId  = paras[i].getParameterAsInt();
			else 			 
				log.info("Parameter not found " + paras[i].getParameterName());
		}

	}

	@Override
	protected String doIt() throws Exception {
		if (tableId == 0) {
			//load all
			String where = "EXISTS (SELECT * FROM ad_column WHERE isincludeints = 'Y' AND   ad_table_id = ad_table.ad_table_id)";

			int[] ids = MTable.getAllIDs(MTable.Table_Name, where, null);
			for (int id : ids) {
				populateTable(id);
			}
			
		} else {
			populateTable(tableId);
			return "Tokens created for Table=" + MTable.get(getCtx(), tableId).getName();
		}
		
		return "Tokens populated the following tables - " + Arrays.toString(updatedTable.toArray());
	}
	
	private void populateTable(int tableID)
	{
		MTable table = MTable.get(getCtx(), tableID);
	
		table.populateTSColumn();
		updatedTable.add(table.getName());

	}
}