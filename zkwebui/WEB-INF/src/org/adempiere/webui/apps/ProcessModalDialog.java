/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2007 Adempiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *
 * Copyright (C) 2007 Low Heng Sin hengsin@avantz.com
 * _____________________________________________
 *****************************************************************************/
package org.adempiere.webui.apps;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.webui.AdempiereWebUI;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.CWindowToolbar;
import org.adempiere.webui.component.Combobox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.VerticalBox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.WTableDirEditor;
import org.adempiere.webui.event.DialogEvents;
import org.adempiere.webui.panel.ADWindowPanel;
import org.adempiere.webui.process.WProcessInfo;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.ADWindow;
import org.compiere.apps.ProcessCtl;
import org.compiere.model.Lookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MPInstance;
import org.compiere.model.MProcess;
import org.compiere.model.MRole;
import org.compiere.model.X_AD_ReportView;
import org.compiere.print.MPrintFormat;
import org.compiere.process.ProcessInfo;
import org.compiere.util.ASyncProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Ini;
import org.compiere.util.Msg;
import org.zkoss.zk.au.out.AuEcho;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.DesktopUnavailableException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Html;
import org.zkoss.zul.Row;
import org.zkoss.zul.Separator;


/**
 * Modal Dialog to Start process. Displays information about the process and
 * lets the user decide to start it and displays results (optionally print
 * them). Calls ProcessCtl to execute.
 * 
 * @author Low Heng Sin
 * @author arboleda - globalqss - Implement ShowHelp option on processes and
 *         reports
 */
public class ProcessModalDialog extends Window implements EventListener<Event>, DialogEvents
{
	/**
	 * generated serial version ID
	 */
	private static final long	serialVersionUID		= -7109707014309321369L;

	private boolean				m_autoStart;
	private VerticalBox			dialogBody;

	// Print Format
	private WTableDirEditor		fPrintFormat		= null;
	private Combobox			freportType			= new Combobox();
	private Label				lPrintFormat		= new Label("Print Format:");
	private Label				lreportType			= new Label("Report Type:");
	
	private int 		    m_AD_Process_ID;

	private Desktop m_desktop;
		
	/**
	 * @param aProcess
	 * @param WindowNo
	 * @param pi
	 * @param autoStart
	 * @param desktop 
	 */
	public ProcessModalDialog(ASyncProcess aProcess, int WindowNo, ProcessInfo pi, boolean autoStart, Desktop desktop)
	{
		m_ctx = Env.getCtx();
		m_ASyncProcess = aProcess;
		m_WindowNo = WindowNo;
		m_pi = pi;
		m_autoStart = autoStart;
		m_AD_Process_ID = m_pi.getAD_Process_ID();
		m_desktop = desktop;
		if (m_desktop == null) {
			log.severe("ZK Desktop is null.  This will cause a UI error.");
		}
		
		log.info("Process=" + pi.getAD_Process_ID());		
		try
		{
			initComponents();
			init();
		}
		catch(Exception ex)
		{
			log.log(Level.SEVERE, "", ex);
		}
	}
	
	/**
	 * Dialog to start a process/report
	 * @param ctx
	 * @param aProcess
	 * @param WindowNo
	 * @param AD_Process_ID
	 * @param tableId
	 * @param recordId
	 * @param autoStart
	 * @param desktop 
	 */
	public ProcessModalDialog(ASyncProcess aProcess, int WindowNo, int AD_Process_ID, int tableId, int recordId,
			boolean autoStart, Desktop desktop)
	{						
		this(aProcess, WindowNo, new ProcessInfo("", AD_Process_ID, tableId, recordId), autoStart, desktop);		
	}

	/**
	 * Feature #1449
	 * 
	 * @author Sachin Bhimani
	 * @param listener
	 * @param pi
	 * @param WindowNo
	 * @param autoStart
	 */
	public ProcessModalDialog(EventListener<Event> listener, ProcessInfo pi, int WindowNo, boolean autoStart)
	{
		super();

		m_ctx = Env.getCtx();
		m_WindowNo = WindowNo;
		m_pi = pi;
		m_autoStart = autoStart;
		m_AD_Process_ID = m_pi.getAD_Process_ID();
		m_desktop = AEnv.getDesktop();

		if (listener != null)
		{
			addEventListener(ProcessModalDialog.ON_WINDOW_CLOSE, listener);
			addEventListener(ProcessModalDialog.ON_BEFORE_RUN_PROCESS, listener);
		}

		log.info("Process=" + pi.getAD_Process_ID());
		try
		{
			initComponents();
			init(Env.getCtx(), WindowNo, pi.getAD_Process_ID(), pi, "100%", autoStart, true);
		}
		catch (Exception ex)
		{
			log.log(Level.SEVERE, "", ex);
		}
	}

	/**
	 * 
	 * Feature #1449 - Dynamic Init
	 * 
	 * @author Sachin Bhimani
	 * @return true, if there is something to process (start from menu)
	 */
	private boolean init(Properties ctx, int windowNo, int ad_Process_ID, ProcessInfo pi, String string,
			boolean autoStart, boolean b)
	{
		m_ctx = ctx;
		m_WindowNo = windowNo;
		m_AD_Process_ID = ad_Process_ID;
		m_pi = pi;
		m_autoStart = autoStart;

		log.config("");
		//
		boolean trl = !Env.isBaseLanguage(m_ctx, "AD_Process");
		String sql = "SELECT Name, Description, Help, IsReport, ShowHelp " + "FROM AD_Process "
				+ "WHERE AD_Process_ID=?";
		if (trl)
			sql = "SELECT t.Name, t.Description, t.Help, p.IsReport, p.ShowHelp "
					+ "FROM AD_Process p, AD_Process_Trl t " + "WHERE p.AD_Process_ID=t.AD_Process_ID"
					+ " AND p.AD_Process_ID=? AND t.AD_Language=?";

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, m_AD_Process_ID);
			if (trl)
				pstmt.setString(2, Env.getAD_Language(m_ctx));
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				m_Name = rs.getString(1);
				// m_IsReport = rs.getString(4).equals("Y");
				m_ShowHelp = rs.getString(5);
				//
				m_messageText.append("<b>");
				String s = rs.getString(2); // Description
				if (rs.wasNull())
					m_messageText.append(Msg.getMsg(m_ctx, "StartProcess?"));
				else
					m_messageText.append(s);
				m_messageText.append("</b>");

				s = rs.getString(3); // Help
				if (!rs.wasNull())
					m_messageText.append("<p>").append(s).append("</p>");
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql, e);
			return false;
		}
		finally
		{
			DB.close(rs, pstmt);
		}

		if (m_Name == null)
			return false;
		//
		this.setTitle(m_Name);
		message.setContent(m_messageText.toString());

		// Move from APanel.actionButton
		if (m_pi == null)
			m_pi = new WProcessInfo(m_Name, m_AD_Process_ID);
		m_pi.setAD_User_ID(Env.getAD_User_ID(Env.getCtx()));
		m_pi.setAD_Client_ID(Env.getAD_Client_ID(Env.getCtx()));
		parameterPanel = new ProcessParameterPanel(m_WindowNo, m_pi, "100%");
		centerPanel.getChildren().clear();
		if (parameterPanel.init())
		{
			centerPanel.appendChild(parameterPanel);
		}
		else
		{
			if (m_ShowHelp != null && m_ShowHelp.equals("N"))
			{
				startProcess();
			}
		}

		// Check if the process is a silent one
		if (m_ShowHelp != null && m_ShowHelp.equals("S"))
		{
			startProcess();
		}
		// querySaved();
		return true;
	} // init

	/**
	 * @return true if user have press the cancel button to close the dialog
	 */
	public boolean isCancel()
	{
		return m_cancel;
	}

	public int getAD_Process_ID()
	{
		return m_AD_Process_ID;
	}

	private void initComponents()
	{
		this.setBorder("normal");
		dialogBody = new VerticalBox();
		Div div = new Div();
		message = new Html();
		div.appendChild(message);
		div.setStyle("max-height: 150px; min-height:50px; overflow: auto;");
		dialogBody.appendChild(div);
		centerPanel = new Panel();
		dialogBody.appendChild(centerPanel);
		ZKUpdateUtil.setWidth(dialogBody, "100%");
		
		//Print format on report para
		MProcess pr=new MProcess(m_ctx, m_pi.getAD_Process_ID(), null);
		if(pr.getAD_Process_ID() != 202 && pr.isReport() && pr.getJasperReport() == null)
		{
			listPrintFormat();

			Grid grid = GridFactory.newGridLayout();
			Rows rows = grid.newRows();

			div = new Div();
			div.setStyle("text-align: left");
			div.appendChild(grid);
			dialogBody.appendChild(div);

			Row row1 = rows.newRow();
			row1.appendChild(lPrintFormat);
			row1.appendChild(lreportType);

			Row row2 = rows.newRow();
			row2.appendChild(fPrintFormat.getComponent());
			row2.appendChild(freportType);
		}
				
		div = new Div();
		div.setStyle("text-align: right");
		Button btn = null;
//		Hbox hbox = new Hbox();
////		ZKUpdateUtil.setHeight(hbox, "60px");
////		hbox.setSclass("confirm-panel");
//		Button btn = new Button("Ok");
//		LayoutUtils.addSclass("action-text-button", btn);
//		btn.setId("Ok");
//		btn.addEventListener(Events.ON_CLICK, this);
//		hbox.appendChild(btn);
//		
//		btn = new Button("Cancel");
//		btn.setId("Cancel");
//		LayoutUtils.addSclass("action-text-button", btn);
//		btn.addEventListener(Events.ON_CLICK, this);
//		
//		hbox.appendChild(btn);
//		div.appendChild(hbox);

		Panel confParaPanel =new Panel();
		confParaPanel.setClientAttribute("align", "right");
		
		Separator separator = new Separator();
		ZKUpdateUtil.setWidth(separator, "96%");
		separator.setBar(true);
		separator.setStyle("position: absolute; bottom: 42px;");
		confParaPanel.appendChild(separator);
		
		String label = Msg.getMsg(Env.getCtx(), "Start");
		btn = new Button(label.replaceAll("&", ""));
		btn.setIconSclass("z-icon-Ok");
		btn.setId("Ok");
		btn.addEventListener(Events.ON_CLICK, this);
		btn.setSclass("action-button");
		confParaPanel.appendChild(btn);
		
		label = Msg.getMsg(Env.getCtx(), "Cancel");
		btn = new Button(label.replaceAll("&", ""));
		btn.setId("Cancel");
		btn.setIconSclass("z-icon-Cancel");
		btn.addEventListener(Events.ON_CLICK, this);
		btn.setSclass("action-button");
		confParaPanel.appendChild(btn);	
		div.appendChild(confParaPanel);
		dialogBody.appendChild(div);
		this.appendChild(dialogBody);
		
	}

	private ASyncProcess m_ASyncProcess;
	private int m_WindowNo;
	private Properties m_ctx;
	private String		    m_Name = null;
	private StringBuffer	m_messageText = new StringBuffer();
	private String          m_ShowHelp = null; // Determine if a Help Process Window is shown
	private boolean m_valid = true;
	private boolean					m_cancel		= false;
	
	private Panel centerPanel = null;
	private Html message = null;
	
	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(ProcessDialog.class);
	//
	private ProcessParameterPanel parameterPanel = null;
	
	private ProcessInfo m_pi = null;
	private ProgressMonitorDialog progressWindow;
	
	/**
	 * Set Visible (set focus to OK if visible)
	 * 
	 * 	@param visible true if visible
	 */
	public boolean setVisible (boolean visible)
	{
		return super.setVisible(visible);
	}	//	setVisible

	/**
	 *	Dispose
	 */
	public void dispose()
	{		
		parameterPanel.restoreContext();
		m_valid = false;
		this.detach();
	}	//	dispose

	/**
	 * is dialog still valid
	 * 
	 * @return boolean
	 */
	public boolean isValid()
	{
		return m_valid;
	}

	/**
	 *	Dynamic Init
	 * 
	 *  @return true, if there is something to process (start from menu)
	 */
	public boolean init()
	{
		log.config("");
		//
		boolean trl = !Env.isBaseLanguage(m_ctx, "AD_Process");
		String sql = "SELECT Name, Description, Help, IsReport, ShowHelp " + "FROM AD_Process "
				+ "WHERE AD_Process_ID=?";
		if (trl)
			sql = "SELECT t.Name, t.Description, t.Help, p.IsReport, p.ShowHelp "
					+ "FROM AD_Process p, AD_Process_Trl t " + "WHERE p.AD_Process_ID=t.AD_Process_ID"
				+ " AND p.AD_Process_ID=? AND t.AD_Language=?";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, m_pi.getAD_Process_ID());
			if (trl)
				pstmt.setString(2, Env.getAD_Language(m_ctx));
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				m_Name = rs.getString(1);
				m_ShowHelp = rs.getString(5);
				//
				m_messageText.append("<b>");
				String s = rs.getString(2);		//	Description
				if (rs.wasNull())
					m_messageText.append(Msg.getMsg(m_ctx, "StartProcess?"));
				else
					m_messageText.append(s);
				m_messageText.append("</b>");
				
				s = rs.getString(3);			//	Help
				if (!rs.wasNull())
					m_messageText.append("<p>").append(s).append("</p>");
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql, e);
			return false;
		}
		finally
		{
			DB.close(rs, pstmt);
		}

		if (m_Name == null)
			return false;
		//
		this.setTitle(m_Name);
		message.setContent(m_messageText.toString());
		
		//	Move from APanel.actionButton
		m_pi.setAD_User_ID (Env.getAD_User_ID(Env.getCtx()));
		m_pi.setAD_Client_ID(Env.getAD_Client_ID(Env.getCtx()));
		m_pi.setTitle(m_Name);
		parameterPanel = new ProcessParameterPanel(m_WindowNo, m_pi);
		centerPanel.getChildren().clear();
		if (parameterPanel.init())
		{
			centerPanel.appendChild(parameterPanel);
		}
		else
		{
			if (m_ShowHelp != null && m_ShowHelp.equals("N"))
			{
				m_autoStart = true;
			}
			if (m_autoStart)
			{
				this.getFirstChild().setVisible(false);
				startProcess();
				return true;
			}
		}
		
		// Check if the process is a silent one
		if(isValid() && m_ShowHelp != null && m_ShowHelp.equals("S"))
		{
			this.getFirstChild().setVisible(false);
			startProcess();			
		}
		return true;
	}	//	init

	/**
	 * launch process
	 */
	private void startProcess()
	{			
		m_pi.setPrintPreview(true);
		m_pi.setWindowNo(m_WindowNo);
		if (m_desktop == null) {
			this.lockUI(m_pi);
			Clients.response(new AuEcho(this, "runProcess", null));
			return;
		}
		if (!m_desktop.isServerPushEnabled()) {
			m_desktop.enableServerPush(true);
		}

		Events.postEvent(ProcessModalDialog.ON_BEFORE_RUN_PROCESS, this, null);
		this.lockUI(m_pi);
		Runnable runnable = new Runnable() {
			public void run() {
				//get full control of desktop
				org.zkoss.zk.ui.Desktop desktop = m_desktop;
				try {
					try {             
						if (progressWindow != null) {
							progressWindow.fireTimer(Thread.currentThread());
						}
//						ReportCtl.setDesktop(desktop);
						ProcessCtl.process(null, m_WindowNo, parameterPanel, m_pi, null);
					
					} catch (Exception e) {
						log.severe(e.getLocalizedMessage());
					
					} finally{
						if (SessionManager.grantAccess(desktop)) {
							unlockUI(m_pi);
							SessionManager.releaseDesktop(desktop);
							
						}
					}
				} catch (DesktopUnavailableException e) {
					log.log(Level.SEVERE, e.getLocalizedMessage(), e);
				}												
			}			
		};
		DesktopRunnable dr = new DesktopRunnable(runnable, m_desktop);
		new Thread(dr).start();
	}
	

	public void lockUI(ProcessInfo pi) {

		m_desktop.setAttribute(AdempiereWebUI.PROCESS_IN_PROGRESS, "Y");
		showBusyDialog();

		if (m_ASyncProcess != null) {
			m_ASyncProcess.lockUI(m_pi);
//			Clients.showBusy(null);
		}
	}


	public void unlockUI(ProcessInfo pi) 
	{

		m_desktop.setAttribute(AdempiereWebUI.PROCESS_IN_PROGRESS, "N");
		
		//updateUI(pi);
		dispose();
		hideBusyDialog();

		if (m_ASyncProcess != null) {
			m_ASyncProcess.unlockUI(m_pi);
		} 
	}

	private void showBusyDialog()
	{
		this.setBorder("none");
		this.setTitle(null);
		//dialogBody.setVisible(false);
		this.getChildren().clear();
		this.setShadow(false);
		this.setZclass("process-dialog");
		progressWindow = new ProgressMonitorDialog(m_pi, m_desktop);
		progressWindow.setStyle("background-color: #1f9bde; border: 5px solid #09437b;");
		this.appendChild(progressWindow);
	}
	
	/**
	 * internal use, don't call this directly
	 */
	public void runProcess()
	{
		log.severe("runProcess NOT SUPPORTED");
/*		Events.sendEvent(this, new Event(ON_BEFORE_RUN_PROCESS, this, null));

		try
		{
			if (ReportCtl.getDesktop() == null) {
				org.zkoss.zk.ui.Desktop desktop = m_desktop;
				if (desktop == null) {
					log.severe("NULL desktop will cause an error");
				}
				ReportCtl.setDesktop(desktop);
			}
			ProcessCtl.process(null, m_WindowNo, parameterPanel, m_pi, null);					
		}
		finally
		{
			dispose();
			if (m_ASyncProcess != null)
			{
				m_ASyncProcess.unlockUI(m_pi);
			} 
			hideBusyDialog();
			
			Events.sendEvent(this, new Event(ON_WINDOW_CLOSE, this, null));
		}*/
	}

	private void hideBusyDialog()
	{
		if (progressWindow != null)
		{
			progressWindow.dispose();
			progressWindow = null;
		}
		//Trigger Tab to refresh on completion of the process - jobrian		
		Object obj = SessionManager.getAppDesktop().findWindow(m_WindowNo);
		if (obj != null && obj instanceof ADWindow)
		{
			ADWindow win = (ADWindow) obj;
			ADWindowPanel winPanel = win.getADWindowPanel();
			if (winPanel != null && winPanel.getActiveGridTab() != null)
			{
//				win.getADWindowPanel().onRefresh();
				win.getADWindowPanel().curTabpanel.getGridTab().dataRefresh();
			}
		}
	}
	
	/**
	 * handle events
	 */
	public void onEvent(Event event)
	{
		Component component = event.getTarget();
		if (component instanceof Button)
		{
			Button element = (Button) component;
			if ("Ok".equalsIgnoreCase(element.getId()))
			{
				// Print format on report para
				if (freportType != null && freportType.getSelectedItem() != null
						&& freportType.getSelectedItem().getValue() != null)
				{
					m_pi.setReportType(freportType.getSelectedItem().getValue().toString());
				}
				if (fPrintFormat != null && fPrintFormat.getValue() != null)
				{
					MPrintFormat format = new MPrintFormat(m_ctx, (Integer) fPrintFormat.getValue(), null);
					if (format != null)
					{
						if (Ini.isClient())
							m_pi.setTransientObject(format);
						else
							m_pi.setSerializableObject(format);
					}
				}
				
				
				onOK();
			}
			else if ("Cancel".equalsIgnoreCase(element.getId()))
			{
				cancelProcess();
			}
		}
	}
	
	private void onOK() {
		this.startProcess();
	}

	protected void cancelProcess()
	{
		m_cancel = true;
		this.dispose();
	}
	
	/**
	 * 
	 * @return ProcessInfo
	 */
	public ProcessInfo getProcessInfo() {
		return m_pi;
	}

	private void listPrintFormat()
	{
		int AD_Column_ID = 0;
		int table_ID = 0;
		MProcess pr = new MProcess(m_ctx, m_AD_Process_ID, null);

		try
		{
			if (pr.getAD_ReportView_ID() > 0)
			{
				X_AD_ReportView m_Reportview = new X_AD_ReportView(m_ctx, pr.getAD_ReportView_ID(), null);
				table_ID = m_Reportview.getAD_Table_ID();
			}
			else if (pr.getAD_PrintFormat_ID() > 0)
			{
				MPrintFormat format = new MPrintFormat(m_ctx, pr.getAD_PrintFormat_ID(), null);
				table_ID = format.getAD_Table_ID();
			}

			String valCode = null;
			if (table_ID > 0)
			{
				valCode = "AD_PrintFormat.AD_Table_ID=" + table_ID;
			}

			Lookup lookup = MLookupFactory.get(Env.getCtx(), m_WindowNo, AD_Column_ID, DisplayType.TableDir,
					Env.getLanguage(Env.getCtx()), "AD_PrintFormat_ID", 0, false, valCode);

			fPrintFormat = new WTableDirEditor("AD_PrintFormat_ID", false, false, true, lookup);
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, e.getMessage());
		}

		MRole roleCurrent = MRole.get(Env.getCtx(), Env.getAD_Role_ID(Env.getCtx()));
		boolean m_isAllowHTMLView = roleCurrent.isAllow_HTML_View();
		boolean m_isAllowXLSView = roleCurrent.isAllow_XLS_View();

		freportType.removeAllItems();
		if (m_isAllowHTMLView) {
			freportType.appendItem("HTML", "H");
		}
		freportType.appendItem("PDF", "P");
		if (m_isAllowXLSView) {
			freportType.appendItem("XLS", "X");
			freportType.appendItem("XLSX","XX");
		}
		freportType.setSelectedIndex(0);

	}
	
	public void setLinkID(String linkID)
	{
		Env.setContext(Env.getCtx(), m_WindowNo, WExecute.LINK_ID, linkID);
	}
	
	public void preloadParameter(MPInstance instance)
	{
		parameterPanel.loadParameters(instance);
	}
	
	public void overrideParameter(int AD_Column_ID, String value)
	{
		parameterPanel.overrideParameter(AD_Column_ID, value);
	}
	
	public void setDrillSource(String source)
	{
		m_pi.setDrillSource(source);
	}
	
	/**
	 * Start process without showing process dialog and locking desktop.
	 */
	private void startAsyncProcess()
	{
		try
		{
			WProcessCtl.process(m_WindowNo, parameterPanel, m_pi, m_desktop, null);
		}
		catch (Exception e)
		{
			log.severe(e.getLocalizedMessage());
		}
		finally
		{
			try
			{
				if (SessionManager.activateDesktop(m_desktop))
				{
					// dispose current dialog pop-up
					cancelProcess();
					// Get current window toolbar panel to highlight the process is running and disabling toolbar events
					Object obj = SessionManager.getAppDesktop(m_desktop).findWindow(m_WindowNo);
					if (obj != null && obj instanceof ADWindow)
					{
						ADWindow win = (ADWindow) obj;
						if (win.getADWindowPanel() != null && win.getADWindowPanel().getActiveGridTab() != null)
						{
							win.getADWindowPanel().getToolbar().setVisible(CWindowToolbar.BTN_PROCESSING, true);
							win.getADWindowPanel().getToolbar().setStyle("pointer-events: none; opacity: 0.5;");
							win.getADWindowPanel().onRefresh(false);
						}
					}
					SessionManager.releaseDesktop(m_desktop);
				}
			}
			catch (DesktopUnavailableException e)
			{
				log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			}
		}
	} // startAsyncProcess
	
}	//	ProcessDialog
