/******************************************************************************
 * Copyright (C) 2009 Low Heng Sin                                            *
 * Copyright (C) 2009 Idalica Corporation                                     *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.webui.apps.form;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import org.adempiere.util.Callback;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.BusyDialog;
import org.adempiere.webui.apps.ProcessModalDialog;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.DesktopTabpanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Tab;
import org.adempiere.webui.component.Tabbox;
import org.adempiere.webui.component.Tabpanel;
import org.adempiere.webui.component.Tabpanels;
import org.adempiere.webui.component.Tabs;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.InfoPanel;
import org.adempiere.webui.panel.StatusBarPanel;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.adempiere.webui.window.SimplePDFViewer;
import org.compiere.apps.ProcessCtl;
import org.compiere.apps.form.InOutGenForm;
import org.compiere.model.MPInstancePara;
import org.compiere.model.MQuery;
import org.compiere.model.MTable;
import org.compiere.model.PrintInfo;
import org.compiere.model.Query;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportCtl;
import org.compiere.print.ReportEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoUtil;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.au.out.AuEcho;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Div;
import org.zkoss.zul.Html;
import org.zkoss.zul.North;
import org.zkoss.zul.South;

/**
 * Generate custom form window
 * 
 */
public class WInOutGenForm extends ADForm implements WTableModelListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8669256486969882958L;

	private InOutGenForm genForm;
	
	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(WInOutGenForm.class);
	//
	private Tabbox tabbedPane = new Tabbox();
	private Borderlayout selPanel = new Borderlayout();


	private Grid selNorthPanel = GridFactory.newGridLayout();
	private ConfirmPanel confirmPanelSel = new ConfirmPanel(true, true, false, false, true, false, false);
	private ConfirmPanel confirmPanelGen = new ConfirmPanel(false, true, false, false, false, false, false);
	private StatusBarPanel statusBar = new StatusBarPanel();
	private Borderlayout genPanel = new Borderlayout();
	private Html info = new Html();
	private WListbox miniTable = ListboxFactory.newDataTable();
	private WListbox tableProductRequired = ListboxFactory.newDataTable();


	private BusyDialog progressWindow;
	
	Tabbox tabbedInfoPane = new Tabbox();
	WListbox contendBPTbl = ListboxFactory.newDataTable();
	
	private boolean tableOnQuery;
	private boolean onSelectionUpdate;

	private Set<Integer> _selItems = null;

	private int[] m_ids;
	
	public WInOutGenForm(InOutGenForm genForm)
	{
		log.info("");
		this.genForm = genForm;
	}
	
	@Override
	protected void initForm() 
	{
		try
		{
			zkInit();
			dynInit();
			Borderlayout contentPane = new Borderlayout();
			this.appendChild(contentPane);
			ZKUpdateUtil.setWidth(contentPane, "99%");
			ZKUpdateUtil.setHeight(contentPane, "100%");
			Center center = new Center();
			center.setStyle("border: none");
			contentPane.appendChild(center);
			center.appendChild(tabbedPane);
			ZKUpdateUtil.setWidth(tabbedPane, "99%");
			ZKUpdateUtil.setHeight(tabbedPane, "100%");

			South south = new South();
			south.setStyle("border: none");
			contentPane.appendChild(south);
			south.appendChild(statusBar);
			LayoutUtils.addSclass("status-border", statusBar);
			ZKUpdateUtil.setHeight(south, "32px");
		}
		catch(Exception ex)
		{
			log.log(Level.SEVERE, "init", ex);
		}
	}	//	init
	
	/**
	 *	Static Init.
	 *  <pre>
	 *  selPanel (tabbed)
	 *      fOrg, fBPartner
	 *      scrollPane & miniTable
	 *  genPanel
	 *      info
	 *  </pre>
	 *  @throws Exception
	 */
	void zkInit() throws Exception
	{
		//
		ZKUpdateUtil.setWidth(selPanel, "99%");
		ZKUpdateUtil.setHeight(selPanel, "95%");
		selPanel.setStyle("border: none; position: absolute");
		DesktopTabpanel tabpanel = new DesktopTabpanel();
		ZKUpdateUtil.setHeight(tabpanel, "95%");
		tabpanel.appendChild(selPanel);
		Tabpanels tabPanels = new Tabpanels();
		ZKUpdateUtil.setHeight(tabPanels, "95%");
		tabPanels.appendChild(tabpanel);
		tabbedPane.appendChild(tabPanels);
		Tabs tabs = new Tabs();
		tabbedPane.appendChild(tabs);
		Tab tab = new Tab(Msg.getMsg(Env.getCtx(), "Select"));
		tabs.appendChild(tab);
		
		North north = new North();
		north.setAutoscroll(true);
		selPanel.appendChild(north);
		north.appendChild(selNorthPanel);
		
		South south = new South();
		ZKUpdateUtil.setHeight(south, "34px");
//		south.setCollapsible(true);
//		south.setSplittable(true);
//		ZKUpdateUtil.setVflex(south, "flex");
//		south.setTitle("Additional Information");
//		south.setTooltiptext("Additional Information that User can Show or Hide");
		
		selPanel.appendChild(south);
		south.appendChild(confirmPanelSel);
		
		Center center = new Center();  //contains 2 section
		Borderlayout centerPanel = new Borderlayout();
		South vboxSouth = new South();
		ZKUpdateUtil.setHeight(vboxSouth, "220px");
		vboxSouth.setCollapsible(true);
		vboxSouth.setSplittable(true);
		vboxSouth.setTitle("Additional Information");
		vboxSouth.setTooltiptext("Additional Information that User can Show or Hide");
		
		selPanel.appendChild(center);
		center.appendChild(centerPanel);
		Center tableCenter = new Center();
		ZKUpdateUtil.setVflex(tableCenter, "flex");
		tableCenter.appendChild(miniTable);
		centerPanel.appendChild(tableCenter);
		centerPanel.appendChild(vboxSouth);

		vboxSouth.appendChild(tabbedInfoPane);
//		vboxSouth.appendChild(tableProductRequired);
		ZKUpdateUtil.setVflex(center, "flex");
		ZKUpdateUtil.setHeight(miniTable, "99%");
		confirmPanelSel.addActionListener(this);
		//
		tabpanel = new DesktopTabpanel();
		tabPanels.appendChild(tabpanel);
		tabpanel.appendChild(genPanel);
		tab = new Tab(Msg.getMsg(Env.getCtx(), "Generate"));
		tabs.appendChild(tab);
		ZKUpdateUtil.setWidth(genPanel, "99%");
		ZKUpdateUtil.setHeight(genPanel, "95%");
		genPanel.setStyle("border: none; position: absolute");
		center = new Center();
		genPanel.appendChild(center);
		Div div = new Div();
		div.appendChild(info);
		center.appendChild(div);
		south = new South();
		genPanel.appendChild(south);
		south.appendChild(confirmPanelGen);
		confirmPanelSel.getButton(ConfirmPanel.A_HISTORY).setTooltiptext("Orders On Hold");
		confirmPanelGen.addActionListener(this);		
		
		ZKUpdateUtil.setHeight(tabbedInfoPane, "100%");
//		Tabpanels tabInfoPanels = new Tabpanels();
//		tabbedInfoPane.appendChild(tabInfoPanels);
//		Tabs tabsInfo = new Tabs();
//		tabbedInfoPane.appendChild(tabsInfo);
//
//		Tab tabInfo = new Tab(Util.cleanAmp(Msg.translate(Env.getCtx(), "Warehouse")));
//		tabInfo.setHeight("100%");
//		tabsInfo.appendChild(tabInfo);
//		Tabpanel desktopTabPanel = new Tabpanel();
//		desktopTabPanel.setHeight("100%");
//		desktopTabPanel.appendChild(tableProductRequired);
//		tabPanels.appendChild(desktopTabPanel);
		
//		tabbedInfoPane.appendChild(tableProductRequired);

	    infoInit();
	}	//	jbInit

	public void infoInit() {
		Tabpanels tabPanels = new Tabpanels();
		Tabpanel panel1 = new Tabpanel();
		panel1.appendChild(tableProductRequired);
		tabPanels.appendChild(panel1);
		Tabpanel panel2 = new Tabpanel();
		panel1.appendChild(tableProductRequired);
		tabPanels.appendChild(panel1);
		tabPanels.appendChild(panel2);
		
		Tabs tabs = new Tabs();
		
		Tab tab1 = new Tab("Products Required");
		Tab tab2 = new Tab("Orders with no contention");
		//Tab tab3 = new Tab("Products Required");
		tabs.appendChild(tab1);
		//tabs.appendChild(tab2);
		
		tabbedInfoPane.appendChild(tabPanels);
		tabbedInfoPane.appendChild(tabs);
		
	}
	/**
	 *	Dynamic Init.
	 *	- Create GridController & Panel
	 *	- AD_Column_ID from C_Order
	 */
	public void dynInit()
	{
		genForm.configureMiniTable(miniTable);
		miniTable.getModel().addTableModelListener(this);

		//	Info
		statusBar.setStatusDB(" ");
		//	Tabbed Pane Listener
		tabbedPane.addEventListener(Events.ON_SELECT, this);
		
		_selItems = new LinkedHashSet<Integer>();
		
		genForm.configureRequiredProductTable(tableProductRequired);
	}	//	dynInit

	public void postQueryEvent() 
    {
		Clients.showBusy(Msg.getMsg(Env.getCtx(), "Processing"));
    	Events.echoEvent("onExecuteQuery", this, null);
    }
    
    /**
     * Dont call this directly, use internally to handle execute query event 
     */
    public void onExecuteQuery()
    {
    	try
    	{
    		tableOnQuery = true;
    		genForm.executeQuery();
    	}
    	finally
    	{
    		Clients.clearBusy();
        	tableOnQuery = false;
    	}
    }
    
	/**
	 *	Action Listener
	 *  @param e event
	 */
	public void onEvent(Event e)
	{
		log.info("Cmd=" + e.getTarget().getId());
		//
		if (e.getTarget().getId().equals(ConfirmPanel.A_CANCEL))
		{
			dispose();
			return;
		}
		else if (e.getTarget().getId().equals(ConfirmPanel.A_REFRESH))
		{
			postQueryEvent();
			return;
		}
		else if (e.getTarget().getId().equals(ConfirmPanel.A_HISTORY))
		{
			InfoPanel.showOrderOnCredit(m_WindowNo, null);
			return;
		}
		else if (e.getTarget() instanceof Tab)
		{
			int index = tabbedPane.getSelectedIndex();
			genForm.setSelectionActive(index == 0);
			return;
		}
		
		genForm.validate();
	}	//	actionPerformed

	/**
	 *  Table Model Listener
	 *  @param e event
	 */
	public void tableChanged(WTableModelEvent e)
	{
		if (!tableOnQuery)  // don't run when loading
		{
			int rowsSelected = miniTable.getSelectedCount();
			statusBar.setStatusDB(" " + rowsSelected + " ");

		}
	}   //  tableChanged


	/**
	 *	Save Selection & return selecion Query or ""
	 *  @return where clause like C_Order_ID IN (...)
	 */
	public void saveSelection()
	{
		genForm.saveSelection(miniTable);
	}	//	saveSelection

	
	/**************************************************************************
	 *	Generate Shipments
	 */
	public void generate()
	{
		info.setContent(genForm.generate());		
		boolean isCreateManifest = false;
		ProcessInfo pi = genForm.getProcessInfo();
		
		List<MPInstancePara> params = new Query(Env.getCtx(), MPInstancePara.Table_Name, "ad_pinstance_id=?", genForm.getTrx().getTrxName())
		.setParameters(pi.getAD_PInstance_ID())
		.setOrderBy(MPInstancePara.COLUMNNAME_SeqNo)
		.list();
		for (MPInstancePara param : params) {
			if (param.getParameterName().equals("IsUseManifest")) {
				isCreateManifest = "Y".equals(param.getP_String());
				break;
			}
		}
		if (isCreateManifest) {
			ProcessModalDialog para = new ProcessModalDialog(null,m_WindowNo, pi, false, this.getDesktop());
			if (para.isValid()) {
				try {
					DB.commit(true, genForm.getTrx().getTrxName());
					para.setTitle("Additional Parameters to Create Manifest");
					ZKUpdateUtil.setWidth(para, "500px");
					para.setVisible(true);
					para.setPosition("center");
					AEnv.showWindow(para);
					unlockUI();
				} catch (Exception e) {
				}
			}

		} else {
			this.lockUI();
			Clients.response(new AuEcho(this, "runProcess", null));		
			
		}
	}	//	generate

	/**
	 * Internal use, don't call this directly
	 */
	public void runProcess() 
	{
		final ProcessCtl worker = new ProcessCtl(null, getWindowNo(), genForm.getProcessInfo(), genForm.getTrx());
		try {                    						
			worker.run();     //  complete tasks in unlockUI / generateShipments_complete						
		} finally{						
			unlockUI();
		}
	}
	
	/**
	 *  Complete generating shipments.
	 *  Called from Unlock UI
	 *  @param pi process info
	 */
	private void generateComplete ()
	{
		if (progressWindow != null) {
			progressWindow.dispose();
			progressWindow = null;
		}
		
		//  Switch Tabs
		tabbedPane.setSelectedIndex(1);
		//
		ProcessInfoUtil.setLogFromDB(genForm.getProcessInfo());
		StringBuffer iText = new StringBuffer();
		iText.append("<b>").append(genForm.getProcessInfo().getSummary())
			.append("</b><br>(")
			.append(Msg.getMsg(Env.getCtx(), genForm.getTitle()))
			//  Shipments are generated depending on the Delivery Rule selection in the Order
			.append(")<br>")
			.append(genForm.getProcessInfo().getLogInfo(true));
		info.setContent(iText.toString());

		//	Get results
		int[] ids = genForm.getProcessInfo().getIDs();
		if (ids == null || ids.length == 0)
			return;
		log.config("PrintItems=" + ids.length);
		
		m_ids = ids;
		Clients.response(new AuEcho(this, "onAfterProcess", null));
		
	}   //  generateShipments_complete
	
	
	public void onAfterProcess()
	{
		//	OK to print
		FDialog.ask(getWindowNo(), this, genForm.getAskPrintMsg(), new Callback<Boolean>() {

			@Override
			public void onCallback(Boolean result) 
			{
				if (result) 
				{
					Clients.showBusy("Processing...");
					Clients.response(new AuEcho(WInOutGenForm.this, "onPrint", null));
				}

			}
		});
	}
	
	public void onPrint() 
	{
//		Loop through all items
		List<File> pdfList = new ArrayList<File>();
		for (int i = 0; i < m_ids.length; i++)
		{
			int RecordID = m_ids[i];
			ReportEngine re = null;
			
			if(genForm.getPrintFormat() != null)
			{
				MPrintFormat format = genForm.getPrintFormat();
				MTable table = MTable.get(Env.getCtx(),format.getAD_Table_ID());
				MQuery query = new MQuery(table.getTableName());
				query.addRestriction(table.getTableName() + "_ID", MQuery.EQUAL, RecordID);
				//	Engine
				PrintInfo info = new PrintInfo(table.getTableName(),table.get_Table_ID(), RecordID);               
				re = new ReportEngine(Env.getCtx(), format, query, info);
			}
			else
			{	
				re = ReportEngine.get (Env.getCtx(), genForm.getReportEngineType(), RecordID);
				if (re.getPrintFormat() != null) {
					String printerName = re.getPrintFormat().getPrinterName();
					if (printerName != null) {
						Clients.clearBusy();
						ReportCtl.createOutput(re, false, printerName);
						continue;
					}
				}
			}	
			
			pdfList.add(re.getPDF());				
		}
		
		if (pdfList.size() > 1) {
			try {
				File outFile = File.createTempFile(genForm.getClass().getName(), ".pdf");					
				AEnv.mergePdf(pdfList, outFile);

				Clients.clearBusy();
				Window win = new SimplePDFViewer(getFormName(), new FileInputStream(outFile));
				SessionManager.getAppDesktop().showWindow(win, "center");
			} catch (Exception e) {
				log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			}
		} else if (pdfList.size() == 1) {
			Clients.clearBusy();
			try {
				Window win = new SimplePDFViewer(getFormName(), new FileInputStream(pdfList.get(0)));
				SessionManager.getAppDesktop().showWindow(win, "center");
			} catch (Exception e)
			{
				log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			}
		}
	}

	/**************************************************************************
	 *  Lock User Interface.
	 *  Called from the Worker before processing
	 *  @param pi process info
	 */
	public void lockUI ()
	{
		progressWindow = new BusyDialog();
		progressWindow.setPage(this.getPage());
		progressWindow.doHighlighted();
	}   //  lockUI

	/**
	 *  Unlock User Interface.
	 *  Called from the Worker when processing is done
	 *  @param pi result of execute ASync call
	 */
	public void unlockUI ()
	{		
		generateComplete();
	}   //  unlockUI
	
	public void dispose() {
		SessionManager.getAppDesktop().closeActiveWindow();
	}
	
	public Grid getParameterPanel()
	{
		return selNorthPanel;
	}
	
	public WListbox getMiniTable()
	{
		return miniTable;
	}
	
	public WListbox getTableProductRequired() {
		return tableProductRequired;
	}
	
	public StatusBarPanel getStatusBar()
	{
		return statusBar;
	}
	
	public boolean isTableOnQuery() {
		return tableOnQuery;
	}

	public void setTableOnQuery(boolean tableOnQuery) {
		this.tableOnQuery = tableOnQuery;
	}
}
