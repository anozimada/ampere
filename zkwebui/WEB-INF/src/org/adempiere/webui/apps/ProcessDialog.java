package org.adempiere.webui.apps;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.util.Callback;
import org.adempiere.util.IProcessUI;
import org.adempiere.webui.AdempiereWebUI;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Combobox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.desktop.IDesktop;
import org.adempiere.webui.editor.WTableDirEditor;
import org.adempiere.webui.process.WProcessInfo;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.adempiere.webui.window.SimplePDFViewer;
import org.compiere.apps.ProcessCtl;
import org.compiere.model.Lookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MPInstance;
import org.compiere.model.MPInstancePara;
import org.compiere.model.MProcess;
import org.compiere.model.MRole;
import org.compiere.model.MTable;
import org.compiere.model.X_AD_ReportView;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoUtil;
import org.compiere.util.AdempiereSystemError;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Ini;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.zkoss.zk.au.out.AuEcho;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.DesktopUnavailableException;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Html;
import org.zkoss.zul.North;
import org.zkoss.zul.South;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2007 Low Heng Sin											  *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/



/**
 *	Dialog to Start process or report.
 *	Displays information about the process
 *		and lets the user decide to start it
 *  	and displays results (optionally print them).
 *  Calls ProcessCtl to execute.
 *  @author 	Low Heng Sin
 *  @author     arboleda - globalqss
 *  - Implement ShowHelp option on processes and reports
 */
public class ProcessDialog extends Window implements EventListener<Event>//, ASyncProcess
{
	/**
	 * generate serial version ID
	 */
	private static final long serialVersionUID = 5545731871518761455L;
	private static final String MESSAGE_DIV_STYLE = "line-height:16px;  font-size:16px; margin:10px;";	
	private Div messageDiv;
	private Center center;
	private North north;

	private Grid southRowPanel = GridFactory.newGridLayout();
	
	/**
	 * Dialog to start a process/report
	 * @param ctx
	 * @param parent
	 * @param title
	 * @param aProcess
	 * @param WindowNo
	 * @param AD_Process_ID
	 * @param tableId
	 * @param recordId
	 * @param autoStart
	 */
	public ProcessDialog (int AD_Process_ID, boolean isSOTrx)
	{
		
		log.info("Process=" + AD_Process_ID );
		m_ctx = Env.getCtx();
		m_WindowNo = SessionManager.getAppDesktop().registerWindow(this);
		this.setAttribute(IDesktop.WINDOWNO_ATTRIBUTE, m_WindowNo);
		m_AD_Process_ID = AD_Process_ID;
		Env.setContext(Env.getCtx(), m_WindowNo, "IsSOTrx", isSOTrx ? "Y" : "N");
		try
		{
			initComponents();
			init();
		}
		catch(Exception ex)
		{
			log.log(Level.SEVERE, "", ex);
		}
	}	//	ProcessDialog

	private void initComponents() {
		this.setStyle("position: absolute; width: 100%; height: 100%");
		Borderlayout layout = new Borderlayout();
//		layout.setStyle("position: absolute; width: 100%; height: 100%; border: none; overflow: auto;");
		messageDiv = new Div();
		message = new Html();
		messageDiv.appendChild(message);
		messageDiv.setStyle(MESSAGE_DIV_STYLE);
		
		north = new North();
		north.appendChild(messageDiv);
		north.setStyle("max-height: 155px");
		layout.appendChild(north);
		ZKUpdateUtil.setHeight(north, "auto");
		north.setAutoscroll(true);
//		north.setStyle("border: none;");
		
		centerPanel = new Panel();
		center = new Center();
		center.setStyle("overflow-y:auto");
		layout.appendChild(center);
		center.appendChild(centerPanel);
//		ZKUpdateUtil.setVflex(center, "flex");
//		center.setAutoscroll(true);
//		center.setStyle("border: none");
		
		Rows rows = southRowPanel.newRows();
		Row row1 = rows.newRow();
		Row row = rows.newRow();
		
		
		Hbox hBox = new Hbox();
		Hbox hBox1 = new Hbox();
		
		//Panel saveParaPanel =new Panel();
		//saveParaPanel.setAlign("left");
		
		hBox1.appendChild(lSaved);
		fSavedName.addEventListener(Events.ON_CHANGE, this);
		hBox.appendChild(fSavedName);
		
		bSave.setEnabled(false);
		bSave.setIconSclass("z-icon-Save");
		bSave.setSclass("action-button");
		bSave.addActionListener(this);
		hBox.appendChild(bSave);
		
		bDelete.setEnabled(false);
		bDelete.setIconSclass("z-icon-Delete");
		bDelete.setSclass("action-button");
		bDelete.addActionListener(this);
		hBox.appendChild(bDelete);

		row1.appendCellChild(hBox1, 2);
		row.appendCellChild(hBox, 2);
		
		//Print format on report para
		MProcess pr=new MProcess(m_ctx, m_AD_Process_ID, null);
		if(pr.isReport() && pr.getJasperReport() == null)
		{
			listPrintFormat();
		
			hBox = new Hbox();
			hBox1 = new Hbox();
			hBox1.appendChild(lPrintFormat);
			hBox.appendChild(fPrintFormat.getComponent());
			row1.appendChild(hBox1);
			row.appendChild(hBox);
			
			hBox = new Hbox();
			hBox1 = new Hbox();
			hBox1.appendChild(lreportType);
			hBox.appendChild(freportType);		
		
			row1.appendChild(hBox1);
			row.appendChild(hBox);
		
		}
		
		Panel confParaPanel =new Panel();
		confParaPanel.setClientAttribute("align", "right");
		
		String label = Msg.getMsg(Env.getCtx(), "Start");
		bOK = new Button(label.replaceAll("&", ""));
		bOK.setIconSclass("z-icon-Ok");
		bOK.setId("Ok");
		bOK.addEventListener(Events.ON_CLICK, this);
		bOK.setSclass("action-button");
		confParaPanel.appendChild(bOK);
		
		label = Msg.getMsg(Env.getCtx(), "Cancel");
		bCancel = new Button(label.replaceAll("&", ""));
		bCancel.setIconSclass("z-icon-Cancel");
		bCancel.addEventListener(Events.ON_CLICK, this);
		bCancel.setSclass("action-button");
		confParaPanel.appendChild(bCancel);	
		row.appendChild(confParaPanel);
	
		
		
		South south = new South();
		layout.appendChild(south);
		south.appendChild(southRowPanel);		
		
		this.appendChild(layout);
	}

	private int m_WindowNo;
	private Properties m_ctx;
	private int 		    m_AD_Process_ID;
	private String		    m_Name = null;
	
	// private boolean		    m_IsReport = false;
	private int[]		    m_ids = null;
	private StringBuffer	m_messageText = new StringBuffer();
	private String          m_ShowHelp = null; // Determine if a Help Process Window is shown
	
	private Panel centerPanel = null;
	private Html message = null;
	private Button bOK = null;
	private Button bCancel = null;
	
	
	private boolean valid = true;
	
	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(ProcessDialog.class);
	//
	private ProcessParameterPanel parameterPanel = null;
	
	private ProcessInfo m_pi = null;
	private boolean m_isLocked = false;
	private boolean isResult;
	private String initialMessage;
	private ProgressMonitorDialog progressWindow;
	
	//saved paramaters
	
	private Combobox fSavedName=new Combobox();
	private Button bSave=new Button("");
	private Button bDelete=new Button("");
	private List<MPInstance> savedParams;
	private Label lSaved=new Label(Msg.getMsg(Env.getCtx(), "SavedParameter"));
	
	//Print Format
	
	private WTableDirEditor fPrintFormat=null;
	private Combobox freportType=new Combobox();
	private Label lPrintFormat = new Label("Print Format:");
	private Label lreportType = new Label("Report Type:");
	private Desktop m_desktop = null; 
	
	
	/**
	 * 	Set Visible 
	 * 	(set focus to OK if visible)
	 * 	@param visible true if visible
	 */
	public boolean setVisible (boolean visible)
	{
		return super.setVisible(visible);
	}	//	setVisible

	/**
	 *	Dispose
	 */
	public void dispose()
	{
		SessionManager.getAppDesktop().closeWindow(m_WindowNo);
		valid = false;
	}//	dispose

	/**
	 *	Dynamic Init
	 *  @return true, if there is something to process (start from menu)
	 */
	public boolean init()
	{
		log.config("");
		//
		boolean trl = !Env.isBaseLanguage(m_ctx, "AD_Process");
		String sql = "SELECT Name, Description, Help, IsReport, ShowHelp "
				+ "FROM AD_Process "
				+ "WHERE AD_Process_ID=?";
		if (trl)
			sql = "SELECT t.Name, t.Description, t.Help, p.IsReport, p.ShowHelp "
				+ "FROM AD_Process p, AD_Process_Trl t "
				+ "WHERE p.AD_Process_ID=t.AD_Process_ID"
				+ " AND p.AD_Process_ID=? AND t.AD_Language=?";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, m_AD_Process_ID);
			if (trl)
				pstmt.setString(2, Env.getAD_Language(m_ctx));
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				m_Name = rs.getString(1);
				// m_IsReport = rs.getString(4).equals("Y");
				m_ShowHelp = rs.getString(5);
				//
				m_messageText.append("<b>");
				String s = rs.getString(2);		//	Description
				if (rs.wasNull())
					m_messageText.append(Msg.getMsg(m_ctx, "StartProcess?"));
				else
					m_messageText.append(s);
				m_messageText.append("</b>");
				
				s = rs.getString(3);			//	Help
				if (!rs.wasNull())
					m_messageText.append("<p>").append(s).append("</p>");
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql, e);
			return false;
		}
		finally
		{
			DB.close(rs, pstmt);
		}

		if (m_Name == null)
			return false;

		m_desktop = AEnv.getDesktop();
		//
		this.setTitle(m_Name);
		initialMessage = m_messageText.toString();
		message.setContent(initialMessage);
		bOK.setLabel(Msg.getMsg(Env.getCtx(), "Start"));

		//	Move from APanel.actionButton
		m_pi = new WProcessInfo(m_Name, m_AD_Process_ID);
		m_pi.setAD_User_ID (Env.getAD_User_ID(Env.getCtx()));
		m_pi.setAD_Client_ID(Env.getAD_Client_ID(Env.getCtx()));
		parameterPanel = new ProcessParameterPanel(m_WindowNo, m_pi, "70%");
		centerPanel.getChildren().clear();
		if ( parameterPanel.init() ) {
			centerPanel.appendChild(parameterPanel);
		} else {
			if (m_ShowHelp != null && m_ShowHelp.equals("N")) {
				startProcess();
			}
		}
		
		// Check if the process is a silent one
		if(m_ShowHelp != null && m_ShowHelp.equals("S"))
		{
			startProcess();
		}
		querySaved();
		
		
		return true;
	}	//	init
	
	private void listPrintFormat()
	{
		int AD_Column_ID = 0;
		MProcess pr=new MProcess(m_ctx, m_AD_Process_ID, null);
		int table_ID=0;
		try 
		{
			if(pr.getAD_ReportView_ID() > 0)
			{
				X_AD_ReportView m_Reportview = new X_AD_ReportView(m_ctx, pr.getAD_ReportView_ID() ,null);
				table_ID=m_Reportview.getAD_Table_ID();
			}
			else if (pr.getAD_PrintFormat_ID() > 0)
			{
				MPrintFormat format = new MPrintFormat(m_ctx, pr.getAD_PrintFormat_ID(), null);
				table_ID=format.getAD_Table_ID();
			}
			String valCode=null;
			if(table_ID > 0)
			{
				valCode="AD_PrintFormat.AD_Table_ID="+table_ID;
			}
			Lookup lookup = MLookupFactory.get (Env.getCtx(), m_WindowNo, 
					AD_Column_ID, DisplayType.TableDir,
					Env.getLanguage(Env.getCtx()), "AD_PrintFormat_ID", 0, false,
					valCode);
			
			fPrintFormat = new WTableDirEditor("AD_PrintFormat_ID", false, false, true, lookup);
		} 
		catch (Exception e)
		{
			log.log(Level.SEVERE,e.getLocalizedMessage());
		}
		freportType.removeAllItems();
		MRole roleCurrent = MRole.get(Env.getCtx(), Env.getAD_Role_ID(Env.getCtx()));
		boolean m_isAllowHTMLView = roleCurrent.isAllow_HTML_View();
		boolean m_isAllowXLSView = roleCurrent.isAllow_XLS_View();
	
		
		if(m_isAllowHTMLView)
			freportType.appendItem("HTML","H");
		freportType.appendItem("PDF","P");
		if(m_isAllowXLSView) {
			freportType.appendItem("CSV","C");
			freportType.appendItem("XLSX","XX");
		}
		freportType.setSelectedIndex(0);
		
		String where = "AD_Process_ID = ? AND AD_User_ID = ? AND Name IS NULL ";
		
		MPInstance lastrun = MTable.get(Env.getCtx(), MPInstance.Table_Name).createQuery(where, null).setOnlyActiveRecords(true)
		.setClient_ID().setParameters(m_AD_Process_ID, Env.getContextAsInt(Env.getCtx(), "#AD_User_ID")).setOrderBy("Created DESC").first();
		
		setReportTypeAndPrintFormat(lastrun);
	}
	private void querySaved() 
	{
		//user query
		savedParams = MPInstance.get(Env.getCtx(), m_AD_Process_ID, Env.getContextAsInt(Env.getCtx(), "#AD_User_ID"));
		fSavedName.removeAllItems();
		for (MPInstance instance : savedParams)
		{
			String queries = instance.getName();
			fSavedName.appendItem(queries);
		}
		
		fSavedName.setValue("");
	}

	public void startProcess()
	{
		m_pi.setPrintPreview(true);
		m_pi.setWindowNo(m_WindowNo);

		if (m_desktop == null) {
//			this.lockUI(m_pi);
//			Clients.response(new AuEcho(this, "runProcess", null));
			return;
		}
		if (!m_desktop.isServerPushEnabled()) {
			m_desktop.enableServerPush(true);
		}

		this.lockUI(m_pi);
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					try {
						if (progressWindow != null) {
							progressWindow.fireTimer(Thread.currentThread());
						}
						ProcessCtl.process(null, m_WindowNo, parameterPanel, m_pi, null);
					} finally{
						if (SessionManager.grantAccess(m_desktop)) {
							unlockUI(m_pi);
							SessionManager.releaseDesktop(m_desktop);
						} else {
							if (!m_desktop.isAlive()) {
								//Desktop processing was interrupted by user 
							}
						}
					}
				} catch (DesktopUnavailableException e) {
					log.log(Level.SEVERE, e.getLocalizedMessage(), e);
				}
			}
		};
		DesktopRunnable dr = new DesktopRunnable(runnable, m_desktop);
		new Thread(dr).start();
	}

	public void runProcess() {
		try {
			Events.postEvent(ProcessModalDialog.ON_BEFORE_RUN_PROCESS, this, null);
			ProcessCtl.process(null, m_WindowNo, parameterPanel, m_pi, null);
		} finally {
			unlockUI(m_pi);
		}
	}
	
	public void onEvent(Event event) 
	{
		String saveName = null;
		boolean lastRun=false;
		if(fSavedName.getRawText() != null )
		{
			saveName = fSavedName.getRawText();
			lastRun = ("** " + Msg.getMsg(Env.getCtx(), "LastRun") + " **").equals(saveName);
		}
		
		// Component component = event.getTarget(); 
		if (event.getTarget().equals(bOK)) 
		{
			//Print format on report para
			if(freportType != null && freportType.getSelectedItem() != null && freportType.getSelectedItem().getValue() != null)
			{
				m_pi.setReportType(freportType.getSelectedItem().getValue().toString());
			}
			if(fPrintFormat!=null && fPrintFormat.getValue() != null)
			{
				MPrintFormat format=new MPrintFormat(m_ctx, (Integer)fPrintFormat.getValue(), null);
				if(format != null)
				{
					if (Ini.isClient())
						m_pi.setTransientObject (format);
					else
						m_pi.setSerializableObject(format);				
				}
			}
			// Button element = (Button) component;
			if (!parameterPanel.validateParameters())
				return;
			//if (element.getLabel().length() > 0)
			if (!isResult)
				this.startProcess();
			else
				restart();
				//this.dispose();

		} 
		else if (event.getTarget().equals(bCancel)) 
		{
			this.dispose();
		}
			//saved paramater
		else if(event.getTarget().equals(bSave) && fSavedName != null && !lastRun)
		{
		
				// Update existing
			if (fSavedName.getSelectedIndex() > -1 && savedParams != null)
			{
				for (int i = 0; i < savedParams.size(); i++) 
				{
					if (savedParams.get(i).getName().equals(saveName))
					{
						m_pi.setAD_PInstance_ID(savedParams.get(i).getAD_PInstance_ID());
						for (MPInstancePara para : savedParams.get(i).getParameters())
						{
							para.deleteEx(true);
						}
						parameterPanel.saveParameters();
						
						if (fPrintFormat != null)
							savedParams.get(i).set_ValueOfColumn("AD_PrintFormat_ID", (Integer) fPrintFormat.getValue());
						if (freportType != null && freportType.getSelectedItem() != null)
							savedParams.get(i).set_ValueOfColumn("ReportType", freportType.getSelectedItem().getValue().toString());
						savedParams.get(i).saveEx();
					}
				}
			}
				// create new
			else 
			{
				MPInstance instance = null; 
				try 
				{ 
					instance = new MPInstance(Env.getCtx(), m_pi.getAD_Process_ID(), m_pi.getRecord_ID()); 
					instance.setName(saveName);
					if (fPrintFormat != null)
						instance.set_ValueOfColumn("AD_PrintFormat_ID", (Integer) fPrintFormat.getValue());
					if (freportType != null && freportType.getSelectedItem() != null)
						instance.set_ValueOfColumn("ReportType", freportType.getSelectedItem().getValue().toString());
					instance.saveEx();

					m_pi.setAD_PInstance_ID(instance.getAD_PInstance_ID());
					//				Get Parameters
					if (parameterPanel != null) 
					{
						if (!parameterPanel.saveParameters())
						{
							throw new AdempiereSystemError(Msg.getMsg(Env.getCtx(), "SaveParameterError"));
						}
					}
				} 
				catch (Exception ex) 
				{ 
					log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
				}
			}
			querySaved();
			fSavedName.setSelectedItem(getComboItem(saveName));
		}
		else if(event.getTarget().equals(bDelete)   && fSavedName != null && !lastRun )
		{
			Object o = fSavedName.getSelectedItem();
			if (savedParams != null && o != null)
			{
				String selected = fSavedName.getSelectedItem().getLabel();
				for (int i = 0; i < savedParams.size(); i++) 
				{
					if (savedParams.get(i).getName().equals(selected))
					{
						savedParams.get(i).deleteEx(true);
					}
				}
			}
				
			querySaved();

		}
		
		else if(event.getTarget().equals(fSavedName) )
		{
			if (savedParams != null && saveName != null)
			{
				for (int i = 0; i < savedParams.size(); i++) 
				{
					if (savedParams.get(i).getName().equals(saveName))
					{
						loadSavedParams(savedParams.get(i));
					}
				}
			}
			boolean enabled = !Util.isEmpty(saveName);
			bSave.setEnabled(enabled && !lastRun);
			bDelete.setEnabled(enabled && fSavedName.getSelectedIndex() > -1 && !lastRun);

		}
		
	}
	
	public  Comboitem getComboItem( String value){
		
		Comboitem item = null;

		for(int i = 0; i < fSavedName.getItems().size(); i++){
			if(fSavedName.getItems().get(i) != null){
				item = (Comboitem)fSavedName.getItems().get(i);
				if(value.equals(item.getLabel().toString())){
					break;
				}
			}
		}
		return item;
	
	}
	
	private void setReportTypeAndPrintFormat(MPInstance instance)
	{
		if (fPrintFormat != null && instance != null)
		{
			fPrintFormat.setValue((Integer) instance.get_Value("AD_PrintFormat_ID"));
		}
		if (freportType != null && instance != null)
		{
			if (instance.get_Value("ReportType").equals("H"))
				freportType.setValue("HTML");
			else if (instance.get_Value("ReportType").equals("C"))
				freportType.setValue("CSV");
			else if (instance.get_Value("ReportType").equals("XX"))
				freportType.setValue("XLSX");
			else
				freportType.setValue("PDF");
		}
	}
	
	private void loadSavedParams(MPInstance instance) {
		parameterPanel.loadParameters(instance);
		
		setReportTypeAndPrintFormat(instance);
	}

	public void lockUI(ProcessInfo pi) {
		if (m_isLocked) return;
		
		m_isLocked = true;
		m_desktop.setAttribute(AdempiereWebUI.PROCESS_IN_PROGRESS, "Y");

		showBusyDialog();
	}

	private void showBusyDialog() {
		//progressWindow = new BusyDialog();
		progressWindow = new ProgressMonitorDialog(m_pi, m_desktop);
		progressWindow.setPage(this.getPage());
		progressWindow.doHighlighted();
//		AEnv.showCenterScreen(progressWindow);
		
//		try {
//			progressWindow.doModal();
//		} catch (SuspendNotAllowedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	public void unlockUI(ProcessInfo pi) {
		if (!m_isLocked) return;
		
		m_isLocked = false;
		m_desktop.setAttribute(AdempiereWebUI.PROCESS_IN_PROGRESS, "N");
		
		updateUI(pi);
		hideBusyDialog();
	}

	private void hideBusyDialog() {
		if (progressWindow != null) {
			progressWindow.dispose();
			progressWindow = null;
		}
	}

	private void updateUI(ProcessInfo pi) {
		ProcessInfoUtil.setLogFromDB(pi);
		m_messageText.append("<p><font color=\"").append(pi.isError() ? "#FF0000" : "#0000FF").append("\">** ")
			.append(pi.getSummary())
			.append("</font></p>");
		m_messageText.append(pi.getLogInfo(true));
		message.setContent(m_messageText.toString());
		
		bOK.setLabel(Msg.getMsg(Env.getCtx(), "Parameter"));
		bOK.setIconSclass("z-icon-Reset");
		isResult = true;
		
		m_ids = pi.getIDs();
		
		//no longer needed, hide to give more space to display log
		centerPanel.setVisible(false);
		
		invalidate();
		
		Clients.response(new AuEcho(this, "onAfterProcess", null));
	}

	private void restart() {
		m_messageText = new StringBuffer(initialMessage);
		message.setContent(initialMessage);
		
		centerPanel.setVisible(true);
		
		isResult = false;
		
		bOK.setLabel(Msg.getMsg(Env.getCtx(), "Start"));
		bOK.setIconSclass("z-icon-Ok");
		
		//recreate process info
		m_pi = new WProcessInfo(m_Name, m_AD_Process_ID);
		m_pi.setAD_User_ID (Env.getAD_User_ID(Env.getCtx()));
		m_pi.setAD_Client_ID(Env.getAD_Client_ID(Env.getCtx()));
		parameterPanel.setProcessInfo(m_pi);
		
		invalidate();
	}

	public void onAfterProcess() 
	{
		//
		if (!afterProcessTask()) {
			// If the process is a silent one and no errors occured, close the dialog
			if(m_ShowHelp != null && m_ShowHelp.equals("S"))
				this.dispose();	
		}
	}
	
	/**************************************************************************
	 *	Optional Processing Task
	 */
	private boolean afterProcessTask()
	{
		//  something to do?
		if (m_ids != null && m_ids.length > 0)
		{
			log.config("");
			//	Print invoices
			if (m_AD_Process_ID == 119)
			{
				printInvoices();
				return true;
			}
			else if (m_AD_Process_ID == 118)
			{
				printShipments();
				return true;
			}
			else
			{
				return false;
			}
		}
		
		return false;
	}	//	afterProcessTask
	
	/**************************************************************************
	 *	Print Shipments
	 */
	private void printShipments()
	{		
		if (m_ids == null)
			return;
		FDialog.ask(m_WindowNo, this, "PrintShipments", new Callback<Boolean>() {
			@Override
			public void onCallback(Boolean result) {
				if (result) {
					m_messageText.append("<p>").append(Msg.getMsg(Env.getCtx(), "PrintShipments")).append("</p>");
					message.setContent(m_messageText.toString());
					showBusyDialog();
					Clients.response(new AuEcho(ProcessDialog.this, "onPrintShipments", null));
				}
			}
		});
	}	//	printInvoices
	
	public void onPrintShipments() 
	{		
		//	Loop through all items
		List<File> pdfList = new ArrayList<File>();
		for (int i = 0; i < m_ids.length; i++)
		{
			int M_InOut_ID = m_ids[i];
			ReportEngine re = ReportEngine.get (Env.getCtx(), ReportEngine.SHIPMENT, M_InOut_ID);
			pdfList.add(re.getPDF());				
		}
		
		if (pdfList.size() > 1) {
			try {
				File outFile = File.createTempFile("PrintShipments", ".pdf");					
				Document document = null;
				PdfWriter copy = null;					
				for (File f : pdfList) 
				{
					String fileName = f.getAbsolutePath();
					PdfReader reader = new PdfReader(fileName);
					reader.consolidateNamedDestinations();
					if (document == null)
					{
						document = new Document(reader.getPageSizeWithRotation(1));
						copy = PdfWriter.getInstance(document, new FileOutputStream(outFile));
						document.open();
					}
					int pages = reader.getNumberOfPages();
					PdfContentByte cb = copy.getDirectContent();
					for (int i = 1; i <= pages; i++) {
						document.newPage();
						PdfImportedPage page = copy.getImportedPage(reader, i);
						cb.addTemplate(page, 0, 0);
					}
				}
				document.close();

				Clients.clearBusy();
				Window win = new SimplePDFViewer(this.getTitle(), new FileInputStream(outFile));
				SessionManager.getAppDesktop().showWindow(win, "center");
			} catch (Exception e) {
				log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			}
		} else if (pdfList.size() > 0) {
			Clients.clearBusy();
			try {
				Window win = new SimplePDFViewer(this.getTitle(), new FileInputStream(pdfList.get(0)));
				SessionManager.getAppDesktop().showWindow(win, "center");
			} catch (Exception e)
			{
				log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			}
		}
		
		// If the process is a silent one and no errors occured, close the dialog
		if(m_ShowHelp != null && m_ShowHelp.equals("S"))
			this.dispose();
	}

	/**
	 *	Print Invoices
	 */
	private void printInvoices()
	{
		if (m_ids == null)
			return;
		FDialog.ask(m_WindowNo, this, "PrintInvoices", new Callback<Boolean>() {
			@Override
			public void onCallback(Boolean result) 
			{
				if (result)
				{
					m_messageText.append("<p>").append(Msg.getMsg(Env.getCtx(), "PrintInvoices")).append("</p>");
					message.setContent(m_messageText.toString());
					showBusyDialog();
					Clients.response(new AuEcho(ProcessDialog.this, "onPrintInvoices", null));
				}
			}
		});				
	}	//	printInvoices
	
	public void onPrintInvoices()
	{
		//	Loop through all items
		List<File> pdfList = new ArrayList<File>();
		for (int i = 0; i < m_ids.length; i++)
		{
			int C_Invoice_ID = m_ids[i];
			ReportEngine re = ReportEngine.get (Env.getCtx(), ReportEngine.INVOICE, C_Invoice_ID);
			pdfList.add(re.getPDF());				
		}
		
		if (pdfList.size() > 1) {
			try {
				File outFile = File.createTempFile("PrintInvoices", ".pdf");					
				Document document = null;
				PdfWriter copy = null;					
				for (File f : pdfList) 
				{
					PdfReader reader = new PdfReader(f.getAbsolutePath());
					if (document == null)
					{
						document = new Document(reader.getPageSizeWithRotation(1));
						copy = PdfWriter.getInstance(document, new FileOutputStream(outFile));
						document.open();						
					}
					PdfContentByte cb = copy.getDirectContent(); // Holds the PDF
					int pages = reader.getNumberOfPages();
					for (int i = 1; i <= pages; i++) {
						document.newPage();
						PdfImportedPage page = copy.getImportedPage(reader, i);
						cb.addTemplate(page, 0, 0);
					}
				}
				document.close();

				Clients.clearBusy();
				Window win = new SimplePDFViewer(this.getTitle(), new FileInputStream(outFile));
				SessionManager.getAppDesktop().showWindow(win, "center");
			} catch (Exception e) {
				log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			}
		} else if (pdfList.size() > 0) {
			try {
				Window win = new SimplePDFViewer(this.getTitle(), new FileInputStream(pdfList.get(0)));
				SessionManager.getAppDesktop().showWindow(win, "center");
			} catch (Exception e)
			{
				log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			}
		}
		
		// If the process is a silent one and no errors occured, close the dialog
		if(m_ShowHelp != null && m_ShowHelp.equals("S"))
			this.dispose();	
	}

	public boolean isValid() {
		return valid;
	}

	public void executeASync(ProcessInfo pi) {
	}

	public boolean isUILocked() {
		return m_isLocked;
	}
	
	public void ask(String message, Callback<Boolean> callback) {
		Executions.schedule(getDesktop(), new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				FDialog.ask(m_WindowNo, null, message, callback);
			}
		}, new Event("onAsk"));
	}
}	//	ProcessDialog
