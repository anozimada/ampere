/******************************************************************************
 * Product: Posterita Ajax UI 												  *
 * Copyright (C) 2007 Posterita Ltd.  All Rights Reserved.                    *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Posterita Ltd., 3, Draper Avenue, Quatre Bornes, Mauritius                 *
 * or via info@posterita.org or http://www.posterita.org/                     *
 *****************************************************************************/

package org.adempiere.webui;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.ProcessModalDialog;
import org.adempiere.webui.apps.ProgressMonitorDialog;
import org.adempiere.webui.component.CaptureCommand;
import org.adempiere.webui.component.DrillCommand;
import org.adempiere.webui.component.DropUploadCommand;
import org.adempiere.webui.component.ExecuteCommand;
import org.adempiere.webui.component.TokenCommand;
import org.adempiere.webui.component.ZoomCommand;
import org.adempiere.webui.desktop.DefaultDesktop;
import org.adempiere.webui.desktop.IDesktop;
import org.adempiere.webui.session.ServerContext;
import org.adempiere.webui.session.SessionContextListener;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.UserPreference;
import org.adempiere.webui.window.FindWindow;
import org.compiere.model.MClient;
import org.compiere.model.MRole;
import org.compiere.model.MSession;
import org.compiere.model.MSysConfig;
import org.compiere.model.MUser;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Language;
import org.compiere.util.Util;
import org.hashids.Hashids;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.IDToken;
import org.zkforge.keylistener.Keylistener;
import org.zkoss.util.Locales;
import org.zkoss.web.Attributes;
import org.zkoss.web.servlet.Servlets;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.ClientInfoEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.impl.ExecutionCarryOver;
import org.zkoss.zk.ui.sys.DesktopCache;
import org.zkoss.zk.ui.sys.DesktopCtrl;
import org.zkoss.zk.ui.sys.ExecutionCtrl;
import org.zkoss.zk.ui.sys.ExecutionsCtrl;
import org.zkoss.zk.ui.sys.SessionCtrl;
import org.zkoss.zk.ui.sys.Visualizer;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

/**
 *
 * @author  <a href="mailto:agramdass@gmail.com">Ashley G Ramdass</a>
 * @date    Feb 25, 2007
 * @version $Revision: 0.10 $
 *
 * @author hengsin
 */
public class AdempiereWebUI extends Window implements EventListener<Event>, IWebClient
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3744725245132180915L;
	private static Logger log = Logger.getLogger(Util.class.getName());

	public static final String APP_NAME = "Adaxa";

    public static final String UID          = "1.8";

    private WLogin             loginDesktop;
    
    private IDesktop           appDesktop;

    private ClientInfo		   clientInfo;

	private String langSession;

	private UserPreference userPreference;

	private Keylistener keyListener;
	
	private static final CLogger logger = CLogger.getCLogger(AdempiereWebUI.class);

	public static final String EXECUTION_CARRYOVER_SESSION_KEY = "execution.carryover";

	public static final String ZK_DESKTOP_SESSION_KEY = "zk.desktop";
	
	public static final String ZK_CLIENT_CONTEXT = "zk_client_context";
	
	private static final String SAVED_CONTEXT = "saved.context";
	
	public static final String APPLICATION_DESKTOP_KEY = "application.desktop";

	public static final String PROCESS_IN_PROGRESS = "process.inprogress";
	
    public static final String WIDGET_INSTANCE_NAME = "instanceName";

	private ConcurrentMap<String, String[]>	m_URLParameters;
	
	private static boolean eventThreadEnabled = false;

	public AdempiereWebUI()
    {
    	this.addEventListener(Events.ON_CLIENT_INFO, this);
    	this.setVisible(false);

    	//  Register the available themes in the system.
    	ThemeUtils.registerAllThemes(Env.getCtx());

    	userPreference = new UserPreference();

		// preserve the original URL parameters as is destroyed later on the logging
		m_URLParameters = new ConcurrentHashMap<String, String[]>(Executions.getCurrent().getParameterMap());
    }

    public void onCreate()
    {
        SessionManager.setSessionApplication(this);
        Session session = Executions.getCurrent().getDesktop().getSession();

        Properties ctx = Env.getCtx();
        langSession = Env.getContext(ctx, Env.LANGUAGE);
        
        if (session.getAttribute(SessionContextListener.SESSION_CTX) == null || !SessionManager.isUserLoggedIn(ctx))
        {
        	@SuppressWarnings("unchecked")
    		Map<String, Object>map = (Map<String, Object>) session.getAttribute(SAVED_CONTEXT);
            session.removeAttribute(SAVED_CONTEXT);
            if (map != null && !map.isEmpty())
            {
            	onChangeRole(map);
            	return;
            }
            
        	 ServletRequest request = (ServletRequest) Executions.getCurrent().getNativeRequest();
             KeycloakSecurityContext keycloakSecurityContext =
             		(KeycloakSecurityContext) request
             		.getAttribute(KeycloakSecurityContext.class.getName());
             IDToken kc_idToken = keycloakSecurityContext.getIdToken();
             String kc_username = kc_idToken.getPreferredUsername();
             
             
            loginDesktop = new WLogin(this);
            loginDesktop.createPart(this.getPage());
            
            MUser loggedInUser = MUser.getByValue(Env.getCtx(), kc_username);
            if ( loggedInUser != null )
            {
            	// to reload preferences when the user refresh the browser
        		userPreference = loadUserPreference(loggedInUser.getAD_User_ID());
        		String lang = userPreference.getProperty(UserPreference.P_LANGUAGE);
        		
        		if ( lang == null )
        		{
        			// Make the default language the language of client System
        	        lang = MClient.get(ctx, 0).getAD_Language();
        		}
        		
        		Language language = Language.getLanguage(lang);
            	Locale locale = language.getLocale();
            	Env.setContext(Env.getCtx(), Env.LANGUAGE, language.getAD_Language());
            	Env.setContext(Env.getCtx(), AEnv.LOCALE, locale.toString());

                Session currSess = Executions.getCurrent().getDesktop().getSession();
                currSess.setAttribute(Attributes.PREFERRED_LOCALE, locale);
                try {
        			Clients.reloadMessages(locale);
        		} catch (IOException e) {
        			//
        		}
                Locales.setThreadLocal(locale);
                
            	ctx.setProperty(Env.AD_CLIENT_ID, String.valueOf(loggedInUser.getAD_Client_ID()));
            	ctx.setProperty(Env.AD_ORG_ID, String.valueOf(loggedInUser.getAD_Org_ID()));
        		ctx.setProperty(Env.AD_USER_ID, String.valueOf(loggedInUser.getAD_User_ID()));
        		//newCtx.setProperty(Env.AD_ROLE_ID, String.valueOf(loggedInUser.));

            	Properties newCtx = Env.getCtx();
            	loginDesktop.changeRole(kc_username, locale, newCtx);
            }
        }
        else
        {
            loginCompleted();
        } 
        
        Executions.getCurrent().getDesktop().addListener(new ZoomCommand());
        Executions.getCurrent().getDesktop().addListener(new DrillCommand());
        Executions.getCurrent().getDesktop().addListener(new ExecuteCommand());
        Executions.getCurrent().getDesktop().addListener(new TokenCommand());
        Executions.getCurrent().getDesktop().addListener(new DropUploadCommand());
        Executions.getCurrent().getDesktop().addListener(new CaptureCommand());

        eventThreadEnabled = Executions.getCurrent().getDesktop().getWebApp().getConfiguration().isEventThreadEnabled();
    }

    public void onOk()
    {
    }

    public void onCancel()
    {
    }

	private void onChangeRole(Map<String, Object> map)
	{
		Locale locale = (Locale) map.get("locale");
		String user = (String) map.get("username");
		Properties properties = (Properties) map.get("context");

		SessionManager.setSessionApplication(this);
		loginDesktop = new WLogin(this);
		loginDesktop.createPart(this.getPage());
		loginDesktop.changeRole(user, locale, properties);
	}

    //--> Ashley
    private String getClientContext()
    {
        String sql = "SELECT Value FROM AD_SysConfig " +
        		"WHERE Name='ZK_CLIENT_CONTEXT' AND IsActive='Y' AND AD_Client_ID=?";
        
        return DB.getSQLValueString(null, sql, Env.getAD_Client_ID(Env.getCtx()));
    }
    //<--

    /* (non-Javadoc)
	 * @see org.adempiere.webui.IWebClient#loginCompleted()
	 */
    public void loginCompleted()
    {
    	if (loginDesktop != null)
    	{
    		loginDesktop.detach();
    		loginDesktop = null;
    	}

        Properties ctx = Env.getCtx();
        String langLogin = AEnv.getLocale(ctx).toString();
        if (langLogin == null || langLogin.length() <= 0)
        {
        	langLogin = langSession;
        	Env.setContext(ctx, Env.LANGUAGE, langSession);
        }

        // Validate language
		Language language = Language.getLanguage(langLogin);
    	Env.setContext(ctx, Env.LANGUAGE, language.getAD_Language());
    	Env.setContext(ctx, AEnv.LOCALE, language.getLocale().toString());

		//	Create adempiere Session - user id in ctx
        Session currSess = Executions.getCurrent().getDesktop().getSession();
        HttpSession httpSess = (HttpSession) currSess.getNativeSession();

        int timeout = MSysConfig.getIntValue(MSysConfig.ZK_SESSION_TIMEOUT_IN_SECONDS, 3600, Env.getAD_Client_ID(Env.getCtx()));
        httpSess.setMaxInactiveInterval(timeout);

		MSession mSession = MSession.get(ctx, Executions.getCurrent().getRemoteAddr(), Executions.getCurrent().getRemoteHost(), httpSess.getId());
		String ua = Servlets.getUserAgent((ServletRequest) Executions.getCurrent().getNativeRequest());
		if (ua != null)
		{
			String str = mSession.getDescription() + "\n" + ua;
			if (str.length() < 2000)
			{
				mSession.setDescription(str);
				mSession.saveEx();
			}
		}
		
		currSess.setAttribute("Check_AD_User_ID", Env.getAD_User_ID(ctx));
		
		//enable full interface, relook into this when doing preference
		Env.setContext(ctx, "#ShowTrl", true);
		Env.setContext(ctx, "#ShowAcct", MRole.getDefault().isShowAcct());
		Env.setContext(ctx, "#ShowAdvanced", true);

		// to reload preferences when the user refresh the browser
		userPreference = loadUserPreference(Env.getAD_User_ID(ctx));
		
		//auto commit user preference
		String autoCommit = userPreference.getProperty(UserPreference.P_AUTO_COMMIT);
		Env.setAutoCommit(ctx, "true".equalsIgnoreCase(autoCommit) || "y".equalsIgnoreCase(autoCommit));

		//auto new user preference
		String autoNew = userPreference.getProperty(UserPreference.P_AUTO_NEW);
		Env.setAutoNew(ctx, "true".equalsIgnoreCase(autoNew) || "y".equalsIgnoreCase(autoNew));
		
		keyListener = new Keylistener();
		keyListener.setPage(this.getPage());
		keyListener.setCtrlKeys("#enter");
		keyListener.setAutoBlur(false);
		
		// --> Sachin Bhimani [#1462 To add support for multiple tabs on same browser and older behavior having one tab per browser]
		// Check for same browser have multiple tab support or not 
		boolean isMultipleTabSupport = MSysConfig.getBooleanValue(MSysConfig.SUPPORT_MULTITAB_BROWSER, false);
		
		if(!isMultipleTabSupport)
		{
			IDesktop d = (IDesktop) currSess.getAttribute(APPLICATION_DESKTOP_KEY);

			appDesktop = null;
			if (d != null && d instanceof IDesktop)
			{
				boolean isUnstableState = false;
				if (d.getComponent().getDesktop() != null) {
					String s = (String) d.getComponent().getDesktop().getAttribute(PROCESS_IN_PROGRESS);
					if (s != null && "Y".equals(s)) {
						isUnstableState = true;
					}
					if (!isUnstableState) {
						ExecutionCarryOver eco = (ExecutionCarryOver) currSess.getAttribute(EXECUTION_CARRYOVER_SESSION_KEY);
						if (eco != null)
						{
							// try restore
							appDesktop = restoreLastSession(currSess, d, eco);
						}
					}
				}
			}

			if (appDesktop == null)
			{
				// create new desktop
				appDesktop = createDesktop();
				appDesktop.setClientInfo(clientInfo);
				appDesktop.createPart(this.getPage());
				currSess.setAttribute(APPLICATION_DESKTOP_KEY, appDesktop);
				ExecutionCarryOver eco = new ExecutionCarryOver(this.getPage().getDesktop());
				currSess.setAttribute(EXECUTION_CARRYOVER_SESSION_KEY, eco);
				currSess.setAttribute(ZK_DESKTOP_SESSION_KEY, this.getPage().getDesktop());
			}
		}
		else
		{
			// create new desktop
			appDesktop = createDesktop();
			appDesktop.setClientInfo(clientInfo);
			appDesktop.createPart(this.getPage());

			// ensure server push is on
			if (!this.getPage().getDesktop().isServerPushEnabled())
				this.getPage().getDesktop().enableServerPush(true);
		}
		// <-- Sachin Bhimani

		this.getPage().getDesktop().setAttribute(APPLICATION_DESKTOP_KEY, new WeakReference<IDesktop>(appDesktop));

		// track browser tab per session
		SessionContextListener.addDesktopId(mSession.getAD_Session_ID(), getPage().getDesktop().getId());
		
		
		// update session context
		currSess.setAttribute(SessionContextListener.SESSION_CTX, ServerContext.getCurrentInstance());
		

		// --> Ashley
		if (MSysConfig.getBooleanValue("ZK_ENABLE_CLIENT_URL", false))
		{
			Env.setContext(ctx, ZK_CLIENT_CONTEXT, (String) getClientContext());
			this.getPage().setTitle(ThemeUtils.getBrowserTitle());
		}
		// <--
		
		Env.setContext(ctx, "#UIClient", "zk");
		StringBuilder localHttpAddr = new StringBuilder(Executions.getCurrent().getScheme());
		localHttpAddr.append("://").append(Executions.getCurrent().getLocalAddr());
		int port = Executions.getCurrent().getLocalPort();
		if (port > 0 && port != 80)
		{
			localHttpAddr.append(":").append(port);
		}
		Env.setContext(ctx, "#LocalHttpAddr", localHttpAddr.toString());

		processParameters();
    }

	private IDesktop restoreLastSession(Session currSess, IDesktop d, ExecutionCarryOver eco) {
		try
		{
			appDesktop = (IDesktop) d;

			ExecutionCarryOver current = new ExecutionCarryOver(this.getPage().getDesktop());
			ExecutionCtrl ctrl = ExecutionsCtrl.getCurrentCtrl();
			Visualizer vi = ctrl.getVisualizer();
			eco.carryOver();
			
			Collection<Component> rootComponents = new ArrayList<Component>();
			try
			{
				ctrl = ExecutionsCtrl.getCurrentCtrl();
				((DesktopCtrl) Executions.getCurrent().getDesktop()).setVisualizer(vi);

				// detach root component from old page
				Page page = appDesktop.getComponent().getPage();
				Collection<?> collection = page.getRoots();
				Object[] objects = new Object[0];
				objects = collection.toArray(objects);
				for (Object obj : objects)
				{
					if (obj instanceof Component)
					{
						((Component) obj).detach();
						if (obj instanceof ProgressMonitorDialog) {
							continue;
						}
						if (!(obj instanceof FindWindow))
							rootComponents.add((Component) obj);
					}
				}
				appDesktop.getComponent().detach();
				DesktopCache desktopCache = ((SessionCtrl) currSess).getDesktopCache();
				if (desktopCache != null)
					desktopCache.removeDesktop(Executions.getCurrent().getDesktop());
			}
			catch (Exception e)
			{
				appDesktop = null;
			}
			finally
			{
				eco.cleanup();
				current.carryOver();
			}

			if (appDesktop != null)
			{
				// re-attach root components
				for (Component component : rootComponents)
				{
					try
					{
						if  (component instanceof ProcessModalDialog) {
							((ProcessModalDialog) component).dispose();
							continue;
							
						}
						component.setPage(this.getPage());
					}
					catch (UiException e)
					{
						// e.printStackTrace();
						// an exception is thrown here when refreshing the page, it seems is harmless to catch and ignore it
						// i.e.: org.zkoss.zk.ui.UiException: Not unique in the ID space of [Page z_kg_0]: zk_comp_2
					}
				}
				appDesktop.setPage(this.getPage());
				currSess.setAttribute(EXECUTION_CARRYOVER_SESSION_KEY, current);
			}
			currSess.setAttribute(ZK_DESKTOP_SESSION_KEY, this.getPage().getDesktop());
			return appDesktop;
		}
		catch (Throwable t)
		{
			// restore fail
			return null;
		}
	}

    private IDesktop createDesktop()
	{
		return new DefaultDesktop();
	}

	/* (non-Javadoc)
	 * @see org.adempiere.webui.IWebClient#logout()
	 */
    public void logout()
    {
        //--> Ashley
        String clientValue = Env.getContext(Env.getCtx(), ZK_CLIENT_CONTEXT);
        //<--
        IDesktop appDesktop = getAppDesktop();
    	appDesktop.logout();
    	Executions.getCurrent().getDesktop().getSession().getAttributes().clear();

    	MSession mSession = MSession.get(Env.getCtx(), false);
    	if (mSession != null) {
    		mSession.logout();
    	}

        SessionManager.clearSession();
        super.getChildren().clear();
        Page page = this.getPage();
        page.removeComponents();
        

   	    ServletRequest request = (ServletRequest) Executions.getCurrent().getNativeRequest();
        KeycloakSecurityContext keycloakSecurityContext =
        		(KeycloakSecurityContext) request
        		.getAttribute(KeycloakSecurityContext.class.getName());
        IDToken kc_idToken = keycloakSecurityContext.getIdToken();
        
        HttpServletRequest req = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
        
        String redirectURL;
        // reconstruct base URL
        try {
        	URL url = new URL(req.getRequestURL().toString());
			redirectURL = url.getProtocol() + "://" + url.getHost() + ":" + url.getPort() + "/";     
        } catch (MalformedURLException e) {
        	redirectURL = req.getRequestURL().toString();
        }    
        
        if (clientValue != null && clientValue.length() > 0)
        {
            redirectURL += "?" + clientValue;
        }
        
        //<--
        String logoutURL =  kc_idToken.getIssuer()
        		+ "/protocol/openid-connect/logout?redirect_uri=" 
        		+ URLEncoder.encode(redirectURL, StandardCharsets.UTF_8);
        
        Executions.sendRedirect(logoutURL);
        
        
    }

	/**
	 * Get application desktop
	 * 
	 * @return IDesktop
	 */
	public IDesktop getAppDesktop()
	{
		Desktop desktop = Executions.getCurrent() != null ? Executions.getCurrent().getDesktop() : AEnv.getDesktop();
		IDesktop appDesktop = null;
		if (desktop != null)
		{
			@SuppressWarnings("unchecked")
			WeakReference<IDesktop> ref = (WeakReference<IDesktop>) desktop.getAttribute(APPLICATION_DESKTOP_KEY);
			if (ref != null)
			{
				appDesktop = ref.get();
			}
		}

		return appDesktop;
	}

	public void onEvent(Event event) {
		if (event instanceof ClientInfoEvent) {
			ClientInfoEvent c = (ClientInfoEvent)event;
			clientInfo = new ClientInfo();
			clientInfo.colorDepth = c.getColorDepth();
			clientInfo.desktopHeight = c.getDesktopHeight();
			clientInfo.desktopWidth = c.getDesktopWidth();
			clientInfo.desktopXOffset = c.getDesktopXOffset();
			clientInfo.desktopYOffset = c.getDesktopYOffset();
			clientInfo.timeZone = c.getTimeZone();
			IDesktop appDesktop = getAppDesktop();
			if (appDesktop != null)
				appDesktop.setClientInfo(clientInfo);
		}

	}

	/**
	 * @param userId
	 * @return UserPreference
	 */
	public UserPreference loadUserPreference(int userId) {
		userPreference.loadPreference(userId);
		return userPreference;
	}

	/**
	 * @return UserPrerence
	 */
	public UserPreference getUserPreference() {
		return userPreference;
	}
	
	public static boolean isEventThreadEnabled() {
		return eventThreadEnabled;
	}

	@Override
	public void changeRole(MUser user)
	{
		// save context for re-login
		Properties properties = new Properties();
		Env.setContext(properties, Env.AD_CLIENT_ID, Env.getAD_Client_ID(Env.getCtx()));
		Env.setContext(properties, Env.AD_ORG_ID, Env.getAD_Org_ID(Env.getCtx()));
		Env.setContext(properties, Env.AD_USER_ID, user.getAD_User_ID());
		Env.setContext(properties, Env.AD_ROLE_ID, Env.getAD_Role_ID(Env.getCtx()));
		Env.setContext(properties, Env.AD_ORG_NAME, Env.getContext(Env.getCtx(), Env.AD_ORG_NAME));
		Env.setContext(properties, Env.M_WAREHOUSE_ID, Env.getContext(Env.getCtx(), Env.M_WAREHOUSE_ID));
		Env.setContext(properties, UserPreference.P_LANGUAGE,
				Env.getContext(Env.getCtx(), UserPreference.P_LANGUAGE));
		Env.setContext(properties, Env.LANGUAGE, Env.getContext(Env.getCtx(), Env.LANGUAGE));
		Env.setContext(properties, AEnv.LOCALE, Env.getContext(Env.getCtx(), AEnv.LOCALE));

		Locale locale = (Locale) Executions.getCurrent().getDesktop().getSession().getAttribute(Attributes.PREFERRED_LOCALE);
		HttpServletRequest httpRequest = (HttpServletRequest) Executions.getCurrent().getNativeRequest();		

		Session session = Executions.getCurrent().getDesktop().getSession();

		// stop background thread
		IDesktop appDesktop = getAppDesktop();
		if (appDesktop != null)
			appDesktop.logout();

		// clear remove all children and root component
		getChildren().clear();
		getPage().removeComponents();
		
		Env.getCtx().clear();

		// clear session attributes
		session.getAttributes().clear();

		// put saved context into new session
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("context", properties);
		map.put("locale", locale);
		map.put("username", user.getValue());

		HttpSession newSession = httpRequest.getSession(false);
		
		newSession.setAttribute(SAVED_CONTEXT, map);
		properties.setProperty(SessionContextListener.SERVLET_SESSION_ID, newSession.getId());
		
		Executions.sendRedirect("index.zul");
	}

	/*
	 * (non-Javadoc)
	 * @see org.adempiere.webui.IWebClient#logoutAfterTabDestroyed()
	 */
	@Override
	public void logoutAfterTabDestroyed()
	{
		Session session = logout0();

		// clear context, invalidate session
		Env.getCtx().clear();
		session.invalidate();
	}

	/**
	 * @author Sachin Bhimani
	 * @return Session
	 */
	protected Session logout0()
	{
		String clientValue = Env.getContext(Env.getCtx(), ZK_CLIENT_CONTEXT);
		Session session = Executions.getCurrent().getDesktop().getSession();

		if (keyListener != null)
		{
			keyListener.detach();
			keyListener = null;
		}
		
		// stop background thread
		IDesktop appDesktop = getAppDesktop();
		if (appDesktop != null)
			appDesktop.logout();

		// clear remove all children and root component
//		getChildren().clear();
//		getPage().removeComponents();

		// clear session attributes
		session.getAttributes().clear();

		Env.logout();

		if (clientValue != null && clientValue.length() > 0)
		{
			Executions.sendRedirect("?" + clientValue);
		}
		else
		{
			Executions.sendRedirect("index.zul");
		}

		return session;
	}

	@Override
	public Keylistener getKeylistener()
	{
		return keyListener;
	}
	
	/**
	 * Process URL Parameters.
	 */
	private void processParameters()
	{
		String hashedTableID = getParamString("table");
		String hashedRecordID = getParamString("id");
		if (!Util.isEmpty(hashedTableID, true) && !Util.isEmpty(hashedTableID, true)) {
			Hashids hashids = new Hashids(System.getenv("HASHID_SALT"));
			try {
				long tableID = hashids.decode(hashedTableID)[0];
				long recordID = hashids.decode(hashedRecordID)[0];
				if (tableID > 0) {
					AEnv.zoom(Long.valueOf(tableID).intValue(), Long.valueOf(recordID).intValue());
				}
			} catch (Exception e) {
				String str = "Failed to decode query param: table=" + hashedTableID + ", id=" + hashedRecordID;
				log.log(Level.SEVERE, str, e);
			}
		}
		m_URLParameters = null;
	}

	/**
	 * Retrieve Parameter as String
	 * 
	 * @param param
	 * @return
	 */
	private String getParamString(String param)
	{
		String retValue = "";
		if (m_URLParameters != null)
		{
			String[] strs = m_URLParameters.get(param);
			if (strs != null && strs.length == 1 && strs[0] != null)
				retValue = strs[0];
		}
		return retValue;
	}
}
