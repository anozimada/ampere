package org.adempiere.webui;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import org.apache.catalina.Valve;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.valves.RemoteIpValve;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;
import org.apache.tomcat.util.scan.StandardJarScanner;
import org.compiere.db.CConnection;
import org.compiere.util.CLogger;
import org.keycloak.adapters.tomcat.KeycloakAuthenticatorValve;

public class TomcatLauncher {

	/** Logger for the class * */
    private static final CLogger logger;

    static
    {
        logger = CLogger.getCLogger(TomcatLauncher.class); 
    }


    public static void main(String[] args) throws Exception {

        String webappDirLocation = ".";
        Tomcat tomcat = new Tomcat();

        String webPort = System.getenv("PORT");
        if(webPort == null || webPort.isEmpty()) {
            webPort = "8080";
        }

        tomcat.setPort(Integer.valueOf(webPort));
        tomcat.setBaseDir(createTempDir());

        StandardContext ctx = (StandardContext) tomcat.addWebapp("", new File(webappDirLocation).getAbsolutePath());

        String kccf = System.getenv("KEYCLOAK_JSON_FILE");
        if ( kccf == null || kccf.isEmpty() )
        	kccf = "keycloak.json";

        ctx.addParameter("keycloak.config.file", kccf);
        
        Valve valve = new KeycloakAuthenticatorValve();
        ctx.getPipeline().addValve(valve);
        
        RemoteIpValve ipValve = new RemoteIpValve();
        ipValve.setProtocolHeader("X-Forwarded-Proto");
        ipValve.setRemoteIpHeader("X-Forwarded-For");
        ctx.getPipeline().addValve(ipValve);
        
        File additionWebInfClasses = new File("bin/main");  // eclipse build output path
        WebResourceRoot resources = new StandardRoot(ctx);
        resources.addPreResources(new DirResourceSet(resources, "/WEB-INF/classes",  // standard location
                additionWebInfClasses.getAbsolutePath(), "/"));
        ctx.setResources(resources);
        StandardJarScanner js = new StandardJarScanner();
        js.setScanClassPath(false);
        js.setScanManifest(false);
        ctx.setJarScanner(js);
        
        String logText = "\n" +
        		"___________________________________________  \n" +
        		"|     _                                   |  Starting webui tomcat container in dev mode \n" + 
        		"|    / \\   _ __ ___  _ __   ___ _ __ ___  |  Listening on: http://0.0.0.0:" + webPort + " \n" + 
        		"|   / _ \\ | '_ ` _ \\| '_ \\ / _ \\ '__/ _ \\ |  DB connection: " + CConnection.get(null).getConnectionURL() + " \n" + 
        		"|  / ___ \\| | | | | | |_) |  __/ | |  __/ |  \n" + 
        		"| /_/   \\_\\_| |_| |_| .__/ \\___|_|  \\___| |  \n" + 
        		"|                   |_|                   |  \n" +
        		"|_________________________________________|";
        logger.log(Level.INFO, logText);

        tomcat.start();
        tomcat.getServer().await();
    }
    
    // based on AbstractEmbeddedServletContainerFactory
    private static String createTempDir() {
        try {
            File tempDir = File.createTempFile("tomcat.", "webui");
            tempDir.delete();
            tempDir.mkdir();
            tempDir.deleteOnExit();
            return tempDir.getAbsolutePath();
        } catch (IOException ex) {
            throw new RuntimeException(
                    "Unable to create tempDir. java.io.tmpdir is set to " + System.getProperty("java.io.tmpdir"),
                    ex
            );
        }
    }
}