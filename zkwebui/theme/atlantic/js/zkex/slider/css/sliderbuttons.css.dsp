<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-sliderbuttons-button {
  position: absolute;
  border-style: solid;
  background-color: #FFFFFF;
}
.z-sliderbuttons-tooltip {
  position: absolute;
  display: none;
  color: #FFFFFF;
  vertical-align: top;
  line-height: 1;
}
.z-sliderbuttons-area {
  position: absolute;
}
