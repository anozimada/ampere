<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-drawer {
  position: fixed;
  top: 0;
  z-index: 1000;
  width: 0;
  height: 0;
}
.z-drawer-open {
  width: 100%;
  height: 100%;
}
.z-drawer-mask {
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  background-color: #000;
  opacity: 0;
}
.z-drawer-mask-enabled {
  opacity: 0.5;
}
.z-drawer-real {
  position: fixed;
  background-color: #FFFFFF;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-direction: column;
  flex-direction: column;
  will-change: transform;
  transition: transform 200ms ease-out;
}
.z-drawer-container {
  -ms-flex: 1;
  flex: 1;
  overflow: auto;
}
.z-drawer-header {
  padding: 9px 8px;
  background-color: rgba(50, 111, 112, 0.8);
  border-bottom: 1px solid transparent;
}
.z-drawer-title {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 85%;
  font-weight: bold;
  font-style: normal;
  color: #FFFFFF;
}
.z-drawer-close {
  font-size: 85%;
  color: #FFFFFF;
  width: 85%;
  height: 85%;
  background-color: rgba(50, 111, 112, 0.8);
  position: absolute;
  right: 8px;
  top: 10px;
  text-align: center;
  cursor: pointer;
}
.z-drawer-close:hover {
  background-color: #FFFFFF;
  color: rgba(50, 111, 112, 0.8);
}
.z-drawer-cave {
  padding: 8px;
  height: 100%;
}
.z-drawer-open.z-drawer-left .z-drawer-real {
  box-shadow: 4px 0 8px rgba(0, 0, 0, 0.3);
  transform: translateX(0%);
}
.z-drawer-open.z-drawer-right .z-drawer-real {
  box-shadow: -4px 0 8px rgba(0, 0, 0, 0.3);
  transform: translateX(0%);
}
.z-drawer-open.z-drawer-top .z-drawer-real {
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.3);
  transform: translateY(0%);
}
.z-drawer-open.z-drawer-bottom .z-drawer-real {
  box-shadow: 0 -4px 8px rgba(0, 0, 0, 0.3);
  transform: translateY(0%);
}
.z-drawer-left .z-drawer-real {
  left: 0;
  width: 280px;
  height: 100%;
  transform: translateX(-100%);
}
.z-drawer-right .z-drawer-real {
  right: 0;
  width: 280px;
  height: 100%;
  transform: translateX(100%);
}
.z-drawer-top .z-drawer-real {
  top: 0;
  bottom: auto;
  left: 0;
  width: 100%;
  height: 280px;
  transform: translateY(-100%);
}
.z-drawer-bottom .z-drawer-real {
  top: auto;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 280px;
  transform: translateY(100%);
}
