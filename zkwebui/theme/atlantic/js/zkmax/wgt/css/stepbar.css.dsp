<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-stepbar {
  background: #FFFFFF;
  overflow: hidden;
  padding: 12px;
  display: flex;
  display: -ms-flexbox;
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
}
.z-stepbar:not(.z-stepbar-linear) .z-step {
  cursor: pointer;
}
.z-step {
  display: flex;
  display: -ms-flexbox;
  flex-direction: row;
  -ms-flex-direction: row;
  align-items: center;
  flex: 1;
  z-index: 0;
  padding: 12px;
  text-align: center;
}
.z-step:first-child,
.z-step:first-child:before {
  flex: 0 1 auto;
  white-space: nowrap;
}
.z-step:before {
  content: '';
  height: 6px;
  background-color: #E3E3E3;
  flex: 1;
  margin: 0 12px 0 -12px;
}
.z-stepbar-linear .z-step-active:before,
.z-stepbar-linear .z-step-complete:before {
  background-color: rgba(50, 111, 112, 0.8);
}
.z-step-icon {
  display: inline-block;
  width: 28px;
  height: 28px;
  margin-right: 12px;
  line-height: 28px;
  text-align: center;
  font-size: 28px;
  vertical-align: top;
  color: #E3E3E3;
}
.z-step-icon-empty {
  border-radius: 50%;
  border: 3px solid #E3E3E3;
  background-color: #FFFFFF;
}
.z-step-title {
  background-color: #FFFFFF;
  font-size: 16px;
  line-height: 28px;
  vertical-align: top;
}
.z-step-active .z-step-icon,
.z-step-complete .z-step-icon {
  color: rgba(50, 111, 112, 0.8);
}
.z-step-active .z-step-icon-empty,
.z-step-complete .z-step-icon-empty {
  border-color: rgba(50, 111, 112, 0.8);
}
.z-stepbar:not(.z-stepbar-linear) .z-step-complete .z-step-icon:hover {
  color: rgba(50, 111, 112, 0.7);
}
.z-stepbar:not(.z-stepbar-linear) .z-step-complete .z-step-icon:active {
  color: #668788;
}
.z-stepbar:not(.z-stepbar-linear) .z-step .z-icon-check:hover {
  color: #FFFFFF;
  background-color: rgba(50, 111, 112, 0.7);
  border-color: rgba(50, 111, 112, 0.7);
}
.z-stepbar:not(.z-stepbar-linear) .z-step .z-icon-check:active {
  color: #FFFFFF;
  background-color: #668788;
  border-color: #668788;
}
.z-step .z-icon-check {
  color: #FFFFFF;
  background-color: rgba(50, 111, 112, 0.8);
  border-color: rgba(50, 111, 112, 0.8);
  border-radius: 50%;
  font-size: 16px;
}
.z-step-error .z-step-icon {
  color: #FFFFFF;
  background-color: #AE312E;
  border-radius: 50%;
  font-size: 16px;
}
.z-stepbar-wrapped-label .z-step {
  position: relative;
  flex: 2 2 28px;
  align-items: stretch;
}
.z-stepbar-wrapped-label .z-step:first-child,
.z-stepbar-wrapped-label .z-step:last-child {
  flex-grow: 1;
  flex-shrink: 1;
}
.z-stepbar-wrapped-label .z-step:before {
  margin-left: -12px;
  margin-right: 0;
}
.z-stepbar-wrapped-label .z-step:after {
  content: '';
  height: 6px;
  background-color: #E3E3E3;
  flex: 1;
  margin-left: 0;
  margin-right: -12px;
}
.z-stepbar-wrapped-label .z-step:before,
.z-stepbar-wrapped-label .z-step:after {
  margin-top: 11px;
}
.z-stepbar-wrapped-label .z-step-content {
  position: relative;
  display: flex;
  flex-direction: column;
  -ms-flex-direction: column;
  align-items: center;
  text-align: center;
  vertical-align: top;
}
.z-stepbar-wrapped-label .z-step-content:before,
.z-stepbar-wrapped-label .z-step-content:after {
  position: absolute;
  height: 6px;
  background-color: #E3E3E3;
  content: '';
  top: 11px;
}
.z-stepbar-wrapped-label .z-step-content:before {
  left: 0;
  right: calc(50% + (28px / 2));
}
.z-stepbar-wrapped-label .z-step-content:after {
  left: calc(50% + (28px / 2));
  right: 0;
}
.z-stepbar-wrapped-label .z-step:first-child:before,
.z-stepbar-wrapped-label .z-step:last-child:after,
.z-stepbar-wrapped-label .z-step:first-child .z-step-content:before,
.z-stepbar-wrapped-label .z-step:last-child .z-step-content:after {
  display: none;
}
.z-stepbar-wrapped-label.z-stepbar-linear .z-step-complete:after,
.z-stepbar-wrapped-label.z-stepbar-linear .z-step-complete .z-step-content:after,
.z-stepbar-wrapped-label.z-stepbar-linear .z-step-active .z-step-content:before,
.z-stepbar-wrapped-label.z-stepbar-linear .z-step-complete .z-step-content:before {
  background-color: rgba(50, 111, 112, 0.8);
}
.z-stepbar-wrapped-label .z-step-icon {
  margin-right: 0;
}
.z-stepbar-wrapped-label .z-step-title {
  display: block;
  margin-top: 12px;
}
