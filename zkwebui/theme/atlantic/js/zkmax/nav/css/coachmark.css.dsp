<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-coachmark {
  position: absolute;
  top: 0;
  left: 0;
  opacity: 0;
  visibility: hidden;
}
@keyframes expand {
  0% {
    transform: scale(0);
  }
  100% {
    transform: scale(1);
    opacity: 1;
    visibility: visible;
  }
}
.z-coachmark-open {
  animation: expand 0.8s 0.2s ease-in-out;
  animation-fill-mode: forwards;
}
.z-coachmark-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 85%;
  font-weight: normal;
  position: relative;
  overflow: hidden;
  height: 100%;
  padding: 10px 26px 10px 15px;
  vertical-align: middle;
  background-color: #FFFFFF;
}
.z-coachmark-pointer {
  display: none;
  width: 0;
  height: 0;
  border: 10px solid transparent;
  position: absolute;
  z-index: 100;
}
.z-coachmark-close {
  font-size: 16px;
  width: 16px;
  height: 16px;
  position: absolute;
  top: 10px;
  right: 5px;
  color: #ACACAC;
  cursor: pointer;
}
.z-coachmark-icon {
  background-color: #FFFFFF;
  position: absolute;
  z-index: 1;
}
.z-coachmark-mask {
  position: fixed;
  background: transparent;
  animation: mask 0.3s 0.3s ease-in-out;
  animation-fill-mode: forwards;
}
@keyframes mask {
  0% {
    background: transparent;
  }
  100% {
    background: rgba(0, 0, 0, 0.5);
  }
}
.z-coachmark-right ~ .z-coachmark-close {
  right: 26px;
}
.z-coachmark-up ~ .z-coachmark-close {
  top: 26px;
}
.z-coachmark-left {
  border-right-color: #FFFFFF;
}
.z-coachmark-right {
  border-left-color: #FFFFFF;
}
.z-coachmark-up {
  border-bottom-color: #FFFFFF;
}
.z-coachmark-down {
  border-top-color: #FFFFFF;
}
