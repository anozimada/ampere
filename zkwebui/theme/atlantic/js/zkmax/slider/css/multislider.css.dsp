<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-multislider {
  white-space: nowrap;
  overflow: visible;
  cursor: pointer;
  font-size: 85%;
}
.z-multislider-inner {
  position: relative;
}
.z-multislider-track {
  position: absolute;
  width: 100%;
  height: 100%;
  background-color: #d9d9d9;
}
.z-multislider-marks {
  position: relative;
}
.z-multislider-mark {
  position: absolute;
}
.z-multislider-mark-label {
  position: absolute;
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  text-align: center;
}
.z-multislider .z-sliderbuttons-button {
  width: 20px;
  height: 20px;
  background-color: #FFFFFF;
  border-width: 1px;
  border-color: #ACACAC;
  border-radius: 50%;
  z-index: 2;
}
.z-multislider .z-sliderbuttons-tooltip {
  background: rgba(0, 0, 0, 0.8);
  font-size: 114%;
}
.z-multislider .z-sliderbuttons-area {
  background-color: rgba(50, 111, 112, 0.8);
}
.z-multislider .z-sliderbuttons:nth-last-of-type(2n-2) .z-sliderbuttons-area {
  background-color: #326F70;
}
.z-multislider-horizontal {
  width: 300px;
}
.z-multislider-horizontal .z-sliderbuttons-area {
  height: 100%;
}
.z-multislider-horizontal .z-sliderbuttons-button {
  margin-top: -6px;
  margin-left: -10px;
  top: 0;
}
.z-multislider-horizontal .z-sliderbuttons-tooltip {
  top: -10px;
  left: 50%;
  transform: translateX(-50%) translateY(-100%);
  padding: 4px 6px;
}
.z-multislider-horizontal .z-multislider-inner {
  margin: 40px 12px 30px;
  height: 8px;
}
.z-multislider-horizontal .z-multislider-marks {
  width: 100%;
}
.z-multislider-horizontal .z-multislider-mark-label {
  top: 22px;
  transform: translateX(-50%);
}
.z-multislider-vertical {
  height: 300px;
}
.z-multislider-vertical .z-sliderbuttons-area {
  width: 100%;
}
.z-multislider-vertical .z-sliderbuttons-button {
  margin-top: -10px;
  margin-left: -6px;
  left: 0;
}
.z-multislider-vertical .z-sliderbuttons-tooltip {
  left: 26px;
  top: 50%;
  transform: translateY(-50%);
  padding: 6px 4px;
}
.z-multislider-vertical .z-multislider-inner {
  height: 100%;
  margin: 12px 30px 12px 12px;
  width: 8px;
}
.z-multislider-vertical .z-multislider-marks {
  height: 100%;
}
.z-multislider-vertical .z-multislider-mark-label {
  left: 22px;
  transform: translateY(-50%);
}
.z-multislider:not(.z-multislider-disabled) .z-sliderbuttons-button:hover {
  background-color: rgba(50, 111, 112, 0.7);
}
.z-multislider:not(.z-multislider-disabled) .z-sliderbuttons-button:hover .z-sliderbuttons-tooltip {
  display: block;
}
.z-multislider:not(.z-multislider-disabled) .z-sliderbuttons-button:active {
  background-color: #668788;
}
.z-multislider:not(.z-multislider-disabled) .z-sliderbuttons-button:focus {
  border-color: rgba(50, 111, 112, 0.9);
}
.z-multislider-disabled .z-multislider-track {
  background-color: #f0f0f0;
}
.z-multislider-disabled {
  cursor: default;
}
.z-multislider-disabled .z-sliderbuttons-area {
  background-color: #bbcfdc;
}
.z-multislider-disabled .z-sliderbuttons-button {
  background-color: #E3E3E3;
}
.z-multislider-disabled .z-sliderbuttons-tooltip {
  background-color: rgba(0, 0, 0, 0.5);
}
.z-multislider-disabled .z-sliderbuttons:nth-last-of-type(2n-2) .z-sliderbuttons-area {
  background-color: #78c2c3;
}
