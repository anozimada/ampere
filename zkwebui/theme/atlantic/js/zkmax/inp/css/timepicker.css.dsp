<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-timepicker {
  display: inline-block;
  min-height: 24px;
  line-height: normal;
  white-space: nowrap;
  clear: both;
}
.z-timepicker-input {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
  width: 100%;
  height: 24px;
  border: 1px solid #326F70;
  margin: 0;
  padding: 3px 7px;
  padding-right: 31px;
  line-height: 16px;
  background: #FFFFFF;
}
.z-timepicker-input:focus {
  border-color: rgba(50, 111, 112, 0.9);
  -webkit-box-shadow: inset -1px 0 0 rgba(50, 111, 112, 0.9);
  -moz-box-shadow: inset -1px 0 0 rgba(50, 111, 112, 0.9);
  -o-box-shadow: inset -1px 0 0 rgba(50, 111, 112, 0.9);
  -ms-box-shadow: inset -1px 0 0 rgba(50, 111, 112, 0.9);
  box-shadow: inset -1px 0 0 rgba(50, 111, 112, 0.9);
}
.z-timepicker-input[readonly] {
  border-color: #326F70;
  background: rgba(102, 135, 136, 0.35);
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  -o-box-shadow: none;
  -ms-box-shadow: none;
  box-shadow: none;
}
.z-timepicker-input-full {
  padding-right: 7px;
}
.z-timepicker-button {
  display: inline-block;
  width: 24px;
  height: 24px;
  position: relative;
  top: 0;
  right: 24px;
  border: 1px solid #326F70;
  background: #FFFFFF;
  vertical-align: middle;
  cursor: pointer;
  overflow: hidden;
  font-size: 114%;
}
.z-timepicker-button:hover {
  border-color: rgba(50, 111, 112, 0.7);
  background: rgba(50, 111, 112, 0.7);
}
.z-timepicker-button:hover > i {
  color: #FFFFFF;
}
.z-timepicker-button:active {
  border-color: #668788;
  background: #668788;
}
.z-timepicker-button:active > i {
  color: #FFFFFF;
}
.z-timepicker-disabled .z-timepicker-button:hover,
.z-timepicker-disabled .z-timepicker-button:active {
  border-color: #326F70;
  background: 0;
}
.z-timepicker-icon {
  font-size: 100%;
  color: #000000;
  display: block !important;
  width: 22px;
  line-height: 22px;
  text-align: center;
}
.z-timepicker-disabled {
  opacity: 1;
  filter: alpha(opacity=100);;
}
.z-timepicker-disabled * {
  color: #ACACAC !important;
  background: #F2F2F2 !important;
  cursor: default !important;
}
.z-timepicker-invalid {
  border: 1px solid #AE312E !important;
  margin-right: -1px;
  background: #FFFFFF;
  -webkit-box-shadow: inset -1px 0 0 #AE312E;
  -moz-box-shadow: inset -1px 0 0 #AE312E;
  -o-box-shadow: inset -1px 0 0 #AE312E;
  -ms-box-shadow: inset -1px 0 0 #AE312E;
  box-shadow: inset -1px 0 0 #AE312E;
}
.z-timepicker-readonly:focus {
  border-right-width: 0;
  border-color: #326F70;
  background: transparent repeat-x 0 0;
  cursor: default;
}
.z-timepicker-rightedge {
  border-right: 1px solid #326F70;
}
.z-timepicker-inplace .z-timepicker-input {
  padding: 5px;
  border: 0;
  background: none;
}
.z-timepicker-inplace .z-timepicker-input:focus {
  background: 0;
}
.z-timepicker-inplace .z-timepicker-button {
  display: none;
}
.z-timepicker-option {
  display: block;
  padding: 4px 8px;
  line-height: 16px;
  position: relative;
  white-space: nowrap;
  cursor: pointer;
  color: #000000;
  text-decoration: none;
}
.z-timepicker-option:hover {
  color: #FFFFFF;
  background: rgba(50, 111, 112, 0.7);
}
.z-timepicker-option:hover .z-timepicker-option-inner,
.z-timepicker-option:hover .z-timepicker-option-content {
  color: #FFFFFF;
}
.z-timepicker-popup {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
  display: block;
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
  position: absolute;
  overflow: auto;
  max-height: 200px;
}
.z-timepicker-content {
  border: 0;
  margin: 0;
  padding: 0;
  background: transparent none repeat 0 0;
  position: relative;
  list-style-image: none;
  list-style-position: outside;
  list-style-type: none;
}
