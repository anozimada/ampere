<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-chosenbox {
  display: inline-block;
  border: 1px solid #326F70;
  padding-bottom: 3px;
  line-height: normal;
  background: #FFFFFF;
  overflow: hidden;
  vertical-align: bottom;
  min-height: 24px;
}
.z-chosenbox-item {
  display: inline-block;
  height: 16px;
  margin: 3px 0 0 4px;
  background: rgba(50, 111, 112, 0.8);
  vertical-align: middle;
  position: relative;
  white-space: nowrap;
  cursor: pointer;
}
.z-chosenbox-item-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 80%;
  font-weight: 400;
  font-style: normal;
  color: #FFFFFF;
  line-height: 16px;
  padding: 0 18px 0 4px;
}
.z-chosenbox-button {
  width: 12px;
  height: 12px;
  text-align: center;
  position: absolute;
  right: 4px;
  top: 50%;
  margin-top: -6px;
}
.z-chosenbox-button:hover .z-chosenbox-icon {
  color: rgba(50, 111, 112, 0.8);
  background: #FFFFFF;
}
.z-chosenbox-icon {
  font-size: 70%;
  color: #FFFFFF;
  display: block;
  line-height: 12px;
}
.z-chosenbox-input {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
  display: inline-block;
  width: 24px;
  height: 16px;
  border-width: 0;
  padding: 4px;
  background: transparent !important;
}
.z-chosenbox-disabled {
  background: rgba(102, 135, 136, 0.35);
  opacity: 1;
  filter: alpha(opacity=100);;
}
.z-chosenbox-disabled .z-chosenbox-item {
  cursor: default;
}
.z-chosenbox-disabled .z-chosenbox-item-content {
  padding-right: 4px;
}
.z-chosenbox-disabled .z-chosenbox-button {
  display: none;
}
.z-chosenbox-textcontent {
  display: none;
}
.z-chosenbox-popup {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
  display: block;
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
  position: absolute;
  overflow: auto;
}
.z-chosenbox-popup-hidden {
  display: none;
}
.z-chosenbox-option {
  display: block;
  padding: 4px 8px;
  line-height: 16px;
  cursor: pointer;
}
.z-chosenbox-option-hover {
  color: #FFFFFF;
  background: rgba(50, 111, 112, 0.7);
}
.z-chosenbox-empty {
  padding: 1px 5px;
  font-style: italic;
  color: #ACACAC;
}
.z-chosenbox-empty-creatable {
  cursor: pointer;
  color: #FFFFFF;
  padding-top: 2px;
}
.z-chosenbox-select {
  display: inline-block;
  min-width: 100%;
}
.z-chosenbox-create {
  font-size: 100%;
  color: #59A755;
  display: inline-block;
  width: 100%;
  height: 100%;
  margin-right: 4px;
  line-height: normal;
  background: #FFFFFF;
  text-align: center;
}
.z-chosenbox-inplace {
  width: 100%;
  height: 100%;
  font-style: normal;
  display: inline-block;
  font-size: 18px;
  padding: 4px;
}
.z-chosenbox-hide {
  display: none;
}
