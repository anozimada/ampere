<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-portallayout,
.z-portalchildren,
.z-portalchildren-content {
  overflow: hidden;
}
.z-portallayout,
.z-portalchildren {
  -ms-zoom: 1;
}
.z-portallayout-horizontal .z-portalchildren-content > .z-panel,
.z-portallayout-horizontal .z-portalchildren-content > .z-panel-move-block {
  float: left;
}
.z-portalchildren {
  height: 100%;
  float: left;
}
.z-portalchildren-content {
  height: 100%;
  width: 100%;
}
.z-portalchildren-counter {
  display: none;
  border-radius: 10px;
  padding: 0 8px;
  margin-right: 8px;
  background: #E3E3E3;
  font-size: 14px;
  font-weight: 700;
}
.z-portalchildren-counter-on {
  display: inline;
}
.z-portalchildren-frame {
  border: 1px solid #E3E3E3;
  border-radius: 0;
  background: #FFFFFF;
  overflow: hidden;
  padding: 15px;
  padding-bottom: 0;
  margin-right: 15px;
}
.z-portalchildren-frame:last-child {
  margin-right: 0;
}
.z-portalchildren-frame > .z-portalchildren-title {
  display: block;
}
.z-portalchildren-frame > .z-portalchildren-content .z-panel {
  background: #FFFFFF;
  margin-bottom: 8px;
  position: relative;
}
.z-portalchildren-frame > .z-portalchildren-content .z-panel:last-child {
  margin-bottom: 15px;
}
.z-portalchildren-frame > .z-portalchildren-content .z-panel-head {
  padding: 0;
  border: 0;
}
.z-portalchildren-frame > .z-portalchildren-content .z-panel-header {
  color: rgba(50, 111, 112, 0.8);
  padding: 8px 15px 0 15px;
  font-size: 14px;
  font-weight: 400;
}
.z-portalchildren-frame > .z-portalchildren-content .z-panel-body {
  padding: 0;
}
.z-portalchildren-frame > .z-portalchildren-content .z-panel-noframe {
  border: 1px solid #E3E3E3;
}
.z-portalchildren-frame > .z-portalchildren-content .z-panel-noborder {
  border: 0;
}
.z-portalchildren-frame > .z-portalchildren-content .z-panel-drag-button {
  display: block;
  color: #ACACAC;
  width: 12px;
  height: 12px;
  float: left;
  position: absolute;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
  z-index: 1;
}
.z-portalchildren-frame > .z-portalchildren-content .z-panel-collapsed .z-panel-drag-button {
  position: relative;
}
.z-portalchildren-frame > .z-portalchildren-content .z-panel .z-panelchildren {
  padding: 8px 15px;
  border: 0;
}
.z-portalchildren-frame > .z-portalchildren-content .z-panel .z-panelchildren-noheader {
  border: 0;
}
.z-portalchildren-title {
  display: none;
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 114%;
  font-weight: normal;
  font-style: normal;
  color: #ACACAC;
  border: 0;
  overflow: hidden;
  padding-bottom: 15px;
}
.z-portallayout-horizontal > .z-portalchildren {
  height: 100%;
}
.z-portallayout-horizontal > .z-portalchildren-frame {
  float: left;
  clear: both;
  margin-bottom: 15px;
}
.z-portallayout-horizontal > .z-portalchildren-frame:last-child {
  margin-bottom: 0;
}
.z-portallayout-horizontal > .z-portalchildren-frame > .z-portalchildren-content > .z-panel {
  margin-right: 8px;
}
.z-portallayout-horizontal > .z-portalchildren-frame > .z-portalchildren-content > .z-panel:last-child {
  margin-right: 0;
}
.z-portallayout-popup {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-weight: 400;
  font-size: 100%;
  display: none;
  color: #000000;
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
  position: absolute;
  overflow: auto;
  min-height: 10px;
}
.z-portallayout-popup-content {
  border: 0;
  padding: 0;
  margin: 0;
  background: transparent none repeat 0 0;
  position: relative;
  list-style: none outside none;
  min-width: 100%;
  display: inline-block;
}
.z-portallayout-popup-nav {
  cursor: pointer;
  padding: 4px 8px;
}
.z-portallayout-popup-nav:focus {
  color: #FFFFFF;
  background: rgba(50, 111, 112, 0.7);
}
.z-portallayout-popup-open {
  display: block;
}
