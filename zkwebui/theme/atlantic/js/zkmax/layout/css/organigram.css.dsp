<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-organigram {
  overflow: auto;
}
.z-orgchildren {
  display: flex;
}
.z-orgchildren:not(:only-child) > .z-orgitem:after {
  content: '';
  border-left: 1px solid #9B9B9B;
  height: 10px;
  position: absolute;
  left: 50%;
  top: 0px;
}
.z-orgitem {
  flex: auto;
  display: flex;
  flex-direction: column;
  position: relative;
}
.z-orgitem:not(:only-child)::before {
  content: '';
  border-top: 1px solid #9B9B9B;
  width: 100%;
  height: 1px;
  top: 0px;
  position: absolute;
}
.z-orgitem:first-child:before {
  width: 50%;
  left: 50%;
}
.z-orgitem:last-child:before {
  width: 50%;
}
.z-orgitem:not(.z-orgitem-disabled) > .z-orgnode:hover {
  background-color: #5E94B8;
  border-bottom-color: #436983;
}
.z-orgitem-selected > .z-orgnode {
  background-color: #436983;
}
.z-orgitem-disabled > .z-orgnode {
  color: #ACACAC;
  background-color: #E3E3E3;
  cursor: default;
}
.z-orgitem-non-selectable > .z-orgnode {
  cursor: default;
}
.z-orgitem-close > .z-orgchildren {
  display: none;
}
.z-orgitem-close > .z-orgnode:after {
  display: none;
}
.z-orgnode {
  align-self: center;
  position: relative;
  margin: 10px;
  padding: 10px;
  cursor: pointer;
  border: 1px solid #5687A8;
  background-color: #5687A8;
  color: #FFFFFF;
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
}
.z-orgnode > i[class="z-orgnode-icon"] {
  display: none;
}
.z-orgnode-icon {
  position: absolute;
  width: 12px;
  height: 12px;
  right: 0;
  bottom: 0;
  cursor: pointer;
  color: #FFFFFF;
  font-size: 12px;
}
.z-orgnode:not(:only-child)::after {
  content: '';
  border-left: 1px solid #9B9B9B;
  height: 10px;
  position: absolute;
  bottom: -11px;
  left: 50%;
}
