<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-listbox {
  border: 1px solid #E3E3E3;
  overflow: hidden;
  zoom: 1;
}
.z-listbox-header {
  width: 100%;
  background: #326F70;
  overflow: hidden;
}
.z-listbox-header table {
  border-spacing: 0;
}
.z-listbox-header table th,
.z-listbox-header table td {
  background-clip: padding-box;
  padding: 0;
}
.z-listbox-header table th {
  text-align: inherit;
}
.z-listbox-body {
  position: relative;
  overflow: hidden;
  overflow-anchor: none;
}
.z-listbox-body table {
  border-spacing: 0;
}
.z-listbox-body table th,
.z-listbox-body table td {
  background-clip: padding-box;
  padding: 0;
}
.z-listbox-body table th {
  text-align: inherit;
}
.z-listbox-emptybody td {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #ACACAC;
  font-style: italic;
  padding: 4px 8px !important;
  line-height: 16px;
  text-align: center;
  height: 1px;
}
.z-listbox-footer {
  border-top: 1px solid #E3E3E3;
  background: #dfe1e1;
  overflow: hidden;
  white-space: nowrap;
}
.z-listbox-footer table {
  border-spacing: 0;
}
.z-listbox-footer table th,
.z-listbox-footer table td {
  background-clip: padding-box;
  padding: 0;
}
.z-listbox-footer table th {
  text-align: inherit;
}
.z-listbox-footer .z-listfooter {
  overflow: hidden;
  background: #dfe1e1;
}
.z-listbox-footer .z-listfoot-bar {
  background: #dfe1e1;
}
.z-listbox-odd.z-listitem {
  background: #FFFFFF;
}
.z-listbox-loading {
  background: transparent no-repeat center;
  background-image: url(${c:encodeThemeURL("~./zul/img/misc/progress-large.gif")});
}
.z-listhead th:first-child {
  border-left-width: 0;
}
.z-listhead th:first-child.z-listhead-border {
  border-left: 1px solid #FFFFFF;
}
.z-listhead-bar {
  background: #326F70;
  border-top: 1px solid #FFFFFF;
  border-left: 1px solid #FFFFFF;
}
.z-listheader {
  border-top: 1px solid #FFFFFF;
  border-left: 1px solid #FFFFFF;
  padding: 0;
  background: #326F70;
  position: relative;
  overflow: hidden;
  white-space: nowrap;
}
.z-listheader-hover .z-listheader-button {
  display: block;
}
.z-listheader-sort .z-listheader-content {
  cursor: pointer;
}
.z-listheader-sort .z-listheader-sorticon {
  font-size: 100%;
  color: #FFFFFF;
  line-height: normal;
  position: absolute;
  top: 0;
  left: 50%;
}
.z-listheader-checkable {
  display: inline-block;
  width: 20px;
  height: 20px;
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
  vertical-align: text-top;
}
.z-listheader-checkable .z-listheader-icon {
  display: none;
  cursor: default;
}
.z-listheader-checkable.z-listheader-checked .z-listheader-icon {
  font-size: 14px;
  color: #326F70;
  display: block;
  margin-top: -7px;
  line-height: normal;
  text-align: center;
  position: relative;
  top: 50%;
}
.z-listheader-button {
  color: #FFFFFF;
  display: none;
  width: 36px;
  height: 35px;
  border-left: 1px solid #FFFFFF;
  line-height: 36px;
  text-align: center;
  position: absolute;
  top: 0;
  right: 0;
  text-decoration: none;
  z-index: 15;
  cursor: pointer;
}
.z-listheader-button:hover {
  background: #2a5e5e;
}
.z-listheader-sizing,
.z-listheader-sizing .z-listheader-button,
.z-listheader-sizing.z-listheader-sort .z-listheader-content {
  cursor: e-resize;
}
.z-listitem {
  background: #FFFFFF;
}
.z-listitem:first-child .z-listcell {
  border-top-width: 0;
}
.z-listitem:first-child .z-listcell-content,
.z-listitem:first-child .z-listgroup-content {
  padding-top: 4px;
}
.z-listitem .z-listcell {
  border-top: 1px solid #E3E3E3;
  background: #FFFFFF;
  overflow: hidden;
  position: relative;
  z-index: 0;
}
.z-listitem:hover > .z-listcell {
  background: rgba(50, 111, 112, 0.7);
}
.z-listitem:hover > .z-listcell > .z-listcell-content {
  color: #FFFFFF;
}
.z-listitem-checkable {
  display: inline-block;
  width: 20px;
  height: 20px;
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
  vertical-align: text-top;
}
.z-listitem-checkable.z-listitem-radio {
  -webkit-border-radius: 10px;
  -moz-border-radius: 10px;
  -o-border-radius: 10px;
  -ms-border-radius: 10px;
  border-radius: 10px;
}
.z-listitem-checkable .z-listitem-icon {
  display: none;
  cursor: pointer;
}
.z-listitem-selected > .z-listcell {
  border-color: rgba(102, 135, 136, 0.2);
  background: rgba(102, 135, 136, 0.2);
}
.z-listitem-selected > .z-listcell > .z-listcell-content {
  color: #000000;
}
.z-listitem-selected:hover > .z-listcell {
  border-color: #BD652F;
  background: #BD652F;
}
.z-listitem-selected:hover > .z-listcell > .z-listcell-content {
  color: #FFFFFF;
}
.z-listitem-selected > .z-listcell > .z-listcell-content > .z-listitem-checkable .z-listitem-icon {
  font-size: 14px;
  color: #326F70;
  display: block;
  margin-top: -7px;
  line-height: normal;
  text-align: center;
  position: relative;
  top: 50%;
}
.z-listitem-selected > .z-listcell > .z-listcell-content > .z-listitem-checkable .z-listitem-icon.z-icon-check {
  color: #326F70;
}
.z-listitem-selected > .z-listcell > .z-listcell-content > .z-listitem-checkable .z-listitem-icon.z-icon-radio {
  width: 10px;
  height: 10px;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  -o-border-radius: 5px;
  -ms-border-radius: 5px;
  border-radius: 5px;
  margin-top: -5px;
  margin-left: -5px;
  background: #326F70;
  left: 50%;
}
.z-listitem.z-listitem-disabled * {
  color: #ACACAC !important;
  background: #FFFFFF;
  cursor: default !important;
}
.z-listitem.z-listitem-disabled:hover > .z-listcell {
  background: #FFFFFF;
}
.z-listitem.z-listitem-disabled a,
.z-listitem.z-listitem-disabled a:visited,
.z-listitem.z-listitem-disabled a:hover {
  text-decoration: none;
}
.z-listitem a,
.z-listitem a:visited,
.z-listitem a:hover {
  text-decoration: none;
}
.z-listgroup-inner {
  border-top: 1px solid #FFFFFF;
  background: #E3E3E3;
  overflow: hidden;
}
.z-listgroup-inner .z-listcell-content,
.z-listgroup-inner .z-listgroup-content {
  padding: 3px 4px 4px;
}
.z-listgroup:first-child .z-listgroup-inner {
  border-top-width: 0;
}
.z-listgroup-checkable {
  display: inline-block;
  width: 20px;
  height: 20px;
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
  vertical-align: text-top;
}
.z-listgroup-checkable .z-listgroup-icon {
  display: none;
  cursor: default;
}
.z-listgroup-icon {
  font-size: 114%;
  color: #000000;
  display: inline-block;
  width: 18px;
  height: 18px;
  text-align: center;
  vertical-align: top;
  cursor: pointer;
}
.z-listgroup-selected > .z-listcell > .z-listcell-content > .z-listgroup-checkable .z-listgroup-icon {
  font-size: 14px;
  color: #326F70;
  display: block;
  width: auto;
  height: auto;
  margin-top: -7px;
  line-height: normal;
  text-align: center;
  position: relative;
  top: 50%;
}
.z-listgroupfoot-inner {
  border-top: 1px solid #E3E3E3;
  background: #dfe1e1;
  overflow: hidden;
}
.z-listheader-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 85%;
  font-weight: 600;
  font-style: normal;
  color: #FFFFFF;
  padding: 4px 8px;
  line-height: 28px;
  position: relative;
  overflow: hidden;
}
.z-listcell-content,
.z-listgroup-content,
.z-listgroupfoot-content,
.z-listfooter-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
  padding: 3px 4px 4px;
  line-height: 16px;
  overflow: hidden;
  cursor: pointer;
}
.z-listgroup-content,
.z-listgroupfoot-content,
.z-listfooter-content {
  line-height: 22px;
  cursor: default;
}
.z-listgroup-content .z-label,
.z-listgroupfoot-content .z-label {
  font-weight: 600;
}
.z-listbox-paging-top {
  overflow: hidden;
  width: 100%;
}
.z-listbox-paging-top .z-paging {
  border: none;
  border-bottom: 1px solid #E3E3E3;
}
.z-listbox-paging-bottom {
  overflow: hidden;
  width: 100%;
}
.z-listbox-paging-bottom .z-paging {
  border: none;
  border-top: 1px solid #E3E3E3;
}
.z-listbox-autopaging .z-listcell-content {
  height: 32px;
  line-height: 24px;
  overflow: hidden;
}
.z-listhead-menugrouping .z-menuitem-image {
  background-image: url(${c:encodeThemeURL("~./zul/img/grid/menu-group.png")});
}
.z-listhead-menuungrouping .z-menuitem-image {
  background-image: url(${c:encodeThemeURL("~./zul/img/grid/menu-ungroup.png")});
}
.z-listhead-menuascending .z-menuitem-image {
  background-image: url(${c:encodeThemeURL("~./zul/img/grid/menu-arrowup.png")});
}
.z-listhead-menudescending .z-menuitem-image {
  background-image: url(${c:encodeThemeURL("~./zul/img/grid/menu-arrowdown.png")});
}
.z-select {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
}
.z-listcell-hidden-header {
  white-space: nowrap;
  overflow: hidden;
}
.ie9 .z-listbox-body {
  margin-top: auto;
}
.ie9 .z-listitem .z-listcell {
  position: static;
  z-index: auto;
}
