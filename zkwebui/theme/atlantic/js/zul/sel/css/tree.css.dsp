<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-tree {
  border: 1px solid #E3E3E3;
  overflow: hidden;
  zoom: 1;
}
.z-tree-header {
  width: 100%;
  background: #326F70;
  overflow: hidden;
}
.z-tree-header table {
  border-spacing: 0;
}
.z-tree-header table th,
.z-tree-header table td {
  background-clip: padding-box;
  padding: 0;
}
.z-tree-header table th {
  text-align: inherit;
}
.z-tree-body {
  position: relative;
  overflow: hidden;
  overflow-anchor: none;
}
.z-tree-body table {
  border-spacing: 0;
}
.z-tree-body table th,
.z-tree-body table td {
  background-clip: padding-box;
  padding: 0;
}
.z-tree-body table th {
  text-align: inherit;
}
.z-tree-emptybody td {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #ACACAC;
  font-style: italic;
  padding: 4px 8px !important;
  line-height: 16px;
  text-align: center;
  height: 1px;
}
.z-tree-footer {
  border-top: 1px solid #E3E3E3;
  background: #dfe1e1;
  overflow: hidden;
  white-space: nowrap;
}
.z-tree-footer table {
  border-spacing: 0;
}
.z-tree-footer table th,
.z-tree-footer table td {
  background-clip: padding-box;
  padding: 0;
}
.z-tree-footer table th {
  text-align: inherit;
}
.z-tree-footer .z-treefooter {
  overflow: hidden;
  background: #dfe1e1;
}
.z-tree-footer .z-treefoot-bar {
  background: #dfe1e1;
}
.z-tree-loading {
  background: transparent no-repeat center;
  background-image: url(${c:encodeThemeURL("~./zul/img/misc/progress-large.gif")});
}
.z-tree-icon,
.z-tree-line {
  display: inline-block;
  width: 18px;
  height: 18px;
  vertical-align: middle;
}
.z-tree-icon {
  font-size: 100%;
  color: #000000;
  text-align: center;
  position: relative;
  cursor: pointer;
}
.z-treecols th:first-child {
  border-left: none;
}
.z-treecols th:first-child.z-treecols-border {
  border-left: 1px solid #FFFFFF;
}
.z-treecols-bar {
  background: #326F70;
  border-top: 1px solid #FFFFFF;
  border-left: 1px solid #FFFFFF;
}
.z-treecol {
  border-top: 1px solid #FFFFFF;
  border-left: 1px solid #FFFFFF;
  padding: 0;
  background: #326F70;
  position: relative;
  overflow: hidden;
  white-space: nowrap;
}
.z-treecol-sort {
  cursor: pointer;
}
.z-treecol-sort .z-treecol-sorticon {
  color: #ACACAC;
  position: absolute;
  top: -7px;
  left: 50%;
}
.z-treecol-sizing,
.z-treecol-sizing .z-treecol-content {
  cursor: e-resize;
}
.z-treecol-checkable {
  display: inline-block;
  width: 20px;
  height: 20px;
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
  vertical-align: text-top;
}
.z-treecol-checkable .z-treecol-icon {
  display: none;
  cursor: pointer;
}
.z-treecol-checkable.z-treecol-checked .z-treecol-icon {
  font-size: 14px;
  color: #326F70;
  display: block;
  margin-top: -7px;
  line-height: normal;
  text-align: center;
  position: relative;
  top: 50%;
}
.z-treerow {
  background: #FFFFFF;
}
.z-treerow .z-treecell {
  overflow: hidden;
  cursor: pointer;
  background: inherit;
  position: relative;
  z-index: 0;
}
.z-treerow:hover > .z-treecell {
  background: rgba(50, 111, 112, 0.7);
}
.z-treerow:hover > .z-treecell > .z-treecell-content {
  color: #FFFFFF;
}
.z-treerow-checkable {
  display: inline-block;
  width: 20px;
  height: 20px;
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
  vertical-align: text-top;
}
.z-treerow-checkable.z-treerow-radio {
  -webkit-border-radius: 10px;
  -moz-border-radius: 10px;
  -o-border-radius: 10px;
  -ms-border-radius: 10px;
  border-radius: 10px;
}
.z-treerow-checkable .z-treerow-icon {
  display: none;
  cursor: pointer;
}
.z-treerow-selected > .z-treecell {
  border-color: rgba(102, 135, 136, 0.2);
  background: rgba(102, 135, 136, 0.2);
}
.z-treerow-selected > .z-treecell > .z-treecell-content {
  color: #000000;
}
.z-treerow-selected:hover > .z-treecell {
  border-color: #BD652F;
  background: #BD652F;
}
.z-treerow-selected:hover > .z-treecell > .z-treecell-content {
  color: #FFFFFF;
}
.z-treerow-selected > .z-treecell > .z-treecell-content > .z-treerow-checkable .z-treerow-icon {
  font-size: 14px;
  color: #326F70;
  display: block;
  margin-top: -7px;
  line-height: normal;
  text-align: center;
  position: relative;
  top: 50%;
}
.z-treerow-selected > .z-treecell > .z-treecell-content > .z-treerow-checkable .z-treerow-icon.z-icon-check {
  color: #326F70;
}
.z-treerow-selected > .z-treecell > .z-treecell-content > .z-treerow-checkable .z-treerow-icon.z-icon-radio {
  width: 10px;
  height: 10px;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  -o-border-radius: 5px;
  -ms-border-radius: 5px;
  border-radius: 5px;
  margin-top: -5px;
  margin-left: -5px;
  background: #326F70;
  left: 50%;
}
.z-treerow.z-treerow-disabled * {
  color: #ACACAC !important;
  background: #FFFFFF;
  cursor: default !important;
}
.z-treerow.z-treerow-disabled:hover > .z-treecell {
  background: #FFFFFF;
}
.z-treecell-hidden-col {
  white-space: nowrap;
  overflow: hidden;
}
.z-treecol-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 85%;
  font-weight: 600;
  font-style: normal;
  color: #FFFFFF;
  padding: 4px 8px;
  line-height: 28px;
  overflow: hidden;
}
.z-treecell-content,
.z-treefooter-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
  padding: 3px 4px 4px;
  line-height: 16px;
  overflow: hidden;
  cursor: pointer;
}
.z-treefooter-content {
  line-height: 22px;
  cursor: default;
}
.z-treecell-text {
  vertical-align: middle;
}
.z-tree-paging-top {
  overflow: hidden;
  width: 100%;
}
.z-tree-paging-top .z-paging {
  border: none;
  border-bottom: 1px solid #E3E3E3;
}
.z-tree-paging-bottom {
  overflow: hidden;
  width: 100%;
}
.z-tree-paging-bottom .z-paging {
  border: none;
  border-top: 1px solid #E3E3E3;
}
.z-tree-autopaging .z-treecell-content {
  height: 32px;
  line-height: 24px;
  overflow: hidden;
}
.ie9 .z-tree-body {
  margin-top: auto;
}
.ie9 .z-treerow .z-treecell {
  position: static;
  z-index: auto;
}
