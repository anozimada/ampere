<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-paging {
  height: 30px;
  border: 1px solid #E3E3E3;
  padding: 3px 7px;
  background: #dfe1e1;
  position: relative;
}
.z-paging ul {
  display: inline-block;
  margin: 0;
  padding: 0;
}
.z-paging ul > li {
  display: inline;
}
.z-paging-button {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 600;
  font-style: normal;
  color: #000000;
  display: inline-block;
  min-width: 24px;
  height: 24px;
  border: 1px solid transparent;
  margin-right: 6px;
  padding: 3px;
  line-height: 16px;
  background: #FFFFFF;
  text-align: center;
  vertical-align: top;
  text-decoration: none;
  white-space: nowrap;
  cursor: pointer;
}
.z-paging-button:hover {
  color: #FFFFFF;
  border-color: rgba(50, 111, 112, 0.7);
  background: rgba(50, 111, 112, 0.7);
  -webkit-box-shadow: inset 0 -1px 0 rgba(50, 111, 112, 0.7);
  -moz-box-shadow: inset 0 -1px 0 rgba(50, 111, 112, 0.7);
  -o-box-shadow: inset 0 -1px 0 rgba(50, 111, 112, 0.7);
  -ms-box-shadow: inset 0 -1px 0 rgba(50, 111, 112, 0.7);
  box-shadow: inset 0 -1px 0 rgba(50, 111, 112, 0.7);
}
.z-paging-button:hover .z-paging-icon {
  color: #FFFFFF;
}
.z-paging-button:active {
  color: #FFFFFF;
  border-color: #668788;
  background: #668788;
  -webkit-box-shadow: inset 0px 1px 0 #326F70;
  -moz-box-shadow: inset 0px 1px 0 #326F70;
  -o-box-shadow: inset 0px 1px 0 #326F70;
  -ms-box-shadow: inset 0px 1px 0 #326F70;
  box-shadow: inset 0px 1px 0 #326F70;
}
.z-paging-button:active .z-paging-icon {
  color: #FFFFFF;
}
.z-paging-button[disabled] {
  color: #ACACAC;
  background: rgba(102, 135, 136, 0.35);
  cursor: default;
  opacity: 1;
  filter: alpha(opacity=100);;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  -o-box-shadow: none;
  -ms-box-shadow: none;
  box-shadow: none;
}
.z-paging-button[disabled]:hover,
.z-paging-button[disabled]:active {
  color: #ACACAC;
  border-color: #326F70;
  background: rgba(102, 135, 136, 0.35);
  cursor: default;
  opacity: 1;
  filter: alpha(opacity=100);;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  -o-box-shadow: none;
  -ms-box-shadow: none;
  box-shadow: none;
}
.z-paging-button[disabled] .z-paging-icon,
.z-paging-button[disabled] .z-paging-icon:hover {
  color: #ACACAC;
}
.z-paging .z-paging-icon {
  font-size: 125%;
  color: rgba(50, 111, 112, 0.8);
  line-height: 12px;
}
.z-paging-selected,
.z-paging-selected:hover,
.z-paging-selected:active {
  font-weight: 700;
  color: rgba(50, 111, 112, 0.8);
  border-color: transparent;
  background: #FFFFFF;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  -o-box-shadow: none;
  -ms-box-shadow: none;
  box-shadow: none;
  cursor: default;
}
.z-paging-input {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
  height: 24px;
  border: 1px solid #E3E3E3;
  padding: 3px;
  line-height: 16px;
  vertical-align: baseline;
}
.z-paging-input[disabled] {
  color: #ACACAC;
  opacity: 1;
  filter: alpha(opacity=100);;
  cursor: default;
}
.z-paging-text {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
  margin-right: 6px;
}
.z-paging-text-disabled {
  color: #ACACAC;
}
.z-paging-info {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
  display: inline-block;
  width: auto;
  height: 24px;
  line-height: 24px;
  position: absolute;
  right: 6px;
}
.ie9 .z-paging-input {
  line-height: normal;
}
