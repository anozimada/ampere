<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-selectbox {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  color: #000000;
  height: 32px;
  min-width: 180px;
  background-color: #FFFFFF;
  background-image: url("data:image/svg+xml;charset=utf8,%3Csvg%20fill%3D%27%23000000%27%20height%3D%2724%27%20viewBox%3D%270%200%2024%2024%27%20width%3D%2724%27%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%3E%3Cpath%20d%3D%27M7%2010l5%205%205-5z%27%2F%3E%3C%2Fsvg%3E");
  background-repeat: no-repeat;
  background-position: top 50% right 0;
  border: 1px solid #E3E3E3;
  border-radius: 0;
  padding: 0 7px;
  padding-right: 24px;
  -moz-appearance: none;
  -webkit-appearance: none;
  appearance: none;
}
.z-selectbox::-ms-expand {
  display: none;
}
.z-selectbox:hover {
  border-color: rgba(50, 111, 112, 0.7);
}
.z-selectbox:focus {
  border-color: rgba(50, 111, 112, 0.9);
}
.z-selectbox:active {
  color: #FFFFFF;
  background-image: url("data:image/svg+xml;charset=utf8,%3Csvg%20fill%3D%27%23FFFFFF%27%20height%3D%2724%27%20viewBox%3D%270%200%2024%2024%27%20width%3D%2724%27%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%3E%3Cpath%20d%3D%27M7%2010l5%205%205-5z%27%2F%3E%3C%2Fsvg%3E");
  background-color: #668788;
}
.z-selectbox[disabled] {
  color: #ACACAC;
  background-image: url("data:image/svg+xml;charset=utf8,%3Csvg%20fill%3D%27%23ACACAC%27%20height%3D%2724%27%20viewBox%3D%270%200%2024%2024%27%20width%3D%2724%27%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%3E%3Cpath%20d%3D%27M7%2010l5%205%205-5z%27%2F%3E%3C%2Fsvg%3E");
  background-color: #F2F2F2;
  cursor: default;
  border-color: #326F70;
}
.ie9 .z-selectbox {
  background-image: none;
}
.ie9 .z-selectbox:active {
  background-image: none;
  color: #000000;
  background-color: #FFFFFF;
}
.ie9 .z-selectbox[disabled] {
  background-image: none;
}
