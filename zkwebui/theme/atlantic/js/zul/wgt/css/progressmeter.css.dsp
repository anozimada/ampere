<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-progressmeter {
  height: 8px;
  border: 1px solid #E3E3E3;
  margin-top: 2px;
  background: #FFFFFF 0 0 repeat-x;
  background-image: url(${c:encodeThemeURL("~./zul/img/misc/prgmeter-anim.gif")});
  text-align: left;
  overflow: hidden;
}
.z-progressmeter-image {
  height: 6px;
  background: rgba(50, 111, 112, 0.8);
  font-size: 0;
  display: inline-block;
  vertical-align: top;
  line-height: 0;
}
.z-progressmeter-indeterminate {
  position: relative;
  opacity: 0.9;
  width: 150%;
}
.z-progressmeter-indeterminate > .z-progressmeter-image {
  position: absolute;
  height: 100%;
  animation: decrease 1.5s 0.5s infinite;
}
@keyframes decrease {
  from {
    left: -80%;
    width: 80%;
  }
  to {
    left: 110%;
    width: 10%;
  }
}
.z-loadingbar {
  width: 100%;
  height: 6px;
  overflow: hidden;
  position: relative;
  pointer-events: none;
}
.z-loadingbar-colorbar {
  background-color: rgba(50, 111, 112, 0.8);
  height: 100%;
}
.z-loadingbar-image {
  display: inline-block;
  vertical-align: top;
}
.z-loadingbar-effectbar {
  display: none;
}
.z-loadingbar-indeterminate {
  opacity: 0.9;
  width: 150%;
}
.z-loadingbar-indeterminate > .z-loadingbar-image {
  position: absolute;
  animation: ani1 3s ease-in infinite;
}
.z-loadingbar-indeterminate > .z-loadingbar-effectbar {
  display: inline-block;
  position: absolute;
  vertical-align: top;
  animation: ani2 3s ease infinite;
}
@keyframes ani1 {
  from {
    left: -10%;
    width: 10%;
  }
  13.33% {
    width: 10%;
  }
  60% {
    left: 100%;
    width: 100%;
  }
  to {
    left: 100%;
  }
}
@keyframes ani2 {
  from {
    left: -80%;
    width: 80%;
  }
  30% {
    width: 80%;
  }
  40% {
    left: -80%;
  }
  96% {
    left: 100%;
  }
  to {
    left: 100%;
    width: 5%;
  }
}
.z-loadingbar-position {
  display: -ms-flexbox;
  display: flex;
  position: fixed;
  width: 100%;
  left: 0;
  pointer-events: none;
}
.z-loadingbar-position > .z-loadingbar:nth-child(even) .z-loadingbar-colorbar {
  background-color: #326F70;
}
.z-loadingbar-position-top {
  -ms-flex-direction: column;
  flex-direction: column;
  bottom: unset;
  top: 0;
}
.z-loadingbar-position-bottom {
  -ms-flex-direction: column-reverse;
  flex-direction: column-reverse;
  top: unset;
  bottom: 0;
}
