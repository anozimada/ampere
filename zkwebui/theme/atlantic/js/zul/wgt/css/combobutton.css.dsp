<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-combobutton {
  display: inline-block;
  vertical-align: middle;
  min-height: 24px;
  cursor: pointer;
}
.z-combobutton-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 600;
  font-style: normal;
  color: #FFFFFF;
  display: block;
  min-height: 24px;
  border: 1px solid #7D443E;
  padding: 3px 8px;
  padding-right: 32px;
  line-height: 16px;
  background: #7D443E;
  vertical-align: middle;
  position: relative;
  white-space: nowrap;
}
.z-combobutton-button {
  display: block;
  width: 24px;
  height: 100%;
  border-left: 1px solid #668788;
  line-height: normal;
  position: absolute;
  top: 0;
  right: 0;
}
.z-combobutton-icon {
  font-size: 114%;
  color: #FFFFFF;
  display: block;
  width: 22px;
  line-height: 22px;
  text-align: center;
}
.z-combobutton:hover .z-combobutton-content {
  color: #FFFFFF;
  border-color: rgba(50, 111, 112, 0.7);
  background: rgba(50, 111, 112, 0.7);
  -webkit-box-shadow: inset 0 -1px 0 rgba(50, 111, 112, 0.7);
  -moz-box-shadow: inset 0 -1px 0 rgba(50, 111, 112, 0.7);
  -o-box-shadow: inset 0 -1px 0 rgba(50, 111, 112, 0.7);
  -ms-box-shadow: inset 0 -1px 0 rgba(50, 111, 112, 0.7);
  box-shadow: inset 0 -1px 0 rgba(50, 111, 112, 0.7);
}
.z-combobutton:hover .z-combobutton-button {
  border-left-color: rgba(41, 91, 92, 0.7);
}
.z-combobutton:focus .z-combobutton-content,
.z-combobutton:focus .z-combobutton-button {
  color: #FFFFFF;
  border-color: rgba(50, 111, 112, 0.9);
  background: rgba(50, 111, 112, 0.9);
}
.z-combobutton:focus .z-combobutton-button {
  border-left-color: rgba(41, 91, 92, 0.9);
}
.z-combobutton:active .z-combobutton-content {
  color: #FFFFFF;
  border-color: #668788;
  background: #668788;
  -webkit-box-shadow: inset 0px 1px 0 #326F70;
  -moz-box-shadow: inset 0px 1px 0 #326F70;
  -o-box-shadow: inset 0px 1px 0 #326F70;
  -ms-box-shadow: inset 0px 1px 0 #326F70;
  box-shadow: inset 0px 1px 0 #326F70;
}
.z-combobutton:active .z-combobutton-button {
  border-left-color: #5a7778;
}
.z-combobutton[disabled],
.z-combobutton[disabled]:hover {
  cursor: default;
}
.z-combobutton[disabled] .z-combobutton-content,
.z-combobutton[disabled]:hover .z-combobutton-content,
.z-combobutton[disabled] .z-combobutton-button,
.z-combobutton[disabled]:hover .z-combobutton-button,
.z-combobutton[disabled] .z-combobutton-icon,
.z-combobutton[disabled]:hover .z-combobutton-icon {
  color: #ACACAC;
  border-color: #326F70;
  background: rgba(102, 135, 136, 0.35);
  opacity: 1;
  filter: alpha(opacity=100);;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  -o-box-shadow: none;
  -ms-box-shadow: none;
  box-shadow: none;
}
.z-combobutton-toolbar .z-combobutton-content,
.z-combobutton-toolbar .z-combobutton-button {
  color: rgba(50, 111, 112, 0.8);
  background: transparent;
  border-color: transparent;
}
.z-combobutton-toolbar .z-combobutton-icon {
  color: rgba(50, 111, 112, 0.8);
}
.z-combobutton-toolbar:hover .z-combobutton-icon {
  color: #FFFFFF;
}
.z-combobutton-toolbar[disabled] .z-combobutton-content,
.z-combobutton-toolbar[disabled] .z-combobutton-button {
  border-color: transparent;
}
