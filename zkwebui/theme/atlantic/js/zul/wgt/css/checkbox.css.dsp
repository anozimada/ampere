<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-checkbox-default > .z-checkbox-mold {
  display: none;
}
.z-checkbox-tristate > .z-checkbox-mold {
  display: none;
}
.z-checkbox-switch {
  display: inline-block;
}
.z-checkbox-switch > input[type="checkbox"] {
  display: none;
}
.z-checkbox-switch > .z-checkbox-mold {
  margin: 2px 4px 4px;
  position: relative;
  width: 50px;
  height: 29px;
  transition: 0.4s;
  border-radius: 29px;
  display: inline-block;
  vertical-align: middle;
}
.z-checkbox-switch > .z-checkbox-mold:before {
  content: "";
  position: absolute;
  width: 21px;
  height: 21px;
  left: 4px;
  bottom: 4px;
  transition: 0.4s;
  border-radius: 50%;
  background-color: white;
}
.z-checkbox-switch > .z-checkbox-mold:focus {
  -webkit-box-shadow: 0 0 0px 2px rgba(50, 111, 112, 0.9);
  -moz-box-shadow: 0 0 0px 2px rgba(50, 111, 112, 0.9);
  -o-box-shadow: 0 0 0px 2px rgba(50, 111, 112, 0.9);
  -ms-box-shadow: 0 0 0px 2px rgba(50, 111, 112, 0.9);
  box-shadow: 0 0 0px 2px rgba(50, 111, 112, 0.9);
  transition: unset;
}
.z-checkbox-switch > .z-checkbox-content {
  display: inline-block;
}
.z-checkbox-switch-off > .z-checkbox-mold {
  background-color: #ACACAC;
}
.z-checkbox-switch-on > .z-checkbox-mold {
  background-color: #326F70;
}
.z-checkbox-switch-on > .z-checkbox-mold:before {
  -webkit-transform: translateX(21px);
  -moz-transform: translateX(21px);
  -o-transform: translateX(21px);
  -ms-transform: translateX(21px);
  transform: translateX(21px);
}
.z-checkbox-switch-disabled > .z-checkbox-mold {
  opacity: 0.5;
  cursor: default;
}
.z-checkbox-toggle {
  display: inline-block;
}
.z-checkbox-toggle > input[type="checkbox"] {
  display: none;
}
.z-checkbox-toggle > .z-checkbox-mold {
  margin: 0 4px 4px;
  width: 30px;
  height: 30px;
  border-radius: 4px;
  transition: 0.1s;
  display: inline-block;
  vertical-align: middle;
}
.z-checkbox-toggle > .z-checkbox-content {
  display: inline-block;
}
.z-checkbox-toggle-off > .z-checkbox-mold {
  background-color: #ACACAC;
  -webkit-box-shadow: 0 4px 1px rgba(0, 0, 0, 0.39);
  -moz-box-shadow: 0 4px 1px rgba(0, 0, 0, 0.39);
  -o-box-shadow: 0 4px 1px rgba(0, 0, 0, 0.39);
  -ms-box-shadow: 0 4px 1px rgba(0, 0, 0, 0.39);
  box-shadow: 0 4px 1px rgba(0, 0, 0, 0.39);
}
.z-checkbox-toggle-off > .z-checkbox-mold:focus {
  -webkit-box-shadow: 0 4px 1px rgba(0, 0, 0, 0.39), 0 1px 0px 2px rgba(50, 111, 112, 0.9);
  -moz-box-shadow: 0 4px 1px rgba(0, 0, 0, 0.39), 0 1px 0px 2px rgba(50, 111, 112, 0.9);
  -o-box-shadow: 0 4px 1px rgba(0, 0, 0, 0.39), 0 1px 0px 2px rgba(50, 111, 112, 0.9);
  -ms-box-shadow: 0 4px 1px rgba(0, 0, 0, 0.39), 0 1px 0px 2px rgba(50, 111, 112, 0.9);
  box-shadow: 0 4px 1px rgba(0, 0, 0, 0.39), 0 1px 0px 2px rgba(50, 111, 112, 0.9);
  transition: unset;
}
.z-checkbox-toggle-on > .z-checkbox-mold {
  background-color: #326F70;
  -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.48), 0 0 6px 2px rgba(0, 0, 0, 0.35) inset;
  -moz-box-shadow: 0 0 5px rgba(0, 0, 0, 0.48), 0 0 6px 2px rgba(0, 0, 0, 0.35) inset;
  -o-box-shadow: 0 0 5px rgba(0, 0, 0, 0.48), 0 0 6px 2px rgba(0, 0, 0, 0.35) inset;
  -ms-box-shadow: 0 0 5px rgba(0, 0, 0, 0.48), 0 0 6px 2px rgba(0, 0, 0, 0.35) inset;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.48), 0 0 6px 2px rgba(0, 0, 0, 0.35) inset;
  -webkit-transform: translateY(4px);
  -moz-transform: translateY(4px);
  -o-transform: translateY(4px);
  -ms-transform: translateY(4px);
  transform: translateY(4px);
}
.z-checkbox-toggle-on > .z-checkbox-mold:focus {
  -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.48) inset, 0 0 6px 2px rgba(0, 0, 0, 0.35) inset, 0 1px 0px 2px rgba(50, 111, 112, 0.9);
  -moz-box-shadow: 0 0 5px rgba(0, 0, 0, 0.48) inset, 0 0 6px 2px rgba(0, 0, 0, 0.35) inset, 0 1px 0px 2px rgba(50, 111, 112, 0.9);
  -o-box-shadow: 0 0 5px rgba(0, 0, 0, 0.48) inset, 0 0 6px 2px rgba(0, 0, 0, 0.35) inset, 0 1px 0px 2px rgba(50, 111, 112, 0.9);
  -ms-box-shadow: 0 0 5px rgba(0, 0, 0, 0.48) inset, 0 0 6px 2px rgba(0, 0, 0, 0.35) inset, 0 1px 0px 2px rgba(50, 111, 112, 0.9);
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.48) inset, 0 0 6px 2px rgba(0, 0, 0, 0.35) inset, 0 1px 0px 2px rgba(50, 111, 112, 0.9);
  transition: unset;
}
.z-checkbox-toggle-disabled > .z-checkbox-mold {
  opacity: 0.5;
  cursor: default;
}
