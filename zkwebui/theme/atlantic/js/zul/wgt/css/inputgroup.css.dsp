<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-inputgroup {
  display: -ms-inline-flexbox;
  display: inline-flex;
}
.z-inputgroup-text {
  display: -ms-flexbox;
  display: flex;
  background: #E9ECEF;
  border: 1px solid #326F70;
  padding: 0 12px;
  min-height: 32px;
  -ms-flex-align: center;
  align-items: center;
}
.z-inputgroup-vertical {
  -ms-flex-direction: column;
  flex-direction: column;
}
.z-inputgroup:not(.z-inputgroup-vertical) > :nth-child(n) {
  height: auto;
}
