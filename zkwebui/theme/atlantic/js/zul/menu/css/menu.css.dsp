<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-menubar {
  display: block;
  background: rgba(50, 111, 112, 0.8);
  position: relative;
}
.z-menubar ul {
  overflow: hidden;
  border: 0;
  margin: 0;
  padding: 0;
  background: transparent none repeat 0 0;
  position: relative;
  list-style-image: none;
  list-style-position: outside;
  list-style-type: none;
}
.z-menubar li {
  display: block;
  padding: 0;
  position: relative;
}
.z-menubar-horizontal li {
  margin-right: 2px;
  float: left;
}
.z-menubar-horizontal .z-menuseparator {
  width: 1px;
  border-left: 1px solid #FFFFFF;
  line-height: 36px;
}
.z-menubar-vertical ul {
  display: table;
  border-collapse: collapse;
  border-spacing: 0;
}
.z-menubar-vertical .z-menuseparator {
  height: 0;
  border-top: 1px solid #FFFFFF;
  line-height: 0;
}
.z-menubar-scroll {
  overflow: hidden;
}
.z-menubar-body {
  width: 100%;
  margin: 0 36px;
  position: relative;
  overflow: hidden;
}
.z-menubar-content {
  width: 5500px;
}
.z-menubar-scrollable {
  width: 36px;
  height: 100%;
  border-width: 0 1px;
  border-style: solid;
  border-color: #E3E3E3;
  line-height: normal;
  background: rgba(50, 111, 112, 0.8);
  position: absolute;
  top: 0;
  right: 0;
  cursor: pointer;
  z-index: 25;
}
.z-menubar-left,
.z-menubar-right {
  padding: 5px 11px;
}
.z-menubar-left {
  left: 0;
  padding: 5px 12px;
}
.z-menubar-icon {
  font-size: 12px;
  color: #FFFFFF;
  margin-top: -6px;
  margin-left: -6px;
  position: absolute;
  top: 50%;
  left: 50%;
}
.z-menu,
.z-menuitem {
  border: 0;
  margin: 0;
  padding: 0;
  background: transparent none repeat 0 0;
  position: relative;
  list-style-image: none;
  list-style-position: outside;
  list-style-type: none;
}
.z-menu-content,
.z-menuitem-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 114%;
  font-weight: 400;
  font-style: normal;
  color: #FFFFFF;
  display: block;
  min-height: 36px;
  border: 1px solid transparent;
  padding: 8px 7px;
  line-height: 18px;
  position: relative;
  cursor: pointer;
  text-decoration: none;
  white-space: nowrap;
  z-index: 20;
}
.z-menu-content:active,
.z-menuitem-content:active {
  color: rgba(50, 111, 112, 0.8);
  background: #FFFFFF;
}
.z-menu-content[disabled],
.z-menuitem-content[disabled],
.z-menu-content[disabled]:hover,
.z-menuitem-content[disabled]:hover {
  color: #ACACAC;
  background: none;
  opacity: 1;
  filter: alpha(opacity=100);;
  cursor: default;
}
.z-menu-image,
.z-menuitem-image {
  min-width: 16px;
  min-height: 16px;
  vertical-align: text-top;
  position: relative;
}
.z-menu-text,
.z-menuitem-text {
  margin: 0 2px;
}
.z-menu-separator {
  display: none;
}
.z-menu-clickable .z-menu-separator {
  width: 1px;
  height: 100%;
  background: rgba(50, 111, 112, 0.7);
  position: absolute;
  top: 0;
  right: 16px;
}
.z-menu-clickable.z-menu:hover .z-menu-separator {
  display: block;
}
.z-menu-clickable.z-menu:active .z-menu-separator,
.z-menu-clickable.z-menu-selected .z-menu-separator {
  background: rgba(50, 111, 112, 0.8);
}
.z-menu-clickable .z-menu-text {
  margin-right: 3px;
}
.z-menu-content {
  padding-right: 15px;
}
.z-menu-icon {
  color: #FFFFFF;
  position: absolute;
  top: 10px;
  right: 5px;
}
.z-menu-hover > .z-menu-content,
.z-menu-hover .z-menu-icon {
  color: rgba(50, 111, 112, 0.8);
  background: #FFFFFF;
}
.z-menu-selected > .z-menu-content,
.z-menu-selected .z-menu-icon {
  color: rgba(50, 111, 112, 0.8);
  background: #FFFFFF;
}
.z-menuitem-content:hover {
  color: rgba(50, 111, 112, 0.8);
  background: #FFFFFF;
}
.z-menuitem-selected > .z-menuitem-content {
  color: rgba(50, 111, 112, 0.8);
  background: #FFFFFF;
}
.z-menu-content-popup {
  display: none;
  width: auto;
  height: auto;
  position: absolute;
  overflow: auto;
  z-index: 88000;
}
.z-menupopup {
  padding: 4px;
  background: #FFFFFF;
  z-index: 88000;
  left: 0;
  top: 0;
  white-space: nowrap;
  max-height: 100%;
  overflow-y: auto;
}
.z-menupopup ul {
  border: 0;
  margin: 0;
  padding: 0;
  background: transparent none repeat 0 0;
  position: relative;
  list-style-image: none;
  list-style-position: outside;
  list-style-type: none;
}
.z-menupopup-separator {
  width: 2px;
  height: 100%;
  border-width: 3px 1px 3px 0;
  border-style: solid;
  border-color: #FFFFFF;
  background: #FFFFFF;
  position: absolute;
  top: 0;
  left: 31px;
  z-index: 10;
}
.z-menupopup .z-menu,
.z-menupopup .z-menuitem {
  background: #FFFFFF;
}
.z-menupopup .z-menu-content,
.z-menupopup .z-menuitem-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #ACACAC;
  min-height: 30px;
  padding: 5px 7px;
  padding-right: 15px;
}
.z-menupopup .z-menu-content[disabled],
.z-menupopup .z-menuitem-content[disabled],
.z-menupopup .z-menu-content[disabled]:hover,
.z-menupopup .z-menuitem-content[disabled]:hover {
  color: #ACACAC;
  background: rgba(102, 135, 136, 0.35);
  opacity: 1;
  filter: alpha(opacity=100);;
  cursor: default;
}
.z-menupopup .z-menu-image,
.z-menupopup .z-menuitem-image {
  margin-right: 9px;
}
.z-menupopup .z-menu-icon {
  color: #ACACAC;
  line-height: normal;
  top: 12px;
}
.z-menupopup .z-menu-content:hover,
.z-menupopup .z-menu-content:hover .z-menu-icon,
.z-menupopup .z-menuitem-content:hover {
  color: #FFFFFF;
  background: #BD652F;
}
.z-menupopup .z-menuitem-icon {
  font-size: 14px;
  color: #326F70;
  display: none;
  line-height: 20px;
  position: absolute;
  left: 8px;
  top: 10px;
}
.z-menupopup .z-menuseparator {
  font-size: 1px;
  display: block;
  width: auto;
  min-height: 2px;
  border-bottom: 1px solid #FFFFFF;
  padding: 0px;
  line-height: 1px;
  background: #FFFFFF;
  position: relative;
}
.z-menupopup [class^="z-icon"] {
  text-align: center;
  display: inline-block;
  min-width: 16px;
  margin-right: 9px;
}
.z-menuitem-checkable .z-menuitem-image {
  width: 16px;
  height: 16px;
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
  top: 0;
}
.z-menuitem-checked.z-menuitem-checkable .z-menuitem-icon {
  display: block;
}
