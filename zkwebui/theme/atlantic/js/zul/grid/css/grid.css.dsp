<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-grid {
  border: 1px solid #E3E3E3;
  overflow: hidden;
  zoom: 1;
}
.z-grid-header {
  width: 100%;
  background: #326F70;
  overflow: hidden;
}
.z-grid-header table {
  border-spacing: 0;
}
.z-grid-header table th,
.z-grid-header table td {
  background-clip: padding-box;
  padding: 0;
}
.z-grid-header table th {
  text-align: inherit;
}
.z-grid-body {
  margin-top: auto;
  position: relative;
  overflow: hidden;
  overflow-anchor: none;
}
.z-grid-body table {
  border-spacing: 0;
}
.z-grid-body table th,
.z-grid-body table td {
  background-clip: padding-box;
  padding: 0;
}
.z-grid-body table th {
  text-align: inherit;
}
.z-grid-emptybody td {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #ACACAC;
  font-style: italic;
  padding: 4px 8px !important;
  line-height: 16px;
  text-align: center;
  height: 1px;
}
.z-grid-footer {
  border-top: 1px solid #E3E3E3;
  background: #dfe1e1;
  overflow: hidden;
  white-space: nowrap;
}
.z-grid-footer table {
  border-spacing: 0;
}
.z-grid-footer table th,
.z-grid-footer table td {
  background-clip: padding-box;
  padding: 0;
}
.z-grid-footer table th {
  text-align: inherit;
}
.z-grid-footer .z-footer {
  overflow: hidden;
  background: #dfe1e1;
}
.z-grid-footer .z-foot-bar {
  background: #dfe1e1;
}
.z-grid-odd > .z-row-inner,
.z-grid-odd > .z-cell {
  background: #FFFFFF;
}
.z-grid-loading {
  background: transparent no-repeat center;
  background-image: url(${c:encodeThemeURL("~./zul/img/misc/progress-large.gif")});
}
.z-columns th:first-child {
  border-left-width: 0;
}
.z-columns th:first-child.z-columns-border {
  border-left: 1px solid #FFFFFF;
}
.z-columns-bar {
  background: #326F70;
  border-top: 1px solid #FFFFFF;
  border-left: 1px solid #FFFFFF;
}
.z-column {
  border-top: 1px solid #FFFFFF;
  border-left: 1px solid #FFFFFF;
  padding: 0;
  background: #326F70;
  position: relative;
  overflow: hidden;
  white-space: nowrap;
}
.z-column-hover .z-column-button {
  display: block;
}
.z-column-sort .z-column-content {
  cursor: pointer;
}
.z-column-sort .z-column-sorticon {
  font-size: 100%;
  color: #FFFFFF;
  line-height: normal;
  position: absolute;
  top: 0;
  left: 50%;
}
.z-column-button {
  color: #FFFFFF;
  display: none;
  width: 36px;
  height: 35px;
  border-left: 1px solid #FFFFFF;
  line-height: 36px;
  text-align: center;
  position: absolute;
  top: 0;
  right: 0;
  text-decoration: none;
  z-index: 15;
  cursor: pointer;
}
.z-column-button:hover {
  background: #2a5e5e;
}
.z-column-sizing,
.z-column-sizing .z-column-button,
.z-column-sizing.z-column-sort .z-column-content {
  cursor: e-resize;
}
.z-row:first-child .z-row-inner,
.z-row:first-child .z-cell {
  border-top-width: 0;
}
.z-row:first-child .z-row-content,
.z-row:first-child .z-group-content {
  padding-top: 4px;
}
.z-row .z-row-inner,
.z-row .z-cell {
  border-top: 1px solid #E3E3E3;
  background: #FFFFFF;
  overflow: hidden;
  position: relative;
  z-index: 0;
}
.z-row:hover > .z-row-inner,
.z-row:hover > .z-cell {
  background: rgba(50, 111, 112, 0.7);
}
.z-row:hover > .z-row-inner > .z-row-content {
  color: #FFFFFF;
}
.z-group-inner {
  border-top: 1px solid #FFFFFF;
  background: #E3E3E3;
  overflow: hidden;
}
.z-group-inner .z-group-content,
.z-group-inner .z-cell {
  padding: 3px 4px 4px;
}
.z-group:first-child .z-group-inner {
  border-top-width: 0;
}
.z-group-icon {
  font-size: 114%;
  color: #000000;
  display: inline-block;
  width: 18px;
  height: 18px;
  text-align: center;
  vertical-align: top;
  position: relative;
  cursor: pointer;
}
.z-groupfoot-inner {
  border-top: 1px solid #E3E3E3;
  background: #dfe1e1;
  overflow: hidden;
}
.z-column-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 85%;
  font-weight: 600;
  font-style: normal;
  color: #FFFFFF;
  padding: 4px 8px;
  line-height: 28px;
  position: relative;
  overflow: hidden;
}
.z-row-content,
.z-group-content,
.z-groupfoot-content,
.z-footer-content {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
  padding: 3px 4px 4px;
  line-height: 16px;
  overflow: hidden;
}
.z-group-content,
.z-groupfoot-content,
.z-footer-content {
  line-height: 22px;
}
.z-group-content .z-label,
.z-groupfoot-content .z-label {
  font-weight: 600;
}
.z-grid-body .z-cell {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
  padding: 4px;
  line-height: 22px;
  overflow: hidden;
}
.z-grid-paging-top {
  overflow: hidden;
  width: 100%;
}
.z-grid-paging-top .z-paging {
  border: none;
  border-bottom: 1px solid #E3E3E3;
}
.z-grid-paging-bottom {
  overflow: hidden;
  width: 100%;
}
.z-grid-paging-bottom .z-paging {
  border: none;
  border-top: 1px solid #E3E3E3;
}
.z-grid-autopaging .z-row-content,
.z-grid-autopaging .z-groupfoot-content {
  height: 32px;
  line-height: 24px;
  overflow: hidden;
}
.z-grid-autopaging .z-group-content {
  height: 30px;
  line-height: 22px;
  overflow: hidden;
}
.z-columns-menugrouping .z-menuitem-image {
  background-image: url(${c:encodeThemeURL("~./zul/img/grid/menu-group.png")});
}
.z-columns-menuungrouping .z-menuitem-image {
  background-image: url(${c:encodeThemeURL("~./zul/img/grid/menu-ungroup.png")});
}
.z-columns-menuascending .z-menuitem-image {
  background-image: url(${c:encodeThemeURL("~./zul/img/grid/menu-arrowup.png")});
}
.z-columns-menudescending .z-menuitem-image {
  background-image: url(${c:encodeThemeURL("~./zul/img/grid/menu-arrowdown.png")});
}
.z-cell-hidden-column,
.z-row-hidden-column {
  white-space: nowrap;
  overflow: hidden;
}
.ie9 .z-row .z-row-inner,
.ie9 .z-row .z-cell {
  position: static;
  z-index: auto;
}
