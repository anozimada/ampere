<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-toolbar.z-toolbar-tabs {
  position: absolute;
  top: 0;
  right: 0;
  overflow: hidden;
  z-index: 1;
}
.z-tabbox {
  position: relative;
  overflow: hidden;
}
.z-tabbox-icon {
  font-size: 85%;
  color: #FFFFFF;
  display: none;
  min-height: 36px;
  border: 1px solid #E3E3E3;
  line-height: 34px;
  background: #326F70;
  text-align: center;
  position: absolute;
  top: 0;
  cursor: pointer;
  z-index: 25;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
.z-tabbox-icon:hover {
  background: rgba(50, 111, 112, 0.9);
}
.z-tabbox-left-scroll,
.z-tabbox-right-scroll {
  width: 36px;
}
.z-tabbox-right-scroll {
  right: 0;
}
.z-tabbox-up-scroll,
.z-tabbox-down-scroll {
  height: 36px;
}
.z-tabbox-down-scroll {
  top: auto;
  bottom: 0;
}
.z-tabbox-top > .z-tabs .z-tabs-content {
  border-bottom: 1px solid #E3E3E3;
  white-space: nowrap;
}
.z-tabbox-top > .z-tabs .z-tab {
  display: inline-block;
  border-bottom-width: 0;
  padding-bottom: 8px;
  float: none;
  vertical-align: bottom;
}
.z-tabbox-top > .z-tabs .z-tab.z-tab-selected {
  -webkit-box-shadow: 0 1px 0 #FFFFFF;
  -moz-box-shadow: 0 1px 0 #FFFFFF;
  -o-box-shadow: 0 1px 0 #FFFFFF;
  -ms-box-shadow: 0 1px 0 #FFFFFF;
  box-shadow: 0 1px 0 #FFFFFF;
}
.z-tabbox-top > .z-tabpanels > .z-tabpanel {
  border-top-width: 0;
  padding-top: 4px;
}
.z-tabbox-top > .z-toolbar-tabs {
  border-width: 0 0 1px 0;
  padding: 8px 8px 7px;
}
.z-tabbox-bottom > .z-tabs .z-tabs-content {
  border-top: 1px solid #E3E3E3;
  white-space: nowrap;
}
.z-tabbox-bottom > .z-tabs .z-tab {
  display: inline-block;
  border-top-width: 0;
  padding-top: 8px;
  float: none;
  vertical-align: bottom;
}
.z-tabbox-bottom > .z-tabs .z-tab.z-tab-selected {
  -webkit-box-shadow: 0 -1px 0 #FFFFFF;
  -moz-box-shadow: 0 -1px 0 #FFFFFF;
  -o-box-shadow: 0 -1px 0 #FFFFFF;
  -ms-box-shadow: 0 -1px 0 #FFFFFF;
  box-shadow: 0 -1px 0 #FFFFFF;
}
.z-tabbox-bottom > .z-tabpanels > .z-tabpanel {
  border-bottom-width: 0;
  padding-bottom: 8px;
}
.z-tabbox-bottom > .z-tabbox-icon {
  top: auto;
  bottom: 0;
}
.z-tabbox-bottom > .z-toolbar-tabs {
  border-width: 1px 0 0 0;
  padding: 7px 8px 8px;
  top: auto;
  bottom: 0;
}
.z-tabbox-left > .z-tabs {
  float: left;
}
.z-tabbox-left > .z-tabs .z-tabs-content {
  display: block;
  width: 100%;
  border-right: 1px solid #E3E3E3;
}
.z-tabbox-left > .z-tabs .z-tab {
  border-right-width: 0;
  padding-right: 8px;
  float: none;
}
.z-tabbox-left > .z-tabs .z-tab-button {
  left: 8px;
  right: auto;
}
.z-tabbox-left > .z-tabs .z-tab-button + .z-tab-text {
  margin-right: 0;
  margin-left: 26px;
}
.z-tabbox-left > .z-tabs .z-tab.z-tab-selected {
  -webkit-box-shadow: 1px 0 0 #FFFFFF;
  -moz-box-shadow: 1px 0 0 #FFFFFF;
  -o-box-shadow: 1px 0 0 #FFFFFF;
  -ms-box-shadow: 1px 0 0 #FFFFFF;
  box-shadow: 1px 0 0 #FFFFFF;
}
.z-tabbox-left > .z-tabpanels > .z-tabpanel {
  border-left-width: 0;
  padding-left: 4px;
}
.z-tabbox-left.z-tabbox-scroll > .z-tabs {
  margin: 36px 0px;
}
.z-tabbox-right > .z-tabs {
  float: right;
}
.z-tabbox-right > .z-tabs .z-tabs-content {
  display: block;
  width: 100%;
  height: 4096px;
  border-left: 1px solid #E3E3E3;
}
.z-tabbox-right > .z-tabs .z-tab {
  border-left-width: 0;
  padding-left: 8px;
  float: none;
}
.z-tabbox-right > .z-tabs .z-tab.z-tab-selected {
  -webkit-box-shadow: -1px 0 0 #FFFFFF;
  -moz-box-shadow: -1px 0 0 #FFFFFF;
  -o-box-shadow: -1px 0 0 #FFFFFF;
  -ms-box-shadow: -1px 0 0 #FFFFFF;
  box-shadow: -1px 0 0 #FFFFFF;
}
.z-tabbox-right > .z-tabpanels > .z-tabpanel {
  border-right-width: 0;
  padding-right: 4px;
}
.z-tabbox-right.z-tabbox-scroll > .z-tabs {
  margin: 36px 0;
}
.z-tabbox-right > .z-tabbox-icon {
  right: 0;
}
.z-tabbox-scroll > .z-tabs {
  margin: 0px 36px;
}
.z-tabbox-scroll > .z-tabbox-icon {
  display: block;
}
.z-tabbox-accordion > .z-tabpanels {
  border-top: 1px solid #E3E3E3;
}
.z-tabbox-accordion > .z-tabpanels > .z-tabpanel {
  border: none;
  padding: 0;
}
.z-tabbox-accordion .z-tabpanel > .z-tabpanel-content {
  border: 1px solid #E3E3E3;
  border-top-width: 0px;
  padding: 4px;
  background: #FFFFFF;
  zoom: 1;
}
.z-tabbox-accordion .z-tabpanel > .z-tab {
  color: #FFFFFF;
  border-top-width: 0;
  padding-top: 8px;
  background: #326F70;
  text-align: left;
  float: none;
  zoom: 1;
}
.z-tabbox-accordion .z-tabpanel > .z-tab-selected {
  background: rgba(50, 111, 112, 0.8);
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  -o-box-shadow: none;
  -ms-box-shadow: none;
  box-shadow: none;
  cursor: default;
}
.z-tabbox-accordion .z-tabpanel > .z-tab-selected .z-tab-button,
.z-tabbox-accordion .z-tabpanel > .z-tab-selected .z-tab-text {
  color: #FFFFFF;
}
.z-tabbox-accordion .z-tabpanel > .z-tab-selected .z-tab-button:hover {
  color: rgba(50, 111, 112, 0.8);
  background: #FFFFFF;
}
.z-tabbox-accordion .z-tabpanel > .z-tab-disabled {
  color: #ACACAC;
  background: rgba(102, 135, 136, 0.35);
  opacity: 1;
  filter: alpha(opacity=100);;
  cursor: default;
}
.z-tabbox-accordion .z-tabpanel > .z-tab-disabled .z-tab-button {
  color: #ACACAC;
}
.z-tabbox-accordion .z-tabpanel > .z-tab-disabled .z-tab-button:hover {
  opacity: 1;
  filter: alpha(opacity=100);;
}
.z-tabbox-accordion .z-tabpanel > .z-tab-disabled .z-tab-text {
  color: #ACACAC;
  opacity: 1;
  filter: alpha(opacity=100);;
  white-space: nowrap;
  cursor: default;
}
.z-tabs {
  position: relative;
  overflow: hidden;
}
.z-tabs-content {
  display: table;
  width: 100%;
  height: 100%;
  border-collapse: separate;
  border-spacing: 0;
  margin: 0;
  padding: 0;
  list-style-image: none;
  list-style-position: outside;
  list-style-type: none;
}
.z-tab {
  display: block;
  border: 1px solid #E3E3E3;
  padding: 7px;
  line-height: 20px;
  background: #326F70;
  text-align: center;
  position: relative;
  cursor: pointer;
  float: left;
}
.z-tab-content {
  display: block;
}
.z-tab:hover {
  background: rgba(50, 111, 112, 0.9);
}
.z-tab-text {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 114%;
  font-weight: 400;
  font-style: normal;
  color: #FFFFFF;
  display: block;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}
.z-tab-image {
  vertical-align: middle;
}
.z-tab-button {
  font-size: 100%;
  color: #FFFFFF;
  width: 18px;
  height: 18px;
  margin-top: -9px;
  line-height: 18px;
  text-align: center;
  position: absolute;
  top: 50%;
  right: 8px;
  cursor: pointer;
  z-index: 15;
  zoom: 1;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
.z-tab-button:hover {
  color: rgba(50, 111, 112, 0.8);
  background: #FFFFFF;
}
.z-tab-button + .z-tab-text {
  margin-right: 26px;
}
.z-tab .z-caption {
  margin: auto;
}
.z-tab-selected {
  background: #FFFFFF;
}
.z-tab-selected:hover {
  background: #FFFFFF;
}
.z-tab-selected .z-tab-text {
  font-weight: 600;
  color: rgba(50, 111, 112, 0.8);
  cursor: default;
}
.z-tab-selected .z-tab-button {
  color: rgba(50, 111, 112, 0.8);
}
.z-tab-selected .z-tab-button:hover {
  color: #FFFFFF;
  background: rgba(50, 111, 112, 0.7);
}
.z-tab-disabled {
  background: rgba(102, 135, 136, 0.35);
  opacity: 1;
  filter: alpha(opacity=100);;
  cursor: default;
}
.z-tab-disabled:hover {
  background: rgba(102, 135, 136, 0.35);
}
.z-tab-disabled .z-tab-button,
.z-tab-disabled .z-tab-button:hover {
  color: #ACACAC;
  background: rgba(102, 135, 136, 0.35);
  opacity: 1;
  filter: alpha(opacity=100);;
  cursor: default;
}
.z-tab-disabled .z-tab-text {
  color: #ACACAC;
  cursor: default;
  white-space: nowrap;
}
.z-tabpanels {
  position: relative;
  overflow: hidden;
  zoom: 1;
}
.z-tabpanel {
  border: 1px solid #E3E3E3;
  padding: 3px;
  background: #FFFFFF;
}
.ie9 .z-tabs {
  line-height: 1px;
}
.ie9 .z-tabs > .z-tabs-content {
  display: inline-block;
}
