# Ampere

Adempiere with a few things left out.

## Getting Started

Introduction to building (using gradle) and deploying (with Docker).

### Prerequisites

You need git, gradle and docker-compose (tested with version 1.26.2) installed on your system.  OpenSSL (version >= 1.1.1) is also required if you want to generate self-signed certificates. Currently compiles against openjdk-11.

Instructions and scripts assume Ubuntu 18.04 as the OS. 


### Installing

Clone this repository.

```
git clone https://gitlab.com/adaxa/ampere.git
cd ampere
```
Run the build.

```
./gradlew build
```
Install build artifacts

```
./gradlew installDist
```
Change to the output directory


```
cd build/install/ampere/
```
Edit the config file (set passwords and hostname and domain)

```
vi .env
```
Create self-signed certificates for testing on system without access for letsencrypt (NB script uses sudo to update host certificate store which you may not want to do!)

```
cd certs
./generate_ssl_cert_and_truststore
cd ..
```
Add rule to hosts for your selected hostname

```
sudo -- sh -c "echo 127.0.0.1 www.ampere.local >> /etc/hosts"

```

Create a docker network, then fire up the docker containers

```
docker network create web
docker-compose build
docker-compose up
```

Open https://www.ampere.local in your browser to view landing page.

Default user/password for accessing ampere web UI is superadmin/sa.

Open https://www.ampere.local:8080/dashboard to see the Traefik reverse proxy UI


## License

This project is licensed under GPL v2 as for Compiere - see the [license.html](license.html) file for details

## Acknowledgments

* [Compiere](http://www.compiere.com/) for the original code
* [Adempiere](http://www.adempiere.net/)
* [iDempiere](https://www.idempiere.org/)