
CREATE OR REPLACE RULE alter_column_rule AS
    ON INSERT TO t_alter_column DO INSTEAD  SELECT altercolumn(new.tablename, new.columnname, new.datatype, new.nullclause, new.defaultclause) AS altercolumn;

