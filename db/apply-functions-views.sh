#!/bin/sh

DB_NAME=${1:-ampere}
DB_USER=${2:-ampere}
DB_PASS=${3:-ampere}
DB_HOST=${4:-ampere-postgres}
DB_PORT=${5:-5432}

execute_sql () {
    echo "Applying script: " $1
    psql -U $DB_USER -h $DB_HOST -p $DB_PORT -d $DB_NAME -f "$1"
}

export PGPASSWORD=$DB_PASS
 
psql -U $DB_USER -h $DB_HOST -p $DB_PORT -d $DB_NAME -c "SET search_path TO 'ampere';" 

find ./functions -name "*.sql" -type f | sort -n | \
while read f
do
    execute_sql "$f"
done

find ./operators -name "*.sql" -type f | sort -n | \
while read f
do
    execute_sql "$f"
done

find ./rules -name "*.sql" -type f | sort -n | \
while read f
do
    execute_sql "$f"
done

find ./views -name "*.sql" -type f | sort -n | \
while read f
do
    execute_sql "$f"
done

export PGPASSWORD=
