--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2 (Debian 14.2-1.pgdg110+1)
-- Dumped by pg_dump version 14.2 (Debian 14.2-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: a_asset_acct a_asset_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_acct
    ADD CONSTRAINT a_asset_acct_pkey PRIMARY KEY (a_asset_acct_id);


--
-- Name: a_asset_addition a_asset_addition_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_addition
    ADD CONSTRAINT a_asset_addition_pkey PRIMARY KEY (a_asset_addition_id);


--
-- Name: a_asset_change_amt a_asset_change_amt_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_change_amt
    ADD CONSTRAINT a_asset_change_amt_pkey PRIMARY KEY (a_asset_change_id, c_acctschema_id);


--
-- Name: a_asset_change a_asset_change_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_change
    ADD CONSTRAINT a_asset_change_pkey PRIMARY KEY (a_asset_change_id);


--
-- Name: a_asset_delivery a_asset_delivery_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_delivery
    ADD CONSTRAINT a_asset_delivery_pkey PRIMARY KEY (a_asset_delivery_id);


--
-- Name: a_asset_disposed a_asset_disposed_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_disposed
    ADD CONSTRAINT a_asset_disposed_key PRIMARY KEY (a_asset_disposed_id);


--
-- Name: a_asset_group_acct a_asset_group_acct_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_group_acct
    ADD CONSTRAINT a_asset_group_acct_key PRIMARY KEY (a_asset_group_acct_id);


--
-- Name: a_asset_group a_asset_group_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_group
    ADD CONSTRAINT a_asset_group_pkey PRIMARY KEY (a_asset_group_id);


--
-- Name: a_asset_info_fin a_asset_info_fin_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_info_fin
    ADD CONSTRAINT a_asset_info_fin_key PRIMARY KEY (a_asset_info_fin_id);


--
-- Name: a_asset_info_ins a_asset_info_ins_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_info_ins
    ADD CONSTRAINT a_asset_info_ins_key PRIMARY KEY (a_asset_info_ins_id);


--
-- Name: a_asset_info_lic a_asset_info_lic_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_info_lic
    ADD CONSTRAINT a_asset_info_lic_key PRIMARY KEY (a_asset_info_lic_id);


--
-- Name: a_asset_info_oth a_asset_info_oth_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_info_oth
    ADD CONSTRAINT a_asset_info_oth_key PRIMARY KEY (ad_client_id);


--
-- Name: a_asset_info_tax a_asset_info_tax_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_info_tax
    ADD CONSTRAINT a_asset_info_tax_key PRIMARY KEY (a_asset_info_tax_id);


--
-- Name: a_asset a_asset_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset
    ADD CONSTRAINT a_asset_pkey PRIMARY KEY (a_asset_id);


--
-- Name: a_asset_retirement a_asset_retirement_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_retirement
    ADD CONSTRAINT a_asset_retirement_pkey PRIMARY KEY (a_asset_retirement_id);


--
-- Name: a_asset_reval_entry a_asset_reval_entry_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_reval_entry
    ADD CONSTRAINT a_asset_reval_entry_key PRIMARY KEY (a_asset_reval_entry_id);


--
-- Name: a_asset_reval_index a_asset_reval_index_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_reval_index
    ADD CONSTRAINT a_asset_reval_index_key PRIMARY KEY (a_asset_reval_index_id);


--
-- Name: a_asset_split a_asset_split_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_split
    ADD CONSTRAINT a_asset_split_key PRIMARY KEY (a_asset_split_id);


--
-- Name: a_asset_spread a_asset_spread_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_spread
    ADD CONSTRAINT a_asset_spread_key PRIMARY KEY (a_asset_spread_id);


--
-- Name: a_asset_transfer a_asset_transfer_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_transfer
    ADD CONSTRAINT a_asset_transfer_key PRIMARY KEY (a_asset_transfer_id);


--
-- Name: a_asset_use a_asset_use_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_use
    ADD CONSTRAINT a_asset_use_pkey PRIMARY KEY (a_asset_use_id);


--
-- Name: a_depreciation_build a_depreciation_build_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_build
    ADD CONSTRAINT a_depreciation_build_key PRIMARY KEY (a_depreciation_build_id);


--
-- Name: a_depreciation_convention a_depreciation_convention_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_convention
    ADD CONSTRAINT a_depreciation_convention_key PRIMARY KEY (a_depreciation_convention_id);


--
-- Name: a_depreciation_entry a_depreciation_entry_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_entry
    ADD CONSTRAINT a_depreciation_entry_key PRIMARY KEY (a_depreciation_entry_id);


--
-- Name: a_depreciation_exp a_depreciation_exp_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_exp
    ADD CONSTRAINT a_depreciation_exp_key PRIMARY KEY (a_depreciation_exp_id);


--
-- Name: a_depreciation_forecast a_depreciation_forecast_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_forecast
    ADD CONSTRAINT a_depreciation_forecast_key PRIMARY KEY (a_depreciation_forecast_id);


--
-- Name: a_depreciation_method a_depreciation_method_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_method
    ADD CONSTRAINT a_depreciation_method_key PRIMARY KEY (a_depreciation_method_id);


--
-- Name: a_depreciation a_depreciation_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation
    ADD CONSTRAINT a_depreciation_pkey PRIMARY KEY (a_depreciation_id);


--
-- Name: a_depreciation_table_detail a_depreciation_table_detai_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_table_detail
    ADD CONSTRAINT a_depreciation_table_detai_key PRIMARY KEY (a_depreciation_table_detail_id);


--
-- Name: a_depreciation_table_header a_depreciation_table_heade_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_table_header
    ADD CONSTRAINT a_depreciation_table_heade_key PRIMARY KEY (a_depreciation_table_header_id);


--
-- Name: a_depreciation_workfile a_depreciation_workfile_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_workfile
    ADD CONSTRAINT a_depreciation_workfile_key PRIMARY KEY (a_depreciation_workfile_id);


--
-- Name: a_registration a_registration_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_registration
    ADD CONSTRAINT a_registration_pkey PRIMARY KEY (a_registration_id);


--
-- Name: a_registrationattribute a_registrationattribute_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_registrationattribute
    ADD CONSTRAINT a_registrationattribute_pkey PRIMARY KEY (a_registrationattribute_id);


--
-- Name: a_registrationproduct a_registrationproduct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_registrationproduct
    ADD CONSTRAINT a_registrationproduct_pkey PRIMARY KEY (a_registrationattribute_id, m_product_id);


--
-- Name: a_registrationvalue a_registrationvalue_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_registrationvalue
    ADD CONSTRAINT a_registrationvalue_pkey PRIMARY KEY (a_registration_id, a_registrationattribute_id);


--
-- Name: ad_accesslog ad_accesslog_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_accesslog
    ADD CONSTRAINT ad_accesslog_pkey PRIMARY KEY (ad_accesslog_id);


--
-- Name: ad_alert ad_alert_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_alert
    ADD CONSTRAINT ad_alert_pkey PRIMARY KEY (ad_alert_id);


--
-- Name: ad_alertprocessor ad_alertprocessor_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_alertprocessor
    ADD CONSTRAINT ad_alertprocessor_pkey PRIMARY KEY (ad_alertprocessor_id);


--
-- Name: ad_alertprocessorlog ad_alertprocessorlog_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_alertprocessorlog
    ADD CONSTRAINT ad_alertprocessorlog_pkey PRIMARY KEY (ad_alertprocessor_id, ad_alertprocessorlog_id);


--
-- Name: ad_alertrecipient ad_alertrecipient_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_alertrecipient
    ADD CONSTRAINT ad_alertrecipient_pkey PRIMARY KEY (ad_alertrecipient_id);


--
-- Name: ad_alertrule ad_alertrule_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_alertrule
    ADD CONSTRAINT ad_alertrule_pkey PRIMARY KEY (ad_alertrule_id);


--
-- Name: ad_archive ad_archive_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_archive
    ADD CONSTRAINT ad_archive_pkey PRIMARY KEY (ad_archive_id);


--
-- Name: ad_attachment ad_attachment_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_attachment
    ADD CONSTRAINT ad_attachment_pkey PRIMARY KEY (ad_attachment_id);


--
-- Name: ad_attachmententry ad_attachmententry_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_attachmententry
    ADD CONSTRAINT ad_attachmententry_key PRIMARY KEY (ad_attachmententry_id);


--
-- Name: ad_attachmentnote ad_attachmentnote_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_attachmentnote
    ADD CONSTRAINT ad_attachmentnote_pkey PRIMARY KEY (ad_attachmentnote_id);


--
-- Name: ad_attribute ad_attribute_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_attribute
    ADD CONSTRAINT ad_attribute_pkey PRIMARY KEY (ad_attribute_id);


--
-- Name: ad_attribute_value ad_attribute_value_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_attribute_value
    ADD CONSTRAINT ad_attribute_value_pkey PRIMARY KEY (ad_attribute_id, record_id);


--
-- Name: ad_chart ad_chart_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_chart
    ADD CONSTRAINT ad_chart_key PRIMARY KEY (ad_chart_id);


--
-- Name: ad_chartdatasource ad_chartdatasource_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_chartdatasource
    ADD CONSTRAINT ad_chartdatasource_key PRIMARY KEY (ad_chartdatasource_id);


--
-- Name: ad_client ad_client_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_client
    ADD CONSTRAINT ad_client_pkey PRIMARY KEY (ad_client_id);


--
-- Name: ad_clientinfo ad_clientinfo_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT ad_clientinfo_pkey PRIMARY KEY (ad_client_id);


--
-- Name: ad_clientshare ad_clientshare_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientshare
    ADD CONSTRAINT ad_clientshare_pkey PRIMARY KEY (ad_clientshare_id);


--
-- Name: ad_color ad_color_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_color
    ADD CONSTRAINT ad_color_pkey PRIMARY KEY (ad_color_id);


--
-- Name: ad_column_access ad_column_access_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column_access
    ADD CONSTRAINT ad_column_access_pkey PRIMARY KEY (ad_role_id, ad_column_id);


--
-- Name: ad_column ad_column_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column
    ADD CONSTRAINT ad_column_pkey PRIMARY KEY (ad_column_id);


--
-- Name: ad_column_trl ad_column_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column_trl
    ADD CONSTRAINT ad_column_trl_pkey PRIMARY KEY (ad_column_id, ad_language);


--
-- Name: ad_columnprocess ad_columnprocess_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_columnprocess
    ADD CONSTRAINT ad_columnprocess_key PRIMARY KEY (ad_columnprocess_id);


--
-- Name: ad_columnprocesspara ad_columnprocesspara_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_columnprocesspara
    ADD CONSTRAINT ad_columnprocesspara_key PRIMARY KEY (ad_columnprocesspara_id);


--
-- Name: ad_document_action_access ad_document_action_access_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_document_action_access
    ADD CONSTRAINT ad_document_action_access_key PRIMARY KEY (ad_role_id, c_doctype_id, ad_ref_list_id);


--
-- Name: ad_element ad_element_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_element
    ADD CONSTRAINT ad_element_pkey PRIMARY KEY (ad_element_id);


--
-- Name: ad_element_trl ad_element_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_element_trl
    ADD CONSTRAINT ad_element_trl_pkey PRIMARY KEY (ad_element_id, ad_language);


--
-- Name: ad_entitytype ad_entitytype_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_entitytype
    ADD CONSTRAINT ad_entitytype_pkey PRIMARY KEY (entitytype);


--
-- Name: ad_error ad_error_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_error
    ADD CONSTRAINT ad_error_pkey PRIMARY KEY (ad_error_id);


--
-- Name: ad_eventlog ad_eventlog_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_eventlog
    ADD CONSTRAINT ad_eventlog_key PRIMARY KEY (ad_eventlog_id);


--
-- Name: ad_field ad_field_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_field
    ADD CONSTRAINT ad_field_pkey PRIMARY KEY (ad_field_id);


--
-- Name: ad_field_trl ad_field_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_field_trl
    ADD CONSTRAINT ad_field_trl_pkey PRIMARY KEY (ad_field_id, ad_language);


--
-- Name: ad_fieldgroup ad_fieldgroup_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_fieldgroup
    ADD CONSTRAINT ad_fieldgroup_pkey PRIMARY KEY (ad_fieldgroup_id);


--
-- Name: ad_fieldgroup_trl ad_fieldgroup_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_fieldgroup_trl
    ADD CONSTRAINT ad_fieldgroup_trl_pkey PRIMARY KEY (ad_fieldgroup_id, ad_language);


--
-- Name: ad_find ad_find_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_find
    ADD CONSTRAINT ad_find_pkey PRIMARY KEY (ad_find_id);


--
-- Name: ad_form_access ad_form_access_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_form_access
    ADD CONSTRAINT ad_form_access_pkey PRIMARY KEY (ad_form_id, ad_role_id);


--
-- Name: ad_form ad_form_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_form
    ADD CONSTRAINT ad_form_pkey PRIMARY KEY (ad_form_id);


--
-- Name: ad_form_trl ad_form_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_form_trl
    ADD CONSTRAINT ad_form_trl_pkey PRIMARY KEY (ad_form_id, ad_language);


--
-- Name: ad_housekeeping ad_housekeeping_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_housekeeping
    ADD CONSTRAINT ad_housekeeping_key PRIMARY KEY (ad_housekeeping_id);


--
-- Name: ad_image ad_image_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_image
    ADD CONSTRAINT ad_image_pkey PRIMARY KEY (ad_image_id);


--
-- Name: ad_impformat ad_impformat_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_impformat
    ADD CONSTRAINT ad_impformat_pkey PRIMARY KEY (ad_impformat_id);


--
-- Name: ad_impformat_row ad_impformat_row_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_impformat_row
    ADD CONSTRAINT ad_impformat_row_pkey PRIMARY KEY (ad_impformat_row_id);


--
-- Name: ad_import ad_import_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_import
    ADD CONSTRAINT ad_import_key PRIMARY KEY (ad_import_id);


--
-- Name: ad_importcolumn ad_importcolumn_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_importcolumn
    ADD CONSTRAINT ad_importcolumn_key PRIMARY KEY (ad_importcolumn_id);


--
-- Name: ad_importvalidation ad_importvalidation_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_importvalidation
    ADD CONSTRAINT ad_importvalidation_key PRIMARY KEY (ad_importvalidation_id);


--
-- Name: ad_infocolumn ad_infocolumn_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_infocolumn
    ADD CONSTRAINT ad_infocolumn_pkey PRIMARY KEY (ad_infocolumn_id);


--
-- Name: ad_infocolumn_trl ad_infocolumn_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_infocolumn_trl
    ADD CONSTRAINT ad_infocolumn_trl_pkey PRIMARY KEY (ad_infocolumn_id, ad_language);


--
-- Name: ad_infoprocess ad_infoprocess_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_infoprocess
    ADD CONSTRAINT ad_infoprocess_key PRIMARY KEY (ad_infoprocess_id);


--
-- Name: ad_infowindow ad_infowindow_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_infowindow
    ADD CONSTRAINT ad_infowindow_pkey PRIMARY KEY (ad_infowindow_id);


--
-- Name: ad_infowindow_trl ad_infowindow_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_infowindow_trl
    ADD CONSTRAINT ad_infowindow_trl_pkey PRIMARY KEY (ad_infowindow_id, ad_language);


--
-- Name: ad_issue ad_issue_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_issue
    ADD CONSTRAINT ad_issue_pkey PRIMARY KEY (ad_issue_id);


--
-- Name: ad_language ad_language_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_language
    ADD CONSTRAINT ad_language_pkey PRIMARY KEY (ad_language);


--
-- Name: ad_memo ad_memo_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_memo
    ADD CONSTRAINT ad_memo_key PRIMARY KEY (ad_memo_id);


--
-- Name: ad_menu ad_menu_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_menu
    ADD CONSTRAINT ad_menu_pkey PRIMARY KEY (ad_menu_id);


--
-- Name: ad_menu_trl ad_menu_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_menu_trl
    ADD CONSTRAINT ad_menu_trl_pkey PRIMARY KEY (ad_menu_id, ad_language);


--
-- Name: ad_message ad_message_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_message
    ADD CONSTRAINT ad_message_pkey PRIMARY KEY (ad_message_id);


--
-- Name: ad_message_trl ad_message_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_message_trl
    ADD CONSTRAINT ad_message_trl_pkey PRIMARY KEY (ad_message_id, ad_language);


--
-- Name: ad_modelvalidator ad_modelvalidator_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_modelvalidator
    ADD CONSTRAINT ad_modelvalidator_key PRIMARY KEY (ad_modelvalidator_id);


--
-- Name: ad_modification ad_modification_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_modification
    ADD CONSTRAINT ad_modification_pkey PRIMARY KEY (ad_modification_id);


--
-- Name: ad_note ad_note_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_note
    ADD CONSTRAINT ad_note_pkey PRIMARY KEY (ad_note_id);


--
-- Name: ad_org ad_org_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_org
    ADD CONSTRAINT ad_org_pkey PRIMARY KEY (ad_org_id);


--
-- Name: ad_orginfo ad_orginfo_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_orginfo
    ADD CONSTRAINT ad_orginfo_pkey PRIMARY KEY (ad_org_id);


--
-- Name: ad_orgtype ad_orgtype_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_orgtype
    ADD CONSTRAINT ad_orgtype_pkey PRIMARY KEY (ad_orgtype_id);


--
-- Name: ad_package_exp_common ad_package_exp_common_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_common
    ADD CONSTRAINT ad_package_exp_common_pkey PRIMARY KEY (ad_package_exp_common_id);


--
-- Name: ad_package_exp_detail ad_package_exp_detail_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_detail
    ADD CONSTRAINT ad_package_exp_detail_pkey PRIMARY KEY (ad_package_exp_detail_id);


--
-- Name: ad_package_exp ad_package_exp_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp
    ADD CONSTRAINT ad_package_exp_pkey PRIMARY KEY (ad_package_exp_id);


--
-- Name: ad_package_imp_backup ad_package_imp_backup_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_imp_backup
    ADD CONSTRAINT ad_package_imp_backup_pkey PRIMARY KEY (ad_package_imp_backup_id);


--
-- Name: ad_package_imp_detail ad_package_imp_detail_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_imp_detail
    ADD CONSTRAINT ad_package_imp_detail_pkey PRIMARY KEY (ad_package_imp_detail_id);


--
-- Name: ad_package_imp_inst ad_package_imp_inst_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_imp_inst
    ADD CONSTRAINT ad_package_imp_inst_pkey PRIMARY KEY (ad_package_imp_inst_id);


--
-- Name: ad_package_imp ad_package_imp_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_imp
    ADD CONSTRAINT ad_package_imp_pkey PRIMARY KEY (ad_package_imp_id);


--
-- Name: ad_package_imp_proc ad_package_imp_proc_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_imp_proc
    ADD CONSTRAINT ad_package_imp_proc_pkey PRIMARY KEY (ad_package_imp_proc_id);


--
-- Name: ad_pinstance_log ad_pinstance_log_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_pinstance_log
    ADD CONSTRAINT ad_pinstance_log_pkey PRIMARY KEY (ad_pinstance_id, log_id);


--
-- Name: ad_pinstance_para ad_pinstance_para_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_pinstance_para
    ADD CONSTRAINT ad_pinstance_para_pkey PRIMARY KEY (ad_pinstance_id, seqno);


--
-- Name: ad_pinstance ad_pinstance_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_pinstance
    ADD CONSTRAINT ad_pinstance_pkey PRIMARY KEY (ad_pinstance_id);


--
-- Name: ad_preference ad_preference_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_preference
    ADD CONSTRAINT ad_preference_pkey PRIMARY KEY (ad_preference_id);


--
-- Name: ad_printcolor ad_printcolor_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printcolor
    ADD CONSTRAINT ad_printcolor_pkey PRIMARY KEY (ad_printcolor_id);


--
-- Name: ad_printconfig ad_printconfig_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printconfig
    ADD CONSTRAINT ad_printconfig_key PRIMARY KEY (ad_printconfig_id);


--
-- Name: ad_printfont ad_printfont_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printfont
    ADD CONSTRAINT ad_printfont_pkey PRIMARY KEY (ad_printfont_id);


--
-- Name: ad_printform ad_printform_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT ad_printform_pkey PRIMARY KEY (ad_printform_id);


--
-- Name: ad_printformat ad_printformat_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformat
    ADD CONSTRAINT ad_printformat_pkey PRIMARY KEY (ad_printformat_id);


--
-- Name: ad_printformatitem ad_printformatitem_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformatitem
    ADD CONSTRAINT ad_printformatitem_pkey PRIMARY KEY (ad_printformatitem_id);


--
-- Name: ad_printformatitem_trl ad_printformatitem_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformatitem_trl
    ADD CONSTRAINT ad_printformatitem_trl_pkey PRIMARY KEY (ad_printformatitem_id, ad_language);


--
-- Name: ad_printgraph ad_printgraph_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printgraph
    ADD CONSTRAINT ad_printgraph_pkey PRIMARY KEY (ad_printgraph_id);


--
-- Name: ad_printpaper ad_printpaper_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printpaper
    ADD CONSTRAINT ad_printpaper_pkey PRIMARY KEY (ad_printpaper_id);


--
-- Name: ad_printtableformat ad_printtableformat_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printtableformat
    ADD CONSTRAINT ad_printtableformat_pkey PRIMARY KEY (ad_printtableformat_id);


--
-- Name: ad_private_access ad_private_access_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_private_access
    ADD CONSTRAINT ad_private_access_pkey PRIMARY KEY (ad_user_id, ad_table_id, record_id);


--
-- Name: ad_process_access ad_process_access_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_access
    ADD CONSTRAINT ad_process_access_pkey PRIMARY KEY (ad_process_id, ad_role_id);


--
-- Name: ad_process_para ad_process_para_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_para
    ADD CONSTRAINT ad_process_para_pkey PRIMARY KEY (ad_process_para_id);


--
-- Name: ad_process_para_trl ad_process_para_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_para_trl
    ADD CONSTRAINT ad_process_para_trl_pkey PRIMARY KEY (ad_process_para_id, ad_language);


--
-- Name: ad_process ad_process_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process
    ADD CONSTRAINT ad_process_pkey PRIMARY KEY (ad_process_id);


--
-- Name: ad_process_trl ad_process_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_trl
    ADD CONSTRAINT ad_process_trl_pkey PRIMARY KEY (ad_process_id, ad_language);


--
-- Name: ad_recentitem ad_recentitem_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_recentitem
    ADD CONSTRAINT ad_recentitem_key PRIMARY KEY (ad_recentitem_id);


--
-- Name: ad_record_access ad_record_access_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_record_access
    ADD CONSTRAINT ad_record_access_pkey PRIMARY KEY (ad_role_id, ad_table_id, record_id);


--
-- Name: ad_ref_list ad_ref_list_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_list
    ADD CONSTRAINT ad_ref_list_pkey PRIMARY KEY (ad_ref_list_id);


--
-- Name: ad_ref_list_trl ad_ref_list_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_list_trl
    ADD CONSTRAINT ad_ref_list_trl_pkey PRIMARY KEY (ad_ref_list_id, ad_language);


--
-- Name: ad_ref_table ad_ref_table_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_table
    ADD CONSTRAINT ad_ref_table_pkey PRIMARY KEY (ad_reference_id);


--
-- Name: ad_reference ad_reference_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_reference
    ADD CONSTRAINT ad_reference_pkey PRIMARY KEY (ad_reference_id);


--
-- Name: ad_reference_trl ad_reference_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_reference_trl
    ADD CONSTRAINT ad_reference_trl_pkey PRIMARY KEY (ad_reference_id, ad_language);


--
-- Name: ad_registration ad_registration_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_registration
    ADD CONSTRAINT ad_registration_pkey PRIMARY KEY (ad_registration_id, ad_client_id, ad_system_id);


--
-- Name: ad_relationtype ad_relationtype_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_relationtype
    ADD CONSTRAINT ad_relationtype_key PRIMARY KEY (ad_relationtype_id);


--
-- Name: ad_reportview_col ad_reportview_col_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_reportview_col
    ADD CONSTRAINT ad_reportview_col_pkey PRIMARY KEY (ad_reportview_col_id);


--
-- Name: ad_reportview ad_reportview_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_reportview
    ADD CONSTRAINT ad_reportview_pkey PRIMARY KEY (ad_reportview_id);


--
-- Name: ad_role_included ad_role_included_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_role_included
    ADD CONSTRAINT ad_role_included_key PRIMARY KEY (ad_role_id, included_role_id);


--
-- Name: ad_role_orgaccess ad_role_orgaccess_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_role_orgaccess
    ADD CONSTRAINT ad_role_orgaccess_pkey PRIMARY KEY (ad_role_id, ad_org_id);


--
-- Name: ad_role ad_role_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_role
    ADD CONSTRAINT ad_role_pkey PRIMARY KEY (ad_role_id);


--
-- Name: ad_rule ad_rule_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_rule
    ADD CONSTRAINT ad_rule_key PRIMARY KEY (ad_rule_id);


--
-- Name: ad_scheduler_para ad_scheduler_para_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_scheduler_para
    ADD CONSTRAINT ad_scheduler_para_pkey PRIMARY KEY (ad_scheduler_id, ad_process_para_id);


--
-- Name: ad_scheduler ad_scheduler_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_scheduler
    ADD CONSTRAINT ad_scheduler_pkey PRIMARY KEY (ad_scheduler_id);


--
-- Name: ad_schedulerlog ad_schedulerlog_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_schedulerlog
    ADD CONSTRAINT ad_schedulerlog_pkey PRIMARY KEY (ad_scheduler_id, ad_schedulerlog_id);


--
-- Name: ad_schedulerrecipient ad_schedulerrecipient_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_schedulerrecipient
    ADD CONSTRAINT ad_schedulerrecipient_pkey PRIMARY KEY (ad_schedulerrecipient_id);


--
-- Name: ad_searchdefinition ad_searchdefinition_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_searchdefinition
    ADD CONSTRAINT ad_searchdefinition_key PRIMARY KEY (ad_searchdefinition_id);


--
-- Name: ad_sequence_audit ad_sequence_audit_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_sequence_audit
    ADD CONSTRAINT ad_sequence_audit_pkey PRIMARY KEY (ad_sequence_id, documentno);


--
-- Name: ad_sequence_no ad_sequence_no_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_sequence_no
    ADD CONSTRAINT ad_sequence_no_pkey PRIMARY KEY (ad_sequence_id, calendaryear);


--
-- Name: ad_sequence ad_sequence_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_sequence
    ADD CONSTRAINT ad_sequence_pkey PRIMARY KEY (ad_sequence_id);


--
-- Name: ad_session ad_session_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_session
    ADD CONSTRAINT ad_session_pkey PRIMARY KEY (ad_session_id);


--
-- Name: ad_sysconfig ad_sysconfig_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_sysconfig
    ADD CONSTRAINT ad_sysconfig_pkey PRIMARY KEY (ad_sysconfig_id);


--
-- Name: ad_system ad_system_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_system
    ADD CONSTRAINT ad_system_pkey PRIMARY KEY (ad_system_id, ad_client_id);


--
-- Name: ad_tab_customization ad_tab_customization_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab_customization
    ADD CONSTRAINT ad_tab_customization_key PRIMARY KEY (ad_tab_customization_id);


--
-- Name: ad_tab ad_tab_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab
    ADD CONSTRAINT ad_tab_pkey PRIMARY KEY (ad_tab_id);


--
-- Name: ad_tab_trl ad_tab_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab_trl
    ADD CONSTRAINT ad_tab_trl_pkey PRIMARY KEY (ad_tab_id, ad_language);


--
-- Name: ad_table_access ad_table_access_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table_access
    ADD CONSTRAINT ad_table_access_pkey PRIMARY KEY (ad_role_id, ad_table_id, accesstyperule);


--
-- Name: ad_table_edi ad_table_edi_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table_edi
    ADD CONSTRAINT ad_table_edi_key PRIMARY KEY (ad_table_edi_id);


--
-- Name: ad_table ad_table_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table
    ADD CONSTRAINT ad_table_pkey PRIMARY KEY (ad_table_id);


--
-- Name: ad_table_scriptvalidator ad_table_scriptvalidator_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table_scriptvalidator
    ADD CONSTRAINT ad_table_scriptvalidator_key PRIMARY KEY (ad_table_scriptvalidator_id);


--
-- Name: ad_table_trl ad_table_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table_trl
    ADD CONSTRAINT ad_table_trl_pkey PRIMARY KEY (ad_table_id, ad_language);


--
-- Name: ad_tableselection_column ad_tableselection_column_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tableselection_column
    ADD CONSTRAINT ad_tableselection_column_key PRIMARY KEY (ad_tableselection_column_id);


--
-- Name: ad_tableselection_column_trl ad_tableselection_column_t_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tableselection_column_trl
    ADD CONSTRAINT ad_tableselection_column_t_key PRIMARY KEY (ad_tableselection_column_id);


--
-- Name: ad_tableselection ad_tableselection_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tableselection
    ADD CONSTRAINT ad_tableselection_key PRIMARY KEY (ad_tableselection_id);


--
-- Name: ad_tree_favorite ad_tree_favorite_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tree_favorite
    ADD CONSTRAINT ad_tree_favorite_key PRIMARY KEY (ad_tree_favorite_id);


--
-- Name: ad_tree_favorite_node ad_tree_favorite_node_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tree_favorite_node
    ADD CONSTRAINT ad_tree_favorite_node_key PRIMARY KEY (ad_tree_favorite_node_id);


--
-- Name: ad_tree ad_tree_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tree
    ADD CONSTRAINT ad_tree_pkey PRIMARY KEY (ad_tree_id);


--
-- Name: ad_treebar ad_treebar_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treebar
    ADD CONSTRAINT ad_treebar_pkey PRIMARY KEY (ad_tree_id, ad_user_id, node_id);


--
-- Name: ad_treenode ad_treenode_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenode
    ADD CONSTRAINT ad_treenode_pkey PRIMARY KEY (ad_tree_id, node_id);


--
-- Name: ad_treenodebp ad_treenodebp_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenodebp
    ADD CONSTRAINT ad_treenodebp_pkey PRIMARY KEY (ad_tree_id, node_id);


--
-- Name: ad_treenodemm ad_treenodemm_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenodemm
    ADD CONSTRAINT ad_treenodemm_pkey PRIMARY KEY (ad_tree_id, node_id);


--
-- Name: ad_treenodepr ad_treenodepr_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenodepr
    ADD CONSTRAINT ad_treenodepr_pkey PRIMARY KEY (ad_tree_id, node_id);


--
-- Name: ad_treenodeu1 ad_treenodeu1_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenodeu1
    ADD CONSTRAINT ad_treenodeu1_pkey PRIMARY KEY (ad_tree_id, node_id);


--
-- Name: ad_treenodeu2 ad_treenodeu2_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenodeu2
    ADD CONSTRAINT ad_treenodeu2_pkey PRIMARY KEY (ad_tree_id, node_id);


--
-- Name: ad_treenodeu3 ad_treenodeu3_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenodeu3
    ADD CONSTRAINT ad_treenodeu3_pkey PRIMARY KEY (ad_tree_id, node_id);


--
-- Name: ad_treenodeu4 ad_treenodeu4_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenodeu4
    ADD CONSTRAINT ad_treenodeu4_pkey PRIMARY KEY (ad_tree_id, node_id);


--
-- Name: ad_uiaction ad_uiaction_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_uiaction
    ADD CONSTRAINT ad_uiaction_key PRIMARY KEY (ad_uiaction_id);


--
-- Name: ad_user_orgaccess ad_user_orgaccess_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user_orgaccess
    ADD CONSTRAINT ad_user_orgaccess_pkey PRIMARY KEY (ad_user_id, ad_org_id);


--
-- Name: ad_user ad_user_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user
    ADD CONSTRAINT ad_user_pkey PRIMARY KEY (ad_user_id);


--
-- Name: ad_user_roles ad_user_roles_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user_roles
    ADD CONSTRAINT ad_user_roles_pkey PRIMARY KEY (ad_user_id, ad_role_id);


--
-- Name: ad_user_substitute ad_user_substitute_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user_substitute
    ADD CONSTRAINT ad_user_substitute_pkey PRIMARY KEY (ad_user_substitute_id);


--
-- Name: ad_userbpaccess ad_userbpaccess_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userbpaccess
    ADD CONSTRAINT ad_userbpaccess_pkey PRIMARY KEY (ad_userbpaccess_id);


--
-- Name: ad_userdef_field ad_userdef_field_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userdef_field
    ADD CONSTRAINT ad_userdef_field_pkey PRIMARY KEY (ad_userdef_field_id);


--
-- Name: ad_userdef_tab ad_userdef_tab_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userdef_tab
    ADD CONSTRAINT ad_userdef_tab_pkey PRIMARY KEY (ad_userdef_tab_id);


--
-- Name: ad_userdef_win ad_userdef_win_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userdef_win
    ADD CONSTRAINT ad_userdef_win_pkey PRIMARY KEY (ad_userdef_win_id);


--
-- Name: ad_usermail ad_usermail_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_usermail
    ADD CONSTRAINT ad_usermail_pkey PRIMARY KEY (ad_usermail_id);


--
-- Name: ad_userquery ad_userquery_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userquery
    ADD CONSTRAINT ad_userquery_pkey PRIMARY KEY (ad_userquery_id);


--
-- Name: ad_val_rule ad_val_rule_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_val_rule
    ADD CONSTRAINT ad_val_rule_pkey PRIMARY KEY (ad_val_rule_id);


--
-- Name: ad_wf_process ad_wf_process_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_wf_process
    ADD CONSTRAINT ad_wf_process_pkey PRIMARY KEY (ad_wf_process_id);


--
-- Name: ad_window_access ad_window_access_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_window_access
    ADD CONSTRAINT ad_window_access_pkey PRIMARY KEY (ad_window_id, ad_role_id);


--
-- Name: ad_window ad_window_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_window
    ADD CONSTRAINT ad_window_pkey PRIMARY KEY (ad_window_id);


--
-- Name: ad_window_trl ad_window_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_window_trl
    ADD CONSTRAINT ad_window_trl_pkey PRIMARY KEY (ad_window_id, ad_language);


--
-- Name: ad_workflow_access ad_workflow_access_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_workflow_access
    ADD CONSTRAINT ad_workflow_access_pkey PRIMARY KEY (ad_workflow_id, ad_role_id);


--
-- Name: ad_workflow ad_workflow_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_workflow
    ADD CONSTRAINT ad_workflow_pkey PRIMARY KEY (ad_workflow_id);


--
-- Name: ad_workflow_trl ad_workflow_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_workflow_trl
    ADD CONSTRAINT ad_workflow_trl_pkey PRIMARY KEY (ad_workflow_id, ad_language);


--
-- Name: ad_zoomcondition ad_zoomcondition_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_zoomcondition
    ADD CONSTRAINT ad_zoomcondition_key PRIMARY KEY (ad_zoomcondition_id);


--
-- Name: ax_seasonality ax_seasonality_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ax_seasonality
    ADD CONSTRAINT ax_seasonality_key PRIMARY KEY (ax_seasonality_id);


--
-- Name: c_acctprocessor c_acctprocessor_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctprocessor
    ADD CONSTRAINT c_acctprocessor_pkey PRIMARY KEY (c_acctprocessor_id);


--
-- Name: c_acctprocessorlog c_acctprocessorlog_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctprocessorlog
    ADD CONSTRAINT c_acctprocessorlog_pkey PRIMARY KEY (c_acctprocessor_id, c_acctprocessorlog_id);


--
-- Name: c_acctschema_default c_acctschema_default_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT c_acctschema_default_pkey PRIMARY KEY (c_acctschema_id);


--
-- Name: c_acctschema_element c_acctschema_element_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_element
    ADD CONSTRAINT c_acctschema_element_pkey PRIMARY KEY (c_acctschema_element_id);


--
-- Name: c_acctschema_gl c_acctschema_gl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_gl
    ADD CONSTRAINT c_acctschema_gl_pkey PRIMARY KEY (c_acctschema_id);


--
-- Name: c_acctschema c_acctschema_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema
    ADD CONSTRAINT c_acctschema_pkey PRIMARY KEY (c_acctschema_id);


--
-- Name: c_activity c_activity_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_activity
    ADD CONSTRAINT c_activity_pkey PRIMARY KEY (c_activity_id);


--
-- Name: c_allocationhdr c_allocationhdr_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_allocationhdr
    ADD CONSTRAINT c_allocationhdr_pkey PRIMARY KEY (c_allocationhdr_id);


--
-- Name: c_allocationline c_allocationline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_allocationline
    ADD CONSTRAINT c_allocationline_pkey PRIMARY KEY (c_allocationline_id);


--
-- Name: c_bank c_bank_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bank
    ADD CONSTRAINT c_bank_pkey PRIMARY KEY (c_bank_id);


--
-- Name: c_bankaccount_acct c_bankaccount_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount_acct
    ADD CONSTRAINT c_bankaccount_acct_pkey PRIMARY KEY (c_bankaccount_id, c_acctschema_id);


--
-- Name: c_bankaccount c_bankaccount_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount
    ADD CONSTRAINT c_bankaccount_pkey PRIMARY KEY (c_bankaccount_id);


--
-- Name: c_bankaccountdoc c_bankaccountdoc_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccountdoc
    ADD CONSTRAINT c_bankaccountdoc_pkey PRIMARY KEY (c_bankaccountdoc_id);


--
-- Name: c_bankstatement c_bankstatement_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankstatement
    ADD CONSTRAINT c_bankstatement_pkey PRIMARY KEY (c_bankstatement_id);


--
-- Name: c_bankstatementline c_bankstatementline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankstatementline
    ADD CONSTRAINT c_bankstatementline_pkey PRIMARY KEY (c_bankstatementline_id);


--
-- Name: c_bankstatementloader c_bankstatementloader_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankstatementloader
    ADD CONSTRAINT c_bankstatementloader_pkey PRIMARY KEY (c_bankstatementloader_id);


--
-- Name: c_bankstatementmatcher c_bankstatementmatcher_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankstatementmatcher
    ADD CONSTRAINT c_bankstatementmatcher_pkey PRIMARY KEY (c_bankstatementmatcher_id);


--
-- Name: c_bp_bankaccount c_bp_bankaccount_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_bankaccount
    ADD CONSTRAINT c_bp_bankaccount_pkey PRIMARY KEY (c_bp_bankaccount_id);


--
-- Name: c_bp_customer_acct c_bp_customer_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_customer_acct
    ADD CONSTRAINT c_bp_customer_acct_pkey PRIMARY KEY (c_bpartner_id, c_acctschema_id);


--
-- Name: c_bp_edi c_bp_edi_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_edi
    ADD CONSTRAINT c_bp_edi_pkey PRIMARY KEY (c_bp_edi_id);


--
-- Name: c_bp_employee_acct c_bp_employee_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_employee_acct
    ADD CONSTRAINT c_bp_employee_acct_pkey PRIMARY KEY (c_bpartner_id, c_acctschema_id);


--
-- Name: c_bp_group_acct c_bp_group_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT c_bp_group_acct_pkey PRIMARY KEY (c_acctschema_id, c_bp_group_id);


--
-- Name: c_bp_group c_bp_group_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group
    ADD CONSTRAINT c_bp_group_pkey PRIMARY KEY (c_bp_group_id);


--
-- Name: c_bp_relation c_bp_relation_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_relation
    ADD CONSTRAINT c_bp_relation_pkey PRIMARY KEY (c_bp_relation_id);


--
-- Name: c_bp_vendor_acct c_bp_vendor_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_vendor_acct
    ADD CONSTRAINT c_bp_vendor_acct_pkey PRIMARY KEY (c_acctschema_id, c_bpartner_id);


--
-- Name: c_bp_withholding c_bp_withholding_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_withholding
    ADD CONSTRAINT c_bp_withholding_pkey PRIMARY KEY (c_bpartner_id, c_withholding_id);


--
-- Name: c_bpartner_edi c_bpartner_edi_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner_edi
    ADD CONSTRAINT c_bpartner_edi_key PRIMARY KEY (c_bpartner_edi_id);


--
-- Name: c_bpartner_location c_bpartner_location_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner_location
    ADD CONSTRAINT c_bpartner_location_pkey PRIMARY KEY (c_bpartner_location_id);


--
-- Name: c_bpartner c_bpartner_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT c_bpartner_pkey PRIMARY KEY (c_bpartner_id);


--
-- Name: c_bpartner_product c_bpartner_product_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner_product
    ADD CONSTRAINT c_bpartner_product_pkey PRIMARY KEY (c_bpartner_id, m_product_id);


--
-- Name: c_calendar c_calendar_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_calendar
    ADD CONSTRAINT c_calendar_pkey PRIMARY KEY (c_calendar_id);


--
-- Name: c_campaign c_campaign_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_campaign
    ADD CONSTRAINT c_campaign_pkey PRIMARY KEY (c_campaign_id);


--
-- Name: c_cash c_cash_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cash
    ADD CONSTRAINT c_cash_pkey PRIMARY KEY (c_cash_id);


--
-- Name: c_cashbook_acct c_cashbook_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashbook_acct
    ADD CONSTRAINT c_cashbook_acct_pkey PRIMARY KEY (c_cashbook_id, c_acctschema_id);


--
-- Name: c_cashbook c_cashbook_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashbook
    ADD CONSTRAINT c_cashbook_pkey PRIMARY KEY (c_cashbook_id);


--
-- Name: c_cashline c_cashline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashline
    ADD CONSTRAINT c_cashline_pkey PRIMARY KEY (c_cashline_id);


--
-- Name: c_cashplan c_cashplan_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplan
    ADD CONSTRAINT c_cashplan_key PRIMARY KEY (c_cashplan_id);


--
-- Name: c_cashplanline c_cashplanline_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplanline
    ADD CONSTRAINT c_cashplanline_key PRIMARY KEY (c_cashplanline_id);


--
-- Name: c_channel c_channel_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_channel
    ADD CONSTRAINT c_channel_pkey PRIMARY KEY (c_channel_id);


--
-- Name: c_charge_acct c_charge_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_charge_acct
    ADD CONSTRAINT c_charge_acct_pkey PRIMARY KEY (c_charge_id, c_acctschema_id);


--
-- Name: c_charge c_charge_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_charge
    ADD CONSTRAINT c_charge_pkey PRIMARY KEY (c_charge_id);


--
-- Name: c_charge_trl c_charge_trl_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_charge_trl
    ADD CONSTRAINT c_charge_trl_key PRIMARY KEY (ad_language, c_charge_id);


--
-- Name: c_chargetype_doctype c_chargetype_doctype_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_chargetype_doctype
    ADD CONSTRAINT c_chargetype_doctype_key PRIMARY KEY (c_chargetype_id, c_doctype_id);


--
-- Name: c_chargetype c_chargetype_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_chargetype
    ADD CONSTRAINT c_chargetype_key PRIMARY KEY (c_chargetype_id);


--
-- Name: c_city c_city_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_city
    ADD CONSTRAINT c_city_pkey PRIMARY KEY (c_city_id);


--
-- Name: c_commission c_commission_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commission
    ADD CONSTRAINT c_commission_pkey PRIMARY KEY (c_commission_id);


--
-- Name: c_commissionamt c_commissionamt_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissionamt
    ADD CONSTRAINT c_commissionamt_pkey PRIMARY KEY (c_commissionamt_id);


--
-- Name: c_commissiondetail c_commissiondetail_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissiondetail
    ADD CONSTRAINT c_commissiondetail_pkey PRIMARY KEY (c_commissiondetail_id);


--
-- Name: c_commissionline c_commissionline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissionline
    ADD CONSTRAINT c_commissionline_pkey PRIMARY KEY (c_commissionline_id);


--
-- Name: c_commissionrun c_commissionrun_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissionrun
    ADD CONSTRAINT c_commissionrun_pkey PRIMARY KEY (c_commissionrun_id);


--
-- Name: c_contactactivity c_contactactivity_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_contactactivity
    ADD CONSTRAINT c_contactactivity_key PRIMARY KEY (c_contactactivity_id);


--
-- Name: c_conversion_rate c_conversion_rate_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_conversion_rate
    ADD CONSTRAINT c_conversion_rate_pkey PRIMARY KEY (c_conversion_rate_id);


--
-- Name: c_conversiontype c_conversiontype_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_conversiontype
    ADD CONSTRAINT c_conversiontype_pkey PRIMARY KEY (c_conversiontype_id);


--
-- Name: c_country c_country_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_country
    ADD CONSTRAINT c_country_pkey PRIMARY KEY (c_country_id);


--
-- Name: c_country_trl c_country_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_country_trl
    ADD CONSTRAINT c_country_trl_pkey PRIMARY KEY (c_country_id, ad_language);


--
-- Name: c_currency_acct c_currency_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_currency_acct
    ADD CONSTRAINT c_currency_acct_pkey PRIMARY KEY (c_acctschema_id, c_currency_id);


--
-- Name: c_currency c_currency_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_currency
    ADD CONSTRAINT c_currency_pkey PRIMARY KEY (c_currency_id);


--
-- Name: c_currency_trl c_currency_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_currency_trl
    ADD CONSTRAINT c_currency_trl_pkey PRIMARY KEY (c_currency_id, ad_language);


--
-- Name: c_cycle c_cycle_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cycle
    ADD CONSTRAINT c_cycle_pkey PRIMARY KEY (c_cycle_id);


--
-- Name: c_cyclephase c_cyclephase_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cyclephase
    ADD CONSTRAINT c_cyclephase_pkey PRIMARY KEY (c_cyclestep_id, c_phase_id);


--
-- Name: c_cyclestep c_cyclestep_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cyclestep
    ADD CONSTRAINT c_cyclestep_pkey PRIMARY KEY (c_cyclestep_id);


--
-- Name: c_doctype c_doctype_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_doctype
    ADD CONSTRAINT c_doctype_pkey PRIMARY KEY (c_doctype_id);


--
-- Name: c_doctype_trl c_doctype_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_doctype_trl
    ADD CONSTRAINT c_doctype_trl_pkey PRIMARY KEY (c_doctype_id, ad_language);


--
-- Name: c_doctypecounter c_doctypecounter_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_doctypecounter
    ADD CONSTRAINT c_doctypecounter_pkey PRIMARY KEY (c_doctypecounter_id);


--
-- Name: c_dunning c_dunning_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunning
    ADD CONSTRAINT c_dunning_pkey PRIMARY KEY (c_dunning_id);


--
-- Name: c_dunninglevel c_dunninglevel_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunninglevel
    ADD CONSTRAINT c_dunninglevel_pkey PRIMARY KEY (c_dunninglevel_id);


--
-- Name: c_dunninglevel_trl c_dunninglevel_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunninglevel_trl
    ADD CONSTRAINT c_dunninglevel_trl_pkey PRIMARY KEY (ad_language, c_dunninglevel_id);


--
-- Name: c_dunningrun c_dunningrun_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrun
    ADD CONSTRAINT c_dunningrun_pkey PRIMARY KEY (c_dunningrun_id);


--
-- Name: c_dunningrunentry c_dunningrunentry_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrunentry
    ADD CONSTRAINT c_dunningrunentry_pkey PRIMARY KEY (c_dunningrunentry_id);


--
-- Name: c_dunningrunline c_dunningrunline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrunline
    ADD CONSTRAINT c_dunningrunline_pkey PRIMARY KEY (c_dunningrunline_id);


--
-- Name: c_edi_ack c_edi_ack_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_edi_ack
    ADD CONSTRAINT c_edi_ack_key PRIMARY KEY (c_edi_ack_id);


--
-- Name: c_edi_ackline c_edi_ackline_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_edi_ackline
    ADD CONSTRAINT c_edi_ackline_key PRIMARY KEY (c_edi_ackline_id);


--
-- Name: c_edi_doctype c_edi_doctype_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_edi_doctype
    ADD CONSTRAINT c_edi_doctype_key PRIMARY KEY (c_edi_doctype_id);


--
-- Name: c_edi_interchange c_edi_interchange_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_edi_interchange
    ADD CONSTRAINT c_edi_interchange_key PRIMARY KEY (c_edi_interchange_id);


--
-- Name: c_edi_message c_edi_message_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_edi_message
    ADD CONSTRAINT c_edi_message_key PRIMARY KEY (c_edi_message_id);


--
-- Name: c_ediformat c_ediformat_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_ediformat
    ADD CONSTRAINT c_ediformat_key PRIMARY KEY (c_ediformat_id);


--
-- Name: c_ediformat_line c_ediformat_line_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_ediformat_line
    ADD CONSTRAINT c_ediformat_line_key PRIMARY KEY (c_ediformat_line_id);


--
-- Name: c_ediformat_lineelement c_ediformat_lineelement_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_ediformat_lineelement
    ADD CONSTRAINT c_ediformat_lineelement_key PRIMARY KEY (c_ediformat_lineelement_id);


--
-- Name: c_ediformat_lineelementcomp c_ediformat_lineelementcom_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_ediformat_lineelementcomp
    ADD CONSTRAINT c_ediformat_lineelementcom_key PRIMARY KEY (c_ediformat_lineelementcomp_id);


--
-- Name: c_ediprocessor c_ediprocessor_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_ediprocessor
    ADD CONSTRAINT c_ediprocessor_key PRIMARY KEY (c_ediprocessor_id);


--
-- Name: c_ediprocessor_parameter c_ediprocessor_parameter_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_ediprocessor_parameter
    ADD CONSTRAINT c_ediprocessor_parameter_key PRIMARY KEY (c_ediprocessor_parameter_id);


--
-- Name: c_ediprocessortype c_ediprocessortype_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_ediprocessortype
    ADD CONSTRAINT c_ediprocessortype_key PRIMARY KEY (c_ediprocessortype_id);


--
-- Name: c_element c_element_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_element
    ADD CONSTRAINT c_element_pkey PRIMARY KEY (c_element_id);


--
-- Name: c_elementvalidation c_elementvalidation_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_elementvalidation
    ADD CONSTRAINT c_elementvalidation_key PRIMARY KEY (c_elementvalidation_id);


--
-- Name: c_elementvalue_fa c_elementvalue_fa_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_elementvalue_fa
    ADD CONSTRAINT c_elementvalue_fa_key PRIMARY KEY (c_elementvalue_fa_id);


--
-- Name: c_elementvalue c_elementvalue_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_elementvalue
    ADD CONSTRAINT c_elementvalue_pkey PRIMARY KEY (c_elementvalue_id);


--
-- Name: c_elementvalue_trl c_elementvalue_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_elementvalue_trl
    ADD CONSTRAINT c_elementvalue_trl_pkey PRIMARY KEY (c_elementvalue_id, ad_language);


--
-- Name: c_greeting c_greeting_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_greeting
    ADD CONSTRAINT c_greeting_pkey PRIMARY KEY (c_greeting_id);


--
-- Name: c_greeting_trl c_greeting_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_greeting_trl
    ADD CONSTRAINT c_greeting_trl_pkey PRIMARY KEY (ad_language, c_greeting_id);


--
-- Name: c_interorg_acct c_interorg_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_interorg_acct
    ADD CONSTRAINT c_interorg_acct_pkey PRIMARY KEY (c_acctschema_id, ad_org_id, ad_orgto_id);


--
-- Name: c_invoice c_invoice_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT c_invoice_pkey PRIMARY KEY (c_invoice_id);


--
-- Name: c_invoicebatch c_invoicebatch_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatch
    ADD CONSTRAINT c_invoicebatch_pkey PRIMARY KEY (c_invoicebatch_id);


--
-- Name: c_invoicebatchline c_invoicebatchline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT c_invoicebatchline_pkey PRIMARY KEY (c_invoicebatchline_id);


--
-- Name: c_invoiceline c_invoiceline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT c_invoiceline_pkey PRIMARY KEY (c_invoiceline_id);


--
-- Name: c_invoicepayschedule c_invoicepayschedule_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicepayschedule
    ADD CONSTRAINT c_invoicepayschedule_pkey PRIMARY KEY (c_invoicepayschedule_id);


--
-- Name: c_invoiceschedule c_invoiceschedule_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceschedule
    ADD CONSTRAINT c_invoiceschedule_pkey PRIMARY KEY (c_invoiceschedule_id);


--
-- Name: c_invoicetax c_invoicetax_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicetax
    ADD CONSTRAINT c_invoicetax_pkey PRIMARY KEY (c_tax_id, c_invoice_id);


--
-- Name: c_job c_job_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_job
    ADD CONSTRAINT c_job_pkey PRIMARY KEY (c_job_id);


--
-- Name: c_jobassignment c_jobassignment_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_jobassignment
    ADD CONSTRAINT c_jobassignment_pkey PRIMARY KEY (c_jobassignment_id);


--
-- Name: c_jobcategory c_jobcategory_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_jobcategory
    ADD CONSTRAINT c_jobcategory_pkey PRIMARY KEY (c_jobcategory_id);


--
-- Name: c_jobremuneration c_jobremuneration_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_jobremuneration
    ADD CONSTRAINT c_jobremuneration_pkey PRIMARY KEY (c_jobremuneration_id);


--
-- Name: c_landedcost c_landedcost_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_landedcost
    ADD CONSTRAINT c_landedcost_pkey PRIMARY KEY (c_landedcost_id);


--
-- Name: c_landedcostallocation c_landedcostallocation_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_landedcostallocation
    ADD CONSTRAINT c_landedcostallocation_pkey PRIMARY KEY (c_landedcostallocation_id);


--
-- Name: c_location c_location_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_location
    ADD CONSTRAINT c_location_pkey PRIMARY KEY (c_location_id);


--
-- Name: c_nonbusinessday c_nonbusinessday_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_nonbusinessday
    ADD CONSTRAINT c_nonbusinessday_pkey PRIMARY KEY (c_nonbusinessday_id);


--
-- Name: c_opportunity c_opportunity_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_opportunity
    ADD CONSTRAINT c_opportunity_key PRIMARY KEY (c_opportunity_id);


--
-- Name: c_order c_order_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT c_order_pkey PRIMARY KEY (c_order_id);


--
-- Name: c_orderline c_orderline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT c_orderline_pkey PRIMARY KEY (c_orderline_id);


--
-- Name: c_orderpayschedule c_orderpayschedule_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderpayschedule
    ADD CONSTRAINT c_orderpayschedule_key PRIMARY KEY (c_orderpayschedule_id);


--
-- Name: c_ordertax c_ordertax_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_ordertax
    ADD CONSTRAINT c_ordertax_pkey PRIMARY KEY (c_order_id, c_tax_id);


--
-- Name: c_orgassignment c_orgassignment_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orgassignment
    ADD CONSTRAINT c_orgassignment_pkey PRIMARY KEY (c_orgassignment_id);


--
-- Name: c_payment c_payment_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT c_payment_pkey PRIMARY KEY (c_payment_id);


--
-- Name: c_paymentallocate c_paymentallocate_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_paymentallocate
    ADD CONSTRAINT c_paymentallocate_pkey PRIMARY KEY (c_paymentallocate_id);


--
-- Name: c_paymentbatch c_paymentbatch_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_paymentbatch
    ADD CONSTRAINT c_paymentbatch_pkey PRIMARY KEY (c_paymentbatch_id);


--
-- Name: c_paymentprocessor c_paymentprocessor_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_paymentprocessor
    ADD CONSTRAINT c_paymentprocessor_pkey PRIMARY KEY (c_paymentprocessor_id);


--
-- Name: c_paymentterm c_paymentterm_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_paymentterm
    ADD CONSTRAINT c_paymentterm_pkey PRIMARY KEY (c_paymentterm_id);


--
-- Name: c_paymentterm_trl c_paymentterm_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_paymentterm_trl
    ADD CONSTRAINT c_paymentterm_trl_pkey PRIMARY KEY (c_paymentterm_id, ad_language);


--
-- Name: c_payschedule c_payschedule_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payschedule
    ADD CONSTRAINT c_payschedule_pkey PRIMARY KEY (c_payschedule_id);


--
-- Name: c_payselection c_payselection_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payselection
    ADD CONSTRAINT c_payselection_pkey PRIMARY KEY (c_payselection_id);


--
-- Name: c_payselectioncheck c_payselectioncheck_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payselectioncheck
    ADD CONSTRAINT c_payselectioncheck_pkey PRIMARY KEY (c_payselectioncheck_id);


--
-- Name: c_payselectionline c_payselectionline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payselectionline
    ADD CONSTRAINT c_payselectionline_pkey PRIMARY KEY (c_payselectionline_id);


--
-- Name: c_period c_period_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_period
    ADD CONSTRAINT c_period_pkey PRIMARY KEY (c_period_id);


--
-- Name: c_periodcontrol c_periodcontrol_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_periodcontrol
    ADD CONSTRAINT c_periodcontrol_pkey PRIMARY KEY (c_periodcontrol_id);


--
-- Name: c_phase c_phase_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_phase
    ADD CONSTRAINT c_phase_pkey PRIMARY KEY (c_phase_id);


--
-- Name: c_project_acct c_project_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project_acct
    ADD CONSTRAINT c_project_acct_pkey PRIMARY KEY (c_project_id, c_acctschema_id);


--
-- Name: c_project c_project_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project
    ADD CONSTRAINT c_project_pkey PRIMARY KEY (c_project_id);


--
-- Name: c_projectissue c_projectissue_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectissue
    ADD CONSTRAINT c_projectissue_pkey PRIMARY KEY (c_projectissue_id);


--
-- Name: c_projectissuema c_projectissuema_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectissuema
    ADD CONSTRAINT c_projectissuema_pkey PRIMARY KEY (c_projectissue_id, m_attributesetinstance_id);


--
-- Name: c_projectline c_projectline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectline
    ADD CONSTRAINT c_projectline_pkey PRIMARY KEY (c_projectline_id);


--
-- Name: c_projectphase c_projectphase_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectphase
    ADD CONSTRAINT c_projectphase_pkey PRIMARY KEY (c_projectphase_id);


--
-- Name: c_projectresource c_projectresource_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectresource
    ADD CONSTRAINT c_projectresource_key PRIMARY KEY (c_projectresource_id);


--
-- Name: c_projecttask c_projecttask_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projecttask
    ADD CONSTRAINT c_projecttask_pkey PRIMARY KEY (c_projecttask_id);


--
-- Name: c_projecttype c_projecttype_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projecttype
    ADD CONSTRAINT c_projecttype_pkey PRIMARY KEY (c_projecttype_id);


--
-- Name: c_recurring_document c_recurring_document_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_recurring_document
    ADD CONSTRAINT c_recurring_document_key PRIMARY KEY (c_recurring_document_id);


--
-- Name: c_recurring c_recurring_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_recurring
    ADD CONSTRAINT c_recurring_pkey PRIMARY KEY (c_recurring_id);


--
-- Name: c_recurring_run c_recurring_run_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_recurring_run
    ADD CONSTRAINT c_recurring_run_pkey PRIMARY KEY (c_recurring_run_id);


--
-- Name: c_region c_region_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_region
    ADD CONSTRAINT c_region_pkey PRIMARY KEY (c_region_id);


--
-- Name: c_remuneration c_remuneration_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_remuneration
    ADD CONSTRAINT c_remuneration_pkey PRIMARY KEY (c_remuneration_id);


--
-- Name: c_revenuerecog_service c_revenuerecog_service_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_revenuerecog_service
    ADD CONSTRAINT c_revenuerecog_service_key PRIMARY KEY (c_revenuerecog_service_id);


--
-- Name: c_revenuerecognition c_revenuerecognition_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_revenuerecognition
    ADD CONSTRAINT c_revenuerecognition_pkey PRIMARY KEY (c_revenuerecognition_id);


--
-- Name: c_revenuerecognition_plan c_revenuerecognition_plan_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_revenuerecognition_plan
    ADD CONSTRAINT c_revenuerecognition_plan_pkey PRIMARY KEY (c_revenuerecognition_plan_id);


--
-- Name: c_revenuerecognition_run c_revenuerecognition_run_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_revenuerecognition_run
    ADD CONSTRAINT c_revenuerecognition_run_pkey PRIMARY KEY (c_revenuerecognition_run_id);


--
-- Name: c_revenuerecognition_service c_revenuerecognition_servi_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_revenuerecognition_service
    ADD CONSTRAINT c_revenuerecognition_servi_key PRIMARY KEY (c_revenuerecognition_service_i);


--
-- Name: c_rfq c_rfq_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq
    ADD CONSTRAINT c_rfq_pkey PRIMARY KEY (c_rfq_id);


--
-- Name: c_rfq_topic c_rfq_topic_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq_topic
    ADD CONSTRAINT c_rfq_topic_pkey PRIMARY KEY (c_rfq_topic_id);


--
-- Name: c_rfq_topicsubscriber c_rfq_topicsubscriber_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq_topicsubscriber
    ADD CONSTRAINT c_rfq_topicsubscriber_pkey PRIMARY KEY (c_rfq_topicsubscriber_id);


--
-- Name: c_rfq_topicsubscriberonly c_rfq_topicsubscriberonly_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq_topicsubscriberonly
    ADD CONSTRAINT c_rfq_topicsubscriberonly_pkey PRIMARY KEY (c_rfq_topicsubscriberonly_id);


--
-- Name: c_rfqline c_rfqline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqline
    ADD CONSTRAINT c_rfqline_pkey PRIMARY KEY (c_rfqline_id);


--
-- Name: c_rfqlineqty c_rfqlineqty_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqlineqty
    ADD CONSTRAINT c_rfqlineqty_pkey PRIMARY KEY (c_rfqlineqty_id);


--
-- Name: c_rfqresponse c_rfqresponse_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqresponse
    ADD CONSTRAINT c_rfqresponse_pkey PRIMARY KEY (c_rfqresponse_id);


--
-- Name: c_rfqresponseline c_rfqresponseline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqresponseline
    ADD CONSTRAINT c_rfqresponseline_pkey PRIMARY KEY (c_rfqresponseline_id);


--
-- Name: c_rfqresponselineqty c_rfqresponselineqty_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqresponselineqty
    ADD CONSTRAINT c_rfqresponselineqty_pkey PRIMARY KEY (c_rfqresponselineqty_id);


--
-- Name: c_salesregion c_salesregion_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_salesregion
    ADD CONSTRAINT c_salesregion_pkey PRIMARY KEY (c_salesregion_id);


--
-- Name: c_salesstage c_salesstage_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_salesstage
    ADD CONSTRAINT c_salesstage_key PRIMARY KEY (c_salesstage_id);


--
-- Name: c_servicelevel c_servicelevel_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_servicelevel
    ADD CONSTRAINT c_servicelevel_pkey PRIMARY KEY (c_servicelevel_id);


--
-- Name: c_servicelevelline c_servicelevelline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_servicelevelline
    ADD CONSTRAINT c_servicelevelline_pkey PRIMARY KEY (c_servicelevelline_id);


--
-- Name: c_subacct c_subacct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_subacct
    ADD CONSTRAINT c_subacct_pkey PRIMARY KEY (c_subacct_id);


--
-- Name: c_subscription_delivery c_subscription_delivery_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_subscription_delivery
    ADD CONSTRAINT c_subscription_delivery_pkey PRIMARY KEY (c_subscription_delivery_id);


--
-- Name: c_subscription c_subscription_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_subscription
    ADD CONSTRAINT c_subscription_pkey PRIMARY KEY (c_subscription_id);


--
-- Name: c_subscriptiontype c_subscriptiontype_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_subscriptiontype
    ADD CONSTRAINT c_subscriptiontype_pkey PRIMARY KEY (c_subscriptiontype_id);


--
-- Name: c_task c_task_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_task
    ADD CONSTRAINT c_task_pkey PRIMARY KEY (c_task_id);


--
-- Name: c_tax_acct c_tax_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax_acct
    ADD CONSTRAINT c_tax_acct_pkey PRIMARY KEY (c_tax_id, c_acctschema_id);


--
-- Name: c_tax c_tax_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax
    ADD CONSTRAINT c_tax_pkey PRIMARY KEY (c_tax_id);


--
-- Name: c_tax_trl c_tax_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax_trl
    ADD CONSTRAINT c_tax_trl_pkey PRIMARY KEY (c_tax_id, ad_language);


--
-- Name: c_taxbase c_taxbase_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxbase
    ADD CONSTRAINT c_taxbase_key PRIMARY KEY (c_taxbase_id);


--
-- Name: c_taxcategory c_taxcategory_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxcategory
    ADD CONSTRAINT c_taxcategory_pkey PRIMARY KEY (c_taxcategory_id);


--
-- Name: c_taxcategory_trl c_taxcategory_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxcategory_trl
    ADD CONSTRAINT c_taxcategory_trl_pkey PRIMARY KEY (c_taxcategory_id, ad_language);


--
-- Name: c_taxdeclaration c_taxdeclaration_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdeclaration
    ADD CONSTRAINT c_taxdeclaration_pkey PRIMARY KEY (c_taxdeclaration_id);


--
-- Name: c_taxdeclarationacct c_taxdeclarationacct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdeclarationacct
    ADD CONSTRAINT c_taxdeclarationacct_pkey PRIMARY KEY (c_taxdeclarationacct_id);


--
-- Name: c_taxdeclarationline c_taxdeclarationline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdeclarationline
    ADD CONSTRAINT c_taxdeclarationline_pkey PRIMARY KEY (c_taxdeclarationline_id);


--
-- Name: c_taxdefinition c_taxdefinition_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdefinition
    ADD CONSTRAINT c_taxdefinition_key PRIMARY KEY (c_taxdefinition_id);


--
-- Name: c_taxgroup c_taxgroup_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxgroup
    ADD CONSTRAINT c_taxgroup_key PRIMARY KEY (c_taxgroup_id);


--
-- Name: c_taxpostal c_taxpostal_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxpostal
    ADD CONSTRAINT c_taxpostal_pkey PRIMARY KEY (c_taxpostal_id);


--
-- Name: c_taxtype c_taxtype_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxtype
    ADD CONSTRAINT c_taxtype_key PRIMARY KEY (c_taxtype_id);


--
-- Name: c_uom_conversion c_uom_conversion_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_uom_conversion
    ADD CONSTRAINT c_uom_conversion_pkey PRIMARY KEY (c_uom_conversion_id);


--
-- Name: c_uom c_uom_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_uom
    ADD CONSTRAINT c_uom_pkey PRIMARY KEY (c_uom_id);


--
-- Name: c_uom_trl c_uom_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_uom_trl
    ADD CONSTRAINT c_uom_trl_pkey PRIMARY KEY (c_uom_id, ad_language);


--
-- Name: c_userremuneration c_userremuneration_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_userremuneration
    ADD CONSTRAINT c_userremuneration_pkey PRIMARY KEY (c_userremuneration_id);


--
-- Name: c_validcombination c_validcombination_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT c_validcombination_pkey PRIMARY KEY (c_validcombination_id);


--
-- Name: c_withholding_acct c_withholding_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_withholding_acct
    ADD CONSTRAINT c_withholding_acct_pkey PRIMARY KEY (c_withholding_id, c_acctschema_id);


--
-- Name: c_withholding c_withholding_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_withholding
    ADD CONSTRAINT c_withholding_pkey PRIMARY KEY (c_withholding_id);


--
-- Name: c_year c_year_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_year
    ADD CONSTRAINT c_year_pkey PRIMARY KEY (c_year_id);


--
-- Name: c_ordersource cordersource_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_ordersource
    ADD CONSTRAINT cordersource_key PRIMARY KEY (c_ordersource_id);


--
-- Name: deps_saved_ddl deps_saved_ddl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.deps_saved_ddl
    ADD CONSTRAINT deps_saved_ddl_pkey PRIMARY KEY (deps_id);


--
-- Name: etl_setup etl_setup_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.etl_setup
    ADD CONSTRAINT etl_setup_key PRIMARY KEY (etl_setup_id);


--
-- Name: etl_setupline etl_setupline_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.etl_setupline
    ADD CONSTRAINT etl_setupline_key PRIMARY KEY (etl_setupline_id);


--
-- Name: fact_acct fact_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT fact_acct_pkey PRIMARY KEY (fact_acct_id);


--
-- Name: fact_reconciliation fact_reconciliation_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_reconciliation
    ADD CONSTRAINT fact_reconciliation_key PRIMARY KEY (fact_reconciliation_id);


--
-- Name: gaas_asset_group_acct gaas_asset_group_acct_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gaas_asset_group_acct
    ADD CONSTRAINT gaas_asset_group_acct_key PRIMARY KEY (gaas_asset_group_acct_id);


--
-- Name: gaas_assetjournal gaas_assetjournal_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gaas_assetjournal
    ADD CONSTRAINT gaas_assetjournal_key PRIMARY KEY (gaas_assetjournal_id);


--
-- Name: gaas_assetjournalline_det gaas_assetjournalline_det_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gaas_assetjournalline_det
    ADD CONSTRAINT gaas_assetjournalline_det_key PRIMARY KEY (gaas_assetjournalline_det_id);


--
-- Name: gaas_assetjournalline gaas_assetjournalline_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gaas_assetjournalline
    ADD CONSTRAINT gaas_assetjournalline_key PRIMARY KEY (gaas_assetjournalline_id);


--
-- Name: gaas_depreciation_expense gaas_depreciation_expense_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gaas_depreciation_expense
    ADD CONSTRAINT gaas_depreciation_expense_key PRIMARY KEY (gaas_depreciation_expense_id);


--
-- Name: gaas_depreciation_workfile gaas_depreciation_workfile_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gaas_depreciation_workfile
    ADD CONSTRAINT gaas_depreciation_workfile_key PRIMARY KEY (gaas_depreciation_workfile_id);


--
-- Name: gl_budget gl_budget_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_budget
    ADD CONSTRAINT gl_budget_pkey PRIMARY KEY (gl_budget_id);


--
-- Name: gl_budgetcontrol gl_budgetcontrol_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_budgetcontrol
    ADD CONSTRAINT gl_budgetcontrol_pkey PRIMARY KEY (gl_budgetcontrol_id);


--
-- Name: gl_category gl_category_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_category
    ADD CONSTRAINT gl_category_pkey PRIMARY KEY (gl_category_id);


--
-- Name: gl_distribution gl_distribution_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT gl_distribution_pkey PRIMARY KEY (gl_distribution_id);


--
-- Name: gl_distributionline gl_distributionline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT gl_distributionline_pkey PRIMARY KEY (gl_distributionline_id);


--
-- Name: gl_fund gl_fund_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_fund
    ADD CONSTRAINT gl_fund_pkey PRIMARY KEY (gl_fund_id);


--
-- Name: gl_fundrestriction gl_fundrestriction_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_fundrestriction
    ADD CONSTRAINT gl_fundrestriction_pkey PRIMARY KEY (gl_fundrestriction_id);


--
-- Name: gl_journal gl_journal_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journal
    ADD CONSTRAINT gl_journal_pkey PRIMARY KEY (gl_journal_id);


--
-- Name: gl_journalbatch gl_journalbatch_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalbatch
    ADD CONSTRAINT gl_journalbatch_pkey PRIMARY KEY (gl_journalbatch_id);


--
-- Name: gl_journalline gl_journalline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalline
    ADD CONSTRAINT gl_journalline_pkey PRIMARY KEY (gl_journalline_id);


--
-- Name: i_asset i_asset_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_asset
    ADD CONSTRAINT i_asset_key PRIMARY KEY (i_asset_id);


--
-- Name: i_attributesetinstance i_attributesetinstance_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_attributesetinstance
    ADD CONSTRAINT i_attributesetinstance_key PRIMARY KEY (i_attributesetinstance_id);


--
-- Name: i_bankstatement i_bankstatement_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bankstatement
    ADD CONSTRAINT i_bankstatement_pkey PRIMARY KEY (i_bankstatement_id);


--
-- Name: i_bpartner i_bpartner_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bpartner
    ADD CONSTRAINT i_bpartner_pkey PRIMARY KEY (i_bpartner_id);


--
-- Name: i_budget i_budget_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_budget
    ADD CONSTRAINT i_budget_key PRIMARY KEY (i_budget_id);


--
-- Name: i_column i_column_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_column
    ADD CONSTRAINT i_column_key PRIMARY KEY (i_column_id);


--
-- Name: i_conversion_rate i_conversion_rate_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_conversion_rate
    ADD CONSTRAINT i_conversion_rate_pkey PRIMARY KEY (i_conversion_rate_id);


--
-- Name: i_elementvalue i_elementvalue_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_elementvalue
    ADD CONSTRAINT i_elementvalue_pkey PRIMARY KEY (i_elementvalue_id);


--
-- Name: i_fajournal i_fajournal_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT i_fajournal_key PRIMARY KEY (i_fajournal_id);


--
-- Name: i_freightregion i_freightregion_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_freightregion
    ADD CONSTRAINT i_freightregion_key PRIMARY KEY (i_freightregion_id);


--
-- Name: i_gaas_asset i_gaas_asset_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gaas_asset
    ADD CONSTRAINT i_gaas_asset_key PRIMARY KEY (i_gaas_asset_id);


--
-- Name: i_gljournal i_gljournal_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT i_gljournal_pkey PRIMARY KEY (i_gljournal_id);


--
-- Name: i_inoutlineconfirm i_inoutlineconfirm_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_inoutlineconfirm
    ADD CONSTRAINT i_inoutlineconfirm_pkey PRIMARY KEY (i_inoutlineconfirm_id);


--
-- Name: i_inventory i_inventory_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_inventory
    ADD CONSTRAINT i_inventory_pkey PRIMARY KEY (i_inventory_id);


--
-- Name: i_invoice i_invoice_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT i_invoice_pkey PRIMARY KEY (i_invoice_id);


--
-- Name: i_order i_order_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT i_order_pkey PRIMARY KEY (i_order_id);


--
-- Name: i_payment i_payment_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_payment
    ADD CONSTRAINT i_payment_pkey PRIMARY KEY (i_payment_id);


--
-- Name: i_pricelist i_pricelist_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_pricelist
    ADD CONSTRAINT i_pricelist_key PRIMARY KEY (i_pricelist_id);


--
-- Name: i_product_bom i_product_bom_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_product_bom
    ADD CONSTRAINT i_product_bom_key PRIMARY KEY (i_product_bom_id);


--
-- Name: i_product i_product_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_product
    ADD CONSTRAINT i_product_pkey PRIMARY KEY (i_product_id);


--
-- Name: i_reportline i_reportline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_reportline
    ADD CONSTRAINT i_reportline_pkey PRIMARY KEY (i_reportline_id);


--
-- Name: i_user i_user_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_user
    ADD CONSTRAINT i_user_key PRIMARY KEY (i_user_id);


--
-- Name: m_attribute m_attribute_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attribute
    ADD CONSTRAINT m_attribute_pkey PRIMARY KEY (m_attribute_id);


--
-- Name: m_attributeinstance m_attributeinstance_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributeinstance
    ADD CONSTRAINT m_attributeinstance_pkey PRIMARY KEY (m_attributesetinstance_id, m_attribute_id);


--
-- Name: m_attributesearch m_attributesearch_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributesearch
    ADD CONSTRAINT m_attributesearch_pkey PRIMARY KEY (m_attributesearch_id);


--
-- Name: m_attributeset m_attributeset_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributeset
    ADD CONSTRAINT m_attributeset_pkey PRIMARY KEY (m_attributeset_id);


--
-- Name: m_attributesetexclude m_attributesetexclude_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributesetexclude
    ADD CONSTRAINT m_attributesetexclude_pkey PRIMARY KEY (m_attributesetexclude_id);


--
-- Name: m_attributesetinstance m_attributesetinstance_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributesetinstance
    ADD CONSTRAINT m_attributesetinstance_pkey PRIMARY KEY (m_attributesetinstance_id);


--
-- Name: m_attributeuse m_attributeuse_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributeuse
    ADD CONSTRAINT m_attributeuse_pkey PRIMARY KEY (m_attribute_id, m_attributeset_id);


--
-- Name: m_attributevalue m_attributevalue_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributevalue
    ADD CONSTRAINT m_attributevalue_pkey PRIMARY KEY (m_attributevalue_id);


--
-- Name: m_bom m_bom_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_bom
    ADD CONSTRAINT m_bom_pkey PRIMARY KEY (m_bom_id);


--
-- Name: m_bomalternative m_bomalternative_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_bomalternative
    ADD CONSTRAINT m_bomalternative_pkey PRIMARY KEY (m_bomalternative_id);


--
-- Name: m_bomproduct m_bomproduct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_bomproduct
    ADD CONSTRAINT m_bomproduct_pkey PRIMARY KEY (m_bomproduct_id);


--
-- Name: m_bp_price m_bp_price_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_bp_price
    ADD CONSTRAINT m_bp_price_key PRIMARY KEY (m_bp_price_id);


--
-- Name: m_changenotice m_changenotice_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_changenotice
    ADD CONSTRAINT m_changenotice_pkey PRIMARY KEY (m_changenotice_id);


--
-- Name: m_changerequest m_changerequest_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_changerequest
    ADD CONSTRAINT m_changerequest_pkey PRIMARY KEY (m_changerequest_id);


--
-- Name: m_cost m_cost_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_cost
    ADD CONSTRAINT m_cost_pkey PRIMARY KEY (ad_client_id, ad_org_id, m_product_id, m_costtype_id, c_acctschema_id, m_costelement_id, m_attributesetinstance_id);


--
-- Name: m_costelement m_costelement_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_costelement
    ADD CONSTRAINT m_costelement_pkey PRIMARY KEY (m_costelement_id);


--
-- Name: m_costtype m_costtype_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_costtype
    ADD CONSTRAINT m_costtype_pkey PRIMARY KEY (m_costtype_id);


--
-- Name: m_demand m_demand_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_demand
    ADD CONSTRAINT m_demand_pkey PRIMARY KEY (m_demand_id);


--
-- Name: m_demanddetail m_demanddetail_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_demanddetail
    ADD CONSTRAINT m_demanddetail_pkey PRIMARY KEY (m_demanddetail_id);


--
-- Name: m_demandline m_demandline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_demandline
    ADD CONSTRAINT m_demandline_pkey PRIMARY KEY (m_demandline_id);


--
-- Name: m_discountschema m_discountschema_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_discountschema
    ADD CONSTRAINT m_discountschema_pkey PRIMARY KEY (m_discountschema_id);


--
-- Name: m_discountschemabreak m_discountschemabreak_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_discountschemabreak
    ADD CONSTRAINT m_discountschemabreak_pkey PRIMARY KEY (m_discountschemabreak_id);


--
-- Name: m_discountschemaline m_discountschemaline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_discountschemaline
    ADD CONSTRAINT m_discountschemaline_pkey PRIMARY KEY (m_discountschemaline_id);


--
-- Name: m_distributionlist m_distributionlist_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_distributionlist
    ADD CONSTRAINT m_distributionlist_pkey PRIMARY KEY (m_distributionlist_id);


--
-- Name: m_distributionlistline m_distributionlistline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_distributionlistline
    ADD CONSTRAINT m_distributionlistline_pkey PRIMARY KEY (m_distributionlistline_id);


--
-- Name: m_distributionrun m_distributionrun_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_distributionrun
    ADD CONSTRAINT m_distributionrun_pkey PRIMARY KEY (m_distributionrun_id);


--
-- Name: m_distributionrunline m_distributionrunline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_distributionrunline
    ADD CONSTRAINT m_distributionrunline_pkey PRIMARY KEY (m_distributionrunline_id);


--
-- Name: m_edi_info m_edi_info_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_edi_info
    ADD CONSTRAINT m_edi_info_key PRIMARY KEY (m_edi_info_id);


--
-- Name: m_edi m_edi_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_edi
    ADD CONSTRAINT m_edi_key PRIMARY KEY (m_edi_id);


--
-- Name: m_forecast m_forecast_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_forecast
    ADD CONSTRAINT m_forecast_pkey PRIMARY KEY (m_forecast_id);


--
-- Name: m_forecastline m_forecastline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_forecastline
    ADD CONSTRAINT m_forecastline_pkey PRIMARY KEY (m_forecastline_id);


--
-- Name: m_freight m_freight_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_freight
    ADD CONSTRAINT m_freight_pkey PRIMARY KEY (m_freight_id);


--
-- Name: m_freightcategory m_freightcat_value; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_freightcategory
    ADD CONSTRAINT m_freightcat_value UNIQUE (ad_client_id, value);


--
-- Name: m_freightcategory m_freightcategory_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_freightcategory
    ADD CONSTRAINT m_freightcategory_pkey PRIMARY KEY (m_freightcategory_id);


--
-- Name: m_freightcharge m_freightcharge_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_freightcharge
    ADD CONSTRAINT m_freightcharge_key PRIMARY KEY (m_freightcharge_id);


--
-- Name: m_inout_manifest m_inout_manifest_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout_manifest
    ADD CONSTRAINT m_inout_manifest_key PRIMARY KEY (m_inout_manifest_id);


--
-- Name: m_inout_manifestline m_inout_manifestline_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout_manifestline
    ADD CONSTRAINT m_inout_manifestline_key PRIMARY KEY (m_inout_manifestline_id);


--
-- Name: m_inout m_inout_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT m_inout_pkey PRIMARY KEY (m_inout_id);


--
-- Name: m_inoutconfirm m_inoutconfirm_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutconfirm
    ADD CONSTRAINT m_inoutconfirm_pkey PRIMARY KEY (m_inoutconfirm_id);


--
-- Name: m_inoutline m_inoutline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT m_inoutline_pkey PRIMARY KEY (m_inoutline_id);


--
-- Name: m_inoutlineconfirm m_inoutlineconfirm_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutlineconfirm
    ADD CONSTRAINT m_inoutlineconfirm_pkey PRIMARY KEY (m_inoutlineconfirm_id);


--
-- Name: m_inoutlinema m_inoutlinema_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutlinema
    ADD CONSTRAINT m_inoutlinema_pkey PRIMARY KEY (m_inoutline_id, m_attributesetinstance_id);


--
-- Name: m_inventory m_inventory_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventory
    ADD CONSTRAINT m_inventory_pkey PRIMARY KEY (m_inventory_id);


--
-- Name: m_inventoryline m_inventoryline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventoryline
    ADD CONSTRAINT m_inventoryline_pkey PRIMARY KEY (m_inventoryline_id);


--
-- Name: m_inventorylinema m_inventorylinema_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventorylinema
    ADD CONSTRAINT m_inventorylinema_pkey PRIMARY KEY (m_inventoryline_id, m_attributesetinstance_id);


--
-- Name: m_locator m_locator_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_locator
    ADD CONSTRAINT m_locator_pkey PRIMARY KEY (m_locator_id);


--
-- Name: m_lot m_lot_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_lot
    ADD CONSTRAINT m_lot_pkey PRIMARY KEY (m_lot_id);


--
-- Name: m_lotctl m_lotctl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_lotctl
    ADD CONSTRAINT m_lotctl_pkey PRIMARY KEY (m_lotctl_id);


--
-- Name: m_lotctlexclude m_lotctlexclude_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_lotctlexclude
    ADD CONSTRAINT m_lotctlexclude_pkey PRIMARY KEY (m_lotctlexclude_id);


--
-- Name: m_matchinv m_matchinv_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_matchinv
    ADD CONSTRAINT m_matchinv_pkey PRIMARY KEY (m_matchinv_id);


--
-- Name: m_matchpo m_matchpo_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_matchpo
    ADD CONSTRAINT m_matchpo_pkey PRIMARY KEY (m_matchpo_id);


--
-- Name: m_movement m_movement_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT m_movement_pkey PRIMARY KEY (m_movement_id);


--
-- Name: m_movementconfirm m_movementconfirm_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementconfirm
    ADD CONSTRAINT m_movementconfirm_pkey PRIMARY KEY (m_movementconfirm_id);


--
-- Name: m_movementline m_movementline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementline
    ADD CONSTRAINT m_movementline_pkey PRIMARY KEY (m_movementline_id);


--
-- Name: m_movementlineconfirm m_movementlineconfirm_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementlineconfirm
    ADD CONSTRAINT m_movementlineconfirm_pkey PRIMARY KEY (m_movementlineconfirm_id);


--
-- Name: m_movementlinema m_movementlinema_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementlinema
    ADD CONSTRAINT m_movementlinema_pkey PRIMARY KEY (m_movementline_id, m_attributesetinstance_id);


--
-- Name: m_operationresource m_operationresource_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_operationresource
    ADD CONSTRAINT m_operationresource_pkey PRIMARY KEY (m_operationresource_id);


--
-- Name: m_package m_package_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_package
    ADD CONSTRAINT m_package_pkey PRIMARY KEY (m_package_id);


--
-- Name: m_packageline m_packageline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_packageline
    ADD CONSTRAINT m_packageline_pkey PRIMARY KEY (m_packageline_id);


--
-- Name: m_parttype m_parttype_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_parttype
    ADD CONSTRAINT m_parttype_key PRIMARY KEY (m_parttype_id);


--
-- Name: m_pbatch_line m_pbatch_line_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_pbatch_line
    ADD CONSTRAINT m_pbatch_line_key PRIMARY KEY (m_pbatch_line_id);


--
-- Name: m_perpetualinv m_perpetualinv_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_perpetualinv
    ADD CONSTRAINT m_perpetualinv_pkey PRIMARY KEY (m_perpetualinv_id);


--
-- Name: m_pricelist m_pricelist_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_pricelist
    ADD CONSTRAINT m_pricelist_pkey PRIMARY KEY (m_pricelist_id);


--
-- Name: m_pricelist_version m_pricelist_version_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_pricelist_version
    ADD CONSTRAINT m_pricelist_version_pkey PRIMARY KEY (m_pricelist_version_id);


--
-- Name: m_product_acct m_product_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT m_product_acct_pkey PRIMARY KEY (m_product_id, c_acctschema_id);


--
-- Name: m_product_bom m_product_bom_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_bom
    ADD CONSTRAINT m_product_bom_pkey PRIMARY KEY (m_product_bom_id);


--
-- Name: m_product_category_acct m_product_category_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT m_product_category_acct_pkey PRIMARY KEY (m_product_category_id, c_acctschema_id);


--
-- Name: m_product_category m_product_category_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category
    ADD CONSTRAINT m_product_category_pkey PRIMARY KEY (m_product_category_id);


--
-- Name: m_product_costing m_product_costing_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_costing
    ADD CONSTRAINT m_product_costing_pkey PRIMARY KEY (m_product_id, c_acctschema_id);


--
-- Name: m_product_document m_product_document_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_document
    ADD CONSTRAINT m_product_document_key PRIMARY KEY (m_product_document_id);


--
-- Name: m_product m_product_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT m_product_pkey PRIMARY KEY (m_product_id);


--
-- Name: m_product_po m_product_po_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_po
    ADD CONSTRAINT m_product_po_pkey PRIMARY KEY (m_product_id, c_bpartner_id);


--
-- Name: m_product_qualitytest m_product_qualitytest_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_qualitytest
    ADD CONSTRAINT m_product_qualitytest_key PRIMARY KEY (m_product_qualitytest_id);


--
-- Name: m_product_trl m_product_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_trl
    ADD CONSTRAINT m_product_trl_pkey PRIMARY KEY (m_product_id, ad_language);


--
-- Name: m_productdownload m_productdownload_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productdownload
    ADD CONSTRAINT m_productdownload_pkey PRIMARY KEY (m_productdownload_id);


--
-- Name: m_production_batch m_production_batch_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_production_batch
    ADD CONSTRAINT m_production_batch_key PRIMARY KEY (m_production_batch_id);


--
-- Name: m_production m_production_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_production
    ADD CONSTRAINT m_production_pkey PRIMARY KEY (m_production_id);


--
-- Name: m_productionline m_productionline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productionline
    ADD CONSTRAINT m_productionline_pkey PRIMARY KEY (m_productionline_id);


--
-- Name: m_productionlinema m_productionlinema_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productionlinema
    ADD CONSTRAINT m_productionlinema_pkey PRIMARY KEY (m_productionline_id, m_attributesetinstance_id);


--
-- Name: m_productionplan m_productionplan_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productionplan
    ADD CONSTRAINT m_productionplan_pkey PRIMARY KEY (m_productionplan_id);


--
-- Name: m_productoperation m_productoperation_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productoperation
    ADD CONSTRAINT m_productoperation_pkey PRIMARY KEY (m_productoperation_id);


--
-- Name: m_productprice m_productprice_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productprice
    ADD CONSTRAINT m_productprice_pkey PRIMARY KEY (m_pricelist_version_id, m_product_id);


--
-- Name: m_productpricevendorbreak m_productpricevendorbreak_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productpricevendorbreak
    ADD CONSTRAINT m_productpricevendorbreak_key PRIMARY KEY (m_productpricevendorbreak_id);


--
-- Name: m_promotion m_promotion_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotion
    ADD CONSTRAINT m_promotion_key PRIMARY KEY (m_promotion_id);


--
-- Name: m_promotiondistribution m_promotiondistribution_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotiondistribution
    ADD CONSTRAINT m_promotiondistribution_key PRIMARY KEY (m_promotiondistribution_id);


--
-- Name: m_promotiongroup m_promotiongroup_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotiongroup
    ADD CONSTRAINT m_promotiongroup_key PRIMARY KEY (m_promotiongroup_id);


--
-- Name: m_promotiongroupline m_promotiongroupline_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotiongroupline
    ADD CONSTRAINT m_promotiongroupline_key PRIMARY KEY (m_promotiongroupline_id);


--
-- Name: m_promotionline m_promotionline_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotionline
    ADD CONSTRAINT m_promotionline_key PRIMARY KEY (m_promotionline_id);


--
-- Name: m_promotionprecondition m_promotionprecondition_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotionprecondition
    ADD CONSTRAINT m_promotionprecondition_key PRIMARY KEY (m_promotionprecondition_id);


--
-- Name: m_promotionreward m_promotionreward_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotionreward
    ADD CONSTRAINT m_promotionreward_key PRIMARY KEY (m_promotionreward_id);


--
-- Name: m_qualitytest m_qualitytest_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_qualitytest
    ADD CONSTRAINT m_qualitytest_key PRIMARY KEY (m_qualitytest_id);


--
-- Name: m_qualitytestresult m_qualitytestresult_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_qualitytestresult
    ADD CONSTRAINT m_qualitytestresult_key PRIMARY KEY (m_qualitytestresult_id);


--
-- Name: m_relatedproduct m_relatedproduct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_relatedproduct
    ADD CONSTRAINT m_relatedproduct_pkey PRIMARY KEY (m_product_id, relatedproduct_id, relatedproducttype);


--
-- Name: m_replenish m_replenish_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_replenish
    ADD CONSTRAINT m_replenish_pkey PRIMARY KEY (m_product_id, m_warehouse_id);


--
-- Name: m_requisition m_requisition_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_requisition
    ADD CONSTRAINT m_requisition_pkey PRIMARY KEY (m_requisition_id);


--
-- Name: m_requisitionline m_requisitionline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_requisitionline
    ADD CONSTRAINT m_requisitionline_pkey PRIMARY KEY (m_requisitionline_id);


--
-- Name: m_rma m_rma_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_rma
    ADD CONSTRAINT m_rma_pkey PRIMARY KEY (m_rma_id);


--
-- Name: m_rmaline m_rmaline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_rmaline
    ADD CONSTRAINT m_rmaline_pkey PRIMARY KEY (m_rmaline_id);


--
-- Name: m_rmatype m_rmatype_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_rmatype
    ADD CONSTRAINT m_rmatype_pkey PRIMARY KEY (m_rmatype_id);


--
-- Name: m_sernoctl m_sernoctl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_sernoctl
    ADD CONSTRAINT m_sernoctl_pkey PRIMARY KEY (m_sernoctl_id);


--
-- Name: m_sernoctlexclude m_sernoctlexclude_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_sernoctlexclude
    ADD CONSTRAINT m_sernoctlexclude_pkey PRIMARY KEY (m_sernoctlexclude_id);


--
-- Name: m_shipper m_shipper_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_shipper
    ADD CONSTRAINT m_shipper_pkey PRIMARY KEY (m_shipper_id);


--
-- Name: m_storage m_storage_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_storage
    ADD CONSTRAINT m_storage_pkey PRIMARY KEY (m_product_id, m_locator_id, m_attributesetinstance_id);


--
-- Name: m_substitute m_substitute_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_substitute
    ADD CONSTRAINT m_substitute_pkey PRIMARY KEY (m_product_id, substitute_id);


--
-- Name: m_transaction m_transaction_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transaction
    ADD CONSTRAINT m_transaction_pkey PRIMARY KEY (m_transaction_id);


--
-- Name: m_transactionallocation m_transactionallocation_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transactionallocation
    ADD CONSTRAINT m_transactionallocation_pkey PRIMARY KEY (m_transaction_id, allocationstrategytype);


--
-- Name: m_warehouse_acct m_warehouse_acct_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_warehouse_acct
    ADD CONSTRAINT m_warehouse_acct_pkey PRIMARY KEY (m_warehouse_id, c_acctschema_id);


--
-- Name: m_warehouse m_warehouse_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_warehouse
    ADD CONSTRAINT m_warehouse_pkey PRIMARY KEY (m_warehouse_id);


--
-- Name: m_workcentre m_workcentre_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_workcentre
    ADD CONSTRAINT m_workcentre_key PRIMARY KEY (m_workcentre_id);


--
-- Name: m_workcentretype m_workcentretype_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_workcentretype
    ADD CONSTRAINT m_workcentretype_key PRIMARY KEY (m_workcentretype_id);


--
-- Name: mrp_run mrp_run_clientorg_check; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.mrp_run
    ADD CONSTRAINT mrp_run_clientorg_check UNIQUE (ad_client_id, ad_org_id);


--
-- Name: CONSTRAINT mrp_run_clientorg_check ON mrp_run; Type: COMMENT; Schema: ampere; Owner: -
--

COMMENT ON CONSTRAINT mrp_run_clientorg_check ON ampere.mrp_run IS 'Prevent duplicate Client and Org';


--
-- Name: mrp_run mrp_run_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.mrp_run
    ADD CONSTRAINT mrp_run_key PRIMARY KEY (mrp_run_id);


--
-- Name: mrp_runline mrp_runline_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.mrp_runline
    ADD CONSTRAINT mrp_runline_key PRIMARY KEY (mrp_runline_id);


--
-- Name: pa_achievement pa_achievement_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_achievement
    ADD CONSTRAINT pa_achievement_pkey PRIMARY KEY (pa_achievement_id);


--
-- Name: pa_benchmark pa_benchmark_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_benchmark
    ADD CONSTRAINT pa_benchmark_pkey PRIMARY KEY (pa_benchmark_id);


--
-- Name: pa_benchmarkdata pa_benchmarkdata_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_benchmarkdata
    ADD CONSTRAINT pa_benchmarkdata_pkey PRIMARY KEY (pa_benchmarkdata_id);


--
-- Name: pa_colorschema pa_colorschema_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_colorschema
    ADD CONSTRAINT pa_colorschema_pkey PRIMARY KEY (pa_colorschema_id);


--
-- Name: pa_dashboardcontent pa_dashboardcontent_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_dashboardcontent
    ADD CONSTRAINT pa_dashboardcontent_key PRIMARY KEY (pa_dashboardcontent_id);


--
-- Name: pa_dashboardcontent_trl pa_dashboardcontent_trl_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_dashboardcontent_trl
    ADD CONSTRAINT pa_dashboardcontent_trl_key PRIMARY KEY (ad_language, pa_dashboardcontent_id);


--
-- Name: pa_goal pa_goal_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_goal
    ADD CONSTRAINT pa_goal_pkey PRIMARY KEY (pa_goal_id);


--
-- Name: pa_goalrestriction pa_goalrestriction_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_goalrestriction
    ADD CONSTRAINT pa_goalrestriction_pkey PRIMARY KEY (pa_goalrestriction_id);


--
-- Name: pa_hierarchy pa_hierarchy_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_hierarchy
    ADD CONSTRAINT pa_hierarchy_pkey PRIMARY KEY (pa_hierarchy_id);


--
-- Name: pa_measure pa_measure_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_measure
    ADD CONSTRAINT pa_measure_pkey PRIMARY KEY (pa_measure_id);


--
-- Name: pa_measurecalc pa_measurecalc_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_measurecalc
    ADD CONSTRAINT pa_measurecalc_pkey PRIMARY KEY (pa_measurecalc_id);


--
-- Name: pa_ratio pa_ratio_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_ratio
    ADD CONSTRAINT pa_ratio_pkey PRIMARY KEY (pa_ratio_id);


--
-- Name: pa_ratioelement pa_ratioelement_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_ratioelement
    ADD CONSTRAINT pa_ratioelement_pkey PRIMARY KEY (pa_ratioelement_id);


--
-- Name: pa_report pa_report_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_report
    ADD CONSTRAINT pa_report_pkey PRIMARY KEY (pa_report_id);


--
-- Name: pa_reportcolumn pa_reportcolumn_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT pa_reportcolumn_pkey PRIMARY KEY (pa_reportcolumn_id);


--
-- Name: pa_reportcolumnset pa_reportcolumnset_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumnset
    ADD CONSTRAINT pa_reportcolumnset_pkey PRIMARY KEY (pa_reportcolumnset_id);


--
-- Name: pa_reportcube pa_reportcube_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcube
    ADD CONSTRAINT pa_reportcube_key PRIMARY KEY (pa_reportcube_id);


--
-- Name: pa_reportline pa_reportline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportline
    ADD CONSTRAINT pa_reportline_pkey PRIMARY KEY (pa_reportline_id);


--
-- Name: pa_reportlineset pa_reportlineset_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportlineset
    ADD CONSTRAINT pa_reportlineset_pkey PRIMARY KEY (pa_reportlineset_id);


--
-- Name: pa_reportsource pa_reportsource_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportsource
    ADD CONSTRAINT pa_reportsource_pkey PRIMARY KEY (pa_reportsource_id);


--
-- Name: pa_sla_criteria pa_sla_criteria_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_sla_criteria
    ADD CONSTRAINT pa_sla_criteria_pkey PRIMARY KEY (pa_sla_criteria_id);


--
-- Name: pa_sla_goal pa_sla_goal_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_sla_goal
    ADD CONSTRAINT pa_sla_goal_pkey PRIMARY KEY (pa_sla_goal_id);


--
-- Name: pa_sla_measure pa_sla_measure_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_sla_measure
    ADD CONSTRAINT pa_sla_measure_pkey PRIMARY KEY (pa_sla_measure_id);


--
-- Name: r_category r_category_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_category
    ADD CONSTRAINT r_category_pkey PRIMARY KEY (r_category_id);


--
-- Name: r_categoryupdates r_categoryupdates_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_categoryupdates
    ADD CONSTRAINT r_categoryupdates_pkey PRIMARY KEY (ad_user_id, r_category_id);


--
-- Name: r_contactinterest r_contactinterest_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_contactinterest
    ADD CONSTRAINT r_contactinterest_key PRIMARY KEY (ad_user_id, r_interestarea_id);


--
-- Name: r_group r_group_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_group
    ADD CONSTRAINT r_group_pkey PRIMARY KEY (r_group_id);


--
-- Name: r_groupupdates r_groupupdates_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_groupupdates
    ADD CONSTRAINT r_groupupdates_pkey PRIMARY KEY (ad_user_id, r_group_id);


--
-- Name: r_interestarea r_interestarea_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_interestarea
    ADD CONSTRAINT r_interestarea_pkey PRIMARY KEY (r_interestarea_id);


--
-- Name: r_issueknown r_issueknown_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issueknown
    ADD CONSTRAINT r_issueknown_pkey PRIMARY KEY (r_issueknown_id);


--
-- Name: r_issueproject r_issueproject_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issueproject
    ADD CONSTRAINT r_issueproject_pkey PRIMARY KEY (r_issueproject_id);


--
-- Name: r_issuerecommendation r_issuerecommendation_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issuerecommendation
    ADD CONSTRAINT r_issuerecommendation_pkey PRIMARY KEY (r_issuerecommendation_id);


--
-- Name: r_issuesource r_issuesource_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issuesource
    ADD CONSTRAINT r_issuesource_pkey PRIMARY KEY (r_issuesource_id);


--
-- Name: r_issuestatus r_issuestatus_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issuestatus
    ADD CONSTRAINT r_issuestatus_pkey PRIMARY KEY (r_issuestatus_id);


--
-- Name: r_issuesystem r_issuesystem_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issuesystem
    ADD CONSTRAINT r_issuesystem_pkey PRIMARY KEY (r_issuesystem_id);


--
-- Name: r_issueuser r_issueuser_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issueuser
    ADD CONSTRAINT r_issueuser_pkey PRIMARY KEY (r_issueuser_id);


--
-- Name: r_mailtext r_mailtext_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_mailtext
    ADD CONSTRAINT r_mailtext_pkey PRIMARY KEY (r_mailtext_id);


--
-- Name: r_mailtext_trl r_mailtext_trl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_mailtext_trl
    ADD CONSTRAINT r_mailtext_trl_pkey PRIMARY KEY (r_mailtext_id, ad_language);


--
-- Name: r_request r_request_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT r_request_pkey PRIMARY KEY (r_request_id);


--
-- Name: r_requestaction r_requestaction_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT r_requestaction_pkey PRIMARY KEY (r_requestaction_id);


--
-- Name: r_requestprocessor r_requestprocessor_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestprocessor
    ADD CONSTRAINT r_requestprocessor_pkey PRIMARY KEY (r_requestprocessor_id);


--
-- Name: r_requestprocessor_route r_requestprocessor_route_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestprocessor_route
    ADD CONSTRAINT r_requestprocessor_route_pkey PRIMARY KEY (r_requestprocessor_route_id);


--
-- Name: r_requestprocessorlog r_requestprocessorlog_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestprocessorlog
    ADD CONSTRAINT r_requestprocessorlog_pkey PRIMARY KEY (r_requestprocessor_id, r_requestprocessorlog_id);


--
-- Name: r_requesttype r_requesttype_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requesttype
    ADD CONSTRAINT r_requesttype_pkey PRIMARY KEY (r_requesttype_id);


--
-- Name: r_requesttypeupdates r_requesttypeupdates_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requesttypeupdates
    ADD CONSTRAINT r_requesttypeupdates_pkey PRIMARY KEY (ad_user_id, r_requesttype_id);


--
-- Name: r_requestupdate r_requestupdate_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestupdate
    ADD CONSTRAINT r_requestupdate_pkey PRIMARY KEY (r_requestupdate_id);


--
-- Name: r_requestupdates r_requestupdates_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestupdates
    ADD CONSTRAINT r_requestupdates_pkey PRIMARY KEY (ad_user_id, r_request_id);


--
-- Name: r_resolution r_resolution_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_resolution
    ADD CONSTRAINT r_resolution_pkey PRIMARY KEY (r_resolution_id);


--
-- Name: r_standardresponse r_standardresponse_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_standardresponse
    ADD CONSTRAINT r_standardresponse_pkey PRIMARY KEY (r_standardresponse_id);


--
-- Name: r_status r_status_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_status
    ADD CONSTRAINT r_status_pkey PRIMARY KEY (r_status_id);


--
-- Name: r_statuscategory r_statuscategory_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_statuscategory
    ADD CONSTRAINT r_statuscategory_pkey PRIMARY KEY (r_statuscategory_id);


--
-- Name: s_expensetype s_expensetype_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_expensetype
    ADD CONSTRAINT s_expensetype_pkey PRIMARY KEY (s_expensetype_id);


--
-- Name: s_resource s_resource_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_resource
    ADD CONSTRAINT s_resource_pkey PRIMARY KEY (s_resource_id);


--
-- Name: s_resourceassignment s_resourceassignment_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_resourceassignment
    ADD CONSTRAINT s_resourceassignment_pkey PRIMARY KEY (s_resourceassignment_id);


--
-- Name: s_resourcetype s_resourcetype_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_resourcetype
    ADD CONSTRAINT s_resourcetype_pkey PRIMARY KEY (s_resourcetype_id);


--
-- Name: s_resourceunavailable s_resourceunavailable_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_resourceunavailable
    ADD CONSTRAINT s_resourceunavailable_pkey PRIMARY KEY (s_resourceunavailable_id);


--
-- Name: s_timeexpense s_timeexpense_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpense
    ADD CONSTRAINT s_timeexpense_pkey PRIMARY KEY (s_timeexpense_id);


--
-- Name: s_timeexpenseline s_timeexpenseline_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpenseline
    ADD CONSTRAINT s_timeexpenseline_pkey PRIMARY KEY (s_timeexpenseline_id);


--
-- Name: s_timesheetentry s_timesheetentry_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timesheetentry
    ADD CONSTRAINT s_timesheetentry_key PRIMARY KEY (s_timesheetentry_id);


--
-- Name: s_timetype s_timetype_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timetype
    ADD CONSTRAINT s_timetype_pkey PRIMARY KEY (s_timetype_id);


--
-- Name: s_training_class s_training_class_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_training_class
    ADD CONSTRAINT s_training_class_pkey PRIMARY KEY (s_training_class_id);


--
-- Name: s_training s_training_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_training
    ADD CONSTRAINT s_training_pkey PRIMARY KEY (s_training_id);


--
-- Name: t_aging t_aging_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_aging
    ADD CONSTRAINT t_aging_pkey PRIMARY KEY (ad_pinstance_id, c_bpartner_id, c_currency_id, c_invoice_id, c_invoicepayschedule_id);


--
-- Name: t_bom_indented t_bom_indented_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_bom_indented
    ADD CONSTRAINT t_bom_indented_key PRIMARY KEY (t_bom_indented_id);


--
-- Name: t_bomline t_bomline_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_bomline
    ADD CONSTRAINT t_bomline_key PRIMARY KEY (t_bomline_id);


--
-- Name: t_cashflow t_cashflow_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_cashflow
    ADD CONSTRAINT t_cashflow_key PRIMARY KEY (t_cashflow_id);


--
-- Name: t_combinedaging t_combinedaging_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_combinedaging
    ADD CONSTRAINT t_combinedaging_key PRIMARY KEY (ad_pinstance_id, c_bpartner_id, c_currency_id, c_invoice_id, c_payment_id);


--
-- Name: t_distributionrundetail t_distributionrundetail_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_distributionrundetail
    ADD CONSTRAINT t_distributionrundetail_pkey PRIMARY KEY (m_distributionrun_id, m_distributionrunline_id, m_distributionlist_id, m_distributionlistline_id);


--
-- Name: t_inventoryvalue t_inventoryvalue_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_inventoryvalue
    ADD CONSTRAINT t_inventoryvalue_pkey PRIMARY KEY (ad_pinstance_id, m_warehouse_id, m_product_id, m_locator_id, m_attributesetinstance_id);


--
-- Name: t_invoicegl t_invoicegl_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_invoicegl
    ADD CONSTRAINT t_invoicegl_pkey PRIMARY KEY (ad_pinstance_id, c_invoice_id, fact_acct_id);


--
-- Name: t_minimrp t_minimrp_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_minimrp
    ADD CONSTRAINT t_minimrp_key PRIMARY KEY (t_minimrp_id);


--
-- Name: t_mrp_crp t_mrp_crp_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_mrp_crp
    ADD CONSTRAINT t_mrp_crp_key PRIMARY KEY (t_mrp_crp_id);


--
-- Name: t_mrp_run_capacity t_mrp_run_capacity_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_mrp_run_capacity
    ADD CONSTRAINT t_mrp_run_capacity_key PRIMARY KEY (t_mrp_run_capacity_id);


--
-- Name: t_mrp_run_capacity_summary t_mrp_run_capacity_summary_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_mrp_run_capacity_summary
    ADD CONSTRAINT t_mrp_run_capacity_summary_key PRIMARY KEY (t_mrp_run_capacity_summary_id);


--
-- Name: t_product_bomline t_product_bomline_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_product_bomline
    ADD CONSTRAINT t_product_bomline_key PRIMARY KEY (t_product_bomline_id);


--
-- Name: t_replenish t_replenish_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_replenish
    ADD CONSTRAINT t_replenish_pkey PRIMARY KEY (ad_pinstance_id, m_warehouse_id, m_product_id);


--
-- Name: t_replenish_po t_replenish_po_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_replenish_po
    ADD CONSTRAINT t_replenish_po_key PRIMARY KEY (t_replenish_po_id);


--
-- Name: t_report t_report_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_report
    ADD CONSTRAINT t_report_pkey PRIMARY KEY (ad_pinstance_id, pa_reportline_id, record_id, fact_acct_id, c_validcombination_id);


--
-- Name: t_reportstatement t_reportstatement_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_reportstatement
    ADD CONSTRAINT t_reportstatement_pkey PRIMARY KEY (ad_pinstance_id, fact_acct_id);


--
-- Name: t_selection2 t_selection2_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_selection2
    ADD CONSTRAINT t_selection2_pkey PRIMARY KEY (ad_pinstance_id, query_id, t_selection_id);


--
-- Name: t_selection_infowindow t_selection_infowindow_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_selection_infowindow
    ADD CONSTRAINT t_selection_infowindow_key PRIMARY KEY (ad_pinstance_id, t_selection_id, columnname);


--
-- Name: t_selection t_selection_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_selection
    ADD CONSTRAINT t_selection_pkey PRIMARY KEY (ad_pinstance_id, t_selection_id);


--
-- Name: t_transaction t_transaction_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_transaction
    ADD CONSTRAINT t_transaction_pkey PRIMARY KEY (ad_pinstance_id, m_transaction_id);


--
-- Name: t_trialbalance t_trialbalance_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_trialbalance
    ADD CONSTRAINT t_trialbalance_pkey PRIMARY KEY (ad_pinstance_id, fact_acct_id);


--
-- Name: test test_pkey; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.test
    ADD CONSTRAINT test_pkey PRIMARY KEY (test_id);


--
-- Name: u_pa_documentstatus u_pa_documentstatus_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.u_pa_documentstatus
    ADD CONSTRAINT u_pa_documentstatus_key PRIMARY KEY (u_pa_documentstatus_id);


--
-- Name: c_conversion_rate unique_c_conversion_rate; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_conversion_rate
    ADD CONSTRAINT unique_c_conversion_rate UNIQUE (ad_client_id, ad_org_id, c_currency_id, c_currency_id_to, validfrom, c_conversiontype_id);


--
-- Name: x_authoritytype x_authoritytype_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.x_authoritytype
    ADD CONSTRAINT x_authoritytype_key PRIMARY KEY (x_authoritytype_id);


--
-- Name: x_conv_currency x_conv_currency_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.x_conv_currency
    ADD CONSTRAINT x_conv_currency_key PRIMARY KEY (x_conv_currency_id);


--
-- Name: x_conv_currency_rate x_conv_currency_rate_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.x_conv_currency_rate
    ADD CONSTRAINT x_conv_currency_rate_key PRIMARY KEY (x_conv_currency_rate_id);


--
-- Name: x_inoutcons x_inoutcons_key; Type: CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.x_inoutcons
    ADD CONSTRAINT x_inoutcons_key PRIMARY KEY (x_inoutcons_id);


--
-- Name: ad_attachment_record; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_attachment_record ON ampere.ad_attachment USING btree (ad_table_id, record_id);


--
-- Name: ad_client_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_client_name ON ampere.ad_client USING btree (name);


--
-- Name: ad_clientshare_table; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_clientshare_table ON ampere.ad_clientshare USING btree (ad_client_id, ad_table_id);


--
-- Name: ad_column_element; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_column_element ON ampere.ad_column USING btree (ad_element_id);


--
-- Name: ad_column_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_column_name ON ampere.ad_column USING btree (ad_table_id, columnname);


--
-- Name: ad_element_clientorg; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_element_clientorg ON ampere.ad_element USING btree (ad_client_id, ad_org_id);


--
-- Name: ad_element_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_element_name ON ampere.ad_element USING btree (name);


--
-- Name: ad_element_uppercolumnname; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_element_uppercolumnname ON ampere.ad_element USING btree (upper(columnname));


--
-- Name: ad_eventlog_record; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_eventlog_record ON ampere.ad_eventlog USING btree (ad_table_id, record_id);


--
-- Name: ad_field_column; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_field_column ON ampere.ad_field USING btree (ad_tab_id, ad_column_id);


--
-- Name: ad_fieldgroup_key; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_fieldgroup_key ON ampere.ad_fieldgroup USING btree (ad_fieldgroup_id);


--
-- Name: ad_impformat_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_impformat_name ON ampere.ad_impformat USING btree (name);


--
-- Name: ad_message_value; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_message_value ON ampere.ad_message USING btree (value);


--
-- Name: ad_note_key; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_note_key ON ampere.ad_note USING btree (ad_note_id);


--
-- Name: ad_org_value; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_org_value ON ampere.ad_org USING btree (ad_client_id, value);


--
-- Name: ad_pinstance_record; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_pinstance_record ON ampere.ad_pinstance USING btree (ad_process_id, record_id);


--
-- Name: ad_preference_attribute; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_preference_attribute ON ampere.ad_preference USING btree (ad_client_id, ad_org_id, ad_window_id, ad_user_id, attribute);


--
-- Name: ad_printcolor_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_printcolor_name ON ampere.ad_printcolor USING btree (ad_client_id, name);


--
-- Name: ad_printfont_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_printfont_name ON ampere.ad_printfont USING btree (ad_client_id, name);


--
-- Name: ad_printform_client; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_printform_client ON ampere.ad_printform USING btree (ad_client_id, ad_org_id);


--
-- Name: ad_printformat_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_printformat_name ON ampere.ad_printformat USING btree (ad_client_id, ad_table_id, name);


--
-- Name: ad_printformat_table; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_printformat_table ON ampere.ad_printformat USING btree (ad_table_id);


--
-- Name: ad_printformatitem_format; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_printformatitem_format ON ampere.ad_printformatitem USING btree (ad_printformat_id);


--
-- Name: ad_printpaper_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_printpaper_name ON ampere.ad_printpaper USING btree (ad_client_id, name);


--
-- Name: ad_process_para_process; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_process_para_process ON ampere.ad_process_para USING btree (ad_process_id);


--
-- Name: ad_procpara_procseqno; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_procpara_procseqno ON ampere.ad_process_para USING btree (ad_process_id, seqno);


--
-- Name: ad_ref_list_value; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_ref_list_value ON ampere.ad_ref_list USING btree (ad_reference_id, value);


--
-- Name: ad_reference_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_reference_name ON ampere.ad_reference USING btree (name);


--
-- Name: ad_sequence_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_sequence_name ON ampere.ad_sequence USING btree (ad_client_id, name);


--
-- Name: ad_tab_table; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_tab_table ON ampere.ad_tab USING btree (ad_table_id);


--
-- Name: ad_tab_window; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_tab_window ON ampere.ad_tab USING btree (ad_window_id);


--
-- Name: ad_table_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_table_name ON ampere.ad_table USING btree (tablename);


--
-- Name: ad_tree_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_tree_name ON ampere.ad_tree USING btree (ad_client_id, name);


--
-- Name: ad_treenode_parentid; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_treenode_parentid ON ampere.ad_treenode USING btree (parent_id);


--
-- Name: ad_treenodebp_parent; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_treenodebp_parent ON ampere.ad_treenodebp USING btree (parent_id);


--
-- Name: ad_treenodemm_parent; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_treenodemm_parent ON ampere.ad_treenodemm USING btree (parent_id);


--
-- Name: ad_treenodepr_parent; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_treenodepr_parent ON ampere.ad_treenodepr USING btree (parent_id);


--
-- Name: ad_user_email; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_user_email ON ampere.ad_user USING btree (email);


--
-- Name: ad_wf_process_workflow; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX ad_wf_process_workflow ON ampere.ad_wf_process USING btree (ad_workflow_id);


--
-- Name: ad_window_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_window_name ON ampere.ad_window USING btree (ad_client_id, name);


--
-- Name: ad_workflow_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX ad_workflow_name ON ampere.ad_workflow USING btree (ad_client_id, name);


--
-- Name: c_acctschema_element_schema; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_acctschema_element_schema ON ampere.c_acctschema_element USING btree (c_acctschema_id);


--
-- Name: c_acctschema_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_acctschema_name ON ampere.c_acctschema USING btree (ad_client_id, name);


--
-- Name: c_activity_value; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_activity_value ON ampere.c_activity USING btree (ad_client_id, value);


--
-- Name: c_allocationline_invoice; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_allocationline_invoice ON ampere.c_allocationline USING btree (c_invoice_id);


--
-- Name: c_allocationline_payment; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_allocationline_payment ON ampere.c_allocationline USING btree (c_payment_id);


--
-- Name: c_bankacct_bank; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_bankacct_bank ON ampere.c_bankaccount USING btree (c_bank_id);


--
-- Name: c_bankstmtline_bankstmt; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_bankstmtline_bankstmt ON ampere.c_bankstatementline USING btree (c_bankstatement_id);


--
-- Name: c_bp_group_value; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_bp_group_value ON ampere.c_bp_group USING btree (ad_client_id, value);


--
-- Name: c_bpartner_bporg; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_bpartner_bporg ON ampere.c_bpartner USING btree (ad_orgbp_id);


--
-- Name: c_bpartner_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_bpartner_name ON ampere.c_bpartner USING btree (name);


--
-- Name: c_bpartner_value; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_bpartner_value ON ampere.c_bpartner USING btree (ad_client_id, value);


--
-- Name: c_bpbankacct_bpartner; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_bpbankacct_bpartner ON ampere.c_bp_bankaccount USING btree (c_bpartner_id);


--
-- Name: c_bplocation_bpartner; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_bplocation_bpartner ON ampere.c_bpartner_location USING btree (c_bpartner_id);


--
-- Name: c_bplocation_updated; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_bplocation_updated ON ampere.c_bpartner_location USING btree (updated);


--
-- Name: c_calendar_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_calendar_name ON ampere.c_calendar USING btree (ad_client_id, name);


--
-- Name: c_campaign_value; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_campaign_value ON ampere.c_campaign USING btree (ad_client_id, value);


--
-- Name: c_cashline_cash; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_cashline_cash ON ampere.c_cashline USING btree (c_cash_id);


--
-- Name: c_channel_key; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_channel_key ON ampere.c_channel USING btree (c_channel_id);


--
-- Name: c_channel_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_channel_name ON ampere.c_channel USING btree (ad_client_id, name);


--
-- Name: c_charge_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_charge_name ON ampere.c_charge USING btree (ad_client_id, name);


--
-- Name: c_commissionamt_comline; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_commissionamt_comline ON ampere.c_commissionamt USING btree (c_commissionline_id);


--
-- Name: c_commissionamt_run; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_commissionamt_run ON ampere.c_commissionamt USING btree (c_commissionrun_id);


--
-- Name: c_commissionline_commission; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_commissionline_commission ON ampere.c_commissionline USING btree (c_commission_id);


--
-- Name: c_conversionrate_once; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_conversionrate_once ON ampere.c_conversion_rate USING btree (ad_client_id, ad_org_id, c_currency_id, c_currency_id_to, c_conversiontype_id, validfrom);


--
-- Name: c_countrycode; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_countrycode ON ampere.c_country USING btree (countrycode);


--
-- Name: c_currencyisocode; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_currencyisocode ON ampere.c_currency USING btree (iso_code);


--
-- Name: c_cycle_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_cycle_name ON ampere.c_cycle USING btree (ad_client_id, name);


--
-- Name: c_doctype_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_doctype_name ON ampere.c_doctype USING btree (ad_client_id, name);


--
-- Name: c_element_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_element_name ON ampere.c_element USING btree (ad_client_id, name);


--
-- Name: c_elementvalue_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_elementvalue_name ON ampere.c_elementvalue USING btree (name);


--
-- Name: c_elementvalue_value; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_elementvalue_value ON ampere.c_elementvalue USING btree (c_element_id, value);


--
-- Name: c_invoice_documentno; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_invoice_documentno ON ampere.c_invoice USING btree (documentno, c_doctype_id, c_bpartner_id);


--
-- Name: c_invoice_documentno_target; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_invoice_documentno_target ON ampere.c_invoice USING btree (c_bpartner_id, documentno, c_doctypetarget_id);


--
-- Name: c_invoice_key; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_invoice_key ON ampere.c_invoice USING btree (c_invoice_id);


--
-- Name: c_invoice_order; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_invoice_order ON ampere.c_invoice USING btree (c_order_id);


--
-- Name: c_invoice_paid; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_invoice_paid ON ampere.c_invoice USING btree (ad_client_id, ispaid);


--
-- Name: c_invoiceline_invoice; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_invoiceline_invoice ON ampere.c_invoiceline USING btree (c_invoice_id);


--
-- Name: c_invoiceline_orderline; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_invoiceline_orderline ON ampere.c_invoiceline USING btree (c_orderline_id);


--
-- Name: c_invoiceline_product; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_invoiceline_product ON ampere.c_invoiceline USING btree (m_product_id);


--
-- Name: c_invoicepayschedule_invoice; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_invoicepayschedule_invoice ON ampere.c_invoicepayschedule USING btree (c_invoice_id);


--
-- Name: c_order_bpartner; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_order_bpartner ON ampere.c_order USING btree (c_bpartner_id);


--
-- Name: c_order_documentno; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_order_documentno ON ampere.c_order USING btree (documentno, c_doctype_id, c_bpartner_id);


--
-- Name: c_order_processed; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_order_processed ON ampere.c_order USING btree (ad_client_id, processed);


--
-- Name: c_orderline_order; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_orderline_order ON ampere.c_orderline USING btree (c_order_id);


--
-- Name: c_orderline_product; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_orderline_product ON ampere.c_orderline USING btree (m_product_id);


--
-- Name: c_payment_bankaccount; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_payment_bankaccount ON ampere.c_payment USING btree (c_bankaccount_id);


--
-- Name: c_payment_bpartner; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_payment_bpartner ON ampere.c_payment USING btree (c_bpartner_id);


--
-- Name: c_paymentprocessor_key; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_paymentprocessor_key ON ampere.c_paymentprocessor USING btree (c_paymentprocessor_id);


--
-- Name: c_paymentterm_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_paymentterm_name ON ampere.c_paymentterm USING btree (ad_client_id, name);


--
-- Name: c_payselline_paysel; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_payselline_paysel ON ampere.c_payselectionline USING btree (c_payselection_id);


--
-- Name: c_period_nounique; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_period_nounique ON ampere.c_period USING btree (c_year_id, periodno);


--
-- Name: c_project_value; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_project_value ON ampere.c_project USING btree (ad_client_id, value);


--
-- Name: c_region_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_region_name ON ampere.c_region USING btree (c_country_id, name);


--
-- Name: c_revenuerecognition_key; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_revenuerecognition_key ON ampere.c_revenuerecognition USING btree (c_revenuerecognition_id);


--
-- Name: c_salesregion_value; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_salesregion_value ON ampere.c_salesregion USING btree (ad_client_id, value);


--
-- Name: c_tax_key; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_tax_key ON ampere.c_tax USING btree (c_tax_id);


--
-- Name: c_tax_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_tax_name ON ampere.c_tax USING btree (ad_client_id, name);


--
-- Name: c_taxcategory_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_taxcategory_name ON ampere.c_taxcategory USING btree (ad_client_id, name);


--
-- Name: c_uom_conversion_product; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_uom_conversion_product ON ampere.c_uom_conversion USING btree (c_uom_id, c_uom_to_id, m_product_id);


--
-- Name: c_uom_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_uom_name ON ampere.c_uom USING btree (ad_client_id, name);


--
-- Name: c_uom_x12; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_uom_x12 ON ampere.c_uom USING btree (x12de355);


--
-- Name: c_validcombination_alias; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX c_validcombination_alias ON ampere.c_validcombination USING btree (ad_client_id, alias);


--
-- Name: c_validcombination_alt; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_validcombination_alt ON ampere.c_validcombination USING btree (c_acctschema_id, ad_org_id, account_id, c_subacct_id, m_product_id, c_bpartner_id, ad_orgtrx_id, c_locfrom_id, c_locto_id, c_salesregion_id, c_project_id, c_campaign_id, c_activity_id, user1_id, user2_id, userelement1_id, userelement2_id);


--
-- Name: c_year_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX c_year_name ON ampere.c_year USING btree (c_calendar_id, fiscalyear);


--
-- Name: cordersource_value; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX cordersource_value ON ampere.c_ordersource USING btree (ad_client_id, value);


--
-- Name: fact_acct_account; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX fact_acct_account ON ampere.fact_acct USING btree (ad_client_id, ad_org_id, c_acctschema_id, account_id);


--
-- Name: fact_acct_dateacct; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX fact_acct_dateacct ON ampere.fact_acct USING btree (dateacct);


--
-- Name: fact_acct_trunc_dateacct; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX fact_acct_trunc_dateacct ON ampere.fact_acct USING btree (((dateacct)::date));


--
-- Name: fact_matchcode; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX fact_matchcode ON ampere.fact_reconciliation USING btree (matchcode);


--
-- Name: fas_account; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX fas_account ON ampere.fact_acct_summary USING btree (ad_client_id, ad_org_id, c_acctschema_id, account_id);


--
-- Name: fas_dateacct; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX fas_dateacct ON ampere.fact_acct_summary USING btree (dateacct);


--
-- Name: fas_period; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX fas_period ON ampere.fact_acct_summary USING btree (c_period_id);


--
-- Name: fas_reportcube; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX fas_reportcube ON ampere.fact_acct_summary USING btree (pa_reportcube_id);


--
-- Name: fki_adattachment_entry; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX fki_adattachment_entry ON ampere.ad_attachmententry USING btree (ad_attachment_id);


--
-- Name: fki_callocation_callocationline; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX fki_callocation_callocationline ON ampere.c_allocationline USING btree (c_allocationhdr_id);


--
-- Name: fki_pa_reportcolumn_pareportsource; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX fki_pa_reportcolumn_pareportsource ON ampere.pa_reportsource USING btree (pa_reportcolumn_id);


--
-- Name: gl_budget_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX gl_budget_name ON ampere.gl_budget USING btree (ad_client_id, name);


--
-- Name: gl_category_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX gl_category_name ON ampere.gl_category USING btree (ad_client_id, name);


--
-- Name: gl_journal_docno; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX gl_journal_docno ON ampere.gl_journal USING btree (ad_org_id, c_period_id, documentno);


--
-- Name: gl_journalbatch_docno; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX gl_journalbatch_docno ON ampere.gl_journalbatch USING btree (ad_org_id, c_period_id, documentno);


--
-- Name: gl_journalline_key; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX gl_journalline_key ON ampere.gl_journalline USING btree (gl_journalline_id);


--
-- Name: m_bom_productversiontype; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX m_bom_productversiontype ON ampere.m_bom USING btree (m_product_id, m_changenotice_id);


--
-- Name: m_inout_bpartner; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_inout_bpartner ON ampere.m_inout USING btree (c_bpartner_id);


--
-- Name: m_inout_documentno; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_inout_documentno ON ampere.m_inout USING btree (documentno);


--
-- Name: m_inout_order; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_inout_order ON ampere.m_inout USING btree (c_order_id);


--
-- Name: m_inoutline_inout; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_inoutline_inout ON ampere.m_inoutline USING btree (m_inout_id);


--
-- Name: m_inoutline_product; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_inoutline_product ON ampere.m_inoutline USING btree (m_product_id);


--
-- Name: m_inventoryline_productlocattr; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX m_inventoryline_productlocattr ON ampere.m_inventoryline USING btree (m_inventory_id, m_locator_id, m_product_id, m_attributesetinstance_id);


--
-- Name: m_location_where; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX m_location_where ON ampere.m_locator USING btree (m_warehouse_id, x, y, z);


--
-- Name: m_matchinv_ship; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_matchinv_ship ON ampere.m_matchinv USING btree (c_invoiceline_id, m_inoutline_id);


--
-- Name: m_matchpo_ship; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_matchpo_ship ON ampere.m_matchpo USING btree (c_orderline_id, m_inoutline_id);


--
-- Name: m_movementline_movement; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_movementline_movement ON ampere.m_movementline USING btree (m_movement_id);


--
-- Name: m_pricelist_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX m_pricelist_name ON ampere.m_pricelist USING btree (ad_client_id, name);


--
-- Name: m_product_category_value; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX m_product_category_value ON ampere.m_product_category USING btree (ad_client_id, value);


--
-- Name: m_product_expensetype; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX m_product_expensetype ON ampere.m_product USING btree (s_expensetype_id);


--
-- Name: m_product_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_product_name ON ampere.m_product USING btree (name);


--
-- Name: m_product_po_vendorprodno; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_product_po_vendorprodno ON ampere.m_product_po USING btree (c_bpartner_id, vendorproductno);


--
-- Name: m_product_productcategory; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_product_productcategory ON ampere.m_product USING btree (m_product_category_id);


--
-- Name: m_product_resource; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX m_product_resource ON ampere.m_product USING btree (s_resource_id);


--
-- Name: m_product_upc; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_product_upc ON ampere.m_product USING btree (upc);


--
-- Name: m_product_value; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX m_product_value ON ampere.m_product USING btree (ad_client_id, value);


--
-- Name: m_productionline_key; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX m_productionline_key ON ampere.m_productionline USING btree (m_productionline_id);


--
-- Name: m_productionline_production_idx; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_productionline_production_idx ON ampere.m_productionline USING btree (m_production_id);


--
-- Name: m_productionplan_production; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_productionplan_production ON ampere.m_productionplan USING btree (m_production_id);


--
-- Name: m_shipper_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX m_shipper_name ON ampere.m_shipper USING btree (ad_client_id, name);


--
-- Name: m_transactionallocation_prd; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_transactionallocation_prd ON ampere.m_transactionallocation USING btree (m_product_id, m_attributesetinstance_id, isallocated);


--
-- Name: m_transsaction_product; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX m_transsaction_product ON ampere.m_transaction USING btree (m_product_id);


--
-- Name: m_warehouse_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX m_warehouse_name ON ampere.m_warehouse USING btree (ad_client_id, name);


--
-- Name: r_issueproject_name; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX r_issueproject_name ON ampere.r_issueproject USING btree (name);


--
-- Name: r_issuesource_spo; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX r_issuesource_spo ON ampere.r_issuesource USING btree (r_issuesystem_id, r_issueproject_id, r_issueuser_id);


--
-- Name: r_issuesystem_address; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX r_issuesystem_address ON ampere.r_issuesystem USING btree (dbaddress);


--
-- Name: r_issueuser_email; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX r_issueuser_email ON ampere.r_issueuser USING btree (username, ad_client_id);


--
-- Name: r_knownissue_alt; Type: INDEX; Schema: ampere; Owner: -
--

CREATE UNIQUE INDEX r_knownissue_alt ON ampere.r_issueknown USING btree (issuesummary, releaseno, sourceclassname, sourcemethodname, loggername, lineno);


--
-- Name: r_request_bpartner; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX r_request_bpartner ON ampere.r_request USING btree (c_bpartner_id);


--
-- Name: r_request_user; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX r_request_user ON ampere.r_request USING btree (ad_user_id);


--
-- Name: t_cashflow_speed; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX t_cashflow_speed ON ampere.t_cashflow USING btree (ad_pinstance_id, c_cashplanline_id, cashflowsource);


--
-- Name: trec_matchcode; Type: INDEX; Schema: ampere; Owner: -
--

CREATE INDEX trec_matchcode ON ampere.t_reconciliation USING btree (matchcode);


--
-- Name: m_inout_candidate_v _RETURN; Type: RULE; Schema: ampere; Owner: -
--

CREATE OR REPLACE VIEW ampere.m_inout_candidate_v AS
 SELECT o.ad_client_id,
    o.ad_org_id,
    o.c_bpartner_id,
    o.c_order_id,
    o.documentno,
    o.dateordered,
    o.c_doctype_id,
    o.poreference,
    o.description,
    o.salesrep_id,
    l.m_warehouse_id,
    sum(((l.qtyordered - l.qtydelivered) * l.priceactual)) AS totallines,
    min(
        CASE
            WHEN (l.c_charge_id IS NOT NULL) THEN 'Y'::text
            WHEN (((s.qtyonhand - l.qtyordered) + l.qtydelivered) >= (0)::numeric) THEN 'Y'::text
            ELSE 'N'::text
        END) AS isgoodtoship,
    o.datepromised
   FROM ((ampere.c_order o
     JOIN ampere.c_orderline l ON ((o.c_order_id = l.c_order_id)))
     LEFT JOIN ( SELECT lc.m_warehouse_id,
            st.m_product_id,
            sum(st.qtyonhand) AS qtyonhand
           FROM (ampere.m_storage st
             JOIN ampere.m_locator lc ON ((st.m_locator_id = lc.m_locator_id)))
          GROUP BY lc.m_warehouse_id, st.m_product_id) s ON (((s.m_product_id = l.m_product_id) AND (o.m_warehouse_id = s.m_warehouse_id))))
  WHERE (((o.docstatus)::bpchar = 'CO'::bpchar) AND (o.isdelivered = 'N'::bpchar) AND (o.c_doctype_id IN ( SELECT c_doctype.c_doctype_id
           FROM ampere.c_doctype
          WHERE ((c_doctype.docbasetype = 'SOO'::bpchar) AND (c_doctype.docsubtypeso <> ALL (ARRAY['ON'::bpchar, 'OB'::bpchar, 'WR'::bpchar]))))) AND (o.deliveryrule <> 'M'::bpchar) AND ((l.m_product_id IS NULL) OR (EXISTS ( SELECT 1
           FROM ampere.m_product p
          WHERE ((l.m_product_id = p.m_product_id) AND (p.isexcludeautodelivery = 'N'::bpchar))))) AND (l.qtyordered <> l.qtydelivered) AND ((l.m_product_id IS NOT NULL) OR (l.c_charge_id IS NOT NULL)) AND (NOT (EXISTS ( SELECT 1
           FROM (ampere.m_inoutline iol
             JOIN ampere.m_inout io ON ((iol.m_inout_id = io.m_inout_id)))
          WHERE ((iol.c_orderline_id = l.c_orderline_id) AND (io.docstatus = ANY (ARRAY['IP'::bpchar, 'WC'::bpchar])))))))
  GROUP BY o.ad_client_id, o.ad_org_id, o.c_bpartner_id, o.c_order_id, o.documentno, o.dateordered, o.c_doctype_id, o.poreference, o.description, o.salesrep_id, l.m_warehouse_id;


--
-- Name: t_alter_column alter_column_rule; Type: RULE; Schema: ampere; Owner: -
--

CREATE RULE alter_column_rule AS
    ON INSERT TO ampere.t_alter_column DO INSTEAD  SELECT ampere.altercolumn(new.tablename, new.columnname, new.datatype, new.nullclause, new.defaultclause) AS altercolumn;


--
-- Name: a_asset_change aaaddition_aachange; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_change
    ADD CONSTRAINT aaaddition_aachange FOREIGN KEY (a_asset_addition_id) REFERENCES ampere.a_asset_addition(a_asset_addition_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_change aaretirement_aachange; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_change
    ADD CONSTRAINT aaretirement_aachange FOREIGN KEY (a_asset_retirement_id) REFERENCES ampere.a_asset_retirement(a_asset_retirement_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_acct aasset_aassetacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_acct
    ADD CONSTRAINT aasset_aassetacct FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_addition aasset_aassetaddition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_addition
    ADD CONSTRAINT aasset_aassetaddition FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_change aasset_aassetchange; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_change
    ADD CONSTRAINT aasset_aassetchange FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_delivery aasset_aassetdelivery; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_delivery
    ADD CONSTRAINT aasset_aassetdelivery FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_retirement aasset_aassetretirement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_retirement
    ADD CONSTRAINT aasset_aassetretirement FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_use aasset_aassetuse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_use
    ADD CONSTRAINT aasset_aassetuse FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_issue aasset_adissue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_issue
    ADD CONSTRAINT aasset_adissue FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_registration aasset_aregistration; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_registration
    ADD CONSTRAINT aasset_aregistration FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline aasset_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT aasset_cinvoiceline FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct aasset_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT aasset_factacct FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journalline aasset_gljournalline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalline
    ADD CONSTRAINT aasset_gljournalline FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_operationresource aasset_moperationresource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_operationresource
    ADD CONSTRAINT aasset_moperationresource FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_issueproject aasset_rissueproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issueproject
    ADD CONSTRAINT aasset_rissueproject FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_issuesystem aasset_rissuesystem; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issuesystem
    ADD CONSTRAINT aasset_rissuesystem FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request aasset_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT aasset_rrequest FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction aasset_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT aasset_rrequestaction FOREIGN KEY (a_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_change_amt aassetchange_aassetchangeamt; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_change_amt
    ADD CONSTRAINT aassetchange_aassetchangeamt FOREIGN KEY (a_asset_change_id) REFERENCES ampere.a_asset_change(a_asset_change_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset aassetgroup_aasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset
    ADD CONSTRAINT aassetgroup_aasset FOREIGN KEY (a_asset_group_id) REFERENCES ampere.a_asset_group(a_asset_group_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_group_acct aassetgroup_aassetgroupacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_group_acct
    ADD CONSTRAINT aassetgroup_aassetgroupacct FOREIGN KEY (a_asset_group_id) REFERENCES ampere.a_asset_group(a_asset_group_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline aassetgroup_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT aassetgroup_cinvoiceline FOREIGN KEY (a_asset_group_id) REFERENCES ampere.a_asset_group(a_asset_group_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journalline aassetgroup_gljournalline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalline
    ADD CONSTRAINT aassetgroup_gljournalline FOREIGN KEY (a_asset_group_id) REFERENCES ampere.a_asset_group(a_asset_group_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_asset aassetgroup_iasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_asset
    ADD CONSTRAINT aassetgroup_iasset FOREIGN KEY (a_asset_group_id) REFERENCES ampere.a_asset_group(a_asset_group_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category aassetgroup_mproductcategory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category
    ADD CONSTRAINT aassetgroup_mproductcategory FOREIGN KEY (a_asset_group_id) REFERENCES ampere.a_asset_group(a_asset_group_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_acct aassetspread_aassetacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_acct
    ADD CONSTRAINT aassetspread_aassetacct FOREIGN KEY (a_asset_spread_id) REFERENCES ampere.a_asset_spread(a_asset_spread_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_disposed aassettrade_aassetdisposed; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_disposed
    ADD CONSTRAINT aassettrade_aassetdisposed FOREIGN KEY (a_asset_trade_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct ac_client_fact_acct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT ac_client_fact_acct FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary account_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT account_factacctsummary FOREIGN KEY (account_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal account_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT account_ifajournal FOREIGN KEY (account_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema ad_client_c_acctschema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema
    ADD CONSTRAINT ad_client_c_acctschema FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_tab ad_column_ad_tab; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab
    ADD CONSTRAINT ad_column_ad_tab FOREIGN KEY (ad_column_id) REFERENCES ampere.ad_column(ad_column_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_field ad_column_field; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_field
    ADD CONSTRAINT ad_column_field FOREIGN KEY (ad_column_id) REFERENCES ampere.ad_column(ad_column_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_ref_table ad_column_reftable_display; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_table
    ADD CONSTRAINT ad_column_reftable_display FOREIGN KEY (ad_display) REFERENCES ampere.ad_column(ad_column_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_ref_table ad_column_reftable_id; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_table
    ADD CONSTRAINT ad_column_reftable_id FOREIGN KEY (ad_key) REFERENCES ampere.ad_column(ad_column_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_table_access ad_dataaccessclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table_access
    ADD CONSTRAINT ad_dataaccessclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_table_access ad_dataaccessorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table_access
    ADD CONSTRAINT ad_dataaccessorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_column ad_element_ad_column; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column
    ADD CONSTRAINT ad_element_ad_column FOREIGN KEY (ad_element_id) REFERENCES ampere.ad_element(ad_element_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_field_trl ad_fieldtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_field_trl
    ADD CONSTRAINT ad_fieldtrl FOREIGN KEY (ad_field_id) REFERENCES ampere.ad_field(ad_field_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_window_access ad_functaccess_client; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_window_access
    ADD CONSTRAINT ad_functaccess_client FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_window_access ad_functaccessorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_window_access
    ADD CONSTRAINT ad_functaccessorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_element_trl ad_language_ad_element_trl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_element_trl
    ADD CONSTRAINT ad_language_ad_element_trl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process_trl ad_language_ad_process_trl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_trl
    ADD CONSTRAINT ad_language_ad_process_trl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner ad_language_c_buspartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT ad_language_c_buspartner FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_field_trl ad_language_fieldtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_field_trl
    ADD CONSTRAINT ad_language_fieldtrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_menu_trl ad_language_menutrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_menu_trl
    ADD CONSTRAINT ad_language_menutrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_message_trl ad_language_messagetrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_message_trl
    ADD CONSTRAINT ad_language_messagetrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_reference_trl ad_language_referencetrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_reference_trl
    ADD CONSTRAINT ad_language_referencetrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_ref_list_trl ad_language_reflisttrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_list_trl
    ADD CONSTRAINT ad_language_reflisttrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_tab_trl ad_language_tabtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab_trl
    ADD CONSTRAINT ad_language_tabtrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_window_trl ad_language_windowtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_window_trl
    ADD CONSTRAINT ad_language_windowtrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_workflow_trl ad_language_workflowtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_workflow_trl
    ADD CONSTRAINT ad_language_workflowtrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_menu ad_menu_org; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_menu
    ADD CONSTRAINT ad_menu_org FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_menu_trl ad_menutrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_menu_trl
    ADD CONSTRAINT ad_menutrl FOREIGN KEY (ad_menu_id) REFERENCES ampere.ad_menu(ad_menu_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_message_trl ad_messagetrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_message_trl
    ADD CONSTRAINT ad_messagetrl FOREIGN KEY (ad_message_id) REFERENCES ampere.ad_message(ad_message_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema ad_org_c_acctschema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema
    ADD CONSTRAINT ad_org_c_acctschema FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct ad_org_fact_acct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT ad_org_fact_acct FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product ad_org_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT ad_org_mproduct FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct ad_orgtrx_fact_acct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT ad_orgtrx_fact_acct FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_trialbalance ad_pinstance_t_trialbalance; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_trialbalance
    ADD CONSTRAINT ad_pinstance_t_trialbalance FOREIGN KEY (ad_pinstance_id) REFERENCES ampere.ad_pinstance(ad_pinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_preference ad_preference_client; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_preference
    ADD CONSTRAINT ad_preference_client FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_preference ad_preference_org; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_preference
    ADD CONSTRAINT ad_preference_org FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printformat ad_printfont_adprintformat; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformat
    ADD CONSTRAINT ad_printfont_adprintformat FOREIGN KEY (ad_printfont_id) REFERENCES ampere.ad_printfont(ad_printfont_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process_trl ad_process_ad_process_trl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_trl
    ADD CONSTRAINT ad_process_ad_process_trl FOREIGN KEY (ad_process_id) REFERENCES ampere.ad_process(ad_process_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process_access ad_processaccess_client; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_access
    ADD CONSTRAINT ad_processaccess_client FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process_access ad_processtaccess_org; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_access
    ADD CONSTRAINT ad_processtaccess_org FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_column ad_reference_columndatatype; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column
    ADD CONSTRAINT ad_reference_columndatatype FOREIGN KEY (ad_reference_id) REFERENCES ampere.ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_column ad_reference_columnvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column
    ADD CONSTRAINT ad_reference_columnvalue FOREIGN KEY (ad_reference_value_id) REFERENCES ampere.ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_ref_list ad_reference_reflist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_list
    ADD CONSTRAINT ad_reference_reflist FOREIGN KEY (ad_reference_id) REFERENCES ampere.ad_reference(ad_reference_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_ref_table ad_reference_reftable; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_table
    ADD CONSTRAINT ad_reference_reftable FOREIGN KEY (ad_reference_id) REFERENCES ampere.ad_reference(ad_reference_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_reference_trl ad_referencetrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_reference_trl
    ADD CONSTRAINT ad_referencetrl FOREIGN KEY (ad_reference_id) REFERENCES ampere.ad_reference(ad_reference_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_ref_list ad_reflist_client; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_list
    ADD CONSTRAINT ad_reflist_client FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_ref_list ad_reflist_org; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_list
    ADD CONSTRAINT ad_reflist_org FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_ref_list_trl ad_reflisttrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_list_trl
    ADD CONSTRAINT ad_reflisttrl FOREIGN KEY (ad_ref_list_id) REFERENCES ampere.ad_ref_list(ad_ref_list_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_role_included ad_role_included_parent; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_role_included
    ADD CONSTRAINT ad_role_included_parent FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) ON DELETE CASCADE;


--
-- Name: ad_role_included ad_role_included_role; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_role_included
    ADD CONSTRAINT ad_role_included_role FOREIGN KEY (included_role_id) REFERENCES ampere.ad_role(ad_role_id);


--
-- Name: ad_role ad_roleclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_role
    ADD CONSTRAINT ad_roleclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_role ad_roleorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_role
    ADD CONSTRAINT ad_roleorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_doctype ad_sequence_doctypedoc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_doctype
    ADD CONSTRAINT ad_sequence_doctypedoc FOREIGN KEY (docnosequence_id) REFERENCES ampere.ad_sequence(ad_sequence_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_sequence_audit ad_sequence_sequenceaudit; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_sequence_audit
    ADD CONSTRAINT ad_sequence_sequenceaudit FOREIGN KEY (ad_sequence_id) REFERENCES ampere.ad_sequence(ad_sequence_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_sequence_no ad_sequence_sequenceno; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_sequence_no
    ADD CONSTRAINT ad_sequence_sequenceno FOREIGN KEY (ad_sequence_id) REFERENCES ampere.ad_sequence(ad_sequence_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_field ad_tab_field; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_field
    ADD CONSTRAINT ad_tab_field FOREIGN KEY (ad_tab_id) REFERENCES ampere.ad_tab(ad_tab_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_column ad_table_column; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column
    ADD CONSTRAINT ad_table_column FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_tab ad_table_tab; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab
    ADD CONSTRAINT ad_table_tab FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_tab_trl ad_tabtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab_trl
    ADD CONSTRAINT ad_tabtrl FOREIGN KEY (ad_tab_id) REFERENCES ampere.ad_tab(ad_tab_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user ad_user_client; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user
    ADD CONSTRAINT ad_user_client FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user ad_user_org; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user
    ADD CONSTRAINT ad_user_org FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_preference ad_user_preference; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_preference
    ADD CONSTRAINT ad_user_preference FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user_roles ad_userrolesclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user_roles
    ADD CONSTRAINT ad_userrolesclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user_roles ad_userrolesorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user_roles
    ADD CONSTRAINT ad_userrolesorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_column ad_valrule_column; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column
    ADD CONSTRAINT ad_valrule_column FOREIGN KEY (ad_val_rule_id) REFERENCES ampere.ad_val_rule(ad_val_rule_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_table ad_valrule_table; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table
    ADD CONSTRAINT ad_valrule_table FOREIGN KEY (ad_val_rule_id) REFERENCES ampere.ad_val_rule(ad_val_rule_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_preference ad_window_preference; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_preference
    ADD CONSTRAINT ad_window_preference FOREIGN KEY (ad_window_id) REFERENCES ampere.ad_window(ad_window_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_tab ad_window_tab; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab
    ADD CONSTRAINT ad_window_tab FOREIGN KEY (ad_window_id) REFERENCES ampere.ad_window(ad_window_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_table ad_window_table; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table
    ADD CONSTRAINT ad_window_table FOREIGN KEY (ad_window_id) REFERENCES ampere.ad_window(ad_window_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_window_trl ad_windowtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_window_trl
    ADD CONSTRAINT ad_windowtrl FOREIGN KEY (ad_window_id) REFERENCES ampere.ad_window(ad_window_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_workflow_access ad_workflowaccess_client; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_workflow_access
    ADD CONSTRAINT ad_workflowaccess_client FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_workflow_access ad_workflowaccess_org; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_workflow_access
    ADD CONSTRAINT ad_workflowaccess_org FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_workflow_trl ad_workflowtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_workflow_trl
    ADD CONSTRAINT ad_workflowtrl FOREIGN KEY (ad_workflow_id) REFERENCES ampere.ad_workflow(ad_workflow_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_alertrecipient adalert_adalertrecipient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_alertrecipient
    ADD CONSTRAINT adalert_adalertrecipient FOREIGN KEY (ad_alert_id) REFERENCES ampere.ad_alert(ad_alert_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_alertrule adaltert_aralertrule; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_alertrule
    ADD CONSTRAINT adaltert_aralertrule FOREIGN KEY (ad_alert_id) REFERENCES ampere.ad_alert(ad_alert_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_attachmententry adattachment_entry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_attachmententry
    ADD CONSTRAINT adattachment_entry FOREIGN KEY (ad_attachment_id) REFERENCES ampere.ad_attachment(ad_attachment_id);


--
-- Name: ad_attachmentnote adattachment_note; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_attachmentnote
    ADD CONSTRAINT adattachment_note FOREIGN KEY (ad_attachment_id) REFERENCES ampere.ad_attachment(ad_attachment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_attribute_value adattribute_adattributevalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_attribute_value
    ADD CONSTRAINT adattribute_adattributevalue FOREIGN KEY (ad_attribute_id) REFERENCES ampere.ad_attribute(ad_attribute_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo adclient_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT adclient_adclientinfo FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientshare adclient_adclientshare; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientshare
    ADD CONSTRAINT adclient_adclientshare FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_menu adclient_admenu; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_menu
    ADD CONSTRAINT adclient_admenu FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_org adclient_adorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_org
    ADD CONSTRAINT adclient_adorg FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printform adclient_adprintform; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT adclient_adprintform FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_element adclient_caschemaelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_element
    ADD CONSTRAINT adclient_caschemaelement FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner adclient_cbpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT adclient_cbpartner FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_element adclient_celement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_element
    ADD CONSTRAINT adclient_celement FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_elementvalue adclient_celementvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_elementvalue
    ADD CONSTRAINT adclient_celementvalue FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_location adclient_clocation; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_location
    ADD CONSTRAINT adclient_clocation FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_ordersource adclient_cordersource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_ordersource
    ADD CONSTRAINT adclient_cordersource FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id);


--
-- Name: c_project adclient_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project
    ADD CONSTRAINT adclient_cproject FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journalline adclient_gljournalline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalline
    ADD CONSTRAINT adclient_gljournalline FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_cost adclient_mcost; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_cost
    ADD CONSTRAINT adclient_mcost FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product adclient_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT adclient_mproduct FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination adclient_vc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT adclient_vc FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_window adcolor_adwindow; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_window
    ADD CONSTRAINT adcolor_adwindow FOREIGN KEY (ad_color_id) REFERENCES ampere.ad_color(ad_color_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_accesslog adcolumn_adaccesslog; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_accesslog
    ADD CONSTRAINT adcolumn_adaccesslog FOREIGN KEY (ad_column_id) REFERENCES ampere.ad_column(ad_column_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_column_access adcolumn_adcolumnaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column_access
    ADD CONSTRAINT adcolumn_adcolumnaccess FOREIGN KEY (ad_column_id) REFERENCES ampere.ad_column(ad_column_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_column_trl adcolumn_adcolumntrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column_trl
    ADD CONSTRAINT adcolumn_adcolumntrl FOREIGN KEY (ad_column_id) REFERENCES ampere.ad_column(ad_column_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_find adcolumn_adfind; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_find
    ADD CONSTRAINT adcolumn_adfind FOREIGN KEY (ad_column_id) REFERENCES ampere.ad_column(ad_column_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_impformat_row adcolumn_adimpformatrow; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_impformat_row
    ADD CONSTRAINT adcolumn_adimpformatrow FOREIGN KEY (ad_column_id) REFERENCES ampere.ad_column(ad_column_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_imp_backup adcolumn_adpackageimpbackup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_imp_backup
    ADD CONSTRAINT adcolumn_adpackageimpbackup FOREIGN KEY (ad_column_id) REFERENCES ampere.ad_column(ad_column_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printformatitem adcolumn_adprintformatitem; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformatitem
    ADD CONSTRAINT adcolumn_adprintformatitem FOREIGN KEY (ad_column_id) REFERENCES ampere.ad_column(ad_column_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_reportview_col adcolumn_adreportviewcol; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_reportview_col
    ADD CONSTRAINT adcolumn_adreportviewcol FOREIGN KEY (ad_column_id) REFERENCES ampere.ad_column(ad_column_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_searchdefinition adcolumn_adsearchdefinition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_searchdefinition
    ADD CONSTRAINT adcolumn_adsearchdefinition FOREIGN KEY (ad_column_id) REFERENCES ampere.ad_column(ad_column_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_tab adcolumn_adtabsortorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab
    ADD CONSTRAINT adcolumn_adtabsortorder FOREIGN KEY (ad_columnsortorder_id) REFERENCES ampere.ad_column(ad_column_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_tab adcolumn_adtabsortyesno; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab
    ADD CONSTRAINT adcolumn_adtabsortyesno FOREIGN KEY (ad_columnsortyesno_id) REFERENCES ampere.ad_column(ad_column_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_element adcolumn_cacctschemaelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_element
    ADD CONSTRAINT adcolumn_cacctschemaelement FOREIGN KEY (ad_column_id) REFERENCES ampere.ad_column(ad_column_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_elementvalue adcolumn_ielementvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_elementvalue
    ADD CONSTRAINT adcolumn_ielementvalue FOREIGN KEY (ad_column_id) REFERENCES ampere.ad_column(ad_column_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_ref_table add_table_reftable; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_table
    ADD CONSTRAINT add_table_reftable FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_element_trl adelement_adelementtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_element_trl
    ADD CONSTRAINT adelement_adelementtrl FOREIGN KEY (ad_element_id) REFERENCES ampere.ad_element(ad_element_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_infocolumn adelement_adinfocolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_infocolumn
    ADD CONSTRAINT adelement_adinfocolumn FOREIGN KEY (ad_element_id) REFERENCES ampere.ad_element(ad_element_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process_para adelement_adprocesspara; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_para
    ADD CONSTRAINT adelement_adprocesspara FOREIGN KEY (ad_element_id) REFERENCES ampere.ad_element(ad_element_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_modification adenritytype_admodification; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_modification
    ADD CONSTRAINT adenritytype_admodification FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_acct adepreciation_aassetacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_acct
    ADD CONSTRAINT adepreciation_aassetacct FOREIGN KEY (a_depreciation_id) REFERENCES ampere.a_depreciation(a_depreciation_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_group_acct adepreciation_aassetgroupacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_group_acct
    ADD CONSTRAINT adepreciation_aassetgroupacct FOREIGN KEY (a_depreciation_id) REFERENCES ampere.a_depreciation(a_depreciation_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_acct adepreciationconv_aassetacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_acct
    ADD CONSTRAINT adepreciationconv_aassetacct FOREIGN KEY (a_depreciation_conv_id) REFERENCES ampere.a_depreciation_convention(a_depreciation_convention_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_acct adepreciationmethod_aassetacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_acct
    ADD CONSTRAINT adepreciationmethod_aassetacct FOREIGN KEY (a_depreciation_method_id) REFERENCES ampere.a_depreciation_method(a_depreciation_method_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_change adepreciationtableheader_aass2; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_change
    ADD CONSTRAINT adepreciationtableheader_aass2 FOREIGN KEY (a_depreciation_table_header_id) REFERENCES ampere.a_depreciation_table_header(a_depreciation_table_header_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_group_acct adepreciationtableheader_aass3; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_group_acct
    ADD CONSTRAINT adepreciationtableheader_aass3 FOREIGN KEY (a_depreciation_table_header_id) REFERENCES ampere.a_depreciation_table_header(a_depreciation_table_header_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_acct adepreciationtableheader_aasse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_acct
    ADD CONSTRAINT adepreciationtableheader_aasse FOREIGN KEY (a_depreciation_table_header_id) REFERENCES ampere.a_depreciation_table_header(a_depreciation_table_header_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_asset adepreciationtableheader_iasse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_asset
    ADD CONSTRAINT adepreciationtableheader_iasse FOREIGN KEY (a_depreciation_table_header_id) REFERENCES ampere.a_depreciation_table_header(a_depreciation_table_header_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_userdef_field adfield_aduserdeffield; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userdef_field
    ADD CONSTRAINT adfield_aduserdeffield FOREIGN KEY (ad_field_id) REFERENCES ampere.ad_field(ad_field_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_field adfieldgroup_adfield; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_field
    ADD CONSTRAINT adfieldgroup_adfield FOREIGN KEY (ad_fieldgroup_id) REFERENCES ampere.ad_fieldgroup(ad_fieldgroup_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_fieldgroup_trl adfieldgroup_trl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_fieldgroup_trl
    ADD CONSTRAINT adfieldgroup_trl FOREIGN KEY (ad_fieldgroup_id) REFERENCES ampere.ad_fieldgroup(ad_fieldgroup_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_form_access adform_adformaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_form_access
    ADD CONSTRAINT adform_adformaccess FOREIGN KEY (ad_form_id) REFERENCES ampere.ad_form(ad_form_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_form_trl adform_adformtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_form_trl
    ADD CONSTRAINT adform_adformtrl FOREIGN KEY (ad_form_id) REFERENCES ampere.ad_form(ad_form_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_issue adform_adissue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_issue
    ADD CONSTRAINT adform_adissue FOREIGN KEY (ad_form_id) REFERENCES ampere.ad_form(ad_form_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_menu adform_admenu; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_menu
    ADD CONSTRAINT adform_admenu FOREIGN KEY (ad_form_id) REFERENCES ampere.ad_form(ad_form_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_common adform_adpackageexpcommon; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_common
    ADD CONSTRAINT adform_adpackageexpcommon FOREIGN KEY (ad_form_id) REFERENCES ampere.ad_form(ad_form_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_detail adform_adpackageexpdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_detail
    ADD CONSTRAINT adform_adpackageexpdetail FOREIGN KEY (ad_form_id) REFERENCES ampere.ad_form(ad_form_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process adform_adprocess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process
    ADD CONSTRAINT adform_adprocess FOREIGN KEY (ad_form_id) REFERENCES ampere.ad_form(ad_form_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_color adimage_adcolor; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_color
    ADD CONSTRAINT adimage_adcolor FOREIGN KEY (ad_image_id) REFERENCES ampere.ad_image(ad_image_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printtableformat adimage_adprinttableformat; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printtableformat
    ADD CONSTRAINT adimage_adprinttableformat FOREIGN KEY (ad_image_id) REFERENCES ampere.ad_image(ad_image_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_tab adimage_adtab; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab
    ADD CONSTRAINT adimage_adtab FOREIGN KEY (ad_image_id) REFERENCES ampere.ad_image(ad_image_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_window adimage_adwindow; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_window
    ADD CONSTRAINT adimage_adwindow FOREIGN KEY (ad_image_id) REFERENCES ampere.ad_image(ad_image_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_impformat_row adimpformat_adimpformatrow; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_impformat_row
    ADD CONSTRAINT adimpformat_adimpformatrow FOREIGN KEY (ad_impformat_id) REFERENCES ampere.ad_impformat(ad_impformat_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_common adimpformat_adpackageexpcommon; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_common
    ADD CONSTRAINT adimpformat_adpackageexpcommon FOREIGN KEY (ad_impformat_id) REFERENCES ampere.ad_impformat(ad_impformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_detail adimpformat_adpackageexpdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_detail
    ADD CONSTRAINT adimpformat_adpackageexpdetail FOREIGN KEY (ad_impformat_id) REFERENCES ampere.ad_impformat(ad_impformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_infocolumn_trl adinfocolumn_adinfocolumntrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_infocolumn_trl
    ADD CONSTRAINT adinfocolumn_adinfocolumntrl FOREIGN KEY (ad_infocolumn_id) REFERENCES ampere.ad_infocolumn(ad_infocolumn_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_infocolumn adinfowindow_adinfocolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_infocolumn
    ADD CONSTRAINT adinfowindow_adinfocolumn FOREIGN KEY (ad_infowindow_id) REFERENCES ampere.ad_infowindow(ad_infowindow_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_infowindow_trl adinfowindow_adinfowindowtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_infowindow_trl
    ADD CONSTRAINT adinfowindow_adinfowindowtrl FOREIGN KEY (ad_infowindow_id) REFERENCES ampere.ad_infowindow(ad_infowindow_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_client adlangu_adclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_client
    ADD CONSTRAINT adlangu_adclient FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_error adlangu_aderror; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_error
    ADD CONSTRAINT adlangu_aderror FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_userdef_win adlangu_aduserdefwin; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userdef_win
    ADD CONSTRAINT adlangu_aduserdefwin FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_charge_trl adlangu_cchargetrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_charge_trl
    ADD CONSTRAINT adlangu_cchargetrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_dashboardcontent_trl adlangu_padashboardcontenttrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_dashboardcontent_trl
    ADD CONSTRAINT adlangu_padashboardcontenttrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_column_trl adlanguage_adcolumntrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column_trl
    ADD CONSTRAINT adlanguage_adcolumntrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_fieldgroup_trl adlanguage_adfieldgrouptrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_fieldgroup_trl
    ADD CONSTRAINT adlanguage_adfieldgrouptrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_form_trl adlanguage_adformtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_form_trl
    ADD CONSTRAINT adlanguage_adformtrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_infocolumn_trl adlanguage_adinfocolumntrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_infocolumn_trl
    ADD CONSTRAINT adlanguage_adinfocolumntrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_infowindow_trl adlanguage_adinfowindowtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_infowindow_trl
    ADD CONSTRAINT adlanguage_adinfowindowtrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printformatitem_trl adlanguage_adprintformitemtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformatitem_trl
    ADD CONSTRAINT adlanguage_adprintformitemtrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process_para_trl adlanguage_adprocessparatrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_para_trl
    ADD CONSTRAINT adlanguage_adprocessparatrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_table_trl adlanguage_adtabletrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table_trl
    ADD CONSTRAINT adlanguage_adtabletrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_country adlanguage_ccountry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_country
    ADD CONSTRAINT adlanguage_ccountry FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_country_trl adlanguage_ccountrytrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_country_trl
    ADD CONSTRAINT adlanguage_ccountrytrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_currency_trl adlanguage_ccurrencytrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_currency_trl
    ADD CONSTRAINT adlanguage_ccurrencytrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_doctype_trl adlanguage_cdoctypetrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_doctype_trl
    ADD CONSTRAINT adlanguage_cdoctypetrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunninglevel_trl adlanguage_cdunninglevel; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunninglevel_trl
    ADD CONSTRAINT adlanguage_cdunninglevel FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_elementvalue_trl adlanguage_celementvaluetrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_elementvalue_trl
    ADD CONSTRAINT adlanguage_celementvaluetrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_greeting_trl adlanguage_cgreetingtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_greeting_trl
    ADD CONSTRAINT adlanguage_cgreetingtrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_paymentterm_trl adlanguage_cpaymenttermtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_paymentterm_trl
    ADD CONSTRAINT adlanguage_cpaymenttermtrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxcategory_trl adlanguage_ctaxcategorytrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxcategory_trl
    ADD CONSTRAINT adlanguage_ctaxcategorytrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax_trl adlanguage_ctaxtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax_trl
    ADD CONSTRAINT adlanguage_ctaxtrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_uom_trl adlanguage_cuomtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_uom_trl
    ADD CONSTRAINT adlanguage_cuomtrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_trl adlanguage_mproducttrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_trl
    ADD CONSTRAINT adlanguage_mproducttrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_mailtext_trl adlanguage_rmailtexttrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_mailtext_trl
    ADD CONSTRAINT adlanguage_rmailtexttrl FOREIGN KEY (ad_language) REFERENCES ampere.ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_common admenu_adpackageexpcommon; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_common
    ADD CONSTRAINT admenu_adpackageexpcommon FOREIGN KEY (ad_menu_id) REFERENCES ampere.ad_menu(ad_menu_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_detail admenu_adpackageexpdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_detail
    ADD CONSTRAINT admenu_adpackageexpdetail FOREIGN KEY (ad_menu_id) REFERENCES ampere.ad_menu(ad_menu_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_note admessage_adnote; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_note
    ADD CONSTRAINT admessage_adnote FOREIGN KEY (ad_message_id) REFERENCES ampere.ad_message(ad_message_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_detail admessage_adpackageexpdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_detail
    ADD CONSTRAINT admessage_adpackageexpdetail FOREIGN KEY (ad_message_id) REFERENCES ampere.ad_message(ad_message_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_wf_process admessage_adwfprocess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_wf_process
    ADD CONSTRAINT admessage_adwfprocess FOREIGN KEY (ad_message_id) REFERENCES ampere.ad_message(ad_message_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_detail admodval_adpackageexpdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_detail
    ADD CONSTRAINT admodval_adpackageexpdetail FOREIGN KEY (ad_modelvalidator_id) REFERENCES ampere.ad_modelvalidator(ad_modelvalidator_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_goalrestriction adorg2_pagoalrestriction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_goalrestriction
    ADD CONSTRAINT adorg2_pagoalrestriction FOREIGN KEY (org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientshare adorg_adclientshare; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientshare
    ADD CONSTRAINT adorg_adclientshare FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_orginfo adorg_adorginfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_orginfo
    ADD CONSTRAINT adorg_adorginfo FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_role_orgaccess adorg_adroleorgaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_role_orgaccess
    ADD CONSTRAINT adorg_adroleorgaccess FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user_orgaccess adorg_aduserorgaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user_orgaccess
    ADD CONSTRAINT adorg_aduserorgaccess FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_element adorg_caschemaelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_element
    ADD CONSTRAINT adorg_caschemaelement FOREIGN KEY (org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner adorg_cbpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT adorg_cbpartner FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner adorg_cbpartnerorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT adorg_cbpartnerorg FOREIGN KEY (ad_orgbp_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cash adorg_ccash; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cash
    ADD CONSTRAINT adorg_ccash FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_elementvalue adorg_celementvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_elementvalue
    ADD CONSTRAINT adorg_celementvalue FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_interorg_acct adorg_cinterorgacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_interorg_acct
    ADD CONSTRAINT adorg_cinterorgacct FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice adorg_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT adorg_cinvoice FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatchline adorg_cinvoicebatchline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT adorg_cinvoicebatchline FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline adorg_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT adorg_cinvoiceline FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_location adorg_clocation; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_location
    ADD CONSTRAINT adorg_clocation FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order adorg_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT adorg_corder FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline adorg_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT adorg_corderline FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_ordersource adorg_cordersource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_ordersource
    ADD CONSTRAINT adorg_cordersource FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id);


--
-- Name: c_orgassignment adorg_corgassignment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orgassignment
    ADD CONSTRAINT adorg_corgassignment FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment adorg_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT adorg_cpayment FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project adorg_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project
    ADD CONSTRAINT adorg_cproject FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution adorg_gldist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT adorg_gldist FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distributionline adorg_gldistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT adorg_gldistline FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journalline adorg_gljournalline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalline
    ADD CONSTRAINT adorg_gljournalline FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal adorg_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT adorg_igljournal FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice adorg_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT adorg_iinvoice FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order adorg_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT adorg_iorder FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_cost adorg_m_cost; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_cost
    ADD CONSTRAINT adorg_m_cost FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout adorg_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT adorg_minout FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline adorg_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT adorg_minoutline FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventory adorg_minventory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventory
    ADD CONSTRAINT adorg_minventory FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movement adorg_mmovement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT adorg_mmovement FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_production adorg_mproduction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_production
    ADD CONSTRAINT adorg_mproduction FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_goalrestriction adorg_pagoalrestriction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_goalrestriction
    ADD CONSTRAINT adorg_pagoalrestriction FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_report adorg_pareport; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_report
    ADD CONSTRAINT adorg_pareport FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcolumn adorg_pareportcolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT adorg_pareportcolumn FOREIGN KEY (org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportsource adorg_pareportsource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportsource
    ADD CONSTRAINT adorg_pareportsource FOREIGN KEY (org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination adorg_vc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT adorg_vc FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal adorgdoc_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT adorgdoc_ifajournal FOREIGN KEY (ad_orgdoc_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal adorgdoc_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT adorgdoc_igljournal FOREIGN KEY (ad_orgdoc_id) REFERENCES ampere.ad_org(ad_org_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_element adorgid_c_aschemaelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_element
    ADD CONSTRAINT adorgid_c_aschemaelement FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema adorgonly_cacctschema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema
    ADD CONSTRAINT adorgonly_cacctschema FOREIGN KEY (ad_orgonly_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution adorgorg_gldist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT adorgorg_gldist FOREIGN KEY (org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distributionline adorgorg_gldistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT adorgorg_gldistline FOREIGN KEY (org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_orginfo adorgparent_adorginfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_orginfo
    ADD CONSTRAINT adorgparent_adorginfo FOREIGN KEY (parent_org_id) REFERENCES ampere.ad_org(ad_org_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_interorg_acct adorgto_cinterorgacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_interorg_acct
    ADD CONSTRAINT adorgto_cinterorgacct FOREIGN KEY (ad_orgto_id) REFERENCES ampere.ad_org(ad_org_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user adorgtrx_aduser; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user
    ADD CONSTRAINT adorgtrx_aduser FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cash adorgtrx_ccash; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cash
    ADD CONSTRAINT adorgtrx_ccash FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplan adorgtrx_ccashplan; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplan
    ADD CONSTRAINT adorgtrx_ccashplan FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplanline adorgtrx_ccashplanline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplanline
    ADD CONSTRAINT adorgtrx_ccashplanline FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commissionline adorgtrx_ccommissionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissionline
    ADD CONSTRAINT adorgtrx_ccommissionline FOREIGN KEY (org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice adorgtrx_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT adorgtrx_cinvoice FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatchline adorgtrx_cinvoicebatchline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT adorgtrx_cinvoicebatchline FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline adorgtrx_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT adorgtrx_cinvoiceline FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order adorgtrx_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT adorgtrx_corder FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline adorgtrx_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT adorgtrx_corderline FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment adorgtrx_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT adorgtrx_cpayment FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution adorgtrx_gldist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT adorgtrx_gldist FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distributionline adorgtrx_gldistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT adorgtrx_gldistline FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal adorgtrx_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT adorgtrx_ifajournal FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal adorgtrx_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT adorgtrx_igljournal FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice adorgtrx_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT adorgtrx_iinvoice FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order adorgtrx_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT adorgtrx_iorder FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout adorgtrx_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT adorgtrx_minout FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline adorgtrx_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT adorgtrx_minoutline FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventory adorgtrx_minventory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventory
    ADD CONSTRAINT adorgtrx_minventory FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movement adorgtrx_mmovement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT adorgtrx_mmovement FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_production adorgtrx_mproduction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_production
    ADD CONSTRAINT adorgtrx_mproduction FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcolumn adorgtrx_pareportcolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT adorgtrx_pareportcolumn FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportsource adorgtrx_pareportsource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportsource
    ADD CONSTRAINT adorgtrx_pareportsource FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination adorgtrx_vc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT adorgtrx_vc FOREIGN KEY (ad_orgtrx_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_orginfo adorgtype_adorginfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_orginfo
    ADD CONSTRAINT adorgtype_adorginfo FOREIGN KEY (ad_orgtype_id) REFERENCES ampere.ad_orgtype(ad_orgtype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdefinition adorgtype_ctaxdefinition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdefinition
    ADD CONSTRAINT adorgtype_ctaxdefinition FOREIGN KEY (ad_orgtype_id) REFERENCES ampere.ad_orgtype(ad_orgtype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_pinstance_para adpinstance_adpinstancepara; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_pinstance_para
    ADD CONSTRAINT adpinstance_adpinstancepara FOREIGN KEY (ad_pinstance_id) REFERENCES ampere.ad_pinstance(ad_pinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_pinstance_log adpinstance_pilog; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_pinstance_log
    ADD CONSTRAINT adpinstance_pilog FOREIGN KEY (ad_pinstance_id) REFERENCES ampere.ad_pinstance(ad_pinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_aging adpinstance_taging; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_aging
    ADD CONSTRAINT adpinstance_taging FOREIGN KEY (ad_pinstance_id) REFERENCES ampere.ad_pinstance(ad_pinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_inventoryvalue adpinstance_tinventoryvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_inventoryvalue
    ADD CONSTRAINT adpinstance_tinventoryvalue FOREIGN KEY (ad_pinstance_id) REFERENCES ampere.ad_pinstance(ad_pinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_invoicegl adpinstance_tinvoicegl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_invoicegl
    ADD CONSTRAINT adpinstance_tinvoicegl FOREIGN KEY (ad_pinstance_id) REFERENCES ampere.ad_pinstance(ad_pinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_replenish adpinstance_treplenish; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_replenish
    ADD CONSTRAINT adpinstance_treplenish FOREIGN KEY (ad_pinstance_id) REFERENCES ampere.ad_pinstance(ad_pinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_report adpinstance_treport; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_report
    ADD CONSTRAINT adpinstance_treport FOREIGN KEY (ad_pinstance_id) REFERENCES ampere.ad_pinstance(ad_pinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_reportstatement adpinstance_treportstatement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_reportstatement
    ADD CONSTRAINT adpinstance_treportstatement FOREIGN KEY (ad_pinstance_id) REFERENCES ampere.ad_pinstance(ad_pinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_transaction adpinstance_ttransaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_transaction
    ADD CONSTRAINT adpinstance_ttransaction FOREIGN KEY (ad_pinstance_id) REFERENCES ampere.ad_pinstance(ad_pinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_colorschema adprintcolor1_pacolorschema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_colorschema
    ADD CONSTRAINT adprintcolor1_pacolorschema FOREIGN KEY (ad_printcolor1_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_colorschema adprintcolor2_pacolorschema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_colorschema
    ADD CONSTRAINT adprintcolor2_pacolorschema FOREIGN KEY (ad_printcolor2_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_colorschema adprintcolor3_pacolorschema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_colorschema
    ADD CONSTRAINT adprintcolor3_pacolorschema FOREIGN KEY (ad_printcolor3_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_colorschema adprintcolor4_pacolorschema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_colorschema
    ADD CONSTRAINT adprintcolor4_pacolorschema FOREIGN KEY (ad_printcolor4_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_orgtype adprintcolor_adorgtype; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_orgtype
    ADD CONSTRAINT adprintcolor_adorgtype FOREIGN KEY (ad_printcolor_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printformat adprintcolor_adprintformat; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformat
    ADD CONSTRAINT adprintcolor_adprintformat FOREIGN KEY (ad_printcolor_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printformatitem adprintcolor_adprintformatitem; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformatitem
    ADD CONSTRAINT adprintcolor_adprintformatitem FOREIGN KEY (ad_printcolor_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group adprintcolor_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group
    ADD CONSTRAINT adprintcolor_cbpgroup FOREIGN KEY (ad_printcolor_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_channel adprintcolor_cchannel; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_channel
    ADD CONSTRAINT adprintcolor_cchannel FOREIGN KEY (ad_printcolor_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category adprintcolor_mproductcategory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category
    ADD CONSTRAINT adprintcolor_mproductcategory FOREIGN KEY (ad_printcolor_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printtableformat adprintcolor_tablefunctbg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printtableformat
    ADD CONSTRAINT adprintcolor_tablefunctbg FOREIGN KEY (functbg_printcolor_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printtableformat adprintcolor_tablefunctfg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printtableformat
    ADD CONSTRAINT adprintcolor_tablefunctfg FOREIGN KEY (functfg_printcolor_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printtableformat adprintcolor_tablehdrline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printtableformat
    ADD CONSTRAINT adprintcolor_tablehdrline FOREIGN KEY (hdrline_printcolor_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printtableformat adprintcolor_tablehdrtextbg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printtableformat
    ADD CONSTRAINT adprintcolor_tablehdrtextbg FOREIGN KEY (hdrtextbg_printcolor_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printtableformat adprintcolor_tablehdrtextfg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printtableformat
    ADD CONSTRAINT adprintcolor_tablehdrtextfg FOREIGN KEY (hdrtextfg_printcolor_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printtableformat adprintcolor_tableline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printtableformat
    ADD CONSTRAINT adprintcolor_tableline FOREIGN KEY (line_printcolor_id) REFERENCES ampere.ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printformatitem adprintfont_adprintformatitem; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformatitem
    ADD CONSTRAINT adprintfont_adprintformatitem FOREIGN KEY (ad_printfont_id) REFERENCES ampere.ad_printfont(ad_printfont_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printtableformat adprintfont_tableformatfunc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printtableformat
    ADD CONSTRAINT adprintfont_tableformatfunc FOREIGN KEY (funct_printfont_id) REFERENCES ampere.ad_printfont(ad_printfont_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printtableformat adprintfont_tablehdr; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printtableformat
    ADD CONSTRAINT adprintfont_tablehdr FOREIGN KEY (hdr_printfont_id) REFERENCES ampere.ad_printfont(ad_printfont_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_detail adprintformat_adpackageexpdeta; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_detail
    ADD CONSTRAINT adprintformat_adpackageexpdeta FOREIGN KEY (ad_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printgraph adprintformat_adprintgraph; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printgraph
    ADD CONSTRAINT adprintformat_adprintgraph FOREIGN KEY (ad_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process adprintformat_adprocess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process
    ADD CONSTRAINT adprintformat_adprocess FOREIGN KEY (ad_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfq_topic adprintformat_arfqtopic; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq_topic
    ADD CONSTRAINT adprintformat_arfqtopic FOREIGN KEY (ad_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccountdoc adprintformat_cbankaccountdoc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccountdoc
    ADD CONSTRAINT adprintformat_cbankaccountdoc FOREIGN KEY (check_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_doctype adprintformat_cdoctype; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_doctype
    ADD CONSTRAINT adprintformat_cdoctype FOREIGN KEY (ad_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunninglevel adprintformat_cdunninglevel; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunninglevel
    ADD CONSTRAINT adprintformat_cdunninglevel FOREIGN KEY (dunning_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printform adprintformat_forminvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT adprintformat_forminvoice FOREIGN KEY (invoice_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printform adprintformat_formorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT adprintformat_formorder FOREIGN KEY (order_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printform adprintformat_formproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT adprintformat_formproject FOREIGN KEY (project_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printform adprintformat_formremittance; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT adprintformat_formremittance FOREIGN KEY (remittance_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printform adprintformat_formshipment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT adprintformat_formshipment FOREIGN KEY (shipment_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_report adprintformat_pareport; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_report
    ADD CONSTRAINT adprintformat_pareport FOREIGN KEY (ad_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printformatitem adprintformat_printformatchild; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformatitem
    ADD CONSTRAINT adprintformat_printformatchild FOREIGN KEY (ad_printformatchild_id) REFERENCES ampere.ad_printformat(ad_printformat_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printformatitem adprintformat_printformatitem; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformatitem
    ADD CONSTRAINT adprintformat_printformatitem FOREIGN KEY (ad_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner adprintformatinv_cbpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT adprintformatinv_cbpartner FOREIGN KEY (invoice_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printgraph adprintformatitem_graphdata; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printgraph
    ADD CONSTRAINT adprintformatitem_graphdata FOREIGN KEY (data_printformatitem_id) REFERENCES ampere.ad_printformatitem(ad_printformatitem_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printgraph adprintformatitem_graphdata1; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printgraph
    ADD CONSTRAINT adprintformatitem_graphdata1 FOREIGN KEY (data1_printformatitem_id) REFERENCES ampere.ad_printformatitem(ad_printformatitem_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printgraph adprintformatitem_graphdata2; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printgraph
    ADD CONSTRAINT adprintformatitem_graphdata2 FOREIGN KEY (data2_printformatitem_id) REFERENCES ampere.ad_printformatitem(ad_printformatitem_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printgraph adprintformatitem_graphdata3; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printgraph
    ADD CONSTRAINT adprintformatitem_graphdata3 FOREIGN KEY (data3_printformatitem_id) REFERENCES ampere.ad_printformatitem(ad_printformatitem_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printgraph adprintformatitem_graphdata4; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printgraph
    ADD CONSTRAINT adprintformatitem_graphdata4 FOREIGN KEY (data4_printformatitem_id) REFERENCES ampere.ad_printformatitem(ad_printformatitem_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printgraph adprintformatitem_graphdescr; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printgraph
    ADD CONSTRAINT adprintformatitem_graphdescr FOREIGN KEY (description_printformatitem_id) REFERENCES ampere.ad_printformatitem(ad_printformatitem_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printformatitem_trl adprintformatitem_trl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformatitem_trl
    ADD CONSTRAINT adprintformatitem_trl FOREIGN KEY (ad_printformatitem_id) REFERENCES ampere.ad_printformatitem(ad_printformatitem_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printformat adprintformattable_format; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformat
    ADD CONSTRAINT adprintformattable_format FOREIGN KEY (ad_printtableformat_id) REFERENCES ampere.ad_printtableformat(ad_printtableformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printformatitem adprintgraph_printformatitem; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformatitem
    ADD CONSTRAINT adprintgraph_printformatitem FOREIGN KEY (ad_printgraph_id) REFERENCES ampere.ad_printgraph(ad_printgraph_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printformat adprintpaper_adprintformat; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformat
    ADD CONSTRAINT adprintpaper_adprintformat FOREIGN KEY (ad_printpaper_id) REFERENCES ampere.ad_printpaper(ad_printpaper_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printformat adprintview_adprintformat; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformat
    ADD CONSTRAINT adprintview_adprintformat FOREIGN KEY (ad_reportview_id) REFERENCES ampere.ad_reportview(ad_reportview_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_archive adprocess_adarchive; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_archive
    ADD CONSTRAINT adprocess_adarchive FOREIGN KEY (ad_process_id) REFERENCES ampere.ad_process(ad_process_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_column adprocess_adcolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column
    ADD CONSTRAINT adprocess_adcolumn FOREIGN KEY (ad_process_id) REFERENCES ampere.ad_process(ad_process_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_issue adprocess_adissue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_issue
    ADD CONSTRAINT adprocess_adissue FOREIGN KEY (ad_process_id) REFERENCES ampere.ad_process(ad_process_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_menu adprocess_admenu; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_menu
    ADD CONSTRAINT adprocess_admenu FOREIGN KEY (ad_process_id) REFERENCES ampere.ad_process(ad_process_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_common adprocess_adpackageexpcommon; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_common
    ADD CONSTRAINT adprocess_adpackageexpcommon FOREIGN KEY (ad_process_id) REFERENCES ampere.ad_process(ad_process_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_detail adprocess_adpackageexpdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_detail
    ADD CONSTRAINT adprocess_adpackageexpdetail FOREIGN KEY (ad_process_id) REFERENCES ampere.ad_process(ad_process_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_pinstance adprocess_adpinstance; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_pinstance
    ADD CONSTRAINT adprocess_adpinstance FOREIGN KEY (ad_process_id) REFERENCES ampere.ad_process(ad_process_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process_para adprocess_adprocesspara; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_para
    ADD CONSTRAINT adprocess_adprocesspara FOREIGN KEY (ad_process_id) REFERENCES ampere.ad_process(ad_process_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_scheduler adprocess_adscheduler; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_scheduler
    ADD CONSTRAINT adprocess_adscheduler FOREIGN KEY (ad_process_id) REFERENCES ampere.ad_process(ad_process_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_tab adprocess_adtab; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab
    ADD CONSTRAINT adprocess_adtab FOREIGN KEY (ad_process_id) REFERENCES ampere.ad_process(ad_process_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_scheduler_para adprocesspara_adschedulerpara; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_scheduler_para
    ADD CONSTRAINT adprocesspara_adschedulerpara FOREIGN KEY (ad_process_para_id) REFERENCES ampere.ad_process_para(ad_process_para_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process_para_trl adprocpara_adprocparatrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_para_trl
    ADD CONSTRAINT adprocpara_adprocparatrl FOREIGN KEY (ad_process_para_id) REFERENCES ampere.ad_process_para(ad_process_para_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_attribute adreference_adattribute; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_attribute
    ADD CONSTRAINT adreference_adattribute FOREIGN KEY (ad_reference_id) REFERENCES ampere.ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_field adreference_adfield; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_field
    ADD CONSTRAINT adreference_adfield FOREIGN KEY (ad_reference_id) REFERENCES ampere.ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_infocolumn adreference_adinfocolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_infocolumn
    ADD CONSTRAINT adreference_adinfocolumn FOREIGN KEY (ad_reference_id) REFERENCES ampere.ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_detail adreference_adpackageexpdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_detail
    ADD CONSTRAINT adreference_adpackageexpdetail FOREIGN KEY (ad_reference_id) REFERENCES ampere.ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_imp_backup adreference_adpackageimpbackup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_imp_backup
    ADD CONSTRAINT adreference_adpackageimpbackup FOREIGN KEY (ad_reference_id) REFERENCES ampere.ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process_para adreference_adprocesspara; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_para
    ADD CONSTRAINT adreference_adprocesspara FOREIGN KEY (ad_reference_id) REFERENCES ampere.ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_registrationattribute adreference_aregattribute; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_registrationattribute
    ADD CONSTRAINT adreference_aregattribute FOREIGN KEY (ad_reference_id) REFERENCES ampere.ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_relationtype adreferencesource_adrelationty; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_relationtype
    ADD CONSTRAINT adreferencesource_adrelationty FOREIGN KEY (ad_reference_source_id) REFERENCES ampere.ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_relationtype adreferencetarget_adrelationty; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_relationtype
    ADD CONSTRAINT adreferencetarget_adrelationty FOREIGN KEY (ad_reference_target_id) REFERENCES ampere.ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_attribute adreferencevalue_adattribute; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_attribute
    ADD CONSTRAINT adreferencevalue_adattribute FOREIGN KEY (ad_reference_value_id) REFERENCES ampere.ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_field adreferencevalue_adfield; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_field
    ADD CONSTRAINT adreferencevalue_adfield FOREIGN KEY (ad_reference_value_id) REFERENCES ampere.ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process_para adreferencevalue_adprocpara; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_para
    ADD CONSTRAINT adreferencevalue_adprocpara FOREIGN KEY (ad_reference_value_id) REFERENCES ampere.ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_registrationattribute adreferencevalue_aregattribute; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_registrationattribute
    ADD CONSTRAINT adreferencevalue_aregattribute FOREIGN KEY (ad_reference_value_id) REFERENCES ampere.ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_document_action_access adreflist_addocumentactionacce; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_document_action_access
    ADD CONSTRAINT adreflist_addocumentactionacce FOREIGN KEY (ad_ref_list_id) REFERENCES ampere.ad_ref_list(ad_ref_list_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_common adreportview_adpackageexpcommo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_common
    ADD CONSTRAINT adreportview_adpackageexpcommo FOREIGN KEY (ad_reportview_id) REFERENCES ampere.ad_reportview(ad_reportview_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_detail adreportview_adpackageexpdetai; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_detail
    ADD CONSTRAINT adreportview_adpackageexpdetai FOREIGN KEY (ad_reportview_id) REFERENCES ampere.ad_reportview(ad_reportview_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process adreportview_adprocess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process
    ADD CONSTRAINT adreportview_adprocess FOREIGN KEY (ad_reportview_id) REFERENCES ampere.ad_reportview(ad_reportview_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_reportview_col adreportview_col; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_reportview_col
    ADD CONSTRAINT adreportview_col FOREIGN KEY (ad_reportview_id) REFERENCES ampere.ad_reportview(ad_reportview_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_alertrecipient adrole_adaltertrecipient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_alertrecipient
    ADD CONSTRAINT adrole_adaltertrecipient FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_column_access adrole_adcolumnaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column_access
    ADD CONSTRAINT adrole_adcolumnaccess FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_document_action_access adrole_addocumentactionaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_document_action_access
    ADD CONSTRAINT adrole_addocumentactionaccess FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_form_access adrole_adformaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_form_access
    ADD CONSTRAINT adrole_adformaccess FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_common adrole_adpackageexpcommon; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_common
    ADD CONSTRAINT adrole_adpackageexpcommon FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_detail adrole_adpackageexpdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_detail
    ADD CONSTRAINT adrole_adpackageexpdetail FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_role_orgaccess adrole_adroleorgaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_role_orgaccess
    ADD CONSTRAINT adrole_adroleorgaccess FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_schedulerrecipient adrole_adschedulerrecipient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_schedulerrecipient
    ADD CONSTRAINT adrole_adschedulerrecipient FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_session adrole_adsession; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_session
    ADD CONSTRAINT adrole_adsession FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_table_access adrole_adtableaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table_access
    ADD CONSTRAINT adrole_adtableaccess FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_userdef_win adrole_aduserdefwin; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userdef_win
    ADD CONSTRAINT adrole_aduserdefwin FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user_roles adrole_aduserroles; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user_roles
    ADD CONSTRAINT adrole_aduserroles FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_window_access adrole_adwindowaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_window_access
    ADD CONSTRAINT adrole_adwindowaccess FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_workflow_access adrole_adworkflowaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_workflow_access
    ADD CONSTRAINT adrole_adworkflowaccess FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_record_access adrole_ardecordaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_record_access
    ADD CONSTRAINT adrole_ardecordaccess FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_goal adrole_pagoal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_goal
    ADD CONSTRAINT adrole_pagoal FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request adrole_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT adrole_rrequest FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction adrole_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT adrole_rrequestaction FOREIGN KEY (ad_role_id) REFERENCES ampere.ad_role(ad_role_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_table_scriptvalidator adrule_adtablescriptvalidator; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table_scriptvalidator
    ADD CONSTRAINT adrule_adtablescriptvalidator FOREIGN KEY (ad_rule_id) REFERENCES ampere.ad_rule(ad_rule_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax adrule_ctax; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax
    ADD CONSTRAINT adrule_ctax FOREIGN KEY (ad_rule_id) REFERENCES ampere.ad_rule(ad_rule_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_scheduler_para adscheduler_adschedulerpara; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_scheduler_para
    ADD CONSTRAINT adscheduler_adschedulerpara FOREIGN KEY (ad_scheduler_id) REFERENCES ampere.ad_scheduler(ad_scheduler_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_schedulerlog adscheduler_log; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_schedulerlog
    ADD CONSTRAINT adscheduler_log FOREIGN KEY (ad_scheduler_id) REFERENCES ampere.ad_scheduler(ad_scheduler_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_schedulerrecipient adscheduler_recipient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_schedulerrecipient
    ADD CONSTRAINT adscheduler_recipient FOREIGN KEY (ad_scheduler_id) REFERENCES ampere.ad_scheduler(ad_scheduler_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_edi adsequence_cbpedi; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_edi
    ADD CONSTRAINT adsequence_cbpedi FOREIGN KEY (ad_sequence_id) REFERENCES ampere.ad_sequence(ad_sequence_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_paymentprocessor adsequence_cpaymentprocessor; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_paymentprocessor
    ADD CONSTRAINT adsequence_cpaymentprocessor FOREIGN KEY (ad_sequence_id) REFERENCES ampere.ad_sequence(ad_sequence_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_registration adsystem_adregistration; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_registration
    ADD CONSTRAINT adsystem_adregistration FOREIGN KEY (ad_system_id, ad_client_id) REFERENCES ampere.ad_system(ad_system_id, ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_userdef_tab adtab_aduserdeftab; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userdef_tab
    ADD CONSTRAINT adtab_aduserdeftab FOREIGN KEY (ad_tab_id) REFERENCES ampere.ad_tab(ad_tab_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_userquery adtab_aduserquery; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userquery
    ADD CONSTRAINT adtab_aduserquery FOREIGN KEY (ad_tab_id) REFERENCES ampere.ad_tab(ad_tab_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_tab adtab_included; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab
    ADD CONSTRAINT adtab_included FOREIGN KEY (included_tab_id) REFERENCES ampere.ad_tab(ad_tab_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_accesslog adtable_adacceslog; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_accesslog
    ADD CONSTRAINT adtable_adacceslog FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_alertrule adtable_adaltertrule; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_alertrule
    ADD CONSTRAINT adtable_adaltertrule FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_archive adtable_adarchive; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_archive
    ADD CONSTRAINT adtable_adarchive FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_attachment adtable_adattachment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_attachment
    ADD CONSTRAINT adtable_adattachment FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_attribute adtable_adattribute; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_attribute
    ADD CONSTRAINT adtable_adattribute FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientshare adtable_adclientshare; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientshare
    ADD CONSTRAINT adtable_adclientshare FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_column_access adtable_adcolumnaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column_access
    ADD CONSTRAINT adtable_adcolumnaccess FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_housekeeping adtable_adhousekeeping; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_housekeeping
    ADD CONSTRAINT adtable_adhousekeeping FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_impformat adtable_adimpformat; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_impformat
    ADD CONSTRAINT adtable_adimpformat FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_infowindow adtable_adinfowindow; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_infowindow
    ADD CONSTRAINT adtable_adinfowindow FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_note adtable_adnote; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_note
    ADD CONSTRAINT adtable_adnote FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_common adtable_adpackageexpcommon; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_common
    ADD CONSTRAINT adtable_adpackageexpcommon FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_detail adtable_adpackageexpdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_detail
    ADD CONSTRAINT adtable_adpackageexpdetail FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printformat adtable_adprintformat; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformat
    ADD CONSTRAINT adtable_adprintformat FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_private_access adtable_adprivateaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_private_access
    ADD CONSTRAINT adtable_adprivateaccess FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_record_access adtable_adrecordaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_record_access
    ADD CONSTRAINT adtable_adrecordaccess FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_reportview adtable_adreportview; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_reportview
    ADD CONSTRAINT adtable_adreportview FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_searchdefinition adtable_adsearchdefinition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_searchdefinition
    ADD CONSTRAINT adtable_adsearchdefinition FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_sequence_audit adtable_adsequenceaudit; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_sequence_audit
    ADD CONSTRAINT adtable_adsequenceaudit FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_table_access adtable_adtableaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table_access
    ADD CONSTRAINT adtable_adtableaccess FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_table_scriptvalidator adtable_adtablescriptvalidator; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table_scriptvalidator
    ADD CONSTRAINT adtable_adtablescriptvalidator FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_table_trl adtable_adtabletrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table_trl
    ADD CONSTRAINT adtable_adtabletrl FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_userquery adtable_aduserquery; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userquery
    ADD CONSTRAINT adtable_aduserquery FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_wf_process adtable_adwfprocess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_wf_process
    ADD CONSTRAINT adtable_adwfprocess FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_workflow adtable_adworkflow; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_workflow
    ADD CONSTRAINT adtable_adworkflow FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctprocessor adtable_cacctprocessor; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctprocessor
    ADD CONSTRAINT adtable_cacctprocessor FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct adtable_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT adtable_factacct FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_attributesetexclude adtable_mattributesetexclude; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributesetexclude
    ADD CONSTRAINT adtable_mattributesetexclude FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_lotctlexclude adtable_mlotctlexclude; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_lotctlexclude
    ADD CONSTRAINT adtable_mlotctlexclude FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_sernoctlexclude adtable_msernoctlexclude; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_sernoctlexclude
    ADD CONSTRAINT adtable_msernoctlexclude FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_measurecalc adtable_pameasurecalc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_measurecalc
    ADD CONSTRAINT adtable_pameasurecalc FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_sla_measure adtable_paslameasure; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_sla_measure
    ADD CONSTRAINT adtable_paslameasure FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request adtable_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT adtable_rrequest FOREIGN KEY (ad_table_id) REFERENCES ampere.ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_role adtree_adrole; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_role
    ADD CONSTRAINT adtree_adrole FOREIGN KEY (ad_tree_menu_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_treebar adtree_adtreebar; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treebar
    ADD CONSTRAINT adtree_adtreebar FOREIGN KEY (ad_tree_id) REFERENCES ampere.ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_treenode adtree_adtreenode; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenode
    ADD CONSTRAINT adtree_adtreenode FOREIGN KEY (ad_tree_id) REFERENCES ampere.ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_treenodebp adtree_adtreenodebp; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenodebp
    ADD CONSTRAINT adtree_adtreenodebp FOREIGN KEY (ad_tree_id) REFERENCES ampere.ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_treenodemm adtree_adtreenodemm; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenodemm
    ADD CONSTRAINT adtree_adtreenodemm FOREIGN KEY (ad_tree_id) REFERENCES ampere.ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_treenodepr adtree_adtreenodepr; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenodepr
    ADD CONSTRAINT adtree_adtreenodepr FOREIGN KEY (ad_tree_id) REFERENCES ampere.ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_treenodeu1 adtree_adtreenodeu1; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenodeu1
    ADD CONSTRAINT adtree_adtreenodeu1 FOREIGN KEY (ad_tree_id) REFERENCES ampere.ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_treenodeu2 adtree_adtreenodeu2; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenodeu2
    ADD CONSTRAINT adtree_adtreenodeu2 FOREIGN KEY (ad_tree_id) REFERENCES ampere.ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_treenodeu3 adtree_adtreenodeu3; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenodeu3
    ADD CONSTRAINT adtree_adtreenodeu3 FOREIGN KEY (ad_tree_id) REFERENCES ampere.ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_treenodeu4 adtree_adtreenodeu4; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treenodeu4
    ADD CONSTRAINT adtree_adtreenodeu4 FOREIGN KEY (ad_tree_id) REFERENCES ampere.ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_element adtree_celement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_element
    ADD CONSTRAINT adtree_celement FOREIGN KEY (ad_tree_id) REFERENCES ampere.ad_tree(ad_tree_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_hierarchy adtreeaccount_pahierarchy; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_hierarchy
    ADD CONSTRAINT adtreeaccount_pahierarchy FOREIGN KEY (ad_tree_account_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo adtreeactivity_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT adtreeactivity_adclientinfo FOREIGN KEY (ad_tree_activity_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_hierarchy adtreeactivity_pahierarchy; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_hierarchy
    ADD CONSTRAINT adtreeactivity_pahierarchy FOREIGN KEY (ad_tree_activity_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo adtreebpartner_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT adtreebpartner_adclientinfo FOREIGN KEY (ad_tree_bpartner_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_hierarchy adtreebpartner_pahierarchy; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_hierarchy
    ADD CONSTRAINT adtreebpartner_pahierarchy FOREIGN KEY (ad_tree_bpartner_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo adtreecampaign_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT adtreecampaign_adclientinfo FOREIGN KEY (ad_tree_campaign_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_hierarchy adtreecampaign_pahierarchy; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_hierarchy
    ADD CONSTRAINT adtreecampaign_pahierarchy FOREIGN KEY (ad_tree_campaign_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo adtreemenu_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT adtreemenu_adclientinfo FOREIGN KEY (ad_tree_menu_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo adtreeorg_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT adtreeorg_adclientinfo FOREIGN KEY (ad_tree_org_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_role adtreeorg_adrole; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_role
    ADD CONSTRAINT adtreeorg_adrole FOREIGN KEY (ad_tree_org_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_hierarchy adtreeorg_pahierarchy; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_hierarchy
    ADD CONSTRAINT adtreeorg_pahierarchy FOREIGN KEY (ad_tree_org_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo adtreeproduct_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT adtreeproduct_adclientinfo FOREIGN KEY (ad_tree_product_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_hierarchy adtreeproduct_pahierarchy; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_hierarchy
    ADD CONSTRAINT adtreeproduct_pahierarchy FOREIGN KEY (ad_tree_product_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo adtreeproject_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT adtreeproject_adclientinfo FOREIGN KEY (ad_tree_project_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_hierarchy adtreeproject_pahierarchy; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_hierarchy
    ADD CONSTRAINT adtreeproject_pahierarchy FOREIGN KEY (ad_tree_project_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo adtreesalesreg_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT adtreesalesreg_adclientinfo FOREIGN KEY (ad_tree_salesregion_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_hierarchy adtreesr_pahierarchy; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_hierarchy
    ADD CONSTRAINT adtreesr_pahierarchy FOREIGN KEY (ad_tree_salesregion_id) REFERENCES ampere.ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_ordersource aduser1_cordersource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_ordersource
    ADD CONSTRAINT aduser1_cordersource FOREIGN KEY (createdby) REFERENCES ampere.ad_user(ad_user_id);


--
-- Name: c_ordersource aduser2_cordersource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_ordersource
    ADD CONSTRAINT aduser2_cordersource FOREIGN KEY (updatedby) REFERENCES ampere.ad_user(ad_user_id);


--
-- Name: a_asset aduser_aasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset
    ADD CONSTRAINT aduser_aasset FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_change aduser_aassetchange; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_change
    ADD CONSTRAINT aduser_aassetchange FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_delivery aduser_aassetdelivery; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_delivery
    ADD CONSTRAINT aduser_aassetdelivery FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_alertrecipient aduser_adalertrecipient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_alertrecipient
    ADD CONSTRAINT aduser_adalertrecipient FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_attachmentnote aduser_adattachmentnote; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_attachmentnote
    ADD CONSTRAINT aduser_adattachmentnote FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_note aduser_adnote; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_note
    ADD CONSTRAINT aduser_adnote FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_orginfo aduser_adorginfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_orginfo
    ADD CONSTRAINT aduser_adorginfo FOREIGN KEY (supervisor_id) REFERENCES ampere.ad_user(ad_user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_private_access aduser_adprivateaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_private_access
    ADD CONSTRAINT aduser_adprivateaccess FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_scheduler aduser_adscheduler; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_scheduler
    ADD CONSTRAINT aduser_adscheduler FOREIGN KEY (supervisor_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_schedulerrecipient aduser_adschedulerrecipient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_schedulerrecipient
    ADD CONSTRAINT aduser_adschedulerrecipient FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_treebar aduser_adtreebar; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_treebar
    ADD CONSTRAINT aduser_adtreebar FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_userbpaccess aduser_aduserbpaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userbpaccess
    ADD CONSTRAINT aduser_aduserbpaccess FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_userdef_win aduser_aduserdefwin; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userdef_win
    ADD CONSTRAINT aduser_aduserdefwin FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_usermail aduser_adusermail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_usermail
    ADD CONSTRAINT aduser_adusermail FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user_orgaccess aduser_aduserorgaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user_orgaccess
    ADD CONSTRAINT aduser_aduserorgaccess FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_userquery aduser_aduserquery; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userquery
    ADD CONSTRAINT aduser_aduserquery FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user_substitute aduser_adusersub; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user_substitute
    ADD CONSTRAINT aduser_adusersub FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_wf_process aduser_adwfprocess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_wf_process
    ADD CONSTRAINT aduser_adwfprocess FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_registration aduser_aregistration; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_registration
    ADD CONSTRAINT aduser_aregistration FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfq_topicsubscriber aduser_arfqtopicsubcr; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq_topicsubscriber
    ADD CONSTRAINT aduser_arfqtopicsubcr FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctprocessor aduser_cacctprocessor; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctprocessor
    ADD CONSTRAINT aduser_cacctprocessor FOREIGN KEY (supervisor_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_alertprocessor aduser_calertprocessor; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_alertprocessor
    ADD CONSTRAINT aduser_calertprocessor FOREIGN KEY (supervisor_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_bankaccount aduser_cbpbankaccount; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_bankaccount
    ADD CONSTRAINT aduser_cbpbankaccount FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunningrunentry aduser_cdunningrunentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrunentry
    ADD CONSTRAINT aduser_cdunningrunentry FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice aduser_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT aduser_cinvoice FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatch aduser_cinvoicebatch; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatch
    ADD CONSTRAINT aduser_cinvoicebatch FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatchline aduser_cinvoicebatchline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT aduser_cinvoicebatchline FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_jobassignment aduser_cjobassignment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_jobassignment
    ADD CONSTRAINT aduser_cjobassignment FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order aduser_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT aduser_corder FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orgassignment aduser_corgassignment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orgassignment
    ADD CONSTRAINT aduser_corgassignment FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project aduser_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project
    ADD CONSTRAINT aduser_cproject FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfq aduser_crfq; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq
    ADD CONSTRAINT aduser_crfq FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfqresponse aduser_crfqresponse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqresponse
    ADD CONSTRAINT aduser_crfqresponse FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_userremuneration aduser_cuserremuneration; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_userremuneration
    ADD CONSTRAINT aduser_cuserremuneration FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bpartner aduser_ibpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bpartner
    ADD CONSTRAINT aduser_ibpartner FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice aduser_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT aduser_iinvoice FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order aduser_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT aduser_iorder FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout aduser_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT aduser_minout FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movement aduser_mmovement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT aduser_mmovement FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_requisition aduser_mrequisition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_requisition
    ADD CONSTRAINT aduser_mrequisition FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_goal aduser_pagoal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_goal
    ADD CONSTRAINT aduser_pagoal FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_pinstance aduser_pinstance; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_pinstance
    ADD CONSTRAINT aduser_pinstance FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_categoryupdates aduser_rcategoryupdates; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_categoryupdates
    ADD CONSTRAINT aduser_rcategoryupdates FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_contactinterest aduser_rcontactinterest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_contactinterest
    ADD CONSTRAINT aduser_rcontactinterest FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_issueuser aduser_rissueuser; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issueuser
    ADD CONSTRAINT aduser_rissueuser FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request aduser_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT aduser_rrequest FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction aduser_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT aduser_rrequestaction FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestprocessor aduser_rrequestprocessor; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestprocessor
    ADD CONSTRAINT aduser_rrequestprocessor FOREIGN KEY (supervisor_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestprocessor_route aduser_rrequestprocessorroute; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestprocessor_route
    ADD CONSTRAINT aduser_rrequestprocessorroute FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requesttypeupdates aduser_rrequesttypeupdates; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requesttypeupdates
    ADD CONSTRAINT aduser_rrequesttypeupdates FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestupdates aduser_rrequestupdates; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestupdates
    ADD CONSTRAINT aduser_rrequestupdates FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_groupupdates aduser_ruserupdates; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_groupupdates
    ADD CONSTRAINT aduser_ruserupdates FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice aduser_sr_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT aduser_sr_cinvoice FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order aduser_sr_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT aduser_sr_corder FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project aduser_sr_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project
    ADD CONSTRAINT aduser_sr_cproject FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout aduser_sr_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT aduser_sr_minout FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_resource aduser_sresource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_resource
    ADD CONSTRAINT aduser_sresource FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user aduser_supervisor; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user
    ADD CONSTRAINT aduser_supervisor FOREIGN KEY (supervisor_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user_roles aduser_userroles; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user_roles
    ADD CONSTRAINT aduser_userroles FOREIGN KEY (ad_user_id) REFERENCES ampere.ad_user(ad_user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order aduserbill_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT aduserbill_corder FOREIGN KEY (bill_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_userdef_field aduserdeftab_aduserdeffield; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userdef_field
    ADD CONSTRAINT aduserdeftab_aduserdeffield FOREIGN KEY (ad_userdef_tab_id) REFERENCES ampere.ad_userdef_tab(ad_userdef_tab_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_userdef_tab aduserdefwin_aduserdeftab; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userdef_tab
    ADD CONSTRAINT aduserdefwin_aduserdeftab FOREIGN KEY (ad_userdef_win_id) REFERENCES ampere.ad_userdef_win(ad_userdef_win_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner adusersalesrep_cbpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT adusersalesrep_cbpartner FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfq adusersalesrep_crfq; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq
    ADD CONSTRAINT adusersalesrep_crfq FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice adusersalesrep_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT adusersalesrep_iinvoice FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order adusersalesrep_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT adusersalesrep_iorder FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request adusersr_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT adusersr_rrequest FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction adusersr_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT adusersr_rrequestaction FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user_substitute adusersub_ad_usersub; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user_substitute
    ADD CONSTRAINT adusersub_ad_usersub FOREIGN KEY (substitute_id) REFERENCES ampere.ad_user(ad_user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_role adusersupervisor_adrole; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_role
    ADD CONSTRAINT adusersupervisor_adrole FOREIGN KEY (supervisor_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process_para advalrule_ad_processpara; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_para
    ADD CONSTRAINT advalrule_ad_processpara FOREIGN KEY (ad_val_rule_id) REFERENCES ampere.ad_val_rule(ad_val_rule_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_attribute advalrule_adattribute; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_attribute
    ADD CONSTRAINT advalrule_adattribute FOREIGN KEY (ad_val_rule_id) REFERENCES ampere.ad_val_rule(ad_val_rule_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_field advalrule_adfield; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_field
    ADD CONSTRAINT advalrule_adfield FOREIGN KEY (ad_val_rule_id) REFERENCES ampere.ad_val_rule(ad_val_rule_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_wf_process adwfprocess_adwfprocess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_wf_process
    ADD CONSTRAINT adwfprocess_adwfprocess FOREIGN KEY (ad_wf_process_id) REFERENCES ampere.ad_wf_process(ad_wf_process_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_issue adwindow_adissue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_issue
    ADD CONSTRAINT adwindow_adissue FOREIGN KEY (ad_window_id) REFERENCES ampere.ad_window(ad_window_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_menu adwindow_admenu; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_menu
    ADD CONSTRAINT adwindow_admenu FOREIGN KEY (ad_window_id) REFERENCES ampere.ad_window(ad_window_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_common adwindow_adpackageexpcommon; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_common
    ADD CONSTRAINT adwindow_adpackageexpcommon FOREIGN KEY (ad_window_id) REFERENCES ampere.ad_window(ad_window_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_detail adwindow_adpackageexpdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_detail
    ADD CONSTRAINT adwindow_adpackageexpdetail FOREIGN KEY (ad_window_id) REFERENCES ampere.ad_window(ad_window_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_ref_table adwindow_adreftable; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_table
    ADD CONSTRAINT adwindow_adreftable FOREIGN KEY (ad_window_id) REFERENCES ampere.ad_window(ad_window_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_searchdefinition adwindow_adsearchdefinition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_searchdefinition
    ADD CONSTRAINT adwindow_adsearchdefinition FOREIGN KEY (ad_window_id) REFERENCES ampere.ad_window(ad_window_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_userdef_win adwindow_aduserdefwin; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userdef_win
    ADD CONSTRAINT adwindow_aduserdefwin FOREIGN KEY (ad_window_id) REFERENCES ampere.ad_window(ad_window_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_dashboardcontent adwindow_padashboardcontent; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_dashboardcontent
    ADD CONSTRAINT adwindow_padashboardcontent FOREIGN KEY (ad_window_id) REFERENCES ampere.ad_window(ad_window_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_table adwindowpo_adtable; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table
    ADD CONSTRAINT adwindowpo_adtable FOREIGN KEY (po_window_id) REFERENCES ampere.ad_window(ad_window_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_common adworkflow_adpackageexpcommon; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_common
    ADD CONSTRAINT adworkflow_adpackageexpcommon FOREIGN KEY (ad_workflow_id) REFERENCES ampere.ad_workflow(ad_workflow_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_package_exp_detail adworkflow_adpackageexpdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_package_exp_detail
    ADD CONSTRAINT adworkflow_adpackageexpdetail FOREIGN KEY (ad_workflow_id) REFERENCES ampere.ad_workflow(ad_workflow_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process adworkflow_adprocess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process
    ADD CONSTRAINT adworkflow_adprocess FOREIGN KEY (ad_workflow_id) REFERENCES ampere.ad_workflow(ad_workflow_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_wf_process adworkflow_adwfprocess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_wf_process
    ADD CONSTRAINT adworkflow_adwfprocess FOREIGN KEY (ad_workflow_id) REFERENCES ampere.ad_workflow(ad_workflow_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_depreciation_build aendasset_adepreciationbuild; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_build
    ADD CONSTRAINT aendasset_adepreciationbuild FOREIGN KEY (a_end_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_depreciation_forecast aendasset_adepreciationforecas; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_forecast
    ADD CONSTRAINT aendasset_adepreciationforecas FOREIGN KEY (a_end_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset aparentasset_aasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset
    ADD CONSTRAINT aparentasset_aasset FOREIGN KEY (a_parent_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_change aparentasset_aassetchange; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_change
    ADD CONSTRAINT aparentasset_aassetchange FOREIGN KEY (a_parent_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_registrationproduct aregattribute_aregproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_registrationproduct
    ADD CONSTRAINT aregattribute_aregproduct FOREIGN KEY (a_registrationattribute_id) REFERENCES ampere.a_registrationattribute(a_registrationattribute_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_registrationvalue aregattribute_aregvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_registrationvalue
    ADD CONSTRAINT aregattribute_aregvalue FOREIGN KEY (a_registrationattribute_id) REFERENCES ampere.a_registrationattribute(a_registrationattribute_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_registrationvalue aregistration_aregvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_registrationvalue
    ADD CONSTRAINT aregistration_aregvalue FOREIGN KEY (a_registration_id) REFERENCES ampere.a_registration(a_registration_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_depreciation_build astartasset_adepreciationbuild; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_build
    ADD CONSTRAINT astartasset_adepreciationbuild FOREIGN KEY (a_start_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_depreciation_forecast astartasset_adepreciationforec; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_forecast
    ADD CONSTRAINT astartasset_adepreciationforec FOREIGN KEY (a_start_asset_id) REFERENCES ampere.a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_pricelist basepricelist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_pricelist
    ADD CONSTRAINT basepricelist FOREIGN KEY (basepricelist_id) REFERENCES ampere.m_pricelist(m_pricelist_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_withholding benefici_cwithholding; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_withholding
    ADD CONSTRAINT benefici_cwithholding FOREIGN KEY (beneficiary) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journal c_acctschema_gl_journal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journal
    ADD CONSTRAINT c_acctschema_gl_journal FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_edi c_bpartner_cbpedi; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_edi
    ADD CONSTRAINT c_bpartner_cbpedi FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request c_bpartner_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT c_bpartner_rrequest FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice c_bplocation_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT c_bplocation_cinvoice FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_vendor_acct c_buspartner_c_bp_vendor_acct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_vendor_acct
    ADD CONSTRAINT c_buspartner_c_bp_vendor_acct FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct c_buspartner_fact_acct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT c_buspartner_fact_acct FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner_location c_buspartner_locationclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner_location
    ADD CONSTRAINT c_buspartner_locationclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner_location c_buspartner_locationorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner_location
    ADD CONSTRAINT c_buspartner_locationorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_po c_buspartner_m_product_po; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_po
    ADD CONSTRAINT c_buspartner_m_product_po FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_year c_calendar_year; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_year
    ADD CONSTRAINT c_calendar_year FOREIGN KEY (c_calendar_id) REFERENCES ampere.c_calendar(c_calendar_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_calendar c_calendarclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_calendar
    ADD CONSTRAINT c_calendarclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_nonbusinessday c_calendarnonbusinessday; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_nonbusinessday
    ADD CONSTRAINT c_calendarnonbusinessday FOREIGN KEY (c_calendar_id) REFERENCES ampere.c_calendar(c_calendar_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_calendar c_calendarorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_calendar
    ADD CONSTRAINT c_calendarorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_city c_cityclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_city
    ADD CONSTRAINT c_cityclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_city c_cityorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_city
    ADD CONSTRAINT c_cityorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_conversion_rate c_conversion_rateclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_conversion_rate
    ADD CONSTRAINT c_conversion_rateclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_conversion_rate c_conversion_rateorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_conversion_rate
    ADD CONSTRAINT c_conversion_rateorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax c_country_c_tax; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax
    ADD CONSTRAINT c_country_c_tax FOREIGN KEY (c_country_id) REFERENCES ampere.c_country(c_country_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_location c_country_location; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_location
    ADD CONSTRAINT c_country_location FOREIGN KEY (c_country_id) REFERENCES ampere.c_country(c_country_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_country c_countryclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_country
    ADD CONSTRAINT c_countryclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_country c_countryorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_country
    ADD CONSTRAINT c_countryorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax c_countryto_c_tax; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax
    ADD CONSTRAINT c_countryto_c_tax FOREIGN KEY (to_country_id) REFERENCES ampere.c_country(c_country_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_role c_currency_ad_role; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_role
    ADD CONSTRAINT c_currency_ad_role FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema c_currency_c_acctschema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema
    ADD CONSTRAINT c_currency_c_acctschema FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct c_currency_fact_acct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT c_currency_fact_acct FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_currency c_currencyclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_currency
    ADD CONSTRAINT c_currencyclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_conversion_rate c_currencyconvrateto; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_conversion_rate
    ADD CONSTRAINT c_currencyconvrateto FOREIGN KEY (c_currency_id_to) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_currency c_currencyorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_currency
    ADD CONSTRAINT c_currencyorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order c_doctype_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT c_doctype_corder FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_element c_elementorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_element
    ADD CONSTRAINT c_elementorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_orginfo c_location_ad_orginfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_orginfo
    ADD CONSTRAINT c_location_ad_orginfo FOREIGN KEY (c_location_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_warehouse c_location_warehouse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_warehouse
    ADD CONSTRAINT c_location_warehouse FOREIGN KEY (c_location_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct c_locationfrom_fact_acct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT c_locationfrom_fact_acct FOREIGN KEY (c_locfrom_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct c_locationto_fact_acct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT c_locationto_fact_acct FOREIGN KEY (c_locto_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_nonbusinessday c_nonbusinesdaysclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_nonbusinessday
    ADD CONSTRAINT c_nonbusinesdaysclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_nonbusinessday c_nonbusinesdaysorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_nonbusinessday
    ADD CONSTRAINT c_nonbusinesdaysorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order c_order__c_orders_c_ordersou; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT c_order__c_orders_c_ordersou FOREIGN KEY (c_ordersource_id) REFERENCES ampere.c_ordersource(c_ordersource_id);


--
-- Name: c_payment c_payment__c_cashbo_c_cashbook; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT c_payment__c_cashbo_c_cashbook FOREIGN KEY (c_cashbook_id) REFERENCES ampere.c_cashbook(c_cashbook_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journal c_period_journal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journal
    ADD CONSTRAINT c_period_journal FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journalbatch c_period_journalbatch; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalbatch
    ADD CONSTRAINT c_period_journalbatch FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_periodcontrol c_period_periodcontrol; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_periodcontrol
    ADD CONSTRAINT c_period_periodcontrol FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_period c_periodclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_period
    ADD CONSTRAINT c_periodclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_period c_periodorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_period
    ADD CONSTRAINT c_periodorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct c_project_fact_acct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT c_project_fact_acct FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project_acct c_project_projectacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project_acct
    ADD CONSTRAINT c_project_projectacct FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline c_projectphase_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT c_projectphase_minoutline FOREIGN KEY (c_projectphase_id) REFERENCES ampere.c_projectphase(c_projectphase_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax c_region_c_tax; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax
    ADD CONSTRAINT c_region_c_tax FOREIGN KEY (c_region_id) REFERENCES ampere.c_region(c_region_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_location c_region_location; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_location
    ADD CONSTRAINT c_region_location FOREIGN KEY (c_region_id) REFERENCES ampere.c_region(c_region_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_region c_regionclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_region
    ADD CONSTRAINT c_regionclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_region c_regionorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_region
    ADD CONSTRAINT c_regionorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax c_regionto_c_tax; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax
    ADD CONSTRAINT c_regionto_c_tax FOREIGN KEY (to_region_id) REFERENCES ampere.c_region(c_region_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfq_topicsubscriber c_rfqtopic_subscriber; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq_topicsubscriber
    ADD CONSTRAINT c_rfqtopic_subscriber FOREIGN KEY (c_rfq_topic_id) REFERENCES ampere.c_rfq_topic(c_rfq_topic_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct c_salesregion_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT c_salesregion_factacct FOREIGN KEY (c_salesregion_id) REFERENCES ampere.c_salesregion(c_salesregion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_uom_conversion c_uom_conversionclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_uom_conversion
    ADD CONSTRAINT c_uom_conversionclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_uom_conversion c_uom_conversionorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_uom_conversion
    ADD CONSTRAINT c_uom_conversionorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct c_uom_fact_acct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT c_uom_fact_acct FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo c_uom_length_ad_clientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT c_uom_length_ad_clientinfo FOREIGN KEY (c_uom_length_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_po c_uom_m_product_po; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_po
    ADD CONSTRAINT c_uom_m_product_po FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo c_uom_time_ad_clientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT c_uom_time_ad_clientinfo FOREIGN KEY (c_uom_time_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo c_uom_volume_ad_clientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT c_uom_volume_ad_clientinfo FOREIGN KEY (c_uom_volume_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo c_uom_weight_ad_clientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT c_uom_weight_ad_clientinfo FOREIGN KEY (c_uom_weight_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_uom c_uomclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_uom
    ADD CONSTRAINT c_uomclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_uom_conversion c_uomconversionto; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_uom_conversion
    ADD CONSTRAINT c_uomconversionto FOREIGN KEY (c_uom_to_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_uom c_uomorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_uom
    ADD CONSTRAINT c_uomorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_period c_year_period; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_period
    ADD CONSTRAINT c_year_period FOREIGN KEY (c_year_id) REFERENCES ampere.c_year(c_year_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_year c_yearclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_year
    ADD CONSTRAINT c_yearclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_year c_yearorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_year
    ADD CONSTRAINT c_yearorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctprocessorlog cacctprocessor_log; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctprocessorlog
    ADD CONSTRAINT cacctprocessor_log FOREIGN KEY (c_acctprocessor_id) REFERENCES ampere.c_acctprocessor(c_acctprocessor_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo cacctschema1_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT cacctschema1_adclientinfo FOREIGN KEY (c_acctschema1_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_acct cacctschema_aassetacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_acct
    ADD CONSTRAINT cacctschema_aassetacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_change cacctschema_aassetchange; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_change
    ADD CONSTRAINT cacctschema_aassetchange FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_change_amt cacctschema_aassetchangeamt; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_change_amt
    ADD CONSTRAINT cacctschema_aassetchangeamt FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_group_acct cacctschema_aassetgroupacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_group_acct
    ADD CONSTRAINT cacctschema_aassetgroupacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_reval_entry cacctschema_aassetrevalentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_reval_entry
    ADD CONSTRAINT cacctschema_aassetrevalentry FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_transfer cacctschema_aassettransfer; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_transfer
    ADD CONSTRAINT cacctschema_aassettransfer FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_depreciation_entry cacctschema_adepreciationentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_entry
    ADD CONSTRAINT cacctschema_adepreciationentry FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctprocessor cacctschema_cacctprocessor; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctprocessor
    ADD CONSTRAINT cacctschema_cacctprocessor FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_gl cacctschema_cacctschemagl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_gl
    ADD CONSTRAINT cacctschema_cacctschemagl FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_element cacctschema_caschemaelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_element
    ADD CONSTRAINT cacctschema_caschemaelement FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount_acct cacctschema_cbankaccountacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount_acct
    ADD CONSTRAINT cacctschema_cbankaccountacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_customer_acct cacctschema_cbpcustomeracct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_customer_acct
    ADD CONSTRAINT cacctschema_cbpcustomeracct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_employee_acct cacctschema_cbpemployeeacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_employee_acct
    ADD CONSTRAINT cacctschema_cbpemployeeacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group_acct cacctschema_cbpgroupacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT cacctschema_cbpgroupacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_vendor_acct cacctschema_cbpvendoracct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_vendor_acct
    ADD CONSTRAINT cacctschema_cbpvendoracct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashbook_acct cacctschema_ccashbookacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashbook_acct
    ADD CONSTRAINT cacctschema_ccashbookacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_charge_acct cacctschema_cchargeacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_charge_acct
    ADD CONSTRAINT cacctschema_cchargeacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_currency_acct cacctschema_ccurrencyacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_currency_acct
    ADD CONSTRAINT cacctschema_ccurrencyacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_interorg_acct cacctschema_cinterorgacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_interorg_acct
    ADD CONSTRAINT cacctschema_cinterorgacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project_acct cacctschema_cprojectacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project_acct
    ADD CONSTRAINT cacctschema_cprojectacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_revenuerecognition_plan cacctschema_crevrecplan; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_revenuerecognition_plan
    ADD CONSTRAINT cacctschema_crevrecplan FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax_acct cacctschema_ctaxacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax_acct
    ADD CONSTRAINT cacctschema_ctaxacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdeclarationacct cacctschema_ctaxdeclacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdeclarationacct
    ADD CONSTRAINT cacctschema_ctaxdeclacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination cacctschema_cvalidcombination; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT cacctschema_cvalidcombination FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_withholding_acct cacctschema_cwithholdingacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_withholding_acct
    ADD CONSTRAINT cacctschema_cwithholdingacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default cacctschema_default; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT cacctschema_default FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct cacctschema_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT cacctschema_factacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary cacctschema_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT cacctschema_factacctsummary FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_budgetcontrol cacctschema_glbudgetcontrol; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_budgetcontrol
    ADD CONSTRAINT cacctschema_glbudgetcontrol FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution cacctschema_gldist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT cacctschema_gldist FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_fund cacctschema_glfund; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_fund
    ADD CONSTRAINT cacctschema_glfund FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_asset cacctschema_iasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_asset
    ADD CONSTRAINT cacctschema_iasset FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal cacctschema_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT cacctschema_ifajournal FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal cacctschema_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT cacctschema_igljournal FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_cost cacctschema_mcost; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_cost
    ADD CONSTRAINT cacctschema_mcost FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct cacctschema_mprodcatacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT cacctschema_mprodcatacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct cacctschema_mproductacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT cacctschema_mproductacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_costing cacctschema_mproductcosting; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_costing
    ADD CONSTRAINT cacctschema_mproductcosting FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_warehouse_acct cacctschema_mwarehouseacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_warehouse_acct
    ADD CONSTRAINT cacctschema_mwarehouseacct FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_ratio cacctschema_paratio; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_ratio
    ADD CONSTRAINT cacctschema_paratio FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_report cacctschema_pareport; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_report
    ADD CONSTRAINT cacctschema_pareport FOREIGN KEY (c_acctschema_id) REFERENCES ampere.c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_element cactivity_cacctschemaelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_element
    ADD CONSTRAINT cactivity_cacctschemaelement FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cash cactivity_ccash; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cash
    ADD CONSTRAINT cactivity_ccash FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplan cactivity_ccashplan; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplan
    ADD CONSTRAINT cactivity_ccashplan FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplanline cactivity_ccashplanline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplanline
    ADD CONSTRAINT cactivity_ccashplanline FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice cactivity_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT cactivity_cinvoice FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatchline cactivity_cinvoicebatchline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT cactivity_cinvoicebatchline FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline cactivity_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT cactivity_cinvoiceline FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order cactivity_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT cactivity_corder FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline cactivity_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT cactivity_corderline FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment cactivity_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT cactivity_cpayment FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination cactivity_cvalidcombination; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT cactivity_cvalidcombination FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct cactivity_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT cactivity_factacct FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary cactivity_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT cactivity_factacctsummary FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution cactivity_gldist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT cactivity_gldist FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distributionline cactivity_gldistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT cactivity_gldistline FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal cactivity_gljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT cactivity_gljournal FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal cactivity_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT cactivity_ifajournal FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice cactivity_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT cactivity_iinvoice FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order cactivity_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT cactivity_iorder FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout cactivity_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT cactivity_minout FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline cactivity_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT cactivity_minoutline FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventory cactivity_minventory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventory
    ADD CONSTRAINT cactivity_minventory FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movement cactivity_mmovement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT cactivity_mmovement FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_production cactivity_mproduction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_production
    ADD CONSTRAINT cactivity_mproduction FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotionprecondition cactivity_mpromotionpreconditi; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotionprecondition
    ADD CONSTRAINT cactivity_mpromotionpreconditi FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcolumn cactivity_pareportcolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT cactivity_pareportcolumn FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportsource cactivity_pareportsource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportsource
    ADD CONSTRAINT cactivity_pareportsource FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request cactivity_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT cactivity_rrequest FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction cactivity_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT cactivity_rrequestaction FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpenseline cactivity_stimeexpenseline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpenseline
    ADD CONSTRAINT cactivity_stimeexpenseline FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_aging cactivity_taging; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_aging
    ADD CONSTRAINT cactivity_taging FOREIGN KEY (c_activity_id) REFERENCES ampere.c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_alert calertprocessor_adalert; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_alert
    ADD CONSTRAINT calertprocessor_adalert FOREIGN KEY (ad_alertprocessor_id) REFERENCES ampere.ad_alertprocessor(ad_alertprocessor_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_alertprocessorlog calertprocessor_log; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_alertprocessorlog
    ADD CONSTRAINT calertprocessor_log FOREIGN KEY (ad_alertprocessor_id) REFERENCES ampere.ad_alertprocessor(ad_alertprocessor_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_allocationline callocation_callocationline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_allocationline
    ADD CONSTRAINT callocation_callocationline FOREIGN KEY (c_allocationhdr_id) REFERENCES ampere.c_allocationhdr(c_allocationhdr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdeclarationline callocationline_ctaxdeclline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdeclarationline
    ADD CONSTRAINT callocationline_ctaxdeclline FOREIGN KEY (c_allocationline_id) REFERENCES ampere.c_allocationline(c_allocationline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_orginfo cbank_adorginfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_orginfo
    ADD CONSTRAINT cbank_adorginfo FOREIGN KEY (transferbank_id) REFERENCES ampere.c_bank(c_bank_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount cbank_cbankaccount; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount
    ADD CONSTRAINT cbank_cbankaccount FOREIGN KEY (c_bank_id) REFERENCES ampere.c_bank(c_bank_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_bankaccount cbank_cbpbankaccount; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_bankaccount
    ADD CONSTRAINT cbank_cbpbankaccount FOREIGN KEY (c_bank_id) REFERENCES ampere.c_bank(c_bank_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccountdoc cbankaccount_cbadoc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccountdoc
    ADD CONSTRAINT cbankaccount_cbadoc FOREIGN KEY (c_bankaccount_id) REFERENCES ampere.c_bankaccount(c_bankaccount_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount_acct cbankaccount_cbankacctacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount_acct
    ADD CONSTRAINT cbankaccount_cbankacctacct FOREIGN KEY (c_bankaccount_id) REFERENCES ampere.c_bankaccount(c_bankaccount_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankstatement cbankaccount_cbankstatement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankstatement
    ADD CONSTRAINT cbankaccount_cbankstatement FOREIGN KEY (c_bankaccount_id) REFERENCES ampere.c_bankaccount(c_bankaccount_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_elementvalue cbankaccount_celementvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_elementvalue
    ADD CONSTRAINT cbankaccount_celementvalue FOREIGN KEY (c_bankaccount_id) REFERENCES ampere.c_bankaccount(c_bankaccount_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment cbankaccount_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT cbankaccount_cpayment FOREIGN KEY (c_bankaccount_id) REFERENCES ampere.c_bankaccount(c_bankaccount_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_paymentprocessor cbankaccount_cpaymtprocessor; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_paymentprocessor
    ADD CONSTRAINT cbankaccount_cpaymtprocessor FOREIGN KEY (c_bankaccount_id) REFERENCES ampere.c_bankaccount(c_bankaccount_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payselection cbankaccount_cpayselection; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payselection
    ADD CONSTRAINT cbankaccount_cpayselection FOREIGN KEY (c_bankaccount_id) REFERENCES ampere.c_bankaccount(c_bankaccount_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bankstatement cbankaccount_ibankstatement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bankstatement
    ADD CONSTRAINT cbankaccount_ibankstatement FOREIGN KEY (c_bankaccount_id) REFERENCES ampere.c_bankaccount(c_bankaccount_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_payment cbankaccount_ipayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_payment
    ADD CONSTRAINT cbankaccount_ipayment FOREIGN KEY (c_bankaccount_id) REFERENCES ampere.c_bankaccount(c_bankaccount_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankstatementloader cbankacct_cbankstmtloader; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankstatementloader
    ADD CONSTRAINT cbankacct_cbankstmtloader FOREIGN KEY (c_bankaccount_id) REFERENCES ampere.c_bankaccount(c_bankaccount_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashline cbankacct_ccashline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashline
    ADD CONSTRAINT cbankacct_ccashline FOREIGN KEY (c_bankaccount_id) REFERENCES ampere.c_bankaccount(c_bankaccount_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bankstatement cbankstatement_ibankstatement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bankstatement
    ADD CONSTRAINT cbankstatement_ibankstatement FOREIGN KEY (c_bankstatement_id) REFERENCES ampere.c_bankstatement(c_bankstatement_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bankstatement cbankstmtline_ibankstmt; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bankstatement
    ADD CONSTRAINT cbankstmtline_ibankstmt FOREIGN KEY (c_bankstatementline_id) REFERENCES ampere.c_bankstatementline(c_bankstatementline_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order cbolocation_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT cbolocation_iorder FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset cbpartner_aasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset
    ADD CONSTRAINT cbpartner_aasset FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_archive cbpartner_adarchive; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_archive
    ADD CONSTRAINT cbpartner_adarchive FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user cbpartner_aduser; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user
    ADD CONSTRAINT cbpartner_aduser FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_registration cbpartner_aregistration; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_registration
    ADD CONSTRAINT cbpartner_aregistration FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_allocationline cbpartner_callocationline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_allocationline
    ADD CONSTRAINT cbpartner_callocationline FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankstatementline cbpartner_cbankstatementline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankstatementline
    ADD CONSTRAINT cbpartner_cbankstatementline FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_bankaccount cbpartner_cbpbankaccount; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_bankaccount
    ADD CONSTRAINT cbpartner_cbpbankaccount FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner_location cbpartner_cbplocation; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner_location
    ADD CONSTRAINT cbpartner_cbplocation FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner_product cbpartner_cbpproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner_product
    ADD CONSTRAINT cbpartner_cbpproduct FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_relation cbpartner_cbprelation; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_relation
    ADD CONSTRAINT cbpartner_cbprelation FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_relation cbpartner_cbprelationbp; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_relation
    ADD CONSTRAINT cbpartner_cbprelationbp FOREIGN KEY (c_bpartnerrelation_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_withholding cbpartner_cbpwithholding; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_withholding
    ADD CONSTRAINT cbpartner_cbpwithholding FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplan cbpartner_ccashplan; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplan
    ADD CONSTRAINT cbpartner_ccashplan FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplanline cbpartner_ccashplanline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplanline
    ADD CONSTRAINT cbpartner_ccashplanline FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_charge cbpartner_ccharge; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_charge
    ADD CONSTRAINT cbpartner_ccharge FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commission cbpartner_ccommission; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commission
    ADD CONSTRAINT cbpartner_ccommission FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commissionline cbpartner_ccommissionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissionline
    ADD CONSTRAINT cbpartner_ccommissionline FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunningrunentry cbpartner_cdunningrunentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrunentry
    ADD CONSTRAINT cbpartner_cdunningrunentry FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice cbpartner_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT cbpartner_cinvoice FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatchline cbpartner_cinvoicebatchline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT cbpartner_cinvoicebatchline FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order cbpartner_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT cbpartner_corder FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment cbpartner_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT cbpartner_cpayment FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payselectioncheck cbpartner_cpayselectioncheck; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payselectioncheck
    ADD CONSTRAINT cbpartner_cpayselectioncheck FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner cbpartner_cpbartnerparent; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT cbpartner_cpbartnerparent FOREIGN KEY (bpartner_parent_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project cbpartner_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project
    ADD CONSTRAINT cbpartner_cproject FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfq cbpartner_crfq; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq
    ADD CONSTRAINT cbpartner_crfq FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfqresponse cbpartner_crfqresponse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqresponse
    ADD CONSTRAINT cbpartner_crfqresponse FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfq_topicsubscriber cbpartner_crfqtopicsubr; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq_topicsubscriber
    ADD CONSTRAINT cbpartner_crfqtopicsubr FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_subscription cbpartner_csubscription; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_subscription
    ADD CONSTRAINT cbpartner_csubscription FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdeclarationline cbpartner_ctaxdeclline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdeclarationline
    ADD CONSTRAINT cbpartner_ctaxdeclline FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdefinition cbpartner_ctaxdefinition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdefinition
    ADD CONSTRAINT cbpartner_ctaxdefinition FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary cbpartner_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT cbpartner_factacctsummary FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution cbpartner_gldist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT cbpartner_gldist FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distributionline cbpartner_gldistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT cbpartner_gldistline FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_asset cbpartner_iasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_asset
    ADD CONSTRAINT cbpartner_iasset FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bankstatement cbpartner_ibankstatement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bankstatement
    ADD CONSTRAINT cbpartner_ibankstatement FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bpartner cbpartner_ibpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bpartner
    ADD CONSTRAINT cbpartner_ibpartner FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal cbpartner_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT cbpartner_ifajournal FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal cbpartner_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT cbpartner_igljournal FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice cbpartner_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT cbpartner_iinvoice FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order cbpartner_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT cbpartner_iorder FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_payment cbpartner_ipayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_payment
    ADD CONSTRAINT cbpartner_ipayment FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_pricelist cbpartner_ipricelist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_pricelist
    ADD CONSTRAINT cbpartner_ipricelist FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_product cbpartner_iproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_product
    ADD CONSTRAINT cbpartner_iproduct FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_discountschemaline cbpartner_mdiscountsline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_discountschemaline
    ADD CONSTRAINT cbpartner_mdiscountsline FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_distributionlistline cbpartner_mdistlistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_distributionlistline
    ADD CONSTRAINT cbpartner_mdistlistline FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_distributionrun cbpartner_mdistributionrun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_distributionrun
    ADD CONSTRAINT cbpartner_mdistributionrun FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout cbpartner_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT cbpartner_minout FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movement cbpartner_mmovement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT cbpartner_mmovement FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productpricevendorbreak cbpartner_mproductpricevendorb; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productpricevendorbreak
    ADD CONSTRAINT cbpartner_mproductpricevendorb FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotionprecondition cbpartner_mpromotionpreconditi; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotionprecondition
    ADD CONSTRAINT cbpartner_mpromotionpreconditi FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_requisitionline cbpartner_mrequisitionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_requisitionline
    ADD CONSTRAINT cbpartner_mrequisitionline FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_rma cbpartner_mrma; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_rma
    ADD CONSTRAINT cbpartner_mrma FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_shipper cbpartner_mshipper; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_shipper
    ADD CONSTRAINT cbpartner_mshipper FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_goalrestriction cbpartner_pagoalrestriction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_goalrestriction
    ADD CONSTRAINT cbpartner_pagoalrestriction FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcolumn cbpartner_pareportcolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT cbpartner_pareportcolumn FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportsource cbpartner_pareportsource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportsource
    ADD CONSTRAINT cbpartner_pareportsource FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_sla_goal cbpartner_paslagoal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_sla_goal
    ADD CONSTRAINT cbpartner_paslagoal FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction cbpartner_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT cbpartner_rrequestaction FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline cbpartner_soline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT cbpartner_soline FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpense cbpartner_stimeexpense; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpense
    ADD CONSTRAINT cbpartner_stimeexpense FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpenseline cbpartner_stimeexpenseline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpenseline
    ADD CONSTRAINT cbpartner_stimeexpenseline FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_aging cbpartner_taging; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_aging
    ADD CONSTRAINT cbpartner_taging FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_distributionrundetail cbpartner_tdrdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_distributionrundetail
    ADD CONSTRAINT cbpartner_tdrdetail FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination cbpartner_vc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT cbpartner_vc FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order cbpartnerbill_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT cbpartnerbill_corder FOREIGN KEY (bill_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo cbpartnercashtrx_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT cbpartnercashtrx_adclientinfo FOREIGN KEY (c_bpartnercashtrx_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfq_topicsubscriber cbpartnerloc_crfqtopicsubr; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq_topicsubscriber
    ADD CONSTRAINT cbpartnerloc_crfqtopicsubr FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_distributionlistline cbpartnerloc_mdistlistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_distributionlistline
    ADD CONSTRAINT cbpartnerloc_mdistlistline FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_change cbpartnerlocation_aassetchange; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_change
    ADD CONSTRAINT cbpartnerlocation_aassetchange FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunningrunentry cbpartnerlocation_cdunningrune; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrunentry
    ADD CONSTRAINT cbpartnerlocation_cdunningrune FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order cbpartnerlocation_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT cbpartnerlocation_corder FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_asset cbpartnerlocation_iasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_asset
    ADD CONSTRAINT cbpartnerlocation_iasset FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bpartner cbpartnerlocation_ibpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bpartner
    ADD CONSTRAINT cbpartnerlocation_ibpartner FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movement cbpartnerlocation_mmovement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT cbpartnerlocation_mmovement FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline cbpartnerlocation_soline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT cbpartnerlocation_soline FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_distributionrundetail cbpartnerlocation_tdrdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_distributionrundetail
    ADD CONSTRAINT cbpartnerlocation_tdrdetail FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order cbpartnerlocbillto_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT cbpartnerlocbillto_iorder FOREIGN KEY (billto_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order cbpartnerpay_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT cbpartnerpay_corder FOREIGN KEY (pay_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset cbpartnersr_aasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset
    ADD CONSTRAINT cbpartnersr_aasset FOREIGN KEY (c_bpartnersr_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project cbpartnersr_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project
    ADD CONSTRAINT cbpartnersr_cproject FOREIGN KEY (c_bpartnersr_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payselectioncheck cbpbankaccount_cpayselectionch; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payselectioncheck
    ADD CONSTRAINT cbpbankaccount_cpayselectionch FOREIGN KEY (c_bp_bankaccount_id) REFERENCES ampere.c_bp_bankaccount(c_bp_bankaccount_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment cbpbankacct_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT cbpbankacct_cpayment FOREIGN KEY (c_bp_bankaccount_id) REFERENCES ampere.c_bp_bankaccount(c_bp_bankaccount_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner cbpgroup_cbpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT cbpgroup_cbpartner FOREIGN KEY (c_bp_group_id) REFERENCES ampere.c_bp_group(c_bp_group_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group_acct cbpgroup_cbpgroupacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT cbpgroup_cbpgroupacct FOREIGN KEY (c_bp_group_id) REFERENCES ampere.c_bp_group(c_bp_group_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commissionline cbpgroup_commissionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissionline
    ADD CONSTRAINT cbpgroup_commissionline FOREIGN KEY (c_bp_group_id) REFERENCES ampere.c_bp_group(c_bp_group_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdefinition cbpgroup_ctaxdefinition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdefinition
    ADD CONSTRAINT cbpgroup_ctaxdefinition FOREIGN KEY (c_bp_group_id) REFERENCES ampere.c_bp_group(c_bp_group_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bpartner cbpgroup_ibpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bpartner
    ADD CONSTRAINT cbpgroup_ibpartner FOREIGN KEY (c_bp_group_id) REFERENCES ampere.c_bp_group(c_bp_group_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotionprecondition cbpgroup_mpromotionpreconditio; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotionprecondition
    ADD CONSTRAINT cbpgroup_mpromotionpreconditio FOREIGN KEY (c_bp_group_id) REFERENCES ampere.c_bp_group(c_bp_group_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_goalrestriction cbpgroup_pagoalrestriction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_goalrestriction
    ADD CONSTRAINT cbpgroup_pagoalrestriction FOREIGN KEY (c_bp_group_id) REFERENCES ampere.c_bp_group(c_bp_group_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_aging cbpgroup_taging; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_aging
    ADD CONSTRAINT cbpgroup_taging FOREIGN KEY (c_bp_group_id) REFERENCES ampere.c_bp_group(c_bp_group_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset cbplocation_aasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset
    ADD CONSTRAINT cbplocation_aasset FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user cbplocation_aduser; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user
    ADD CONSTRAINT cbplocation_aduser FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_relation cbplocation_cbprelation; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_relation
    ADD CONSTRAINT cbplocation_cbprelation FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_relation cbplocation_cbprelationbp; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_relation
    ADD CONSTRAINT cbplocation_cbprelationbp FOREIGN KEY (c_bpartnerrelation_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatchline cbplocation_cinvoicebline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT cbplocation_cinvoicebline FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project cbplocation_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project
    ADD CONSTRAINT cbplocation_cproject FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfq cbplocation_crfq; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq
    ADD CONSTRAINT cbplocation_crfq FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfqresponse cbplocation_crfqresponse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqresponse
    ADD CONSTRAINT cbplocation_crfqresponse FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice cbplocation_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT cbplocation_iinvoice FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_distributionrun cbplocation_mdistributionrun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_distributionrun
    ADD CONSTRAINT cbplocation_mdistributionrun FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order cbplocationbill_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT cbplocationbill_corder FOREIGN KEY (bill_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order cbplocationpay_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT cbplocationpay_corder FOREIGN KEY (pay_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankstatementline cbstatement_cbstatementline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankstatementline
    ADD CONSTRAINT cbstatement_cbstatementline FOREIGN KEY (c_bankstatement_id) REFERENCES ampere.c_bankstatement(c_bankstatement_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_employee_acct cbuspartner_c_bpemployeeacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_employee_acct
    ADD CONSTRAINT cbuspartner_c_bpemployeeacct FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_element cbuspartner_caschemaelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_element
    ADD CONSTRAINT cbuspartner_caschemaelement FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_customer_acct cbuspartner_cbpcustomer_acct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_customer_acct
    ADD CONSTRAINT cbuspartner_cbpcustomer_acct FOREIGN KEY (c_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo ccalendar_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT ccalendar_adclientinfo FOREIGN KEY (c_calendar_id) REFERENCES ampere.c_calendar(c_calendar_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_orginfo ccalendar_adorginfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_orginfo
    ADD CONSTRAINT ccalendar_adorginfo FOREIGN KEY (c_calendar_id) REFERENCES ampere.c_calendar(c_calendar_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_demand ccalendar_mdemand; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_demand
    ADD CONSTRAINT ccalendar_mdemand FOREIGN KEY (c_calendar_id) REFERENCES ampere.c_calendar(c_calendar_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_forecast ccalendar_mforecast; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_forecast
    ADD CONSTRAINT ccalendar_mforecast FOREIGN KEY (c_calendar_id) REFERENCES ampere.c_calendar(c_calendar_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_report ccalendar_pareport; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_report
    ADD CONSTRAINT ccalendar_pareport FOREIGN KEY (c_calendar_id) REFERENCES ampere.c_calendar(c_calendar_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcube ccalendar_pareportcube; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcube
    ADD CONSTRAINT ccalendar_pareportcube FOREIGN KEY (c_calendar_id) REFERENCES ampere.c_calendar(c_calendar_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cash ccampaign_ccash; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cash
    ADD CONSTRAINT ccampaign_ccash FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplan ccampaign_ccashplan; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplan
    ADD CONSTRAINT ccampaign_ccashplan FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplanline ccampaign_ccashplanline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplanline
    ADD CONSTRAINT ccampaign_ccashplanline FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice ccampaign_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT ccampaign_cinvoice FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline ccampaign_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT ccampaign_cinvoiceline FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order ccampaign_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT ccampaign_corder FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline ccampaign_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT ccampaign_corderline FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment ccampaign_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT ccampaign_cpayment FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project ccampaign_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project
    ADD CONSTRAINT ccampaign_cproject FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary ccampaign_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT ccampaign_factacctsummary FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution ccampaign_gldist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT ccampaign_gldist FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distributionline ccampaign_gldistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT ccampaign_gldistline FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal ccampaign_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT ccampaign_ifajournal FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal ccampaign_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT ccampaign_igljournal FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice ccampaign_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT ccampaign_iinvoice FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order ccampaign_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT ccampaign_iorder FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout ccampaign_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT ccampaign_minout FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline ccampaign_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT ccampaign_minoutline FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventory ccampaign_minventory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventory
    ADD CONSTRAINT ccampaign_minventory FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movement ccampaign_mmovement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT ccampaign_mmovement FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_production ccampaign_mproduction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_production
    ADD CONSTRAINT ccampaign_mproduction FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotion ccampaign_mpromotion; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotion
    ADD CONSTRAINT ccampaign_mpromotion FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcolumn ccampaign_pareportcolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT ccampaign_pareportcolumn FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportsource ccampaign_pareportsource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportsource
    ADD CONSTRAINT ccampaign_pareportsource FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request ccampaign_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT ccampaign_rrequest FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpenseline ccampaign_stimeexpenseline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpenseline
    ADD CONSTRAINT ccampaign_stimeexpenseline FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_aging ccampaign_taging; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_aging
    ADD CONSTRAINT ccampaign_taging FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashline ccash_ccashline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashline
    ADD CONSTRAINT ccash_ccashline FOREIGN KEY (c_cash_id) REFERENCES ampere.c_cash(c_cash_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_orginfo ccashbook_adorginfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_orginfo
    ADD CONSTRAINT ccashbook_adorginfo FOREIGN KEY (transfercashbook_id) REFERENCES ampere.c_cashbook(c_cashbook_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cash ccashbook_ccash; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cash
    ADD CONSTRAINT ccashbook_ccash FOREIGN KEY (c_cashbook_id) REFERENCES ampere.c_cashbook(c_cashbook_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashbook_acct ccashbook_ccashbookacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashbook_acct
    ADD CONSTRAINT ccashbook_ccashbookacct FOREIGN KEY (c_cashbook_id) REFERENCES ampere.c_cashbook(c_cashbook_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_allocationline ccashline_callocationline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_allocationline
    ADD CONSTRAINT ccashline_callocationline FOREIGN KEY (c_cashline_id) REFERENCES ampere.c_cashline(c_cashline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice ccashline_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT ccashline_cinvoice FOREIGN KEY (c_cashline_id) REFERENCES ampere.c_cashline(c_cashline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order ccashline_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT ccashline_corder FOREIGN KEY (c_cashline_id) REFERENCES ampere.c_cashline(c_cashline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplanline ccashplan_ccashplanline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplanline
    ADD CONSTRAINT ccashplan_ccashplanline FOREIGN KEY (c_cashplan_id) REFERENCES ampere.c_cashplan(c_cashplan_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice ccashplanline_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT ccashplanline_cinvoice FOREIGN KEY (c_cashplanline_id) REFERENCES ampere.c_cashplanline(c_cashplanline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order ccashplanline_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT ccashplanline_corder FOREIGN KEY (c_cashplanline_id) REFERENCES ampere.c_cashplanline(c_cashplanline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_campaign cchannel_ccampaign; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_campaign
    ADD CONSTRAINT cchannel_ccampaign FOREIGN KEY (c_channel_id) REFERENCES ampere.c_channel(c_channel_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankstatementline ccharge_cbankstmtlime; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankstatementline
    ADD CONSTRAINT ccharge_cbankstmtlime FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashline ccharge_ccashline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashline
    ADD CONSTRAINT ccharge_ccashline FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplanline ccharge_ccashplanline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplanline
    ADD CONSTRAINT ccharge_ccashplanline FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_charge_trl ccharge_cchargetrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_charge_trl
    ADD CONSTRAINT ccharge_cchargetrl FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commission ccharge_ccommission; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commission
    ADD CONSTRAINT ccharge_ccommission FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice ccharge_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT ccharge_cinvoice FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatchline ccharge_cinvoicebatchline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT ccharge_cinvoicebatchline FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline ccharge_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT ccharge_cinvoiceline FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order ccharge_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT ccharge_corder FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline ccharge_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT ccharge_corderline FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment ccharge_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT ccharge_cpayment FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bankstatement ccharge_ibankstmt; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bankstatement
    ADD CONSTRAINT ccharge_ibankstmt FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice ccharge_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT ccharge_iinvoice FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order ccharge_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT ccharge_iorder FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_payment ccharge_ipayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_payment
    ADD CONSTRAINT ccharge_ipayment FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout ccharge_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT ccharge_minout FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline ccharge_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT ccharge_minoutline FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventoryline ccharge_minventoryline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventoryline
    ADD CONSTRAINT ccharge_minventoryline FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movement ccharge_mmovement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT ccharge_mmovement FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotionreward ccharge_mpromotionreward; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotionreward
    ADD CONSTRAINT ccharge_mpromotionreward FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_requisitionline ccharge_mrequisitionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_requisitionline
    ADD CONSTRAINT ccharge_mrequisitionline FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_rmaline ccharge_mrmaline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_rmaline
    ADD CONSTRAINT ccharge_mrmaline FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_charge cchargetype_ccharge; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_charge
    ADD CONSTRAINT cchargetype_ccharge FOREIGN KEY (c_chargetype_id) REFERENCES ampere.c_chargetype(c_chargetype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_chargetype_doctype cchargetype_cchargetypedoctype; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_chargetype_doctype
    ADD CONSTRAINT cchargetype_cchargetypedoctype FOREIGN KEY (c_chargetype_id) REFERENCES ampere.c_chargetype(c_chargetype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_charge_acct cchrage_cchargeacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_charge_acct
    ADD CONSTRAINT cchrage_cchargeacct FOREIGN KEY (c_charge_id) REFERENCES ampere.c_charge(c_charge_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_location ccity_clocation; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_location
    ADD CONSTRAINT ccity_clocation FOREIGN KEY (c_city_id) REFERENCES ampere.c_city(c_city_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commissionamt ccomline_ccomamt; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissionamt
    ADD CONSTRAINT ccomline_ccomamt FOREIGN KEY (c_commissionline_id) REFERENCES ampere.c_commissionline(c_commissionline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commissionamt ccommentrun_ccommissionamt; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissionamt
    ADD CONSTRAINT ccommentrun_ccommissionamt FOREIGN KEY (c_commissionrun_id) REFERENCES ampere.c_commissionrun(c_commissionrun_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commissionline ccommission_ccommissionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissionline
    ADD CONSTRAINT ccommission_ccommissionline FOREIGN KEY (c_commission_id) REFERENCES ampere.c_commission(c_commission_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commissionrun ccommission_ccommissionrun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissionrun
    ADD CONSTRAINT ccommission_ccommissionrun FOREIGN KEY (c_commission_id) REFERENCES ampere.c_commission(c_commission_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commissiondetail ccommissionamt_ccomdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissiondetail
    ADD CONSTRAINT ccommissionamt_ccomdetail FOREIGN KEY (c_commissionamt_id) REFERENCES ampere.c_commissionamt(c_commissionamt_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatch cconventiontype_cinvoicebatch; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatch
    ADD CONSTRAINT cconventiontype_cinvoicebatch FOREIGN KEY (c_conversiontype_id) REFERENCES ampere.c_conversiontype(c_conversiontype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_conversion_rate cconversionrate_iconvrate; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_conversion_rate
    ADD CONSTRAINT cconversionrate_iconvrate FOREIGN KEY (c_conversion_rate_id) REFERENCES ampere.c_conversion_rate(c_conversion_rate_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_conversion_rate cconversiontype_cconvrate; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_conversion_rate
    ADD CONSTRAINT cconversiontype_cconvrate FOREIGN KEY (c_conversiontype_id) REFERENCES ampere.c_conversiontype(c_conversiontype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice cconversiontype_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT cconversiontype_cinvoice FOREIGN KEY (c_conversiontype_id) REFERENCES ampere.c_conversiontype(c_conversiontype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order cconversiontype_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT cconversiontype_corder FOREIGN KEY (c_conversiontype_id) REFERENCES ampere.c_conversiontype(c_conversiontype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment cconversiontype_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT cconversiontype_cpayment FOREIGN KEY (c_conversiontype_id) REFERENCES ampere.c_conversiontype(c_conversiontype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journal cconversiontype_gljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journal
    ADD CONSTRAINT cconversiontype_gljournal FOREIGN KEY (c_conversiontype_id) REFERENCES ampere.c_conversiontype(c_conversiontype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journalline cconversiontype_gljournalline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalline
    ADD CONSTRAINT cconversiontype_gljournalline FOREIGN KEY (c_conversiontype_id) REFERENCES ampere.c_conversiontype(c_conversiontype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal cconversiontype_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT cconversiontype_igljournal FOREIGN KEY (c_conversiontype_id) REFERENCES ampere.c_conversiontype(c_conversiontype_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_discountschemaline cconversiontype_mdiscschline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_discountschemaline
    ADD CONSTRAINT cconversiontype_mdiscschline FOREIGN KEY (c_conversiontype_id) REFERENCES ampere.c_conversiontype(c_conversiontype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_invoicegl cconversiontype_tinvoicegl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_invoicegl
    ADD CONSTRAINT cconversiontype_tinvoicegl FOREIGN KEY (c_conversiontypereval_id) REFERENCES ampere.c_conversiontype(c_conversiontype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_conversion_rate cconvtype_iconvrate; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_conversion_rate
    ADD CONSTRAINT cconvtype_iconvrate FOREIGN KEY (c_conversiontype_id) REFERENCES ampere.c_conversiontype(c_conversiontype_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_landedcost ccostelement_clandedcost; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_landedcost
    ADD CONSTRAINT ccostelement_clandedcost FOREIGN KEY (m_costelement_id) REFERENCES ampere.m_costelement(m_costelement_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_city ccountry_ccity; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_city
    ADD CONSTRAINT ccountry_ccity FOREIGN KEY (c_country_id) REFERENCES ampere.c_country(c_country_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_country ccountry_ccountry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_country
    ADD CONSTRAINT ccountry_ccountry FOREIGN KEY (c_country_id) REFERENCES ampere.c_country(c_country_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_country_trl ccountry_ccountrytrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_country_trl
    ADD CONSTRAINT ccountry_ccountrytrl FOREIGN KEY (c_country_id) REFERENCES ampere.c_country(c_country_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_region ccountry_cregion; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_region
    ADD CONSTRAINT ccountry_cregion FOREIGN KEY (c_country_id) REFERENCES ampere.c_country(c_country_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice ccountry_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT ccountry_iinvoice FOREIGN KEY (c_country_id) REFERENCES ampere.c_country(c_country_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order ccountry_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT ccountry_iorder FOREIGN KEY (c_country_id) REFERENCES ampere.c_country(c_country_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bpartner ccountry_ipartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bpartner
    ADD CONSTRAINT ccountry_ipartner FOREIGN KEY (c_country_id) REFERENCES ampere.c_country(c_country_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_freight ccountry_mfreight; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_freight
    ADD CONSTRAINT ccountry_mfreight FOREIGN KEY (c_country_id) REFERENCES ampere.c_country(c_country_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_freight ccountryto_mfreight; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_freight
    ADD CONSTRAINT ccountryto_mfreight FOREIGN KEY (to_country_id) REFERENCES ampere.c_country(c_country_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_reval_entry ccurrency_aassetrevalentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_reval_entry
    ADD CONSTRAINT ccurrency_aassetrevalentry FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_depreciation_entry ccurrency_adepreciationentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_entry
    ADD CONSTRAINT ccurrency_adepreciationentry FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_registration ccurrency_adregistration; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_registration
    ADD CONSTRAINT ccurrency_adregistration FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_allocationhdr ccurrency_callocation; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_allocationhdr
    ADD CONSTRAINT ccurrency_callocation FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount ccurrency_cbankaccount; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount
    ADD CONSTRAINT ccurrency_cbankaccount FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankstatementline ccurrency_cbankstmtline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankstatementline
    ADD CONSTRAINT ccurrency_cbankstmtline FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashbook ccurrency_ccashbook; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashbook
    ADD CONSTRAINT ccurrency_ccashbook FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashline ccurrency_ccashline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashline
    ADD CONSTRAINT ccurrency_ccashline FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commission ccurrency_ccommission; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commission
    ADD CONSTRAINT ccurrency_ccommission FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commissiondetail ccurrency_ccommissiondetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissiondetail
    ADD CONSTRAINT ccurrency_ccommissiondetail FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_conversion_rate ccurrency_cconversionrate; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_conversion_rate
    ADD CONSTRAINT ccurrency_cconversionrate FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_country ccurrency_ccountry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_country
    ADD CONSTRAINT ccurrency_ccountry FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_currency_acct ccurrency_ccurrencyacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_currency_acct
    ADD CONSTRAINT ccurrency_ccurrencyacct FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_currency_trl ccurrency_ccurrencytrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_currency_trl
    ADD CONSTRAINT ccurrency_ccurrencytrl FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cycle ccurrency_ccycle; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cycle
    ADD CONSTRAINT ccurrency_ccycle FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunningrunentry ccurrency_cdunningrunentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrunentry
    ADD CONSTRAINT ccurrency_cdunningrunentry FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_elementvalue ccurrency_celementvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_elementvalue
    ADD CONSTRAINT ccurrency_celementvalue FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice ccurrency_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT ccurrency_cinvoice FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatch ccurrency_cinvoicebatch; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatch
    ADD CONSTRAINT ccurrency_cinvoicebatch FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order ccurrency_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT ccurrency_corder FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline ccurrency_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT ccurrency_corderline FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment ccurrency_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT ccurrency_cpayment FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_paymentprocessor ccurrency_cpaymentprocessor; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_paymentprocessor
    ADD CONSTRAINT ccurrency_cpaymentprocessor FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project ccurrency_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project
    ADD CONSTRAINT ccurrency_cproject FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_revenuerecognition_plan ccurrency_crevenuerecplan; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_revenuerecognition_plan
    ADD CONSTRAINT ccurrency_crevenuerecplan FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfq ccurrency_crfq; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq
    ADD CONSTRAINT ccurrency_crfq FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfqresponse ccurrency_crfqresponse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqresponse
    ADD CONSTRAINT ccurrency_crfqresponse FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdeclarationline ccurrency_ctaxdeclline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdeclarationline
    ADD CONSTRAINT ccurrency_ctaxdeclline FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journal ccurrency_gljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journal
    ADD CONSTRAINT ccurrency_gljournal FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journalbatch ccurrency_gljournalbatch; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalbatch
    ADD CONSTRAINT ccurrency_gljournalbatch FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journalline ccurrency_gljournalline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalline
    ADD CONSTRAINT ccurrency_gljournalline FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bankstatement ccurrency_ibankstatement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bankstatement
    ADD CONSTRAINT ccurrency_ibankstatement FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_conversion_rate ccurrency_iconvrate; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_conversion_rate
    ADD CONSTRAINT ccurrency_iconvrate FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_conversion_rate ccurrency_iconvrateto; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_conversion_rate
    ADD CONSTRAINT ccurrency_iconvrateto FOREIGN KEY (c_currency_id_to) REFERENCES ampere.c_currency(c_currency_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal ccurrency_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT ccurrency_ifajournal FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal ccurrency_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT ccurrency_igljournal FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice ccurrency_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT ccurrency_iinvoice FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order ccurrency_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT ccurrency_iorder FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_payment ccurrency_ipayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_payment
    ADD CONSTRAINT ccurrency_ipayment FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_pricelist ccurrency_ipricelist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_pricelist
    ADD CONSTRAINT ccurrency_ipricelist FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_product ccurrency_iproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_product
    ADD CONSTRAINT ccurrency_iproduct FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_freight ccurrency_mfreight; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_freight
    ADD CONSTRAINT ccurrency_mfreight FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_pricelist ccurrency_mpricelist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_pricelist
    ADD CONSTRAINT ccurrency_mpricelist FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_po ccurrency_mproductpo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_po
    ADD CONSTRAINT ccurrency_mproductpo FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_rma ccurrency_mrma; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_rma
    ADD CONSTRAINT ccurrency_mrma FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcolumn ccurrency_pareportcolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT ccurrency_pareportcolumn FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpenseline ccurrency_stimeexpenseline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpenseline
    ADD CONSTRAINT ccurrency_stimeexpenseline FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_aging ccurrency_taging; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_aging
    ADD CONSTRAINT ccurrency_taging FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_inventoryvalue ccurrency_tinventoryvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_inventoryvalue
    ADD CONSTRAINT ccurrency_tinventoryvalue FOREIGN KEY (c_currency_id) REFERENCES ampere.c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cyclestep ccycle_ccyclestep; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cyclestep
    ADD CONSTRAINT ccycle_ccyclestep FOREIGN KEY (c_cycle_id) REFERENCES ampere.c_cycle(c_cycle_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cyclephase ccyclestep_ccyclephase; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cyclephase
    ADD CONSTRAINT ccyclestep_ccyclephase FOREIGN KEY (c_cyclestep_id) REFERENCES ampere.c_cyclestep(c_cyclestep_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_reval_entry cdoctype_aassetrevalentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_reval_entry
    ADD CONSTRAINT cdoctype_aassetrevalentry FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_document_action_access cdoctype_addocumentactionacces; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_document_action_access
    ADD CONSTRAINT cdoctype_addocumentactionacces FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_depreciation_entry cdoctype_adepreciationentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_entry
    ADD CONSTRAINT cdoctype_adepreciationentry FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_chargetype_doctype cdoctype_cchargetypedoctype; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_chargetype_doctype
    ADD CONSTRAINT cdoctype_cchargetypedoctype FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_doctypecounter cdoctype_cdoctypecounter; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_doctypecounter
    ADD CONSTRAINT cdoctype_cdoctypecounter FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_doctype_trl cdoctype_cdoctypetrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_doctype_trl
    ADD CONSTRAINT cdoctype_cdoctypetrl FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice cdoctype_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT cdoctype_cinvoice FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatchline cdoctype_cinvoicebatchline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT cdoctype_cinvoicebatchline FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment cdoctype_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT cdoctype_cpayment FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution cdoctype_gldistribution; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT cdoctype_gldistribution FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journal cdoctype_gljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journal
    ADD CONSTRAINT cdoctype_gljournal FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journalbatch cdoctype_gljournalbatch; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalbatch
    ADD CONSTRAINT cdoctype_gljournalbatch FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal cdoctype_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT cdoctype_ifajournal FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal cdoctype_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT cdoctype_igljournal FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice cdoctype_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT cdoctype_iinvoice FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_doctype cdoctype_invoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_doctype
    ADD CONSTRAINT cdoctype_invoice FOREIGN KEY (c_doctypeinvoice_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order cdoctype_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT cdoctype_iorder FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_payment cdoctype_ipayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_payment
    ADD CONSTRAINT cdoctype_ipayment FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout cdoctype_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT cdoctype_minout FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventory cdoctype_minventory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventory
    ADD CONSTRAINT cdoctype_minventory FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movement cdoctype_mmovement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT cdoctype_mmovement FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_requisition cdoctype_mrequisition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_requisition
    ADD CONSTRAINT cdoctype_mrequisition FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_rma cdoctype_mrma; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_rma
    ADD CONSTRAINT cdoctype_mrma FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_doctype cdoctype_proforma; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_doctype
    ADD CONSTRAINT cdoctype_proforma FOREIGN KEY (c_doctypeproforma_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_doctype cdoctype_shipment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_doctype
    ADD CONSTRAINT cdoctype_shipment FOREIGN KEY (c_doctypeshipment_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_invoicegl cdoctype_tinvoicegl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_invoicegl
    ADD CONSTRAINT cdoctype_tinvoicegl FOREIGN KEY (c_doctypereval_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_replenish cdoctype_treplenish; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_replenish
    ADD CONSTRAINT cdoctype_treplenish FOREIGN KEY (c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_doctypecounter cdoctypecount_cdoctypecount; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_doctypecounter
    ADD CONSTRAINT cdoctypecount_cdoctypecount FOREIGN KEY (counter_c_doctype_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_doctype cdoctypedifference_cdoctype; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_doctype
    ADD CONSTRAINT cdoctypedifference_cdoctype FOREIGN KEY (c_doctypedifference_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice cdoctypetarget_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT cdoctypetarget_cinvoice FOREIGN KEY (c_doctypetarget_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order cdoctypetarget_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT cdoctypetarget_corder FOREIGN KEY (c_doctypetarget_id) REFERENCES ampere.c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner cdunning_cbpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT cdunning_cbpartner FOREIGN KEY (c_dunning_id) REFERENCES ampere.c_dunning(c_dunning_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group cdunning_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group
    ADD CONSTRAINT cdunning_cbpgroup FOREIGN KEY (c_dunning_id) REFERENCES ampere.c_dunning(c_dunning_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunninglevel cdunning_cdunninglevel; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunninglevel
    ADD CONSTRAINT cdunning_cdunninglevel FOREIGN KEY (c_dunning_id) REFERENCES ampere.c_dunning(c_dunning_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunningrun cdunning_cdunningrun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrun
    ADD CONSTRAINT cdunning_cdunningrun FOREIGN KEY (c_dunning_id) REFERENCES ampere.c_dunning(c_dunning_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunninglevel_trl cdunninglevel_cdltrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunninglevel_trl
    ADD CONSTRAINT cdunninglevel_cdltrl FOREIGN KEY (c_dunninglevel_id) REFERENCES ampere.c_dunninglevel(c_dunninglevel_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunningrun cdunninglevel_cdunningrun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrun
    ADD CONSTRAINT cdunninglevel_cdunningrun FOREIGN KEY (c_dunninglevel_id) REFERENCES ampere.c_dunninglevel(c_dunninglevel_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunningrunentry cdunninglevel_cdunningrunentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrunentry
    ADD CONSTRAINT cdunninglevel_cdunningrunentry FOREIGN KEY (c_dunninglevel_id) REFERENCES ampere.c_dunninglevel(c_dunninglevel_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice cdunninglevel_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT cdunninglevel_cinvoice FOREIGN KEY (c_dunninglevel_id) REFERENCES ampere.c_dunninglevel(c_dunninglevel_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunningrunentry cdunningrun_cdunningrunentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrunentry
    ADD CONSTRAINT cdunningrun_cdunningrunentry FOREIGN KEY (c_dunningrun_id) REFERENCES ampere.c_dunningrun(c_dunningrun_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunningrunline cdunningrunentry_line; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrunline
    ADD CONSTRAINT cdunningrunentry_line FOREIGN KEY (c_dunningrunentry_id) REFERENCES ampere.c_dunningrunentry(c_dunningrunentry_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline celemenrvalueuser1_cinvline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT celemenrvalueuser1_cinvline FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline celemenrvalueuser1_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT celemenrvalueuser1_corderline FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline celemenrvalueuser1_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT celemenrvalueuser1_minoutline FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline celemenrvalueuser2_cinvline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT celemenrvalueuser2_cinvline FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order celemenrvalueuser2_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT celemenrvalueuser2_corder FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline celemenrvalueuser2_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT celemenrvalueuser2_corderline FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline celemenrvalueuser2_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT celemenrvalueuser2_minoutline FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_element celement_caschemaelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_element
    ADD CONSTRAINT celement_caschemaelement FOREIGN KEY (c_element_id) REFERENCES ampere.c_element(c_element_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_elementvalue celement_celementvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_elementvalue
    ADD CONSTRAINT celement_celementvalue FOREIGN KEY (c_element_id) REFERENCES ampere.c_element(c_element_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_elementvalue celement_ielementvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_elementvalue
    ADD CONSTRAINT celement_ielementvalue FOREIGN KEY (c_element_id) REFERENCES ampere.c_element(c_element_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_element celementvalue_caschemaelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_element
    ADD CONSTRAINT celementvalue_caschemaelement FOREIGN KEY (c_elementvalue_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_elementvalue_trl celementvalue_cevaluetrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_elementvalue_trl
    ADD CONSTRAINT celementvalue_cevaluetrl FOREIGN KEY (c_elementvalue_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_subacct celementvalue_csubacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_subacct
    ADD CONSTRAINT celementvalue_csubacct FOREIGN KEY (c_elementvalue_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct celementvalue_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT celementvalue_factacct FOREIGN KEY (account_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_fundrestriction celementvalue_glfundrestr; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_fundrestriction
    ADD CONSTRAINT celementvalue_glfundrestr FOREIGN KEY (c_elementvalue_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_elementvalue celementvalue_ielementvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_elementvalue
    ADD CONSTRAINT celementvalue_ielementvalue FOREIGN KEY (c_elementvalue_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_reportline celementvalue_ireportline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_reportline
    ADD CONSTRAINT celementvalue_ireportline FOREIGN KEY (c_elementvalue_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_ratioelement celementvalue_paratioelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_ratioelement
    ADD CONSTRAINT celementvalue_paratioelement FOREIGN KEY (account_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcolumn celementvalue_pareportcolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT celementvalue_pareportcolumn FOREIGN KEY (c_elementvalue_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportsource celementvalue_pareportsource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportsource
    ADD CONSTRAINT celementvalue_pareportsource FOREIGN KEY (c_elementvalue_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination celementvalueaccount_vc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT celementvalueaccount_vc FOREIGN KEY (account_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatchline celementvalueu1_cinvoicebline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT celementvalueu1_cinvoicebline FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatchline celementvalueu2_cinvoicebline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT celementvalueu2_cinvoicebline FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cash celementvalueuser1_ccash; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cash
    ADD CONSTRAINT celementvalueuser1_ccash FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice celementvalueuser1_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT celementvalueuser1_cinvoice FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order celementvalueuser1_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT celementvalueuser1_corder FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment celementvalueuser1_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT celementvalueuser1_cpayment FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct celementvalueuser1_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT celementvalueuser1_factacct FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout celementvalueuser1_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT celementvalueuser1_minout FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventory celementvalueuser1_minvent; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventory
    ADD CONSTRAINT celementvalueuser1_minvent FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movement celementvalueuser1_mmove; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT celementvalueuser1_mmove FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_production celementvalueuser1_mprod; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_production
    ADD CONSTRAINT celementvalueuser1_mprod FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination celementvalueuser1_vc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT celementvalueuser1_vc FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cash celementvalueuser2_ccash; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cash
    ADD CONSTRAINT celementvalueuser2_ccash FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice celementvalueuser2_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT celementvalueuser2_cinvoice FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment celementvalueuser2_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT celementvalueuser2_cpayment FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct celementvalueuser2_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT celementvalueuser2_factacct FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout celementvalueuser2_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT celementvalueuser2_minout FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventory celementvalueuser2_minvent; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventory
    ADD CONSTRAINT celementvalueuser2_minvent FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movement celementvalueuser2_mmove; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT celementvalueuser2_mmove FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_production celementvalueuser2_mprod; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_production
    ADD CONSTRAINT celementvalueuser2_mprod FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination celementvalueuser2_vc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT celementvalueuser2_vc FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal celvalueaccount_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT celvalueaccount_igljournal FOREIGN KEY (account_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal celvalueuser2_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT celvalueuser2_igljournal FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution cevalueacct_gldist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT cevalueacct_gldist FOREIGN KEY (account_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distributionline cevalueacct_gldistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT cevalueacct_gldistline FOREIGN KEY (account_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_elementvalue cevalueparent_ielementvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_elementvalue
    ADD CONSTRAINT cevalueparent_ielementvalue FOREIGN KEY (parentelementvalue_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution cevalueuser1_gldist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT cevalueuser1_gldist FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distributionline cevalueuser1_gldistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT cevalueuser1_gldistline FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal cevalueuser1_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT cevalueuser1_igljournal FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution cevalueuser2_gldist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT cevalueuser2_gldist FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distributionline cevalueuser2_gldistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT cevalueuser2_gldistline FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user cgreeting_aduser; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user
    ADD CONSTRAINT cgreeting_aduser FOREIGN KEY (c_greeting_id) REFERENCES ampere.c_greeting(c_greeting_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner cgreeting_cbpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT cgreeting_cbpartner FOREIGN KEY (c_greeting_id) REFERENCES ampere.c_greeting(c_greeting_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_greeting_trl cgreeting_cgreetingtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_greeting_trl
    ADD CONSTRAINT cgreeting_cgreetingtrl FOREIGN KEY (c_greeting_id) REFERENCES ampere.c_greeting(c_greeting_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bpartner cgreeting_ibpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bpartner
    ADD CONSTRAINT cgreeting_ibpartner FOREIGN KEY (c_greeting_id) REFERENCES ampere.c_greeting(c_greeting_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice cinvliceline_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT cinvliceline_iinvoice FOREIGN KEY (c_invoiceline_id) REFERENCES ampere.c_invoiceline(c_invoiceline_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_addition cinvoice_aassetaddition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_addition
    ADD CONSTRAINT cinvoice_aassetaddition FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_allocationline cinvoice_callocationline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_allocationline
    ADD CONSTRAINT cinvoice_callocationline FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankstatementline cinvoice_cbankstatementline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankstatementline
    ADD CONSTRAINT cinvoice_cbankstatementline FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashline cinvoice_ccashline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashline
    ADD CONSTRAINT cinvoice_ccashline FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunningrunline cinvoice_cdunningrunline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrunline
    ADD CONSTRAINT cinvoice_cdunningrunline FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatchline cinvoice_cinvoicebatchline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT cinvoice_cinvoicebatchline FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline cinvoice_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT cinvoice_cinvoiceline FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicepayschedule cinvoice_cinvoicepaysched; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicepayschedule
    ADD CONSTRAINT cinvoice_cinvoicepaysched FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicetax cinvoice_cinvoicetax; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicetax
    ADD CONSTRAINT cinvoice_cinvoicetax FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment cinvoice_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT cinvoice_cpayment FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_paymentallocate cinvoice_cpaymentallocate; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_paymentallocate
    ADD CONSTRAINT cinvoice_cpaymentallocate FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payselectionline cinvoice_cpayselectline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payselectionline
    ADD CONSTRAINT cinvoice_cpayselectline FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_recurring cinvoice_crecurring; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_recurring
    ADD CONSTRAINT cinvoice_crecurring FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_recurring_run cinvoice_crecurringrun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_recurring_run
    ADD CONSTRAINT cinvoice_crecurringrun FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdeclarationline cinvoice_ctaxdeclline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdeclarationline
    ADD CONSTRAINT cinvoice_ctaxdeclline FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bankstatement cinvoice_ibankstatement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bankstatement
    ADD CONSTRAINT cinvoice_ibankstatement FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice cinvoice_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT cinvoice_iinvoice FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_payment cinvoice_ipayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_payment
    ADD CONSTRAINT cinvoice_ipayment FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout cinvoice_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT cinvoice_minout FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutconfirm cinvoice_minoutconfirm; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutconfirm
    ADD CONSTRAINT cinvoice_minoutconfirm FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice cinvoice_ref; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT cinvoice_ref FOREIGN KEY (ref_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction cinvoice_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT cinvoice_rrequestaction FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_invoicegl cinvoice_tinvoicegl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_invoicegl
    ADD CONSTRAINT cinvoice_tinvoicegl FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatchline cinvoicebatch_cinvoicebline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT cinvoicebatch_cinvoicebline FOREIGN KEY (c_invoicebatch_id) REFERENCES ampere.c_invoicebatch(c_invoicebatch_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_addition cinvoiceline_aassetaddition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_addition
    ADD CONSTRAINT cinvoiceline_aassetaddition FOREIGN KEY (c_invoiceline_id) REFERENCES ampere.c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_retirement cinvoiceline_aassetretirement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_retirement
    ADD CONSTRAINT cinvoiceline_aassetretirement FOREIGN KEY (c_invoiceline_id) REFERENCES ampere.c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commissiondetail cinvoiceline_ccommissiondet; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissiondetail
    ADD CONSTRAINT cinvoiceline_ccommissiondet FOREIGN KEY (c_invoiceline_id) REFERENCES ampere.c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatchline cinvoiceline_cinvoicebline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT cinvoiceline_cinvoicebline FOREIGN KEY (c_invoiceline_id) REFERENCES ampere.c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_landedcost cinvoiceline_clandedcost; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_landedcost
    ADD CONSTRAINT cinvoiceline_clandedcost FOREIGN KEY (c_invoiceline_id) REFERENCES ampere.c_invoiceline(c_invoiceline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_landedcostallocation cinvoiceline_clandedcostalloc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_landedcostallocation
    ADD CONSTRAINT cinvoiceline_clandedcostalloc FOREIGN KEY (c_invoiceline_id) REFERENCES ampere.c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_revenuerecognition_plan cinvoiceline_crevenuerecplan; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_revenuerecognition_plan
    ADD CONSTRAINT cinvoiceline_crevenuerecplan FOREIGN KEY (c_invoiceline_id) REFERENCES ampere.c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdeclarationline cinvoiceline_ctaxdeclline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdeclarationline
    ADD CONSTRAINT cinvoiceline_ctaxdeclline FOREIGN KEY (c_invoiceline_id) REFERENCES ampere.c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutlineconfirm cinvoiceline_minoutlineconfirm; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutlineconfirm
    ADD CONSTRAINT cinvoiceline_minoutlineconfirm FOREIGN KEY (c_invoiceline_id) REFERENCES ampere.c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_matchpo cinvoiceline_mmatchpo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_matchpo
    ADD CONSTRAINT cinvoiceline_mmatchpo FOREIGN KEY (c_invoiceline_id) REFERENCES ampere.c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpenseline cinvoiceline_stimeexpenseline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpenseline
    ADD CONSTRAINT cinvoiceline_stimeexpenseline FOREIGN KEY (c_invoiceline_id) REFERENCES ampere.c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunningrunline cinvoicepayschedule_cdunningru; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrunline
    ADD CONSTRAINT cinvoicepayschedule_cdunningru FOREIGN KEY (c_invoicepayschedule_id) REFERENCES ampere.c_invoicepayschedule(c_invoicepayschedule_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request cinvoicerequest_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT cinvoicerequest_rrequest FOREIGN KEY (c_invoicerequest_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner cinvoiceschedule_cbpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT cinvoiceschedule_cbpartner FOREIGN KEY (c_invoiceschedule_id) REFERENCES ampere.c_invoiceschedule(c_invoiceschedule_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_user cjob_aduser; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_user
    ADD CONSTRAINT cjob_aduser FOREIGN KEY (c_job_id) REFERENCES ampere.c_job(c_job_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_jobassignment cjob_cjobassignment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_jobassignment
    ADD CONSTRAINT cjob_cjobassignment FOREIGN KEY (c_job_id) REFERENCES ampere.c_job(c_job_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_jobremuneration cjob_cjobremuneration; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_jobremuneration
    ADD CONSTRAINT cjob_cjobremuneration FOREIGN KEY (c_job_id) REFERENCES ampere.c_job(c_job_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_operationresource cjob_moperationresource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_operationresource
    ADD CONSTRAINT cjob_moperationresource FOREIGN KEY (c_job_id) REFERENCES ampere.c_job(c_job_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_job cjobcategory_cjob; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_job
    ADD CONSTRAINT cjobcategory_cjob FOREIGN KEY (c_jobcategory_id) REFERENCES ampere.c_jobcategory(c_jobcategory_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset clocation_aasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset
    ADD CONSTRAINT clocation_aasset FOREIGN KEY (c_location_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_change clocation_aassetchange; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_change
    ADD CONSTRAINT clocation_aassetchange FOREIGN KEY (c_location_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_registration clocation_adregistration; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_registration
    ADD CONSTRAINT clocation_adregistration FOREIGN KEY (c_location_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_element clocation_caschemaelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_element
    ADD CONSTRAINT clocation_caschemaelement FOREIGN KEY (c_location_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bank clocation_cbank; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bank
    ADD CONSTRAINT clocation_cbank FOREIGN KEY (c_location_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner_location clocation_cbplocation; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner_location
    ADD CONSTRAINT clocation_cbplocation FOREIGN KEY (c_location_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_asset clocation_iasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_asset
    ADD CONSTRAINT clocation_iasset FOREIGN KEY (c_location_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice clocation_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT clocation_iinvoice FOREIGN KEY (c_location_id) REFERENCES ampere.c_location(c_location_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order clocation_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT clocation_iorder FOREIGN KEY (c_location_id) REFERENCES ampere.c_location(c_location_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcolumn clocation_pareportcolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT clocation_pareportcolumn FOREIGN KEY (c_location_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportsource clocation_pareportsource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportsource
    ADD CONSTRAINT clocation_pareportsource FOREIGN KEY (c_location_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution clocfrom_gldist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT clocfrom_gldist FOREIGN KEY (c_locfrom_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distributionline clocfrom_gldistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT clocfrom_gldistline FOREIGN KEY (c_locfrom_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal clocfrom_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT clocfrom_igljournal FOREIGN KEY (c_locfrom_id) REFERENCES ampere.c_location(c_location_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution clocto_gldist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT clocto_gldist FOREIGN KEY (c_locto_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distributionline clocto_gldistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT clocto_gldistline FOREIGN KEY (c_locto_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal clocto_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT clocto_igljournal FOREIGN KEY (c_locto_id) REFERENCES ampere.c_location(c_location_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_column columnclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column
    ADD CONSTRAINT columnclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_column columnorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column
    ADD CONSTRAINT columnorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_gl commitmentoffset_cacctschemagl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_gl
    ADD CONSTRAINT commitmentoffset_cacctschemagl FOREIGN KEY (commitmentoffset_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_gl commitmentoffsetsales_cacctsch; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_gl
    ADD CONSTRAINT commitmentoffsetsales_cacctsch FOREIGN KEY (commitmentoffsetsales_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_matchinv convoiceline_mmatchinv; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_matchinv
    ADD CONSTRAINT convoiceline_mmatchinv FOREIGN KEY (c_invoiceline_id) REFERENCES ampere.c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline convoiceline_ref; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT convoiceline_ref FOREIGN KEY (ref_invoiceline_id) REFERENCES ampere.c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_allocationline corder_callocation; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_allocationline
    ADD CONSTRAINT corder_callocation FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice corder_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT corder_cinvoice FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline corder_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT corder_corderline FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderpayschedule corder_corderpayschedule; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderpayschedule
    ADD CONSTRAINT corder_corderpayschedule FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_ordertax corder_cordertax; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_ordertax
    ADD CONSTRAINT corder_cordertax FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment corder_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT corder_cpayment FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectline corder_cprojectline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectline
    ADD CONSTRAINT corder_cprojectline FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectphase corder_cprojectphase; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectphase
    ADD CONSTRAINT corder_cprojectphase FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_recurring corder_crecurring; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_recurring
    ADD CONSTRAINT corder_crecurring FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_recurring_run corder_crecurringrun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_recurring_run
    ADD CONSTRAINT corder_crecurringrun FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfq corder_crfq; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq
    ADD CONSTRAINT corder_crfq FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfqresponse corder_crfqresponse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqresponse
    ADD CONSTRAINT corder_crfqresponse FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order corder_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT corder_iorder FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout corder_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT corder_minout FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_rma corder_mrma; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_rma
    ADD CONSTRAINT corder_mrma FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order corder_ref; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT corder_ref FOREIGN KEY (ref_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request corder_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT corder_rrequest FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction corder_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT corder_rrequestaction FOREIGN KEY (c_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commissiondetail corderline_ccommissiondetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissiondetail
    ADD CONSTRAINT corderline_ccommissiondetail FOREIGN KEY (c_orderline_id) REFERENCES ampere.c_orderline(c_orderline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline corderline_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT corderline_cinvoiceline FOREIGN KEY (c_orderline_id) REFERENCES ampere.c_orderline(c_orderline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order corderline_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT corderline_iorder FOREIGN KEY (c_orderline_id) REFERENCES ampere.c_orderline(c_orderline_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_demanddetail corderline_mdemanddetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_demanddetail
    ADD CONSTRAINT corderline_mdemanddetail FOREIGN KEY (c_orderline_id) REFERENCES ampere.c_orderline(c_orderline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline corderline_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT corderline_minout FOREIGN KEY (c_orderline_id) REFERENCES ampere.c_orderline(c_orderline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_matchpo corderline_mmatchpo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_matchpo
    ADD CONSTRAINT corderline_mmatchpo FOREIGN KEY (c_orderline_id) REFERENCES ampere.c_orderline(c_orderline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_requisitionline corderline_mrequisitionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_requisitionline
    ADD CONSTRAINT corderline_mrequisitionline FOREIGN KEY (c_orderline_id) REFERENCES ampere.c_orderline(c_orderline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline corderline_ref; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT corderline_ref FOREIGN KEY (ref_orderline_id) REFERENCES ampere.c_orderline(c_orderline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpenseline corderline_stimeexpenseline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpenseline
    ADD CONSTRAINT corderline_stimeexpenseline FOREIGN KEY (c_orderline_id) REFERENCES ampere.c_orderline(c_orderline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectline corderpo_cprojectline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectline
    ADD CONSTRAINT corderpo_cprojectline FOREIGN KEY (c_orderpo_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order cordersource_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT cordersource_iorder FOREIGN KEY (c_ordersource_id) REFERENCES ampere.c_ordersource(c_ordersource_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_allocationline cpayment_callocationline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_allocationline
    ADD CONSTRAINT cpayment_callocationline FOREIGN KEY (c_payment_id) REFERENCES ampere.c_payment(c_payment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankstatementline cpayment_cbankstmtline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankstatementline
    ADD CONSTRAINT cpayment_cbankstmtline FOREIGN KEY (c_payment_id) REFERENCES ampere.c_payment(c_payment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashline cpayment_ccashline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashline
    ADD CONSTRAINT cpayment_ccashline FOREIGN KEY (c_payment_id) REFERENCES ampere.c_payment(c_payment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunningrunline cpayment_cdunningrunline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrunline
    ADD CONSTRAINT cpayment_cdunningrunline FOREIGN KEY (c_payment_id) REFERENCES ampere.c_payment(c_payment_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice cpayment_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT cpayment_cinvoice FOREIGN KEY (c_payment_id) REFERENCES ampere.c_payment(c_payment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order cpayment_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT cpayment_corder FOREIGN KEY (c_payment_id) REFERENCES ampere.c_payment(c_payment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_paymentallocate cpayment_cpaymentallocate; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_paymentallocate
    ADD CONSTRAINT cpayment_cpaymentallocate FOREIGN KEY (c_payment_id) REFERENCES ampere.c_payment(c_payment_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payselectioncheck cpayment_cpayselectioncheck; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payselectioncheck
    ADD CONSTRAINT cpayment_cpayselectioncheck FOREIGN KEY (c_payment_id) REFERENCES ampere.c_payment(c_payment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_recurring cpayment_crecurring; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_recurring
    ADD CONSTRAINT cpayment_crecurring FOREIGN KEY (c_payment_id) REFERENCES ampere.c_payment(c_payment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_recurring_run cpayment_crecurringrun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_recurring_run
    ADD CONSTRAINT cpayment_crecurringrun FOREIGN KEY (c_payment_id) REFERENCES ampere.c_payment(c_payment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bankstatement cpayment_ibankstatement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bankstatement
    ADD CONSTRAINT cpayment_ibankstatement FOREIGN KEY (c_payment_id) REFERENCES ampere.c_payment(c_payment_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_payment cpayment_ipayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_payment
    ADD CONSTRAINT cpayment_ipayment FOREIGN KEY (c_payment_id) REFERENCES ampere.c_payment(c_payment_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request cpayment_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT cpayment_rrequest FOREIGN KEY (c_payment_id) REFERENCES ampere.c_payment(c_payment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction cpayment_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT cpayment_rrequestaction FOREIGN KEY (c_payment_id) REFERENCES ampere.c_payment(c_payment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment cpaymentbatch_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT cpaymentbatch_cpayment FOREIGN KEY (c_paymentbatch_id) REFERENCES ampere.c_paymentbatch(c_paymentbatch_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner cpaymentterm_cbpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT cpaymentterm_cbpartner FOREIGN KEY (c_paymentterm_id) REFERENCES ampere.c_paymentterm(c_paymentterm_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunninglevel cpaymentterm_cdunninglevel; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunninglevel
    ADD CONSTRAINT cpaymentterm_cdunninglevel FOREIGN KEY (c_paymentterm_id) REFERENCES ampere.c_paymentterm(c_paymentterm_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice cpaymentterm_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT cpaymentterm_cinvoice FOREIGN KEY (c_paymentterm_id) REFERENCES ampere.c_paymentterm(c_paymentterm_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payschedule cpaymentterm_cpayschedule; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payschedule
    ADD CONSTRAINT cpaymentterm_cpayschedule FOREIGN KEY (c_paymentterm_id) REFERENCES ampere.c_paymentterm(c_paymentterm_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_paymentterm_trl cpaymentterm_cpaytermtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_paymentterm_trl
    ADD CONSTRAINT cpaymentterm_cpaytermtrl FOREIGN KEY (c_paymentterm_id) REFERENCES ampere.c_paymentterm(c_paymentterm_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project cpaymentterm_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project
    ADD CONSTRAINT cpaymentterm_cproject FOREIGN KEY (c_paymentterm_id) REFERENCES ampere.c_paymentterm(c_paymentterm_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_withholding cpaymentterm_cwithholding; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_withholding
    ADD CONSTRAINT cpaymentterm_cwithholding FOREIGN KEY (c_paymentterm_id) REFERENCES ampere.c_paymentterm(c_paymentterm_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice cpaymentterm_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT cpaymentterm_iinvoice FOREIGN KEY (c_paymentterm_id) REFERENCES ampere.c_paymentterm(c_paymentterm_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order cpaymentterm_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT cpaymentterm_iorder FOREIGN KEY (c_paymentterm_id) REFERENCES ampere.c_paymentterm(c_paymentterm_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order cpaymentterm_soheader; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT cpaymentterm_soheader FOREIGN KEY (c_paymentterm_id) REFERENCES ampere.c_paymentterm(c_paymentterm_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_paymentallocate cpaymtallocate_callocationline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_paymentallocate
    ADD CONSTRAINT cpaymtallocate_callocationline FOREIGN KEY (c_allocationline_id) REFERENCES ampere.c_allocationline(c_allocationline_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_paymentbatch cpaymtprocessor_cpaymtbatch; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_paymentbatch
    ADD CONSTRAINT cpaymtprocessor_cpaymtbatch FOREIGN KEY (c_paymentprocessor_id) REFERENCES ampere.c_paymentprocessor(c_paymentprocessor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicepayschedule cpayschedule_cinvoicepaysched; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicepayschedule
    ADD CONSTRAINT cpayschedule_cinvoicepaysched FOREIGN KEY (c_payschedule_id) REFERENCES ampere.c_payschedule(c_payschedule_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderpayschedule cpayschedule_corderpayschedule; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderpayschedule
    ADD CONSTRAINT cpayschedule_corderpayschedule FOREIGN KEY (c_payschedule_id) REFERENCES ampere.c_payschedule(c_payschedule_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payselectionline cpaysel_cpayselline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payselectionline
    ADD CONSTRAINT cpaysel_cpayselline FOREIGN KEY (c_payselection_id) REFERENCES ampere.c_payselection(c_payselection_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payselectionline cpayselcheck_cpayselline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payselectionline
    ADD CONSTRAINT cpayselcheck_cpayselline FOREIGN KEY (c_payselectioncheck_id) REFERENCES ampere.c_payselectioncheck(c_payselectioncheck_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payselectioncheck cpayselection_cpayselectcheck; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payselectioncheck
    ADD CONSTRAINT cpayselection_cpayselectcheck FOREIGN KEY (c_payselection_id) REFERENCES ampere.c_payselection(c_payselection_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_disposed cperiod_aassetdisposed; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_disposed
    ADD CONSTRAINT cperiod_aassetdisposed FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_reval_entry cperiod_aassetrevalentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_reval_entry
    ADD CONSTRAINT cperiod_aassetrevalentry FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_split cperiod_aassetsplit; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_split
    ADD CONSTRAINT cperiod_aassetsplit FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_transfer cperiod_aassettransfer; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_transfer
    ADD CONSTRAINT cperiod_aassettransfer FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_depreciation_build cperiod_adepreciationbuild; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_build
    ADD CONSTRAINT cperiod_adepreciationbuild FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_depreciation_entry cperiod_adepreciationentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_entry
    ADD CONSTRAINT cperiod_adepreciationentry FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema cperiod_cacctschema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema
    ADD CONSTRAINT cperiod_cacctschema FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct cperiod_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT cperiod_factacct FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary cperiod_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT cperiod_factacctsummary FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal cperiod_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT cperiod_ifajournal FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal cperiod_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT cperiod_igljournal FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_demandline cperiod_mdemandline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_demandline
    ADD CONSTRAINT cperiod_mdemandline FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_forecastline cperiod_mforecastline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_forecastline
    ADD CONSTRAINT cperiod_mforecastline FOREIGN KEY (c_period_id) REFERENCES ampere.c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cyclephase cphase_ccyclephase; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cyclephase
    ADD CONSTRAINT cphase_ccyclephase FOREIGN KEY (c_phase_id) REFERENCES ampere.c_phase(c_phase_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project cphase_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project
    ADD CONSTRAINT cphase_cproject FOREIGN KEY (c_phase_id) REFERENCES ampere.c_phase(c_phase_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectphase cphase_cprojectphase; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectphase
    ADD CONSTRAINT cphase_cprojectphase FOREIGN KEY (c_phase_id) REFERENCES ampere.c_phase(c_phase_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_task cphase_ctask; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_task
    ADD CONSTRAINT cphase_ctask FOREIGN KEY (c_phase_id) REFERENCES ampere.c_phase(c_phase_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner cpopaymentterm_cbpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT cpopaymentterm_cbpartner FOREIGN KEY (po_paymentterm_id) REFERENCES ampere.c_paymentterm(c_paymentterm_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset cproject_aasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset
    ADD CONSTRAINT cproject_aasset FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_element cproject_caschemaelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_element
    ADD CONSTRAINT cproject_caschemaelement FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cash cproject_ccash; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cash
    ADD CONSTRAINT cproject_ccash FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplan cproject_ccashplan; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplan
    ADD CONSTRAINT cproject_ccashplan FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplanline cproject_ccashplanline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplanline
    ADD CONSTRAINT cproject_ccashplanline FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice cproject_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT cproject_cinvoice FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatchline cproject_cinvoicebatchline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT cproject_cinvoicebatchline FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline cproject_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT cproject_cinvoiceline FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order cproject_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT cproject_corder FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline cproject_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT cproject_corderline FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment cproject_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT cproject_cpayment FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectissue cproject_cprojectissue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectissue
    ADD CONSTRAINT cproject_cprojectissue FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectline cproject_cprojectline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectline
    ADD CONSTRAINT cproject_cprojectline FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectphase cproject_cprojectphase; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectphase
    ADD CONSTRAINT cproject_cprojectphase FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_recurring cproject_crecurring; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_recurring
    ADD CONSTRAINT cproject_crecurring FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_recurring_run cproject_crecurringrun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_recurring_run
    ADD CONSTRAINT cproject_crecurringrun FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary cproject_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT cproject_factacctsummary FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution cproject_gldist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT cproject_gldist FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distributionline cproject_gldistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT cproject_gldistline FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal cproject_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT cproject_ifajournal FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal cproject_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT cproject_igljournal FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice cproject_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT cproject_iinvoice FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order cproject_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT cproject_iorder FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout cproject_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT cproject_minout FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline cproject_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT cproject_minoutline FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventory cproject_minventory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventory
    ADD CONSTRAINT cproject_minventory FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movement cproject_mmovement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT cproject_mmovement FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_production cproject_mproduction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_production
    ADD CONSTRAINT cproject_mproduction FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcolumn cproject_pareportcolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT cproject_pareportcolumn FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportsource cproject_pareportsource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportsource
    ADD CONSTRAINT cproject_pareportsource FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_issueproject cproject_rissueproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issueproject
    ADD CONSTRAINT cproject_rissueproject FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request cproject_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT cproject_rrequest FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction cproject_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT cproject_rrequestaction FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpenseline cproject_stimeexpenseline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpenseline
    ADD CONSTRAINT cproject_stimeexpenseline FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_aging cproject_taging; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_aging
    ADD CONSTRAINT cproject_taging FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination cproject_vc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT cproject_vc FOREIGN KEY (c_project_id) REFERENCES ampere.c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectissuema cprojectissue_cprojectissuema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectissuema
    ADD CONSTRAINT cprojectissue_cprojectissuema FOREIGN KEY (c_projectissue_id) REFERENCES ampere.c_projectissue(c_projectissue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectline cprojectissue_cprojectline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectline
    ADD CONSTRAINT cprojectissue_cprojectline FOREIGN KEY (c_projectissue_id) REFERENCES ampere.c_projectissue(c_projectissue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transaction cprojectissue_mtransaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transaction
    ADD CONSTRAINT cprojectissue_mtransaction FOREIGN KEY (c_projectissue_id) REFERENCES ampere.c_projectissue(c_projectissue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_transaction cprojectissue_ttransaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_transaction
    ADD CONSTRAINT cprojectissue_ttransaction FOREIGN KEY (c_projectissue_id) REFERENCES ampere.c_projectissue(c_projectissue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplanline cprojectphase_ccashplanline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplanline
    ADD CONSTRAINT cprojectphase_ccashplanline FOREIGN KEY (c_projectphase_id) REFERENCES ampere.c_projectphase(c_projectphase_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline cprojectphase_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT cprojectphase_cinvoiceline FOREIGN KEY (c_projectphase_id) REFERENCES ampere.c_projectphase(c_projectphase_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline cprojectphase_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT cprojectphase_corderline FOREIGN KEY (c_projectphase_id) REFERENCES ampere.c_projectphase(c_projectphase_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectline cprojectphase_cprojectline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectline
    ADD CONSTRAINT cprojectphase_cprojectline FOREIGN KEY (c_projectphase_id) REFERENCES ampere.c_projectphase(c_projectphase_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projecttask cprojectphase_cprojecttask; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projecttask
    ADD CONSTRAINT cprojectphase_cprojecttask FOREIGN KEY (c_projectphase_id) REFERENCES ampere.c_projectphase(c_projectphase_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct cprojectphase_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT cprojectphase_factacct FOREIGN KEY (c_projectphase_id) REFERENCES ampere.c_projectphase(c_projectphase_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary cprojectphase_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT cprojectphase_factacctsummary FOREIGN KEY (c_projectphase_id) REFERENCES ampere.c_projectphase(c_projectphase_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpenseline cprojectphase_stimeexpenseline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpenseline
    ADD CONSTRAINT cprojectphase_stimeexpenseline FOREIGN KEY (c_projectphase_id) REFERENCES ampere.c_projectphase(c_projectphase_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplanline cprojecttask_ccashplanline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplanline
    ADD CONSTRAINT cprojecttask_ccashplanline FOREIGN KEY (c_projecttask_id) REFERENCES ampere.c_projecttask(c_projecttask_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline cprojecttask_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT cprojecttask_cinvoiceline FOREIGN KEY (c_projecttask_id) REFERENCES ampere.c_projecttask(c_projecttask_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline cprojecttask_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT cprojecttask_corderline FOREIGN KEY (c_projecttask_id) REFERENCES ampere.c_projecttask(c_projecttask_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectline cprojecttask_cprojectline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectline
    ADD CONSTRAINT cprojecttask_cprojectline FOREIGN KEY (c_projecttask_id) REFERENCES ampere.c_projecttask(c_projecttask_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct cprojecttask_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT cprojecttask_factacct FOREIGN KEY (c_projecttask_id) REFERENCES ampere.c_projecttask(c_projecttask_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary cprojecttask_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT cprojecttask_factacctsummary FOREIGN KEY (c_projecttask_id) REFERENCES ampere.c_projecttask(c_projecttask_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline cprojecttask_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT cprojecttask_minoutline FOREIGN KEY (c_projecttask_id) REFERENCES ampere.c_projecttask(c_projecttask_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpenseline cprojecttask_stimeexpenseline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpenseline
    ADD CONSTRAINT cprojecttask_stimeexpenseline FOREIGN KEY (c_projecttask_id) REFERENCES ampere.c_projecttask(c_projecttask_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_phase cprojecttype_cphase; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_phase
    ADD CONSTRAINT cprojecttype_cphase FOREIGN KEY (c_projecttype_id) REFERENCES ampere.c_projecttype(c_projecttype_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project cprojecttype_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project
    ADD CONSTRAINT cprojecttype_cproject FOREIGN KEY (c_projecttype_id) REFERENCES ampere.c_projecttype(c_projecttype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_measure cprojecttype_pameasure; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_measure
    ADD CONSTRAINT cprojecttype_pameasure FOREIGN KEY (c_projecttype_id) REFERENCES ampere.c_projecttype(c_projecttype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default creceivableservices_cacctschem; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT creceivableservices_cacctschem FOREIGN KEY (c_receivable_services_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_customer_acct creceivableservices_cbpcustome; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_customer_acct
    ADD CONSTRAINT creceivableservices_cbpcustome FOREIGN KEY (c_receivable_services_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group_acct creceivableservices_cbpgroupac; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT creceivableservices_cbpgroupac FOREIGN KEY (c_receivable_services_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_recurring_run crecurring_crecurringrun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_recurring_run
    ADD CONSTRAINT crecurring_crecurringrun FOREIGN KEY (c_recurring_id) REFERENCES ampere.c_recurring(c_recurring_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_city cregion_ccity; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_city
    ADD CONSTRAINT cregion_ccity FOREIGN KEY (c_region_id) REFERENCES ampere.c_region(c_region_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bpartner cregion_ibpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bpartner
    ADD CONSTRAINT cregion_ibpartner FOREIGN KEY (c_region_id) REFERENCES ampere.c_region(c_region_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice cregion_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT cregion_iinvoice FOREIGN KEY (c_region_id) REFERENCES ampere.c_region(c_region_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order cregion_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT cregion_iorder FOREIGN KEY (c_region_id) REFERENCES ampere.c_region(c_region_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_freight cregion_mfreight; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_freight
    ADD CONSTRAINT cregion_mfreight FOREIGN KEY (c_region_id) REFERENCES ampere.c_region(c_region_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_freight cregionto_mfreight; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_freight
    ADD CONSTRAINT cregionto_mfreight FOREIGN KEY (to_region_id) REFERENCES ampere.c_region(c_region_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_jobremuneration cremuneration_cjobrem; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_jobremuneration
    ADD CONSTRAINT cremuneration_cjobrem FOREIGN KEY (c_remuneration_id) REFERENCES ampere.c_remuneration(c_remuneration_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_userremuneration cremuneration_cuserrem; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_userremuneration
    ADD CONSTRAINT cremuneration_cuserrem FOREIGN KEY (c_remuneration_id) REFERENCES ampere.c_remuneration(c_remuneration_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_revenuerecognition_plan crevenuerecognition_plan; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_revenuerecognition_plan
    ADD CONSTRAINT crevenuerecognition_plan FOREIGN KEY (c_revenuerecognition_id) REFERENCES ampere.c_revenuerecognition(c_revenuerecognition_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product crevrecognition_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT crevrecognition_mproduct FOREIGN KEY (c_revenuerecognition_id) REFERENCES ampere.c_revenuerecognition(c_revenuerecognition_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_revenuerecognition_run crevrecplan_crefrecrun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_revenuerecognition_run
    ADD CONSTRAINT crevrecplan_crefrecrun FOREIGN KEY (c_revenuerecognition_plan_id) REFERENCES ampere.c_revenuerecognition_plan(c_revenuerecognition_plan_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_servicelevel crevrecplan_cservicelevel; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_servicelevel
    ADD CONSTRAINT crevrecplan_cservicelevel FOREIGN KEY (c_revenuerecognition_plan_id) REFERENCES ampere.c_revenuerecognition_plan(c_revenuerecognition_plan_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfqline crfq_crfqline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqline
    ADD CONSTRAINT crfq_crfqline FOREIGN KEY (c_rfq_id) REFERENCES ampere.c_rfq(c_rfq_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfqresponse crfq_crfqresponse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqresponse
    ADD CONSTRAINT crfq_crfqresponse FOREIGN KEY (c_rfq_id) REFERENCES ampere.c_rfq(c_rfq_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfqlineqty crfqline_crfqlineqty; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqlineqty
    ADD CONSTRAINT crfqline_crfqlineqty FOREIGN KEY (c_rfqline_id) REFERENCES ampere.c_rfqline(c_rfqline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfqresponseline crfqline_crfqresponseline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqresponseline
    ADD CONSTRAINT crfqline_crfqresponseline FOREIGN KEY (c_rfqline_id) REFERENCES ampere.c_rfqline(c_rfqline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfqresponselineqty crfqlineqty_crfqresplineqty; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqresponselineqty
    ADD CONSTRAINT crfqlineqty_crfqresplineqty FOREIGN KEY (c_rfqlineqty_id) REFERENCES ampere.c_rfqlineqty(c_rfqlineqty_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfqresponselineqty crfqresonseline_qty; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqresponselineqty
    ADD CONSTRAINT crfqresonseline_qty FOREIGN KEY (c_rfqresponseline_id) REFERENCES ampere.c_rfqresponseline(c_rfqresponseline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfqresponseline crfqresponse_line; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqresponseline
    ADD CONSTRAINT crfqresponse_line FOREIGN KEY (c_rfqresponse_id) REFERENCES ampere.c_rfqresponse(c_rfqresponse_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfq crfqtopic_crfq; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq
    ADD CONSTRAINT crfqtopic_crfq FOREIGN KEY (c_rfq_topic_id) REFERENCES ampere.c_rfq_topic(c_rfq_topic_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfq_topicsubscriberonly crfqtopicsubscriber_only; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq_topicsubscriberonly
    ADD CONSTRAINT crfqtopicsubscriber_only FOREIGN KEY (c_rfq_topicsubscriber_id) REFERENCES ampere.c_rfq_topicsubscriber(c_rfq_topicsubscriber_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner_location csalesregion_bpartnerlocation; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner_location
    ADD CONSTRAINT csalesregion_bpartnerlocation FOREIGN KEY (c_salesregion_id) REFERENCES ampere.c_salesregion(c_salesregion_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_element csalesregion_caschemaelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_element
    ADD CONSTRAINT csalesregion_caschemaelement FOREIGN KEY (c_salesregion_id) REFERENCES ampere.c_salesregion(c_salesregion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commissionline csalesregion_ccommissionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissionline
    ADD CONSTRAINT csalesregion_ccommissionline FOREIGN KEY (c_salesregion_id) REFERENCES ampere.c_salesregion(c_salesregion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary csalesregion_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT csalesregion_factacctsummary FOREIGN KEY (c_salesregion_id) REFERENCES ampere.c_salesregion(c_salesregion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distributionline csalesregion_gldistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT csalesregion_gldistline FOREIGN KEY (c_salesregion_id) REFERENCES ampere.c_salesregion(c_salesregion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal csalesregion_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT csalesregion_ifajournal FOREIGN KEY (c_salesregion_id) REFERENCES ampere.c_salesregion(c_salesregion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal csalesregion_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT csalesregion_igljournal FOREIGN KEY (c_salesregion_id) REFERENCES ampere.c_salesregion(c_salesregion_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcolumn csalesregion_pareportcolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT csalesregion_pareportcolumn FOREIGN KEY (c_salesregion_id) REFERENCES ampere.c_salesregion(c_salesregion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportsource csalesregion_pareportsource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportsource
    ADD CONSTRAINT csalesregion_pareportsource FOREIGN KEY (c_salesregion_id) REFERENCES ampere.c_salesregion(c_salesregion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination csalesregion_vc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT csalesregion_vc FOREIGN KEY (c_salesregion_id) REFERENCES ampere.c_salesregion(c_salesregion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution csalesregopn_gldist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT csalesregopn_gldist FOREIGN KEY (c_salesregion_id) REFERENCES ampere.c_salesregion(c_salesregion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_servicelevelline cservicelevel_line; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_servicelevelline
    ADD CONSTRAINT cservicelevel_line FOREIGN KEY (c_servicelevel_id) REFERENCES ampere.c_servicelevel(c_servicelevel_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination csubacct_cvalidcombination; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT csubacct_cvalidcombination FOREIGN KEY (c_subacct_id) REFERENCES ampere.c_subacct(c_subacct_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct csubacct_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT csubacct_factacct FOREIGN KEY (c_subacct_id) REFERENCES ampere.c_subacct(c_subacct_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary csubacct_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT csubacct_factacctsummary FOREIGN KEY (c_subacct_id) REFERENCES ampere.c_subacct(c_subacct_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_subscription_delivery csubcription_csubscrdelivery; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_subscription_delivery
    ADD CONSTRAINT csubcription_csubscrdelivery FOREIGN KEY (c_subscription_id) REFERENCES ampere.c_subscription(c_subscription_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product csubscriptiontype_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT csubscriptiontype_mproduct FOREIGN KEY (c_subscriptiontype_id) REFERENCES ampere.c_subscriptiontype(c_subscriptiontype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_subscription csubscrtype_csubscription; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_subscription
    ADD CONSTRAINT csubscrtype_csubscription FOREIGN KEY (c_subscriptiontype_id) REFERENCES ampere.c_subscriptiontype(c_subscriptiontype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projecttask ctask_cprojecttask; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projecttask
    ADD CONSTRAINT ctask_cprojecttask FOREIGN KEY (c_task_id) REFERENCES ampere.c_task(c_task_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicebatchline ctax_cinvoicebatchline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicebatchline
    ADD CONSTRAINT ctax_cinvoicebatchline FOREIGN KEY (c_tax_id) REFERENCES ampere.c_tax(c_tax_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline ctax_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT ctax_cinvoiceline FOREIGN KEY (c_tax_id) REFERENCES ampere.c_tax(c_tax_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoicetax ctax_cinvoicetax; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoicetax
    ADD CONSTRAINT ctax_cinvoicetax FOREIGN KEY (c_tax_id) REFERENCES ampere.c_tax(c_tax_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline ctax_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT ctax_corderline FOREIGN KEY (c_tax_id) REFERENCES ampere.c_tax(c_tax_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_ordertax ctax_cordertax; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_ordertax
    ADD CONSTRAINT ctax_cordertax FOREIGN KEY (c_tax_id) REFERENCES ampere.c_tax(c_tax_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax_acct ctax_ctaxacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax_acct
    ADD CONSTRAINT ctax_ctaxacct FOREIGN KEY (c_tax_id) REFERENCES ampere.c_tax(c_tax_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdeclarationline ctax_ctaxdeclline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdeclarationline
    ADD CONSTRAINT ctax_ctaxdeclline FOREIGN KEY (c_tax_id) REFERENCES ampere.c_tax(c_tax_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdefinition ctax_ctaxdefinition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdefinition
    ADD CONSTRAINT ctax_ctaxdefinition FOREIGN KEY (c_tax_id) REFERENCES ampere.c_tax(c_tax_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxpostal ctax_ctaxpostal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxpostal
    ADD CONSTRAINT ctax_ctaxpostal FOREIGN KEY (c_tax_id) REFERENCES ampere.c_tax(c_tax_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax_trl ctax_ctaxtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax_trl
    ADD CONSTRAINT ctax_ctaxtrl FOREIGN KEY (c_tax_id) REFERENCES ampere.c_tax(c_tax_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct ctax_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT ctax_factacct FOREIGN KEY (c_tax_id) REFERENCES ampere.c_tax(c_tax_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice ctax_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT ctax_iinvoice FOREIGN KEY (c_tax_id) REFERENCES ampere.c_tax(c_tax_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order ctax_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT ctax_iorder FOREIGN KEY (c_tax_id) REFERENCES ampere.c_tax(c_tax_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax ctax_parent; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax
    ADD CONSTRAINT ctax_parent FOREIGN KEY (parent_tax_id) REFERENCES ampere.c_tax(c_tax_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdefinition ctaxbase_ctaxdefinition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdefinition
    ADD CONSTRAINT ctaxbase_ctaxdefinition FOREIGN KEY (c_taxbase_id) REFERENCES ampere.c_taxbase(c_taxbase_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_charge ctaxcategory_ccharge; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_charge
    ADD CONSTRAINT ctaxcategory_ccharge FOREIGN KEY (c_taxcategory_id) REFERENCES ampere.c_taxcategory(c_taxcategory_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax ctaxcategory_ctax; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax
    ADD CONSTRAINT ctaxcategory_ctax FOREIGN KEY (c_taxcategory_id) REFERENCES ampere.c_taxcategory(c_taxcategory_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdefinition ctaxcategory_ctaxdefinition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdefinition
    ADD CONSTRAINT ctaxcategory_ctaxdefinition FOREIGN KEY (c_taxcategory_id) REFERENCES ampere.c_taxcategory(c_taxcategory_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product ctaxcategory_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT ctaxcategory_mproduct FOREIGN KEY (c_taxcategory_id) REFERENCES ampere.c_taxcategory(c_taxcategory_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_expensetype ctaxcategory_sexpensetype; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_expensetype
    ADD CONSTRAINT ctaxcategory_sexpensetype FOREIGN KEY (c_taxcategory_id) REFERENCES ampere.c_taxcategory(c_taxcategory_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_resourcetype ctaxcategory_sresourcetype; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_resourcetype
    ADD CONSTRAINT ctaxcategory_sresourcetype FOREIGN KEY (c_taxcategory_id) REFERENCES ampere.c_taxcategory(c_taxcategory_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_training ctaxcategory_straining; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_training
    ADD CONSTRAINT ctaxcategory_straining FOREIGN KEY (c_taxcategory_id) REFERENCES ampere.c_taxcategory(c_taxcategory_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxcategory_trl ctaxcategory_trl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxcategory_trl
    ADD CONSTRAINT ctaxcategory_trl FOREIGN KEY (c_taxcategory_id) REFERENCES ampere.c_taxcategory(c_taxcategory_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdeclarationacct ctaxdecl_ctaxdeclacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdeclarationacct
    ADD CONSTRAINT ctaxdecl_ctaxdeclacct FOREIGN KEY (c_taxdeclaration_id) REFERENCES ampere.c_taxdeclaration(c_taxdeclaration_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdeclarationline ctaxdeclaration_ctaxdeclline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdeclarationline
    ADD CONSTRAINT ctaxdeclaration_ctaxdeclline FOREIGN KEY (c_taxdeclaration_id) REFERENCES ampere.c_taxdeclaration(c_taxdeclaration_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner ctaxgroup_cbpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT ctaxgroup_cbpartner FOREIGN KEY (c_taxgroup_id) REFERENCES ampere.c_taxgroup(c_taxgroup_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdefinition ctaxgroup_ctaxdefinition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdefinition
    ADD CONSTRAINT ctaxgroup_ctaxdefinition FOREIGN KEY (c_taxgroup_id) REFERENCES ampere.c_taxgroup(c_taxgroup_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdefinition ctaxtype_ctaxdefinition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdefinition
    ADD CONSTRAINT ctaxtype_ctaxdefinition FOREIGN KEY (c_taxtype_id) REFERENCES ampere.c_taxtype(c_taxtype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline cuom_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT cuom_cinvoiceline FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline cuom_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT cuom_corderline FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfqlineqty cuom_crfqlineqty; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqlineqty
    ADD CONSTRAINT cuom_crfqlineqty FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_uom_conversion cuom_cuomconversion; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_uom_conversion
    ADD CONSTRAINT cuom_cuomconversion FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_uom_trl cuom_cuomtrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_uom_trl
    ADD CONSTRAINT cuom_cuomtrl FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journalline cuom_gljournalline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalline
    ADD CONSTRAINT cuom_gljournalline FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal cuom_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT cuom_ifajournal FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal cuom_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT cuom_igljournal FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order cuom_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT cuom_iorder FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_pricelist cuom_ipricelist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_pricelist
    ADD CONSTRAINT cuom_ipricelist FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_product cuom_iproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_product
    ADD CONSTRAINT cuom_iproduct FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline cuom_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT cuom_minoutline FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product cuom_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT cuom_mproduct FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_requisitionline cuom_mrequisitionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_requisitionline
    ADD CONSTRAINT cuom_mrequisitionline FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_expensetype cuom_sexpensetype; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_expensetype
    ADD CONSTRAINT cuom_sexpensetype FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_resourcetype cuom_sresourcetype; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_resourcetype
    ADD CONSTRAINT cuom_sresourcetype FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpenseline cuom_stimeexpenseline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpenseline
    ADD CONSTRAINT cuom_stimeexpenseline FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_training cuom_straining; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_training
    ADD CONSTRAINT cuom_straining FOREIGN KEY (c_uom_id) REFERENCES ampere.c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal cvalidcombination_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT cvalidcombination_ifajournal FOREIGN KEY (c_validcombination_id) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal cvalidcombination_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT cvalidcombination_igljournal FOREIGN KEY (c_validcombination_id) REFERENCES ampere.c_validcombination(c_validcombination_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journalline cvc_gljournalline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalline
    ADD CONSTRAINT cvc_gljournalline FOREIGN KEY (c_validcombination_id) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_withholding cwithholding_cbpwithholding; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_withholding
    ADD CONSTRAINT cwithholding_cbpwithholding FOREIGN KEY (c_withholding_id) REFERENCES ampere.c_withholding(c_withholding_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_withholding_acct cwithholding_cwithholdingacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_withholding_acct
    ADD CONSTRAINT cwithholding_cwithholdingacct FOREIGN KEY (c_withholding_id) REFERENCES ampere.c_withholding(c_withholding_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_demand cyear_mdemand; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_demand
    ADD CONSTRAINT cyear_mdemand FOREIGN KEY (c_year_id) REFERENCES ampere.c_year(c_year_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_forecast cyear_mforecast; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_forecast
    ADD CONSTRAINT cyear_mforecast FOREIGN KEY (c_year_id) REFERENCES ampere.c_year(c_year_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_doctype definitesequence_cdoctype; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_doctype
    ADD CONSTRAINT definitesequence_cdoctype FOREIGN KEY (definitesequence_id) REFERENCES ampere.ad_sequence(ad_sequence_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printform distribordermailtext_adprintfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT distribordermailtext_adprintfo FOREIGN KEY (distrib_order_mailtext_id) REFERENCES ampere.r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printform distriborderprintformat_adprin; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT distriborderprintformat_adprin FOREIGN KEY (distrib_order_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order dropshipbpartner_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT dropshipbpartner_corder FOREIGN KEY (dropship_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout dropshipbpartner_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT dropshipbpartner_minout FOREIGN KEY (dropship_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order dropshiplocation_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT dropshiplocation_corder FOREIGN KEY (dropship_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout dropshiplocation_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT dropshiplocation_minout FOREIGN KEY (dropship_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order dropshipuser_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT dropshipuser_corder FOREIGN KEY (dropship_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout dropshipuser_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT dropshipuser_minout FOREIGN KEY (dropship_user_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_orginfo dropshipwarehouse_adorginfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_orginfo
    ADD CONSTRAINT dropshipwarehouse_adorginfo FOREIGN KEY (dropship_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_column entityt_adcolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_column
    ADD CONSTRAINT entityt_adcolumn FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_element entityt_adelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_element
    ADD CONSTRAINT entityt_adelement FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_field entityt_adfield; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_field
    ADD CONSTRAINT entityt_adfield FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_fieldgroup entityt_adfieldgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_fieldgroup
    ADD CONSTRAINT entityt_adfieldgroup FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_form entityt_adform; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_form
    ADD CONSTRAINT entityt_adform FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_image entityt_adimage; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_image
    ADD CONSTRAINT entityt_adimage FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_infocolumn entityt_adinfocolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_infocolumn
    ADD CONSTRAINT entityt_adinfocolumn FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_infowindow entityt_adinfowindow; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_infowindow
    ADD CONSTRAINT entityt_adinfowindow FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_menu entityt_admenu; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_menu
    ADD CONSTRAINT entityt_admenu FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_message entityt_admessage; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_message
    ADD CONSTRAINT entityt_admessage FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_modelvalidator entityt_admodelvalidator; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_modelvalidator
    ADD CONSTRAINT entityt_admodelvalidator FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process entityt_adprocess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process
    ADD CONSTRAINT entityt_adprocess FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_process_para entityt_adprocesspara; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_process_para
    ADD CONSTRAINT entityt_adprocesspara FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_reference entityt_adreference; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_reference
    ADD CONSTRAINT entityt_adreference FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_ref_list entityt_adreflist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_list
    ADD CONSTRAINT entityt_adreflist FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_ref_table entityt_adreftable; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_table
    ADD CONSTRAINT entityt_adreftable FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_reportview entityt_adreportview; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_reportview
    ADD CONSTRAINT entityt_adreportview FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_rule entityt_adrule; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_rule
    ADD CONSTRAINT entityt_adrule FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_sysconfig entityt_adsysconfig; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_sysconfig
    ADD CONSTRAINT entityt_adsysconfig FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_tab entityt_adtab; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab
    ADD CONSTRAINT entityt_adtab FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_table entityt_adtable; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table
    ADD CONSTRAINT entityt_adtable FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_val_rule entityt_advalrule; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_val_rule
    ADD CONSTRAINT entityt_advalrule FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_window entityt_adwindow; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_window
    ADD CONSTRAINT entityt_adwindow FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_workflow entityt_adworkflow; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_workflow
    ADD CONSTRAINT entityt_adworkflow FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_colorschema entityt_pacolorschema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_colorschema
    ADD CONSTRAINT entityt_pacolorschema FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_measurecalc entityt_pameasurecalc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_measurecalc
    ADD CONSTRAINT entityt_pameasurecalc FOREIGN KEY (entitytype) REFERENCES ampere.ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdeclarationacct factacct_ctaxdeclacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdeclarationacct
    ADD CONSTRAINT factacct_ctaxdeclacct FOREIGN KEY (fact_acct_id) REFERENCES ampere.fact_acct(fact_acct_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_invoicegl factacct_tinvoicegl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_invoicegl
    ADD CONSTRAINT factacct_tinvoicegl FOREIGN KEY (fact_acct_id) REFERENCES ampere.fact_acct(fact_acct_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_field fieldclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_field
    ADD CONSTRAINT fieldclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_field fieldorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_field
    ADD CONSTRAINT fieldorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct glbudget_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT glbudget_factacct FOREIGN KEY (gl_budget_id) REFERENCES ampere.gl_budget(gl_budget_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary glbudget_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT glbudget_factacctsummary FOREIGN KEY (gl_budget_id) REFERENCES ampere.gl_budget(gl_budget_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_budgetcontrol glbudget_glbudgetcontrol; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_budgetcontrol
    ADD CONSTRAINT glbudget_glbudgetcontrol FOREIGN KEY (gl_budget_id) REFERENCES ampere.gl_budget(gl_budget_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journal glbudget_gljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journal
    ADD CONSTRAINT glbudget_gljournal FOREIGN KEY (gl_budget_id) REFERENCES ampere.gl_budget(gl_budget_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal glbudget_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT glbudget_ifajournal FOREIGN KEY (gl_budget_id) REFERENCES ampere.gl_budget(gl_budget_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal glbudget_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT glbudget_igljournal FOREIGN KEY (gl_budget_id) REFERENCES ampere.gl_budget(gl_budget_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcolumn glbudget_pareportcolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT glbudget_pareportcolumn FOREIGN KEY (gl_budget_id) REFERENCES ampere.gl_budget(gl_budget_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportline glbudget_pareportline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportline
    ADD CONSTRAINT glbudget_pareportline FOREIGN KEY (gl_budget_id) REFERENCES ampere.gl_budget(gl_budget_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_reval_entry glcategory_aassetrevalentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_reval_entry
    ADD CONSTRAINT glcategory_aassetrevalentry FOREIGN KEY (gl_category_id) REFERENCES ampere.gl_category(gl_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_depreciation_entry glcategory_adepreciationentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_depreciation_entry
    ADD CONSTRAINT glcategory_adepreciationentry FOREIGN KEY (gl_category_id) REFERENCES ampere.gl_category(gl_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_doctype glcategory_cdoctype; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_doctype
    ADD CONSTRAINT glcategory_cdoctype FOREIGN KEY (gl_category_id) REFERENCES ampere.gl_category(gl_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct glcategory_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT glcategory_factacct FOREIGN KEY (gl_category_id) REFERENCES ampere.gl_category(gl_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journal glcategory_gljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journal
    ADD CONSTRAINT glcategory_gljournal FOREIGN KEY (gl_category_id) REFERENCES ampere.gl_category(gl_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journalbatch glcategory_gljournalbatch; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalbatch
    ADD CONSTRAINT glcategory_gljournalbatch FOREIGN KEY (gl_category_id) REFERENCES ampere.gl_category(gl_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal glcategory_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT glcategory_ifajournal FOREIGN KEY (gl_category_id) REFERENCES ampere.gl_category(gl_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal glcategory_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT glcategory_igljournal FOREIGN KEY (gl_category_id) REFERENCES ampere.gl_category(gl_category_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distributionline gldistribution_gldistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT gldistribution_gldistline FOREIGN KEY (gl_distribution_id) REFERENCES ampere.gl_distribution(gl_distribution_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_fundrestriction glfund_glfundrestriction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_fundrestriction
    ADD CONSTRAINT glfund_glfundrestriction FOREIGN KEY (gl_fund_id) REFERENCES ampere.gl_fund(gl_fund_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal gljourbelline_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT gljourbelline_igljournal FOREIGN KEY (gl_journalline_id) REFERENCES ampere.gl_journalline(gl_journalline_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_revenuerecognition_run gljournal_crevenuerecrun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_revenuerecognition_run
    ADD CONSTRAINT gljournal_crevenuerecrun FOREIGN KEY (gl_journal_id) REFERENCES ampere.gl_journal(gl_journal_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journalline gljournal_gljournalline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalline
    ADD CONSTRAINT gljournal_gljournalline FOREIGN KEY (gl_journal_id) REFERENCES ampere.gl_journal(gl_journal_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal gljournal_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT gljournal_ifajournal FOREIGN KEY (gl_journal_id) REFERENCES ampere.gl_journal(gl_journal_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal gljournal_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT gljournal_igljournal FOREIGN KEY (gl_journal_id) REFERENCES ampere.gl_journal(gl_journal_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_addition gljournalbatch_aassetaddition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_addition
    ADD CONSTRAINT gljournalbatch_aassetaddition FOREIGN KEY (gl_journalbatch_id) REFERENCES ampere.gl_journalbatch(gl_journalbatch_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_recurring gljournalbatch_crecurring; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_recurring
    ADD CONSTRAINT gljournalbatch_crecurring FOREIGN KEY (gl_journalbatch_id) REFERENCES ampere.gl_journalbatch(gl_journalbatch_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_recurring_run gljournalbatch_crecurringrun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_recurring_run
    ADD CONSTRAINT gljournalbatch_crecurringrun FOREIGN KEY (gl_journalbatch_id) REFERENCES ampere.gl_journalbatch(gl_journalbatch_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journal gljournalbatch_gljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journal
    ADD CONSTRAINT gljournalbatch_gljournal FOREIGN KEY (gl_journalbatch_id) REFERENCES ampere.gl_journalbatch(gl_journalbatch_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal gljournalbatch_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT gljournalbatch_ifajournal FOREIGN KEY (gl_journalbatch_id) REFERENCES ampere.gl_journalbatch(gl_journalbatch_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal gljournalbatch_igljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT gljournalbatch_igljournal FOREIGN KEY (gl_journalbatch_id) REFERENCES ampere.gl_journalbatch(gl_journalbatch_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal gljournalline_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT gljournalline_ifajournal FOREIGN KEY (gl_journalline_id) REFERENCES ampere.gl_journalline(gl_journalline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_field includedtab_adfield; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_field
    ADD CONSTRAINT includedtab_adfield FOREIGN KEY (included_tab_id) REFERENCES ampere.ad_tab(ad_tab_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printformat jasperprocess_adprintformat; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printformat
    ADD CONSTRAINT jasperprocess_adprintformat FOREIGN KEY (jasperprocess_id) REFERENCES ampere.ad_process(ad_process_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_report jasperprocess_pareport; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_report
    ADD CONSTRAINT jasperprocess_pareport FOREIGN KEY (jasperprocess_id) REFERENCES ampere.ad_process(ad_process_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_language languageclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_language
    ADD CONSTRAINT languageclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_language languageorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_language
    ADD CONSTRAINT languageorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset leasebpartner_aasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset
    ADD CONSTRAINT leasebpartner_aasset FOREIGN KEY (lease_bpartner_id) REFERENCES ampere.c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order linkorder_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT linkorder_corder FOREIGN KEY (link_order_id) REFERENCES ampere.c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline linkorderline_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT linkorderline_corderline FOREIGN KEY (link_orderline_id) REFERENCES ampere.c_orderline(c_orderline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo logo_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT logo_adclientinfo FOREIGN KEY (logo_id) REFERENCES ampere.ad_image(ad_image_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_orginfo logo_adorginfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_orginfo
    ADD CONSTRAINT logo_adorginfo FOREIGN KEY (logo_id) REFERENCES ampere.ad_image(ad_image_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner logo_cbpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT logo_cbpartner FOREIGN KEY (logo_id) REFERENCES ampere.ad_image(ad_image_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo logoreport_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT logoreport_adclientinfo FOREIGN KEY (logoreport_id) REFERENCES ampere.ad_image(ad_image_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo logoweb_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT logoweb_adclientinfo FOREIGN KEY (logoweb_id) REFERENCES ampere.ad_image(ad_image_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_storage m_item_storage_client; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_storage
    ADD CONSTRAINT m_item_storage_client FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_storage m_item_storage_org; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_storage
    ADD CONSTRAINT m_item_storage_org FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_storage m_locator_storage; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_storage
    ADD CONSTRAINT m_locator_storage FOREIGN KEY (m_locator_id) REFERENCES ampere.m_locator(m_locator_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct m_product_fact_acct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT m_product_fact_acct FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct m_product_m_product_acct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT m_product_m_product_acct FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_po m_product_productpo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_po
    ADD CONSTRAINT m_product_productpo FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_replenish m_product_replenish; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_replenish
    ADD CONSTRAINT m_product_replenish FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_po m_productpo_client; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_po
    ADD CONSTRAINT m_productpo_client FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_po m_productpo_org; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_po
    ADD CONSTRAINT m_productpo_org FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_substitute m_substitute_client; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_substitute
    ADD CONSTRAINT m_substitute_client FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_substitute m_substitute_org; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_substitute
    ADD CONSTRAINT m_substitute_org FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_warehouse m_warehouse_client; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_warehouse
    ADD CONSTRAINT m_warehouse_client FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_locator m_warehouse_locator; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_locator
    ADD CONSTRAINT m_warehouse_locator FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_warehouse m_warehouse_org; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_warehouse
    ADD CONSTRAINT m_warehouse_org FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_replenish m_warehouse_replenish; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_replenish
    ADD CONSTRAINT m_warehouse_replenish FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_warehouse_acct m_warehouse_warehouse_acct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_warehouse_acct
    ADD CONSTRAINT m_warehouse_warehouse_acct FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_locator m_wh_locator_client; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_locator
    ADD CONSTRAINT m_wh_locator_client FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_locator m_wh_locator_org; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_locator
    ADD CONSTRAINT m_wh_locator_org FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printform manufordermailtext_adprintform; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT manufordermailtext_adprintform FOREIGN KEY (manuf_order_mailtext_id) REFERENCES ampere.r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printform manuforderprintformat_adprintf; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT manuforderprintformat_adprintf FOREIGN KEY (manuf_order_printformat_id) REFERENCES ampere.ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfqline masetinstance_crfqline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqline
    ADD CONSTRAINT masetinstance_crfqline FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_landedcostallocation masi_clandedcostallocation; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_landedcostallocation
    ADD CONSTRAINT masi_clandedcostallocation FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectissuema masi_cprojectissuema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectissuema
    ADD CONSTRAINT masi_cprojectissuema FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_bomproduct masi_mbomproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_bomproduct
    ADD CONSTRAINT masi_mbomproduct FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_cost masi_mcost; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_cost
    ADD CONSTRAINT masi_mcost FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutlinema masi_minourlinema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutlinema
    ADD CONSTRAINT masi_minourlinema FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventorylinema masi_minventorylinema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventorylinema
    ADD CONSTRAINT masi_minventorylinema FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movementlinema masi_mmovementlinema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementlinema
    ADD CONSTRAINT masi_mmovementlinema FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productionlinema masi_mproductionlinema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productionlinema
    ADD CONSTRAINT masi_mproductionlinema FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_inventoryvalue masi_tinventoryvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_inventoryvalue
    ADD CONSTRAINT masi_tinventoryvalue FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_transaction masi_ttransaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_transaction
    ADD CONSTRAINT masi_ttransaction FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_attributeinstance mattribute_mattributeinst; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributeinstance
    ADD CONSTRAINT mattribute_mattributeinst FOREIGN KEY (m_attribute_id) REFERENCES ampere.m_attribute(m_attribute_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_attributeuse mattribute_mattributeuse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributeuse
    ADD CONSTRAINT mattribute_mattributeuse FOREIGN KEY (m_attribute_id) REFERENCES ampere.m_attribute(m_attribute_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_attributevalue mattribute_mattributevalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributevalue
    ADD CONSTRAINT mattribute_mattributevalue FOREIGN KEY (m_attribute_id) REFERENCES ampere.m_attribute(m_attribute_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_attribute mattributesearch_mattribute; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attribute
    ADD CONSTRAINT mattributesearch_mattribute FOREIGN KEY (m_attributesearch_id) REFERENCES ampere.m_attributesearch(m_attributesearch_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_attributesetexclude mattributeset_masexclude; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributesetexclude
    ADD CONSTRAINT mattributeset_masexclude FOREIGN KEY (m_attributeset_id) REFERENCES ampere.m_attributeset(m_attributeset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_attributesetinstance mattributeset_mattribsetinst; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributesetinstance
    ADD CONSTRAINT mattributeset_mattribsetinst FOREIGN KEY (m_attributeset_id) REFERENCES ampere.m_attributeset(m_attributeset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_attributeuse mattributeset_mattributeuse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributeuse
    ADD CONSTRAINT mattributeset_mattributeuse FOREIGN KEY (m_attributeset_id) REFERENCES ampere.m_attributeset(m_attributeset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product mattributeset_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT mattributeset_mproduct FOREIGN KEY (m_attributeset_id) REFERENCES ampere.m_attributeset(m_attributeset_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transactionallocation mattributesetinst_mtrxalloc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transactionallocation
    ADD CONSTRAINT mattributesetinst_mtrxalloc FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset mattributesetinstance_aasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset
    ADD CONSTRAINT mattributesetinstance_aasset FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_matchinv mattributesetinstance_mmatchin; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_matchinv
    ADD CONSTRAINT mattributesetinstance_mmatchin FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_matchpo mattributesetinstance_mmatchpo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_matchpo
    ADD CONSTRAINT mattributesetinstance_mmatchpo FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_requisitionline mattributesetinstance_mrequisi; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_requisitionline
    ADD CONSTRAINT mattributesetinstance_mrequisi FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movementline mattributesetinstanceto_mmovem; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementline
    ADD CONSTRAINT mattributesetinstanceto_mmovem FOREIGN KEY (m_attributesetinstanceto_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_attributeinstance mattributevalue_mattrinst; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributeinstance
    ADD CONSTRAINT mattributevalue_mattrinst FOREIGN KEY (m_attributevalue_id) REFERENCES ampere.m_attributevalue(m_attributevalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_attributeinstance mattrsetinst__mattrinst; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributeinstance
    ADD CONSTRAINT mattrsetinst__mattrinst FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline mattrsetinst_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT mattrsetinst_cinvoiceline FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline mattrsetinst_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT mattrsetinst_corderline FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectissue mattrsetinst_cprojectissue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectissue
    ADD CONSTRAINT mattrsetinst_cprojectissue FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline mattrsetinst_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT mattrsetinst_minoutline FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventoryline mattrsetinst_minventoryline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventoryline
    ADD CONSTRAINT mattrsetinst_minventoryline FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movementline mattrsetinst_mmovementline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementline
    ADD CONSTRAINT mattrsetinst_mmovementline FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product mattrsetinst_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT mattrsetinst_mproduct FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productionline mattrsetinst_mproductionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productionline
    ADD CONSTRAINT mattrsetinst_mproductionline FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_storage mattrsetinst_mstorage; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_storage
    ADD CONSTRAINT mattrsetinst_mstorage FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transaction mattrsetinst_mtransaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transaction
    ADD CONSTRAINT mattrsetinst_mtransaction FOREIGN KEY (m_attributesetinstance_id) REFERENCES ampere.m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_bomproduct mbom_mbomproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_bomproduct
    ADD CONSTRAINT mbom_mbomproduct FOREIGN KEY (m_bom_id) REFERENCES ampere.m_bom(m_bom_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_group mbom_rgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_group
    ADD CONSTRAINT mbom_rgroup FOREIGN KEY (m_bom_id) REFERENCES ampere.m_bom(m_bom_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_bomproduct mbomalternative_mbomproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_bomproduct
    ADD CONSTRAINT mbomalternative_mbomproduct FOREIGN KEY (m_bomalternative_id) REFERENCES ampere.m_bomalternative(m_bomalternative_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_group mchangenotice_rgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_group
    ADD CONSTRAINT mchangenotice_rgroup FOREIGN KEY (m_changenotice_id) REFERENCES ampere.m_changenotice(m_changenotice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request mchangerequest_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT mchangerequest_rrequest FOREIGN KEY (m_changerequest_id) REFERENCES ampere.m_changerequest(m_changerequest_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_bom mcn_mbom; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_bom
    ADD CONSTRAINT mcn_mbom FOREIGN KEY (m_changenotice_id) REFERENCES ampere.m_changenotice(m_changenotice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_bomproduct mcn_mbomproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_bomproduct
    ADD CONSTRAINT mcn_mbomproduct FOREIGN KEY (m_changenotice_id) REFERENCES ampere.m_changenotice(m_changenotice_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_changerequest mcn_mcr; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_changerequest
    ADD CONSTRAINT mcn_mcr FOREIGN KEY (m_changenotice_id) REFERENCES ampere.m_changenotice(m_changenotice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_cost mcostelement_mcost; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_cost
    ADD CONSTRAINT mcostelement_mcost FOREIGN KEY (m_costelement_id) REFERENCES ampere.m_costelement(m_costelement_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_landedcostallocation mcostelement_mlandedcostalloc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_landedcostallocation
    ADD CONSTRAINT mcostelement_mlandedcostalloc FOREIGN KEY (m_costelement_id) REFERENCES ampere.m_costelement(m_costelement_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_inventoryvalue mcostelement_tinventoryvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_inventoryvalue
    ADD CONSTRAINT mcostelement_tinventoryvalue FOREIGN KEY (m_costelement_id) REFERENCES ampere.m_costelement(m_costelement_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema mcosttype_cacctschema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema
    ADD CONSTRAINT mcosttype_cacctschema FOREIGN KEY (m_costtype_id) REFERENCES ampere.m_costtype(m_costtype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_cost mcosttype_mcost; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_cost
    ADD CONSTRAINT mcosttype_mcost FOREIGN KEY (m_costtype_id) REFERENCES ampere.m_costtype(m_costtype_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_demandline mdemand_mdemandline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_demandline
    ADD CONSTRAINT mdemand_mdemandline FOREIGN KEY (m_demand_id) REFERENCES ampere.m_demand(m_demand_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_demanddetail mdemandline_mdemanddetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_demanddetail
    ADD CONSTRAINT mdemandline_mdemanddetail FOREIGN KEY (m_demandline_id) REFERENCES ampere.m_demandline(m_demandline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner mdiscounts_cbpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT mdiscounts_cbpartner FOREIGN KEY (m_discountschema_id) REFERENCES ampere.m_discountschema(m_discountschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_discountschemabreak mdiscounts_mdsbreak; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_discountschemabreak
    ADD CONSTRAINT mdiscounts_mdsbreak FOREIGN KEY (m_discountschema_id) REFERENCES ampere.m_discountschema(m_discountschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_pricelist_version mdiscounts_mplversion; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_pricelist_version
    ADD CONSTRAINT mdiscounts_mplversion FOREIGN KEY (m_discountschema_id) REFERENCES ampere.m_discountschema(m_discountschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group mdiscountschema_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group
    ADD CONSTRAINT mdiscountschema_cbpgroup FOREIGN KEY (m_discountschema_id) REFERENCES ampere.m_discountschema(m_discountschema_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_discountschemaline mdiscountschema_mdsline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_discountschemaline
    ADD CONSTRAINT mdiscountschema_mdsline FOREIGN KEY (m_discountschema_id) REFERENCES ampere.m_discountschema(m_discountschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group mdiscountschemapo_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group
    ADD CONSTRAINT mdiscountschemapo_cbpgroup FOREIGN KEY (po_discountschema_id) REFERENCES ampere.m_discountschema(m_discountschema_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner mdiscountspo_cbpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT mdiscountspo_cbpartner FOREIGN KEY (po_discountschema_id) REFERENCES ampere.m_discountschema(m_discountschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_distributionlistline mdistributionlist_line; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_distributionlistline
    ADD CONSTRAINT mdistributionlist_line FOREIGN KEY (m_distributionlist_id) REFERENCES ampere.m_distributionlist(m_distributionlist_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_distributionrunline mdistributionlist_runline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_distributionrunline
    ADD CONSTRAINT mdistributionlist_runline FOREIGN KEY (m_distributionlist_id) REFERENCES ampere.m_distributionlist(m_distributionlist_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_distributionrundetail mdistributionlist_tdrdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_distributionrundetail
    ADD CONSTRAINT mdistributionlist_tdrdetail FOREIGN KEY (m_distributionlist_id) REFERENCES ampere.m_distributionlist(m_distributionlist_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_distributionrundetail mdistributionlline_tdrdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_distributionrundetail
    ADD CONSTRAINT mdistributionlline_tdrdetail FOREIGN KEY (m_distributionlistline_id) REFERENCES ampere.m_distributionlistline(m_distributionlistline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_distributionrundetail mdistributionrline_tdrdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_distributionrundetail
    ADD CONSTRAINT mdistributionrline_tdrdetail FOREIGN KEY (m_distributionrunline_id) REFERENCES ampere.m_distributionrunline(m_distributionrunline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_distributionrunline mdistributionrun_line; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_distributionrunline
    ADD CONSTRAINT mdistributionrun_line FOREIGN KEY (m_distributionrun_id) REFERENCES ampere.m_distributionrun(m_distributionrun_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_distributionrundetail mdistributionrun_tdrdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_distributionrundetail
    ADD CONSTRAINT mdistributionrun_tdrdetail FOREIGN KEY (m_distributionrun_id) REFERENCES ampere.m_distributionrun(m_distributionrun_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_message messageclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_message
    ADD CONSTRAINT messageclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_message messageorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_message
    ADD CONSTRAINT messageorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_changerequest mfixchangenotice_mchangereques; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_changerequest
    ADD CONSTRAINT mfixchangenotice_mchangereques FOREIGN KEY (m_fixchangenotice_id) REFERENCES ampere.m_changenotice(m_changenotice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request mfixchangenotice_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT mfixchangenotice_rrequest FOREIGN KEY (m_fixchangenotice_id) REFERENCES ampere.m_changenotice(m_changenotice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_forecastline mforecast_mforecastline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_forecastline
    ADD CONSTRAINT mforecast_mforecastline FOREIGN KEY (m_forecast_id) REFERENCES ampere.m_forecast(m_forecast_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_demanddetail mforecastline_mdemanddetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_demanddetail
    ADD CONSTRAINT mforecastline_mdemanddetail FOREIGN KEY (m_forecastline_id) REFERENCES ampere.m_forecastline(m_forecastline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_freight mfreightcategory_mfreight; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_freight
    ADD CONSTRAINT mfreightcategory_mfreight FOREIGN KEY (m_freightcategory_id) REFERENCES ampere.m_freightcategory(m_freightcategory_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product mfreightcategory_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT mfreightcategory_mproduct FOREIGN KEY (m_freightcategory_id) REFERENCES ampere.m_freightcategory(m_freightcategory_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order mfreightcategory_order; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT mfreightcategory_order FOREIGN KEY (m_freightcategory_id) REFERENCES ampere.m_freightcategory(m_freightcategory_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_landedcost minout_clandedcost; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_landedcost
    ADD CONSTRAINT minout_clandedcost FOREIGN KEY (m_inout_id) REFERENCES ampere.m_inout(m_inout_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutconfirm minout_minoutconfirm; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutconfirm
    ADD CONSTRAINT minout_minoutconfirm FOREIGN KEY (m_inout_id) REFERENCES ampere.m_inout(m_inout_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline minout_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT minout_minoutline FOREIGN KEY (m_inout_id) REFERENCES ampere.m_inout(m_inout_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_package minout_mpackage; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_package
    ADD CONSTRAINT minout_mpackage FOREIGN KEY (m_inout_id) REFERENCES ampere.m_inout(m_inout_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction minout_mrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT minout_mrequestaction FOREIGN KEY (m_inout_id) REFERENCES ampere.m_inout(m_inout_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_rma minout_mrma; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_rma
    ADD CONSTRAINT minout_mrma FOREIGN KEY (inout_id) REFERENCES ampere.m_inout(m_inout_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout minout_ref; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT minout_ref FOREIGN KEY (ref_inout_id) REFERENCES ampere.m_inout(m_inout_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request minout_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT minout_rrequest FOREIGN KEY (m_inout_id) REFERENCES ampere.m_inout(m_inout_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutlineconfirm minoutconfirm_minoutlineconf; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutlineconfirm
    ADD CONSTRAINT minoutconfirm_minoutlineconf FOREIGN KEY (m_inoutconfirm_id) REFERENCES ampere.m_inoutconfirm(m_inoutconfirm_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset minoutline_aasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset
    ADD CONSTRAINT minoutline_aasset FOREIGN KEY (m_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline minoutline_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT minoutline_cinvoiceline FOREIGN KEY (m_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_landedcost minoutline_clandedcost; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_landedcost
    ADD CONSTRAINT minoutline_clandedcost FOREIGN KEY (m_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectissue minoutline_cprojectissue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectissue
    ADD CONSTRAINT minoutline_cprojectissue FOREIGN KEY (m_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutlineconfirm minoutline_minoutconfirm; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutlineconfirm
    ADD CONSTRAINT minoutline_minoutconfirm FOREIGN KEY (m_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutlinema minoutline_minoutlinema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutlinema
    ADD CONSTRAINT minoutline_minoutlinema FOREIGN KEY (m_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_matchinv minoutline_mmatchinv; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_matchinv
    ADD CONSTRAINT minoutline_mmatchinv FOREIGN KEY (m_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_matchpo minoutline_mmatchpo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_matchpo
    ADD CONSTRAINT minoutline_mmatchpo FOREIGN KEY (m_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_packageline minoutline_mpackageline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_packageline
    ADD CONSTRAINT minoutline_mpackageline FOREIGN KEY (m_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_rmaline minoutline_mrmaline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_rmaline
    ADD CONSTRAINT minoutline_mrmaline FOREIGN KEY (m_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transaction minoutline_mtransaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transaction
    ADD CONSTRAINT minoutline_mtransaction FOREIGN KEY (m_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transactionallocation minoutline_mtrxalloc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transactionallocation
    ADD CONSTRAINT minoutline_mtrxalloc FOREIGN KEY (m_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline minoutline_ref; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT minoutline_ref FOREIGN KEY (ref_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_transaction minoutline_ttransaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_transaction
    ADD CONSTRAINT minoutline_ttransaction FOREIGN KEY (m_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_inoutlineconfirm minoutlineconfirm_import; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_inoutlineconfirm
    ADD CONSTRAINT minoutlineconfirm_import FOREIGN KEY (m_inoutlineconfirm_id) REFERENCES ampere.m_inoutlineconfirm(m_inoutlineconfirm_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transactionallocation minoutlineout_mtrxalloc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transactionallocation
    ADD CONSTRAINT minoutlineout_mtrxalloc FOREIGN KEY (out_m_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_inventory minventory_iinventory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_inventory
    ADD CONSTRAINT minventory_iinventory FOREIGN KEY (m_inventory_id) REFERENCES ampere.m_inventory(m_inventory_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutconfirm minventory_minoutconfirm; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutconfirm
    ADD CONSTRAINT minventory_minoutconfirm FOREIGN KEY (m_inventory_id) REFERENCES ampere.m_inventory(m_inventory_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventoryline minventory_minventoryline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventoryline
    ADD CONSTRAINT minventory_minventoryline FOREIGN KEY (m_inventory_id) REFERENCES ampere.m_inventory(m_inventory_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movementconfirm minventory_mmovconfirm; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementconfirm
    ADD CONSTRAINT minventory_mmovconfirm FOREIGN KEY (m_inventory_id) REFERENCES ampere.m_inventory(m_inventory_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_inventory minventoryline_iinventory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_inventory
    ADD CONSTRAINT minventoryline_iinventory FOREIGN KEY (m_inventoryline_id) REFERENCES ampere.m_inventoryline(m_inventoryline_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventorylinema minventoryline_milinema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventorylinema
    ADD CONSTRAINT minventoryline_milinema FOREIGN KEY (m_inventoryline_id) REFERENCES ampere.m_inventoryline(m_inventoryline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutlineconfirm minventoryline_minoutlineconfi; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutlineconfirm
    ADD CONSTRAINT minventoryline_minoutlineconfi FOREIGN KEY (m_inventoryline_id) REFERENCES ampere.m_inventoryline(m_inventoryline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movementlineconfirm minventoryline_mmovlineconfirm; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementlineconfirm
    ADD CONSTRAINT minventoryline_mmovlineconfirm FOREIGN KEY (m_inventoryline_id) REFERENCES ampere.m_inventoryline(m_inventoryline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transaction minventoryline_mtransaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transaction
    ADD CONSTRAINT minventoryline_mtransaction FOREIGN KEY (m_inventoryline_id) REFERENCES ampere.m_inventoryline(m_inventoryline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transactionallocation minventoryline_mtrxalloc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transactionallocation
    ADD CONSTRAINT minventoryline_mtrxalloc FOREIGN KEY (m_inventoryline_id) REFERENCES ampere.m_inventoryline(m_inventoryline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_transaction minventoryline_ttransaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_transaction
    ADD CONSTRAINT minventoryline_ttransaction FOREIGN KEY (m_inventoryline_id) REFERENCES ampere.m_inventoryline(m_inventoryline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transactionallocation minventorylineout_mtrxalloc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transactionallocation
    ADD CONSTRAINT minventorylineout_mtrxalloc FOREIGN KEY (out_m_inventoryline_id) REFERENCES ampere.m_inventoryline(m_inventoryline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination mlocationfrom_vc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT mlocationfrom_vc FOREIGN KEY (c_locfrom_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination mlocationto_vc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT mlocationto_vc FOREIGN KEY (c_locto_id) REFERENCES ampere.c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset mlocator_aasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset
    ADD CONSTRAINT mlocator_aasset FOREIGN KEY (m_locator_id) REFERENCES ampere.m_locator(m_locator_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectissue mlocator_cprojectissue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectissue
    ADD CONSTRAINT mlocator_cprojectissue FOREIGN KEY (m_locator_id) REFERENCES ampere.m_locator(m_locator_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct mlocator_factacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT mlocator_factacct FOREIGN KEY (m_locator_id) REFERENCES ampere.m_locator(m_locator_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_asset mlocator_iasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_asset
    ADD CONSTRAINT mlocator_iasset FOREIGN KEY (m_locator_id) REFERENCES ampere.m_locator(m_locator_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_inventory mlocator_iinventory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_inventory
    ADD CONSTRAINT mlocator_iinventory FOREIGN KEY (m_locator_id) REFERENCES ampere.m_locator(m_locator_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline mlocator_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT mlocator_minoutline FOREIGN KEY (m_locator_id) REFERENCES ampere.m_locator(m_locator_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transaction mlocator_minventorycount; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transaction
    ADD CONSTRAINT mlocator_minventorycount FOREIGN KEY (m_locator_id) REFERENCES ampere.m_locator(m_locator_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventoryline mlocator_minventoryline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventoryline
    ADD CONSTRAINT mlocator_minventoryline FOREIGN KEY (m_locator_id) REFERENCES ampere.m_locator(m_locator_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movementline mlocator_movementline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementline
    ADD CONSTRAINT mlocator_movementline FOREIGN KEY (m_locatorto_id) REFERENCES ampere.m_locator(m_locator_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product mlocator_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT mlocator_mproduct FOREIGN KEY (m_locator_id) REFERENCES ampere.m_locator(m_locator_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productionline mlocator_mproductionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productionline
    ADD CONSTRAINT mlocator_mproductionline FOREIGN KEY (m_locator_id) REFERENCES ampere.m_locator(m_locator_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productionplan mlocator_mproductionplan; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productionplan
    ADD CONSTRAINT mlocator_mproductionplan FOREIGN KEY (m_locator_id) REFERENCES ampere.m_locator(m_locator_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_replenish mlocator_mreplenish; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_replenish
    ADD CONSTRAINT mlocator_mreplenish FOREIGN KEY (m_locator_id) REFERENCES ampere.m_locator(m_locator_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_transaction mlocator_ttransaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_transaction
    ADD CONSTRAINT mlocator_ttransaction FOREIGN KEY (m_locator_id) REFERENCES ampere.m_locator(m_locator_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movementline mlocatorto_mmovementline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementline
    ADD CONSTRAINT mlocatorto_mmovementline FOREIGN KEY (m_locator_id) REFERENCES ampere.m_locator(m_locator_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_attributesetinstance mlot_mattributesetinstance; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributesetinstance
    ADD CONSTRAINT mlot_mattributesetinstance FOREIGN KEY (m_lot_id) REFERENCES ampere.m_lot(m_lot_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_attributeset mlotctl_mattributeset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributeset
    ADD CONSTRAINT mlotctl_mattributeset FOREIGN KEY (m_lotctl_id) REFERENCES ampere.m_lotctl(m_lotctl_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_lot mlotctl_mlot; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_lot
    ADD CONSTRAINT mlotctl_mlot FOREIGN KEY (m_lotctl_id) REFERENCES ampere.m_lotctl(m_lotctl_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_lotctlexclude mlotctl_mlotctlexclude; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_lotctlexclude
    ADD CONSTRAINT mlotctl_mlotctlexclude FOREIGN KEY (m_lotctl_id) REFERENCES ampere.m_lotctl(m_lotctl_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movementconfirm mmovement_mmovementconfirm; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementconfirm
    ADD CONSTRAINT mmovement_mmovementconfirm FOREIGN KEY (m_movement_id) REFERENCES ampere.m_movement(m_movement_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movementline mmovement_mmovementline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementline
    ADD CONSTRAINT mmovement_mmovementline FOREIGN KEY (m_movement_id) REFERENCES ampere.m_movement(m_movement_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movementlineconfirm mmovementconfirm_mmovlineconf; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementlineconfirm
    ADD CONSTRAINT mmovementconfirm_mmovlineconf FOREIGN KEY (m_movementconfirm_id) REFERENCES ampere.m_movementconfirm(m_movementconfirm_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movementlinema mmovementline_mmovementlinema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementlinema
    ADD CONSTRAINT mmovementline_mmovementlinema FOREIGN KEY (m_movementline_id) REFERENCES ampere.m_movementline(m_movementline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movementlineconfirm mmovementline_mmovlineconfirm; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementlineconfirm
    ADD CONSTRAINT mmovementline_mmovlineconfirm FOREIGN KEY (m_movementline_id) REFERENCES ampere.m_movementline(m_movementline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transaction mmovementline_mtransaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transaction
    ADD CONSTRAINT mmovementline_mtransaction FOREIGN KEY (m_movementline_id) REFERENCES ampere.m_movementline(m_movementline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_transaction mmovementline_ttransaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_transaction
    ADD CONSTRAINT mmovementline_ttransaction FOREIGN KEY (m_movementline_id) REFERENCES ampere.m_movementline(m_movementline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_delivery moutline_aassetdelivery; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_delivery
    ADD CONSTRAINT moutline_aassetdelivery FOREIGN KEY (m_inoutline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_packageline mpackage_mpackageline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_packageline
    ADD CONSTRAINT mpackage_mpackageline FOREIGN KEY (m_package_id) REFERENCES ampere.m_package(m_package_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventory mperpetualinv_minventory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventory
    ADD CONSTRAINT mperpetualinv_minventory FOREIGN KEY (m_perpetualinv_id) REFERENCES ampere.m_perpetualinv(m_perpetualinv_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_inventoryvalue mplversion_tinventoryvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_inventoryvalue
    ADD CONSTRAINT mplversion_tinventoryvalue FOREIGN KEY (m_pricelist_version_id) REFERENCES ampere.m_pricelist_version(m_pricelist_version_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner mpricelist_cbpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT mpricelist_cbpartner FOREIGN KEY (m_pricelist_id) REFERENCES ampere.m_pricelist(m_pricelist_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group mpricelist_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group
    ADD CONSTRAINT mpricelist_cbpgroup FOREIGN KEY (m_pricelist_id) REFERENCES ampere.m_pricelist(m_pricelist_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice mpricelist_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT mpricelist_cinvoice FOREIGN KEY (m_pricelist_id) REFERENCES ampere.m_pricelist(m_pricelist_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice mpricelist_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT mpricelist_iinvoice FOREIGN KEY (m_pricelist_id) REFERENCES ampere.m_pricelist(m_pricelist_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order mpricelist_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT mpricelist_iorder FOREIGN KEY (m_pricelist_id) REFERENCES ampere.m_pricelist(m_pricelist_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_pricelist mpricelist_ipricelist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_pricelist
    ADD CONSTRAINT mpricelist_ipricelist FOREIGN KEY (m_pricelist_id) REFERENCES ampere.m_pricelist(m_pricelist_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_forecast mpricelist_mforecast; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_forecast
    ADD CONSTRAINT mpricelist_mforecast FOREIGN KEY (m_pricelist_id) REFERENCES ampere.m_pricelist(m_pricelist_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_pricelist_version mpricelist_mpricelistversion; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_pricelist_version
    ADD CONSTRAINT mpricelist_mpricelistversion FOREIGN KEY (m_pricelist_id) REFERENCES ampere.m_pricelist(m_pricelist_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotionprecondition mpricelist_mpromotionprecondit; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotionprecondition
    ADD CONSTRAINT mpricelist_mpromotionprecondit FOREIGN KEY (m_pricelist_id) REFERENCES ampere.m_pricelist(m_pricelist_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order mpricelist_soheader; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT mpricelist_soheader FOREIGN KEY (m_pricelist_id) REFERENCES ampere.m_pricelist(m_pricelist_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpense mpricelist_stimeexpense; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpense
    ADD CONSTRAINT mpricelist_stimeexpense FOREIGN KEY (m_pricelist_id) REFERENCES ampere.m_pricelist(m_pricelist_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group mpricelistpo_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group
    ADD CONSTRAINT mpricelistpo_cbpgroup FOREIGN KEY (po_pricelist_id) REFERENCES ampere.m_pricelist(m_pricelist_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner mpricelistpo_cbuspartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner
    ADD CONSTRAINT mpricelistpo_cbuspartner FOREIGN KEY (po_pricelist_id) REFERENCES ampere.m_pricelist(m_pricelist_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productprice mpricelistver_mproductprice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productprice
    ADD CONSTRAINT mpricelistver_mproductprice FOREIGN KEY (m_pricelist_version_id) REFERENCES ampere.m_pricelist_version(m_pricelist_version_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project mpricelistversion_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project
    ADD CONSTRAINT mpricelistversion_cproject FOREIGN KEY (m_pricelist_version_id) REFERENCES ampere.m_pricelist_version(m_pricelist_version_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_pricelist mpricelistversion_ipricelist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_pricelist
    ADD CONSTRAINT mpricelistversion_ipricelist FOREIGN KEY (m_pricelist_version_id) REFERENCES ampere.m_pricelist_version(m_pricelist_version_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productpricevendorbreak mpricelistversion_mproductpric; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productpricevendorbreak
    ADD CONSTRAINT mpricelistversion_mproductpric FOREIGN KEY (m_pricelist_version_id) REFERENCES ampere.m_pricelist_version(m_pricelist_version_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_pricelist_version mpricelistversionbase_mpriceli; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_pricelist_version
    ADD CONSTRAINT mpricelistversionbase_mpriceli FOREIGN KEY (m_pricelist_version_base_id) REFERENCES ampere.m_pricelist_version(m_pricelist_version_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_requisition mprocelist_mrequisition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_requisition
    ADD CONSTRAINT mprocelist_mrequisition FOREIGN KEY (m_pricelist_id) REFERENCES ampere.m_pricelist(m_pricelist_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct mprodcat_mprodcatacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT mprodcat_mprodcatacct FOREIGN KEY (m_product_category_id) REFERENCES ampere.m_product_category(m_product_category_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfq_topicsubscriberonly mprodcategory_crfqtsubonly; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq_topicsubscriberonly
    ADD CONSTRAINT mprodcategory_crfqtsubonly FOREIGN KEY (m_product_category_id) REFERENCES ampere.m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_discountschemabreak mprodcategory_mdiscountsbreak; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_discountschemabreak
    ADD CONSTRAINT mprodcategory_mdiscountsbreak FOREIGN KEY (m_product_category_id) REFERENCES ampere.m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_discountschemaline mprodcategory_mdiscountsline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_discountschemaline
    ADD CONSTRAINT mprodcategory_mdiscountsline FOREIGN KEY (m_product_category_id) REFERENCES ampere.m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_perpetualinv mprodcategory_mperpetualinv; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_perpetualinv
    ADD CONSTRAINT mprodcategory_mperpetualinv FOREIGN KEY (m_product_category_id) REFERENCES ampere.m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_resourcetype mprodcategory_sresourcetype; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_resourcetype
    ADD CONSTRAINT mprodcategory_sresourcetype FOREIGN KEY (m_product_category_id) REFERENCES ampere.m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset mproduct_aasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset
    ADD CONSTRAINT mproduct_aasset FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_registration mproduct_aregistration; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_registration
    ADD CONSTRAINT mproduct_aregistration FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_registrationproduct mproduct_aregproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_registrationproduct
    ADD CONSTRAINT mproduct_aregproduct FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_bom mproduct_bomproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_bom
    ADD CONSTRAINT mproduct_bomproduct FOREIGN KEY (m_productbom_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_element mproduct_caschemaelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_element
    ADD CONSTRAINT mproduct_caschemaelement FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bpartner_product mproduct_cbpproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bpartner_product
    ADD CONSTRAINT mproduct_cbpproduct FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplanline mproduct_ccashplanline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplanline
    ADD CONSTRAINT mproduct_ccashplanline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commissionline mproduct_ccommissionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissionline
    ADD CONSTRAINT mproduct_ccommissionline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline mproduct_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT mproduct_cinvoiceline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_landedcost mproduct_clandedcost; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_landedcost
    ADD CONSTRAINT mproduct_clandedcost FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_landedcostallocation mproduct_clandedcostalloc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_landedcostallocation
    ADD CONSTRAINT mproduct_clandedcostalloc FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline mproduct_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT mproduct_corderline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_phase mproduct_cphase; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_phase
    ADD CONSTRAINT mproduct_cphase FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectissue mproduct_cprojectissue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectissue
    ADD CONSTRAINT mproduct_cprojectissue FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectline mproduct_cprojectline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectline
    ADD CONSTRAINT mproduct_cprojectline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectphase mproduct_cprojectphase; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectphase
    ADD CONSTRAINT mproduct_cprojectphase FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projecttask mproduct_cprojecttask; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projecttask
    ADD CONSTRAINT mproduct_cprojecttask FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfqline mproduct_crfqline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfqline
    ADD CONSTRAINT mproduct_crfqline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_rfq_topicsubscriberonly mproduct_crfqtsubonly; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_rfq_topicsubscriberonly
    ADD CONSTRAINT mproduct_crfqtsubonly FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_servicelevel mproduct_cservicelevel; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_servicelevel
    ADD CONSTRAINT mproduct_cservicelevel FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_subscription mproduct_csubscription; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_subscription
    ADD CONSTRAINT mproduct_csubscription FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_task mproduct_ctask; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_task
    ADD CONSTRAINT mproduct_ctask FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdefinition mproduct_ctaxdefinition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdefinition
    ADD CONSTRAINT mproduct_ctaxdefinition FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_uom_conversion mproduct_cuomconversion; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_uom_conversion
    ADD CONSTRAINT mproduct_cuomconversion FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary mproduct_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT mproduct_factacctsummary FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distribution mproduct_gldist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distribution
    ADD CONSTRAINT mproduct_gldist FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_distributionline mproduct_gldistline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_distributionline
    ADD CONSTRAINT mproduct_gldistline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_asset mproduct_iasset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_asset
    ADD CONSTRAINT mproduct_iasset FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal mproduct_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT mproduct_ifajournal FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_inventory mproduct_iinventory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_inventory
    ADD CONSTRAINT mproduct_iinventory FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order mproduct_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT mproduct_iorder FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_pricelist mproduct_ipricelist; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_pricelist
    ADD CONSTRAINT mproduct_ipricelist FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_product mproduct_iproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_product
    ADD CONSTRAINT mproduct_iproduct FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_bom mproduct_mbom; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_bom
    ADD CONSTRAINT mproduct_mbom FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_bomalternative mproduct_mbomalternative; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_bomalternative
    ADD CONSTRAINT mproduct_mbomalternative FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_bomproduct mproduct_mbomproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_bomproduct
    ADD CONSTRAINT mproduct_mbomproduct FOREIGN KEY (m_productbom_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_cost mproduct_mcost; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_cost
    ADD CONSTRAINT mproduct_mcost FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_demandline mproduct_mdemandline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_demandline
    ADD CONSTRAINT mproduct_mdemandline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_discountschemabreak mproduct_mdiscountsbreak; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_discountschemabreak
    ADD CONSTRAINT mproduct_mdiscountsbreak FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_discountschemaline mproduct_mdiscountsline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_discountschemaline
    ADD CONSTRAINT mproduct_mdiscountsline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_distributionrunline mproduct_mdistributionrun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_distributionrunline
    ADD CONSTRAINT mproduct_mdistributionrun FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_forecastline mproduct_mforecastline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_forecastline
    ADD CONSTRAINT mproduct_mforecastline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline mproduct_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT mproduct_minoutline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transaction mproduct_minventorycount; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transaction
    ADD CONSTRAINT mproduct_minventorycount FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventoryline mproduct_minventoryline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventoryline
    ADD CONSTRAINT mproduct_minventoryline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_lot mproduct_mlot; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_lot
    ADD CONSTRAINT mproduct_mlot FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_matchinv mproduct_mmatchinv; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_matchinv
    ADD CONSTRAINT mproduct_mmatchinv FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_matchpo mproduct_mmatchpo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_matchpo
    ADD CONSTRAINT mproduct_mmatchpo FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movementline mproduct_mmovementline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementline
    ADD CONSTRAINT mproduct_mmovementline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_bom mproduct_mproductbom; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_bom
    ADD CONSTRAINT mproduct_mproductbom FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product mproduct_mproductcategory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT mproduct_mproductcategory FOREIGN KEY (m_product_category_id) REFERENCES ampere.m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_costing mproduct_mproductcosting; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_costing
    ADD CONSTRAINT mproduct_mproductcosting FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productdownload mproduct_mproductdownload; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productdownload
    ADD CONSTRAINT mproduct_mproductdownload FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productionline mproduct_mproductionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productionline
    ADD CONSTRAINT mproduct_mproductionline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productionplan mproduct_mproductionplan; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productionplan
    ADD CONSTRAINT mproduct_mproductionplan FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productoperation mproduct_mproductoperation; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productoperation
    ADD CONSTRAINT mproduct_mproductoperation FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productprice mproduct_mproductprice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productprice
    ADD CONSTRAINT mproduct_mproductprice FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productpricevendorbreak mproduct_mproductpricevendorbr; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productpricevendorbreak
    ADD CONSTRAINT mproduct_mproductpricevendorbr FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_trl mproduct_mproducttrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_trl
    ADD CONSTRAINT mproduct_mproducttrl FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotiongroupline mproduct_mpromotiongroupline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotiongroupline
    ADD CONSTRAINT mproduct_mpromotiongroupline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_relatedproduct mproduct_mrelated_product; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_relatedproduct
    ADD CONSTRAINT mproduct_mrelated_product FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_relatedproduct mproduct_mrelatedproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_relatedproduct
    ADD CONSTRAINT mproduct_mrelatedproduct FOREIGN KEY (relatedproduct_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_requisitionline mproduct_mrequisitionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_requisitionline
    ADD CONSTRAINT mproduct_mrequisitionline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transactionallocation mproduct_mtrxalloc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transactionallocation
    ADD CONSTRAINT mproduct_mtrxalloc FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_gljournal mproduct_ogljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_gljournal
    ADD CONSTRAINT mproduct_ogljournal FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_goalrestriction mproduct_pagoalrestriction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_goalrestriction
    ADD CONSTRAINT mproduct_pagoalrestriction FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcolumn mproduct_pareportcolumn; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT mproduct_pareportcolumn FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportsource mproduct_pareportsource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportsource
    ADD CONSTRAINT mproduct_pareportsource FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_category mproduct_rcategory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_category
    ADD CONSTRAINT mproduct_rcategory FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request mproduct_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT mproduct_rrequest FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction mproduct_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT mproduct_rrequestaction FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpenseline mproduct_stimeexpenseline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpenseline
    ADD CONSTRAINT mproduct_stimeexpenseline FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_storage mproduct_storage; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_storage
    ADD CONSTRAINT mproduct_storage FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_training_class mproduct_strainingclass; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_training_class
    ADD CONSTRAINT mproduct_strainingclass FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_substitute mproduct_substitute; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_substitute
    ADD CONSTRAINT mproduct_substitute FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_substitute mproduct_substitutesub; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_substitute
    ADD CONSTRAINT mproduct_substitutesub FOREIGN KEY (substitute_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_distributionrundetail mproduct_tdrdetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_distributionrundetail
    ADD CONSTRAINT mproduct_tdrdetail FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_inventoryvalue mproduct_tinventoryvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_inventoryvalue
    ADD CONSTRAINT mproduct_tinventoryvalue FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_replenish mproduct_treplenish; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_replenish
    ADD CONSTRAINT mproduct_treplenish FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_transaction mproduct_ttransaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_transaction
    ADD CONSTRAINT mproduct_ttransaction FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination mproduct_vc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT mproduct_vc FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_commissionline mproductcat_ccommissionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_commissionline
    ADD CONSTRAINT mproductcat_ccommissionline FOREIGN KEY (m_product_category_id) REFERENCES ampere.m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectline mproductcat_cprojectline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectline
    ADD CONSTRAINT mproductcat_cprojectline FOREIGN KEY (m_product_category_id) REFERENCES ampere.m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_goalrestriction mproductcat_pagoalrestriction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_goalrestriction
    ADD CONSTRAINT mproductcat_pagoalrestriction FOREIGN KEY (m_product_category_id) REFERENCES ampere.m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category mproductcat_parentcat; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category
    ADD CONSTRAINT mproductcat_parentcat FOREIGN KEY (m_product_category_parent_id) REFERENCES ampere.m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_taxdefinition mproductcategory_ctaxdefinitio; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_taxdefinition
    ADD CONSTRAINT mproductcategory_ctaxdefinitio FOREIGN KEY (m_product_category_id) REFERENCES ampere.m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_product mproductcategory_iproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_product
    ADD CONSTRAINT mproductcategory_iproduct FOREIGN KEY (m_product_category_id) REFERENCES ampere.m_product_category(m_product_category_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_expensetype mproductcategory_sexpensetype; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_expensetype
    ADD CONSTRAINT mproductcategory_sexpensetype FOREIGN KEY (m_product_category_id) REFERENCES ampere.m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_training mproductcategory_straining; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_training
    ADD CONSTRAINT mproductcategory_straining FOREIGN KEY (m_product_category_id) REFERENCES ampere.m_product_category(m_product_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: a_asset_delivery mproductdl_aassetdelivery; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.a_asset_delivery
    ADD CONSTRAINT mproductdl_aassetdelivery FOREIGN KEY (m_productdownload_id) REFERENCES ampere.m_productdownload(m_productdownload_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_clientinfo mproductfreight_adclientinfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_clientinfo
    ADD CONSTRAINT mproductfreight_adclientinfo FOREIGN KEY (m_productfreight_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productionplan mproduction_plan; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productionplan
    ADD CONSTRAINT mproduction_plan FOREIGN KEY (m_production_id) REFERENCES ampere.m_production(m_production_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productionlinema mproductionline_mplinema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productionlinema
    ADD CONSTRAINT mproductionline_mplinema FOREIGN KEY (m_productionline_id) REFERENCES ampere.m_productionline(m_productionline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transactionallocation mproductionline_mtrxalloc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transactionallocation
    ADD CONSTRAINT mproductionline_mtrxalloc FOREIGN KEY (m_productionline_id) REFERENCES ampere.m_productionline(m_productionline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_transaction mproductionline_ttransaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_transaction
    ADD CONSTRAINT mproductionline_ttransaction FOREIGN KEY (m_productionline_id) REFERENCES ampere.m_productionline(m_productionline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transactionallocation mproductionlineout_mtrxalloc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transactionallocation
    ADD CONSTRAINT mproductionlineout_mtrxalloc FOREIGN KEY (out_m_productionline_id) REFERENCES ampere.m_productionline(m_productionline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_productionline mproductionplan_line; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_productionline
    ADD CONSTRAINT mproductionplan_line FOREIGN KEY (m_productionplan_id) REFERENCES ampere.m_productionplan(m_productionplan_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_bomproduct mproductop_mbomproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_bomproduct
    ADD CONSTRAINT mproductop_mbomproduct FOREIGN KEY (m_productoperation_id) REFERENCES ampere.m_productoperation(m_productoperation_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_operationresource mproductop_mopresource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_operationresource
    ADD CONSTRAINT mproductop_mopresource FOREIGN KEY (m_productoperation_id) REFERENCES ampere.m_productoperation(m_productoperation_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request mproductspent_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT mproductspent_rrequest FOREIGN KEY (m_productspent_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction mproductspent_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT mproductspent_rrequestaction FOREIGN KEY (m_productspent_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestupdate mproductspent_rrequestupdate; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestupdate
    ADD CONSTRAINT mproductspent_rrequestupdate FOREIGN KEY (m_productspent_id) REFERENCES ampere.m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline mpromotion_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT mpromotion_corderline FOREIGN KEY (m_promotion_id) REFERENCES ampere.m_promotion(m_promotion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotiondistribution mpromotion_mpromotiondistribut; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotiondistribution
    ADD CONSTRAINT mpromotion_mpromotiondistribut FOREIGN KEY (m_promotion_id) REFERENCES ampere.m_promotion(m_promotion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotionline mpromotion_mpromotionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotionline
    ADD CONSTRAINT mpromotion_mpromotionline FOREIGN KEY (m_promotion_id) REFERENCES ampere.m_promotion(m_promotion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotionprecondition mpromotion_mpromotionprecondit; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotionprecondition
    ADD CONSTRAINT mpromotion_mpromotionprecondit FOREIGN KEY (m_promotion_id) REFERENCES ampere.m_promotion(m_promotion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotionreward mpromotion_mpromotionreward; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotionreward
    ADD CONSTRAINT mpromotion_mpromotionreward FOREIGN KEY (m_promotion_id) REFERENCES ampere.m_promotion(m_promotion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotionreward mpromotiondistribution_mpromot; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotionreward
    ADD CONSTRAINT mpromotiondistribution_mpromot FOREIGN KEY (m_promotiondistribution_id) REFERENCES ampere.m_promotiondistribution(m_promotiondistribution_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotiongroupline mpromotiongroup_mpromotiongrou; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotiongroupline
    ADD CONSTRAINT mpromotiongroup_mpromotiongrou FOREIGN KEY (m_promotiongroup_id) REFERENCES ampere.m_promotiongroup(m_promotiongroup_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotionline mpromotiongroup_mpromotionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotionline
    ADD CONSTRAINT mpromotiongroup_mpromotionline FOREIGN KEY (m_promotiongroup_id) REFERENCES ampere.m_promotiongroup(m_promotiongroup_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotiondistribution mpromotionline_mpromotiondistr; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotiondistribution
    ADD CONSTRAINT mpromotionline_mpromotiondistr FOREIGN KEY (m_promotionline_id) REFERENCES ampere.m_promotionline(m_promotionline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_demanddetail mreqline_mdemanddetail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_demanddetail
    ADD CONSTRAINT mreqline_mdemanddetail FOREIGN KEY (m_requisitionline_id) REFERENCES ampere.m_requisitionline(m_requisitionline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_requisitionline mrequisition_mrequisitionline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_requisitionline
    ADD CONSTRAINT mrequisition_mrequisitionline FOREIGN KEY (m_requisition_id) REFERENCES ampere.m_requisition(m_requisition_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice mrma_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT mrma_cinvoice FOREIGN KEY (m_rma_id) REFERENCES ampere.m_rma(m_rma_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout mrma_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT mrma_minout FOREIGN KEY (m_rma_id) REFERENCES ampere.m_rma(m_rma_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_rmaline mrma_mrmaline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_rmaline
    ADD CONSTRAINT mrma_mrmaline FOREIGN KEY (m_rma_id) REFERENCES ampere.m_rma(m_rma_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request mrma_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT mrma_rrequest FOREIGN KEY (m_rma_id) REFERENCES ampere.m_rma(m_rma_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction mrma_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT mrma_rrequestaction FOREIGN KEY (m_rma_id) REFERENCES ampere.m_rma(m_rma_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline mrmaline_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT mrmaline_cinvoiceline FOREIGN KEY (m_rmaline_id) REFERENCES ampere.m_rmaline(m_rmaline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline mrmaline_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT mrmaline_minoutline FOREIGN KEY (m_rmaline_id) REFERENCES ampere.m_rmaline(m_rmaline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_rma mrmatype_mrma; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_rma
    ADD CONSTRAINT mrmatype_mrma FOREIGN KEY (m_rmatype_id) REFERENCES ampere.m_rmatype(m_rmatype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: mrp_runline mrp_run_mrp_runline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.mrp_runline
    ADD CONSTRAINT mrp_run_mrp_runline FOREIGN KEY (mrp_run_id) REFERENCES ampere.mrp_run(mrp_run_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_attributeset msernoctl_attributeset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_attributeset
    ADD CONSTRAINT msernoctl_attributeset FOREIGN KEY (m_sernoctl_id) REFERENCES ampere.m_sernoctl(m_sernoctl_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_sernoctlexclude msernoctl_msernoctlexclude; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_sernoctlexclude
    ADD CONSTRAINT msernoctl_msernoctlexclude FOREIGN KEY (m_sernoctl_id) REFERENCES ampere.m_sernoctl(m_sernoctl_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order mshipper_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT mshipper_corder FOREIGN KEY (m_shipper_id) REFERENCES ampere.m_shipper(m_shipper_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline mshipper_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT mshipper_corderline FOREIGN KEY (m_shipper_id) REFERENCES ampere.m_shipper(m_shipper_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order mshipper_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT mshipper_iorder FOREIGN KEY (m_shipper_id) REFERENCES ampere.m_shipper(m_shipper_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_freight mshipper_mfreight; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_freight
    ADD CONSTRAINT mshipper_mfreight FOREIGN KEY (m_shipper_id) REFERENCES ampere.m_shipper(m_shipper_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout mshipper_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT mshipper_minout FOREIGN KEY (m_shipper_id) REFERENCES ampere.m_shipper(m_shipper_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movement mshipper_mmovement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT mshipper_mmovement FOREIGN KEY (m_shipper_id) REFERENCES ampere.m_shipper(m_shipper_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_package mshipper_mpackage; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_package
    ADD CONSTRAINT mshipper_mpackage FOREIGN KEY (m_shipper_id) REFERENCES ampere.m_shipper(m_shipper_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotionreward mtargetdistribution_mpromotion; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotionreward
    ADD CONSTRAINT mtargetdistribution_mpromotion FOREIGN KEY (m_targetdistribution_id) REFERENCES ampere.m_promotiondistribution(m_promotiondistribution_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_transaction mtransaction_ttransaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_transaction
    ADD CONSTRAINT mtransaction_ttransaction FOREIGN KEY (m_transaction_id) REFERENCES ampere.m_transaction(m_transaction_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transactionallocation mtransactionout_mtrxalloc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transactionallocation
    ADD CONSTRAINT mtransactionout_mtrxalloc FOREIGN KEY (out_m_transaction_id) REFERENCES ampere.m_transaction(m_transaction_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_transactionallocation mttransaction_mtrxalloc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_transactionallocation
    ADD CONSTRAINT mttransaction_mtrxalloc FOREIGN KEY (m_transaction_id) REFERENCES ampere.m_transaction(m_transaction_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_orginfo mwarehouse_adorginfo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_orginfo
    ADD CONSTRAINT mwarehouse_adorginfo FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_edi mwarehouse_cbpedi; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_edi
    ADD CONSTRAINT mwarehouse_cbpedi FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_order mwarehouse_corder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_order
    ADD CONSTRAINT mwarehouse_corder FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline mwarehouse_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT mwarehouse_corderline FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project mwarehouse_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project
    ADD CONSTRAINT mwarehouse_cproject FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_inventory mwarehouse_iinventory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_inventory
    ADD CONSTRAINT mwarehouse_iinventory FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_order mwarehouse_iorder; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_order
    ADD CONSTRAINT mwarehouse_iorder FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_forecastline mwarehouse_mforecastline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_forecastline
    ADD CONSTRAINT mwarehouse_mforecastline FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout mwarehouse_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT mwarehouse_minout FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventory mwarehouse_minventory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventory
    ADD CONSTRAINT mwarehouse_minventory FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_perpetualinv mwarehouse_mperpetualinv; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_perpetualinv
    ADD CONSTRAINT mwarehouse_mperpetualinv FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_promotionprecondition mwarehouse_mpromotionprecondit; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_promotionprecondition
    ADD CONSTRAINT mwarehouse_mpromotionprecondit FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_requisition mwarehouse_mrequisition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_requisition
    ADD CONSTRAINT mwarehouse_mrequisition FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_resource mwarehouse_sresource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_resource
    ADD CONSTRAINT mwarehouse_sresource FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpense mwarehouse_stimeexpense; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpense
    ADD CONSTRAINT mwarehouse_stimeexpense FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_inventoryvalue mwarehouse_tinventoryvalue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_inventoryvalue
    ADD CONSTRAINT mwarehouse_tinventoryvalue FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_replenish mwarehouse_treplenish; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_replenish
    ADD CONSTRAINT mwarehouse_treplenish FOREIGN KEY (m_warehouse_id) REFERENCES ampere.m_warehouse(m_warehouse_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_replenish mwarehousesource_mreplenish; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_replenish
    ADD CONSTRAINT mwarehousesource_mreplenish FOREIGN KEY (m_warehousesource_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_warehouse mwarehousesource_mwarehouse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_warehouse
    ADD CONSTRAINT mwarehousesource_mwarehouse FOREIGN KEY (m_warehousesource_id) REFERENCES ampere.m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_replenish mwarehousesource_treplenish; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_replenish
    ADD CONSTRAINT mwarehousesource_treplenish FOREIGN KEY (m_warehousesource_id) REFERENCES ampere.m_warehouse(m_warehouse_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_status nextstatus_rstatus; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_status
    ADD CONSTRAINT nextstatus_rstatus FOREIGN KEY (next_status_id) REFERENCES ampere.r_status(r_status_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_invoice nproduct_iinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_invoice
    ADD CONSTRAINT nproduct_iinvoice FOREIGN KEY (m_product_id) REFERENCES ampere.m_product(m_product_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportsource pa_reportcolumn_pareportsource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportsource
    ADD CONSTRAINT pa_reportcolumn_pareportsource FOREIGN KEY (pa_reportcolumn_id) REFERENCES ampere.pa_reportcolumn(pa_reportcolumn_id);


--
-- Name: pa_benchmarkdata pabenchmark_pabenchmarkdata; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_benchmarkdata
    ADD CONSTRAINT pabenchmark_pabenchmarkdata FOREIGN KEY (pa_benchmark_id) REFERENCES ampere.pa_benchmark(pa_benchmark_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_measure pabenchmark_pameasure; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_measure
    ADD CONSTRAINT pabenchmark_pameasure FOREIGN KEY (pa_benchmark_id) REFERENCES ampere.pa_benchmark(pa_benchmark_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_goal pacolorschema_pagoal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_goal
    ADD CONSTRAINT pacolorschema_pagoal FOREIGN KEY (pa_colorschema_id) REFERENCES ampere.pa_colorschema(pa_colorschema_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_dashboardcontent_trl padashboardcontent_padashboard; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_dashboardcontent_trl
    ADD CONSTRAINT padashboardcontent_padashboard FOREIGN KEY (pa_dashboardcontent_id) REFERENCES ampere.pa_dashboardcontent(pa_dashboardcontent_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_dashboardcontent pagoal_padashboardcontent; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_dashboardcontent
    ADD CONSTRAINT pagoal_padashboardcontent FOREIGN KEY (pa_goal_id) REFERENCES ampere.pa_goal(pa_goal_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_goalrestriction pagoal_pagoalrestriction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_goalrestriction
    ADD CONSTRAINT pagoal_pagoalrestriction FOREIGN KEY (pa_goal_id) REFERENCES ampere.pa_goal(pa_goal_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_goal pagoalparent_pagoal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_goal
    ADD CONSTRAINT pagoalparent_pagoal FOREIGN KEY (pa_goalparent_id) REFERENCES ampere.pa_goal(pa_goal_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_measure pahierarchy_pameasure; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_measure
    ADD CONSTRAINT pahierarchy_pameasure FOREIGN KEY (pa_hierarchy_id) REFERENCES ampere.pa_hierarchy(pa_hierarchy_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_achievement pameasure_paachievement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_achievement
    ADD CONSTRAINT pameasure_paachievement FOREIGN KEY (pa_measure_id) REFERENCES ampere.pa_measure(pa_measure_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_goal pameasure_pagoal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_goal
    ADD CONSTRAINT pameasure_pagoal FOREIGN KEY (pa_measure_id) REFERENCES ampere.pa_measure(pa_measure_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_measure pameasurecalc_pameasure; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_measure
    ADD CONSTRAINT pameasurecalc_pameasure FOREIGN KEY (pa_measurecalc_id) REFERENCES ampere.pa_measurecalc(pa_measurecalc_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_ratioelement pameasurecalc_paratioelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_ratioelement
    ADD CONSTRAINT pameasurecalc_paratioelement FOREIGN KEY (pa_measurecalc_id) REFERENCES ampere.pa_measurecalc(pa_measurecalc_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_measure paratio_pameasure; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_measure
    ADD CONSTRAINT paratio_pameasure FOREIGN KEY (pa_ratio_id) REFERENCES ampere.pa_ratio(pa_ratio_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_ratioelement paratio_paratioelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_ratioelement
    ADD CONSTRAINT paratio_paratioelement FOREIGN KEY (pa_ratio_id) REFERENCES ampere.pa_ratio(pa_ratio_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_ratioelement paratioused_paratioelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_ratioelement
    ADD CONSTRAINT paratioused_paratioelement FOREIGN KEY (pa_ratioused_id) REFERENCES ampere.pa_ratio(pa_ratio_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_tab parentcolumn_adtab; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab
    ADD CONSTRAINT parentcolumn_adtab FOREIGN KEY (parent_column_id) REFERENCES ampere.ad_column(ad_column_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_report pareport_columnset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_report
    ADD CONSTRAINT pareport_columnset FOREIGN KEY (pa_reportcolumnset_id) REFERENCES ampere.pa_reportcolumnset(pa_reportcolumnset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_report pareport_lineset; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_report
    ADD CONSTRAINT pareport_lineset FOREIGN KEY (pa_reportlineset_id) REFERENCES ampere.pa_reportlineset(pa_reportlineset_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcolumn pareportcolumn_oper1; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT pareportcolumn_oper1 FOREIGN KEY (oper_1_id) REFERENCES ampere.pa_reportcolumn(pa_reportcolumn_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcolumn pareportcolumn_oper2; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT pareportcolumn_oper2 FOREIGN KEY (oper_2_id) REFERENCES ampere.pa_reportcolumn(pa_reportcolumn_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportcolumn pareportcolumnset_column; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportcolumn
    ADD CONSTRAINT pareportcolumnset_column FOREIGN KEY (pa_reportcolumnset_id) REFERENCES ampere.pa_reportcolumnset(pa_reportcolumnset_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary pareportcube_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT pareportcube_factacctsummary FOREIGN KEY (pa_reportcube_id) REFERENCES ampere.pa_reportcube(pa_reportcube_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_report pareportcube_pareport; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_report
    ADD CONSTRAINT pareportcube_pareport FOREIGN KEY (pa_reportcube_id) REFERENCES ampere.pa_reportcube(pa_reportcube_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_reportline pareportline_ireportline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_reportline
    ADD CONSTRAINT pareportline_ireportline FOREIGN KEY (pa_reportline_id) REFERENCES ampere.pa_reportline(pa_reportline_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportline pareportline_oper1; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportline
    ADD CONSTRAINT pareportline_oper1 FOREIGN KEY (oper_1_id) REFERENCES ampere.pa_reportline(pa_reportline_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportline pareportline_oper2; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportline
    ADD CONSTRAINT pareportline_oper2 FOREIGN KEY (oper_2_id) REFERENCES ampere.pa_reportline(pa_reportline_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportline pareportline_parent; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportline
    ADD CONSTRAINT pareportline_parent FOREIGN KEY (parent_id) REFERENCES ampere.pa_reportline(pa_reportline_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportsource pareportline_pareportsource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportsource
    ADD CONSTRAINT pareportline_pareportsource FOREIGN KEY (pa_reportline_id) REFERENCES ampere.pa_reportline(pa_reportline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: t_report pareportline_treport; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.t_report
    ADD CONSTRAINT pareportline_treport FOREIGN KEY (pa_reportline_id) REFERENCES ampere.pa_reportline(pa_reportline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_reportline pareportlineset_ireportline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_reportline
    ADD CONSTRAINT pareportlineset_ireportline FOREIGN KEY (pa_reportlineset_id) REFERENCES ampere.pa_reportlineset(pa_reportlineset_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_reportline pareportlineset_line; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_reportline
    ADD CONSTRAINT pareportlineset_line FOREIGN KEY (pa_reportlineset_id) REFERENCES ampere.pa_reportlineset(pa_reportlineset_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_reportline pareportsource_ireportline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_reportline
    ADD CONSTRAINT pareportsource_ireportline FOREIGN KEY (pa_reportsource_id) REFERENCES ampere.pa_reportsource(pa_reportsource_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_sla_goal paslacriteria_paslagoal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_sla_goal
    ADD CONSTRAINT paslacriteria_paslagoal FOREIGN KEY (pa_sla_criteria_id) REFERENCES ampere.pa_sla_criteria(pa_sla_criteria_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_sla_measure paslagoal_paslameasure; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_sla_measure
    ADD CONSTRAINT paslagoal_paslameasure FOREIGN KEY (pa_sla_goal_id) REFERENCES ampere.pa_sla_goal(pa_sla_goal_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default paveragecostvariance_cacctsche; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT paveragecostvariance_cacctsche FOREIGN KEY (p_averagecostvariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct paveragecostvariance_mproducta; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT paveragecostvariance_mproducta FOREIGN KEY (p_averagecostvariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct paveragecostvariance_mproductc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT paveragecostvariance_mproductc FOREIGN KEY (p_averagecostvariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default pburden_cacctschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT pburden_cacctschemadefault FOREIGN KEY (p_burden_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct pburden_mproductacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT pburden_mproductacct FOREIGN KEY (p_burden_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct pburden_mproductcategoryacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT pburden_mproductcategoryacct FOREIGN KEY (p_burden_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default pcostadjustment_cacctschemadef; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT pcostadjustment_cacctschemadef FOREIGN KEY (p_costadjustment_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct pcostadjustment_mproductacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT pcostadjustment_mproductacct FOREIGN KEY (p_costadjustment_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct pcostadjustment_mproductcatego; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT pcostadjustment_mproductcatego FOREIGN KEY (p_costadjustment_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default pcostofproduction_cacctschemad; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT pcostofproduction_cacctschemad FOREIGN KEY (p_costofproduction_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct pcostofproduction_mproductacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT pcostofproduction_mproductacct FOREIGN KEY (p_costofproduction_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct pcostofproduction_mproductcate; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT pcostofproduction_mproductcate FOREIGN KEY (p_costofproduction_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default pfloorstock_cacctschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT pfloorstock_cacctschemadefault FOREIGN KEY (p_floorstock_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct pfloorstock_mproductacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT pfloorstock_mproductacct FOREIGN KEY (p_floorstock_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct pfloorstock_mproductcategoryac; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT pfloorstock_mproductcategoryac FOREIGN KEY (p_floorstock_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default pinventoryclearing_cacctschema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT pinventoryclearing_cacctschema FOREIGN KEY (p_inventoryclearing_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct pinventoryclearing_mproductacc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT pinventoryclearing_mproductacc FOREIGN KEY (p_inventoryclearing_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct pinventoryclearing_mproductcat; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT pinventoryclearing_mproductcat FOREIGN KEY (p_inventoryclearing_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default plabor_cacctschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT plabor_cacctschemadefault FOREIGN KEY (p_labor_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct plabor_mproductacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT plabor_mproductacct FOREIGN KEY (p_labor_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct plabor_mproductcategoryacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT plabor_mproductcategoryacct FOREIGN KEY (p_labor_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default pmethodchangevariance_cacctsch; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT pmethodchangevariance_cacctsch FOREIGN KEY (p_methodchangevariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct pmethodchangevariance_mprodcat; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT pmethodchangevariance_mprodcat FOREIGN KEY (p_methodchangevariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct pmethodchangevariance_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT pmethodchangevariance_mproduct FOREIGN KEY (p_methodchangevariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default pmixvariance_cacctschemadefaul; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT pmixvariance_cacctschemadefaul FOREIGN KEY (p_mixvariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct pmixvariance_mproductacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT pmixvariance_mproductacct FOREIGN KEY (p_mixvariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct pmixvariance_mproductcategorya; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT pmixvariance_mproductcategorya FOREIGN KEY (p_mixvariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default poutsideprocessing_cacctschema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT poutsideprocessing_cacctschema FOREIGN KEY (p_outsideprocessing_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct poutsideprocessing_mproductacc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT poutsideprocessing_mproductacc FOREIGN KEY (p_outsideprocessing_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct poutsideprocessing_mproductcat; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT poutsideprocessing_mproductcat FOREIGN KEY (p_outsideprocessing_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default poverhead_cacctschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT poverhead_cacctschemadefault FOREIGN KEY (p_overhead_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct poverhead_mproductacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT poverhead_mproductacct FOREIGN KEY (p_overhead_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct poverhead_mproductcategoryacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT poverhead_mproductcategoryacct FOREIGN KEY (p_overhead_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_searchdefinition powindow_adsearchdefinition; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_searchdefinition
    ADD CONSTRAINT powindow_adsearchdefinition FOREIGN KEY (po_window_id) REFERENCES ampere.ad_window(ad_window_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default pratevariance_cacctschemadefau; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT pratevariance_cacctschemadefau FOREIGN KEY (p_ratevariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct pratevariance_mproductacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT pratevariance_mproductacct FOREIGN KEY (p_ratevariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct pratevariance_mproductcategory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT pratevariance_mproductcategory FOREIGN KEY (p_ratevariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default pscrap_cacctschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT pscrap_cacctschemadefault FOREIGN KEY (p_scrap_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct pscrap_mproductacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT pscrap_mproductacct FOREIGN KEY (p_scrap_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct pscrap_mproductcategoryacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT pscrap_mproductcategoryacct FOREIGN KEY (p_scrap_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default pusagevariance_cacctschemadefa; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT pusagevariance_cacctschemadefa FOREIGN KEY (p_usagevariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct pusagevariance_mproductacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT pusagevariance_mproductacct FOREIGN KEY (p_usagevariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct pusagevariance_mproductcategor; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT pusagevariance_mproductcategor FOREIGN KEY (p_usagevariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default pwip_cacctschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT pwip_cacctschemadefault FOREIGN KEY (p_wip_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct pwip_mproductacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT pwip_mproductacct FOREIGN KEY (p_wip_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct pwip_mproductcategoryacct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT pwip_mproductcategoryacct FOREIGN KEY (p_wip_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_categoryupdates rcategory_rcategoryupdates; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_categoryupdates
    ADD CONSTRAINT rcategory_rcategoryupdates FOREIGN KEY (r_category_id) REFERENCES ampere.r_category(r_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request rcategory_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT rcategory_rrequest FOREIGN KEY (r_category_id) REFERENCES ampere.r_category(r_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction rcategory_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT rcategory_rrequestaction FOREIGN KEY (r_category_id) REFERENCES ampere.r_category(r_category_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_ref_table ref_tableclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_table
    ADD CONSTRAINT ref_tableclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_ref_table ref_tableorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_ref_table
    ADD CONSTRAINT ref_tableorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_reference referenceclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_reference
    ADD CONSTRAINT referenceclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_reference referenceorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_reference
    ADD CONSTRAINT referenceorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment refpayment_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT refpayment_cpayment FOREIGN KEY (ref_payment_id) REFERENCES ampere.c_payment(c_payment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_rma refrma_mrma; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_rma
    ADD CONSTRAINT refrma_mrma FOREIGN KEY (ref_rma_id) REFERENCES ampere.m_rma(m_rma_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_rmaline refrmaline_mrmaline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_rmaline
    ADD CONSTRAINT refrmaline_mrmaline FOREIGN KEY (ref_rmaline_id) REFERENCES ampere.m_rmaline(m_rmaline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoice reversal_cinvoice; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoice
    ADD CONSTRAINT reversal_cinvoice FOREIGN KEY (reversal_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_payment reversal_cpayment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_payment
    ADD CONSTRAINT reversal_cpayment FOREIGN KEY (reversal_id) REFERENCES ampere.c_payment(c_payment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journal reversal_gljournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journal
    ADD CONSTRAINT reversal_gljournal FOREIGN KEY (reversal_id) REFERENCES ampere.gl_journal(gl_journal_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gl_journalbatch reversal_gljournalbatch; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.gl_journalbatch
    ADD CONSTRAINT reversal_gljournalbatch FOREIGN KEY (reversal_id) REFERENCES ampere.gl_journalbatch(gl_journalbatch_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout reversal_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT reversal_minout FOREIGN KEY (reversal_id) REFERENCES ampere.m_inout(m_inout_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventory reversal_minventory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventory
    ADD CONSTRAINT reversal_minventory FOREIGN KEY (reversal_id) REFERENCES ampere.m_inventory(m_inventory_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movement reversal_mmovement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT reversal_mmovement FOREIGN KEY (reversal_id) REFERENCES ampere.m_movement(m_movement_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inoutline reversalline_minoutline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inoutline
    ADD CONSTRAINT reversalline_minoutline FOREIGN KEY (reversalline_id) REFERENCES ampere.m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inventoryline reversalline_minventoryline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inventoryline
    ADD CONSTRAINT reversalline_minventoryline FOREIGN KEY (reversalline_id) REFERENCES ampere.m_inventoryline(m_inventoryline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movementline reversalline_mmovementline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movementline
    ADD CONSTRAINT reversalline_mmovementline FOREIGN KEY (reversalline_id) REFERENCES ampere.m_movementline(m_movementline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_groupupdates rgroup_rgroupupdates; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_groupupdates
    ADD CONSTRAINT rgroup_rgroupupdates FOREIGN KEY (r_group_id) REFERENCES ampere.r_group(r_group_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request rgroup_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT rgroup_rrequest FOREIGN KEY (r_group_id) REFERENCES ampere.r_group(r_group_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction rgroup_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT rgroup_rrequestaction FOREIGN KEY (r_group_id) REFERENCES ampere.r_group(r_group_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_bpartner rinterestarea_ibpartner; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_bpartner
    ADD CONSTRAINT rinterestarea_ibpartner FOREIGN KEY (r_interestarea_id) REFERENCES ampere.r_interestarea(r_interestarea_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_contactinterest rinterestarea_rcontactinterest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_contactinterest
    ADD CONSTRAINT rinterestarea_rcontactinterest FOREIGN KEY (r_interestarea_id) REFERENCES ampere.r_interestarea(r_interestarea_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request rinvoice_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT rinvoice_rrequest FOREIGN KEY (c_invoice_id) REFERENCES ampere.c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_issue rissueproject_adissue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_issue
    ADD CONSTRAINT rissueproject_adissue FOREIGN KEY (r_issueproject_id) REFERENCES ampere.r_issueproject(r_issueproject_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_issuesource rissueproject_rissuesource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issuesource
    ADD CONSTRAINT rissueproject_rissuesource FOREIGN KEY (r_issueproject_id) REFERENCES ampere.r_issueproject(r_issueproject_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_issueknown rissuerecommendation_rissuekno; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issueknown
    ADD CONSTRAINT rissuerecommendation_rissuekno FOREIGN KEY (r_issuerecommendation_id) REFERENCES ampere.r_issuerecommendation(r_issuerecommendation_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_issueknown rissuestatus_rissueknown; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issueknown
    ADD CONSTRAINT rissuestatus_rissueknown FOREIGN KEY (r_issuestatus_id) REFERENCES ampere.r_issuestatus(r_issuestatus_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_issue rissuesystem_ad_issue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_issue
    ADD CONSTRAINT rissuesystem_ad_issue FOREIGN KEY (r_issuesystem_id) REFERENCES ampere.r_issuesystem(r_issuesystem_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_issuesource rissuesystem_rissuesource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issuesource
    ADD CONSTRAINT rissuesystem_rissuesource FOREIGN KEY (r_issuesystem_id) REFERENCES ampere.r_issuesystem(r_issuesystem_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_issue rissueuser_adissue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_issue
    ADD CONSTRAINT rissueuser_adissue FOREIGN KEY (r_issueuser_id) REFERENCES ampere.r_issueuser(r_issueuser_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_issuesource rissueuser_rissuesource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issuesource
    ADD CONSTRAINT rissueuser_rissuesource FOREIGN KEY (r_issueuser_id) REFERENCES ampere.r_issueuser(r_issueuser_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_issue rknownissue_adissue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_issue
    ADD CONSTRAINT rknownissue_adissue FOREIGN KEY (r_issueknown_id) REFERENCES ampere.r_issueknown(r_issueknown_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_usermail rmailtext_adusermail; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_usermail
    ADD CONSTRAINT rmailtext_adusermail FOREIGN KEY (r_mailtext_id) REFERENCES ampere.r_mailtext(r_mailtext_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printform rmailtext_invoiceadprintform; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT rmailtext_invoiceadprintform FOREIGN KEY (invoice_mailtext_id) REFERENCES ampere.r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product rmailtext_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT rmailtext_mproduct FOREIGN KEY (r_mailtext_id) REFERENCES ampere.r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printform rmailtext_orderadprintform; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT rmailtext_orderadprintform FOREIGN KEY (order_mailtext_id) REFERENCES ampere.r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printform rmailtext_projectadprintform; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT rmailtext_projectadprintform FOREIGN KEY (project_mailtext_id) REFERENCES ampere.r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printform rmailtext_remitadprintform; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT rmailtext_remitadprintform FOREIGN KEY (remittance_mailtext_id) REFERENCES ampere.r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_mailtext_trl rmailtext_rmailtexttrl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_mailtext_trl
    ADD CONSTRAINT rmailtext_rmailtexttrl FOREIGN KEY (r_mailtext_id) REFERENCES ampere.r_mailtext(r_mailtext_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request rmailtext_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT rmailtext_rrequest FOREIGN KEY (r_mailtext_id) REFERENCES ampere.r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_printform rmailtext_shipadprintform; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_printform
    ADD CONSTRAINT rmailtext_shipadprintform FOREIGN KEY (shipment_mailtext_id) REFERENCES ampere.r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_issue rrequest_adissue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_issue
    ADD CONSTRAINT rrequest_adissue FOREIGN KEY (r_request_id) REFERENCES ampere.r_request(r_request_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request rrequest_related; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT rrequest_related FOREIGN KEY (r_requestrelated_id) REFERENCES ampere.r_request(r_request_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_issueknown rrequest_rissueknown; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_issueknown
    ADD CONSTRAINT rrequest_rissueknown FOREIGN KEY (r_request_id) REFERENCES ampere.r_request(r_request_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction rrequest_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT rrequest_rrequestaction FOREIGN KEY (r_request_id) REFERENCES ampere.r_request(r_request_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestupdate rrequest_rrequestupdate; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestupdate
    ADD CONSTRAINT rrequest_rrequestupdate FOREIGN KEY (r_request_id) REFERENCES ampere.r_request(r_request_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestupdates rrequest_rrupdates; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestupdates
    ADD CONSTRAINT rrequest_rrupdates FOREIGN KEY (r_request_id) REFERENCES ampere.r_request(r_request_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestprocessorlog rrequestprocessor_log; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestprocessorlog
    ADD CONSTRAINT rrequestprocessor_log FOREIGN KEY (r_requestprocessor_id) REFERENCES ampere.r_requestprocessor(r_requestprocessor_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestprocessor_route rrequestprocessor_route; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestprocessor_route
    ADD CONSTRAINT rrequestprocessor_route FOREIGN KEY (r_requestprocessor_id) REFERENCES ampere.r_requestprocessor(r_requestprocessor_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_userbpaccess rrequesttype_aduserbpaccess; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_userbpaccess
    ADD CONSTRAINT rrequesttype_aduserbpaccess FOREIGN KEY (r_requesttype_id) REFERENCES ampere.r_requesttype(r_requesttype_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pa_measure rrequesttype_pameasure; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.pa_measure
    ADD CONSTRAINT rrequesttype_pameasure FOREIGN KEY (r_requesttype_id) REFERENCES ampere.r_requesttype(r_requesttype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestprocessor_route rrequesttype_rprocessorrule; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestprocessor_route
    ADD CONSTRAINT rrequesttype_rprocessorrule FOREIGN KEY (r_requesttype_id) REFERENCES ampere.r_requesttype(r_requesttype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request rrequesttype_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT rrequesttype_rrequest FOREIGN KEY (r_requesttype_id) REFERENCES ampere.r_requesttype(r_requesttype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction rrequesttype_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT rrequesttype_rrequestaction FOREIGN KEY (r_requesttype_id) REFERENCES ampere.r_requesttype(r_requesttype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestprocessor rrequesttype_rrequestprocessor; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestprocessor
    ADD CONSTRAINT rrequesttype_rrequestprocessor FOREIGN KEY (r_requesttype_id) REFERENCES ampere.r_requesttype(r_requesttype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requesttypeupdates rrequesttype_rrtupdates; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requesttypeupdates
    ADD CONSTRAINT rrequesttype_rrtupdates FOREIGN KEY (r_requesttype_id) REFERENCES ampere.r_requesttype(r_requesttype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request rresolution_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT rresolution_rrequest FOREIGN KEY (r_resolution_id) REFERENCES ampere.r_resolution(r_resolution_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction rresolution_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT rresolution_rrequestaction FOREIGN KEY (r_resolution_id) REFERENCES ampere.r_resolution(r_resolution_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request rstandardresponse_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT rstandardresponse_rrequest FOREIGN KEY (r_standardresponse_id) REFERENCES ampere.r_standardresponse(r_standardresponse_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_request rstatus_rrequest; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_request
    ADD CONSTRAINT rstatus_rrequest FOREIGN KEY (r_status_id) REFERENCES ampere.r_status(r_status_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requestaction rstatus_rrequestaction; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requestaction
    ADD CONSTRAINT rstatus_rrequestaction FOREIGN KEY (r_status_id) REFERENCES ampere.r_status(r_status_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_requesttype rstatuscategory_rrequesttype; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_requesttype
    ADD CONSTRAINT rstatuscategory_rrequesttype FOREIGN KEY (r_statuscategory_id) REFERENCES ampere.r_statuscategory(r_statuscategory_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_status rstatuscategory_rstatus; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_status
    ADD CONSTRAINT rstatuscategory_rstatus FOREIGN KEY (r_statuscategory_id) REFERENCES ampere.r_statuscategory(r_statuscategory_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_dunningrunentry salesrep_cdunningrunentry; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_dunningrunentry
    ADD CONSTRAINT salesrep_cdunningrunentry FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_salesregion salesrep_csalesregion; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_salesregion
    ADD CONSTRAINT salesrep_csalesregion FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_movement salesrep_mmovement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_movement
    ADD CONSTRAINT salesrep_mmovement FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product salesrep_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT salesrep_mproduct FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_rma salesrep_mrma; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_rma
    ADD CONSTRAINT salesrep_mrma FOREIGN KEY (salesrep_id) REFERENCES ampere.ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_sequence_audit sequence_auditclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_sequence_audit
    ADD CONSTRAINT sequence_auditclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_sequence_audit sequence_auditorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_sequence_audit
    ADD CONSTRAINT sequence_auditorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_sequence_no sequence_noclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_sequence_no
    ADD CONSTRAINT sequence_noclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_sequence_no sequence_noorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_sequence_no
    ADD CONSTRAINT sequence_noorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_sequence sequenceclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_sequence
    ADD CONSTRAINT sequenceclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_sequence sequenceorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_sequence
    ADD CONSTRAINT sequenceorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product sexpensetype_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT sexpensetype_mproduct FOREIGN KEY (s_expensetype_id) REFERENCES ampere.s_expensetype(s_expensetype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct so_campaign_fact_acct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct
    ADD CONSTRAINT so_campaign_fact_acct FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_element socampaign_caschemaelement; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_element
    ADD CONSTRAINT socampaign_caschemaelement FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_validcombination socampaign_vc; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_validcombination
    ADD CONSTRAINT socampaign_vc FOREIGN KEY (c_campaign_id) REFERENCES ampere.c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_workflow sresource_adworkflow; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_workflow
    ADD CONSTRAINT sresource_adworkflow FOREIGN KEY (s_resource_id) REFERENCES ampere.s_resource(s_resource_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product sresource_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product
    ADD CONSTRAINT sresource_mproduct FOREIGN KEY (s_resource_id) REFERENCES ampere.s_resource(s_resource_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_resourceassignment sresource_sresourceassignment; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_resourceassignment
    ADD CONSTRAINT sresource_sresourceassignment FOREIGN KEY (s_resource_id) REFERENCES ampere.s_resource(s_resource_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_resourceunavailable sresource_sresunavailable; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_resourceunavailable
    ADD CONSTRAINT sresource_sresunavailable FOREIGN KEY (s_resource_id) REFERENCES ampere.s_resource(s_resource_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_invoiceline sresourceassign_cinvoiceline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_invoiceline
    ADD CONSTRAINT sresourceassign_cinvoiceline FOREIGN KEY (s_resourceassignment_id) REFERENCES ampere.s_resourceassignment(s_resourceassignment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_orderline sresourceassign_corderline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_orderline
    ADD CONSTRAINT sresourceassign_corderline FOREIGN KEY (s_resourceassignment_id) REFERENCES ampere.s_resourceassignment(s_resourceassignment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpenseline sresourceassign_steline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpenseline
    ADD CONSTRAINT sresourceassign_steline FOREIGN KEY (s_resourceassignment_id) REFERENCES ampere.s_resourceassignment(s_resourceassignment_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_resource sresourcetype_sresource; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_resource
    ADD CONSTRAINT sresourcetype_sresource FOREIGN KEY (s_resourcetype_id) REFERENCES ampere.s_resourcetype(s_resourcetype_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpenseline stimeexpense_line; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpenseline
    ADD CONSTRAINT stimeexpense_line FOREIGN KEY (s_timeexpense_id) REFERENCES ampere.s_timeexpense(s_timeexpense_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_projectissue stimeexpline_cprojectissue; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_projectissue
    ADD CONSTRAINT stimeexpline_cprojectissue FOREIGN KEY (s_timeexpenseline_id) REFERENCES ampere.s_timeexpenseline(s_timeexpenseline_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_timeexpenseline stimetype_stimeexpenseline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_timeexpenseline
    ADD CONSTRAINT stimetype_stimeexpenseline FOREIGN KEY (s_timetype_id) REFERENCES ampere.s_timetype(s_timetype_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: s_training_class straining_strainingclass; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.s_training_class
    ADD CONSTRAINT straining_strainingclass FOREIGN KEY (s_training_id) REFERENCES ampere.s_training(s_training_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_tab tabclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab
    ADD CONSTRAINT tabclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_table tableclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table
    ADD CONSTRAINT tableclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_table tableorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_table
    ADD CONSTRAINT tableorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_tab taborg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_tab
    ADD CONSTRAINT taborg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_status updatestatus_rstatus; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.r_status
    ADD CONSTRAINT updatestatus_rstatus FOREIGN KEY (update_status_id) REFERENCES ampere.r_status(r_status_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplan user1_ccashplan; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplan
    ADD CONSTRAINT user1_ccashplan FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplanline user1_ccashplanline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplanline
    ADD CONSTRAINT user1_ccashplanline FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary user1_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT user1_factacctsummary FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal user1_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT user1_ifajournal FOREIGN KEY (user1_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplan user2_ccashplan; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplan
    ADD CONSTRAINT user2_ccashplan FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashplanline user2_ccashplanline; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashplanline
    ADD CONSTRAINT user2_ccashplanline FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fact_acct_summary user2_factacctsummary; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.fact_acct_summary
    ADD CONSTRAINT user2_factacctsummary FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: i_fajournal user2_ifajournal; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.i_fajournal
    ADD CONSTRAINT user2_ifajournal FOREIGN KEY (user2_id) REFERENCES ampere.c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_val_rule val_ruleclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_val_rule
    ADD CONSTRAINT val_ruleclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_val_rule val_ruleorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_val_rule
    ADD CONSTRAINT val_ruleorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_inout vbplocation_minout; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_inout
    ADD CONSTRAINT vbplocation_minout FOREIGN KEY (c_bpartner_location_id) REFERENCES ampere.c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: test vc_account_test; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.test
    ADD CONSTRAINT vc_account_test FOREIGN KEY (account_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount_acct vc_basset_cbankaccount; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount_acct
    ADD CONSTRAINT vc_basset_cbankaccount FOREIGN KEY (b_asset_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_basset_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_basset_cschemadefault FOREIGN KEY (b_asset_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount_acct vc_bexpense_cbankaccount; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount_acct
    ADD CONSTRAINT vc_bexpense_cbankaccount FOREIGN KEY (b_expense_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_bexpense_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_bexpense_cschemadefault FOREIGN KEY (b_expense_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount_acct vc_binterestexp_cbankaccount; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount_acct
    ADD CONSTRAINT vc_binterestexp_cbankaccount FOREIGN KEY (b_interestexp_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_binterestexp_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_binterestexp_cschemadefault FOREIGN KEY (b_interestexp_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount_acct vc_binterestrev_cbankaccount; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount_acct
    ADD CONSTRAINT vc_binterestrev_cbankaccount FOREIGN KEY (b_interestrev_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_binterestrev_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_binterestrev_cschemadefault FOREIGN KEY (b_interestrev_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount_acct vc_bintransit_cbankaccount; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount_acct
    ADD CONSTRAINT vc_bintransit_cbankaccount FOREIGN KEY (b_intransit_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_bintransit_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_bintransit_cschemadefault FOREIGN KEY (b_intransit_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount_acct vc_bpaymentselect_cbankaccount; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount_acct
    ADD CONSTRAINT vc_bpaymentselect_cbankaccount FOREIGN KEY (b_paymentselect_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_bpaymentselect_cschemadefau; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_bpaymentselect_cschemadefau FOREIGN KEY (b_paymentselect_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount_acct vc_brevaluationgain_cbankaccou; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount_acct
    ADD CONSTRAINT vc_brevaluationgain_cbankaccou FOREIGN KEY (b_revaluationgain_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_brevaluationgain_cschemadef; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_brevaluationgain_cschemadef FOREIGN KEY (b_revaluationgain_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount_acct vc_brevaluationloss_cbankaccou; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount_acct
    ADD CONSTRAINT vc_brevaluationloss_cbankaccou FOREIGN KEY (b_revaluationloss_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_brevaluationloss_cschemadef; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_brevaluationloss_cschemadef FOREIGN KEY (b_revaluationloss_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount_acct vc_bsettlementgain_cbankaccoun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount_acct
    ADD CONSTRAINT vc_bsettlementgain_cbankaccoun FOREIGN KEY (b_settlementgain_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_bsettlementgain_cschemadefa; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_bsettlementgain_cschemadefa FOREIGN KEY (b_settlementgain_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount_acct vc_bsettlementloss_cbankaccoun; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount_acct
    ADD CONSTRAINT vc_bsettlementloss_cbankaccoun FOREIGN KEY (b_settlementloss_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_bsettlementloss_cschemadefa; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_bsettlementloss_cschemadefa FOREIGN KEY (b_settlementloss_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount_acct vc_bunallocatedcash_cbankaccou; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount_acct
    ADD CONSTRAINT vc_bunallocatedcash_cbankaccou FOREIGN KEY (b_unallocatedcash_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_bunallocatedcash_cschemadef; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_bunallocatedcash_cschemadef FOREIGN KEY (b_unallocatedcash_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bankaccount_acct vc_bunidentified_cbankaccount; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bankaccount_acct
    ADD CONSTRAINT vc_bunidentified_cbankaccount FOREIGN KEY (b_unidentified_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_bunidentified_cschemadefaul; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_bunidentified_cschemadefaul FOREIGN KEY (b_unidentified_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashbook_acct vc_cbasset_ccashbook; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashbook_acct
    ADD CONSTRAINT vc_cbasset_ccashbook FOREIGN KEY (cb_asset_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_cbasset_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_cbasset_cschemadefault FOREIGN KEY (cb_asset_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashbook_acct vc_cbcashtransfer_ccashbook; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashbook_acct
    ADD CONSTRAINT vc_cbcashtransfer_ccashbook FOREIGN KEY (cb_cashtransfer_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_cbcashtransfer_cschemadefau; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_cbcashtransfer_cschemadefau FOREIGN KEY (cb_cashtransfer_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashbook_acct vc_cbdifferences_ccashbook; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashbook_acct
    ADD CONSTRAINT vc_cbdifferences_ccashbook FOREIGN KEY (cb_differences_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_cbdifferences_cschemadefaul; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_cbdifferences_cschemadefaul FOREIGN KEY (cb_differences_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashbook_acct vc_cbexpense_ccashbook; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashbook_acct
    ADD CONSTRAINT vc_cbexpense_ccashbook FOREIGN KEY (cb_expense_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_cbexpense_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_cbexpense_cschemadefault FOREIGN KEY (cb_expense_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_cashbook_acct vc_cbreceipt_ccashbook; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_cashbook_acct
    ADD CONSTRAINT vc_cbreceipt_ccashbook FOREIGN KEY (cb_receipt_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_cbreceipt_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_cbreceipt_cschemadefault FOREIGN KEY (cb_receipt_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_charge_acct vc_chexpense_ccharge; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_charge_acct
    ADD CONSTRAINT vc_chexpense_ccharge FOREIGN KEY (ch_expense_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_chexpense_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_chexpense_cschemadefault FOREIGN KEY (ch_expense_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_charge_acct vc_chrevenue_ccharge; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_charge_acct
    ADD CONSTRAINT vc_chrevenue_ccharge FOREIGN KEY (ch_revenue_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_chrevenue_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_chrevenue_cschemadefault FOREIGN KEY (ch_revenue_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_customer_acct vc_cprepayment_cbpcustomer; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_customer_acct
    ADD CONSTRAINT vc_cprepayment_cbpcustomer FOREIGN KEY (c_prepayment_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group_acct vc_cprepayment_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT vc_cprepayment_cbpgroup FOREIGN KEY (c_prepayment_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_cprepayment_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_cprepayment_cschemadefault FOREIGN KEY (c_prepayment_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_customer_acct vc_creceivable_cbpcustomer; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_customer_acct
    ADD CONSTRAINT vc_creceivable_cbpcustomer FOREIGN KEY (c_receivable_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group_acct vc_creceivable_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT vc_creceivable_cbpgroup FOREIGN KEY (c_receivable_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_creceivable_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_creceivable_cschemadefault FOREIGN KEY (c_receivable_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_gl vc_currencybalancing_cschemagl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_gl
    ADD CONSTRAINT vc_currencybalancing_cschemagl FOREIGN KEY (currencybalancing_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_employee_acct vc_eexpense_cbpemployee; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_employee_acct
    ADD CONSTRAINT vc_eexpense_cbpemployee FOREIGN KEY (e_expense_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_eexpense_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_eexpense_cschemadefault FOREIGN KEY (e_expense_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_employee_acct vc_eprepayment_cbpemployee; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_employee_acct
    ADD CONSTRAINT vc_eprepayment_cbpemployee FOREIGN KEY (e_prepayment_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_eprepayment_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_eprepayment_cschemadefault FOREIGN KEY (e_prepayment_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_gl vc_incomesummary_cschemagl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_gl
    ADD CONSTRAINT vc_incomesummary_cschemagl FOREIGN KEY (incomesummary_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_interorg_acct vc_intercompanyduefrom_cintero; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_interorg_acct
    ADD CONSTRAINT vc_intercompanyduefrom_cintero FOREIGN KEY (intercompanyduefrom_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_gl vc_intercompanyduefrom_cschema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_gl
    ADD CONSTRAINT vc_intercompanyduefrom_cschema FOREIGN KEY (intercompanyduefrom_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_interorg_acct vc_intercompanydueto_cinterorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_interorg_acct
    ADD CONSTRAINT vc_intercompanydueto_cinterorg FOREIGN KEY (intercompanydueto_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_gl vc_intercompanydueto_cschemagl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_gl
    ADD CONSTRAINT vc_intercompanydueto_cschemagl FOREIGN KEY (intercompanydueto_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group_acct vc_notinvoicedrec_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT vc_notinvoicedrec_cbpgroup FOREIGN KEY (notinvoicedreceivables_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_notinvoicedrec_cschemadefau; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_notinvoicedrec_cschemadefau FOREIGN KEY (notinvoicedreceivables_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group_acct vc_notinvoicedreceipts_cbpgrou; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT vc_notinvoicedreceipts_cbpgrou FOREIGN KEY (notinvoicedreceipts_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_notinvoicedreceipts_cschema; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_notinvoicedreceipts_cschema FOREIGN KEY (notinvoicedreceipts_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group_acct vc_notinvoicedrevenue_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT vc_notinvoicedrevenue_cbpgroup FOREIGN KEY (notinvoicedrevenue_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_notinvoicedrevenue_cschemad; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_notinvoicedrevenue_cschemad FOREIGN KEY (notinvoicedrevenue_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_passet_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_passet_cschemadefault FOREIGN KEY (p_asset_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct vc_passet_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT vc_passet_mproduct FOREIGN KEY (p_asset_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct vc_passet_mproductcategory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT vc_passet_mproductcategory FOREIGN KEY (p_asset_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group_acct vc_paydiscountexp_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT vc_paydiscountexp_cbpgroup FOREIGN KEY (paydiscount_exp_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_paydiscountexp_cschemadefau; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_paydiscountexp_cschemadefau FOREIGN KEY (paydiscount_exp_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group_acct vc_paydiscountrev_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT vc_paydiscountrev_cbpgroup FOREIGN KEY (paydiscount_rev_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_paydiscountrev_cschemadefau; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_paydiscountrev_cschemadefau FOREIGN KEY (paydiscount_rev_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_pcogs_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_pcogs_cschemadefault FOREIGN KEY (p_cogs_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct vc_pcogs_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT vc_pcogs_mproduct FOREIGN KEY (p_cogs_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct vc_pcogs_mproductcategory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT vc_pcogs_mproductcategory FOREIGN KEY (p_cogs_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_pexpense_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_pexpense_cschemadefault FOREIGN KEY (p_expense_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct vc_pexpense_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT vc_pexpense_mproduct FOREIGN KEY (p_expense_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct vc_pexpense_mproductcategory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT vc_pexpense_mproductcategory FOREIGN KEY (p_expense_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_pinvoicepv_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_pinvoicepv_cschemadefault FOREIGN KEY (p_invoicepricevariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct vc_pinvoicepv_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT vc_pinvoicepv_mproduct FOREIGN KEY (p_invoicepricevariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct vc_pinvoicepv_mproductcategory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT vc_pinvoicepv_mproductcategory FOREIGN KEY (p_invoicepricevariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project_acct vc_pjasset_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project_acct
    ADD CONSTRAINT vc_pjasset_cproject FOREIGN KEY (pj_asset_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_pjasset_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_pjasset_cschemadefault FOREIGN KEY (pj_asset_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_project_acct vc_pjwip_cproject; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_project_acct
    ADD CONSTRAINT vc_pjwip_cproject FOREIGN KEY (pj_wip_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_pjwip_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_pjwip_cschemadefault FOREIGN KEY (pj_wip_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_ppurchasepv_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_ppurchasepv_cschemadefault FOREIGN KEY (p_purchasepricevariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct vc_ppurchasepv_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT vc_ppurchasepv_mproduct FOREIGN KEY (p_purchasepricevariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct vc_ppurchasepv_mproductcategor; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT vc_ppurchasepv_mproductcategor FOREIGN KEY (p_purchasepricevariance_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_gl vc_ppvoffset_cschemagl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_gl
    ADD CONSTRAINT vc_ppvoffset_cschemagl FOREIGN KEY (ppvoffset_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_revenuerecognition_plan vc_prevenue_crevenuerecognitio; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_revenuerecognition_plan
    ADD CONSTRAINT vc_prevenue_crevenuerecognitio FOREIGN KEY (p_revenue_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_prevenue_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_prevenue_cschemadefault FOREIGN KEY (p_revenue_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct vc_prevenue_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT vc_prevenue_mproduct FOREIGN KEY (p_revenue_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct vc_prevenue_mproductcategory; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT vc_prevenue_mproductcategory FOREIGN KEY (p_revenue_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_ptdiscountgrant_cschemadefa; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_ptdiscountgrant_cschemadefa FOREIGN KEY (p_tradediscountgrant_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct vc_ptdiscountgrant_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT vc_ptdiscountgrant_mproduct FOREIGN KEY (p_tradediscountgrant_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct vc_ptdiscountgrant_mproductcat; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT vc_ptdiscountgrant_mproductcat FOREIGN KEY (p_tradediscountgrant_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_ptdiscountrec_cschemadefaul; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_ptdiscountrec_cschemadefaul FOREIGN KEY (p_tradediscountrec_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_acct vc_ptdiscountrec_mproduct; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_acct
    ADD CONSTRAINT vc_ptdiscountrec_mproduct FOREIGN KEY (p_tradediscountrec_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_product_category_acct vc_ptdiscountrec_mproductcateg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_product_category_acct
    ADD CONSTRAINT vc_ptdiscountrec_mproductcateg FOREIGN KEY (p_tradediscountrec_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_currency_acct vc_realizedgain_ccurrency; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_currency_acct
    ADD CONSTRAINT vc_realizedgain_ccurrency FOREIGN KEY (realizedgain_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_realizedgain_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_realizedgain_cschemadefault FOREIGN KEY (realizedgain_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_currency_acct vc_realizedloss_ccurrency; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_currency_acct
    ADD CONSTRAINT vc_realizedloss_ccurrency FOREIGN KEY (realizedloss_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_realizedloss_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_realizedloss_cschemadefault FOREIGN KEY (realizedloss_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_gl vc_retainedearning_cschemagl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_gl
    ADD CONSTRAINT vc_retainedearning_cschemagl FOREIGN KEY (retainedearning_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_gl vc_suspensebalancing_cschemagl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_gl
    ADD CONSTRAINT vc_suspensebalancing_cschemagl FOREIGN KEY (suspensebalancing_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_gl vc_suspenseerror_cschemagl; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_gl
    ADD CONSTRAINT vc_suspenseerror_cschemagl FOREIGN KEY (suspenseerror_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_tcredit_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_tcredit_cschemadefault FOREIGN KEY (t_credit_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax_acct vc_tcredit_ctax; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax_acct
    ADD CONSTRAINT vc_tcredit_ctax FOREIGN KEY (t_credit_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_tdue_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_tdue_cschemadefault FOREIGN KEY (t_due_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax_acct vc_tdue_ctax; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax_acct
    ADD CONSTRAINT vc_tdue_ctax FOREIGN KEY (t_due_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_texpense_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_texpense_cschemadefault FOREIGN KEY (t_expense_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax_acct vc_texpense_ctax; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax_acct
    ADD CONSTRAINT vc_texpense_ctax FOREIGN KEY (t_expense_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_tliability_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_tliability_cschemadefault FOREIGN KEY (t_liability_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax_acct vc_tliability_ctax; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax_acct
    ADD CONSTRAINT vc_tliability_ctax FOREIGN KEY (t_liability_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_trec_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_trec_cschemadefault FOREIGN KEY (t_receivables_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_tax_acct vc_trec_ctax; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_tax_acct
    ADD CONSTRAINT vc_trec_ctax FOREIGN KEY (t_receivables_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group_acct vc_unearnedrevenue_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT vc_unearnedrevenue_cbpgroup FOREIGN KEY (unearnedrevenue_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_revenuerecognition_plan vc_unearnedrevenue_crevenuerec; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_revenuerecognition_plan
    ADD CONSTRAINT vc_unearnedrevenue_crevenuerec FOREIGN KEY (unearnedrevenue_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_unearnedrevenue_cschemadefa; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_unearnedrevenue_cschemadefa FOREIGN KEY (unearnedrevenue_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_currency_acct vc_unrealizedgain_ccurrency; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_currency_acct
    ADD CONSTRAINT vc_unrealizedgain_ccurrency FOREIGN KEY (unrealizedgain_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_unrealizedgain_cschemadefau; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_unrealizedgain_cschemadefau FOREIGN KEY (unrealizedgain_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_currency_acct vc_unrealizedloss_ccurrency; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_currency_acct
    ADD CONSTRAINT vc_unrealizedloss_ccurrency FOREIGN KEY (unrealizedloss_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_unrealizedloss_cschemadefau; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_unrealizedloss_cschemadefau FOREIGN KEY (unrealizedloss_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group_acct vc_vliability_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT vc_vliability_cbpgroup FOREIGN KEY (v_liability_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_vendor_acct vc_vliability_cbpvendor; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_vendor_acct
    ADD CONSTRAINT vc_vliability_cbpvendor FOREIGN KEY (v_liability_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_vliability_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_vliability_cschemadefault FOREIGN KEY (v_liability_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group_acct vc_vliabilityservices_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT vc_vliabilityservices_cbpgroup FOREIGN KEY (v_liability_services_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_vendor_acct vc_vliabilityservices_cbpvendo; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_vendor_acct
    ADD CONSTRAINT vc_vliabilityservices_cbpvendo FOREIGN KEY (v_liability_services_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_vliabilityservices_cschemad; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_vliabilityservices_cschemad FOREIGN KEY (v_liability_services_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group_acct vc_vprepayment_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT vc_vprepayment_cbpgroup FOREIGN KEY (v_prepayment_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_vendor_acct vc_vprepayment_cbpvendor; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_vendor_acct
    ADD CONSTRAINT vc_vprepayment_cbpvendor FOREIGN KEY (v_prepayment_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_vprepayment_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_vprepayment_cschemadefault FOREIGN KEY (v_prepayment_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_wdifferences_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_wdifferences_cschemadefault FOREIGN KEY (w_differences_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_warehouse_acct vc_wdifferences_mwarehouse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_warehouse_acct
    ADD CONSTRAINT vc_wdifferences_mwarehouse FOREIGN KEY (w_differences_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_winvactualadjust_cschemadef; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_winvactualadjust_cschemadef FOREIGN KEY (w_invactualadjust_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_warehouse_acct vc_winvactualadjust_mwarehouse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_warehouse_acct
    ADD CONSTRAINT vc_winvactualadjust_mwarehouse FOREIGN KEY (w_invactualadjust_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_winventory_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_winventory_cschemadefault FOREIGN KEY (w_inventory_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_warehouse_acct vc_winventory_mwarehouse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_warehouse_acct
    ADD CONSTRAINT vc_winventory_mwarehouse FOREIGN KEY (w_inventory_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_withholding_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_withholding_cschemadefault FOREIGN KEY (withholding_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_withholding_acct vc_withholding_cwithholding; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_withholding_acct
    ADD CONSTRAINT vc_withholding_cwithholding FOREIGN KEY (withholding_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_wrevaluation_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_wrevaluation_cschemadefault FOREIGN KEY (w_revaluation_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: m_warehouse_acct vc_wrevaluation_mwarehouse; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.m_warehouse_acct
    ADD CONSTRAINT vc_wrevaluation_mwarehouse FOREIGN KEY (w_revaluation_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_bp_group_acct vc_writeoff_cbpgroup; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_bp_group_acct
    ADD CONSTRAINT vc_writeoff_cbpgroup FOREIGN KEY (writeoff_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_acctschema_default vc_writeoff_cschemadefault; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.c_acctschema_default
    ADD CONSTRAINT vc_writeoff_cschemadefault FOREIGN KEY (writeoff_acct) REFERENCES ampere.c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_wf_process wf_instanceclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_wf_process
    ADD CONSTRAINT wf_instanceclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_wf_process wf_instanceorg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_wf_process
    ADD CONSTRAINT wf_instanceorg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_window windowclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_window
    ADD CONSTRAINT windowclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_window windoworg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_window
    ADD CONSTRAINT windoworg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_workflow workflowclient; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_workflow
    ADD CONSTRAINT workflowclient FOREIGN KEY (ad_client_id) REFERENCES ampere.ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ad_workflow workfloworg; Type: FK CONSTRAINT; Schema: ampere; Owner: -
--

ALTER TABLE ONLY ampere.ad_workflow
    ADD CONSTRAINT workfloworg FOREIGN KEY (ad_org_id) REFERENCES ampere.ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

