CREATE OR REPLACE VIEW m_inout_manifestline_v AS 
 SELECT ml.ad_client_id,
    ml.ad_org_id,
    ml.isactive,
    ml.created,
    ml.updated,
    ml.createdby,
    ml.updatedby,
    ml.m_inout_manifestline_id,
    ml.description,
    ml.line,
    ml.seqno,
    ml.m_inout_manifest_id,
    ml.m_inout_id,
    io.nopackages,
    io.c_bpartner_id,
    COALESCE(io.dropship_location_id, io.c_bpartner_location_id) AS c_bpartner_location_id,
    bpl.c_location_id,
    ( SELECT count(DISTINCT m_locator.x) AS count
           FROM m_inoutline,
            m_locator
          WHERE ((m_inoutline.m_locator_id = m_locator.m_locator_id) AND (m_inoutline.m_inout_id = ml.m_inout_id))
          GROUP BY m_inoutline.m_inout_id) AS noofpickslips,
    ( SELECT sum(m_inoutline.movementqty) AS sum
           FROM m_inoutline
          WHERE (m_inoutline.m_inout_id = ml.m_inout_id)) AS totalqty
   FROM ((m_inout_manifestline ml
     JOIN m_inout io ON ((ml.m_inout_id = io.m_inout_id)))
     LEFT JOIN c_bpartner_location bpl ON ((COALESCE(io.dropship_location_id, io.c_bpartner_location_id) = bpl.c_bpartner_location_id)));