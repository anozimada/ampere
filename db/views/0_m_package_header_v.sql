CREATE OR REPLACE VIEW m_package_header_v AS 
 SELECT p.ad_client_id,
    p.ad_org_id,
    p.m_package_id,
    p.m_package_id AS m_package_header_v_id,
    p.isactive,
    p.created,
    p.createdby,
    p.updated,
    p.updatedby,
    p.trackinginfo,
    p.description,
    p.shipdate,
    io.c_bpartner_id,
    io.movementdate,
    bpl.c_location_id,
    io.documentno AS shipno,
    p.documentno AS packageno
   FROM ((m_package p
     JOIN m_inout io ON ((p.m_inout_id = io.m_inout_id)))
     JOIN c_bpartner_location bpl ON ((io.c_bpartner_location_id = bpl.c_bpartner_location_id)));