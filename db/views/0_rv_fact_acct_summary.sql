CREATE OR REPLACE VIEW rv_fact_acct_summary AS 
 SELECT
        CASE
            WHEN (ev.accounttype = ANY (ARRAY['A'::bpchar, 'L'',O'::bpchar])) THEN 'B'::text
            ELSE 'P'::text
        END AS borp,
    ev.value AS accountvalue,
    ev.accounttype,
    (fas.amtacctdr - fas.amtacctcr) AS amtacct,
    fas.ad_client_id,
    fas.ad_orgtrx_id,
    fas.ad_org_id,
    fas.account_id,
    fas.amtacctcr,
    fas.amtacctdr,
    fas.c_acctschema_id,
    fas.c_activity_id,
    fas.c_bpartner_id,
    fas.c_campaign_id,
    fas.c_locfrom_id,
    fas.c_locto_id,
    fas.c_period_id,
    fas.c_projectphase_id,
    fas.c_projecttask_id,
    fas.c_project_id,
    fas.c_salesregion_id,
    fas.c_subacct_id,
    fas.created,
    fas.createdby,
    fas.gl_budget_id,
    fas.isactive,
    fas.m_product_id,
    fas.pa_reportcube_id,
    fas.postingtype,
    fas.qty,
    fas.updated,
    fas.updatedby,
    fas.user1_id,
    fas.user2_id,
    fas.userelement1_id,
    fas.userelement2_id,
    fas.dateacct
   FROM (fact_acct_summary fas
     JOIN c_elementvalue ev ON ((fas.account_id = ev.c_elementvalue_id)));