CREATE OR REPLACE VIEW mrp_run_workcentre_v AS 
 SELECT w.ad_client_id,
    w.ad_org_id,
    w.isactive,
    w.created,
    w.updated,
    w.createdby,
    w.updatedby,
    w.mrp_run_id,
    w.m_workcentre_id
   FROM (mrp_run r
     JOIN t_mrp_run_capacity_summary w ON ((r.mrp_run_id = w.mrp_run_id)))
  GROUP BY w.ad_client_id, w.ad_org_id, w.isactive, w.created, w.updated, w.createdby, w.updatedby, w.mrp_run_id, w.m_workcentre_id;