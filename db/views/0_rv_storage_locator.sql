CREATE OR REPLACE VIEW rv_storage_locator AS 
 SELECT s.ad_client_id,
    s.ad_org_id,
    s.m_product_id,
    p.value,
    p.name,
    p.description,
    p.upc,
    p.sku,
    p.c_uom_id,
    p.m_product_category_id,
    p.classification,
    p.weight,
    p.volume,
    p.versionno,
    p.guaranteedays,
    p.guaranteedaysmin,
    s.m_locator_id,
    l.m_warehouse_id,
    l.x,
    l.y,
    l.z,
    s.datelastinventory,
    sum(s.qtyonhand) AS qtyonhand,
    sum(s.qtyreserved) AS qtyreserved,
    sum((s.qtyonhand - s.qtyreserved)) AS qtyavailable,
    sum(s.qtyordered) AS qtyordered
   FROM ((m_storage s
     JOIN m_locator l ON ((s.m_locator_id = l.m_locator_id)))
     JOIN m_product p ON ((s.m_product_id = p.m_product_id)))
  GROUP BY s.ad_client_id, s.ad_org_id, s.m_product_id, p.value, p.name, p.description, p.upc, p.sku, p.c_uom_id, p.m_product_category_id, p.classification, p.weight, p.volume, p.versionno, p.guaranteedays, p.guaranteedaysmin, s.m_locator_id, l.m_warehouse_id, l.x, l.y, l.z, s.datelastinventory;