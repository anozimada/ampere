CREATE OR REPLACE VIEW c_project_details_vt AS 
 SELECT pl.ad_client_id,
    pl.ad_org_id,
    pl.isactive,
    pl.created,
    pl.createdby,
    pl.updated,
    pl.updatedby,
    l.ad_language,
    pj.c_project_id,
    pl.c_projectline_id,
    pl.line,
    pl.plannedqty,
    pl.plannedprice,
    pl.plannedamt,
    pl.plannedmarginamt,
    pl.committedamt,
    pl.m_product_id,
    COALESCE(p.name, ((pl.description)::character varying)::text) AS name,
        CASE
            WHEN (p.name IS NOT NULL) THEN (pl.description)::character varying
            ELSE NULL::character varying
        END AS description,
    p.documentnote,
    p.upc,
    p.sku,
    p.value AS productvalue,
    pl.m_product_category_id,
    pl.invoicedamt,
    pl.invoicedqty,
    pl.committedqty
   FROM (((c_projectline pl
     JOIN c_project pj ON ((pl.c_project_id = pj.c_project_id)))
     LEFT JOIN m_product p ON ((pl.m_product_id = p.m_product_id)))
     JOIN ad_language l ON ((l.issystemlanguage = 'Y'::bpchar)))
  WHERE (pl.isprinted = 'Y'::bpchar);