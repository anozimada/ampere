CREATE OR REPLACE VIEW m_inout_manifest_v AS 
 SELECT m.ad_client_id,
    m.ad_org_id,
    m.isactive,
    m.created,
    m.updated,
    m.createdby,
    m.updatedby,
    m.m_inout_manifest_id,
    m.documentno,
    m.description,
    m.datedelivered,
    m.datefinish,
    m.processedon,
    m.processed,
    m.processing,
    m.iscomplete,
    m.m_shipper_id,
    COALESCE(oi.logo_id, ci.logo_id) AS logo_id,
    oi.c_location_id,
    oi.phone,
    oi.phone2,
    oi.email,
    oi.c_bankaccount_id,
    oi.taxid
   FROM ((m_inout_manifest m
     LEFT JOIN ad_orginfo oi ON ((m.ad_org_id = oi.ad_org_id)))
     JOIN ad_clientinfo ci ON ((m.ad_client_id = ci.ad_client_id)));