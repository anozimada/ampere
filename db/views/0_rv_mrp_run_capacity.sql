CREATE OR REPLACE VIEW rv_mrp_run_capacity AS 
 SELECT w.ad_client_id,
    w.ad_org_id,
    w.isactive,
    w.created,
    w.updated,
    w.createdby,
    w.updatedby,
    s2.mrp_run_id,
    s2.m_workcentre_id,
    s2.datestart,
    w.capacityintimeuom,
    s2.mpo,
    s2.mop,
    sum(w.capacityintimeuom) OVER (PARTITION BY s2.m_workcentre_id ORDER BY s2.datestart) AS totalcapacitycapacity,
    sum(((w.capacityintimeuom - s2.mpo) - s2.mop)) OVER (PARTITION BY s2.m_workcentre_id ORDER BY s2.datestart) AS cumulatedcapacity
   FROM (( SELECT ss.mrp_run_id,
            ss.m_workcentre_id,
            ss.datestart,
            sum(
                CASE
                    WHEN (ss.docbasetype = 'MPO'::bpchar) THEN ss.qty
                    ELSE (0)::numeric
                END) AS mpo,
            sum(
                CASE
                    WHEN (ss.docbasetype = 'MOP'::bpchar) THEN ss.qty
                    ELSE (0)::numeric
                END) AS mop
           FROM ( SELECT rl.mrp_run_id,
                    p.m_workcentre_id,
                    unnest(ARRAY['week1'::text, 'week2'::text, 'week3'::text, 'week4'::text, 'week5'::text, 'week6'::text, 'week7'::text, 'week8'::text, 'week9'::text, 'week10'::text, 'week11'::text, 'week12'::text, 'week13'::text, 'week14'::text, 'week15'::text, 'week16'::text, 'week17'::text, 'week18'::text, 'week19'::text, 'week20'::text, 'week21'::text, 'week22'::text, 'week23'::text, 'week24'::text]) AS week,
                    adddays((rl.datestart)::timestamp with time zone, ((((replace(unnest(ARRAY['week1'::text, 'week2'::text, 'week3'::text, 'week4'::text, 'week5'::text, 'week6'::text, 'week7'::text, 'week8'::text, 'week9'::text, 'week10'::text, 'week11'::text, 'week12'::text, 'week13'::text, 'week14'::text, 'week15'::text, 'week16'::text, 'week17'::text, 'week18'::text, 'week19'::text, 'week20'::text, 'week21'::text, 'week22'::text, 'week23'::text, 'week24'::text]), 'week'::text, ''::text))::integer - 1) * 7))::numeric) AS datestart,
                    unnest(ARRAY[rl.week1, rl.week2, rl.week3, rl.week4, rl.week5, rl.week6, rl.week7, rl.week8, rl.week9, rl.week10, rl.week11, rl.week12, rl.week13, rl.week14, rl.week15, rl.week16, rl.week17, rl.week18, rl.week19, rl.week20, rl.week21, rl.week22, rl.week23, rl.week24]) AS qty,
                    ( SELECT dt.docbasetype
                           FROM (m_production pp
                             JOIN c_doctype dt ON ((pp.c_doctype_id = dt.c_doctype_id)))
                          WHERE (rl.m_production_id = pp.m_production_id)) AS docbasetype
                   FROM (mrp_runline rl
                     JOIN m_product p ON ((rl.m_product_id = p.m_product_id)))
                  WHERE ((p.m_workcentre_id IS NOT NULL) AND (rl.recordtype = 'Demand'::text))) ss
          GROUP BY ss.mrp_run_id, ss.m_workcentre_id, ss.datestart
          ORDER BY ss.datestart) s2
     JOIN m_workcentre w ON ((s2.m_workcentre_id = w.m_workcentre_id)));