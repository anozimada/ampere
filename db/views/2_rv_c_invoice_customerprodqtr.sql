CREATE OR REPLACE VIEW rv_c_invoice_customerprodqtr AS 
 SELECT il.ad_client_id,
    il.ad_org_id,
    il.c_bpartner_id,
    il.m_product_category_id,
    firstof((il.dateinvoiced)::timestamp with time zone, 'Q'::character varying) AS dateinvoiced,
    sum(il.linenetamt) AS linenetamt,
    sum(il.linelistamt) AS linelistamt,
    sum(il.linelimitamt) AS linelimitamt,
    sum(il.linediscountamt) AS linediscountamt,
        CASE
            WHEN (sum(il.linelistamt) = (0)::numeric) THEN (0)::numeric
            ELSE round((((sum(il.linelistamt) - sum(il.linenetamt)) / sum(il.linelistamt)) * (100)::numeric), 2)
        END AS linediscount,
    sum(il.lineoverlimitamt) AS lineoverlimitamt,
        CASE
            WHEN (sum(il.linenetamt) = (0)::numeric) THEN (0)::numeric
            ELSE ((100)::numeric - round((((sum(il.linenetamt) - sum(il.lineoverlimitamt)) / sum(il.linenetamt)) * (100)::numeric), 2))
        END AS lineoverlimit,
    sum(il.qtyinvoiced) AS qtyinvoiced,
    il.issotrx
   FROM rv_c_invoiceline il
  GROUP BY il.ad_client_id, il.ad_org_id, il.c_bpartner_id, il.m_product_category_id, (firstof((il.dateinvoiced)::timestamp with time zone, 'Q'::character varying)), il.issotrx;