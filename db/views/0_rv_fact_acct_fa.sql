CREATE OR REPLACE VIEW rv_fact_acct_fa AS 
 SELECT f.fact_acct_id AS rv_fact_acct_fa_id,
    f.ad_client_id,
    f.ad_org_id,
    f.isactive,
    f.created,
    f.createdby,
    f.updated,
    f.updatedby,
    f.fact_acct_id,
    f.c_acctschema_id,
    f.account_id,
    f.datetrx,
    f.dateacct,
    f.c_period_id,
    f.ad_table_id,
    f.record_id,
    f.line_id,
    f.gl_category_id,
    f.gl_budget_id,
    f.c_tax_id,
    f.m_locator_id,
    f.postingtype,
    f.c_currency_id,
    f.amtsourcedr,
    f.amtsourcecr,
    (f.amtsourcedr - f.amtsourcecr) AS amtsource,
    f.amtacctdr,
    f.amtacctcr,
    (f.amtacctdr - f.amtacctcr) AS amtacct,
        CASE
            WHEN ((c.faaccounttype)::text = '1'::text) THEN (f.amtacctdr - f.amtacctcr)
            ELSE NULL::numeric
        END AS amtasset,
        CASE
            WHEN ((c.faaccounttype)::text = '2'::text) THEN (f.amtacctdr - f.amtacctcr)
            ELSE NULL::numeric
        END AS amtdepreciation,
        CASE
            WHEN ((f.amtsourcedr - f.amtsourcecr) = (0)::numeric) THEN (0)::numeric
            ELSE ((f.amtacctdr - f.amtacctcr) / (f.amtsourcedr - f.amtsourcecr))
        END AS rate,
    f.c_uom_id,
    f.qty,
    f.m_product_id,
    f.c_bpartner_id,
    f.ad_orgtrx_id,
    f.c_locfrom_id,
    f.c_locto_id,
    f.c_salesregion_id,
    f.c_project_id,
    f.c_campaign_id,
    f.c_activity_id,
    f.user1_id,
    f.user2_id,
    f.a_asset_id,
    f.description,
    o.value AS orgvalue,
    o.name AS orgname,
    ev.value AS accountvalue,
    ev.name,
    ev.accounttype,
    bp.value AS bpartnervalue,
    bp.name AS bpname,
    bp.c_bp_group_id,
    p.value AS productvalue,
    p.name AS productname,
    p.upc,
    p.m_product_category_id,
    a.a_asset_group_id,
    c.faaccounttype,
    aj.entrytype
   FROM (((((((fact_acct f
     JOIN ad_org o ON ((f.ad_org_id = o.ad_org_id)))
     JOIN c_elementvalue ev ON ((f.account_id = ev.c_elementvalue_id)))
     LEFT JOIN c_bpartner bp ON ((f.c_bpartner_id = bp.c_bpartner_id)))
     LEFT JOIN m_product p ON ((f.m_product_id = p.m_product_id)))
     LEFT JOIN a_asset a ON ((f.a_asset_id = a.a_asset_id)))
     JOIN c_elementvalue_fa c ON ((f.account_id = c.c_elementvalue_id)))
     LEFT JOIN gaas_assetjournal aj ON (((aj.gaas_assetjournal_id = f.record_id) AND (f.ad_table_id = ( SELECT ad_table.ad_table_id
           FROM ad_table
          WHERE (ad_table.tablename = 'GAAS_AssetJournal'::text))))));