CREATE OR REPLACE VIEW rv_minimrp_production AS 
 SELECT mrl.ad_client_id,
    mrl.ad_org_id,
    mrl.mrp_run_id,
    p.m_product_id,
    pd.m_production_id,
    pd.productionqty,
    pd.datepromised,
    pd.movementdate,
    pd.isactive,
    trunc(c.currentcostprice, 6) AS currentcostprice,
    p.m_product_category_id
   FROM (((mrp_runline mrl
     JOIN m_production pd ON ((pd.m_production_id = mrl.m_production_id)))
     JOIN m_product p ON ((p.m_product_id = pd.m_product_id)))
     LEFT JOIN m_cost c ON (((c.ad_client_id = mrl.ad_client_id) AND (c.ad_org_id = (0)::numeric) AND (c.m_product_id = p.m_product_id) AND (c.m_attributesetinstance_id = (0)::numeric) AND (c.c_acctschema_id = (1000007)::numeric) AND (c.m_costtype_id = (1000007)::numeric) AND (c.m_costelement_id = (1000007)::numeric))))
  WHERE (mrl.recordtype = 'Planned Production'::text)
  ORDER BY pd.m_production_id;