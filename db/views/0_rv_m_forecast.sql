CREATE OR REPLACE VIEW rv_m_forecast AS 
 SELECT f.ad_client_id,
    fl.ad_org_id,
    f.m_forecast_id,
    f.name,
    f.c_calendar_id,
    f.c_year_id,
    fl.c_period_id,
    fl.datepromised,
    fl.m_product_id,
    fl.salesrep_id,
    p.c_uom_id,
    fl.qty,
    fl.qtycalculated,
    ( SELECT pp.pricelist
           FROM (m_pricelist_version plv
             JOIN m_productprice pp ON ((pp.m_pricelist_version_id = plv.m_pricelist_version_id)))
          WHERE ((pp.m_product_id = fl.m_product_id) AND (plv.m_pricelist_id = pl.m_pricelist_id))) AS pricelist,
    ( SELECT pp.pricestd
           FROM (m_pricelist_version plv
             JOIN m_productprice pp ON ((pp.m_pricelist_version_id = plv.m_pricelist_version_id)))
          WHERE ((pp.m_product_id = fl.m_product_id) AND (plv.m_pricelist_id = pl.m_pricelist_id))) AS pricestd,
    ( SELECT pp.pricelimit
           FROM (m_pricelist_version plv
             JOIN m_productprice pp ON ((pp.m_pricelist_version_id = plv.m_pricelist_version_id)))
          WHERE ((pp.m_product_id = fl.m_product_id) AND (plv.m_pricelist_id = pl.m_pricelist_id))) AS pricelimit,
    (( SELECT pp.pricestd
           FROM (m_pricelist_version plv
             JOIN m_productprice pp ON ((pp.m_pricelist_version_id = plv.m_pricelist_version_id)))
          WHERE ((pp.m_product_id = fl.m_product_id) AND (plv.m_pricelist_id = pl.m_pricelist_id))) * fl.qty) AS totalamt,
    p.m_product_category_id,
    p.classification,
    p.group1,
    p.group2
   FROM (((m_forecast f
     JOIN m_forecastline fl ON ((f.m_forecast_id = fl.m_forecast_id)))
     JOIN m_product p ON ((p.m_product_id = fl.m_product_id)))
     JOIN m_pricelist pl ON ((pl.m_pricelist_id = f.m_pricelist_id)));