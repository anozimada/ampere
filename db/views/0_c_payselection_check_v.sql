CREATE OR REPLACE VIEW c_payselection_check_v AS 
 SELECT psc.ad_client_id,
    psc.ad_org_id,
    psc.isactive,
    psc.created,
    psc.createdby,
    psc.updated,
    psc.updatedby,
    'en_US'::character varying AS ad_language,
    psc.c_payselection_id,
    psc.c_payselectioncheck_id,
    oi.c_location_id AS org_location_id,
    oi.taxid,
    p.c_doctype_id,
    bp.c_bpartner_id,
    bp.value AS bpvalue,
    bp.taxid AS bptaxid,
    bp.naics,
    bp.duns,
    bpg.greeting AS bpgreeting,
    bp.name,
    bp.name2,
    bpartnerremitlocation(bp.c_bpartner_id) AS c_location_id,
    bp.referenceno,
    bp.poreference,
    ps.paydate,
    psc.payamt,
    psc.payamt AS amtinwords,
    psc.qty,
    psc.paymentrule,
    psc.documentno,
    COALESCE(oi.logo_id, ci.logo_id) AS logo_id,
    dt.printname AS documenttype,
    dt.documentnote AS documenttypenote,
    p.description,
    p.c_payment_id,
    p.documentno AS payment_documentno,
    pb.c_paymentbatch_id,
    pb.name AS paymentbatch_name,
    ( SELECT (((b.a_name || ' Bank AcctNo - '::text) || b.accountno))::character varying(200) AS "varchar"
           FROM c_bp_bankaccount b
          WHERE (b.c_bp_bankaccount_id IN ( SELECT max(bb.c_bp_bankaccount_id) AS max
                   FROM c_bp_bankaccount bb
                  WHERE ((bb.c_bpartner_id = bp.c_bpartner_id) AND ((bb.bpbankacctuse = 'B'::bpchar) OR ((bb.bpbankacctuse)::text =
                        CASE
                            WHEN (dt.issotrx = 'Y'::bpchar) THEN 'D'::text
                            ELSE 'T'::text
                        END)))))) AS accountno
   FROM ((((((((c_payselectioncheck psc
     JOIN c_payselection ps ON ((psc.c_payselection_id = ps.c_payselection_id)))
     LEFT JOIN c_payment p ON ((psc.c_payment_id = p.c_payment_id)))
     LEFT JOIN c_doctype dt ON ((p.c_doctype_id = dt.c_doctype_id)))
     JOIN c_bpartner bp ON ((psc.c_bpartner_id = bp.c_bpartner_id)))
     LEFT JOIN c_greeting bpg ON ((bp.c_greeting_id = bpg.c_greeting_id)))
     JOIN ad_orginfo oi ON ((psc.ad_org_id = oi.ad_org_id)))
     LEFT JOIN c_paymentbatch pb ON ((p.c_paymentbatch_id = pb.c_paymentbatch_id)))
     JOIN ad_clientinfo ci ON ((psc.ad_client_id = ci.ad_client_id)));