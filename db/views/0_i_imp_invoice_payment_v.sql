CREATE OR REPLACE VIEW i_imp_invoice_payment_v AS 
 SELECT i_invoice.description,
    i_invoice.c_bpartner_id,
    i_invoice.c_charge_id,
    i_invoice.m_product_id,
    i_invoice.dateacct,
    i_invoice.i_errormsg,
        CASE
            WHEN (i_invoice.i_isimported <> 'E'::bpchar) THEN
            CASE
                WHEN (i_invoice.amtacctcr > (0)::numeric) THEN i_invoice.amtacctcr
                WHEN (i_invoice.amtacctdr > (0)::numeric) THEN i_invoice.amtacctdr
                ELSE NULL::numeric
            END
            ELSE NULL::numeric
        END AS payamt,
        CASE
            WHEN (((i_invoice.ischargeonpayment = 'N'::bpchar) AND (i_invoice.c_charge_id > (0)::numeric)) OR (i_invoice.m_product_id > (0)::numeric)) THEN
            CASE
                WHEN (i_invoice.amtacctcr > (0)::numeric) THEN i_invoice.amtacctcr
                WHEN (i_invoice.amtacctdr > (0)::numeric) THEN i_invoice.amtacctdr
                ELSE NULL::numeric
            END
            ELSE NULL::numeric
        END AS invoiceamt,
    i_invoice.priceactual,
    i_invoice.taxamt,
    i_invoice.issotrx,
    i_invoice.ad_client_id,
    i_invoice.ad_org_id
   FROM i_invoice
  WHERE (i_invoice.i_isimported <> 'Y'::bpchar);