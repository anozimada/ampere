#!/bin/sh

DB_NAME=${1:-ampere}
DB_USER=${2:-ampere}
DB_PASS=${3:-ampere}
DB_HOST=${4:-ampere-postgres}
DB_PORT=${5:-5432}

execute_sql () {
    echo "Applying script: " $1
    psql -U $DB_USER -h $DB_HOST -p $DB_PORT -d $DB_NAME -f "$1"
}

export PGPASSWORD=$DB_PASS
 
dropdb -U $DB_USER -h $DB_HOST -p $DB_PORT $DB_NAME

createdb -U $DB_USER -h $DB_HOST -p $DB_PORT $DB_NAME

execute_sql pre-data.sql

psql -U $DB_USER -h $DB_HOST -p $DB_PORT -d $DB_NAME -c "SET search_path TO 'ampere';" 

find ./data -name "*.sql" -type f | sort -n | \
while read f
do
    execute_sql "$f"
done

execute_sql post-data.sql 

export PGPASSWORD=
