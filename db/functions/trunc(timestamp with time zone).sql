CREATE OR REPLACE FUNCTION ampere.trunc(datetime timestamp with time zone)
 RETURNS timestamp with time zone
 LANGUAGE plpgsql
AS $function$
BEGIN
	RETURN CAST(datetime AS DATE);
END;
$function$
