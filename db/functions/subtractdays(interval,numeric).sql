CREATE OR REPLACE FUNCTION ampere.subtractdays(inter interval, days numeric)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
BEGIN
RETURN ( EXTRACT( EPOCH FROM ( inter ) ) / 86400 ) - days;
END;
$function$
