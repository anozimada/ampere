CREATE OR REPLACE FUNCTION ampere.fact_documentno(p_fact_acct_id numeric)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
DECLARE
	v_DocumentNo VARCHAR;
BEGIN
	-- If NO id return empty string
	IF p_fact_acct_id <= 0 THEN
		RETURN '';
	END IF;
	SELECT 
	CASE
		WHEN fa.ad_table_id = 318 THEN (SELECT c_invoice.documentno FROM c_invoice  WHERE c_invoice.c_invoice_id = fa.record_id)

			
	END INTO v_DocumentNo
	FROM fact_acct fa
	WHERE fa.fact_acct_id = p_fact_acct_id;
	RETURN v_DocumentNo;
END;	
$function$
