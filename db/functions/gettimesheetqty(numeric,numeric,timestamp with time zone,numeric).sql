CREATE OR REPLACE FUNCTION ampere.gettimesheetqty(taskid numeric, resourseid numeric, startdate timestamp with time zone, noofdays numeric)
 RETURNS numeric[]
 LANGUAGE plpgsql
AS $function$

DECLARE 

p_arr_int numeric[];
wdate timestamp without time zone;
qty numeric;

qtytotal numeric;
lastdate timestamp without time zone;
firstdate timestamp without time zone;

BEGIN
  firstdate=startdate;

  qty :=0;
  
  FOR i IN 0 .. noofdays-1 LOOP
    wdate = adddays(startdate, i);
    
	select COALESCE(qtyentered,0) into qty from S_TimeSheetEntry  where C_ProjectTask_ID=taskid   and s_Resource_ID=resourseid and dateacct=wdate;
		p_arr_int[i] :=qty;    
  
	
		
  END LOOP;


  lastdate=adddays(firstdate, noofdays-1);

      
	select COALESCE(sum(qtyentered),0) into qtytotal from S_TimeSheetEntry  where C_ProjectTask_ID=taskid  and s_Resource_ID=resourseid and 
		(dateacct between firstdate and lastdate) ;
		p_arr_int[noofdays] :=qtytotal;    
 
	
   
  return p_arr_int;
END;

$function$
