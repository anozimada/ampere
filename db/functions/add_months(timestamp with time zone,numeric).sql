CREATE OR REPLACE FUNCTION ampere.add_months(datetime timestamp with time zone, months numeric)
 RETURNS date
 LANGUAGE plpgsql
AS $function$
declare duration varchar;
BEGIN
	if datetime is null or months is null then
		return null;
	end if;
	duration = months || ' month';	 
	return cast(datetime + cast(duration as interval) as date);
END;
$function$
