CREATE OR REPLACE FUNCTION ampere.adddays(datetime timestamp with time zone, days numeric)
 RETURNS date
 LANGUAGE plpgsql
AS $function$

declare duration varchar;

BEGIN

	if datetime is null or days is null then

		return null;

	end if;

	duration = days || ' day';	 

	return cast(date_trunc('day',datetime) + cast(duration as interval) as date);

END;

$function$
