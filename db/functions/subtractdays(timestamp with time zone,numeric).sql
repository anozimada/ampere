CREATE OR REPLACE FUNCTION ampere.subtractdays(day timestamp with time zone, days numeric)
 RETURNS date
 LANGUAGE plpgsql
AS $function$

BEGIN

    RETURN addDays(day,(days * -1));

END;

$function$
