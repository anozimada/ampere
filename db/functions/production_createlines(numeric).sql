CREATE OR REPLACE FUNCTION ampere.production_createlines(p_m_production_id numeric)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$

DECLARE 
	v_totalrecords integer;
	v_product_id integer;
BEGIN	
	SELECT m_product_id
	INTO v_product_id
	FROM m_production
	WHERE m_production_id = p_m_production_id;
	
	INSERT INTO m_productionline
		(m_productionline_id
		, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby
		, m_productionplan_id, description, m_attributesetinstance_id
		, LINE
		, m_product_id, movementqty
		, m_locator_id
		, processed, m_production_id, plannedqty
		, qtyused, isendproduct, qtyreserved, pickedqty)
	SELECT nextid(249, 'N') as m_productionline_id
		, p.ad_client_id, p.ad_org_id, p.isactive, p.created, p.createdby, p.updated, p.updatedby
		, NULL AS m_productionplan_id, NULL AS description, 0 AS m_attributesetinstance_id
		, (row_number() over () * 10) + 10 as lineno
		, b.m_productbom_id, b.bomqty*p.productionqty*-1 as movementqty
		, COALESCE (b.m_locator_id, p.m_locator_id) as m_locator_id
		, 'N' as processed, p.m_production_id, b.bomqty*p.productionqty as plannedqty
		, b.bomqty*p.productionqty as qtyused
		, 'N' as isendproduct, b.bomqty*p.productionqty as qtyreserved, 0 as pickedqty
	FROM m_production p, get_bomlines(v_product_id) b 
	WHERE p.m_production_id = p_m_production_id
	AND b.isexplode = 'N';

	select count(*) 
	into v_totalrecords
	from m_productionline where m_production_id = p_m_production_id;
	
	return v_totalrecords;
END;
$function$
