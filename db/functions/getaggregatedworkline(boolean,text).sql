CREATE OR REPLACE FUNCTION ampere.getaggregatedworkline(isaggrbyworkcentre boolean, columnname text)
 RETURNS TABLE(ad_client_id numeric, ad_org_id numeric, mrp_run_id numeric, m_workcentre_id numeric, m_product_id numeric, week1 numeric, week2 numeric, week3 numeric, week4 numeric, week5 numeric, week6 numeric, week7 numeric, week8 numeric, week9 numeric, week10 numeric, week11 numeric, week12 numeric, week13 numeric, week14 numeric, week15 numeric, week16 numeric, week17 numeric, week18 numeric, week19 numeric, week20 numeric, week21 numeric, week22 numeric, week23 numeric, week24 numeric)
 LANGUAGE plpgsql
AS $function$

/**********************************************************************************
	@Author Sachin Bhimani
	@Since 2017 April 21

	Tricking rows data into a column

	GetAggregatedWorkLine ( boolean, text);
	- Parameter 1: Grouping data, if True then aggregate by WorkCentre otherwise Product wise 		
	- Parameter 2: ColumnName with aggregation funtion [Column name based on T_MRP_RUN_Capacity_Summary]

	Example:	
	SELECT * FROM GetAggregatedWorkLine (true,'SuggestedQty');
	
***********************************************************************************/
	
DECLARE
	 v_WhereClause text;
BEGIN
	IF IsAggrByWorkCentre
		THEN	v_WhereClause = ' M_Product_ID = 0 ';
		ELSE	v_WhereClause = ' M_Product_ID > 0 ';
	END IF;

	RETURN QUERY EXECUTE 
	'SELECT AD_Client_ID, AD_Org_ID, MRP_Run_ID, M_WorkCentre_ID, M_Product_ID, 
		SUM(CASE WEEK WHEN ''week1''  THEN '|| $2 ||' ELSE 0 END) AS Week1,
		SUM(CASE WEEK WHEN ''week2''  THEN '|| $2 ||' ELSE 0 END) AS Week2,
		SUM(CASE WEEK WHEN ''week3''  THEN '|| $2 ||' ELSE 0 END) AS Week3,
		SUM(CASE WEEK WHEN ''week4''  THEN '|| $2 ||' ELSE 0 END) AS Week4,
		SUM(CASE WEEK WHEN ''week5''  THEN '|| $2 ||' ELSE 0 END) AS Week5,
		SUM(CASE WEEK WHEN ''week6''  THEN '|| $2 ||' ELSE 0 END) AS Week6,
		SUM(CASE WEEK WHEN ''week7''  THEN '|| $2 ||' ELSE 0 END) AS Week7,
		SUM(CASE WEEK WHEN ''week8''  THEN '|| $2 ||' ELSE 0 END) AS Week8,
		SUM(CASE WEEK WHEN ''week9''  THEN '|| $2 ||' ELSE 0 END) AS Week9,
		SUM(CASE WEEK WHEN ''week10'' THEN '|| $2 ||' ELSE 0 END) AS Week10,
		SUM(CASE WEEK WHEN ''week11'' THEN '|| $2 ||' ELSE 0 END) AS Week11,
		SUM(CASE WEEK WHEN ''week12'' THEN '|| $2 ||' ELSE 0 END) AS Week12,
		SUM(CASE WEEK WHEN ''week13'' THEN '|| $2 ||' ELSE 0 END) AS Week13,
		SUM(CASE WEEK WHEN ''week14'' THEN '|| $2 ||' ELSE 0 END) AS Week14,
		SUM(CASE WEEK WHEN ''week15'' THEN '|| $2 ||' ELSE 0 END) AS Week15,
		SUM(CASE WEEK WHEN ''week16'' THEN '|| $2 ||' ELSE 0 END) AS Week16,
		SUM(CASE WEEK WHEN ''week17'' THEN '|| $2 ||' ELSE 0 END) AS Week17,
		SUM(CASE WEEK WHEN ''week18'' THEN '|| $2 ||' ELSE 0 END) AS Week18,
		SUM(CASE WEEK WHEN ''week19'' THEN '|| $2 ||' ELSE 0 END) AS Week19,
		SUM(CASE WEEK WHEN ''week20'' THEN '|| $2 ||' ELSE 0 END) AS Week20,
		SUM(CASE WEEK WHEN ''week21'' THEN '|| $2 ||' ELSE 0 END) AS Week21,
		SUM(CASE WEEK WHEN ''week22'' THEN '|| $2 ||' ELSE 0 END) AS Week22,
		SUM(CASE WEEK WHEN ''week23'' THEN '|| $2 ||' ELSE 0 END) AS Week23,
		SUM(CASE WEEK WHEN ''week24'' THEN '|| $2 ||' ELSE 0 END) AS Week24
	FROM T_MRP_RUN_Capacity_Summary
	WHERE '|| v_WhereClause ||'
	GROUP BY AD_Client_ID, AD_Org_ID, MRP_Run_ID, M_WorkCentre_ID, M_Product_ID';

END;
 $function$
