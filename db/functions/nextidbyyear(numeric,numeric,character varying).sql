CREATE OR REPLACE FUNCTION ampere.nextidbyyear(p_ad_sequence_id numeric, p_incrementno numeric, p_calendaryear character varying)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
DECLARE
    o_NextID numeric;
BEGIN
   SELECT CurrentNext
		INTO o_NextID
	FROM ad_sequence_no
	WHERE AD_Sequence_ID=p_AD_Sequence_ID 
	AND CalendarYear = p_CalendarYear 
	FOR UPDATE OF ad_sequence_no;
	--
	UPDATE ad_sequence_no
	  SET CurrentNext = CurrentNext + p_IncrementNo
	WHERE AD_Sequence_ID=p_AD_Sequence_ID
	AND CalendarYear = p_CalendarYear;
	RETURN o_NextID;
END
$function$
