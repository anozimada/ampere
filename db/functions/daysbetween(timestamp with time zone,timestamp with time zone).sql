CREATE OR REPLACE FUNCTION ampere.daysbetween(p_date1 timestamp with time zone, p_date2 timestamp with time zone)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$

BEGIN

	RETURN CAST(p_date1 AS DATE) - CAST(p_date2 as DATE);

END;

$function$
