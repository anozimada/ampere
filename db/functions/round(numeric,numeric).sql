CREATE OR REPLACE FUNCTION ampere.round(numeric, numeric)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
 BEGIN
	RETURN ROUND($1, cast($2 as integer));
 END;
$function$
