CREATE OR REPLACE FUNCTION ampere.bomqtyavailableasi(product_id numeric, attributesetinstance_id numeric, warehouse_id numeric, locator_id numeric)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
BEGIN
	RETURN bomQtyOnHandASI(Product_ID, AttributeSetInstance_ID, Warehouse_ID, Locator_ID) -
	       bomQtyReservedASI(Product_ID, AttributeSetInstance_ID, Warehouse_ID, Locator_ID);
END;
$function$
