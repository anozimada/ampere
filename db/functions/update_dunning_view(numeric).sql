CREATE OR REPLACE FUNCTION ampere.update_dunning_view(p_dunningrunentry_id numeric)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
DECLARE
	dunningrunentry_id NUMERIC;
	running_invoice	NUMERIC;
	running_payment	NUMERIC;
	running_allocation NUMERIC;
	old_docno	varchar(100);
	r	type_dunningline;
BEGIN

  DELETE FROM c_dunning_line_mv WHERE c_dunningrunentry_id=p_dunningrunentry_id;

  old_docno :='';
  running_invoice :=0;
  running_payment :=0;
  FOR R IN SELECT a.c_dunningrunentry_id, a.datetrx, a.documentno, a.amt
	, a.paydate, a.allocatedamt, 0 AS balance
	, a.ad_client_id, a.ad_org_id, a.isactive, a.created, a.createdby, a.updated, a.updatedby
	, a.totalamt
   FROM c_dunning_line_v a
   WHERE a.c_dunningrunentry_id=p_dunningrunentry_id
   order by a.documentno, a.paydate, a.datetrx, a.updated
	loop
	  if (old_docno != R.documentno) then
	    old_docno := R.documentno;
	    running_invoice := running_invoice + r.amt;
	    running_allocation := 0;
	  end if; 
	  if (r.amt <> r.totalamt) then
	    r.allocatedamt = r.amt - r.totalamt - running_allocation;
	    running_allocation := running_allocation + r.allocatedamt;
	    if (r.paydate is null) then
	      /* credit memo*/
		select i2.dateinvoiced
		from c_invoice oi
		left join c_allocationline al 
			on oi.c_invoice_id=al.c_invoice_id
		left join c_allocationline al2 
			on al.c_allocationhdr_id = al2.c_allocationhdr_id
		left join c_invoice i2 
			on al.c_invoice_id=i2.c_invoice_id
		where oi.documentno=r.documentno 
			and oi.c_invoice_id<> al2.c_invoice_id
		into r.paydate;
	    end if;
	  end if; 		
	  running_payment := running_payment + coalesce(r.allocatedamt, 0);
	  r.balance := running_invoice - running_payment;

	  INSERT INTO c_dunning_line_mv
		(c_dunningrunentry_id, datetrx, documentno, amt, paydate, allocatedamt,
		balance, ad_client_id, ad_org_id, isactive, created, createdby,
		updated, updatedby)
	  VALUES
		(r.c_dunningrunentry_id, r.datetrx, r.documentno, r.amt, r.paydate, r.allocatedamt,
		r.balance, r.ad_client_id, r.ad_org_id, r.isactive, r.created, r.createdby,
		r.updated, r.updatedby);

 
	  --return next r;
	end loop;	
  
END;

$function$
