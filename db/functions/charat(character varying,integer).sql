CREATE OR REPLACE FUNCTION ampere.charat(character varying, integer)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
 BEGIN
 RETURN SUBSTR($1, $2, 1);
 END;
$function$
