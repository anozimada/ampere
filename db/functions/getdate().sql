CREATE OR REPLACE FUNCTION ampere.getdate()
 RETURNS timestamp with time zone
 LANGUAGE plpgsql
AS $function$

BEGIN

    RETURN now();

END;

$function$
