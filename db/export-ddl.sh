#!/bin/sh

DB_CONTAINER=${1:-ampere-postgres-1}
DB_USER=${2:-ampere}
DB_NAME=${3:-ampere}

docker exec -i $DB_CONTAINER pg_dump --no-owner --no-privileges --section=pre-data -U $DB_USER $DB_NAME > pre-data.sql

docker exec -i $DB_CONTAINER pg_dump --no-owner --no-privileges --section=post-data -U $DB_USER $DB_NAME > post-data.sql
