copy "c_task" ("c_task_id", "name", "ad_client_id", "ad_org_id", "c_phase_id", "created", "createdby", "description", "help", "isactive", "m_product_id", "seqno", "standardqty", "updated", "updatedby") from STDIN;
100	Documentation Critical Processes	11	11	100	2003-08-09 22:34:46	100	\N	\N	Y	\N	10	1	2003-08-09 22:34:46	100
101	Documentation Decision Support Needs	11	11	100	2003-08-09 22:35:16	100	\N	\N	Y	\N	20	1	2003-08-09 22:35:16	100
102	Installation	11	11	101	2003-08-09 22:36:13	100	Installation of OS, Oracle and Adempiere	\N	Y	\N	10	1	2003-08-09 22:36:13	100
103	Setup of Main Entities	11	11	101	2003-08-09 22:37:12	100	Setup of Organization, Warehouses, Example Products, Business Partners	\N	Y	\N	20	1	2003-08-09 22:37:12	100
104	Initial CRP to Champions	11	11	101	2003-08-09 22:38:17	100	Get Feedback from Champions	\N	Y	\N	30	1	2003-08-18 13:36:50	100
105	Enhancing CRP Setup	11	11	101	2003-08-09 22:39:30	100	Completing Setup based on Champion Feedback	\N	Y	\N	40	1	2003-08-18 13:36:55	100
106	CRP with Users	11	11	101	2003-08-09 22:40:04	100	Get initial Feedback from future users of System	\N	Y	\N	50	1	2003-08-18 13:37:01	100
107	Environment Analysis	11	11	102	2003-08-18 13:40:08	100	\N	\N	Y	\N	10	1	2003-08-18 13:40:08	100
108	Requirements Gathering	11	11	102	2003-08-18 13:40:25	100	\N	\N	Y	\N	20	1	2003-08-18 13:40:25	100
\.
