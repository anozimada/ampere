copy "c_paymentterm" ("c_paymentterm_id", "name", "ad_client_id", "ad_org_id", "afterdelivery", "created", "createdby", "description", "discount", "discount2", "discountdays", "discountdays2", "documentnote", "fixmonthcutoff", "fixmonthday", "fixmonthoffset", "gracedays", "isactive", "isdefault", "isduefixed", "isnextbusinessday", "isvalid", "netday", "netdays", "paymenttermusage", "processing", "updated", "updatedby", "value") from STDIN;
100	30 Net	11	0	N	1999-12-15 08:25:17	100	\N	0	0	0	0	\N	0	0	0	0	N	N	N	Y	N	\N	30	B	\N	2003-02-22 02:20:37	0	30 Days Net
105	Immediate	11	0	N	2001-03-27 15:44:25	0	\N	0	0	0	0	\N	\N	\N	\N	0	Y	Y	N	Y	N	\N	0	B	\N	2003-02-22 02:20:37	0	Immediate
106	2%10 Net 30	11	11	N	2002-02-21 19:46:47	100	2% discount if paid in 10 days, net due in 30 days	2	0	10	0	Payment Term Note: Long Term Contracts are eligible for payment discounts.	0	0	0	0	Y	N	N	N	N	\N	30	B	\N	2003-02-22 02:20:37	0	2%10 Net 30
107	30 Days Net	11	11	N	2002-06-30 09:54:55	100	\N	0	0	0	0	\N	0	0	0	0	Y	N	N	N	N	\N	30	B	\N	2003-02-22 02:20:37	0	30 Net
108	50% Immediate - 50% in 30 days	11	11	N	2003-12-26 12:26:10	100	\N	0	0	0	0	\N	0	0	0	0	Y	N	N	N	Y	\N	0	B	N	2003-12-26 12:27:23	100	50:50
\.
