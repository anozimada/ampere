copy "a_depreciation" ("a_depreciation_id", "ad_client_id", "ad_org_id", "created", "createdby", "depreciationtype", "description", "isactive", "name", "processed", "text", "updated", "updatedby") from STDIN;
50000	11	11	2008-05-30 17:05:45	100	DB200	Declining Balance 200% (DB) Depreciation	Y	200% Declining Balance	N	Test	2008-05-30 17:05:45	100
50001	11	11	2008-05-30 17:05:46	100	DB150	Declining Balance 150% (DB) Depreciation	Y	150% Declining Balance	N	Test	2008-05-30 17:05:46	100
50002	11	11	2008-05-30 17:05:46	100	SYD	Sum of Year Digits Depreciation	Y	Sum of Years Digit	N	Test	2008-05-30 17:05:46	100
50003	11	11	2008-05-30 17:05:47	100	SL	Straight Line Depreciation	Y	Straight Line	N	Test	2008-05-30 17:05:47	100
50004	11	11	2008-05-30 17:05:47	100	UOP	Units of Production Depreciation	Y	Units of Production	N	Test	2008-05-30 17:05:47	100
50005	11	11	2008-05-30 17:05:48	100	VAR	Variable Accelerated	Y	Variable Accelerated	N	Test	2008-05-30 17:05:48	100
50006	11	11	2008-05-30 17:05:48	100	MAN	Manual Depreciation	Y	Manual	N	Test	2008-05-30 17:05:48	100
50007	11	11	2008-05-30 17:05:49	100	TAB	Depreciate asset by use of depreciation table	Y	Table	N	Test	2008-05-30 17:05:49	100
50008	11	11	2008-05-30 17:05:49	100	DB2SL	Declining Balance 200% (DB) Depreciation with a switch to Straight Line	Y	200% DB to SL	N	Test	2008-05-30 17:05:49	100
50009	11	11	2008-05-30 17:05:50	100	DB1SL	Declining Balance 150% (DB) Depreciation with a switch to Straight Line	Y	150% DB to SL	N	Test	2008-05-30 17:05:50	100
50010	11	11	2008-05-30 17:05:51	100	VARSL	Variable Accelerated with a switch to Straight Line	Y	Variable Accelerated to SL	N	Test	2008-05-30 17:05:51	100
\.
