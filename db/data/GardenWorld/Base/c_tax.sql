copy "c_tax" ("c_tax_id", "name", "ad_client_id", "ad_org_id", "ad_rule_id", "c_country_id", "created", "createdby", "c_region_id", "c_taxcategory_id", "description", "isactive", "isdefault", "isdocumentlevel", "issalestax", "issummary", "istaxexempt", "parent_tax_id", "rate", "requirestaxcertificate", "sopotype", "taxindicator", "to_country_id", "to_region_id", "updated", "updatedby", "validfrom") from STDIN;
104	Standard	11	0	\N	100	2001-03-27 15:44:24	0	\N	107	\N	Y	Y	Y	N	N	N	\N	0	N	B	\N	100	\N	2003-02-22 02:20:37	0	1990-01-01 00:00:00
105	CT Sales	11	0	\N	100	2001-05-11 18:38:19	100	102	107	\N	Y	N	N	N	N	N	\N	6	N	B	6%CT	100	102	2003-02-22 02:20:37	0	1970-01-01 00:00:00
106	GST	11	0	\N	109	2003-01-08 13:14:01	100	\N	107	Canadian Federal Sales Tax	Y	N	N	N	N	N	108	7	N	B	GST	109	\N	2003-02-22 02:20:37	0	2001-01-01 00:00:00
107	PST	11	0	\N	109	2003-01-08 15:06:11	100	\N	107	Canadian Provintial Tax	Y	N	N	N	N	N	108	7.5	N	B	PST	109	\N	2003-02-22 02:20:37	0	2002-01-01 00:00:00
108	GST/PST	11	0	\N	109	2003-01-08 15:07:02	100	\N	107	Canadian Federal & State Tax	Y	N	N	N	Y	N	\N	14.5	N	B	Tax	109	\N	2003-02-22 14:20:21	0	2002-01-01 00:00:00
109	Exempt	11	0	\N	\N	2003-01-17 16:45:29	100	\N	107	Used when Business Partners are exempt from tax	Y	N	N	N	N	Y	\N	0	Y	B	\N	\N	\N	2003-02-22 02:20:37	0	2000-01-01 00:00:00
\.
