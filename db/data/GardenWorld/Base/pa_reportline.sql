copy "pa_reportline" ("pa_reportline_id", "name", "ad_client_id", "ad_org_id", "amounttype", "calculationtype", "created", "createdby", "description", "gl_budget_id", "isactive", "isprinted", "issummary", "linetype", "oper_1_id", "oper_2_id", "overlinestroketype", "paamounttype", "paperiodtype", "parent_id", "pa_reportlineset_id", "postingtype", "seqno", "underlinestroketype", "updated", "updatedby") from STDIN;
100	1	11	11	\N	\N	2003-01-21 00:19:17	100	Assets	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	10	\N	2003-03-03 21:35:53	0
101	11	11	11	\N	\N	2003-01-21 00:19:17	100	Cash	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	20	\N	2003-02-13 17:24:50	0
102	12	11	11	\N	\N	2003-01-21 00:19:17	100	Accounts Receivable	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	30	\N	2003-02-13 17:24:54	0
103	13	11	11	\N	\N	2003-01-21 00:19:17	100	Investments	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	40	\N	2003-02-13 17:24:59	0
104	14	11	11	\N	\N	2003-01-21 00:19:17	100	Inventory	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	50	\N	2003-02-13 17:25:05	0
105	15	11	11	\N	\N	2003-01-21 00:19:17	100	Prepaid Expenses, Deposits & Other Current Assets	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	60	\N	2003-02-13 17:25:11	0
106	15_	11	11	\N	R	2003-01-21 00:19:17	100	Total Current Assets	\N	Y	Y	N	C	101	105	\N	\N	\N	\N	100	\N	70	\N	2003-02-13 17:25:21	0
107	16	11	11	\N	\N	2003-01-21 00:19:17	100	Land and Building	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	80	\N	2003-02-13 17:25:26	0
108	17	11	11	\N	\N	2003-01-21 00:19:17	100	Furniture, Fixtures & Equipment	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	90	\N	2003-02-13 17:25:31	0
109	18	11	11	\N	\N	2003-01-21 00:19:17	100	Accumulated Depreciation	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	100	\N	2003-02-13 17:25:40	0
110	19	11	11	\N	\N	2003-01-21 00:19:17	100	Other Assets	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	110	\N	2004-06-09 16:52:42	100
111	19_	11	11	\N	R	2003-01-21 00:19:17	100	Fixed and other Assets	\N	Y	Y	N	C	107	110	\N	\N	\N	\N	100	\N	120	\N	2003-02-13 17:25:52	0
112	2	11	11	\N	\N	2003-01-21 00:19:17	100	Liabilities	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	140	\N	2003-02-13 17:27:24	0
113	21	11	11	\N	\N	2003-01-21 00:19:17	100	Accounts Payable	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	150	\N	2003-02-13 17:27:30	0
114	22	11	11	\N	\N	2003-01-21 00:19:17	100	Accrued Expenses	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	160	\N	2003-02-13 17:27:40	0
115	23	11	11	\N	\N	2003-01-21 00:19:17	100	Current Note Payables	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	170	\N	2003-02-13 17:27:51	0
116	24	11	11	\N	\N	2003-01-21 00:19:17	100	Long Term Liabilities	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	180	\N	2003-02-13 17:28:06	0
117	3	11	11	\N	\N	2003-01-21 00:19:17	100	Owner's Equity/Net Worth	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	200	\N	2003-02-13 17:30:29	0
118	31	11	11	\N	\N	2003-01-21 00:19:18	100	Capital	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	210	\N	2003-02-13 17:30:35	0
119	32	11	11	\N	\N	2003-01-21 00:19:18	100	Current Profit & Loss	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	100	\N	220	\N	2003-02-13 17:31:03	0
120	33	11	11	\N	R	2003-01-21 00:19:18	100	Total Liabilities and Equity	\N	Y	Y	N	C	122	119	\N	\N	\N	\N	100	\N	252	\N	2003-02-13 17:33:23	0
121	1__	11	11	\N	A	2003-02-13 17:26:37	100	Toral Assets	\N	Y	Y	N	C	106	111	\N	\N	\N	\N	100	\N	130	\N	2003-02-13 17:28:57	0
122	2__	11	11	\N	R	2003-02-13 17:29:50	100	Total Liabilities	\N	Y	Y	N	C	113	116	\N	\N	\N	\N	100	\N	190	\N	2003-02-13 17:29:50	0
123	4	11	11	\N	\N	2005-10-23 13:03:09	100	Sales	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	251	\N	2005-10-23 13:03:09	100
124	49	11	11	\N	\N	2005-10-23 13:03:09	100	Sales Discounts	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	258	\N	2005-10-23 13:03:09	100
125	5	11	11	\N	\N	2005-10-23 13:03:09	100	Cost of Goods Sold	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	262	\N	2005-10-23 13:03:09	100
126	55	11	11	\N	\N	2005-10-23 13:03:09	100	Returns	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	270	\N	2005-10-23 13:03:09	100
127	56	11	11	\N	\N	2005-10-23 13:03:09	100	Inventory CoGs	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	273	\N	2005-10-23 13:03:09	100
128	58	11	11	\N	\N	2005-10-23 13:03:09	100	CoGs Variances	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	279	\N	2005-10-23 13:03:09	100
129	59	11	11	\N	\N	2005-10-23 13:03:09	100	CoGS Discounts	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	283	\N	2005-10-23 13:03:09	100
130	5_	11	11	\N	\N	2005-10-23 13:03:09	100	Gross Margin	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	286	\N	2005-10-23 13:03:09	100
131	60	11	11	\N	\N	2005-10-23 13:03:09	100	Payroll Expense	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	288	\N	2005-10-23 13:03:09	100
132	61	11	11	\N	\N	2005-10-23 13:03:09	100	Occupancy Cost	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	316	\N	2005-10-23 13:03:09	100
133	62	11	11	\N	\N	2005-10-23 13:03:09	100	Advertising & Promotion	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	320	\N	2005-10-23 13:03:09	100
134	63	11	11	\N	\N	2005-10-23 13:03:09	100	Telephone and Communication	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	328	\N	2005-10-23 13:03:09	100
135	64	11	11	\N	\N	2005-10-23 13:03:09	100	Professional Services	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	332	\N	2005-10-23 13:03:09	100
136	65	11	11	\N	\N	2005-10-23 13:03:09	100	Stationary & Supplies	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	337	\N	2005-10-23 13:03:09	100
137	66	11	11	\N	\N	2005-10-23 13:03:09	100	Data Processing Expense	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	340	\N	2005-10-23 13:03:09	100
138	67	11	11	\N	\N	2005-10-23 13:03:09	100	Depreciation Expense	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	345	\N	2005-10-23 13:03:09	100
139	68	11	11	\N	\N	2005-10-23 13:03:09	100	Travel & Entertainment	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	356	\N	2005-10-23 13:03:09	100
140	69	11	11	\N	\N	2005-10-23 13:03:09	100	Insurance	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	364	\N	2005-10-23 13:03:09	100
141	70	11	11	\N	\N	2005-10-23 13:03:09	100	Payment Processor Costs	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	369	\N	2005-10-23 13:03:09	100
142	71	11	11	\N	\N	2005-10-23 13:03:09	100	Dues & Subscriptions	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	373	\N	2005-10-23 13:03:09	100
143	72	11	11	\N	\N	2005-10-23 13:03:09	100	Office Expense	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	376	\N	2005-10-23 13:03:09	100
144	73	11	11	\N	\N	2005-10-23 13:03:09	100	Postage & Shipping	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	378	\N	2005-10-23 13:03:09	100
145	74	11	11	\N	\N	2005-10-23 13:03:09	100	Taxes and Licenses	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	383	\N	2005-10-23 13:03:09	100
146	75	11	11	\N	\N	2005-10-23 13:03:09	100	Education	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	389	\N	2005-10-23 13:03:09	100
147	76	11	11	\N	\N	2005-10-23 13:03:09	100	Equipment Rent	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	394	\N	2005-10-23 13:03:09	100
148	77	11	11	\N	\N	2005-10-23 13:03:09	100	Repairs & Maintenance	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	398	\N	2005-10-23 13:03:09	100
149	78	11	11	\N	\N	2005-10-23 13:03:09	100	Other Operating Expenses	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	405	\N	2005-10-23 13:03:09	100
150	79	11	11	\N	\N	2005-10-23 13:03:09	100	Default/Suspense Accounts	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	415	\N	2005-10-23 13:03:09	100
151	79_	11	11	\N	\N	2005-10-23 13:03:09	100	Total Operating Expenses	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	423	\N	2005-10-23 13:03:09	100
152	79__	11	11	\N	\N	2005-10-23 13:03:09	100	Operating Income	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	424	\N	2005-10-23 13:03:09	100
153	80	11	11	\N	\N	2005-10-23 13:03:09	100	Other Income	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	425	\N	2005-10-23 13:03:09	100
154	805	11	11	\N	\N	2005-10-23 13:03:09	100	Currency Gain	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	430	\N	2005-10-23 13:03:09	100
155	82	11	11	\N	\N	2005-10-23 13:03:09	100	Other Expense	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	438	\N	2005-10-23 13:03:09	100
156	825	11	11	\N	\N	2005-10-23 13:03:09	100	Currency Loss	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	443	\N	2005-10-23 13:03:09	100
157	82_	11	11	\N	\N	2005-10-23 13:03:09	100	Net Income before Tax	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	452	\N	2005-10-23 13:03:09	100
158	89	11	11	\N	\N	2005-10-23 13:03:09	100	Income Tax & Summary	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	453	\N	2005-10-23 13:03:09	100
159	89_	11	11	\N	\N	2005-10-23 13:03:09	100	Net Income	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	102	\N	457	\N	2005-10-23 13:03:09	100
160	4	11	0	\N	\N	2005-12-12 12:25:24	100	Sales	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	251	\N	2005-12-12 12:25:24	100
162	5	11	0	\N	\N	2005-12-12 12:25:24	100	Cost of Goods Sold	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	262	\N	2005-12-12 12:25:24	100
167	Gross Margin	11	0	\N	S	2005-12-12 12:25:24	100	Gross Margin	\N	Y	Y	N	C	160	162	\N	\N	\N	\N	104	\N	286	\N	2005-12-23 15:06:01	100
177	6	11	0	\N	\N	2005-12-12 12:25:24	100	6-Expenses	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	364	\N	2005-12-12 14:56:42	100
188	Total Expenses	11	0	\N	R	2005-12-12 12:25:24	100	Total  Expenses	\N	Y	Y	N	C	177	199	\N	\N	\N	\N	104	\N	423	\N	2005-12-21 14:16:05	100
189	Gross Margin less Expenses	11	0	\N	S	2005-12-12 12:25:24	100	Gross Margin less Expenses	\N	Y	Y	N	C	162	188	\N	\N	\N	\N	104	\N	424	\N	2005-12-21 14:15:06	100
190	80	11	0	\N	\N	2005-12-12 12:25:24	100	Other Income summarized 	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	425	\N	2005-12-21 13:57:04	100
192	82	11	0	\N	\N	2005-12-12 12:25:24	100	Other Expense	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	427	\N	2005-12-21 14:02:51	100
194	Net other income (loss)	11	0	\N	S	2005-12-12 12:25:24	100	Net Other Income (loss)	\N	Y	Y	N	C	190	192	\N	\N	\N	\N	104	\N	429	\N	2006-01-05 15:30:43	101
195	89	11	0	\N	\N	2005-12-12 12:25:24	100	Income Tax & Summary	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	450	\N	2005-12-21 14:26:19	100
196	Net Income	11	0	\N	S	2005-12-12 12:25:24	100	Net Income	\N	Y	Y	N	C	200	195	\N	\N	\N	\N	104	\N	455	\N	2005-12-21 14:27:27	100
199	7-Expenses	11	0	\N	A	2005-12-14 11:23:01	100	Summary of all 7-Expenses	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	400	\N	2005-12-14 11:23:01	100
200	Net Income Before Taxes	11	0	\N	A	2005-12-21 14:06:40	100	Net Income Before Taxes	\N	Y	Y	N	C	189	194	\N	\N	\N	\N	104	\N	440	\N	2006-01-05 15:31:29	101
201	Depreciation Expense summarized	11	0	\N	A	2005-12-21 14:29:29	100	Depreciation Expense summarized	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	459	\N	2005-12-23 15:19:56	100
202	Accrued A/P increase	11	0	\N	A	2005-12-21 14:34:13	100	Accrued A/P increase	\N	Y	Y	N	S	196	201	\N	\N	\N	\N	104	\N	470	\N	2005-12-21 15:36:36	100
203	Additions to Cash Flow	11	0	\N	R	2005-12-21 14:35:15	100	Additions to Cash Flow	\N	Y	Y	N	C	201	202	\N	\N	\N	\N	104	\N	475	\N	2005-12-23 15:32:09	100
204	Cash Flow sub total	11	0	\N	A	2005-12-21 15:44:47	100	Cash Flow sub total	\N	Y	Y	N	C	196	203	\N	\N	\N	\N	104	\N	480	\N	2005-12-21 15:44:47	100
205	Loss on sale of fixed asset	11	0	\N	A	2005-12-21 15:50:43	100	Loss on sale of fixed asset	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	465	\N	2005-12-23 15:17:16	100
206	Receivables(A/R, loans, taxes) summarized	11	0	\N	A	2005-12-21 15:54:47	100	Receivables(A/R, loans, taxes) summarized	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	490	\N	2005-12-21 15:54:47	100
207	Inventory summarized	11	0	\N	A	2005-12-21 15:55:53	100	Inventory summarized	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	495	\N	2005-12-21 15:55:53	100
208	Prepayments summarized	11	0	\N	A	2005-12-21 15:57:17	100	Prepayments summarized	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	500	\N	2005-12-21 15:57:17	100
209	Accounts Payable summarized	11	0	\N	A	2005-12-21 15:58:37	100	Accounts Payable summarized	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	467	\N	2005-12-22 13:00:20	100
210	Subtractions to Cash Flow	11	0	\N	R	2005-12-21 16:01:03	100	Subtractions to Cash Flow	\N	Y	Y	N	C	206	236	\N	\N	\N	\N	104	\N	510	\N	2005-12-23 15:41:11	100
211	Cash Flow from Operations	11	0	\N	S	2005-12-21 16:02:32	100	Cash Flow from Operations	\N	Y	Y	N	C	204	210	\N	\N	\N	\N	104	\N	515	\N	2005-12-21 16:02:32	100
212	Investments activities summarized	11	0	\N	A	2005-12-22 13:10:06	100	Investments activities summarized	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	530	\N	2005-12-22 13:10:06	100
214	Fixed Asset activity summarized	11	0	\N	A	2005-12-22 13:15:18	100	Fixed Asset activity summarized	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	540	\N	2005-12-22 13:15:18	100
215	Equipment activity summarized	11	0	\N	A	2005-12-22 13:20:32	100	Equipment activity summarized	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	545	\N	2005-12-22 13:20:32	100
216	Net cash from Investment activity	11	0	\N	A	2005-12-22 13:24:01	100	Net cash from Investment activity	\N	Y	Y	N	C	220	238	\N	\N	\N	\N	104	\N	555	\N	2006-01-05 15:44:42	101
218	Loss on sale of fixed asset	11	0	\N	A	2005-12-22 13:27:33	100	Loss on sale of fixed asset	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	521	\N	2005-12-22 14:38:14	100
219	Gain in sale of fixed asset	11	0	\N	A	2005-12-22 13:45:06	100	Gain in sale of fixed asset	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	520	\N	2005-12-22 14:38:25	100
220	Net gain on sale of fixed assets	11	0	\N	S	2005-12-22 14:02:07	100	Net gain  on sale of fixed assets	\N	Y	Y	N	C	218	219	\N	\N	\N	\N	104	\N	522	\N	2005-12-22 14:38:57	100
221	Accum. Depreciation summarized	11	0	\N	A	2005-12-22 14:05:24	100	Accum. Depreciation summarized	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	461	\N	2006-01-05 15:40:05	101
222	Accum. Depreciation summarized for Investments	11	0	\N	A	2005-12-22 14:41:54	100	Accum. Depreciation summarized for Investments	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	536	\N	2006-01-05 15:40:51	101
223	Common Stock summarized	11	0	\N	A	2005-12-22 14:55:13	100	Common Stock summarized	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	560	\N	2005-12-22 15:08:38	100
224	Cash Dividends summarized	11	0	\N	A	2005-12-22 14:58:49	100	Cash Dividends summarized	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	565	\N	2005-12-22 15:08:01	100
225	Net cash from financing activity	11	0	\N	R	2005-12-22 15:12:00	100	Net cash from financing activity	\N	Y	Y	N	C	223	224	\N	\N	\N	\N	104	\N	570	\N	2005-12-22 15:14:57	100
226	Cash flow from investing and financing activity	11	0	\N	A	2005-12-22 15:16:48	100	Cash flow from investing and financing activity	\N	Y	Y	N	C	216	225	\N	\N	\N	\N	104	\N	580	\N	2005-12-22 15:17:28	100
227	Net  calculated change in cash	11	0	\N	A	2005-12-22 15:21:46	100	Net calculated change in cash	\N	Y	Y	N	C	211	226	\N	\N	\N	\N	104	\N	590	\N	2006-01-05 16:28:08	101
230	Cash summarized	11	0	\N	A	2005-12-22 15:30:24	100	Cash summarized	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	610	\N	2006-01-05 16:18:54	101
231	Amoritization summarized	11	0	\N	A	2005-12-23 15:21:26	100	Amoritization summarized	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	460	\N	2005-12-23 15:21:26	100
232	Uninsured Casualty Loss 	11	0	\N	A	2005-12-23 15:23:47	100	Uninsured Casualty Loss	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	462	\N	2005-12-23 15:23:47	100
233	Unrealized Loss	11	0	\N	A	2005-12-23 15:33:12	100	Unrealized Loss	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	463	\N	2005-12-23 15:33:12	100
234	Capital Loss	11	0	\N	A	2005-12-23 15:34:24	100	Capital Loss	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	464	\N	2005-12-23 15:34:24	100
235	Unrealized Gain	11	0	\N	A	2005-12-23 15:38:00	100	Unrealized Gain	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	505	\N	2005-12-23 15:38:00	100
236	Fixed Asset Sale Gain	11	0	\N	A	2005-12-23 15:39:27	100	Fixed Asset Sale Gain	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	506	\N	2005-12-23 15:39:27	100
237	Currency Balancing summarized	11	0	\N	A	2006-01-05 15:32:47	101	Currency Balancing summarized	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	468	\N	2006-01-05 15:32:47	101
238	Sub total asset changes	11	0	\N	R	2006-01-05 15:37:21	101	Sub total asset changes	\N	Y	Y	N	C	222	215	\N	\N	\N	\N	104	\N	550	\N	2006-01-05 15:44:12	101
239	Change in Deposits	11	0	\N	A	2006-01-05 16:03:08	101	Change in Deposits	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	562	\N	2006-01-05 16:03:08	101
240	Unrealized Gain summarized for total cash flow	11	0	\N	A	2006-01-05 16:05:54	101	Unrealized Gain summarized for total cash flow	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	592	\N	2006-01-05 16:12:04	101
241	Unrealized Losses summarized for total Cash flow	11	0	\N	A	2006-01-05 16:07:42	101	Unrealized Losses summarized for total Cash flow	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	593	\N	2006-01-05 16:13:40	101
242	Currency Balancing summarized for total Cash flow	11	0	\N	A	2006-01-05 16:09:41	101	Unrealized Balancing summarized for total Cash flow	\N	Y	Y	N	S	\N	\N	\N	\N	\N	\N	104	\N	594	\N	2006-01-05 16:12:50	101
243	Net total effect of change in Direct Exchange rate	11	0	\N	R	2006-01-05 16:16:00	101	Net total effect of change in Direct Exchange rate	\N	Y	Y	N	C	240	242	\N	\N	\N	\N	104	\N	595	\N	2006-01-05 16:16:00	101
244	Total Net  Cash Flow	11	0	\N	A	2006-01-05 16:22:16	101	Total Net  Cash Flow	\N	Y	Y	N	C	227	243	\N	\N	\N	\N	104	\N	600	\N	2006-01-05 16:22:52	101
\.
