copy "a_depreciation_method" ("a_depreciation_method_id", "ad_client_id", "ad_org_id", "created", "createdby", "depreciationtype", "description", "isactive", "name", "processed", "text", "updated", "updatedby") from STDIN;
50000	11	11	2008-05-30 17:05:51	100	MDI	Books entire adjustment in current month	Y	Month - Current Period	N	Test	2008-05-30 17:05:51	100
50001	11	11	2008-05-30 17:05:52	100	LDI	Recalculates depreciation expense from Date of Service and spreads adjustment over remaining periods of the assets life.	Y	Life - Remainder of life	N	Test	2008-05-30 17:05:52	100
50002	11	11	2008-05-30 17:05:53	100	YDI	Recalculates depreciation expense for current year.  Applies monthly spread percentage to determine period expense.	Y	Year - Remainder of year	N	Test	2008-05-30 17:05:53	100
\.
