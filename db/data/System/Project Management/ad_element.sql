copy "ad_element" ("ad_element_id", "columnname", "ad_client_id", "ad_org_id", "ad_reference_id", "ad_reference_value_id", "created", "createdby", "description", "entitytype", "fieldlength", "help", "isactive", "name", "po_description", "po_help", "po_name", "po_printname", "printname", "updated", "updatedby") from STDIN;
1025	M_InOut_ID	0	0	13	\N	1999-12-19 20:40:45	0	Material Shipment Document	Project Management	22	The Material Shipment / Receipt 	Y	Shipment/Receipt	Material Receipt Document	The Material Shipment / Receipt 	Receipt	Receipt	Shipment/Receipt	2000-01-02 00:00:00	0
1550	C_Cycle_ID	0	0	13	\N	2001-03-11 17:37:01	0	Identifier for this Project Reporting Cycle	Project Management	22	Identifies a Project Cycle which can be made up of one or more cycle steps and cycle phases.	Y	Project Cycle	\N	\N	\N	\N	Project Cycle	2000-01-02 00:00:00	0
1551	C_CycleStep_ID	0	0	19	\N	2001-03-11 17:37:01	0	The step for this Cycle	Project Management	22	Identifies one or more steps within a Project Cycle. A cycle Step has multiple Phases	Y	Cycle Step	\N	\N	\N	\N	Cycle Step	2000-01-02 00:00:00	0
1571	RelativeWeight	0	0	22	\N	2001-03-11 17:37:02	0	Relative weight of this step (0 = ignored)	Project Management	22	The relative weight allows you to adjust the project cycle report based on probabilities.  For example, if you have a 1:10 chance in closing a contract when it is in the prospect stage and a 1:2 chance when it is in the contract stage, you may put a weight of 0.1 and 0.5 on those steps. This allows sales funnels or measures of completion of your project.	Y	Relative Weight	\N	\N	\N	\N	Relative Weight	2000-01-02 00:00:00	0
1670	DetailInfo	0	0	36	\N	2001-11-18 16:27:47	0	Additional Detail Information	Project Management	0	\N	Y	Detail Information	\N	\N	\N	\N	Detail Information	2005-05-15 15:47:01	100
1675	ProductValue	0	0	10	\N	2001-11-18 16:27:47	0	Key of the Product	Project Management	40	\N	Y	Product Key	\N	\N	\N	\N	Product Key	2016-04-11 16:36:25	0
1783	S_TimeExpense_ID	0	0	13	\N	2002-06-15 21:03:57	0	Time and Expense Report	Project Management	22	\N	Y	Expense Report	\N	\N	\N	\N	Expense Report	2000-01-02 00:00:00	0
2058	StandardQty	0	0	29	\N	2003-05-28 21:40:00	0	Standard Quantity	Project Management	22	\N	Y	Standard Quantity	\N	\N	\N	\N	Standard Quantity	2000-01-02 00:00:00	0
2094	BPartnerValue	0	0	10	\N	2003-06-07 19:49:50	0	Key of the Business Partner	Project Management	40	\N	Y	Business Partner Key	\N	\N	\N	\N	Business Partner Key	2016-04-11 16:36:22	0
2118	ProjectValue	0	0	10	\N	2003-06-07 19:49:51	0	Key of the Project	Project Management	40	\N	Y	Project Key	\N	\N	\N	\N	Project Key	2000-01-02 00:00:00	0
2162	ProjectPhaseName	0	0	10	\N	2003-08-16 19:59:00	0	Name of the Project Phase	Project Management	60	\N	Y	Project Phase	\N	\N	\N	\N	Project Phase	2000-01-02 00:00:00	0
2163	ProjectTypeName	0	0	10	\N	2003-08-16 19:59:00	0	Name of the Project Type	Project Management	60	\N	Y	Project Type	\N	\N	\N	\N	Project Type	2000-01-02 00:00:00	0
2164	CycleName	0	0	10	\N	2003-08-17 16:48:09	0	Name of the Project Cycle	Project Management	60	\N	Y	Cycle Name	\N	\N	\N	\N	Cycle	2000-01-02 00:00:00	0
2165	CycleStepName	0	0	10	\N	2003-08-17 16:48:09	0	Name of the Project Cycle Step	Project Management	60	\N	Y	Cycle Step Name	\N	\N	\N	\N	Cycle Step	2010-01-13 10:38:15	0
2182	ConsolidateDocument	0	0	\N	\N	2003-09-03 14:48:16	0	Consolidate Lines into one Document	Project Management	\N	\N	Y	Consolidate to one Document	\N	\N	\N	\N	Consolidate	2005-10-24 13:35:19	100
2222	IssueDescription	0	0	10	\N	2003-10-12 23:12:39	0	Description of the Issue line	Project Management	255	\N	Y	Issue Description	\N	\N	\N	\N	Issue Description	2000-01-02 00:00:00	0
2223	IssueLine	0	0	11	\N	2003-10-12 23:12:39	0	Line number of the issue	Project Management	22	\N	Y	Issue Line	\N	\N	\N	\N	Issue Line	2000-01-02 00:00:00	0
2224	LineMargin	0	0	12	\N	2003-10-12 23:12:39	0	Margin of the line - Planned Amount minus Costs	Project Management	22	\N	Y	Line Margin	\N	\N	\N	\N	Line Margin	2000-01-02 00:00:00	0
2319	Cost	0	0	37	\N	2004-01-01 23:37:18	0	Cost information	Project Management	22	\N	Y	Cost	\N	\N	\N	\N	Cost	2000-01-02 00:00:00	0
2342	AmtAcct	0	0	12	\N	2004-01-09 15:15:02	0	Amount Balance in Currency of Accounting Schema	Project Management	22	\N	Y	Accounted Amount	\N	\N	\N	\N	Accounted	2016-04-11 16:36:22	0
2343	AmtSource	0	0	12	\N	2004-01-09 15:15:02	0	Amount Balance in Source Currency	Project Management	22	\N	Y	Source Amount	\N	\N	\N	\N	Source	2016-04-11 16:36:22	0
2659	ProductName	0	0	10	\N	2004-11-28 01:41:51	0	Name of the Product	Project Management	60	\N	Y	Product Name	\N	\N	\N	\N	Product Name	2016-04-11 16:43:10	0
57112	Hours	0	0	\N	\N	2014-04-10 20:13:26	100	\N	Project Management	\N	\N	Y	Hours	\N	\N	\N	\N	Hours	2014-04-10 20:13:26	100
57259	isRevenue	0	0	\N	\N	2014-07-14 15:10:39	100	\N	Project Management	\N	\N	Y	Revenue Account	\N	\N	\N	\N	Revenue Account	2014-07-14 15:10:39	100
57260	isCOGS	0	0	\N	\N	2014-07-14 15:10:40	100	\N	Project Management	\N	\N	Y	COGS Account	\N	\N	\N	\N	COGS Account	2014-07-14 15:10:40	100
57261	Revenue	0	0	\N	\N	2014-07-14 15:10:42	100	\N	Project Management	\N	\N	Y	Revenue	\N	\N	\N	\N	Revenue	2014-07-14 15:10:42	100
57262	COGS	0	0	\N	\N	2014-07-14 15:10:43	100	\N	Project Management	\N	\N	Y	COGS	\N	\N	\N	\N	COGS	2014-07-14 15:10:43	100
\.
