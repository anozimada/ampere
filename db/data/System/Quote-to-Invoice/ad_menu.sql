copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
100	Invoice Reports	\N	0	\N	\N	0	\N	\N	1999-12-20 11:03:18	0	\N	Quote-to-Invoice	Y	N	N	Y	Y	2022-02-17 21:56:48	100
129	Sales Order	W	0	\N	\N	0	\N	143	1999-08-09 00:00:00	0	Enter and change sales orders	Quote-to-Invoice	Y	Y	N	Y	N	2000-01-02 00:00:00	0
166	Sales	\N	0	\N	\N	0	\N	\N	1999-12-02 13:15:53	0	\N	Quote-to-Invoice	Y	N	N	Y	Y	2020-10-16 23:45:32	100
178	AR Invoice	W	0	\N	\N	0	\N	167	1999-12-19 21:29:22	0	AR Invoice Entry	Quote-to-Invoice	Y	Y	N	Y	N	2022-02-28 00:36:44	100
180	Customer Shipment	W	0	\N	\N	0	\N	169	1999-12-19 21:33:22	0	Customer Inventory Shipments Customer Returns	Quote-to-Invoice	Y	Y	N	Y	N	2022-02-28 00:33:09	100
192	Generate Invoices	P	0	\N	\N	0	119	\N	2000-04-26 21:17:38	0	Generate and print Invoices from open Orders	Quote-to-Invoice	Y	Y	N	Y	N	2000-01-02 00:00:00	0
193	Generate Shipments	P	0	\N	\N	0	118	\N	2000-04-26 21:18:25	0	Generate and print Shipments from open Orders	Quote-to-Invoice	Y	Y	N	Y	N	2005-01-05 21:31:14	100
195	Open Orders	R	0	\N	\N	0	121	\N	2000-04-26 21:44:55	0	Open Order Report	Quote-to-Invoice	Y	Y	N	Y	N	2000-01-02 00:00:00	0
202	Invoices (Acct)	R	0	\N	\N	0	127	\N	2000-05-15 22:23:52	0	Invoice Transactions by Accounting Date	Quote-to-Invoice	Y	Y	N	Y	N	2022-02-17 21:58:36	100
210	Invoice summary (Day)	R	0	\N	\N	0	128	\N	2000-06-01 17:03:14	0	Invoice Report per Day	Quote-to-Invoice	Y	Y	N	N	N	2022-02-17 21:59:55	100
211	Invoice summary (Mth)	R	0	\N	\N	0	129	\N	2000-06-01 17:03:34	0	Invoice Report per Month	Quote-to-Invoice	Y	Y	N	N	N	2022-02-17 22:01:51	100
212	Invoice summary (P Cat / Mth)	R	0	\N	\N	0	132	\N	2000-06-01 17:04:12	0	Invoice Report by Product Category per Month	Quote-to-Invoice	Y	Y	N	N	N	2022-02-17 22:02:03	100
213	Invoice summary (P Cat / Wk)	R	0	\N	\N	0	131	\N	2000-06-01 17:04:36	0	Invoice Report by Product Category per Week	Quote-to-Invoice	N	Y	N	N	N	2022-02-17 22:06:35	100
214	Invoice summary (BP / Mth)	R	0	\N	\N	0	133	\N	2000-06-01 17:05:10	0	Invoice Report by Product Vendor per Month	Quote-to-Invoice	Y	Y	N	N	N	2022-02-17 22:02:21	100
215	Invoice summary (Wk)	R	0	\N	\N	0	130	\N	2000-06-01 17:05:28	0	Invoice Report per Week	Quote-to-Invoice	Y	Y	N	N	N	2022-02-17 22:06:30	100
217	Invoice selected SO	X	0	100	\N	0	\N	\N	2000-09-04 14:05:02	0	Select and generate invoices	Quote-to-Invoice	Y	Y	N	N	N	2022-02-17 22:44:58	100
230	Quarterly Invoice Customer by Product	R	0	\N	\N	0	138	\N	2000-12-04 16:37:32	0	Invoice Report by Customer and Product Category per Quarter	Quote-to-Invoice	Y	Y	N	Y	N	2005-12-15 12:11:45	100
231	Invoice summary (BP / Qtr)	R	0	\N	\N	0	139	\N	2000-12-04 16:37:56	0	Invoice Report by Customer and Product Vendor per Quarter	Quote-to-Invoice	N	Y	N	Y	N	2022-02-17 22:03:43	100
252	Invoice Transactions (Doc)	R	0	\N	\N	0	151	\N	2001-01-27 20:13:12	0	Invoice Transactions by Invoice Date	Quote-to-Invoice	Y	Y	N	Y	N	2005-08-27 11:57:45	100
253	Invoice Detail & Margin	R	0	\N	\N	0	152	\N	2001-01-27 20:13:56	0	Invoice (Line) Detail and Margin Report	Quote-to-Invoice	Y	Y	N	Y	N	2000-01-02 00:00:00	0
335	Generate PO from SO	P	0	\N	\N	0	193	\N	2002-12-09 18:30:36	0	Create Purchase Order from Sales Orders	Quote-to-Invoice	Y	Y	N	Y	N	2022-02-17 22:42:54	100
346	Generate Shipments (manual)	X	0	110	\N	0	\N	\N	2003-01-25 14:33:31	0	Select and generate shipments	Quote-to-Invoice	Y	Y	N	N	N	2000-01-02 00:00:00	0
347	Print Invoices	P	0	\N	\N	0	200	\N	2003-01-29 21:28:03	0	Print Invoices to paper or send PDF	Quote-to-Invoice	Y	Y	N	Y	N	2000-01-02 00:00:00	0
377	Invoice Payment Schedule	W	0	\N	\N	0	\N	275	2003-06-03 01:50:07	0	Maintain Invoice Payment Schedule	Quote-to-Invoice	N	Y	N	Y	N	2022-02-17 21:56:02	100
406	Reprice Order/Invoice	P	0	\N	\N	0	232	\N	2003-10-04 11:55:20	0	Recalculate the price based on the latest price list version of an open order or invoice	Quote-to-Invoice	Y	Y	N	Y	N	2022-02-27 23:44:04	100
407	Quote convert	P	0	\N	\N	0	231	\N	2003-10-04 11:55:54	0	Convert open Proposal or Quotation to Order	Quote-to-Invoice	Y	Y	N	Y	N	2000-01-02 00:00:00	0
436	Reopen Order	P	0	\N	\N	0	255	\N	2004-01-17 00:15:40	0	Open previously closed Order	Quote-to-Invoice	Y	Y	N	Y	N	2000-01-02 00:00:00	0
444	Package	W	0	\N	\N	0	\N	319	2004-02-19 22:54:51	0	Manage Shipment Packages	Quote-to-Invoice	N	Y	N	Y	N	2019-12-05 14:07:35	100
448	Subscription Type	W	0	\N	\N	0	\N	317	2004-02-19 14:16:50	0	Maintain Subscription Types	Quote-to-Invoice	N	Y	N	Y	N	2019-12-05 14:07:17	100
449	Subscription	W	0	\N	\N	0	\N	316	2004-02-19 14:13:29	0	Maintain Subscriptions and Deliveries	Quote-to-Invoice	N	Y	N	Y	N	2019-12-05 14:07:21	100
457	Sales Orders	\N	0	\N	\N	0	\N	\N	2004-02-29 15:23:09	0	\N	Quote-to-Invoice	Y	N	N	N	Y	2000-01-02 00:00:00	0
458	Sales Invoices	\N	0	\N	\N	0	\N	\N	2004-02-29 15:24:53	0	\N	Quote-to-Invoice	Y	N	N	N	Y	2000-01-02 00:00:00	0
459	Shipments	\N	0	\N	\N	0	\N	\N	2004-02-29 15:26:47	0	\N	Quote-to-Invoice	Y	N	N	N	Y	2000-01-02 00:00:00	0
494	Shipment Details	R	0	\N	\N	0	294	\N	2004-07-20 21:48:37	0	Shipment Detail Information	Quote-to-Invoice	N	Y	N	Y	N	2019-12-05 13:57:58	100
507	Order Batch Process	P	0	\N	\N	0	315	\N	2005-01-07 14:33:25	100	Process Orders in Batch	Quote-to-Invoice	Y	Y	N	Y	N	2005-01-07 14:33:25	100
543	Order Detail	R	0	\N	\N	0	333	\N	2005-08-27 08:53:28	100	Order Detail Report	Quote-to-Invoice	Y	Y	N	N	N	2005-08-27 09:03:25	100
553	Invoice summary (Product / Qtr)	R	0	\N	\N	0	341	\N	2005-12-15 14:57:18	100	Invoice Report by Product per Quarter	Quote-to-Invoice	Y	Y	N	Y	N	2022-02-17 22:03:59	100
554	Invoice summary (Prod / Mth)	R	0	\N	\N	0	340	\N	2005-12-15 14:57:37	100	Invoice Report by Product per Month	Quote-to-Invoice	Y	Y	N	Y	N	2022-02-17 22:02:38	100
53132	Generate Shipments & Invoices (manual)	X	0	53010	\N	0	\N	\N	2008-05-29 23:18:19	0	Select and generate shipments & Invoices	Quote-to-Invoice	Y	Y	N	N	N	2008-05-29 23:18:19	0
53223	Order Transactions	R	0	\N	\N	0	53176	\N	2009-06-15 18:37:24	100	Sales Order Transaction Report	Quote-to-Invoice	Y	Y	N	N	N	2009-06-15 18:37:24	100
53249	Order Source	W	0	\N	\N	0	\N	53101	2009-09-18 13:27:41	0	\N	Quote-to-Invoice	N	Y	N	N	N	2019-12-05 14:06:51	100
53979	Shipment Scanner	X	0	53056	\N	0	\N	\N	2015-04-08 13:35:42	100	Customer Shipment using Barcode Scanner	Quote-to-Invoice	N	Y	N	N	N	2019-12-05 14:07:39	100
53982	Shipment Manifest	W	0	\N	\N	0	\N	53414	2015-04-23 18:07:24	100	\N	Quote-to-Invoice	N	Y	N	Y	N	2019-12-05 14:07:31	100
\.
