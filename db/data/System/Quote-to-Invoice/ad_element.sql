copy "ad_element" ("ad_element_id", "columnname", "ad_client_id", "ad_org_id", "ad_reference_id", "ad_reference_value_id", "created", "createdby", "description", "entitytype", "fieldlength", "help", "isactive", "name", "po_description", "po_help", "po_name", "po_printname", "printname", "updated", "updatedby") from STDIN;
264	DateDelivered	0	0	15	\N	1999-11-19 10:07:43	0	Date when the product was delivered	Quote-to-Invoice	7	\N	Y	Date Delivered	\N	\N	\N	\N	Date Delivered	2000-01-02 00:00:00	0
268	DateOrdered	0	0	15	\N	1999-11-19 10:07:43	0	Date of Order	Quote-to-Invoice	7	Indicates the Date an item was ordered.	Y	Date Ordered	\N	\N	\N	\N	Date Ordered	2000-01-02 00:00:00	0
363	IsCreditApproved	0	0	20	\N	1999-11-19 10:07:43	0	Credit  has been approved	Quote-to-Invoice	1	Credit Approved indicates if the credit approval was successful for Orders	Y	Credit Approved	\N	\N	\N	\N	Credit Approved	2000-01-02 00:00:00	0
419	IsTransferred	0	0	20	\N	1999-11-19 10:07:43	0	Transferred to General Ledger (i.e. accounted)	Quote-to-Invoice	1	The transferred checkbox indicates if the transactions associated with this document should be transferred to the General Ledger.	Y	Transferred	\N	\N	\N	\N	Transferred	2016-04-11 16:33:13	0
441	LineNetAmt	0	0	12	\N	1999-11-19 10:07:43	0	Line Extended Amount (Quantity * Actual Price) without Freight and Charges	Quote-to-Invoice	22	Indicates the extended line amount based on the quantity and the actual price.  Any additional charges or freight are not included.  The Amount may or may not include tax.  If the price list is inclusive tax, the line amount is the same as the line total.	Y	Line Amount	\N	\N	\N	\N	Line Amt	2016-04-11 16:31:57	0
528	QtyDelivered	0	0	29	\N	1999-11-19 10:07:43	0	Delivered Quantity	Quote-to-Invoice	22	The Delivered Quantity indicates the quantity of a product that has been delivered.	Y	Delivered Quantity	\N	\N	\N	\N	Delivered Qty	2000-01-02 00:00:00	0
529	QtyInvoiced	0	0	29	\N	1999-11-19 10:07:43	0	Invoiced Quantity	Quote-to-Invoice	22	The Invoiced Quantity indicates the quantity of a product that have been invoiced.	Y	Quantity Invoiced	\N	\N	\N	\N	Invoiced	2005-12-21 17:36:54	100
598	TotalLines	0	0	12	\N	1999-11-19 10:07:43	0	Total of all document lines	Quote-to-Invoice	22	The Total amount displays the total of all lines in document currency	Y	Total Lines	\N	\N	\N	\N	Total Lines	2000-01-02 00:00:00	0
1091	DatePrinted	0	0	15	\N	2000-01-24 18:04:25	0	Date the document was printed.	Quote-to-Invoice	7	Indicates the Date that a document was printed.	Y	Date printed	\N	\N	\N	\N	Date printed	2000-01-02 00:00:00	0
1249	NetAmtToInvoice	0	0	12	\N	2000-05-01 18:53:29	0	Net amount of this Invoice	Quote-to-Invoice	22	Indicates the net amount for this invoice.  It does not include shipping or any additional charges.	Y	Invoice net Amount	\N	\N	\N	\N	Invoice net Amt	2000-01-02 00:00:00	0
1250	QtyToDeliver	0	0	29	\N	2000-05-01 18:53:29	0	\N	Quote-to-Invoice	22	\N	Y	Qty to deliver	\N	\N	\N	\N	Qty to deliver	2000-01-02 00:00:00	0
1251	QtyToInvoice	0	0	29	\N	2000-05-01 18:53:29	0	\N	Quote-to-Invoice	22	\N	Y	Qty to invoice	\N	\N	\N	\N	Qty to invoice	2000-01-02 00:00:00	0
1272	LineDiscount	0	0	22	\N	2000-06-01 14:33:42	0	Line Discount as a percentage	Quote-to-Invoice	22	The Line Discount Percent indicates the discount for this line as a percentage.	Y	Line Discount %	\N	\N	\N	\N	Discount %	2000-01-02 00:00:00	0
1273	LineDiscountAmt	0	0	12	\N	2000-06-01 14:33:42	0	Line Discount Amount	Quote-to-Invoice	22	Indicates the discount for this line as an amount.	Y	Line Discount	\N	\N	\N	\N	Discount	2000-01-02 00:00:00	0
1274	LineLimitAmt	0	0	12	\N	2000-06-01 14:33:42	0	\N	Quote-to-Invoice	22	\N	Y	Line Limit Amount	\N	\N	\N	\N	Limit Amt	2000-01-02 00:00:00	0
1275	LineListAmt	0	0	12	\N	2000-06-01 14:33:42	0	\N	Quote-to-Invoice	22	\N	Y	Line List Amount	\N	\N	\N	\N	List Amt	2000-01-02 00:00:00	0
1276	LineOverLimit	0	0	22	\N	2000-06-01 14:33:42	0	\N	Quote-to-Invoice	22	\N	Y	Gross margin %	\N	\N	\N	\N	Margin %	2000-01-02 00:00:00	0
1277	LineOverLimitAmt	0	0	12	\N	2000-06-01 14:33:42	0	\N	Quote-to-Invoice	22	\N	Y	Gross Margin	\N	\N	\N	\N	Margin	2000-01-02 00:00:00	0
1321	IsSelected	0	0	20	\N	2000-09-15 14:49:49	0	\N	Quote-to-Invoice	1	\N	Y	Selected	\N	\N	\N	\N	Selected	2000-01-02 00:00:00	0
1347	Vendor_ID	0	0	18	192	2000-12-04 14:55:54	0	The Vendor of the product/service	Quote-to-Invoice	22	\N	Y	Vendor	\N	\N	\N	\N	Vendor	2005-02-07 20:52:00	100
1380	C_AllocationHdr_ID	0	0	19	\N	2000-12-17 16:23:12	0	Payment allocation	Quote-to-Invoice	22	\N	Y	Allocation	\N	\N	\N	\N	Allocation	2000-01-02 00:00:00	0
1402	IsPaid	0	0	20	\N	2000-12-17 16:23:13	0	The document is paid	Quote-to-Invoice	1	\N	Y	Paid	\N	\N	\N	\N	Paid	2008-03-23 20:54:39	100
1464	C_CashLine_ID	0	0	30	\N	2000-12-22 22:27:31	0	Cash Journal Line	Quote-to-Invoice	22	The Cash Journal Line indicates a unique line in a cash journal.	Y	Cash Journal Line	\N	\N	\N	\N	Cash Line	2000-01-02 00:00:00	0
1525	Multiplier	0	0	22	\N	2001-01-27 17:32:12	0	Type Multiplier (Credit = -1)	Quote-to-Invoice	22	\N	Y	Multiplier	\N	\N	\N	\N	Multiplier	2000-01-02 00:00:00	0
1528	Margin	0	0	22	\N	2001-01-27 19:14:03	0	Margin for a product as a percentage	Quote-to-Invoice	22	The Margin indicates the margin for this product as a percentage of the limit price and selling price.	Y	Margin %	\N	\N	\N	\N	Margin %	2000-01-02 00:00:00	0
1689	M_MatchInv_ID	0	0	13	\N	2001-11-25 18:51:58	0	Match Shipment/Receipt to Invoice	Quote-to-Invoice	22	\N	Y	Match Invoice	\N	\N	\N	\N	Match Invoice	2000-01-02 00:00:00	0
1690	M_MatchPO_ID	0	0	13	\N	2001-11-25 18:51:58	0	Match Purchase Order to Shipment/Receipt and Invoice	Quote-to-Invoice	22	The matching record is usually created automatically.  If price matching is enabled on business partner group level, the matching might have to be approved.	Y	Match PO	\N	\N	\N	\N	Match PO	2005-11-25 14:59:38	100
1778	S_ResourceAssignment_ID	0	0	33	\N	2002-06-15 21:03:57	0	Resource Assignment	Quote-to-Invoice	22	\N	Y	Resource Assignment	\N	\N	\N	\N	Assignment	2000-01-02 00:00:00	0
1905	Ref_OrderLine_ID	0	0	30	271	2002-12-09 20:27:07	0	Reference to corresponding Sales/Purchase Order	Quote-to-Invoice	22	Reference of the Sales Order Line to the corresponding Purchase Order Line or vice versa.	Y	Referenced Order Line	\N	\N	\N	\N	Ref Order Line	2000-01-02 00:00:00	0
1953	EMailPDF	0	0	\N	\N	2003-01-29 21:30:36	0	Email Document PDF files to Business Partner	Quote-to-Invoice	\N	\N	Y	EMail PDF	\N	\N	\N	\N	EMail PDF	2005-04-30 01:09:32	100
1999	DueAmt	0	0	12	\N	2003-05-04 00:31:52	0	Amount of the payment due	Quote-to-Invoice	22	Full amount of the payment due	Y	Amount due	\N	\N	\N	\N	Amount due	2000-01-02 00:00:00	0
2039	Bill_BPartner_ID	0	0	18	138	2003-05-28 21:40:00	0	Business Partner to be invoiced	Quote-to-Invoice	22	If empty the shipment business partner will be invoiced	Y	Invoice Partner	\N	\N	\N	\N	Invoice Partner	2016-04-11 16:37:04	0
2040	Bill_Location_ID	0	0	18	159	2003-05-28 21:40:00	0	Business Partner Location for invoicing	Quote-to-Invoice	22	\N	Y	Invoice Location	\N	\N	\N	\N	Invoice Location	2000-01-02 00:00:00	0
2041	Bill_User_ID	0	0	18	110	2003-05-28 21:40:00	0	Business Partner Contact for invoicing	Quote-to-Invoice	22	\N	Y	Invoice Contact	\N	\N	\N	\N	Invoice Contact	2000-01-02 00:00:00	0
2106	InvoiceDocumentNo	0	0	10	\N	2003-06-07 19:49:51	0	Document Number of the Invoice	Quote-to-Invoice	30	\N	Y	Invoice Document No	\N	\N	\N	\N	Invoice Document No	2000-01-02 00:00:00	0
2108	LineDescription	0	0	10	\N	2003-06-07 19:49:51	0	Description of the Line	Quote-to-Invoice	255	\N	Y	Line Description	\N	\N	\N	\N	Line Description	2000-01-02 00:00:00	0
2113	NoPackages	0	0	11	\N	2003-06-07 19:49:51	0	Number of packages shipped	Quote-to-Invoice	22	\N	Y	No Packages	\N	\N	\N	\N	No Packages	2000-01-02 00:00:00	0
2117	PickDate	0	0	16	\N	2003-06-07 19:49:51	0	Date/Time when picked for Shipment	Quote-to-Invoice	7	\N	Y	Pick Date	\N	\N	\N	\N	Pick Date	2000-01-02 00:00:00	0
2123	ShipDate	0	0	16	\N	2003-06-07 19:49:51	0	Shipment Date/Time	Quote-to-Invoice	7	Actual Date/Time of Shipment (pick up)	Y	Ship Date	\N	\N	\N	\N	Ship Date	2000-01-02 00:00:00	0
2126	TrackingNo	0	0	10	\N	2003-06-07 19:49:51	0	Number to track the shipment	Quote-to-Invoice	60	\N	Y	Tracking No	\N	\N	\N	\N	Tracking No	2000-01-02 00:00:00	0
2183	IsDescription	0	0	20	\N	2003-09-05 21:04:54	0	if true, the line is just description and no transaction	Quote-to-Invoice	1	If a line is Description Only, e.g. Product Inventory is not corrected. No accounting transactions are created and the amount or totals are not included in the document.  This for including descriptive detail lines, e.g. for an Work Order.	Y	Description Only	\N	\N	\N	\N	Description	2010-01-13 10:38:15	0
2186	IsCloseDocument	0	0	\N	\N	2003-10-04 10:44:19	0	Close Document (process)	Quote-to-Invoice	\N	\N	Y	Close Document	\N	\N	\N	\N	Close	2000-01-02 00:00:00	0
2215	LineTotalAmt	0	0	12	\N	2003-10-07 15:10:01	0	Total line amount incl. Tax	Quote-to-Invoice	22	Total line amount	Y	Line Total	\N	\N	\N	\N	Line Total	2000-01-02 00:00:00	0
2383	C_Subscription_Delivery_ID	0	0	13	\N	2004-02-19 10:36:37	0	Optional Delivery Record for a Subscription	Quote-to-Invoice	22	Record of deliveries for a subscription	Y	Subscription Delivery	\N	\N	\N	\N	Subscription Delivery	2000-01-02 00:00:00	0
2384	C_Subscription_ID	0	0	19	\N	2004-02-19 10:36:37	0	Subscription of a Business Partner of a Product to renew	Quote-to-Invoice	22	Subscription of a Business Partner of a Product to renew	Y	Subscription	\N	\N	\N	\N	Subscription	2000-01-02 00:00:00	0
2387	CreatePO	0	0	28	\N	2004-02-19 10:36:37	0	Create Purchase Order	Quote-to-Invoice	1	\N	Y	Create PO	\N	\N	\N	\N	Create PO	2000-01-02 00:00:00	0
2394	IsDue	0	0	20	\N	2004-02-19 10:36:37	0	Subscription Renewal is Due	Quote-to-Invoice	1	\N	Y	Due	\N	\N	\N	\N	Due	2000-01-02 00:00:00	0
2410	M_Package_ID	0	0	19	\N	2004-02-19 10:36:37	0	Shipment Package	Quote-to-Invoice	22	A Shipment can have one or more Packages.  A Package may be individually tracked.	Y	Package	\N	\N	\N	\N	Package	2000-01-02 00:00:00	0
2411	M_PackageLine_ID	0	0	13	\N	2004-02-19 10:36:37	0	The detail content of the Package	Quote-to-Invoice	22	Link to the shipment line	Y	Package Line	\N	\N	\N	\N	Package Line	2000-01-02 00:00:00	0
2412	M_RMA_ID	0	0	19	\N	2004-02-19 10:36:37	0	Return Material Authorization	Quote-to-Invoice	22	A Return Material Authorization may be required to accept returns and to create Credit Memos	Y	RMA	\N	\N	\N	\N	RMA	2000-01-02 00:00:00	0
2413	M_RMALine_ID	0	0	13	\N	2004-02-19 10:36:37	0	Return Material Authorization Line	Quote-to-Invoice	22	Detail information about the returned goods	Y	RMA Line	\N	\N	\N	\N	RMA Line	2000-01-02 00:00:00	0
2418	PaidUntilDate	0	0	15	\N	2004-02-19 10:36:37	0	Subscription is paid/valid until this date	Quote-to-Invoice	7	\N	Y	Paid Until	\N	\N	\N	\N	Paid Until	2000-01-02 00:00:00	0
2420	Pay_BPartner_ID	0	0	13	\N	2004-02-19 10:36:37	0	Business Partner responsible for the payment	Quote-to-Invoice	22	\N	Y	Payment BPartner	\N	\N	\N	\N	Payment BPartner	2000-01-02 00:00:00	0
2421	Pay_Location_ID	0	0	13	\N	2004-02-19 10:36:37	0	Location of the Business Partner responsible for the payment	Quote-to-Invoice	22	\N	Y	Payment Location	\N	\N	\N	\N	Payment Location	2000-01-02 00:00:00	0
2427	ReceivedInfo	0	0	10	\N	2004-02-19 10:36:37	0	Information of the receipt of the package (acknowledgement)	Quote-to-Invoice	255	\N	Y	Info Received	\N	\N	\N	\N	Info Received	2000-01-02 00:00:00	0
2428	Ref_InOut_ID	0	0	13	\N	2004-02-19 10:36:37	0	\N	Quote-to-Invoice	22	\N	Y	Referenced Shipment	\N	\N	\N	\N	Ref Ship	2000-01-02 00:00:00	0
2429	Ref_Invoice_ID	0	0	13	\N	2004-02-19 10:36:37	0	\N	Quote-to-Invoice	22	\N	Y	Referenced Invoice	\N	\N	\N	\N	Ref Invoice	2000-01-02 00:00:00	0
2430	Ref_InvoiceLine_ID	0	0	13	\N	2004-02-19 10:36:37	0	\N	Quote-to-Invoice	22	\N	Y	Referenced Invoice Line	\N	\N	\N	\N	Ref Invoice Line	2000-01-02 00:00:00	0
2431	Ref_Order_ID	0	0	30	290	2004-02-19 10:36:37	0	Reference to corresponding Sales/Purchase Order	Quote-to-Invoice	22	Reference of the Sales Order Line to the corresponding Purchase Order Line or vice versa.	Y	Referenced Order	\N	\N	\N	\N	Ref Order	2000-01-02 00:00:00	0
2434	RenewalDate	0	0	15	\N	2004-02-19 10:36:37	0	\N	Quote-to-Invoice	7	\N	Y	Renewal Date	\N	\N	\N	\N	Renewal Date	2000-01-02 00:00:00	0
2441	TrackingInfo	0	0	10	\N	2004-02-19 10:36:37	0	\N	Quote-to-Invoice	255	\N	Y	Tracking Info	\N	\N	\N	\N	Tracking Info	2000-01-02 00:00:00	0
2445	Ref_InOutLine_ID	0	0	13	\N	2004-02-19 23:48:47	0	\N	Quote-to-Invoice	22	\N	Y	Referenced Shipment Line	\N	\N	\N	\N	Ref Ship Line	2000-01-02 00:00:00	0
2451	DateRequired	0	0	15	\N	2004-03-11 23:54:41	0	Date when required	Quote-to-Invoice	7	\N	Y	Date Required	\N	\N	\N	\N	Date Required	2000-01-02 00:00:00	0
2452	M_Requisition_ID	0	0	13	\N	2004-03-11 23:54:41	0	Material Requisition	Quote-to-Invoice	22	\N	Y	Requisition	\N	\N	\N	\N	Requisition	2000-01-02 00:00:00	0
2487	TotalQty	0	0	29	\N	2004-04-01 17:21:55	0	Total Quantity	Quote-to-Invoice	22	\N	Y	Total Quantity	\N	\N	\N	\N	Total Qty	2000-01-02 00:00:00	0
2520	CreateConfirm	0	0	28	\N	2004-05-10 17:22:03	0	\N	Quote-to-Invoice	1	\N	Y	Create Confirm	\N	\N	\N	\N	Create Confirm	2000-01-02 00:00:00	0
2588	PriceEntered	0	0	37	\N	2004-07-22 22:19:58	0	Price Entered - the price based on the selected/base UoM	Quote-to-Invoice	22	The price entered is converted to the actual price based on the UoM conversion	Y	Price	\N	\N	\N	\N	Price	2016-04-11 16:31:58	0
2589	QtyEntered	0	0	29	\N	2004-07-22 22:19:58	0	The Quantity Entered is based on the selected UoM	Quote-to-Invoice	22	The Quantity Entered is converted to base product UoM quantity	Y	Quantity	\N	\N	\N	\N	Quantity	2000-01-02 00:00:00	0
2679	ProductAttribute	0	0	10	\N	2005-02-10 17:08:44	0	Product Attribute Instance Description	Quote-to-Invoice	2000	\N	Y	Product Attribute	\N	\N	\N	\N	Product Attribute	2005-02-10 17:10:39	100
2686	C_LandedCost_ID	0	0	13	\N	2005-03-31 15:18:30	0	Landed cost to be allocated to material receipts	Quote-to-Invoice	22	Landed costs allow you to allocate costs to previously received material receipts. Examples are freight, excise tax, insurance, etc.	Y	Landed Cost	\N	\N	\N	\N	Landed Cost	2005-03-31 15:33:31	100
2703	LandedCostDistribution	0	0	17	339	2005-04-24 21:45:12	100	Landed Cost Distribution	Quote-to-Invoice	1	How landed costs are distributed to material receipts	Y	Cost Distribution	\N	\N	\N	\N	Cost Distribution	2005-04-24 21:46:28	100
2819	C_LandedCostAllocation_ID	0	0	13	\N	2005-07-28 13:11:03	100	Allocation for Land Costs	Quote-to-Invoice	10	\N	Y	Landed Cost Allocation	\N	\N	\N	\N	Landed Cost Allocation	2005-07-28 13:12:17	100
2821	PriceCost	0	0	37	\N	2005-07-31 16:06:08	100	Price per Unit of Measure including all indirect costs (Freight, etc.)	Quote-to-Invoice	22	Optional Purchase Order Line cost price.	Y	Cost Price	\N	\N	\N	\N	Cost Price	2005-07-31 16:08:39	100
2826	QtyLostSales	0	0	29	\N	2005-08-27 08:33:31	100	Quantity of potential sales	Quote-to-Invoice	22	When an order is closed and there is a difference between the ordered quantity and the delivered (invoiced) quantity is the Lost Sales Quantity.  Note that the Lost Sales Quantity is 0 if you void an order, so close the order if you want to track lost opportunities.  [Void = data entry error - Close = the order is finished]	Y	Lost Sales Qty	\N	\N	\N	\N	Lost Sales Qty	2005-08-27 08:38:32	100
2827	AmtLostSales	0	0	12	\N	2005-08-27 08:59:37	100	Amount of lost sales in Invoice Currency	Quote-to-Invoice	22	\N	Y	Lost Sales Amt	\N	\N	\N	\N	Lost Sales Amt	2005-08-27 09:00:49	100
2828	MarginAmt	0	0	12	\N	2005-08-27 11:48:01	100	Difference between actual and limit price multiplied by the quantity	Quote-to-Invoice	22	The margin amount is calculated as the difference between actual and limit price multiplied by the quantity	Y	Margin Amount	\N	\N	\N	\N	Margin Amt	2005-08-27 11:50:18	100
2885	PriceMatchDifference	0	0	12	\N	2005-11-25 14:55:01	100	Difference between Purchase and Invoice Price per matched line	Quote-to-Invoice	22	The difference between purchase and invoice price may be used for requiring explicit approval if a Price Match Tolerance is defined on Business Partner Group level.	Y	Price Match Difference	\N	\N	\N	\N	Price Match Difference	2005-11-25 15:05:05	100
3032	RRStartDate	0	0	16	\N	2006-03-26 19:03:18	100	Revenue Recognition Start Date	Quote-to-Invoice	7	The date the revenue recognition starts.	Y	Revenue Recognition Start	\N	\N	\N	\N	RR Start	2010-01-13 10:38:15	100
3033	RRAmt	0	0	12	\N	2006-03-26 19:03:18	100	Revenue Recognition Amount	Quote-to-Invoice	22	The amount for revenue recognition calculation.  If empty, the complete invoice amount is used.  The difference between Revenue Recognition Amount and Invoice Line Net Amount is immediately recognized as revenue.	Y	Revenue Recognition Amt	\N	\N	\N	\N	RR Amt	2006-03-26 19:10:08	100
52020	OrderType	0	0	10	\N	2008-03-26 13:20:01.057	0	Type of Order: MRP records grouped by source (Sales Order, Purchase Order, Distribution Order, Requisition)	Quote-to-Invoice	510	\N	Y	Order Type	\N	\N	\N	\N	Order Type	2010-01-08 19:44:17	0
52021	AmountTendered	0	0	12	\N	2008-03-26 13:20:01.058	0	\N	Quote-to-Invoice	22	\N	Y	AmountTendered	\N	\N	\N	\N	AmountTendered	2008-03-26 13:20:01.058	0
52022	AmountRefunded	0	0	12	\N	2008-03-26 13:20:01.06	0	\N	Quote-to-Invoice	22	\N	Y	AmountRefunded	\N	\N	\N	\N	AmountRefunded	2008-03-26 13:20:01.06	0
53458	DropShip_BPartner_ID	0	0	30	138	2020-06-24 23:12:14.813746	100	Business Partner to ship to	Quote-to-Invoice	22	If empty the business partner will be shipped to.	Y	Drop Shipment Partner	\N	\N	\N	\N	Drop Shipment  Partner	2020-06-24 23:12:14.813746	100
53459	DropShip_Location_ID	0	0	18	159	2020-06-24 23:12:14.813746	100	Business Partner Location for shipping to	Quote-to-Invoice	22	\N	Y	Drop Shipment Location	\N	\N	\N	\N	Drop Shipment Location	2020-06-24 23:12:14.813746	100
53460	DropShip_User_ID	0	0	18	110	2020-06-24 23:12:14.813746	100	Business Partner Contact for drop shipment	Quote-to-Invoice	22	\N	Y	Drop Shipment Contact	\N	\N	\N	\N	Drop Shipment Contact	2020-06-24 23:12:14.813746	100
53462	Link_Order_ID	0	0	18	290	2008-05-08 16:30:19	100	This field links a sales order to the purchase order that is generated from it.	Quote-to-Invoice	22	\N	Y	Linked Order	\N	\N	\N	\N	Linked Order	2008-05-08 16:30:19	100
53463	Link_OrderLine_ID	0	0	18	271	2008-05-08 16:58:29	100	This field links a sales order line to the purchase order line that is generated from it.	Quote-to-Invoice	22	\N	Y	Linked Order Line	\N	\N	\N	\N	Linked Order Line	2008-05-08 16:58:29	100
53641	A_CapvsExp	0	0	17	53277	2008-05-30 17:00:07	100	\N	Quote-to-Invoice	3	\N	Y	Capital vs Expense	\N	\N	\N	\N	Capital vs Expense	2008-11-12 16:32:00	100
53942	C_OrderSource_ID	0	0	13	\N	2009-09-18 13:05:44	0	\N	Quote-to-Invoice	10	\N	Y	Order Source	\N	\N	\N	\N	Order Source	2009-12-12 22:54:58	100
54391	C_OrderPaySchedule_ID	0	0	13	\N	2010-12-08 17:00:44	100	\N	Quote-to-Invoice	22	\N	Y	Order Payment Schedule	\N	\N	\N	\N	Order Payment Schedule	2010-12-08 17:00:44	100
54397	C_CashPlanLine_ID	0	0	13	\N	2010-12-08 21:23:02	100	\N	Quote-to-Invoice	22	\N	Y	Cash Plan Line	\N	\N	\N	\N	Cash Plan Line	2010-12-08 21:23:02	100
55286	C_Opportunity_ID	0	0	13	\N	2011-09-01 21:53:13	100	\N	Quote-to-Invoice	22	\N	Y	Sales Opportunity	\N	\N	\N	\N	Sales Opportunity	2011-09-01 21:53:13	100
55498	JMSqueue	0	0	10	\N	2012-05-24 21:38:40	100	JMS Queue name	Quote-to-Invoice	60	\N	Y	JMS Queue	\N	\N	\N	\N	JMS Queue	2012-05-24 21:38:40	100
55499	JMSurl	0	0	10	\N	2012-05-24 21:39:06	100	Message system URL	Quote-to-Invoice	60	\N	Y	JMS URL	\N	\N	\N	\N	JMS URL	2012-05-24 21:39:06	100
55500	JMSuser	0	0	10	\N	2012-05-24 21:39:35	100	Message system user name	Quote-to-Invoice	60	\N	Y	JMS User	\N	\N	\N	\N	JMS User	2012-05-24 21:39:35	100
55501	JMSpassword	0	0	10	\N	2012-05-24 21:40:00	100	Message system password	Quote-to-Invoice	60	\N	Y	JMS Password	\N	\N	\N	\N	JMS Password	2012-05-24 21:40:00	100
55504	Warehouse_DocType_ID	0	0	18	172	2012-06-07 10:07:52	100	Warehouse Order Document Type	Quote-to-Invoice	10	The Document Type for warehouse orders	Y	Warehouse Document Type	\N	\N	\N	\N	Warehouse Doc Type	2012-06-07 10:07:52	100
55505	RMA_DocType_ID	0	0	18	321	2012-06-07 10:09:12	100	RMA Document Type	Quote-to-Invoice	10	The Document Type for RMA	Y	RMA Document Type	\N	\N	\N	\N	RMA Doc Type	2012-06-07 10:09:12	100
57401	IsOverrideTax	0	0	\N	\N	2014-08-05 20:29:55	100	\N	Quote-to-Invoice	\N	\N	Y	IsOverrideTax	\N	\N	\N	\N	Override Tax	2014-08-05 20:29:55	100
57506	DistributeCost	0	0	\N	\N	2014-10-14 19:44:36	100	\N	Quote-to-Invoice	\N	\N	Y	Distribute Cost	\N	\N	\N	\N	Distribute Cost	2014-10-14 19:44:36	100
57531	Po_Qty_Accrued	0	0	\N	\N	2014-11-03 12:41:45	100	\N	Quote-to-Invoice	\N	\N	Y	Po_Qty_Accrued	\N	\N	\N	\N	Po_Qty_Accrued	2014-11-03 12:41:45	100
57963	ConsignmentNo	0	0	\N	\N	2015-04-08 14:40:55	100	\N	Quote-to-Invoice	\N	\N	Y	AWB/Consignment No	\N	\N	\N	\N	AWB/Consignment No	2015-04-08 14:40:55	100
57964	Holder_InOutLine_ID	0	0	\N	\N	2015-04-08 15:00:56	100	\N	Quote-to-Invoice	\N	\N	Y	Holder_InOutLine_ID	\N	\N	\N	\N	Holder_InOutLine_ID	2015-04-08 15:00:56	100
57966	QtyScanned	0	0	\N	\N	2015-04-08 16:00:08	100	\N	Quote-to-Invoice	\N	\N	Y	Qty Scanned	\N	\N	\N	\N	Qty Scanned	2015-04-08 16:00:08	100
57967	QtyToScan	0	0	\N	\N	2015-04-08 16:01:05	100	\N	Quote-to-Invoice	\N	\N	Y	Qty to Scan	\N	\N	\N	\N	Qty to Scan	2015-04-08 16:01:05	100
57968	IsExcessReceipt	0	0	\N	\N	2015-04-08 16:32:17	100	\N	Quote-to-Invoice	\N	\N	Y	Excess Receipt	\N	\N	\N	\N	Excess Receipt	2015-04-08 16:32:17	100
57970	M_Package_Header_V_ID	0	0	\N	\N	2015-04-10 16:47:05	100	\N	Quote-to-Invoice	\N	\N	Y	Package Header View	\N	\N	\N	\N	Package Header View	2015-04-10 16:47:05	100
57971	ShipNo	0	0	\N	\N	2015-04-10 16:47:15	100	\N	Quote-to-Invoice	\N	\N	Y	Ship Doc No	\N	\N	\N	\N	Ship Doc No	2016-04-11 16:48:16	0
57972	PackageNo	0	0	\N	\N	2015-04-10 16:47:17	100	\N	Quote-to-Invoice	\N	\N	Y	Package No	\N	\N	\N	\N	Package No	2016-04-11 16:48:16	0
57980	M_InOut_Manifest_ID	0	0	\N	\N	2015-04-23 17:47:56	100	\N	Quote-to-Invoice	\N	\N	Y	Shipment Manifest ID	\N	\N	\N	\N	Shipment Manifest ID	2015-04-23 17:47:56	100
57981	M_InOut_ManifestLine_ID	0	0	\N	\N	2015-04-23 17:52:27	100	\N	Quote-to-Invoice	\N	\N	Y	Shipment Manifest Lines ID	\N	\N	\N	\N	Shipment Manifest Lines ID	2015-04-23 17:52:27	100
58013	CreateReceipt	0	0	\N	\N	2015-06-08 14:31:05	100	\N	Quote-to-Invoice	\N	\N	Y	Create Receipt	\N	\N	\N	\N	Create Receipt	2015-06-08 14:31:05	100
58014	NoOfPickSlips	0	0	\N	\N	2015-06-09 12:23:13	100	\N	Quote-to-Invoice	\N	\N	Y	No of Pick Slips	\N	\N	\N	\N	No of Pick Slips	2015-06-09 12:23:13	100
58073	HeaderRef	0	0	\N	\N	2015-07-29 11:25:06	100	\N	Quote-to-Invoice	\N	\N	Y	Header Reference	\N	\N	\N	\N	Header Reference	2015-07-29 11:25:06	100
58074	LineRef	0	0	\N	\N	2015-07-29 11:26:18	100	\N	Quote-to-Invoice	\N	\N	Y	Line Reference	\N	\N	\N	\N	Line Reference	2015-07-29 11:26:18	100
58308	Ext_ConfirmedQty	0	0	\N	\N	2015-09-09 10:47:28	100	Qty Confirmed from External Application	Quote-to-Invoice	\N	To be used mostly on integration to external applications (e.g. Magento, Woo Commerce)	Y	Ext Confirmed Qty	\N	\N	\N	\N	Ext Confirmed Qty	2016-04-11 16:52:29	0
59624	IsCreditHold	0	0	20	\N	2017-10-12 15:06:19	100	Complete the Order but not allowed to ship	Quote-to-Invoice	1	\N	Y	Credit Hold	\N	\N	\N	\N	Credit Hold	2017-10-12 15:06:19	100
59637	ShipmentCreateInvoice	0	0	28	\N	2017-10-24 11:45:00	100	This will create an invoice from this Shipment	Quote-to-Invoice	1	\N	Y	Create Invoice from Shipment	\N	\N	\N	\N	Create Invoice	2017-10-24 11:45:00	100
59644	OverrideCreditHold	0	0	28	\N	2017-10-25 18:00:08	100	\N	Quote-to-Invoice	1	\N	Y	Override Credit Hold	\N	\N	\N	\N	Override Credit Hold	2017-10-25 18:00:08	100
\.
