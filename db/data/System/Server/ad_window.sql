copy "ad_window" ("ad_window_id", "name", "ad_client_id", "ad_color_id", "ad_image_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "isbetafunctionality", "isdefault", "issotrx", "processing", "updated", "updatedby", "windowtype", "winheight", "winwidth") from STDIN;
203	Request Processor	0	\N	\N	0	2001-01-11 17:35:36	0	Define Request Processors	Server	The Request Processor Window allows you to define different processes that you want to occur and the frequency and timing of these processes  A Request Processor can be just for a specific Request Type or for all.	Y	N	N	Y	N	2000-01-02 00:00:00	0	M	\N	\N
305	Scheduler	0	\N	\N	0	2004-02-19 10:53:31	0	Maintain Schedule Processes and Logs	Server	Schedule processes to be executed asynchronously	Y	N	N	Y	N	2000-01-02 00:00:00	0	M	\N	\N
311	Accounting Processor	0	\N	\N	0	2004-02-19 13:00:41	0	Maintain Accounting Processor and Logs	Server	Accounting Processor/Server Parameters and Logs	Y	N	N	Y	N	2000-01-02 00:00:00	0	M	\N	\N
312	Alert Processor	0	\N	\N	0	2004-02-19 13:00:51	0	Maintain Alert Processor/Server Parameter and Logs	Server	Alert Processor/Server Parameter	Y	Y	N	Y	N	2000-01-02 00:00:00	0	M	\N	\N
53063	House Keeping	0	\N	\N	0	2008-09-06 20:12:10	100	\N	Server	\N	Y	N	N	Y	N	2008-09-06 20:12:10	100	M	0	0
\.
