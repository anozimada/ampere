copy "ad_ref_list" ("ad_ref_list_id", "name", "ad_client_id", "ad_org_id", "ad_reference_id", "created", "createdby", "description", "entitytype", "isactive", "updated", "updatedby", "validfrom", "validto", "value") from STDIN;
50001	Local Transfer	0	0	50001	2006-12-11 23:46:20	0	This type of transfer will transfer objects to the same Adempiere instance	Application Packaging	N	2006-12-12 00:07:57	0	\N	\N	L
50002	Remote Transfer	0	0	50001	2006-12-11 23:46:20	0	This type of transfer will transfer objects to another Adempiere installation accessible by network.	Application Packaging	N	2006-12-12 00:07:58	0	\N	\N	R
50003	XML File	0	0	50001	2006-12-11 23:46:20	0	This type of transfer will create a XML file 	Application Packaging	Y	2006-12-12 00:07:59	0	\N	\N	X
50004	Release 2.5.2a	0	0	50002	2006-12-11 23:46:22	0	Release 2.5.2a	Application Packaging	Y	2006-12-12 00:11:12	0	\N	\N	Release 2.5.2a
50005	Release 2.5.2b	0	0	50002	2006-12-11 23:46:22	0	Release 2.5.2b	Application Packaging	Y	2006-12-12 00:11:13	0	\N	\N	Release 2.5.2b
50006	Release 2.5.2c	0	0	50002	2006-12-11 23:46:23	0	Release 2.5.2c	Application Packaging	Y	2006-12-12 00:11:14	0	\N	\N	Release 2.5.2c
50007	Release 2.5.2d	0	0	50002	2006-12-11 23:46:23	0	Release 2.5.2d	Application Packaging	Y	2006-12-12 00:11:18	0	\N	\N	Release 2.5.2d
50008	Release 2.5.2e	0	0	50002	2006-12-11 23:46:23	0	Release 2.5.2e	Application Packaging	Y	2006-12-12 00:11:19	0	\N	\N	Release 2.5.2e
50009	Release 2.5.3a	0	0	50002	2006-12-11 23:46:23	0	Release 2.5.3a	Application Packaging	Y	2006-12-12 00:11:20	0	\N	\N	Release 2.5.3a
50010	Release 2.5.3b	0	0	50002	2006-12-11 23:46:23	0	Release 2.5.3b	Application Packaging	Y	2006-12-12 00:11:21	0	\N	\N	Release 2.5.3b
50011	No specific release	0	0	50002	2006-12-11 23:46:23	0	No specific release	Application Packaging	Y	2006-12-12 00:11:24	0	\N	\N	all
50020	Workbench	0	0	50004	2006-12-11 23:46:43	0	\N	Application Packaging	Y	2006-12-12 00:14:09	0	\N	\N	B
50021	File - Code or other	0	0	50004	2006-12-11 23:46:43	0	\N	Application Packaging	Y	2006-12-12 00:14:10	0	\N	\N	C
50022	Data	0	0	50004	2006-12-11 23:46:43	0	\N	Application Packaging	Y	2006-12-12 00:14:11	0	\N	\N	D
50023	Workflow	0	0	50004	2006-12-11 23:46:43	0	\N	Application Packaging	Y	2006-12-12 00:14:12	0	\N	\N	F
50024	Import Format	0	0	50004	2006-12-11 23:46:44	0	Exports Import Format	Application Packaging	Y	2006-12-12 00:14:13	0	\N	\N	IMP
50025	Application or Module	0	0	50004	2006-12-11 23:46:44	0	\N	Application Packaging	Y	2006-12-12 00:14:14	0	\N	\N	M
50026	Process/Report	0	0	50004	2006-12-11 23:46:44	0	\N	Application Packaging	Y	2006-12-12 00:14:14	0	\N	\N	P
50027	ReportView	0	0	50004	2006-12-11 23:46:44	0	\N	Application Packaging	Y	2006-12-12 00:14:15	0	\N	\N	R
50028	Role	0	0	50004	2006-12-11 23:46:44	0	\N	Application Packaging	Y	2006-12-12 00:14:16	0	\N	\N	S
50029	Code Snipit	0	0	50004	2006-12-11 23:46:44	0	Replace a code snipit with in a file	Application Packaging	Y	2006-12-12 00:14:17	0	\N	\N	SNI
50030	SQL Statement	0	0	50004	2006-12-11 23:46:44	0	\N	Application Packaging	Y	2006-12-12 00:14:18	0	\N	\N	SQL
50031	Table	0	0	50004	2006-12-11 23:46:44	0	\N	Application Packaging	Y	2006-12-12 00:14:19	0	\N	\N	T
50032	Window	0	0	50004	2006-12-11 23:46:45	0	\N	Application Packaging	Y	2006-12-12 00:14:20	0	\N	\N	W
50033	Form	0	0	50004	2006-12-11 23:46:45	0	\N	Application Packaging	Y	2006-12-12 00:14:21	0	\N	\N	X
50034	File	0	0	50005	2006-12-11 23:47:44	0	Package is file based	Application Packaging	Y	2006-12-12 00:16:28	0	\N	\N	File
50035	WebService	0	0	50005	2006-12-11 23:47:44	0	Package is available via WebService	Application Packaging	N	2006-12-12 00:16:29	0	\N	\N	WS
50041	Dynamic Validation Rule	0	0	50004	2007-04-26 00:00:00	100	\N	Application Packaging	Y	2007-04-26 00:00:00	100	\N	\N	V
50043	Message	0	0	50004	2007-05-14 19:54:20	100	\N	Application Packaging	Y	2007-05-14 19:54:20	100	\N	\N	MSG
50044	PrintFormat	0	0	50004	2007-05-14 19:54:20	100	\N	Application Packaging	Y	2007-05-14 19:54:20	100	\N	\N	PFT
53227	Reference	0	0	50004	2007-12-08 21:05:21	100	\N	Application Packaging	Y	2007-12-08 21:05:21	100	\N	\N	REF
53324	Release 3.1.0	0	0	50002	2008-02-15 15:05:46	100	Release 3.1.0	Application Packaging	Y	2008-02-15 15:07:21	100	\N	\N	Release 3.1.0
53325	Release 3.2.0	0	0	50002	2008-02-15 15:06:06	100	Release 3.2.0	Application Packaging	Y	2008-02-15 15:07:30	100	\N	\N	Release 3.2.0
53326	Release 3.3.0	0	0	50002	2008-02-15 15:06:36	100	Release 3.3.0	Application Packaging	Y	2008-02-15 15:07:39	100	\N	\N	Release 3.3.0
53504	Model Validator	0	0	50004	2009-08-31 14:28:33	0	\N	Application Packaging	Y	2009-08-31 14:28:33	0	\N	\N	MV
53505	Entity Type	0	0	50004	2009-08-31 15:38:40	0	\N	Application Packaging	Y	2009-08-31 15:38:40	0	\N	\N	ET
\.
