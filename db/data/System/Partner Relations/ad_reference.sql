copy "ad_reference" ("ad_reference_id", "name", "validationtype", "ad_client_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "isorderbyvalue", "updated", "updatedby", "vformat") from STDIN;
124	C_BPartner Parent	T	0	0	1999-06-23 00:00:00	0	Business Partner Parent selection	Partner Relations	\N	Y	N	2000-01-02 00:00:00	0	\N
149	C_Payment CreditCard Type	L	0	0	1999-08-05 00:00:00	0	C_Payment Credit Card Type list	Partner Relations	\N	Y	N	2019-02-04 13:38:36	0	A
150	C_Order InvoiceRule	L	0	0	1999-08-06 00:00:00	0	Invoicing Rules list	Partner Relations	\N	Y	N	2019-02-04 13:38:37	0	\N
151	C_Order DeliveryRule	L	0	0	1999-08-06 00:00:00	0	Delivery Rules list	Partner Relations	\N	Y	N	2019-02-04 13:38:37	0	\N
152	C_Order DeliveryViaRule	L	0	0	1999-08-06 00:00:00	0	Delivery via Rule list	Partner Relations	\N	Y	N	2008-03-23 20:47:57	100	\N
153	C_Order FreightCostRule	L	0	0	1999-08-06 00:00:00	0	Freight cost Rule list	Partner Relations	\N	Y	N	2008-03-23 20:47:41	100	\N
159	C_BPartner Location	T	0	0	1999-08-09 00:00:00	0	Locations of a Business Partner	Partner Relations	\N	Y	N	2000-01-02 00:00:00	0	\N
166	M_PriceList	T	0	0	1999-08-10 00:00:00	0	\N	Partner Relations	\N	Y	N	2008-03-23 20:47:48	100	\N
168	C_InvoiceSchedule InvoiceFrequency	L	0	0	1999-08-10 00:00:00	0	\N	Partner Relations	\N	Y	N	2000-01-02 00:00:00	0	\N
196	C_RevenueRecognition Frequency	L	0	0	2000-02-01 22:04:51	0	Frequency of Revenue Recognition	Partner Relations	\N	Y	N	2000-01-02 00:00:00	0	\N
203	M_EDI Trx Type	L	0	0	2000-06-01 17:59:03	0	\N	Partner Relations	\N	Y	N	2000-01-02 00:00:00	0	\N
213	C_Payment AVS	L	0	0	2000-12-17 20:35:27	0	\N	Partner Relations	\N	Y	N	2019-02-04 13:38:36	0	\N
214	C_Payment Tender Type	L	0	0	2000-12-17 20:37:36	0	Direct Debit/Deposit - Check - CC	Partner Relations	\N	Y	N	2019-02-04 13:38:37	0	\N
215	C_Payment Trx Type	L	0	0	2000-12-17 20:39:43	0	CC Trx Type	Partner Relations	\N	Y	N	2019-02-04 13:38:36	0	\N
227	C_PaymentTerm	T	0	0	2001-04-09 14:43:06	0	\N	Partner Relations	\N	Y	N	2008-03-23 20:47:32	100	\N
249	M_DiscountSchema	T	0	0	2002-01-21 21:19:42	0	\N	Partner Relations	\N	Y	N	2000-01-02 00:00:00	0	\N
289	C_BPartner SOCreditStatus	L	0	0	2003-09-03 10:57:43	0	Sales Credit Status	Partner Relations	\N	Y	N	2008-03-23 20:47:24	100	\N
325	M_DiscountSchema not PL	T	0	0	2004-06-14 23:21:23	0	Not Price List Discount Schema	Partner Relations	\N	Y	N	2008-03-23 20:47:27	100	\N
350	C_BP_Group PriorityBase	L	0	0	2005-05-17 23:31:45	100	\N	Partner Relations	\N	Y	N	2005-05-17 23:34:07	100	\N
356	C_Greeting	T	0	0	2005-08-27 09:55:47	100	\N	Partner Relations	\N	Y	N	2005-08-27 09:55:47	100	\N
358	AD_User BP AccessType	L	0	0	2005-09-09 14:50:53	100	\N	Partner Relations	\N	Y	N	2005-09-09 14:50:53	100	\N
393	C_BPartner BPBankAcctUse	L	0	0	2006-10-28 00:00:00	0	\N	Partner Relations	\N	Y	N	2008-03-23 20:50:42	100	\N
394	C_Invoice InvoiceCollectionType	L	0	0	2006-10-28 00:00:00	0	\N	Partner Relations	\N	Y	N	2006-10-28 00:00:00	0	\N
53382	C_PaymentTerm PaymentTermUsage	L	0	0	2010-12-08 19:15:46	100	\N	Partner Relations	\N	Y	N	2010-12-08 19:15:46	100	\N
53383	C_PaymentTerm Sales	T	0	0	2010-12-08 19:24:33	100	\N	Partner Relations	\N	Y	N	2010-12-08 19:24:33	100	\N
53384	C_PaymentTerm Purchases	T	0	0	2010-12-08 19:26:28	100	\N	Partner Relations	\N	Y	N	2010-12-08 19:26:28	100	\N
53410	PriceOverrideType	L	0	0	2011-07-08 14:43:41	100	Price Override Type	Partner Relations	Fixed price or discount off list	Y	N	2011-07-08 14:43:41	100	\N
\.
