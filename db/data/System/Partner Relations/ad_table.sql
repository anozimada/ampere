copy "ad_table" ("ad_table_id", "tablename", "name", "accesslevel", "ad_client_id", "ad_org_id", "ad_val_rule_id", "ad_window_id", "copycolumnsfromtable", "created", "createdby", "description", "entitytype", "help", "importtable", "isactive", "iscentrallymaintained", "ischangelog", "isdeleteable", "ishighvolume", "issecurityenabled", "isview", "loadseq", "po_window_id", "replicationtype", "sqlstatement", "updated", "updatedby") from STDIN;
113	C_PaymentTerm	Payment Term	3	0	0	\N	141	\N	1999-08-08 00:00:00	0	The terms for Payment of this transaction	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	65	\N	L	\N	2000-01-02 00:00:00	0
183	C_BP_Customer_Acct	C_BP_Customer_Acct	3	0	0	\N	123	\N	1999-06-03 00:00:00	0	\N	Partner Relations	\N	\N	Y	Y	N	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
184	C_BP_Employee_Acct	C_BP_Employee_Acct	3	0	0	\N	123	\N	1999-06-03 00:00:00	0	\N	Partner Relations	\N	\N	Y	Y	N	N	N	N	N	145	\N	L	\N	2008-03-23 20:50:08	100
185	C_BP_Vendor_Acct	C_BP_Vendor_Acct	3	0	0	\N	123	\N	1999-06-03 00:00:00	0	\N	Partner Relations	\N	\N	Y	Y	N	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
257	C_InvoiceSchedule	Invoice Schedule	3	0	0	\N	147	\N	1999-08-08 00:00:00	0	Schedule for generating Invoices	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	65	\N	L	\N	2000-01-02 00:00:00	0
291	C_BPartner	Business Partner 	3	0	0	\N	123	\N	1999-11-10 00:00:00	0	Identifies a Business Partner	Partner Relations	\N	\N	Y	Y	N	Y	Y	N	N	70	\N	L	\N	2008-03-23 20:47:20	100
293	C_BPartner_Location	Partner Location	3	0	0	\N	123	\N	1999-11-10 00:00:00	0	Identifies the (ship to) address for this Business Partner	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	75	\N	L	\N	2008-03-23 20:51:35	100
298	C_BP_BankAccount	Partner Bank Account	3	0	0	\N	123	\N	1999-12-04 19:50:17	0	Bank Account of the Business Partner	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	100	\N	L	\N	2008-03-23 20:50:29	100
299	C_BP_Withholding	C_BP_Withholding	3	0	0	\N	123	\N	1999-12-04 19:50:17	0	\N	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
301	C_Dunning	Dunning	3	0	0	\N	159	\N	1999-12-04 19:50:17	0	Dunning Rules for overdue invoices	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	65	\N	L	\N	2000-01-02 00:00:00	0
303	C_PaymentTerm_Trl	Payment Term Trl	3	0	0	\N	141	\N	1999-12-04 19:50:17	0	The terms of Payment (timing, discount)	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
304	C_Withholding	Withholding	3	0	0	\N	160	\N	1999-12-04 19:50:17	0	Withholding type defined	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
331	C_DunningLevel	Dunning Level	3	0	0	\N	159	\N	2000-01-24 17:03:25	0	\N	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	130	\N	L	\N	2000-01-02 00:00:00	0
332	C_DunningLevel_Trl	Dunning Level Trl	3	0	0	\N	159	\N	2000-01-24 17:03:25	0	\N	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
336	C_RevenueRecognition	Revenue Recognition	3	0	0	\N	174	\N	2000-01-24 17:03:25	0	Method for recording revenue	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	85	\N	L	\N	2000-01-02 00:00:00	0
346	C_Greeting	Greeting	3	0	0	\N	178	\N	2000-03-19 08:35:31	0	Greeting to print on correspondence	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	65	\N	L	\N	2000-01-02 00:00:00	0
347	C_Greeting_Trl	Greeting Trl	3	0	0	\N	178	\N	2000-03-19 08:35:31	0	Greeting to print on correspondence	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
394	C_BP_Group	Business Partner Group	3	0	0	\N	192	\N	2000-12-17 16:19:52	0	Business Partner Group	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	65	\N	L	\N	2000-01-02 00:00:00	0
395	C_BP_Group_Acct	Business Partner Group Acct	3	0	0	\N	192	\N	2000-12-17 16:19:52	0	\N	Partner Relations	\N	\N	Y	Y	N	N	N	N	N	145	\N	L	\N	2005-11-25 12:59:54	100
400	C_Withholding_Acct	C_Withholding_Acct	3	0	0	\N	160	\N	2000-12-17 16:19:52	0	\N	Partner Relations	\N	\N	Y	Y	N	N	N	N	N	145	\N	L	\N	2005-09-24 09:25:08	100
413	RV_OpenItem	Open Item	3	0	0	\N	\N	\N	2001-01-03 22:27:55	0	\N	Partner Relations	\N	\N	Y	Y	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:56:59	100
416	R_MailText	Mail Template	7	0	0	\N	204	\N	2001-01-11 17:01:17	0	Text templates for mailings	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	85	\N	L	\N	2005-11-13 11:49:14	100
443	C_RevenueRecognition_Plan	Revenue Recognition Plan	1	0	0	\N	174	\N	2001-05-09 21:18:36	0	Plan for recognizing or recording revenue	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2000-01-02 00:00:00	0
444	C_RevenueRecognition_Run	Revenue Recognition Run	1	0	0	\N	174	\N	2001-05-09 21:18:36	0	Revenue Recognition Run or Process	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
520	RV_BPartner	BPartner	3	0	0	\N	\N	\N	2002-08-16 20:29:17	0	\N	Partner Relations	\N	\N	Y	Y	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:53:38	100
528	R_ContactInterest	R_ContactInterest	3	0	0	\N	245	\N	2002-10-01 22:47:05	0	\N	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2008-03-23 20:44:58	100
530	R_InterestArea	Interest Area	2	0	0	\N	245	\N	2002-10-01 22:47:05	0	Interest Area or Topic	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
533	I_BPartner	Import Business Partner	2	0	0	\N	172	\N	2003-01-11 14:57:27	0	\N	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
548	C_PaySchedule	Payment Schedule	3	0	0	\N	141	\N	2003-05-04 00:03:42	0	Payment Schedule Template	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
678	C_BP_Relation	Partner Relation	2	0	0	\N	313	\N	2004-02-19 10:29:53	0	Business Partner Relation	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
753	T_TrialBalance	Trial Balance	3	0	0	\N	\N	\N	2004-10-08 01:48:50	0	\N	Partner Relations	\N	\N	Y	Y	N	Y	N	N	Y	150	\N	L	\N	2000-01-02 00:00:00	0
755	RV_Payment	Payment	3	0	0	\N	\N	\N	2005-02-07 21:53:48	0	\N	Partner Relations	\N	\N	Y	Y	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:57:05	100
756	RV_BPartnerOpen	BPartnerOpen	3	0	0	\N	\N	\N	2005-02-11 23:46:36	0	\N	Partner Relations	\N	\N	Y	Y	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:53:43	100
813	AD_UserBPAccess	User BP Access	2	0	0	\N	123	\N	2005-09-09 14:38:28	100	User/concat access to Business Partner information and resources	Partner Relations	\N	N	Y	Y	N	N	N	N	N	145	\N	L	\N	2005-11-09 14:23:25	100
826	R_MailText_Trl	Mail Template Trl	7	0	0	\N	204	\N	2005-11-13 12:34:09	100	Text templates for mailings	Partner Relations	\N	N	Y	Y	N	N	N	N	N	145	\N	L	\N	2005-11-13 12:36:01	100
53325	M_BP_Price	Business Partner Price	3	0	0	\N	123	N	2011-07-08 14:31:36	100	Business Partner specific pricing that will override the price list	Partner Relations	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2011-07-08 14:31:36	100
53336	AD_Memo	Memo	6	0	0	\N	53151	N	2011-08-29 16:05:43	100	\N	Partner Relations	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2011-09-05 15:31:49	100
53888	C_RevenueRecog_Service	Revenue Recognition Service	1	0	0	\N	174	\N	2014-10-28 11:39:41	100	Plan for recognizing or recording revenue based on service	Partner Relations	\N	\N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2016-04-11 16:41:18	0
\.
