copy "ad_table" ("ad_table_id", "tablename", "name", "accesslevel", "ad_client_id", "ad_org_id", "ad_val_rule_id", "ad_window_id", "copycolumnsfromtable", "created", "createdby", "description", "entitytype", "help", "importtable", "isactive", "iscentrallymaintained", "ischangelog", "isdeleteable", "ishighvolume", "issecurityenabled", "isview", "loadseq", "po_window_id", "replicationtype", "sqlstatement", "updated", "updatedby") from STDIN;
541	A_Asset_Delivery	Asset Delivery	3	0	0	\N	251	\N	2003-01-23 00:08:48	0	Delivery of Asset	Assets	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
621	RV_Asset_Delivery	Asset Delivery	3	0	0	\N	\N	\N	2003-08-28 16:47:17	0	\N	Assets	\N	\N	Y	Y	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:53:26	100
622	RV_Asset_Customer	Asset Customer	3	0	0	\N	\N	\N	2003-08-28 16:47:17	0	\N	Assets	\N	\N	Y	Y	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:53:20	100
651	A_Registration	Registration	3	0	0	\N	301	\N	2004-01-08 20:59:39	0	User Asset Registration	Assets	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
652	A_RegistrationAttribute	Registration Attribute	2	0	0	\N	300	\N	2004-01-08 20:59:40	0	Asset Registration Attribute	Assets	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
653	A_RegistrationValue	A_RegistrationValue	3	0	0	\N	301	\N	2004-01-08 20:59:40	0	\N	Assets	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
715	A_RegistrationProduct	Registration Product	2	0	0	\N	300	\N	2004-04-10 10:24:19	0	\N	Assets	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2005-05-15 13:24:38	100
716	RV_Asset_SumMonth	Asset SumMonth	3	0	0	\N	\N	\N	2004-04-13 11:11:58	0	\N	Assets	\N	\N	Y	Y	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:53:33	100
53112	A_Depreciation	Depreciation Type	7	0	0	\N	\N	\N	2008-05-30 16:35:03	100	\N	Assets	This table holds the all the depreciation functions currently available in compiere	N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2008-05-30 16:35:03	100
53113	A_Depreciation_Table_Detail	Depreciation Table Detail	7	0	0	\N	\N	\N	2008-05-30 16:35:19	100	\N	Assets	\N	N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2009-12-12 15:31:23	100
53114	A_Depreciation_Table_Header	Depreciation Table Header	7	0	0	\N	\N	\N	2008-05-30 16:35:40	100	\N	Assets	\N	N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2008-05-30 16:35:40	100
53115	A_Depreciation_Exp	Depreciation Exp.	7	0	0	\N	\N	\N	2008-05-30 16:35:56	100	\N	Assets	This table is the worktable for generating all fixed asset entries and forecasts	N	Y	Y	N	N	N	N	N	\N	\N	L	\N	2008-05-30 16:35:56	100
53116	A_Depreciation_Workfile	Depreciation Workfile	7	0	0	\N	251	\N	2008-05-30 16:36:24	100	\N	Assets	This table holds the Cost and Depreciation Balances for each fixed asset	N	Y	Y	N	N	N	N	N	\N	\N	L	\N	2008-05-30 16:36:24	100
53119	A_Asset_Reval_Entry	Asset Reval. Entry	7	0	0	\N	53044	\N	2008-05-30 16:38:41	100	\N	Assets	This table holds all necessary information to process asset revaluations	N	Y	Y	N	N	N	N	N	\N	\N	L	\N	2008-05-30 16:38:41	100
53120	A_Asset_Reval_Index	Asset Reval Index	7	0	0	\N	53045	\N	2008-05-30 16:39:38	100	\N	Assets	This table hold all index and factor information for asset revaluations	N	Y	Y	N	N	N	N	N	\N	\N	L	\N	2008-05-30 16:39:38	100
53121	A_Depreciation_Entry	Depreciation Entry	7	0	0	\N	\N	\N	2008-05-30 16:40:01	100	\N	Assets	This table hold the necessary information to process the depreciation entry	N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2008-05-30 16:40:01	100
53122	A_Asset_Split	Asset Split	7	0	0	\N	53048	\N	2008-05-30 16:41:53	100	\N	Assets	This table holds all necessary information to process asset splits	N	Y	Y	N	N	N	N	N	\N	\N	L	\N	2008-05-30 16:41:53	100
53123	A_Asset_Acct	Asset Acct.	7	0	0	\N	53272	\N	2008-05-30 16:43:50	100	\N	Assets	This table holds the accounting setup for each fixed asset.  Settings include account numbers to be used in depreciation entries as well depreciation calculation methods	N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2016-04-11 16:36:20	0
53124	A_Depreciation_Method	Depreciation Calculation Type	7	0	0	\N	\N	\N	2008-05-30 16:44:27	100	\N	Assets	This table holds each method available in Adempiere in handling depreciation adjustments to fixed asset balances	N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2008-05-30 16:52:31	100
53125	A_Depreciation_Convention	Depreciation Convention	7	0	0	\N	\N	\N	2008-05-30 16:44:33	100	\N	Assets	This table contains what depreciation convention types are available in Adempiere	N	Y	Y	N	N	N	N	N	\N	\N	L	\N	2008-05-30 16:52:05	100
53126	A_Asset_Spread	Asset Spread	7	0	0	\N	\N	\N	2008-05-30 16:44:38	100	\N	Assets	This table holds the user defined spread of depreciation by period	N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2008-05-30 16:51:10	100
53127	A_Asset_Disposed	Disposed Asset	7	0	0	\N	53049	\N	2008-05-30 16:45:32	100	\N	Assets	\N	N	Y	Y	N	N	N	N	N	\N	\N	L	\N	2008-05-30 16:45:32	100
53128	A_Asset_Transfer	Asset Transfer	7	0	0	\N	53050	\N	2008-05-30 16:46:41	100	\N	Assets	This table holds all necessary information to process asset transfers and create the appropriate entry	N	Y	Y	N	N	N	N	N	\N	\N	L	\N	2008-05-30 16:46:41	100
53129	A_Depreciation_Build	Depreciation Build	7	0	0	\N	53052	\N	2008-05-30 16:49:22	100	\N	Assets	This table contains the necessary to create and process depreciation projections prior to posting to the GL	N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2008-05-30 16:49:22	100
53130	A_Asset_Group_Acct	Asset Group Acct.	7	0	0	\N	252	\N	2008-05-30 16:52:52	100	\N	Assets	This table holds the accounting setup for each fixed asset group.  Settings include account numbers to be used in depreciation entries as well depreciation calculation methods	N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2008-05-30 16:52:55	100
53133	A_Asset_Change	Asset Change	7	0	0	\N	251	\N	2008-05-30 16:55:51	100	\N	Assets	\N	N	Y	Y	N	N	N	N	N	\N	\N	L	\N	2008-05-30 16:55:51	100
53137	A_Asset_Addition	Asset Addition	7	0	0	\N	251	\N	2008-05-30 16:59:44	100	\N	Assets	\N	N	Y	Y	N	N	N	N	N	\N	\N	L	\N	2008-05-30 16:59:44	100
53138	A_Asset_Use	Asset Use	7	0	0	\N	\N	\N	2008-05-30 17:00:20	100	\N	Assets	\N	N	Y	Y	N	N	N	N	N	\N	\N	L	\N	2008-05-30 17:00:20	100
53139	I_Asset	Asset	7	0	0	\N	53056	\N	2008-05-30 17:01:44	100	\N	Assets	\N	N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2008-05-30 17:01:44	100
\.
