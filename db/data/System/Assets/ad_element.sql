copy "ad_element" ("ad_element_id", "columnname", "ad_client_id", "ad_org_id", "ad_reference_id", "ad_reference_value_id", "created", "createdby", "description", "entitytype", "fieldlength", "help", "isactive", "name", "po_description", "po_help", "po_name", "po_printname", "printname", "updated", "updatedby") from STDIN;
1928	A_Asset_Delivery_ID	0	0	13	\N	2003-01-23 00:10:32	0	Delivery of Asset	Assets	22	The availability of the asset to the business partner (customer).	Y	Asset Delivery	\N	\N	\N	\N	Asset Delivery	2000-01-02 00:00:00	0
1930	A_Asset_Retirement_ID	0	0	13	\N	2003-01-23 00:10:32	0	Internally used asset is not longer used.	Assets	22	\N	Y	Asset Retirement	\N	\N	\N	\N	Asset Retirement	2000-01-02 00:00:00	0
1933	AssetMarketValueAmt	0	0	12	\N	2003-01-23 00:10:32	0	Market value of the asset	Assets	22	For reporting, the market value of the asset	Y	Market value Amount	\N	\N	\N	\N	Market value amt	2000-01-02 00:00:00	0
1935	AssetValueAmt	0	0	12	\N	2003-01-23 00:10:32	0	Book Value of the asset	Assets	22	\N	Y	Asset value	\N	\N	\N	\N	Asset value	2000-01-02 00:00:00	0
1980	NoGuarantee_MailText_ID	0	0	\N	\N	2003-02-05 14:40:34	0	Send email to partners with expired guarantee	Assets	\N	\N	Y	Expired Guarantee Mail	\N	\N	\N	\N	Expired Guarantee Mail	2000-01-02 00:00:00	0
2172	AttachAsset	0	0	\N	\N	2003-08-27 23:37:51	0	Attach Asset to be delivered per email	Assets	\N	\N	Y	Attach Asset	\N	\N	\N	\N	Attach Asset	2000-01-02 00:00:00	0
2173	DeliveryCount	0	0	11	\N	2003-08-28 16:48:01	0	Number of Deliveries	Assets	22	\N	Y	Delivery Count	\N	\N	\N	\N	Delivery Count	2000-01-02 00:00:00	0
2339	A_Registration_ID	0	0	13	\N	2004-01-08 20:59:52	0	User Asset Registration	Assets	22	User Registration of an Asset	Y	Registration	\N	\N	\N	\N	Registration	2000-01-02 00:00:00	0
2340	A_RegistrationAttribute_ID	0	0	13	\N	2004-01-08 20:59:52	0	Asset Registration Attribute	Assets	22	Define the individual values for the Asset Registration	Y	Registration Attribute	\N	\N	\N	\N	Registration Attribute	2000-01-02 00:00:00	0
2437	TextDetails	0	0	14	\N	2004-02-19 10:36:37	0	\N	Assets	4000	\N	Y	Details	\N	\N	\N	\N	Details	2000-01-02 00:00:00	0
53474	DepreciationType	0	0	10	\N	2008-05-30 16:35:12	100	\N	Assets	10	\N	Y	DepreciationType	\N	\N	\N	\N	DepreciationType	2008-05-30 16:35:12	100
53476	A_Depreciation_Table_Detail_ID	0	0	13	\N	2008-05-30 16:35:21	100	\N	Assets	22	\N	Y	Depreciation Table Detail	\N	\N	\N	\N	Depreciation Table Detail	2009-12-12 15:32:16	100
53477	A_Period	0	0	11	\N	2008-05-30 16:35:25	100	\N	Assets	22	\N	Y	Period/Yearly	\N	\N	\N	\N	Period/Yearly	2008-11-12 16:47:06	100
53478	A_Table_Rate_Type	0	0	17	53255	2008-05-30 16:35:34	100	\N	Assets	2	\N	Y	Type	\N	\N	\N	\N	Type	2008-11-12 16:46:41	100
53479	A_Depreciation_Rate	0	0	22	\N	2008-05-30 16:35:37	100	\N	Assets	22	\N	Y	Rate	\N	\N	\N	\N	Rate	2008-11-12 16:47:32	100
53480	A_Depreciation_Table_Code	0	0	10	\N	2008-05-30 16:35:38	100	\N	Assets	20	\N	Y	Depreciation Code	\N	\N	\N	\N	Depreciation Code	2008-11-12 16:46:11	100
53481	A_Depreciation_Table_Header_ID	0	0	13	\N	2008-05-30 16:35:41	100	\N	Assets	22	\N	Y	Depreciation Table Header	\N	\N	\N	\N	Depreciation Table Header	2010-03-29 16:15:13	100
53482	A_Term	0	0	17	53256	2008-05-30 16:35:46	100	\N	Assets	2	\N	Y	Period/Yearly	\N	\N	\N	\N	Period/Yearly	2008-11-12 16:46:27	100
53483	A_Depreciation_Exp_ID	0	0	11	\N	2008-05-30 16:35:57	100	\N	Assets	22	\N	Y	Depreciation Exp.	\N	\N	\N	\N	Depreciation Exp.	2010-03-29 16:13:34	100
53484	A_Entry_Type	0	0	17	53257	2008-05-30 16:36:06	100	\N	Assets	3	\N	Y	Entry Type	\N	\N	\N	\N	Entry Type	2008-11-12 16:45:04	100
53485	Expense	0	0	12	\N	2008-05-30 16:36:10	100	\N	Assets	22	\N	Y	Expense	\N	\N	\N	\N	Expense	2008-05-30 16:36:10	100
53486	A_Account_Number	0	0	25	\N	2008-05-30 16:36:22	100	\N	Assets	22	\N	Y	Account Number	\N	\N	\N	\N	Account Number	2010-03-29 16:02:47	100
53487	A_Depreciation_Workfile_ID	0	0	13	\N	2008-05-30 16:36:26	100	\N	Assets	22	\N	Y	Depreciation Workfile	\N	\N	\N	\N	Depreciation Workfile	2010-03-29 16:15:39	100
53488	A_Accumulated_Depr	0	0	12	\N	2008-05-30 16:36:30	100	\N	Assets	22	\N	Y	Accumulated Depreciation	\N	\N	\N	\N	Accumulated Depreciation	2010-03-29 16:03:14	100
53489	A_Asset_Cost	0	0	12	\N	2008-05-30 16:36:32	100	\N	Assets	22	\N	Y	Asset Cost	\N	\N	\N	\N	Asset Cost	2010-03-29 16:04:45	100
53490	A_Asset_Life_Current_Year	0	0	12	\N	2008-05-30 16:36:34	100	\N	Assets	22	\N	Y	Asset Life Current Year	\N	\N	\N	\N	Asset Life Current Year	2010-03-29 16:08:25	100
53491	A_Period_Forecast	0	0	22	\N	2008-05-30 16:36:35	100	\N	Assets	10	\N	Y	Forecast Period	\N	\N	\N	\N	Forecast Period	2010-03-29 16:28:36	100
53492	A_Prior_Year_Accumulated_Depr	0	0	12	\N	2008-05-30 16:36:36	100	\N	Assets	22	\N	Y	Prior. Year Accumulated Depr.	\N	\N	\N	\N	Prior. Year Accumulated Depr.	2010-03-29 16:29:24	100
53493	A_Salvage_Value	0	0	12	\N	2008-05-30 16:36:44	100	\N	Assets	10	\N	Y	Salvage Value	\N	\N	\N	\N	Salvage Value	2008-11-12 16:31:38	100
53495	A_Period_Posted	0	0	11	\N	2008-05-30 16:36:51	100	\N	Assets	22	\N	Y	Period Posted	\N	\N	\N	\N	Period Posted	2010-03-29 16:29:04	100
53496	A_Life_Period	0	0	11	\N	2008-05-30 16:36:52	100	\N	Assets	22	\N	Y	A_Life_Period	\N	\N	\N	\N	A_Life_Period	2008-05-30 16:36:52	100
53497	A_Asset_Life_Years	0	0	11	\N	2008-05-30 16:36:54	100	\N	Assets	22	\N	Y	Asset Life Years	\N	\N	\N	\N	Asset Life Years	2010-03-29 16:08:37	100
53499	A_Calc_Accumulated_Depr	0	0	12	\N	2008-05-30 16:36:58	100	\N	Assets	22	\N	Y	Calc. Accumulated Depr.	\N	\N	\N	\N	Calc. Accumulated Depr.	2010-03-29 16:11:23	100
53500	A_Curr_Dep_Exp	0	0	12	\N	2008-05-30 16:36:59	100	\N	Assets	22	\N	Y	Curr. Dep. Exp.	\N	\N	\N	\N	Curr. Dep. Exp.	2010-03-29 16:12:04	100
53501	A_Current_Period	0	0	11	\N	2008-05-30 16:37:00	100	\N	Assets	22	\N	Y	Current Period	\N	\N	\N	\N	Current Period	2010-03-29 16:12:16	100
53507	A_Asset_Reval_Entry_ID	0	0	13	\N	2008-05-30 16:38:43	100	\N	Assets	22	\N	Y	Asset Reval. Entry	\N	\N	\N	\N	Asset Reval. Entry	2010-03-29 16:09:05	100
53508	A_Effective_Date	0	0	15	\N	2008-05-30 16:38:45	100	\N	Assets	7	\N	Y	Effective Date	\N	\N	\N	\N	Effective Date	2010-03-29 16:24:58	100
53509	A_Reval_Cal_Method	0	0	17	53259	2008-05-30 16:38:51	100	\N	Assets	3	\N	Y	Revaluation Calculation Method	\N	\N	\N	\N	Revaluation Calculation Method	2008-11-12 16:21:47	100
53510	A_Reval_Multiplier	0	0	17	53260	2008-05-30 16:38:55	100	\N	Assets	3	\N	Y	Reval. Multiplier	\N	\N	\N	\N	Reval. Multiplier	2010-03-29 16:31:01	100
53511	A_Reval_Effective_Date	0	0	17	53261	2008-05-30 16:39:14	100	\N	Assets	2	\N	Y	Reval. Effective Date	\N	\N	\N	\N	Reval. Effective Date	2010-03-29 16:30:30	100
53512	A_Rev_Code	0	0	17	53262	2008-05-30 16:39:18	100	\N	Assets	3	\N	Y	Rev. Code	\N	\N	\N	\N	Rev. Code	2010-03-29 16:31:21	100
53513	A_Asset_Reval_Index_ID	0	0	13	\N	2008-05-30 16:39:39	100	\N	Assets	22	\N	Y	Asset Reval Index	\N	\N	\N	\N	Asset Reval Index	2010-03-29 16:09:18	100
53514	A_Reval_Rate	0	0	22	\N	2008-05-30 16:39:49	100	\N	Assets	22	\N	Y	Reval. Rate	\N	\N	\N	\N	Reval. Rate	2010-03-29 16:31:11	100
53515	A_Reval_Code	0	0	17	53262	2008-05-30 16:39:51	100	\N	Assets	3	\N	Y	Reval. Code	\N	\N	\N	\N	Reval. Code	2010-03-29 16:30:11	100
53516	A_Depreciation_Entry_ID	0	0	13	\N	2008-05-30 16:40:03	100	\N	Assets	22	\N	Y	Depreciation Entry	\N	\N	\N	\N	Depreciation Entry	2010-03-29 16:13:18	100
53517	A_Asset_Split_ID	0	0	13	\N	2008-05-30 16:41:55	100	\N	Assets	22	\N	Y	Asset Split	\N	\N	\N	\N	Asset Split	2010-03-29 16:09:31	100
53518	A_Amount_Split	0	0	12	\N	2008-05-30 16:41:58	100	\N	Assets	22	\N	Y	Amount Split	\N	\N	\N	\N	Amount Split	2010-03-29 16:03:27	100
53519	A_Asset_Acct_ID	0	0	11	\N	2008-05-30 16:41:59	100	\N	Assets	22	\N	Y	Asset Acct.	\N	\N	\N	\N	Asset Acct.	2010-03-29 16:03:46	100
53520	A_Percent_Original	0	0	22	\N	2008-05-30 16:42:01	100	\N	Assets	22	\N	Y	Original Percent	\N	\N	\N	\N	Original Percent	2010-03-29 16:26:00	100
53521	A_Split_Type	0	0	17	53263	2008-05-30 16:42:10	100	\N	Assets	3	\N	Y	Split Type	\N	\N	\N	\N	Split Type	2008-11-12 16:57:52	100
53522	A_Transfer_Balance_IS	0	0	20	\N	2008-05-30 16:42:25	100	\N	Assets	1	\N	Y	Transfer Balance IS	\N	\N	\N	\N	Transfer Balance IS	2008-11-12 16:58:40	100
53523	A_QTY_Split	0	0	22	\N	2008-05-30 16:42:26	100	\N	Assets	22	\N	Y	Qty. Split	\N	\N	\N	\N	Qty. Split	2010-03-29 16:29:54	100
53524	A_Percent_Split	0	0	22	\N	2008-05-30 16:42:27	100	\N	Assets	22	\N	Y	A_Percent_Split	\N	\N	\N	\N	A_Percent_Split	2008-05-30 16:42:27	100
53525	A_Asset_ID_To	0	0	18	53258	2008-05-30 16:42:32	100	\N	Assets	22	\N	Y	To Asset ID	\N	\N	\N	\N	To Asset ID	2008-11-12 16:58:21	100
53530	A_Depreciation_Acct	0	0	25	\N	2008-05-30 16:43:55	100	\N	Assets	40	\N	Y	Depreciation Expense Account	\N	\N	\N	\N	Depreciation Expense Account	2008-11-12 16:20:37	100
53531	A_Depreciation_Manual_Period	0	0	17	53256	2008-05-30 16:43:58	100	\N	Assets	2	\N	Y	Depreciation Manual Period	\N	\N	\N	\N	Depreciation Manual Period	2010-03-29 16:14:51	100
53532	A_Disposal_Gain	0	0	25	\N	2008-05-30 16:44:01	100	\N	Assets	10	\N	Y	Disposal Gain	\N	\N	\N	\N	Disposal Gain	2010-03-29 16:15:54	100
53533	A_Reval_Cost_Offset_Prior	0	0	25	\N	2008-05-30 16:44:02	100	\N	Assets	22	\N	Y	Revaluation Cost Offset for Prior Year	\N	\N	\N	\N	Revaluation Cost Offset for Prior Year	2008-11-12 16:22:35	100
53534	A_Reval_Cost_Offset	0	0	25	\N	2008-05-30 16:44:03	100	\N	Assets	22	\N	Y	Revaluation Cost Offset for Current Year	\N	\N	\N	\N	Revaluation Cost Offset for Current Year	2008-11-12 16:22:10	100
53535	A_Reval_Accumdep_Offset_Prior	0	0	25	\N	2008-05-30 16:44:06	100	\N	Assets	22	\N	Y	Revaluation Accumulated Depreciation Offset for Prior Year	\N	\N	\N	\N	Revaluation Accumulated Depreciation Offset for Prior Year	2008-11-12 16:24:31	100
53536	A_Reval_Accumdep_Offset_Cur	0	0	25	\N	2008-05-30 16:44:07	100	\N	Assets	22	\N	Y	Revaluation Accumulated Depreciation Offset for Current Year	\N	\N	\N	\N	Revaluation Accumulated Depreciation Offset for Current Year	2008-11-12 16:24:09	100
53537	A_Period_Start	0	0	11	\N	2008-05-30 16:44:08	100	\N	Assets	10	\N	Y	Period Start	\N	\N	\N	\N	Period Start	2008-11-12 16:31:02	100
53538	A_Period_End	0	0	11	\N	2008-05-30 16:44:09	100	\N	Assets	10	\N	Y	Period End	\N	\N	\N	\N	Period End	2008-11-12 16:31:20	100
53539	A_Disposal_Revenue	0	0	25	\N	2008-05-30 16:44:11	100	\N	Assets	40	\N	Y	Disposal Revenue	\N	\N	\N	\N	Disposal Revenue	2008-11-12 16:21:00	100
53540	A_Disposal_Loss	0	0	25	\N	2008-05-30 16:44:12	100	\N	Assets	40	\N	Y	Loss on Disposal	\N	\N	\N	\N	Loss on Disposal	2008-11-12 16:21:18	100
53541	A_Split_Percent	0	0	22	\N	2008-05-30 16:44:20	100	\N	Assets	22	\N	Y	Split Percentage	\N	\N	\N	\N	Split Percentage	2008-11-12 16:17:00	100
53542	A_Reval_Depexp_Offset	0	0	25	\N	2008-05-30 16:44:23	100	\N	Assets	22	\N	Y	Revaluation Expense Offs	\N	\N	\N	\N	Revaluation Expense Offs	2008-11-12 16:24:51	100
53543	A_Depreciation_Variable_Perc	0	0	22	\N	2008-05-30 16:44:25	100	\N	Assets	22	\N	Y	Depreciation Variable Perc.	\N	\N	\N	\N	Depreciation Variable Perc.	2010-03-29 16:15:25	100
53544	A_Depreciation_Method_ID	0	0	13	\N	2008-05-30 16:44:30	100	\N	Assets	10	\N	Y	Depreciation Calculation Type	\N	\N	\N	\N	Depreciation Calculation Type	2008-11-12 16:26:36	100
53545	A_Depreciation_Manual_Amount	0	0	12	\N	2008-05-30 16:44:31	100	\N	Assets	22	\N	Y	Depreciation Manual Amount	\N	\N	\N	\N	Depreciation Manual Amount	2010-03-29 16:14:38	100
53546	A_Depreciation_Conv_ID	0	0	18	53267	2008-05-30 16:44:35	100	\N	Assets	10	\N	Y	Convention Type	\N	\N	\N	\N	Convention Type	2008-11-12 16:27:20	100
53547	A_Asset_Spread_ID	0	0	13	\N	2008-05-30 16:44:41	100	\N	Assets	22	\N	Y	Asset Spread	\N	\N	\N	\N	Asset Spread	2010-03-29 16:09:46	100
53548	A_Accumdepreciation_Acct	0	0	25	\N	2008-05-30 16:44:43	100	\N	Assets	40	\N	Y	Accumulated Depreciation	\N	\N	\N	\N	Accumulated Depreciation	2008-11-12 16:20:14	100
53549	A_Asset_Acct	0	0	25	\N	2008-05-30 16:44:44	100	\N	Assets	40	\N	Y	Asset Cost Account	\N	\N	\N	\N	Asset Cost Account	2008-11-12 16:17:55	100
53550	A_Asset_Disposed_ID	0	0	13	\N	2008-05-30 16:45:34	100	\N	Assets	22	\N	Y	Disposed Asset	\N	\N	\N	\N	Disposed Asset	2010-03-29 16:05:25	100
53551	A_Disposed_Date	0	0	15	\N	2008-05-30 16:45:38	100	\N	Assets	7	\N	Y	Disposed Date	\N	\N	\N	\N	Disposed Date	2008-11-12 17:01:08	100
53552	A_Disposed_Reason	0	0	17	53269	2008-05-30 16:45:45	100	\N	Assets	10	\N	Y	Disposed Reason Code	\N	\N	\N	\N	Disposed Reason Code	2008-11-12 17:00:03	100
53553	A_Proceeds	0	0	12	\N	2008-05-30 16:45:57	100	\N	Assets	22	\N	Y	Proceeds	\N	\N	\N	\N	Proceeds	2010-03-29 16:29:30	100
53554	A_Disposed_Method	0	0	17	53270	2008-05-30 16:46:01	100	\N	Assets	10	\N	Y	Disposal Method	\N	\N	\N	\N	Disposal Method	2008-11-12 17:00:46	100
53555	A_Asset_Trade_ID	0	0	18	53258	2008-05-30 16:46:02	100	\N	Assets	22	\N	Y	Asset Trade	\N	\N	\N	\N	Asset Trade	2010-03-29 16:10:15	100
53556	A_Asset_Transfer_ID	0	0	13	\N	2008-05-30 16:46:43	100	\N	Assets	22	\N	Y	Asset Transfer	\N	\N	\N	\N	Asset Transfer	2010-03-29 16:10:25	100
53557	A_Asset_Acct_New	0	0	25	\N	2008-05-30 16:46:49	100	\N	Assets	22	\N	Y	New Asset Cost Acct	\N	\N	\N	\N	New Asset Cost Acct	2008-11-12 16:51:57	100
53558	A_Depreciation_Acct_Str	0	0	10	\N	2008-05-30 16:46:50	100	\N	Assets	40	\N	Y	Old Depreciation Exp Acct	\N	\N	\N	\N	Old Depreciation Exp Acct	2008-11-12 16:54:01	100
53559	A_Disposal_Loss_New	0	0	25	\N	2008-05-30 16:46:52	100	\N	Assets	22	\N	Y	New Disposal Loss	\N	\N	\N	\N	New Disposal Loss	2008-11-12 16:56:01	100
53560	A_Transfer_Balance	0	0	20	\N	2008-05-30 16:46:54	100	\N	Assets	1	\N	Y	Transfer Balance Sheet	\N	\N	\N	\N	Transfer Balance Sheet	2008-11-12 16:56:27	100
53561	A_Disposal_Revenue_Str	0	0	10	\N	2008-05-30 16:46:59	100	\N	Assets	40	\N	Y	Old Disposal Revenue	\N	\N	\N	\N	Old Disposal Revenue	2008-11-12 16:54:51	100
53562	A_Disposal_Revenue_New	0	0	25	\N	2008-05-30 16:47:01	100	\N	Assets	22	\N	Y	New Disposal Revenue	\N	\N	\N	\N	New Disposal Revenue	2008-11-12 16:55:11	100
53563	A_Disposal_Loss_Str	0	0	10	\N	2008-05-30 16:47:03	100	\N	Assets	40	\N	Y	Old Disposal Loss	\N	\N	\N	\N	Old Disposal Loss	2008-11-12 16:55:32	100
53564	A_Depreciation_Acct_New	0	0	25	\N	2008-05-30 16:47:16	100	\N	Assets	22	\N	Y	New Depreciation Exp Acct	\N	\N	\N	\N	New Depreciation Exp Acct	2008-11-12 16:54:30	100
53565	A_Asset_Acct_Str	0	0	10	\N	2008-05-30 16:47:19	100	\N	Assets	40	\N	Y	Old Asset Cost Acct	\N	\N	\N	\N	Old Asset Cost Acct	2008-11-12 16:51:41	100
53566	A_Accumdepreciation_Acct_New	0	0	25	\N	2008-05-30 16:47:20	100	\N	Assets	22	\N	Y	New Accum Depreciation Acct	\N	\N	\N	\N	New Accum Depreciation Acct	2008-11-12 16:53:30	100
53567	A_Accumdepreciation_Acct_Str	0	0	10	\N	2008-05-30 16:47:21	100	\N	Assets	40	\N	Y	Old Accum Depreciation Acct	\N	\N	\N	\N	Old Accum Depreciation Acct	2008-11-12 16:52:22	100
53568	A_Depreciation_Build_ID	0	0	13	\N	2008-05-30 16:49:24	100	\N	Assets	22	\N	Y	Depreciation Build	\N	\N	\N	\N	Depreciation Build	2010-03-29 16:12:46	100
53569	A_Asset_Spread_Type	0	0	14	\N	2008-05-30 16:51:12	100	\N	Assets	20	\N	Y	Asset Spread Type	\N	\N	\N	\N	Asset Spread Type	2010-03-29 16:09:59	100
53570	A_Period_10	0	0	22	\N	2008-05-30 16:51:13	100	\N	Assets	22	\N	Y	Period 10	\N	\N	\N	\N	Period 10	2010-03-29 16:26:38	100
53571	A_Period_12	0	0	22	\N	2008-05-30 16:51:15	100	\N	Assets	22	\N	Y	Period 12	\N	\N	\N	\N	Period 12	2010-03-29 16:27:00	100
53572	A_Period_14	0	0	22	\N	2008-05-30 16:51:17	100	\N	Assets	22	\N	Y	Period 14	\N	\N	\N	\N	Period 14	2010-03-29 16:27:15	100
53573	A_Period_3	0	0	22	\N	2008-05-30 16:51:18	100	\N	Assets	22	\N	Y	Period 3	\N	\N	\N	\N	Period 3	2010-03-29 16:27:31	100
53574	A_Period_5	0	0	22	\N	2008-05-30 16:51:19	100	\N	Assets	22	\N	Y	Period 5	\N	\N	\N	\N	Period 5	2010-03-29 16:27:46	100
53575	A_Period_7	0	0	22	\N	2008-05-30 16:51:20	100	\N	Assets	22	\N	Y	Period 7	\N	\N	\N	\N	Period 7	2010-03-29 16:28:01	100
53576	A_Period_9	0	0	22	\N	2008-05-30 16:51:28	100	\N	Assets	22	\N	Y	Period 9	\N	\N	\N	\N	Period 9	2010-03-29 16:28:17	100
53577	A_Period_8	0	0	22	\N	2008-05-30 16:51:29	100	\N	Assets	22	\N	Y	Period 8	\N	\N	\N	\N	Period 8	2010-03-29 16:28:09	100
53578	A_Period_6	0	0	22	\N	2008-05-30 16:51:30	100	\N	Assets	22	\N	Y	Period 6	\N	\N	\N	\N	Period 6	2010-03-29 16:27:54	100
53579	A_Period_4	0	0	22	\N	2008-05-30 16:51:31	100	\N	Assets	22	\N	Y	Period 4	\N	\N	\N	\N	Period 4	2010-03-29 16:27:38	100
53580	A_Period_2	0	0	22	\N	2008-05-30 16:51:33	100	\N	Assets	22	\N	Y	Period 2	\N	\N	\N	\N	Period 2	2010-03-29 16:27:23	100
53581	A_Period_13	0	0	22	\N	2008-05-30 16:51:34	100	\N	Assets	22	\N	Y	Period 13	\N	\N	\N	\N	Period 13	2010-03-29 16:27:07	100
53582	A_Period_11	0	0	22	\N	2008-05-30 16:51:35	100	\N	Assets	22	\N	Y	Period 11	\N	\N	\N	\N	Period 11	2010-03-29 16:26:49	100
53583	A_Period_1	0	0	22	\N	2008-05-30 16:51:37	100	\N	Assets	22	\N	Y	Period 1	\N	\N	\N	\N	Period 1	2010-03-29 16:26:28	100
53584	A_Depreciation_Convention_ID	0	0	13	\N	2008-05-30 16:52:06	100	\N	Assets	22	\N	Y	Depreciation Convention	\N	\N	\N	\N	Depreciation Convention	2010-03-29 16:13:03	100
53585	ConventionType	0	0	10	\N	2008-05-30 16:52:08	100	\N	Assets	10	\N	Y	ConventionType	\N	\N	\N	\N	ConventionType	2008-05-30 16:52:08	100
53587	A_Asset_Group_Acct_ID	0	0	13	\N	2008-05-30 16:52:55	100	\N	Assets	22	\N	Y	Asset Group Acct.	\N	\N	\N	\N	Asset Group Acct.	2010-03-29 16:06:05	100
53588	A_Depreciation_Calc_Type	0	0	18	53266	2008-05-30 16:53:24	100	\N	Assets	10	\N	Y	Depreciation Calculation Type	\N	\N	\N	\N	Depreciation Calculation Type	2008-11-12 16:08:47	100
53603	A_Asset_Change_ID	0	0	13	\N	2008-05-30 16:55:53	100	\N	Assets	22	\N	Y	Asset Change	\N	\N	\N	\N	Asset Change	2010-03-29 16:04:35	100
53604	AssetBookValueAmt	0	0	12	\N	2008-05-30 16:56:12	100	\N	Assets	22	\N	Y	Asset Book value amt.	\N	\N	\N	\N	Asset Book value amt.	2010-03-29 16:32:03	100
53605	AssetAccumDepreciationAmt	0	0	12	\N	2008-05-30 16:56:13	100	\N	Assets	22	\N	Y	Asset Accum. Depreciation Amt.	\N	\N	\N	\N	Asset Accum. Depreciation Amt.	2010-03-29 16:31:43	100
53609	ChangeType	0	0	17	53273	2008-05-30 16:56:45	100	\N	Assets	3	\N	Y	ChangeType	\N	\N	\N	\N	ChangeType	2008-05-30 16:56:45	100
53610	ChangeDate	0	0	16	\N	2008-05-30 16:56:46	100	\N	Assets	7	\N	Y	ChangeDate	\N	\N	\N	\N	ChangeDate	2008-05-30 16:56:46	100
53611	ChangeAmt	0	0	12	\N	2008-05-30 16:56:48	100	\N	Assets	22	\N	Y	ChangeAmt	\N	\N	\N	\N	ChangeAmt	2008-05-30 16:56:48	100
53612	A_Asset_Addition_ID	0	0	13	\N	2008-05-30 16:57:04	100	\N	Assets	22	\N	Y	Asset Addition	\N	\N	\N	\N	Asset Addition	2010-03-29 16:04:03	100
53640	A_SourceType	0	0	17	53276	2008-05-30 17:00:03	100	\N	Assets	3	\N	Y	Source of Entry	\N	\N	\N	\N	Source of Entry	2008-11-12 16:32:20	100
53642	A_Asset_Use_ID	0	0	13	\N	2008-05-30 17:00:22	100	\N	Assets	22	\N	Y	Asset Use	\N	\N	\N	\N	Asset Use	2010-03-29 16:10:50	100
53643	UseDate	0	0	15	\N	2008-05-30 17:00:27	100	\N	Assets	7	\N	Y	UseDate	\N	\N	\N	\N	UseDate	2008-05-30 17:00:27	100
53644	I_Asset_ID	0	0	13	\N	2008-05-30 17:01:46	100	\N	Assets	22	\N	Y	Asset	\N	\N	\N	\N	Asset	2010-03-29 15:58:36	100
\.
