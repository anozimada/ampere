copy "ad_form" ("ad_form_id", "name", "accesslevel", "ad_client_id", "ad_org_id", "classname", "created", "createdby", "description", "entitytype", "help", "isactive", "isbetafunctionality", "jspurl", "updated", "updatedby") from STDIN;
108	Matching PO-Receipt-Invoice	3	0	0	org.compiere.apps.form.VMatch	2002-02-08 20:38:26	0	Match Purchase Orders, Receipts, Vendor Invoices	Requisition-to-Invoice	Make sure that the Receipts and Invoices are processed. If you want to match partial shipment, make sure that "Same Quantity" is not selected.	Y	N	\N	2000-01-02 00:00:00	0
53053	Purchasing from Replenishment	3	0	0	org.compiere.apps.form.VOrderGenPO	2015-03-23 13:14:00	100	\N	Requisition-to-Invoice	\N	Y	N	\N	2015-03-23 13:14:00	100
53055	Receipt Scanner	3	0	0	org.adaxa.form.VReceiptScannerForm	2015-04-08 13:32:51	100	Material Receipt using Barcode Scanner	Requisition-to-Invoice	\N	Y	N	\N	2015-04-08 13:32:51	100
\.
