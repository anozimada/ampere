copy "ad_val_rule" ("ad_val_rule_id", "name", "ad_client_id", "ad_org_id", "code", "created", "createdby", "description", "entitytype", "isactive", "type", "updated", "updatedby") from STDIN;
52454	C_POS by Organization	0	0	AD_Org_ID = @#AD_Org_ID@	2016-04-22 16:10:49	100	\N	Point of Sales	Y	S	2019-02-04 13:38:37	0
52455	C_BankAccount_ID for POS Terminal	0	0	EXISTS (SELECT 1 FROM C_POS pos WHERE pos.C_POS_ID=@C_POS_ID@ AND pos.C_BankAccount_ID=C_BankAccount.C_BankAccount_ID)	2016-04-22 16:11:00	100	\N	Point of Sales	Y	S	2016-04-22 16:11:00	100
52456	C_BankAccount_ID To for POS Terminal	0	0	EXISTS (SELECT 1 FROM C_POS pos WHERE pos.C_POS_ID=@C_POS_ID@ AND pos.CashTransferBankAccount_ID=C_BankAccount.C_BankAccount_ID)	2016-04-22 16:11:03	100	\N	Point of Sales	Y	S	2016-04-22 16:11:03	100
52457	C_DocType Payments	0	0	C_DocType.DocBaseType IN ('APP')	2016-04-22 16:11:05	100	\N	Point of Sales	Y	S	2016-04-22 16:11:05	100
52458	C_DocType Receipts	0	0	C_DocType.DocBaseType IN ('ARR')	2016-04-22 16:11:08	100	\N	Point of Sales	Y	S	2016-04-22 16:11:08	100
\.
