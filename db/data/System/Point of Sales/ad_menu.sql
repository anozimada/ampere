copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
54030	Point of Sales	\N	0	\N	\N	0	\N	\N	2016-01-20 09:41:08	100	The Point of Sales menu	Point of Sales	N	Y	N	N	Y	2022-02-17 23:09:42	100
54032	Generate Immediate Invoice	P	0	\N	\N	0	53823	\N	2016-01-20 11:01:31	100	This process allows generate immediate invoice based on a new business partner.	Point of Sales	N	Y	N	N	N	2022-02-17 21:53:10	100
54033	Reverse The Sales Transaction	P	0	\N	\N	0	53824	\N	2016-01-20 11:01:32	100	This process allows reverse the sales transaction	Point of Sales	N	Y	N	N	N	2022-02-17 23:09:36	100
\.
