copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
271	Service	\N	0	\N	\N	0	\N	\N	2001-04-05 22:06:46	0	Service Management	Service	Y	N	N	N	Y	2000-01-02 00:00:00	0
284	Service Level	W	0	\N	\N	0	\N	220	2001-05-13 12:34:44	0	Maintain Service Levels	Service	N	Y	N	N	N	2000-01-02 00:00:00	0
317	Expense Type	W	0	\N	\N	0	\N	234	2002-06-15 21:53:30	0	Maintain Expense Report Types	Service	N	Y	N	Y	N	2000-01-02 00:00:00	0
318	Expense Report	W	0	\N	\N	0	\N	235	2002-06-15 22:08:17	0	Time and Expense Report	Service	N	Y	N	Y	N	2000-01-02 00:00:00	0
319	Resource	W	0	\N	\N	0	\N	236	2002-06-15 22:20:43	0	Maintain Resources	Service	N	Y	N	Y	N	2000-01-02 00:00:00	0
327	Expenses (to be invoiced)	W	0	\N	\N	0	\N	242	2002-07-14 18:54:12	0	View expenses and charges not invoiced to customers	Service	N	Y	N	Y	N	2000-01-02 00:00:00	0
328	Create Sales Orders from Expense	P	0	\N	\N	0	186	\N	2002-07-14 20:17:31	0	Create Sales Orders for Customers from Expense Reports	Service	N	Y	N	N	N	2000-01-02 00:00:00	0
329	Create AP Expense Invoices	P	0	\N	\N	0	187	\N	2002-07-14 20:17:58	0	Create AP Invoices from Expenses to be paid to employees	Service	N	Y	N	N	N	2000-01-02 00:00:00	0
344	Training	W	0	\N	\N	0	\N	253	2003-01-23 01:10:53	0	Repeated Training	Service	N	Y	N	Y	N	2000-01-02 00:00:00	0
349	Expenses (not reimbursed)	W	0	\N	\N	0	\N	254	2003-02-06 14:43:34	0	View expenses and charges not reimbursed	Service	N	Y	N	N	N	2000-01-02 00:00:00	0
369	Time Type	W	0	\N	\N	0	\N	272	2003-06-02 00:02:59	0	Maintain Time Recording Type	Service	N	Y	N	Y	N	2000-01-02 00:00:00	0
53794	Timesheet Entry	W	0	\N	\N	0	\N	53350	2014-03-18 00:52:40	100	\N	Service	N	Y	N	N	N	2014-03-18 00:52:40	100
53798	Process Timesheet	P	0	\N	\N	0	53608	\N	2014-03-19 18:06:41	100	Process Timesheet Entry	Service	N	Y	N	N	N	2014-03-19 18:06:41	100
53803	Time Sheet	X	0	53043	\N	0	\N	\N	2014-03-20 20:31:36	100	\N	Service	N	Y	N	N	N	2016-04-11 16:45:53	0
400001	Project Task Timer	X	0	400001	\N	0	\N	\N	2022-04-19 13:28:04	100	\N	Service	Y	Y	N	N	N	2022-04-19 13:29:05	100
\.
