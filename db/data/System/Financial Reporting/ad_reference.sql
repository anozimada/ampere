copy "ad_reference" ("ad_reference_id", "name", "validationtype", "ad_client_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "isorderbyvalue", "updated", "updatedby", "vformat") from STDIN;
235	PA_Report AmountType (deprecated)	L	0	0	2001-05-13 09:49:00	0	\N	Financial Reporting	\N	N	N	2009-10-02 11:42:57	100	\N
236	PA_Report CalculationType	L	0	0	2001-05-13 09:53:45	0	\N	Financial Reporting	\N	Y	N	2000-01-02 00:00:00	0	\N
237	PA_Report ColumnType	L	0	0	2001-05-13 09:58:39	0	\N	Financial Reporting	\N	Y	N	2000-01-02 00:00:00	0	\N
238	PA_Report CurrencyType	L	0	0	2001-05-13 10:01:57	0	\N	Financial Reporting	\N	Y	N	2000-01-02 00:00:00	0	\N
239	PA_ReportColumn	T	0	0	2001-05-13 10:07:18	0	\N	Financial Reporting	\N	Y	N	2000-01-02 00:00:00	0	\N
240	PA_ReportLine	T	0	0	2001-05-13 10:18:00	0	\N	Financial Reporting	\N	Y	N	2000-01-02 00:00:00	0	\N
241	PA_Report LineType	L	0	0	2001-05-13 10:18:48	0	\N	Financial Reporting	\N	Y	N	2000-01-02 00:00:00	0	\N
242	PA_ReportLine Parent	T	0	0	2001-05-13 10:45:48	0	\N	Financial Reporting	\N	Y	N	2000-01-02 00:00:00	0	\N
275	C_Period (all)	T	0	0	2003-02-09 13:49:45	0	Periods Sorted by Date	Financial Reporting	\N	Y	N	2000-01-02 00:00:00	0	\N
53280	PA_ReportSource ElementType	L	0	0	2008-07-09 15:33:11	100	Element Types for Accounting Elements - equal to C_AcctSchema ElementType plus Combination	Financial Reporting	Hardcoded Element Types	Y	N	2008-07-09 15:33:11	100	AA
53285	Scaling Factors	L	0	0	2008-10-03 12:30:19	100	Factors for scaling the results of financial reports.	Financial Reporting	Currently supported: thousand, million.	Y	Y	2008-10-03 12:32:44	100	\N
53299	PA_ReportCube	T	0	0	2009-05-14 12:28:22	100	\N	Financial Reporting	\N	Y	N	2009-05-14 12:28:22	100	\N
53327	PA_Report Period Type	L	0	0	2009-10-02 11:33:12	100	\N	Financial Reporting	\N	Y	N	2009-10-02 11:33:12	100	\N
53328	PA_Report Amount Type	L	0	0	2009-10-02 11:43:15	100	\N	Financial Reporting	\N	Y	N	2009-10-02 11:43:15	100	\N
53691	header print format	T	0	0	2014-05-16 15:52:47	100	\N	Financial Reporting	\N	Y	N	2014-05-16 15:52:47	100	\N
53793	PA_ReportLine Line Stroke Type	L	0	0	2015-09-09 16:22:18	100	\N	Financial Reporting	\N	Y	N	2015-09-09 16:22:18	100	\N
\.
