copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
118	GL Journal Batch	W	0	\N	\N	0	\N	132	1999-06-29 00:00:00	0	Enter and change Manual Journal Entries	Performance Analysis	Y	Y	N	Y	N	2016-04-11 16:27:06	0
169	Accounting Fact Details	W	0	\N	\N	0	\N	162	1999-12-04 21:49:37	0	Query Accounting Facts	Performance Analysis	Y	Y	N	Y	N	2000-01-02 00:00:00	0
278	Accounting	\N	0	\N	\N	0	\N	\N	2001-04-24 18:08:23	0	\N	Performance Analysis	Y	N	N	N	Y	2022-02-17 22:47:53	100
352	Accounting Fact Summary	W	0	\N	\N	0	\N	255	2003-04-18 15:42:06	0	Query Accounting Daily Balances	Performance Analysis	Y	Y	N	Y	N	2022-02-17 22:49:01	100
433	Accounting Fact Report	R	0	\N	\N	0	252	\N	2004-01-09 15:36:50	0	Accounting Fact Details Report	Performance Analysis	Y	Y	N	Y	N	2022-02-17 22:48:50	100
434	Accounting Fact Daily	R	0	\N	\N	0	253	\N	2004-01-09 15:37:06	0	Accounting Fact Details summarized by Accounting Date	Performance Analysis	Y	Y	N	Y	N	2005-08-27 11:58:46	100
435	Accounting Fact Period	R	0	\N	\N	0	254	\N	2004-01-09 15:37:24	0	Accounting Fact Details summarized by Accounting Period	Performance Analysis	Y	Y	N	Y	N	2000-01-02 00:00:00	0
53288	GL reconciliation (auto)	P	0	\N	\N	0	53221	\N	2010-09-02 18:18:48	100	Reconcile account transactions according to standard rules	Performance Analysis	Y	Y	N	N	N	2020-10-16 23:42:21	100
53289	GL Reconcilation (manual)	X	0	53016	\N	0	\N	\N	2010-09-02 18:21:06	100	\N	Performance Analysis	Y	Y	N	N	N	2020-10-16 23:42:33	100
53290	Unreconciled facts	R	0	\N	\N	0	53220	\N	2010-09-02 18:21:35	100	\N	Performance Analysis	Y	Y	N	N	N	2022-02-17 22:48:19	100
53690	GL Journal	W	0	\N	\N	0	\N	53296	2013-11-23 17:19:33	100	Enter and change Manual Journal Entries	Performance Analysis	Y	Y	N	Y	N	2016-04-11 16:45:36	0
53938	General Ledger	\N	0	\N	\N	0	\N	\N	2015-02-05 14:34:14	100	\N	Performance Analysis	Y	N	N	N	Y	2022-02-17 22:49:35	100
\.
