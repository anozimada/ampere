copy "ad_language" ("ad_language_id", "name", "ad_client_id", "ad_language", "ad_org_id", "countrycode", "created", "createdby", "datepattern", "isactive", "isbaselanguage", "isdecimalpoint", "issystemlanguage", "languageiso", "processing", "timepattern", "updated", "updatedby") from STDIN;
100	Arabic (United Arab Emirates)	0	ar_AE	0	AE	2003-08-06 18:41:25	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
101	Arabic (Bahrain)	0	ar_BH	0	BH	2003-08-06 18:41:26	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
102	Arabic (Algeria)	0	ar_DZ	0	DZ	2003-08-06 18:41:59	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
103	Arabic (Egypt)	0	ar_EG	0	EG	2003-08-06 18:41:59	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
104	Arabic (Iraq)	0	ar_IQ	0	IQ	2003-08-06 18:41:59	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
105	Arabic (Jordan)	0	ar_JO	0	JO	2003-08-06 18:41:59	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
106	Arabic (Kuwait)	0	ar_KW	0	KW	2003-08-06 18:41:59	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
107	Arabic (Lebanon)	0	ar_LB	0	LB	2003-08-06 18:41:59	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
108	Arabic (Libya)	0	ar_LY	0	LY	2003-08-06 18:42:00	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
109	Arabic (Morocco)	0	ar_MA	0	MA	2003-08-06 18:42:00	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
110	Arabic (Oman)	0	ar_OM	0	OM	2003-08-06 18:42:00	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
111	Arabic (Qatar)	0	ar_QA	0	QA	2003-08-06 18:42:00	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
112	Arabic (Saudi Arabia)	0	ar_SA	0	SA	2003-08-06 18:42:00	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
113	Arabic (Sudan)	0	ar_SD	0	SD	2003-08-06 18:42:00	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
114	Arabic (Syria)	0	ar_SY	0	SY	2003-08-06 18:42:00	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
115	Arabic (Tunisia)	0	ar_TN	0	TN	2003-08-06 18:42:01	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
116	Arabic (Yemen)	0	ar_YE	0	YE	2003-08-06 18:42:01	0	\N	N	N	Y	N	ar	\N	\N	2016-04-11 16:06:46	0
117	Byelorussian (Belarus)	0	be_BY	0	BY	2003-08-06 18:42:01	0	\N	N	N	Y	N	be	\N	\N	2016-04-11 16:06:46	0
118	Bulgarian (Bulgaria)	0	bg_BG	0	BG	2003-08-06 18:42:01	0	\N	N	N	Y	N	bg	\N	\N	2016-04-11 16:06:46	0
119	Catalan (Spain)	0	ca_ES	0	ES	2003-08-06 18:42:01	0	\N	N	N	Y	N	ca	\N	\N	2016-04-11 16:06:46	0
120	Czech (Czech Republic)	0	cs_CZ	0	CZ	2003-08-06 18:42:01	0	\N	N	N	Y	N	cs	\N	\N	2016-04-11 16:06:47	0
121	Danish (Denmark)	0	da_DK	0	DK	2003-08-06 18:42:01	0	\N	N	N	Y	N	da	\N	\N	2016-04-11 16:06:47	0
122	German (Austria)	0	de_AT	0	AT	2003-08-06 18:42:02	0	\N	N	N	Y	N	de	\N	\N	2016-04-11 16:06:47	0
123	German (Switzerland)	0	de_CH	0	CH	2003-08-06 18:42:02	0	\N	N	N	Y	N	de	\N	\N	2016-04-11 16:06:47	0
124	German (Luxembourg)	0	de_LU	0	LU	2003-08-06 18:42:02	0	\N	N	N	Y	N	de	\N	\N	2016-04-11 16:06:47	0
125	Greek (Greece)	0	el_GR	0	GR	2003-08-06 18:42:02	0	\N	N	N	Y	N	el	\N	\N	2016-04-11 16:06:47	0
126	English (Australia)	0	en_AU	0	AU	2003-08-06 18:42:02	0	\N	Y	N	Y	N	en	\N	\N	2000-01-02 00:00:00	0
127	English (Canada)	0	en_CA	0	CA	2003-08-06 18:42:02	0	\N	N	N	Y	N	en	\N	\N	2016-04-11 16:06:47	0
128	English (United Kingdom)	0	en_GB	0	GB	2003-08-06 18:42:03	0	\N	N	N	Y	N	en	\N	\N	2016-04-11 16:06:47	0
129	English (Ireland)	0	en_IE	0	IE	2003-08-06 18:42:03	0	\N	N	N	Y	N	en	\N	\N	2016-04-11 16:06:47	0
130	English (India)	0	en_IN	0	IN	2003-08-06 18:42:03	0	\N	N	N	Y	N	en	\N	\N	2016-04-11 16:06:47	0
131	English (New Zealand)	0	en_NZ	0	NZ	2003-08-06 18:42:03	0	\N	Y	N	Y	N	en	\N	\N	2000-01-02 00:00:00	0
132	English (South Africa)	0	en_ZA	0	ZA	2003-08-06 18:42:03	0	\N	N	N	Y	N	en	\N	\N	2016-04-11 16:06:47	0
133	Spanish (Argentina)	0	es_AR	0	AR	2003-08-06 18:42:03	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
134	Spanish (Bolivia)	0	es_BO	0	BO	2003-08-06 18:42:03	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
135	Spanish (Chile)	0	es_CL	0	CL	2003-08-06 18:42:04	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
136	Spanish (Colombia)	0	es_CO	0	CO	2003-08-06 18:42:04	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
137	Spanish (Costa Rica)	0	es_CR	0	CR	2003-08-06 18:42:04	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
138	Spanish (Dominican Republic)	0	es_DO	0	DO	2003-08-06 18:42:04	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
139	Spanish (Ecuador)	0	es_EC	0	EC	2003-08-06 18:42:04	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
140	Spanish (Spain)	0	es_ES	0	ES	2003-08-06 18:42:04	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
141	Spanish (Guatemala)	0	es_GT	0	GT	2003-08-06 18:42:04	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
142	Spanish (Honduras)	0	es_HN	0	HN	2003-08-06 18:42:04	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
143	Spanish (Mexico)	0	es_MX	0	MX	2003-08-06 18:42:05	0	\N	N	N	Y	Y	es	\N	\N	2016-04-11 16:06:48	0
144	Spanish (Nicaragua)	0	es_NI	0	NI	2003-08-06 18:42:05	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
145	Spanish (Panama)	0	es_PA	0	PA	2003-08-06 18:42:05	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
146	Spanish (Peru)	0	es_PE	0	PE	2003-08-06 18:42:05	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
147	Spanish (Puerto Rico)	0	es_PR	0	PR	2003-08-06 18:42:05	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
148	Spanish (Paraguay)	0	es_PY	0	PY	2003-08-06 18:42:05	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
149	Spanish (El Salvador)	0	es_SV	0	SV	2003-08-06 18:42:05	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
150	Spanish (Uruguay)	0	es_UY	0	UY	2003-08-06 18:42:06	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
151	Spanish (Venezuela)	0	es_VE	0	VE	2003-08-06 18:42:06	0	\N	N	N	Y	N	es	\N	\N	2016-04-11 16:06:48	0
152	Estonian (Estonia)	0	et_EE	0	EE	2003-08-06 18:42:06	0	\N	N	N	Y	N	et	\N	\N	2016-04-11 16:06:47	0
153	Finnish (Finland)	0	fi_FI	0	FI	2003-08-06 18:42:06	0	\N	N	N	Y	N	fi	\N	\N	2016-04-11 16:06:47	0
154	French (Belgium)	0	fr_BE	0	BE	2003-08-06 18:42:06	0	\N	N	N	Y	N	fr	\N	\N	2016-04-11 16:06:47	0
155	French (Canada)	0	fr_CA	0	CA	2003-08-06 18:42:06	0	\N	N	N	Y	N	fr	\N	\N	2016-04-11 16:06:47	0
156	French (Switzerland)	0	fr_CH	0	CH	2003-08-06 18:42:06	0	\N	N	N	Y	N	fr	\N	\N	2016-04-11 16:06:47	0
157	French (Luxembourg)	0	fr_LU	0	LU	2003-08-06 18:42:07	0	\N	N	N	Y	N	fr	\N	\N	2016-04-11 16:06:47	0
158	Hindi (India)	0	hi_IN	0	IN	2003-08-06 18:42:07	0	\N	N	N	Y	N	hi	\N	\N	2016-04-11 16:06:47	0
159	Croatian (Croatia)	0	hr_HR	0	HR	2003-08-06 18:42:07	0	\N	N	N	Y	N	hr	\N	\N	2016-04-11 16:06:46	0
160	Hungarian (Hungary)	0	hu_HU	0	HU	2003-08-06 18:42:07	0	\N	N	N	Y	N	hu	\N	\N	2016-04-11 16:06:47	0
161	Icelandic (Iceland)	0	is_IS	0	IS	2003-08-06 18:42:07	0	\N	N	N	Y	N	is	\N	\N	2016-04-11 16:06:47	0
162	Italian (Switzerland)	0	it_CH	0	CH	2003-08-06 18:42:07	0	\N	N	N	Y	N	it	\N	\N	2016-04-11 16:06:47	0
163	Italian (Italy)	0	it_IT	0	IT	2003-08-06 18:42:07	0	\N	N	N	Y	N	it	\N	\N	2016-04-11 16:06:47	0
164	Hebrew (Israel)	0	iw_IL	0	IL	2003-08-06 18:42:08	0	\N	N	N	Y	N	iw	\N	\N	2016-04-11 16:06:47	0
165	Japanese (Japan)	0	ja_JP	0	JP	2003-08-06 18:42:08	0	\N	N	N	Y	N	ja	\N	\N	2016-04-11 16:06:47	0
166	Korean (South Korea)	0	ko_KR	0	KR	2003-08-06 18:42:08	0	\N	N	N	Y	N	ko	\N	\N	2016-04-11 16:06:48	0
167	Lithuanian (Lithuania)	0	lt_LT	0	LT	2003-08-06 18:42:08	0	\N	N	N	Y	N	lt	\N	\N	2016-04-11 16:06:48	0
168	Latvian (Lettish) (Latvia)	0	lv_LV	0	LV	2003-08-06 18:42:10	0	\N	N	N	Y	N	lv	\N	\N	2016-04-11 16:06:48	0
169	Macedonian (Macedonia)	0	mk_MK	0	MK	2003-08-06 18:42:10	0	\N	N	N	Y	N	mk	\N	\N	2016-04-11 16:06:48	0
170	Dutch (Belgium)	0	nl_BE	0	BE	2003-08-06 18:42:10	0	\N	N	N	Y	N	nl	\N	\N	2016-04-11 16:06:47	0
171	Dutch (Netherlands)	0	nl_NL	0	NL	2003-08-06 18:42:10	0	\N	N	N	Y	N	nl	\N	\N	2016-04-11 16:06:47	0
172	Norwegian (Norway)	0	no_NO	0	NO	2003-08-06 18:42:10	0	\N	N	N	Y	N	no	\N	\N	2016-04-11 16:06:48	0
173	Polish (Poland)	0	pl_PL	0	PL	2003-08-06 18:42:10	0	\N	N	N	Y	N	pl	\N	\N	2016-04-11 16:06:48	0
174	Portuguese (Brazil)	0	pt_BR	0	BR	2003-08-06 18:42:11	0	\N	N	N	Y	N	pt	\N	\N	2016-04-11 16:06:48	0
175	Portuguese (Portugal)	0	pt_PT	0	PT	2003-08-06 18:42:11	0	\N	N	N	Y	N	pt	\N	\N	2016-04-11 16:06:48	0
176	Romanian (Romania)	0	ro_RO	0	RO	2003-08-06 18:42:11	0	\N	N	N	Y	N	ro	\N	\N	2016-04-11 16:06:48	0
177	Russian (Russia)	0	ru_RU	0	RU	2003-08-06 18:42:11	0	\N	N	N	Y	N	ru	\N	\N	2016-04-11 16:06:48	0
178	Serbo-Croatian (Yugoslavia)	0	sh_YU	0	YU	2003-08-06 18:42:11	0	\N	N	N	Y	N	sh	\N	\N	2016-04-11 16:06:48	0
179	Slovak (Slovakia)	0	sk_SK	0	SK	2003-08-06 18:42:11	0	\N	N	N	Y	N	sk	\N	\N	2016-04-11 16:06:48	0
180	Slovenian (Slovenia)	0	sl_SI	0	SI	2003-08-06 18:42:11	0	\N	N	N	Y	N	sl	\N	\N	2016-04-11 16:06:48	0
181	Albanian (Albania)	0	sq_AL	0	AL	2003-08-06 18:42:12	0	\N	N	N	Y	N	sq	\N	\N	2016-04-11 16:06:46	0
182	Serbian (Yugoslavia)	0	sr_YU	0	YU	2003-08-06 18:42:12	0	\N	N	N	Y	N	sr	\N	\N	2016-04-11 16:06:48	0
183	Swedish (Sweden)	0	sv_SE	0	SE	2003-08-06 18:42:12	0	\N	N	N	Y	N	sv	\N	\N	2016-04-11 16:06:48	0
184	Thai (Thailand)	0	th_TH	0	TH	2003-08-06 18:42:12	0	\N	N	N	Y	N	th	\N	\N	2016-04-11 16:06:48	0
185	Turkish (Turkey)	0	tr_TR	0	TR	2003-08-06 18:42:12	0	\N	N	N	Y	N	tr	\N	\N	2016-04-11 16:06:48	0
186	Ukrainian (Ukraine)	0	uk_UA	0	UA	2003-08-06 18:42:12	0	\N	N	N	Y	N	uk	\N	\N	2016-04-11 16:06:48	0
187	Chinese (China)	0	zh_CN	0	CN	2003-08-06 18:42:12	0	\N	N	N	Y	N	zh	\N	\N	2016-04-11 16:06:46	0
188	Chinese (Hong Kong)	0	zh_HK	0	HK	2003-08-06 18:42:13	0	\N	N	N	Y	N	zh	\N	\N	2016-04-11 16:06:46	0
189	Chinese (Taiwan)	0	zh_TW	0	TW	2003-08-06 18:42:13	0	\N	N	N	Y	N	zh	\N	\N	2016-04-11 16:06:46	0
190	French (France)	0	fr_FR	0	FR	2002-07-27 12:00:54	0	\N	N	N	Y	N	fr	N	\N	2016-04-11 16:06:47	0
191	German (Germany)	0	de_DE	0	DE	2001-12-18 21:14:24	0	\N	N	N	Y	N	de	\N	\N	2016-04-11 16:06:47	0
192	English (USA)	0	en_US	0	US	2001-12-18 21:13:52	0	\N	Y	Y	Y	N	en	\N	\N	2000-01-02 00:00:00	0
193	Farsi (Iran)	0	fa_IR	0	IR	2003-08-24 20:54:59	0	\N	N	N	Y	N	fa	N	\N	2016-04-11 16:06:47	0
194	Vietnamese	0	vi_VN	0	VN	2005-07-25 10:22:01	100	\N	N	N	N	N	vi	N	\N	2016-04-11 16:06:48	0
50003	Malay (Malaysia)	0	ms_MY	0	MY	2010-03-18 18:51:35	100	\N	N	N	Y	N	ms	N	\N	2016-04-11 16:06:48	0
50004	Greek (Cyprus)	0	el_CY	0	CY	2010-03-18 18:51:52	100	\N	N	N	Y	N	el	N	\N	2016-04-11 16:06:47	0
50005	English (Malta)	0	en_MT	0	MT	2010-03-18 18:52:14	100	\N	N	N	Y	N	en	N	\N	2016-04-11 16:06:47	0
50006	English (Philippines)	0	en_PH	0	PH	2010-03-18 18:52:33	100	\N	N	N	Y	N	en	N	\N	2016-04-11 16:06:47	0
50007	English (Singapore)	0	en_SG	0	SG	2010-03-18 18:52:48	100	\N	N	N	Y	N	en	N	\N	2016-04-11 16:06:47	0
50008	Spanish (USA)	0	es_US	0	US	2010-03-18 18:53:06	100	\N	N	N	Y	N	es	N	\N	2016-04-11 16:06:48	0
50009	Irish (Ireland)	0	ga_IE	0	IE	2010-03-18 18:53:22	100	\N	N	N	Y	N	ga	N	\N	2016-04-11 16:06:47	0
50010	Indonesian (Indonesia)	0	in_ID	0	ID	2010-03-18 18:53:36	100	\N	N	N	Y	N	in	N	\N	2016-04-11 16:06:47	0
50011	Maltese (Malta)	0	mt_MT	0	MT	2010-03-18 18:53:50	100	\N	N	N	Y	N	mt	N	\N	2016-04-11 16:06:48	0
50012	Serbian (Bosnia and Herzegovina)	0	sr_BA	0	BA	2010-03-18 18:55:52	100	\N	N	N	Y	N	sr	N	\N	2016-04-11 16:06:48	0
50013	Serbian (Serbia and Montenegro)	0	sr_CS	0	CS	2010-03-18 18:56:05	100	\N	N	N	Y	N	sr	N	\N	2016-04-11 16:06:48	0
50014	Serbian (Montenegro)	0	sr_ME	0	ME	2010-03-18 18:56:16	100	\N	N	N	Y	N	sr	N	\N	2016-04-11 16:06:48	0
50015	Serbian (Serbia)	0	sr_RS	0	RS	2010-03-18 18:56:29	100	\N	N	N	Y	N	sr	N	\N	2016-04-11 16:06:48	0
50016	Chinese (Singapore)	0	zh_SG	0	SG	2010-03-18 18:56:41	100	\N	N	N	Y	N	zh	N	\N	2016-04-11 16:06:46	0
\.
