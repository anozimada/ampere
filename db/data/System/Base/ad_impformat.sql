copy "ad_impformat" ("ad_impformat_id", "name", "ad_client_id", "ad_org_id", "ad_table_id", "created", "createdby", "createfrom", "description", "formattype", "isactive", "processing", "separatorchar", "updated", "updatedby") from STDIN;
102	Accounting - Accounts	0	0	534	2003-01-14 16:29:34	0	\N	Based on the format of AccountingUS.cvs import natural Accounts	C	Y	N	\N	2000-01-02 00:00:00	0
103	Accounting - Balance Sheet	0	0	535	2003-01-14 22:39:49	0	\N	US Balance Sheet info in the format of AccountingUS.cvs	C	Y	N	\N	2000-01-02 00:00:00	0
104	Accounting - Tax  Balance Sheet	0	0	535	2003-01-14 22:45:59	0	\N	US Tax (1120) Balance Sheet info in the format of AccountingUS.cvs	C	Y	N	\N	2000-01-02 00:00:00	0
105	Accounting - Profit&Loss Statement	0	0	535	2003-01-14 22:49:21	0	\N	US Profit & Loss Statement info in the format of AccountingUS.cvs	C	Y	N	\N	2000-01-02 00:00:00	0
106	Accounting - Tax Profit&Loss Statement	0	0	535	2003-01-14 22:50:17	0	\N	US Tax (1120) Profit & Loss Statement info in the format of AccountingUS.cvs	C	Y	N	\N	2000-01-02 00:00:00	0
50000	Assets - Import Asset	0	0	53139	2008-05-30 17:05:06	100	\N	Based on format Assets.csv	C	Y	N	\N	2008-05-30 17:05:06	100
50006	Accounting - Report Line Set	0	0	535	2011-08-24 11:01:57	100	\N	\N	C	Y	N	\N	2011-08-24 11:01:57	100
50007	Quickstart Import GL Journal	0	0	599	2011-08-26 14:57:06	100	\N	\N	U	Y	N	\N	2016-04-11 16:06:32	0
50008	Quickstart Import Business Partners	0	0	533	2011-08-26 14:57:36	100	\N	\N	C	Y	N	\N	2016-04-11 16:06:32	0
50009	Quickstart Import Business Partners Group	0	0	533	2011-08-26 14:58:02	100	\N	\N	C	Y	N	\N	2016-04-11 16:06:32	0
50010	Quickstart Import Invoices	0	0	598	2011-08-26 14:58:06	100	\N	\N	U	Y	N	\N	2016-04-11 16:06:32	0
50011	Quickstart Import Order	0	0	591	2011-08-26 14:58:25	100	\N	\N	C	Y	N	\N	2016-04-11 16:06:32	0
50012	Quickstart Import Product	0	0	532	2011-08-26 14:58:45	100	\N	\N	C	Y	N	\N	2016-04-11 16:06:32	0
50013	Quickstart Import Product Price	0	0	53173	2011-08-26 14:58:58	100	\N	\N	C	Y	N	\N	2016-04-11 16:06:32	0
50014	Adaxa Payment	0	0	597	2011-08-26 14:59:07	100	\N	\N	U	Y	N	\N	2011-08-26 14:59:07	100
50031	Import Budget	0	0	53618	2013-10-11 17:55:28	100	\N	\N	C	Y	N	\N	2013-10-11 17:55:28	100
50036	Import invoice/payment from statement	0	0	598	2014-06-26 10:56:11	100	\N	\N	C	Y	N	\N	2014-06-26 10:56:11	100
50040	Import Currency Rate	0	0	641	2015-02-05 17:09:43	100	\N	\N	C	Y	N	\N	2015-02-05 17:09:43	100
50043	Column - Import columns and fields	0	0	54176	2016-12-08 12:09:50	100	N	\N	C	Y	N	\N	2016-12-08 12:09:50	100
\.
