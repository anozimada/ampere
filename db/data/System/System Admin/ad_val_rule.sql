copy "ad_val_rule" ("ad_val_rule_id", "name", "ad_client_id", "ad_org_id", "code", "created", "createdby", "description", "entitytype", "isactive", "type", "updated", "updatedby") from STDIN;
105	AD_User Security validation	0	0	AD_User.AD_Client_ID=@AD_Client_ID@ AND AD_User.AD_Org_ID in (@User_Org@)	1999-07-05 00:00:00	0	Users with access to organization	System Admin	Y	S	2000-01-02 00:00:00	0
116	AD_Client Login	0	0	AD_Client.AD_Client_ID=@#AD_Client_ID@	1999-07-15 00:00:00	0	Restrict to login client	System Admin	Y	S	2000-01-02 00:00:00	0
124	C_DocType AR/AP Invoices and Credit Memos	0	0	C_DocType.DocBaseType IN ('ARI', 'API','ARC','APC') AND C_DocType.IsSOTrx='@IsSOTrx@'	2000-01-30 13:53:50	0	Document Type AR/AP Invoice and Credit Memos	System Admin	Y	S	2000-01-02 00:00:00	0
127	M_Locator of Warehouse	0	0	M_Locator.M_Warehouse_ID=@M_Warehouse_ID@	2000-02-04 11:58:58	0	\N	System Admin	Y	S	2016-04-11 16:47:03	0
131	C_BPartner_Loc	0	0	C_BPartner_Location.C_BPartner_ID=@C_BPartner_ID@	2000-05-10 18:41:17	0	\N	System Admin	Y	S	2016-04-11 16:36:03	0
136	AD_Find Column	0	0	AD_Column.AD_Table_ID=@Find_Table_ID@	2000-07-29 13:06:23	0	Columns of Table	System Admin	Y	S	2000-01-02 00:00:00	0
140	AD_Workflow_Nodes within current Workflow	0	0	AD_WF_Node.AD_WorkFlow_ID=@AD_Workflow_ID@	2000-09-04 13:52:24	0	\N	System Admin	Y	S	2007-12-17 03:01:21	0
148	AD_Org of Client (incl 0)	0	0	(AD_Org.AD_Client_ID=@AD_Client_ID@ OR AD_Org.AD_Org_ID=0)	2001-05-09 21:59:13	0	All and 0	System Admin	Y	S	2000-01-02 00:00:00	0
153	C_Region of Country	0	0	C_Region.C_Country_ID=@C_Country_ID@	2002-08-10 16:38:46	0	\N	System Admin	Y	S	2008-03-03 22:13:14	0
154	AD_PrintGraph of AD_PrintFormat_ID	0	0	AD_PrintGraph.AD_PrintFormat_ID=@AD_PrintFormat_ID@	2002-08-24 16:21:07	0	\N	System Admin	Y	S	2000-01-02 00:00:00	0
164	AD_User - Internal	0	0	EXISTS (SELECT * FROM C_BPartner bp WHERE AD_User.C_BPartner_ID=bp.C_BPartner_ID AND (bp.IsEmployee='Y' OR bp.IsSalesRep='Y'))	2003-06-03 22:52:23	0	Employees & Sales Reps	System Admin	Y	S	2000-01-02 00:00:00	0
170	AD_Org of Remote Client	0	0	AD_Org.AD_Client_ID=@Remote_Client_ID@	2003-08-03 23:23:09	0	\N	System Admin	Y	S	2008-03-05 00:50:58	0
182	AD_LabelPrinterFunction of LabelPrinter	0	0	AD_LabelPrinterFunction.AD_LabelPrinter_ID=@AD_LabelPrinter_ID@	2003-10-07 17:33:35	0	\N	System Admin	Y	S	2000-01-02 00:00:00	0
186	AD_Process_Para of AD_Process	0	0	AD_Process_Para.AD_Process_ID=@AD_Process_ID@	2004-01-02 20:09:08	0	\N	System Admin	Y	S	2000-01-02 00:00:00	0
189	M_Warehouse Org	0	0	M_Warehouse.AD_Org_ID=@AD_Org_ID@	2004-02-29 14:43:20	0	\N	System Admin	Y	S	2007-12-17 07:15:11	0
230	C_BPartner (Trx)	0	0	C_BPartner.IsActive='Y' AND C_BPartner.IsSummary='N'	2005-07-24 13:47:14	100	\N	System Admin	Y	S	2016-04-11 16:47:08	0
231	M_Product (Trx)	0	0	M_Product.IsSummary='N' AND M_Product.IsActive='Y'	2005-07-24 13:53:14	100	\N	System Admin	Y	S	2016-04-11 16:47:03	0
243	C_Job BPartner.IsEmployee	0	0	C_Job.IsEmployee=(SELECT IsEmployee FROM C_BPartner WHERE C_BPartner_ID=@C_BPartner_ID@)	2005-09-16 18:03:47	100	\N	System Admin	Y	S	2008-03-23 20:52:39	100
256	AD_Table Client+Org Access	0	0	AD_Table.AccessLevel IN ('2','3','6','7') AND AD_Table.IsView='N'	2005-11-20 16:14:13	100	\N	System Admin	Y	S	2010-03-24 22:31:45	100
271	M_PriceList is SO/PO	0	0	M_PriceList.IsSOPriceList = '@IsSOTrx@'	2007-04-26 00:00:00	100	Limits the Sales & Purchase Order window to the correct price lsits	System Admin	Y	S	2007-04-26 00:00:00	100
52005	AD_Field in Tab	0	0	AD_Field.AD_Tab_ID=@AD_Tab_ID@	2008-01-09 23:31:50	100	\N	System Admin	Y	S	2008-01-09 23:31:50	100
52006	General Workflows	0	0	AD_Workflow.WorkflowType = 'G'	2008-01-09 23:34:28	100	\N	System Admin	Y	S	2008-01-09 23:34:28	100
52007	ASP_SameModule	0	0	ASP_Module_ID=@ASP_Module_ID@	2008-01-09 23:36:26	100	\N	System Admin	Y	S	2008-01-09 23:36:26	100
52033	All_Payment Rule - No mixed	0	0	AD_Ref_List.Value <> 'M'	2008-05-22 00:00:00	100	\N	System Admin	Y	S	2008-05-22 00:00:00	100
52045	C_City of Region	0	0	((@C_Region_ID@>0 AND C_City.C_Region_ID=@C_Region_ID@) OR (@C_Region_ID@=0 AND C_City.C_Country_ID=@C_Country_ID@ AND C_City.C_Region_ID IS NULL))	2009-02-14 10:49:17	100	Cities of region if C_Region_ID is set, otherwise cities without region	System Admin	Y	S	2010-01-15 08:58:38	100
52401	AD_Org Consol	0	0	AD_Org.IsConsolAdjustOrg='Y'	2014-10-20 15:02:39	100	\N	System Admin	Y	S	2014-10-20 15:02:39	100
\.
