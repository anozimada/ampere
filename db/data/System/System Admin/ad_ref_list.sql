copy "ad_ref_list" ("ad_ref_list_id", "name", "ad_client_id", "ad_org_id", "ad_reference_id", "created", "createdby", "description", "entitytype", "isactive", "updated", "updatedby", "validfrom", "validto", "value") from STDIN;
116	Standard Calendar Period	0	0	115	1999-06-22 00:00:00	0	Periods with non overlapping calendar days	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	S
117	Adjustment Period	0	0	115	1999-06-22 00:00:00	0	Period without calendar days	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	A
120	1 Asset	0	0	117	1999-06-23 00:00:00	0	Asset (Balance Sheet) Account	System Admin	Y	2016-04-11 16:33:25	0	\N	\N	A
121	2 Liability	0	0	117	1999-06-23 00:00:00	0	Liability (Balance Sheet) Account	System Admin	Y	2016-04-11 16:33:25	0	\N	\N	L
122	4 Revenue	0	0	117	1999-06-23 00:00:00	0	Revenue (P&L) Account	System Admin	Y	2016-04-11 16:33:25	0	\N	\N	R
123	6 Expense	0	0	117	1999-06-23 00:00:00	0	Expense (P&L) Account	System Admin	Y	2016-04-11 16:33:25	0	\N	\N	E
124	3 Owner's Equity	0	0	117	1999-06-23 00:00:00	0	Owner's Equity (Balance Sheet) Account	System Admin	Y	2016-04-11 16:33:25	0	\N	\N	O
125	9 Memo	0	0	117	1999-06-23 00:00:00	0	Memo (Non Balance Sheet nor P&L) Account	System Admin	Y	2016-04-11 16:33:25	0	\N	\N	M
126	Natural	0	0	118	1999-06-23 00:00:00	0	Natural sign of the Account Type	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	N
127	Debit	0	0	118	1999-06-23 00:00:00	0	Debit Balance Account	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	D
128	Credit	0	0	118	1999-06-23 00:00:00	0	Credit Balance Account	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	C
129	Menu	0	0	120	1999-06-23 00:00:00	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	MM
130	Element Value	0	0	120	1999-06-23 00:00:00	0	Account, etc.	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	EV
131	Product	0	0	120	1999-06-23 00:00:00	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	PR
132	BPartner	0	0	120	1999-06-23 00:00:00	0	Business Partner	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	BP
153	Actual	0	0	125	1999-06-25 00:00:00	0	Actual Postings	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	A
154	Budget	0	0	125	1999-06-25 00:00:00	0	Budget Postings	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	B
155	Commitment	0	0	125	1999-06-25 00:00:00	0	External Encumbrance or Commitment Postings	System Admin	Y	2005-10-11 17:10:02	100	\N	\N	E
156	Statistical	0	0	125	1999-06-25 00:00:00	0	Statistical Postings	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	S
164	Drafted	0	0	131	1999-07-01 00:00:00	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	DR
165	Completed	0	0	131	1999-07-01 00:00:00	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	CO
166	Approved	0	0	131	1999-07-01 00:00:00	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	AP
168	Not Approved	0	0	131	1999-07-01 00:00:00	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	NA
172	Voided	0	0	131	1999-07-01 00:00:00	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	VO
173	Invalid	0	0	131	1999-07-01 00:00:00	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	IN
176	Reversed	0	0	131	1999-07-01 00:00:00	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	RE
177	Closed	0	0	131	1999-07-01 00:00:00	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	CL
178	Complete	0	0	135	1999-07-01 00:00:00	0	Generate documents and complete transaction	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	CO
179	Approve	0	0	135	1999-07-01 00:00:00	0	Approve this transaction	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	AP
180	Reject	0	0	135	1999-07-01 00:00:00	0	Reject the approval of the document. 	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	RJ
181	Post	0	0	135	1999-07-01 00:00:00	0	Post transaction	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	PO
182	Void	0	0	135	1999-07-01 00:00:00	0	Set all quantities to zero and complete transaction	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	VO
183	Close	0	0	135	1999-07-01 00:00:00	0	Finally close this transaction. It cannot be re-activated.	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	CL
184	Reverse - Correct	0	0	135	1999-07-01 00:00:00	0	Reverse Transaction (correction) by reversing sign with same date	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	RC
185	Reverse - Accrual	0	0	135	1999-07-01 00:00:00	0	Reverse by switching Dr/Cr with current date	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	RA
187	Invalidate	0	0	135	1999-07-01 00:00:00	0	Invalidate Document	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	IN
188	Re-activate	0	0	135	1999-07-01 00:00:00	0	Reopen Document and Reverse automaticly generated documents; You need to Complete the transaction after the change. 	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	RE
189	<None>	0	0	135	1999-07-01 00:00:00	0	No action	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	--
190	Unknown	0	0	131	1999-07-01 00:00:00	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	??
258	Open Period	0	0	176	1999-09-27 00:00:00	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	O
259	Close Period	0	0	176	1999-09-27 00:00:00	0	Soft close - can be re-opened	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	C
260	Permanently Close Period	0	0	176	1999-09-27 00:00:00	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	P
261	Open	0	0	177	1999-09-27 00:00:00	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	O
262	Closed	0	0	177	1999-09-27 00:00:00	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	C
263	Permanently closed	0	0	177	1999-09-27 00:00:00	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	P
264	Never opened	0	0	177	1999-09-27 00:00:00	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	N
271	<No Action>	0	0	176	1999-09-30 00:00:00	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	N
284	Organization	0	0	120	1999-10-18 00:00:00	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	OO
285	BoM	0	0	120	1999-10-18 00:00:00	0	Bill of Materials	System Admin	N	2000-01-02 00:00:00	0	\N	\N	BB
286	Project	0	0	120	1999-10-18 00:00:00	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	PJ
287	Sales Region	0	0	120	1999-10-18 00:00:00	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	SR
288	Product Category	0	0	120	1999-10-18 00:00:00	0	\N	System Admin	N	2000-01-02 00:00:00	0	\N	\N	PC
289	Campaign	0	0	120	1999-10-18 00:00:00	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	MC
302	GL Journal	0	0	183	1999-12-14 13:58:10	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	GLJ
303	GL Document	0	0	183	1999-12-14 14:04:34	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	GLD
304	AP Invoice	0	0	183	1999-12-14 14:04:49	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	API
305	AP Payment	0	0	183	1999-12-14 14:05:10	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	APP
306	AR Invoice	0	0	183	1999-12-14 14:05:23	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	ARI
307	AR Receipt	0	0	183	1999-12-14 14:05:44	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	ARR
308	Sales Order	0	0	183	1999-12-14 14:06:09	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	SOO
319	Activity	0	0	120	1999-12-22 10:37:53	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	AY
322	AR Pro Forma Invoice	0	0	183	2000-01-21 15:30:25	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	ARF
323	Material Delivery	0	0	183	2000-01-21 15:31:59	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	MMS
324	Material Receipt	0	0	183	2000-01-21 15:32:49	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	MMR
328	Material Movement	0	0	183	2000-01-29 09:27:47	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	MMM
329	Purchase Order	0	0	183	2000-01-29 09:29:07	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	POO
330	Purchase Requisition	0	0	183	2000-01-29 09:32:08	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	POR
332	Cash	0	0	195	2000-01-29 11:02:42	0	\N	System Admin	Y	2009-12-10 21:05:19	100	\N	\N	B
333	Credit Card	0	0	195	2000-01-29 11:03:07	0	\N	System Admin	Y	2009-12-10 21:05:48	100	\N	\N	K
334	EFT (AP)	0	0	195	2000-01-29 11:04:30	0	\N	System Admin	Y	2016-04-11 16:41:22	0	\N	\N	T
335	Check	0	0	195	2000-01-29 11:27:10	0	\N	System Admin	Y	2009-12-10 21:06:27	100	\N	\N	S
337	On Credit	0	0	195	2000-01-29 11:29:53	0	\N	System Admin	Y	2009-12-10 21:06:15	100	\N	\N	P
341	In Progress	0	0	131	2000-02-06 17:54:16	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	IP
345	Prepare	0	0	135	2000-04-19 17:24:00	0	Check Document conistency and check Inventory	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	PR
346	Waiting Payment	0	0	131	2000-04-26 07:57:38	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	WP
347	Unlock	0	0	135	2000-04-26 09:13:41	0	Unlock Transaction (process error)	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	XL
348	Material Physical Inventory	0	0	183	2000-05-11 22:04:47	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	MMI
361	AP Credit Memo	0	0	183	2000-06-18 12:08:03	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	APC
362	AR Credit Memo	0	0	183	2000-06-18 12:08:20	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	ARC
364	And	0	0	204	2000-07-29 13:32:13	0	\N	System Admin	Y	2007-12-17 02:28:56	0	\N	\N	A
365	Or	0	0	204	2000-07-29 13:33:03	0	\N	System Admin	Y	2007-12-17 02:29:03	0	\N	\N	O
366	 =	0	0	205	2000-07-29 13:34:40	0	\N	System Admin	Y	2007-12-17 02:31:38	0	\N	\N	==
367	>=	0	0	205	2000-07-29 14:15:14	0	\N	System Admin	Y	2007-12-17 02:31:45	0	\N	\N	>=
368	>	0	0	205	2000-07-29 14:17:39	0	\N	System Admin	Y	2007-12-17 02:31:53	0	\N	\N	>>
369	<	0	0	205	2000-07-29 14:17:49	0	\N	System Admin	Y	2007-12-17 02:32:00	0	\N	\N	<<
370	 ~	0	0	205	2000-07-29 14:18:06	0	\N	System Admin	Y	2007-12-17 02:32:07	0	\N	\N	~~
371	<=	0	0	205	2000-07-29 14:18:35	0	\N	System Admin	Y	2007-12-17 02:32:15	0	\N	\N	<=
372	|<x>|	0	0	205	2000-07-29 14:20:27	0	\N	System Admin	Y	2007-12-17 02:32:24	0	\N	\N	AB
373	sql	0	0	205	2000-07-29 14:21:36	0	\N	System Admin	Y	2007-12-17 02:32:31	0	\N	\N	SQ
409	Checking	0	0	216	2000-12-22 23:03:51	0	\N	System Admin	Y	2008-03-23 20:50:44	100	\N	\N	C
410	Savings	0	0	216	2000-12-22 23:04:29	0	\N	System Admin	Y	2008-03-23 20:50:44	100	\N	\N	S
439	Minute	0	0	221	2001-01-11 19:09:32	0	\N	System Admin	Y	2008-03-05 00:54:27	0	\N	\N	M
440	Hour	0	0	221	2001-01-11 19:09:58	0	\N	System Admin	Y	2008-03-05 00:54:27	0	\N	\N	H
441	Day	0	0	221	2001-01-11 19:10:43	0	\N	System Admin	Y	2008-03-05 00:54:27	0	\N	\N	D
458	Not Posted	0	0	234	2001-04-27 18:18:35	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	N
459	Posted	0	0	234	2001-04-27 18:18:57	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	Y
460	Not Balanced	0	0	234	2001-04-27 18:20:20	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	b
461	Not Convertible (no rate)	0	0	234	2001-04-27 18:55:49	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	c
462	Period Closed	0	0	234	2001-04-27 18:56:26	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	p
463	Post Prepared	0	0	234	2001-04-27 18:57:17	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	y
464	Invalid Account	0	0	234	2001-04-27 18:58:53	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	i
465	Bank Statement	0	0	183	2001-04-27 19:03:51	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	CMB
466	Cash Journal	0	0	183	2001-04-27 19:04:42	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	CMC
467	Payment Allocation	0	0	183	2001-04-27 19:05:18	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	CMA
482	Normal (Flat)	0	0	243	2001-09-05 21:36:25	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	F
483	Gradient	0	0	243	2001-09-05 21:36:39	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	G
484	Line	0	0	243	2001-09-05 21:36:50	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	L
485	Texture (Picture)	0	0	243	2001-09-05 21:37:06	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	T
491	Material Production	0	0	183	2001-12-01 10:43:12	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	MMP
492	Match Invoice	0	0	183	2001-12-01 10:44:17	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	MXI
493	Match PO	0	0	183	2001-12-01 10:44:57	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	MXP
501	North	0	0	248	2002-01-17 21:29:27	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	1
502	North East	0	0	248	2002-01-17 21:29:41	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	2
503	East	0	0	248	2002-01-17 21:29:54	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	3
504	South East	0	0	248	2002-01-17 21:30:10	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	4
505	South	0	0	248	2002-01-17 21:30:21	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	5
506	South West	0	0	248	2002-01-17 21:30:44	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	6
507	West	0	0	248	2002-01-17 21:30:57	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	7
508	North West	0	0	248	2002-01-17 21:31:17	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	8
517	Default	0	0	253	2002-07-11 19:22:15	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	D
518	Leading (left)	0	0	253	2002-07-11 19:22:43	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	L
519	Trailing (right)	0	0	253	2002-07-11 19:23:34	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	T
520	Block	0	0	253	2002-07-11 19:24:26	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	B
521	Leading (left)	0	0	254	2002-07-11 20:50:22	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	L
522	Center	0	0	254	2002-07-11 20:51:08	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	C
523	Trailing (right)	0	0	254	2002-07-11 20:51:32	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	T
524	Center	0	0	253	2002-07-11 20:52:33	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	C
525	Field	0	0	255	2002-07-11 20:53:37	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	F
526	Text	0	0	255	2002-07-11 20:54:11	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	T
527	Print Format	0	0	255	2002-07-11 20:55:33	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	P
528	Content	0	0	256	2002-07-11 20:56:16	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	C
529	Header	0	0	256	2002-07-11 20:56:43	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	H
530	Footer	0	0	256	2002-07-11 20:57:06	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	F
531	None	0	0	254	2002-07-24 16:34:34	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	X
532	Image	0	0	255	2002-08-12 19:38:14	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	I
533	Pie Chart	0	0	265	2002-08-24 16:15:20	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	P
534	Line Chart	0	0	265	2002-08-24 16:15:44	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	L
535	Bar Chart	0	0	265	2002-08-24 16:16:10	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	B
557	Field	0	0	280	2003-05-30 00:22:11	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	F
558	Text	0	0	280	2003-05-30 00:22:27	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	T
559	Invoice	0	0	282	2003-06-04 01:38:22	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	I
560	Daily	0	0	283	2003-06-04 01:41:14	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	D
561	Weekly	0	0	283	2003-06-04 01:41:35	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	W
562	Monthly	0	0	283	2003-06-04 01:42:17	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	M
563	Quarterly	0	0	283	2003-06-04 01:42:50	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	Q
564	Order	0	0	282	2003-06-04 01:44:47	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	O
565	GL Journal Batch	0	0	282	2003-06-04 01:45:27	0	\N	System Admin	Y	2016-04-11 16:27:31	0	\N	\N	G
567	Project	0	0	282	2003-06-04 01:46:24	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	J
584	Project Issue	0	0	183	2003-09-03 14:22:24	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	PJI
648	Re-Create Translation	0	0	311	2004-01-16 14:23:52	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	R
649	Add Missing Translations	0	0	311	2004-01-16 14:24:19	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	A
650	Delete Translation	0	0	311	2004-01-16 14:25:11	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	D
652	EFT (AR)	0	0	195	2004-01-21 19:09:35	0	\N	System Admin	Y	2016-04-11 16:41:22	0	\N	\N	D
653	Solid Line	0	0	312	2004-01-27 12:45:55	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	S
654	Dashed Line	0	0	312	2004-01-27 12:47:33	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	D
655	Dotted Line	0	0	312	2004-01-27 12:47:57	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	d
656	Dash-Dotted Line	0	0	312	2004-01-27 12:50:35	0	\N	System Admin	Y	2000-01-02 00:00:00	0	\N	\N	2
670	Waiting Confirmation	0	0	131	2004-03-21 00:30:10	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	WC
672	!=	0	0	205	2004-03-23 01:44:18	0	\N	System Admin	Y	2007-12-17 02:32:39	0	\N	\N	!=
684	Posting Error	0	0	234	2004-06-13 23:06:06	0	\N	System Admin	Y	2019-02-04 13:38:35	0	\N	\N	E
691	Wait Complete	0	0	135	2004-08-23 21:50:34	0	Wait Condition ok Complete Docuement	System Admin	Y	2008-03-23 20:57:17	100	\N	\N	WC
702	Rectangle	0	0	255	2005-01-06 18:16:08	100	\N	System Admin	Y	2005-01-07 14:44:53	100	\N	\N	R
703	Line	0	0	255	2005-01-06 18:16:21	100	\N	System Admin	Y	2005-01-06 18:16:47	100	\N	\N	L
704	3D Rectangle	0	0	333	2005-01-06 19:40:33	100	\N	System Admin	Y	2005-01-06 19:43:42	100	\N	\N	3
705	Oval	0	0	333	2005-01-06 19:40:56	100	\N	System Admin	Y	2005-01-06 19:40:56	100	\N	\N	O
706	Round Rectangle	0	0	333	2005-01-06 19:42:27	100	\N	System Admin	Y	2005-01-06 19:42:27	100	\N	\N	R
707	Normal Rectangle	0	0	333	2005-01-06 19:43:29	100	\N	System Admin	Y	2005-01-06 19:43:33	100	\N	\N	N
708	None	0	0	334	2005-01-10 15:01:23	100	\N	System Admin	Y	2005-01-10 15:01:23	100	\N	\N	N
709	All (Reports, Documents)	0	0	334	2005-01-10 15:02:16	100	\N	System Admin	Y	2005-01-10 15:03:05	100	\N	\N	1
710	Documents	0	0	334	2005-01-10 15:02:28	100	\N	System Admin	Y	2005-01-10 15:02:59	100	\N	\N	2
711	External Documents	0	0	334	2005-01-10 15:02:40	100	\N	System Admin	Y	2005-02-09 20:36:48	100	\N	\N	3
712	LiFo	0	0	335	2005-03-10 21:03:13	100	\N	System Admin	Y	2005-03-10 21:03:13	100	\N	\N	L
713	FiFo	0	0	335	2005-03-10 21:03:24	100	\N	System Admin	Y	2005-03-10 21:03:24	100	\N	\N	F
727	Order Acknowledgement	0	0	342	2005-05-01 02:48:44	100	\N	System Admin	Y	2005-05-01 02:48:44	100	\N	\N	OA
728	Payment Acknowledgement	0	0	342	2005-05-01 02:49:02	100	\N	System Admin	Y	2005-05-01 02:49:02	100	\N	\N	PA
729	Payment Error	0	0	342	2005-05-01 02:49:29	100	\N	System Admin	Y	2005-05-01 02:49:29	100	\N	\N	PE
730	User Verification	0	0	342	2005-05-01 02:50:06	100	\N	System Admin	Y	2006-05-03 12:49:15	100	\N	\N	UV
731	User Password	0	0	342	2005-05-01 02:53:33	100	\N	System Admin	Y	2005-05-01 02:53:33	100	\N	\N	UP
732	EMail	0	0	344	2005-05-13 22:32:59	100	\N	System Admin	Y	2008-03-23 20:52:52	100	\N	\N	E
733	Notice	0	0	344	2005-05-13 22:33:28	100	\N	System Admin	Y	2008-03-23 20:52:52	100	\N	\N	N
734	None	0	0	344	2005-05-13 22:33:41	100	\N	System Admin	Y	2008-03-23 20:52:51	100	\N	\N	X
735	EMail+Notice	0	0	344	2005-05-13 22:34:04	100	\N	System Admin	Y	2008-03-23 20:52:51	100	\N	\N	B
736	Hourly	0	0	346	2005-05-15 01:30:15	100	\N	System Admin	Y	2005-05-15 01:30:15	100	\N	\N	H
737	Daily	0	0	346	2005-05-15 01:30:30	100	\N	System Admin	Y	2005-05-15 01:30:30	100	\N	\N	D
738	Weekly	0	0	346	2005-05-15 01:30:41	100	\N	System Admin	Y	2005-05-15 01:30:41	100	\N	\N	W
739	Monthly	0	0	346	2005-05-15 01:30:59	100	\N	System Admin	Y	2005-05-15 01:30:59	100	\N	\N	M
740	Twice Monthly	0	0	346	2005-05-15 01:31:59	100	\N	System Admin	Y	2005-05-15 01:31:59	100	\N	\N	T
741	Bi-Weekly	0	0	346	2005-05-15 01:32:37	100	\N	System Admin	Y	2005-05-15 01:32:37	100	\N	\N	B
764	Subscribe	0	0	342	2005-07-13 18:17:19	0	\N	System Admin	Y	2005-07-13 18:20:48	0	\N	\N	LS
765	UnSubscribe	0	0	342	2005-07-13 18:21:04	0	\N	System Admin	Y	2005-07-13 18:38:07	0	\N	\N	LU
766	User Account	0	0	342	2005-07-13 18:37:57	0	\N	System Admin	Y	2005-07-13 18:37:57	0	\N	\N	UA
767	Request	0	0	342	2005-07-13 18:42:42	0	\N	System Admin	Y	2005-07-13 18:42:42	0	\N	\N	WR
783	Reservation	0	0	125	2005-10-11 17:09:49	100	Internal Commitment	System Admin	Y	2005-10-11 17:09:49	100	\N	\N	R
802	LAN	0	0	364	2005-11-19 16:30:39	100	\N	System Admin	Y	2008-03-23 20:52:41	100	\N	\N	L
803	Terminal Server	0	0	364	2005-11-19 16:30:54	100	\N	System Admin	Y	2008-03-23 20:52:42	100	\N	\N	T
804	VPN	0	0	364	2005-11-19 16:31:06	100	\N	System Admin	Y	2008-03-23 20:52:42	100	\N	\N	V
805	WAN	0	0	364	2005-11-19 16:31:26	100	\N	System Admin	Y	2008-03-23 20:52:42	100	\N	\N	W
806	Client (all shared)	0	0	365	2005-11-20 16:17:54	100	\N	System Admin	Y	2005-11-20 16:17:54	100	\N	\N	C
807	Org (not shared)	0	0	365	2005-11-20 16:18:14	100	\N	System Admin	Y	2005-11-20 16:18:14	100	\N	\N	O
808	Client or Org	0	0	365	2005-11-20 16:18:35	100	\N	System Admin	Y	2005-11-20 16:18:35	100	\N	\N	x
850	Evaluation	0	0	374	2005-12-31 16:09:24	100	\N	System Admin	Y	2005-12-31 16:09:24	100	\N	\N	E
851	Implementation	0	0	374	2005-12-31 16:09:36	100	\N	System Admin	Y	2005-12-31 16:09:36	100	\N	\N	I
852	Production	0	0	374	2005-12-31 16:09:48	100	\N	System Admin	Y	2005-12-31 16:09:48	100	\N	\N	P
855	MM	0	0	375	2006-01-17 18:45:08	100	\N	System Admin	Y	2006-01-17 18:45:08	100	\N	\N	M
856	Inch	0	0	375	2006-01-17 18:45:36	100	\N	System Admin	Y	2006-01-17 18:45:36	100	\N	\N	I
859	Codabar 2 of 7 linear	0	0	377	2006-02-14 13:54:17	100	\N	System Admin	Y	2006-02-16 15:16:02	100	\N	\N	2o9
860	Code 39  3 of 9 linear w/o Checksum	0	0	377	2006-02-14 13:54:43	100	\N	System Admin	Y	2006-02-16 15:14:40	100	\N	\N	3o9
861	Codeabar linear	0	0	377	2006-02-14 14:00:45	100	\N	System Admin	Y	2006-02-16 15:16:57	100	\N	\N	COD
862	Code 128 dynamically switching	0	0	377	2006-02-14 14:01:47	100	Barcode that dynamically switches between character sets to give the smallest possible encoding	System Admin	Y	2006-02-16 15:16:48	100	\N	\N	C28
863	Code 128 A character set	0	0	377	2006-02-14 14:02:20	100	\N	System Admin	Y	2006-02-14 14:15:27	100	\N	\N	28A
864	Code 128 B character set	0	0	377	2006-02-14 14:02:44	100	\N	System Admin	Y	2006-02-14 14:17:59	100	\N	\N	28B
865	Code 128 C character set	0	0	377	2006-02-14 14:03:07	100	\N	System Admin	Y	2006-02-14 14:15:48	100	\N	\N	28C
866	Code 39 linear with Checksum	0	0	377	2006-02-14 14:03:27	100	\N	System Admin	Y	2006-02-16 15:41:58	100	\N	\N	C39
867	EAN 128	0	0	377	2006-02-14 14:03:44	100	\N	System Admin	Y	2006-02-16 15:27:58	100	\N	\N	E28
868	Global Trade Item No GTIN UCC/EAN 128	0	0	377	2006-02-14 14:04:26	100	Global Trade Name	System Admin	Y	2006-02-14 14:19:50	100	\N	\N	GTN
869	Codabar Monarch linear	0	0	377	2006-02-14 14:04:46	100	\N	System Admin	Y	2006-02-16 15:16:12	100	\N	\N	MON
870	Codabar NW-7 linear	0	0	377	2006-02-14 14:05:08	100	\N	System Admin	Y	2006-02-16 15:16:28	100	\N	\N	NW7
871	PDF417 two dimensional	0	0	377	2006-02-14 14:05:35	100	\N	System Admin	Y	2006-02-16 15:17:34	100	\N	\N	417
872	SCC-14 shipping code UCC/EAN 128	0	0	377	2006-02-14 14:06:06	100	\N	System Admin	Y	2006-02-14 14:16:11	100	\N	\N	C14
873	Shipment ID number UCC/EAN 128	0	0	377	2006-02-14 14:06:34	100	\N	System Admin	Y	2006-02-14 14:17:02	100	\N	\N	SID
874	UCC 128	0	0	377	2006-02-14 14:06:55	100	\N	System Admin	Y	2006-02-16 15:41:32	100	\N	\N	U28
875	Code 39 USD3 with Checksum	0	0	377	2006-02-14 14:07:20	100	\N	System Admin	Y	2006-02-16 15:14:59	100	\N	\N	US3
876	Codabar USD-4 linear	0	0	377	2006-02-14 14:07:48	100	\N	System Admin	Y	2006-02-16 15:16:35	100	\N	\N	US4
877	US Postal Service UCC/EAN 128	0	0	377	2006-02-14 14:08:31	100	\N	System Admin	Y	2006-02-14 14:17:38	100	\N	\N	USP
878	SSCC-18 number UCC/EAN 128	0	0	377	2006-02-14 14:11:10	100	\N	System Admin	Y	2006-02-14 14:20:32	100	\N	\N	C18
879	Code 39 USD3 w/o Checksum	0	0	377	2006-02-16 15:12:48	100	\N	System Admin	Y	2006-02-16 15:12:48	100	\N	\N	us3
880	Code 39  3 of 9 linear with Checksum	0	0	377	2006-02-16 15:13:48	100	\N	System Admin	Y	2006-02-16 15:14:46	100	\N	\N	3O9
882	Code 39 linear w/o Checksum	0	0	377	2006-02-16 15:42:17	100	\N	System Admin	Y	2006-02-16 15:42:17	100	\N	\N	c39
884	User 1	0	0	120	2006-03-26 14:41:06	100	\N	System Admin	Y	2006-03-26 14:41:06	100	\N	\N	U1
885	User 2	0	0	120	2006-03-26 14:41:18	100	\N	System Admin	Y	2006-03-26 14:41:18	100	\N	\N	U2
886	User 3	0	0	120	2006-03-26 14:41:29	100	\N	System Admin	Y	2006-03-26 14:41:29	100	\N	\N	U3
887	User 4	0	0	120	2006-03-26 14:41:51	100	\N	System Admin	Y	2006-03-26 14:41:51	100	\N	\N	U4
52000	Mixed	0	0	195	2008-05-22 00:00:00	100	\N	System Admin	Y	2008-05-22 00:00:00	100	\N	\N	M
53228	System	0	0	53222	2007-12-15 12:35:20	100	Just allowed system configuration	System Admin	Y	2007-12-15 12:35:20	100	\N	\N	S
53229	Client	0	0	53222	2007-12-15 12:35:42	100	Allowed system and client configuration	System Admin	Y	2007-12-15 12:35:42	100	\N	\N	C
53230	Organization	0	0	53222	2007-12-15 12:36:00	100	Allowed system, client and organization configuration	System Admin	Y	2007-12-15 12:36:00	100	\N	\N	O
53232	Maintenance Order	0	0	183	2007-12-16 22:24:14	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	MOF
53235	Manufacturing Order	0	0	183	2007-12-16 22:24:17	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	MOP
53239	Quality Order	0	0	183	2007-12-16 22:24:21	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	MQO
53240	Payroll	0	0	183	2007-12-16 22:24:22	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	HRP
53241	Distribution Order	0	0	183	2007-12-16 22:24:23	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	DOO
53284	Hide	0	0	53234	2008-01-09 23:30:37	100	\N	System Admin	Y	2008-01-09 23:30:37	100	\N	\N	H
53285	Show	0	0	53234	2008-01-09 23:30:38	100	\N	System Admin	Y	2008-01-09 23:30:38	100	\N	\N	S
53286	Undefined	0	0	53234	2008-01-09 23:30:39	100	\N	System Admin	Y	2008-01-09 23:30:39	100	\N	\N	U
53288	EAN 13	0	0	377	2008-01-21 01:38:27	100	\N	System Admin	Y	2008-01-21 01:38:27	100	\N	\N	E13
53453	Manufacturing Cost Collector	0	0	183	2009-02-13 11:22:06	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	MCC
53501	HTML Table	0	0	53316	2009-07-17 18:31:35	100	\N	System Admin	Y	2009-07-17 18:31:35	100	\N	\N	T
53502	Chart	0	0	53316	2009-07-17 18:31:59	100	\N	System Admin	Y	2009-07-17 18:31:59	100	\N	\N	C
53507	Warehouse Management Order	0	0	183	2009-09-03 20:46:19	0	\N	System Admin	Y	2019-02-04 13:38:24	0	\N	\N	WMO
53552	Order	0	0	53331	2009-11-13 15:10:11	100	\N	System Admin	Y	2009-11-13 15:10:11	100	\N	\N	Order
53553	Invoice	0	0	53331	2009-11-13 15:10:21	100	\N	System Admin	Y	2009-11-13 15:10:21	100	\N	\N	Invoice
53554	Implicit	0	0	53332	2009-11-13 15:15:31	100	\N	System Admin	Y	2009-11-13 15:15:31	100	\N	\N	I
53555	Explicit	0	0	53332	2009-11-13 15:16:19	100	\N	System Admin	N	2009-11-13 15:16:19	100	\N	\N	E
53705	Complete	0	0	53411	2011-07-19 14:55:28	100	Generate documents and complete transaction	System Admin	Y	2011-07-19 14:55:28	100	\N	\N	CO
53706	Post	0	0	53411	2011-07-19 14:55:43	100	Post transaction	System Admin	Y	2011-07-19 14:55:43	100	\N	\N	PO
53707	Prepare	0	0	53411	2011-07-19 14:56:06	100	Check Document consistency and check inventory	System Admin	Y	2011-07-19 14:56:06	100	\N	\N	PR
53716	Cold Call	0	0	53415	2011-09-01 20:50:41	100	\N	System Admin	Y	2011-09-01 20:50:41	100	\N	\N	CC
53717	Existing Customer	0	0	53415	2011-09-01 20:50:50	100	\N	System Admin	Y	2011-09-01 20:50:50	100	\N	\N	EC
53718	Employee	0	0	53415	2011-09-01 20:51:02	100	\N	System Admin	Y	2011-09-01 20:51:02	100	\N	\N	EM
53719	Partner	0	0	53415	2011-09-01 20:51:11	100	\N	System Admin	Y	2011-09-01 20:51:11	100	\N	\N	PT
53720	Conference	0	0	53415	2011-09-01 20:51:23	100	\N	System Admin	Y	2011-09-01 20:51:23	100	\N	\N	CN
53721	Trade Show	0	0	53415	2011-09-01 20:51:32	100	\N	System Admin	Y	2011-09-01 20:51:32	100	\N	\N	TS
53722	Web Site	0	0	53415	2011-09-01 20:51:43	100	\N	System Admin	Y	2011-09-01 20:51:43	100	\N	\N	WS
53723	Word of Mouth	0	0	53415	2011-09-01 20:51:54	100	\N	System Admin	Y	2011-09-01 20:51:54	100	\N	\N	WM
53725	Email	0	0	53415	2011-09-01 20:52:34	100	\N	System Admin	Y	2011-09-01 20:52:34	100	\N	\N	EL
53726	New	0	0	53416	2011-09-01 21:12:36	100	\N	System Admin	Y	2011-09-01 21:12:36	100	\N	\N	N
53727	Working	0	0	53416	2011-09-01 21:12:46	100	\N	System Admin	Y	2011-09-01 21:12:46	100	\N	\N	W
53728	Expired	0	0	53416	2011-09-01 21:12:54	100	\N	System Admin	Y	2011-09-01 21:12:54	100	\N	\N	E
53729	Recycled	0	0	53416	2011-09-01 21:13:10	100	\N	System Admin	Y	2011-09-01 21:13:10	100	\N	\N	R
53748	Converted	0	0	53416	2011-10-26 16:40:18	100	\N	System Admin	Y	2011-10-26 16:40:18	100	\N	\N	C
53758	Database	0	0	53428	2012-01-31 16:59:29	100	\N	System Admin	Y	2012-01-31 16:59:29	100	\N	\N	0
53759	FileSystem	0	0	53428	2012-01-31 16:59:42	100	\N	System Admin	Y	2012-01-31 16:59:42	100	\N	\N	1
53760	Document Management System	0	0	53428	2012-01-31 17:00:00	100	\N	System Admin	Y	2012-01-31 17:00:00	100	\N	\N	2
54394	Timesheet Entry	0	0	183	2014-03-19 15:24:42	100	\N	System Admin	Y	2014-03-19 15:24:42	100	\N	\N	STE
54405	GL Journal	0	0	282	2014-06-05 16:41:59	100	\N	System Admin	Y	2014-06-05 16:41:59	100	\N	\N	N
54460	HTML	0	0	53719	2014-08-28 18:03:18	100	\N	System Admin	Y	2014-08-28 18:03:18	100	\N	\N	H
54461	PDF	0	0	53719	2014-08-28 18:03:27	100	\N	System Admin	Y	2014-08-28 18:03:27	100	\N	\N	P
54462	XLS	0	0	53719	2014-08-28 18:03:37	100	\N	System Admin	Y	2014-08-28 18:03:37	100	\N	\N	X
54625	Manufacturing Planned Order	0	0	183	2016-01-15 12:19:04	100	\N	System Admin	Y	2016-01-15 12:19:04	100	\N	\N	MPO
54628	Manufacturing PO Requisition	0	0	183	2016-02-03 14:45:04	100	\N	System Admin	Y	2016-02-03 14:45:04	100	\N	\N	MPR
54629	XLSX	0	0	53719	2016-03-18 12:52:08	100	\N	System Admin	Y	2016-03-18 12:52:08	100	\N	\N	XX
54859	Attachment	0	0	53874	2017-03-03 15:27:34	100	\N	System Admin	Y	2017-03-03 15:27:34	100	\N	\N	ATT
54860	Archive	0	0	53874	2017-03-03 15:27:47	100	\N	System Admin	Y	2017-03-03 15:27:47	100	\N	\N	ARC
55055	Info	0	0	104	2017-11-30 17:56:48	100	Info Window	System Admin	Y	2017-11-30 17:56:48	100	\N	\N	I
55581	CSV	0	0	53719	2021-04-27 08:58:16	100	\N	System Admin	Y	2021-04-27 08:58:16	100	\N	\N	C
400001	Insert PO	0	0	400001	2022-03-07 17:46:57	100	\N	System Admin	Y	2022-03-07 17:47:17	100	\N	\N	po.insert
400002	Update PO	0	0	400001	2022-03-07 17:47:29	100	\N	System Admin	Y	2022-03-07 17:47:54	100	\N	\N	po.update
400003	Delete PO	0	0	400001	2022-03-07 17:47:41	100	\N	System Admin	Y	2022-03-07 17:47:49	100	\N	\N	po.delete
\.
