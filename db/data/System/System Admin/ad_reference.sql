copy "ad_reference" ("ad_reference_id", "name", "validationtype", "ad_client_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "isorderbyvalue", "updated", "updatedby", "vformat") from STDIN;
102	AD_Message	T	0	0	1999-05-21 00:00:00	0	Message selection	System Admin	\N	Y	N	2007-12-17 04:50:34	0	\N
105	AD_Menu Parent	T	0	0	1999-05-21 00:00:00	0	Menu Parent selection	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
112	C_Currencies	T	0	0	1999-06-22 00:00:00	0	Currencies with dynamic exchange rate selection	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
114	C_UOM	T	0	0	1999-06-22 00:00:00	0	Unit of Measure selection	System Admin	\N	Y	N	2016-04-11 16:47:09	0	\N
115	C_Period Type	L	0	0	1999-06-22 00:00:00	0	Period Type list	System Admin	\N	Y	N	2000-01-02 00:00:00	0	L
117	C_ElementValue AccountType	L	0	0	1999-06-23 00:00:00	0	Account Type list	System Admin	\N	Y	N	2016-04-11 16:36:21	0	L
118	C_ElementValue Account Sign	L	0	0	1999-06-23 00:00:00	0	Account Sign list	System Admin	\N	Y	N	2008-03-23 20:54:34	100	L
120	AD_TreeType Type	L	0	0	1999-06-23 00:00:00	0	Tree Type list	System Admin	Determines which element to use as the base for the information	Y	N	2000-01-02 00:00:00	0	LL
125	_Posting Type	L	0	0	1999-06-25 00:00:00	0	Posting Type (Actual Budget etc.) list	System Admin	\N	Y	N	2016-04-11 16:36:25	0	L
128	AD_Sequence for Documents	T	0	0	1999-06-29 00:00:00	0	Sequence for Document selection	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
129	AD_Client	T	0	0	1999-07-01 00:00:00	0	Client selection	System Admin	\N	Y	N	2016-04-11 16:47:36	0	\N
130	AD_Org (Trx)	T	0	0	1999-07-01 00:00:00	0	Organization selection, no summary, no 0	System Admin	\N	Y	N	2019-02-04 13:38:37	0	\N
131	_Document Status	L	0	0	1999-07-01 00:00:00	0	Document Status list	System Admin	\N	Y	N	2019-02-04 13:38:35	0	LL
135	_Document Action	L	0	0	1999-07-01 00:00:00	0	Document action list	System Admin	\N	Y	N	2019-02-04 13:38:35	0	LL
136	C_AcctSchema	T	0	0	1999-07-07 00:00:00	0	Accounting Schema selection	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
138	C_BPartner (Trx)	T	0	0	1999-07-07 00:00:00	0	Business Partner selection (no Summary)	System Admin	\N	Y	N	2019-02-04 13:38:24	0	\N
143	C_Campaign (No summary)	T	0	0	1999-07-07 00:00:00	0	Campaign selection	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
157	C_Region	T	0	0	1999-08-09 00:00:00	0	Region selection	System Admin	\N	Y	N	2008-03-03 22:13:10	0	\N
158	C_Tax	T	0	0	1999-08-09 00:00:00	0	Tax selection	System Admin	\N	Y	N	2008-03-03 22:13:06	0	\N
162	M_Product (no summary)	T	0	0	1999-08-10 00:00:00	0	Product selection no summary	System Admin	\N	Y	N	2016-04-11 16:47:40	0	\N
163	M_Product Category 	T	0	0	1999-08-10 00:00:00	0	\N	System Admin	\N	Y	N	2016-04-11 16:47:39	0	\N
170	C_DocType	T	0	0	1999-08-10 00:00:00	0	\N	System Admin	\N	Y	N	2019-02-04 13:38:28	0	\N
172	C_DocType SO	T	0	0	1999-08-10 00:00:00	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
176	C_PeriodControl Action	L	0	0	1999-09-27 00:00:00	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	L
177	C_PeriodControl Status	L	0	0	1999-09-27 00:00:00	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	L
182	C_ElementValue (all)	T	0	0	1999-10-11 00:00:00	0	Element Values	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
183	C_DocType DocBaseType	L	0	0	1999-12-01 20:19:19	0	Base Document Types	System Admin	\N	Y	N	2019-02-04 13:38:24	0	LLL
184	AD_Tree	T	0	0	1999-12-09 08:31:27	0	Tree selection	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
190	AD_User - SalesRep	T	0	0	1999-12-22 14:35:16	0	Sales Representative	System Admin	\N	Y	N	2019-02-04 13:38:37	0	\N
195	_Payment Rule	L	0	0	2000-01-29 10:56:59	0	In & Out Payment Options	System Admin	\N	Y	N	2008-03-23 20:47:54	100	\N
197	M_Warehouse of Client	T	0	0	2000-02-06 19:58:33	0	\N	System Admin	\N	Y	N	2016-04-11 16:47:39	0	\N
200	C_Charge	T	0	0	2000-02-12 17:22:32	0	\N	System Admin	\N	Y	N	2019-02-04 13:38:28	0	\N
204	AD_Find AndOr	L	0	0	2000-07-16 11:43:05	0	\N	System Admin	\N	Y	N	2007-12-17 02:28:49	0	L
205	AD_Find Operation	L	0	0	2000-07-16 12:02:18	0	\N	System Admin	\N	Y	N	2007-12-17 02:31:31	0	CC
216	C_Bank Account Type	L	0	0	2000-12-22 23:03:37	0	\N	System Admin	\N	Y	N	2008-03-23 20:50:44	100	\N
221	_Frequency Type	L	0	0	2001-01-11 19:09:15	0	Processor Frequency Type	System Admin	\N	Y	N	2008-03-05 00:54:27	0	L
234	_Posted Status	L	0	0	2001-04-27 18:18:02	0	\N	System Admin	\N	Y	N	2019-02-04 13:38:35	0	c
243	AD_Color Type	L	0	0	2001-09-05 21:34:59	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
248	AD_Color StartPoint	L	0	0	2002-01-17 21:29:14	0	Nord-West-..	System Admin	\N	Y	N	2000-01-02 00:00:00	0	0
253	AD_Print Field Alignment	L	0	0	2002-07-11 19:20:30	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
254	AD_Print Line Alignment	L	0	0	2002-07-11 20:50:04	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
255	AD_Print Format Type	L	0	0	2002-07-11 20:53:18	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
256	AD_Print Area	L	0	0	2002-07-11 20:56:01	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
259	AD_PrintFormat	T	0	0	2002-07-11 21:40:46	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
261	AD_PrintFormat Invoice	T	0	0	2002-08-04 19:22:44	0	\N	System Admin	\N	Y	N	2008-03-23 20:47:22	100	\N
262	AD_PrintFormat Order	T	0	0	2002-08-04 19:25:28	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
263	AD_PrintFormat Shipment	T	0	0	2002-08-04 19:27:01	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
264	AD_PrintFormatItem	T	0	0	2002-08-24 16:11:48	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
265	AD_Print Graph Type	L	0	0	2002-08-24 16:14:58	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
266	AD_PrintColor	T	0	0	2002-08-24 16:30:02	0	\N	System Admin	\N	Y	N	2016-04-11 16:44:27	0	\N
267	AD_PrintFont	T	0	0	2002-08-24 16:30:38	0	\N	System Admin	\N	Y	N	2016-04-11 16:44:28	0	\N
268	AD_PrintFormat Check	T	0	0	2002-08-24 18:15:27	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
272	I_ElementValue Column	T	0	0	2003-01-11 19:04:48	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
274	R_MailText	T	0	0	2003-02-03 20:44:01	0	Mail Text	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
276	AD_Org (all)	T	0	0	2003-02-11 23:36:29	0	Organization selection	System Admin	\N	Y	N	2016-04-11 16:36:21	0	\N
280	AD_Print Label Line Type	L	0	0	2003-05-28 23:31:38	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
282	C_Recurring Type	L	0	0	2003-06-04 01:38:02	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
283	C_Recurring Frequency	L	0	0	2003-06-04 01:40:58	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
286	AD_User - Internal	T	0	0	2003-07-21 23:27:29	0	Employee or SalesRep	System Admin	\N	Y	N	2008-03-23 20:54:10	100	\N
311	AD_Language Maintenance	L	0	0	2004-01-16 14:23:19	0	\N	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
312	AD_PrintTableFormat Stroke	L	0	0	2004-01-27 12:44:48	0	Stroke Type	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
322	AD_Org (all but 0)	T	0	0	2003-02-11 23:36:29	0	Organization selection	System Admin	\N	Y	N	2000-01-02 00:00:00	0	\N
327	AD_Language System	T	0	0	1999-05-21 00:00:00	0	Language selection	System Admin	\N	Y	N	2008-03-23 20:48:22	100	\N
333	AD_PrintFormatItem ShapeType	L	0	0	2005-01-06 19:40:15	100	\N	System Admin	\N	Y	N	2005-01-06 19:40:15	100	\N
334	AD_Client AutoArchive	L	0	0	2005-01-10 15:01:11	100	\N	System Admin	\N	Y	N	2005-01-10 15:01:11	100	\N
335	_MMPolicy	L	0	0	2005-03-10 21:03:02	100	Material Movement Policy	System Admin	\N	Y	N	2005-03-10 21:03:02	100	\N
342	W_MailMsg Type	L	0	0	2005-05-01 02:48:14	100	\N	System Admin	\N	Y	N	2005-05-01 02:48:14	100	\N
344	AD_User NotificationType	L	0	0	2005-05-13 22:32:46	100	\N	System Admin	\N	Y	N	2008-03-23 20:52:51	100	\N
346	C_Remuneration Type	L	0	0	2005-05-15 01:30:01	100	\N	System Admin	\N	Y	N	2005-05-15 01:30:01	100	\N
364	AD_User ConnectionProfile	L	0	0	2005-11-19 16:30:18	100	\N	System Admin	\N	Y	N	2008-03-23 20:52:40	100	\N
365	AD_Client ShareType	L	0	0	2005-11-20 16:17:20	100	\N	System Admin	\N	Y	N	2005-11-20 16:17:20	100	\N
374	AD_System Status	L	0	0	2005-12-31 16:09:11	100	\N	System Admin	\N	Y	N	2005-12-31 16:09:11	100	\N
375	AD_PrintPaper Units	L	0	0	2006-01-17 18:44:56	100	\N	System Admin	\N	Y	N	2006-01-17 18:44:56	100	\N
377	AD_PrintFormatItem BarcodeType	L	0	0	2006-02-14 13:38:24	100	\N	System Admin	\N	Y	N	2006-02-16 15:07:08	100	\N
400	AD_Process_JasperReports	T	0	0	2007-02-27 00:00:00	0	\N	System Admin	\N	Y	N	2007-02-27 00:00:00	0	\N
52001	C_Bank	T	0	0	2008-05-26 00:00:00	100	\N	System Admin	\N	Y	N	2008-05-26 00:00:00	100	\N
53222	AD_SysConfig ConfigurationLevel	L	0	0	2007-12-15 12:34:44	100	Configuration Level	System Admin	\N	Y	N	2007-12-17 01:54:14	0	\N
53234	ASP_Status	L	0	0	2008-01-09 23:30:36	100	\N	System Admin	\N	Y	N	2008-01-09 23:30:36	100	\N
53258	A_Asset_ID	T	0	0	2008-05-30 16:38:16	100	Asset ID	System Admin	\N	Y	N	2016-04-11 16:36:02	0	\N
53281	AD_PrintFormat Manufacturing Order	T	0	0	2008-07-25 01:18:14	0	\N	System Admin	\N	Y	N	2008-07-25 01:29:25	0	\N
53282	AD_PrintFormat Distribution Order	T	0	0	2008-07-25 01:29:44	0	\N	System Admin	\N	Y	N	2008-07-25 01:29:44	0	\N
53316	PA_DashboardContent GoalDisplay	L	0	0	2009-07-17 18:31:09	100	Type of goal display on dashboard	System Admin	\N	Y	N	2009-07-17 18:31:09	100	\N
53330	AD_Reference Table	T	0	0	2009-11-13 15:05:49	100	\N	System Admin	\N	Y	N	2009-11-13 15:05:49	100	\N
53331	AD_RelationType Role	L	0	0	2009-11-13 15:09:50	100	Defines the possible "roles" a the records of a relation can have	System Admin	\N	Y	N	2009-11-13 15:09:50	100	\N
53332	AD_RelationType Type	L	0	0	2009-11-13 15:15:09	100	"Type" of a relation type	System Admin	For now we only have implicit realtion types, i.e. the record pairs are defined by the rule itself. In future we would like to have explicit type also. An explizit type just defines a template, the actual pairs can be added by a user or by the system itself.	Y	N	2009-11-13 15:15:09	100	\N
53411	_Document Action Recurring	L	0	0	2011-07-19 14:55:05	100	Document action list for recurring	System Admin	\N	Y	N	2011-07-19 14:55:05	100	LL
53415	Lead Source	L	0	0	2011-09-01 20:50:21	100	Lead Source	System Admin	The source of a lead	Y	N	2011-09-01 20:50:21	100	\N
53416	Lead Status	L	0	0	2011-09-01 21:12:25	100	\N	System Admin	\N	Y	N	2011-09-01 21:12:25	100	\N
53424	C_SalesStage	T	0	0	2011-10-26 11:28:47	100	\N	System Admin	\N	Y	N	2011-10-26 11:28:47	100	\N
53428	AD_Client Storage	L	0	0	2012-01-31 16:59:16	100	\N	System Admin	\N	Y	N	2012-01-31 16:59:16	100	\N
53719	ReportType	L	0	0	2014-08-28 18:03:01	100	\N	System Admin	\N	Y	N	2014-08-28 18:03:01	100	\N
53874	BLOB_Type	L	0	0	2017-03-03 15:17:03	100	\N	System Admin	\N	Y	N	2017-03-03 15:17:03	100	\N
400001	AD_EventLog_EventType	L	0	0	2022-03-07 17:46:41	100	\N	System Admin	\N	Y	N	2022-03-07 17:49:49	100	\N
\.
