copy "ad_window" ("ad_window_id", "name", "ad_client_id", "ad_color_id", "ad_image_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "isbetafunctionality", "isdefault", "issotrx", "processing", "updated", "updatedby", "windowtype", "winheight", "winwidth") from STDIN;
244	Request Type	0	\N	\N	0	2002-10-01 22:58:52	0	Maintain Request Types	Request	Request Types are used for processing and categorizing requests. Options are Account Inquiry, Warranty Issue, etc.	Y	N	N	Y	N	2000-01-02 00:00:00	0	M	\N	\N
345	Request Category	0	\N	\N	0	2005-04-26 20:29:25	100	Maintain Request Category	Request	Category or Topic of the Request 	Y	N	N	Y	N	2005-04-26 21:30:22	100	M	0	0
346	Request Group	0	\N	\N	0	2005-04-26 20:46:32	100	Maintain Request Group	Request	Group of requests (e.g. version numbers, responsibility, ...)	Y	N	N	Y	N	2005-04-26 21:21:32	100	M	0	0
347	Request Resolution	0	\N	\N	0	2005-04-26 20:48:01	100	Maintain Request Resolutions	Request	Resolution status (e.g. Fixed, Rejected, ..)	Y	N	N	Y	N	2005-04-26 21:14:26	100	M	0	0
348	Request Standard Response	0	\N	\N	0	2005-04-26 20:49:05	100	Maintain Request Standard Response 	Request	Text blocks to be copied into request response text	Y	N	N	Y	N	2005-04-26 21:05:53	100	M	0	0
349	Request Status	0	\N	\N	0	2005-04-26 20:49:56	100	Maintain Request Status	Request	Status if the request (open, closed, investigating, ..)	Y	N	N	Y	N	2005-04-26 21:04:16	100	M	0	0
367	Issue Recommendation	0	\N	\N	0	2005-12-30 14:28:35	100	Maintain Issue Recommendation	Request	\N	Y	N	N	Y	N	2005-12-30 14:28:35	100	M	0	0
368	Issue Status	0	\N	\N	0	2005-12-30 14:34:35	100	Maintain Issue Status	Request	Status of an Issue	Y	N	N	Y	N	2005-12-30 14:35:51	100	M	0	0
369	Known Issue	0	\N	\N	0	2005-12-30 14:41:59	100	Maintain Known Issue	Request	\N	Y	N	N	Y	N	2005-12-30 14:41:59	100	M	0	0
370	Issue Project	0	\N	\N	0	2005-12-30 14:54:39	100	Maintain Issue Management Project Links	Request	Maintain Issue Management Project Links - automatically generated.	Y	N	N	Y	N	2006-03-26 19:50:46	100	M	0	0
371	Issue User	0	\N	\N	0	2005-12-31 21:09:18	100	User who reported Issues	Request	\N	Y	N	N	Y	N	2005-12-31 21:09:18	100	M	0	0
372	Issue System	0	\N	\N	0	2005-12-31 21:37:00	100	Maintain Systems	Request	\N	Y	N	N	Y	N	2005-12-31 21:37:26	100	M	0	0
\.
