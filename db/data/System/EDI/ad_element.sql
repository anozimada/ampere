copy "ad_element" ("ad_element_id", "columnname", "ad_client_id", "ad_org_id", "ad_reference_id", "ad_reference_value_id", "created", "createdby", "description", "entitytype", "fieldlength", "help", "isactive", "name", "po_description", "po_help", "po_name", "po_printname", "printname", "updated", "updatedby") from STDIN;
1260	C_BP_EDI_ID	0	0	13	\N	2000-06-01 14:33:41	0	Electronic Data Interchange	EDI	22	\N	Y	EDI Definition	\N	\N	\N	\N	EDI Definition	2000-01-02 00:00:00	0
1261	CustomerNo	0	0	10	\N	2000-06-01 14:33:42	0	EDI Identification Number 	EDI	20	\N	Y	Customer No	\N	\N	\N	\N	Customer No	2000-01-02 00:00:00	0
1262	EDIStatus	0	0	17	202	2000-06-01 14:33:42	0	\N	EDI	1	\N	Y	EDI Status	\N	\N	\N	\N	EDI Status	2000-01-02 00:00:00	0
1263	EDIType	0	0	17	201	2000-06-01 14:33:42	0	\N	EDI	1	\N	Y	EDI Type	\N	\N	\N	\N	EDI Type	2000-01-02 00:00:00	0
1264	EMail_Error_To	0	0	10	\N	2000-06-01 14:33:42	0	Email address to send error messages to	EDI	60	\N	Y	Error EMail	\N	\N	\N	\N	Error EMail	2005-04-30 01:10:25	100
1265	EMail_From	0	0	10	\N	2000-06-01 14:33:42	0	Full EMail address used to send requests - e.g. edi@organization.com	EDI	60	\N	Y	From EMail	\N	\N	\N	\N	From EMail	2005-04-30 01:10:45	100
1266	EMail_From_Pwd	0	0	10	\N	2000-06-01 14:33:42	0	Password of the sending EMail address	EDI	20	\N	Y	From EMail Password	\N	\N	\N	\N	From EMail Pwd	2005-04-30 01:11:04	100
1267	EMail_From_Uid	0	0	10	\N	2000-06-01 14:33:42	0	User ID of the sending EMail address (on default SMTP Host) - e.g. edi	EDI	20	\N	Y	From EMail User ID	\N	\N	\N	\N	From EMail User	2005-04-30 01:11:27	100
1268	EMail_Info_To	0	0	10	\N	2000-06-01 14:33:42	0	EMail address to send informational messages and copies	EDI	60	The Info EMail address indicates the address to use when sending informational messages or copies of other messages.	Y	Info EMail	\N	\N	\N	\N	Info EMail	2005-04-30 01:11:51	100
1269	EMail_To	0	0	10	\N	2000-06-01 14:33:42	0	EMail address to send requests to - e.g. edi@manufacturer.com 	EDI	60	\N	Y	To EMail	\N	\N	\N	\N	To EMail	2005-04-30 01:12:10	100
1271	IsInfoSent	0	0	20	\N	2000-06-01 14:33:42	0	Send informational messages and copies	EDI	1	\N	Y	Send Info	\N	\N	\N	\N	Send Info	2000-01-02 00:00:00	0
1278	M_EDI_ID	0	0	13	\N	2000-06-01 14:33:42	0	\N	EDI	22	\N	Y	EDI Transaction	\N	\N	\N	\N	EDI Trx	2000-01-02 00:00:00	0
1279	M_EDI_Info_ID	0	0	13	\N	2000-06-01 14:33:42	0	\N	EDI	22	\N	Y	EDI Log	\N	\N	\N	\N	EDI Log	2000-01-02 00:00:00	0
1280	ReceiveInquiryReply	0	0	20	\N	2000-06-01 14:33:42	0	\N	EDI	1	\N	Y	Received Inquiry Reply	\N	\N	\N	\N	Inquiry reply	2000-01-02 00:00:00	0
1281	ReceiveOrderReply	0	0	20	\N	2000-06-01 14:33:42	0	\N	EDI	1	\N	Y	Receive Order Reply	\N	\N	\N	\N	Order reply	2000-01-02 00:00:00	0
1282	Reply_Price	0	0	37	\N	2000-06-01 14:33:42	0	Confirmed Price from EDI Partner	EDI	22	\N	Y	Reply Price	\N	\N	\N	\N	Reply Price	2000-01-02 00:00:00	0
1283	Reply_QtyAvailable	0	0	29	\N	2000-06-01 14:33:42	0	\N	EDI	22	\N	Y	Reply Qty Available	\N	\N	\N	\N	Qty Available	2000-01-02 00:00:00	0
1284	Reply_QtyConfirmed	0	0	29	\N	2000-06-01 14:33:42	0	\N	EDI	22	\N	Y	Reply Qty Confirmed	\N	\N	\N	\N	Qty Confirmed	2000-01-02 00:00:00	0
1285	Reply_Received	0	0	16	\N	2000-06-01 14:33:42	0	\N	EDI	7	\N	Y	Reply Received	\N	\N	\N	\N	Reply	2000-01-02 00:00:00	0
1286	Reply_Remarks	0	0	14	\N	2000-06-01 14:33:42	0	\N	EDI	2000	\N	Y	Reply Remarks	\N	\N	\N	\N	Remarks	2000-01-02 00:00:00	0
1287	Reply_ShipDate	0	0	15	\N	2000-06-01 14:33:42	0	\N	EDI	7	\N	Y	Reply Ship date	\N	\N	\N	\N	Ship date	2000-01-02 00:00:00	0
1288	Request_Price	0	0	37	\N	2000-06-01 14:33:42	0	\N	EDI	22	\N	Y	Request Price	\N	\N	\N	\N	Request Price	2000-01-02 00:00:00	0
1289	Request_Qty	0	0	29	\N	2000-06-01 14:33:42	0	\N	EDI	22	\N	Y	Request Qty	\N	\N	\N	\N	Request Qty	2000-01-02 00:00:00	0
1290	Request_Shipdate	0	0	15	\N	2000-06-01 14:33:42	0	\N	EDI	7	\N	Y	Request Ship date	\N	\N	\N	\N	Request Ship date	2000-01-02 00:00:00	0
1291	SendInquiry	0	0	20	\N	2000-06-01 14:33:42	0	Quantity Availability Inquiry	EDI	1	\N	Y	Send Inquiry	\N	\N	\N	\N	Send Inquiry	2000-01-02 00:00:00	0
1292	SendOrder	0	0	20	\N	2000-06-01 14:33:42	0	\N	EDI	1	\N	Y	Send Order	\N	\N	\N	\N	Send Order	2000-01-02 00:00:00	0
1293	TrxReceived	0	0	16	\N	2000-06-01 14:33:42	0	\N	EDI	7	\N	Y	Transaction received	\N	\N	\N	\N	Trx received	2000-01-02 00:00:00	0
1294	TrxSent	0	0	16	\N	2000-06-01 14:33:42	0	\N	EDI	7	\N	Y	Transaction sent	\N	\N	\N	\N	Trx sent	2000-01-02 00:00:00	0
53374	Host	0	0	10	\N	2008-03-05 00:53:22	0	\N	EDI	255	\N	Y	Host	\N	\N	\N	\N	Host	2008-03-05 00:53:22	0
53375	Port	0	0	11	\N	2008-03-05 00:53:23	0	\N	EDI	14	\N	Y	Port	\N	\N	\N	\N	Port	2008-03-05 00:53:23	0
53377	PasswordInfo	0	0	10	\N	2008-03-05 00:53:26	0	\N	EDI	255	\N	Y	Password Info	\N	\N	\N	\N	Password Info	2008-08-05 12:26:36	0
53379	ParameterValue	0	0	10	\N	2008-03-05 00:53:48	0	\N	EDI	60	\N	Y	Parameter Value	\N	\N	\N	\N	Parameter Value	2008-08-05 12:26:33	0
53380	JavaClass	0	0	10	\N	2008-03-05 00:54:06	0	\N	EDI	255	\N	Y	Java Class	\N	\N	\N	\N	Java Class	2008-08-05 12:26:28	0
55566	C_EDIProcessor_ID	0	0	\N	\N	2012-08-17 16:56:52	100	\N	EDI	\N	\N	Y	EDI Processor	\N	\N	\N	\N	EDI Processor	2016-04-11 16:22:47	0
55567	C_EDIProcessorType_ID	0	0	\N	\N	2012-08-17 16:57:28	100	\N	EDI	\N	\N	Y	EDI Processor Type	\N	\N	\N	\N	EDI Processor Type	2012-08-17 16:57:28	100
55568	C_EDIProcessor_Parameter_ID	0	0	\N	\N	2012-08-17 16:58:02	100	\N	EDI	\N	\N	Y	C_EDIProcessor_Parameter_ID	\N	\N	\N	\N	C_EDIProcessor_Parameter_ID	2016-04-11 16:22:53	0
55569	AD_Table_EDI_ID	0	0	\N	\N	2012-08-17 17:01:01	100	\N	EDI	\N	\N	Y	AD_Table_EDI_ID	\N	\N	\N	\N	AD_Table_EDI_ID	2016-04-11 16:22:58	0
55570	C_EDIFormat_ID	0	0	\N	\N	2012-08-17 17:01:19	100	\N	EDI	\N	\N	Y	EDI Format	\N	\N	\N	\N	EDI Format	2012-08-17 17:01:19	100
55571	Inbound	0	0	\N	\N	2012-08-17 17:01:22	100	\N	EDI	\N	\N	Y	Inbound	\N	\N	\N	\N	Inbound	2012-08-17 17:01:22	100
55572	C_BPartner_EDI_ID	0	0	\N	\N	2012-08-17 17:01:55	100	\N	EDI	\N	\N	Y	BPartner EDI	\N	\N	\N	\N	BPartner EDI	2016-04-11 16:23:03	0
55573	C_EDI_DocType_ID	0	0	\N	\N	2012-08-17 17:02:15	100	\N	EDI	\N	\N	Y	EDI Doc Type	\N	\N	\N	\N	EDI Doc Type	2012-08-17 17:02:15	100
55574	EDIEventListener	0	0	\N	\N	2012-08-17 17:02:24	100	\N	EDI	\N	\N	Y	EDI Event Listener	\N	\N	\N	\N	EDI Event Listener	2012-08-17 17:02:24	100
55575	C_EDI_DocCode	0	0	\N	\N	2012-08-17 17:03:04	100	\N	EDI	\N	\N	Y	C_EDI_DocCode	\N	\N	\N	\N	C_EDI_DocCode	2012-08-17 17:03:04	100
55576	SegmentSeparator	0	0	\N	\N	2012-08-17 17:04:02	100	\N	EDI	\N	\N	Y	SegmentSeparator	\N	\N	\N	\N	SegmentSeparator	2012-08-17 17:04:02	100
55577	FieldSeparator	0	0	\N	\N	2012-08-17 17:04:05	100	\N	EDI	\N	\N	Y	FieldSeparator	\N	\N	\N	\N	FieldSeparator	2012-08-17 17:04:05	100
55578	Header_Table_ID	0	0	\N	\N	2012-08-17 17:04:07	100	\N	EDI	\N	\N	Y	Header Table	\N	\N	\N	\N	Header Table	2012-08-17 17:04:07	100
55579	Real_Table_ID	0	0	\N	\N	2012-08-17 17:04:11	100	\N	EDI	\N	\N	Y	Table	\N	\N	\N	\N	Table	2012-08-17 17:04:11	100
55580	IsAcknowledgement	0	0	\N	\N	2012-08-17 17:04:14	100	\N	EDI	\N	\N	Y	IsAcknowledgement	\N	\N	\N	\N	IsAcknowledgement	2012-08-17 17:04:14	100
55581	FunctionalIdentifier	0	0	\N	\N	2012-08-17 17:04:17	100	\N	EDI	\N	\N	Y	Functional Identifier	\N	\N	\N	\N	Functional Identifier	2012-08-17 17:04:17	100
55582	LineSeparator	0	0	\N	\N	2012-08-17 17:04:26	100	\N	EDI	\N	\N	Y	Line Separator	\N	\N	\N	\N	Line Separator	2012-08-17 17:04:26	100
55583	C_EDIFormat_Line_ID	0	0	\N	\N	2012-08-17 17:05:12	100	\N	EDI	\N	\N	Y	EDI Format Line	\N	\N	\N	\N	EDI Format Line	2016-04-11 16:23:22	0
55584	IsReset	0	0	\N	\N	2012-08-17 17:05:17	100	\N	EDI	\N	\N	Y	IsReset	\N	\N	\N	\N	IsReset	2012-08-17 17:05:17	100
55585	EDIName	0	0	\N	\N	2012-08-17 17:05:19	100	\N	EDI	\N	\N	Y	EDIName	\N	\N	\N	\N	EDIName	2012-08-17 17:05:19	100
55586	VariableName	0	0	\N	\N	2012-08-17 17:05:22	100	\N	EDI	\N	\N	Y	VariableName	\N	\N	\N	\N	VariableName	2012-08-17 17:05:22	100
55587	C_EmbeddedEDIFormat_ID	0	0	\N	\N	2012-08-17 17:05:48	100	\N	EDI	\N	\N	Y	Embedded EDI Format	\N	\N	\N	\N	Embedded EDI Format	2012-08-17 17:05:48	100
55588	IsOneToMany	0	0	\N	\N	2012-08-17 17:05:51	100	Line might repeat one or more time in the source document	EDI	\N	\N	Y	Repeating Line	\N	\N	\N	\N	Repeating Line	2012-08-17 17:05:51	100
55589	IsAckLine	0	0	\N	\N	2012-08-17 17:05:54	100	\N	EDI	\N	\N	Y	Acknowledgement Line	\N	\N	\N	\N	Acknowledgement Line	2012-08-17 17:05:54	100
55590	C_EDIFormat_LineElement_ID	0	0	\N	\N	2012-08-17 17:06:35	100	\N	EDI	\N	\N	Y	EDI Format Line Element	\N	\N	\N	\N	EDI Format Line Element	2016-04-11 16:23:31	0
55591	MinFieldLength	0	0	\N	\N	2012-08-17 17:06:40	100	\N	EDI	\N	\N	Y	MinFieldLength	\N	\N	\N	\N	MinFieldLength	2012-08-17 17:06:40	100
55592	VariableValue	0	0	\N	\N	2012-08-17 17:06:48	100	\N	EDI	\N	\N	Y	VariableValue	\N	\N	\N	\N	VariableValue	2012-08-17 17:06:48	100
55593	ConstValue	0	0	\N	\N	2012-08-17 17:07:14	100	\N	EDI	\N	\N	Y	Const Value	\N	\N	\N	\N	Const Value	2012-08-17 17:07:14	100
55594	LookupColumn	0	0	\N	\N	2012-08-17 17:07:17	100	\N	EDI	\N	\N	Y	Lookup Column	\N	\N	\N	\N	Lookup Column	2012-08-17 17:07:17	100
55595	Header_Column_ID	0	0	\N	\N	2012-08-17 17:07:19	100	\N	EDI	\N	\N	Y	Header Column	\N	\N	\N	\N	Header Column	2012-08-17 17:07:19	100
55596	C_EDIFormat_LineElementComp_ID	0	0	\N	\N	2012-08-17 17:08:03	100	\N	EDI	\N	\N	Y	EDI Format Line Element Component	\N	\N	\N	\N	EDI Format Line Element Component	2016-04-11 16:23:41	0
55597	C_EDI_Ack_ID	0	0	\N	\N	2012-08-17 17:10:00	100	\N	EDI	\N	\N	Y	EDI Acknowledgement	\N	\N	\N	\N	EDI Acknowledgement	2016-04-11 16:23:53	0
55598	InterchangeControlNo	0	0	\N	\N	2012-08-17 17:10:10	100	\N	EDI	\N	\N	Y	Interchange Control Number	\N	\N	\N	\N	Interchange Control Number	2012-08-17 17:10:10	100
55599	InterchangeSender	0	0	\N	\N	2012-08-17 17:10:12	100	\N	EDI	\N	\N	Y	Interchange Sender ID	\N	\N	\N	\N	Interchange Sender ID	2012-08-17 17:10:12	100
55600	InterchangeSenderQualifier	0	0	\N	\N	2012-08-17 17:10:15	100	\N	EDI	\N	\N	Y	InterchangeSenderQualifier	\N	\N	\N	\N	InterchangeSenderQualifier	2012-08-17 17:10:15	100
55601	IsSubmitted	0	0	\N	\N	2012-08-17 17:10:18	100	\N	EDI	\N	\N	Y	Submitted	\N	\N	\N	\N	Submitted	2012-08-17 17:10:18	100
55602	LinesAccepted	0	0	\N	\N	2012-08-17 17:10:21	100	\N	EDI	\N	\N	Y	Lines Accepted	\N	\N	\N	\N	Lines Accepted	2012-08-17 17:10:21	100
55603	LinesIncluded	0	0	\N	\N	2012-08-17 17:10:24	100	\N	EDI	\N	\N	Y	Lines Included	\N	\N	\N	\N	Lines Included	2012-08-17 17:10:24	100
55604	LinesReceived	0	0	\N	\N	2012-08-17 17:10:26	100	\N	EDI	\N	\N	Y	Lines Received	\N	\N	\N	\N	Lines Received	2012-08-17 17:10:26	100
55605	SenderGroupControlNo	0	0	\N	\N	2012-08-17 17:10:30	100	\N	EDI	\N	\N	Y	Sender Group Control Number	\N	\N	\N	\N	Sender Group Control Number	2012-08-17 17:10:30	100
55606	C_EDI_Interchange_ID	0	0	\N	\N	2012-08-17 17:10:33	100	\N	EDI	\N	\N	Y	EDI Interchange	\N	\N	\N	\N	EDI Interchange	2012-08-17 17:10:33	100
55607	C_EDI_AckLine_ID	0	0	\N	\N	2012-08-17 17:11:07	100	\N	EDI	\N	\N	Y	EDI Acknowledgement Line	\N	\N	\N	\N	EDI Acknowledgement Line	2016-04-11 16:24:00	0
55608	TransactionIdentifier	0	0	\N	\N	2012-08-17 17:11:19	100	\N	EDI	\N	\N	Y	Transaction Set Identifier	\N	\N	\N	\N	Transaction Set Identifier	2012-08-17 17:11:19	100
55609	C_EDI_Message_ID	0	0	\N	\N	2012-08-17 17:12:47	100	\N	EDI	\N	\N	Y	EDI Message	\N	\N	\N	\N	EDI Message	2016-04-11 16:24:09	0
55610	MessageReferenceNo	0	0	\N	\N	2012-08-17 17:13:00	100	\N	EDI	\N	\N	Y	MessageReferenceNo	\N	\N	\N	\N	MessageReferenceNo	2012-08-17 17:13:00	100
55611	MessageType	0	0	\N	\N	2012-08-17 17:13:08	100	\N	EDI	\N	\N	Y	Message Type	\N	\N	\N	\N	Message Type	2012-08-17 17:13:08	100
55612	X_INOUTCONS_ID	0	0	\N	\N	2012-08-17 17:13:39	100	\N	EDI	\N	\N	Y	EDI Shipments	\N	\N	\N	\N	EDI Shipments	2016-04-11 16:24:15	0
55613	CreateUCC128	0	0	\N	\N	2012-08-17 17:13:55	100	\N	EDI	\N	\N	Y	CreateUCC128	\N	\N	\N	\N	Create UCC 128	2012-08-17 17:13:55	100
55614	EDIAcknowledged	0	0	\N	\N	2012-08-17 17:14:04	100	\N	EDI	\N	\N	Y	EDI Acknowledged	\N	\N	\N	\N	EDI Acknowledged	2012-08-17 17:14:04	100
55615	IsEDISubmitted	0	0	\N	\N	2012-08-17 17:14:08	100	\N	EDI	\N	\N	Y	Submitted via EDI	\N	\N	\N	\N	Submitted via EDI	2012-08-17 17:14:08	100
55616	SubmitEDI	0	0	\N	\N	2012-08-17 17:14:16	100	\N	EDI	\N	\N	Y	Submit EDI	\N	\N	\N	\N	Submit EDI	2012-08-17 17:14:16	100
55617	X_LABELBOL	0	0	\N	\N	2012-08-17 17:14:26	100	\N	EDI	\N	\N	Y	BOL #	\N	\N	\N	\N	BOL #	2012-08-17 17:14:26	100
55627	EDI_Sender	0	0	\N	\N	2012-08-24 11:19:39	100	EDI Sender Identification	EDI	\N	\N	Y	EDI Sender Id	\N	\N	\N	\N	EDI Sender Id	2012-08-24 11:19:39	100
55628	EDI_Sequence_ID	0	0	\N	\N	2012-08-24 11:22:16	100	EDI Interchange Sequence	EDI	\N	\N	Y	EDI Interchange Sequence	\N	\N	\N	\N	EDI Interchange Sequence	2012-08-24 11:22:16	100
\.
