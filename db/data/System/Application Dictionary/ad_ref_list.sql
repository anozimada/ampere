copy "ad_ref_list" ("ad_ref_list_id", "name", "ad_client_id", "ad_org_id", "ad_reference_id", "created", "createdby", "description", "entitytype", "isactive", "updated", "updatedby", "validfrom", "validto", "value") from STDIN;
51	Organization	0	0	5	1999-05-21 00:00:00	0	\N	Application Dictionary	Y	2007-12-17 03:07:11	0	\N	\N	1
53	Client+Organization	0	0	5	1999-05-21 00:00:00	0	\N	Application Dictionary	Y	2007-12-17 03:07:21	0	\N	\N	3
54	System only	0	0	5	1999-05-21 00:00:00	0	\N	Application Dictionary	Y	2007-12-17 03:07:29	0	\N	\N	4
57	All	0	0	5	1999-05-21 00:00:00	0	\N	Application Dictionary	Y	2007-12-17 03:07:44	0	\N	\N	7
91	List Validation	0	0	2	1999-05-21 00:00:00	0	List Validation	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	L
92	DataType	0	0	2	1999-05-21 00:00:00	0	DataType	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	D
93	Table Validation	0	0	2	1999-05-21 00:00:00	0	Table Validation	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	T
100	SQL	0	0	101	1999-05-21 00:00:00	0	SQL	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	S
101	Java Language	0	0	101	1999-05-21 00:00:00	0	Java Language	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	J
102	Java Script	0	0	101	1999-05-21 00:00:00	0	ECMA Script	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	E
103	Window	0	0	104	1999-05-21 00:00:00	0	\N	Application Dictionary	Y	2009-09-01 21:40:08	0	\N	\N	W
106	Process	0	0	104	1999-05-21 00:00:00	0	\N	Application Dictionary	Y	2009-09-01 21:40:08	0	\N	\N	P
107	Error	0	0	103	1999-05-21 00:00:00	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	E
108	Information	0	0	103	1999-05-21 00:00:00	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	I
109	Menu	0	0	103	1999-05-21 00:00:00	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	M
110	Single Record	0	0	108	1999-06-07 00:00:00	0	\N	Application Dictionary	N	2007-12-17 05:17:41	0	\N	\N	S
111	Maintain	0	0	108	1999-06-07 00:00:00	0	\N	Application Dictionary	Y	2007-12-17 05:17:48	0	\N	\N	M
112	Transaction	0	0	108	1999-06-07 00:00:00	0	\N	Application Dictionary	Y	2007-12-17 05:17:55	0	\N	\N	T
113	Query Only	0	0	108	1999-06-07 00:00:00	0	\N	Application Dictionary	Y	2007-12-17 05:18:04	0	\N	\N	Q
157	Local	0	0	126	1999-06-29 00:00:00	0	Local <-	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	L
159	Merge	0	0	126	1999-06-29 00:00:00	0	Merge <-->	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	M
160	Reference	0	0	126	1999-06-29 00:00:00	0	Reference ->	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	R
194	System+Client	0	0	5	1999-07-11 00:00:00	0	\N	Application Dictionary	Y	2007-12-17 03:06:19	0	\N	\N	6
351	Report	0	0	104	2000-05-25 18:57:27	0	\N	Application Dictionary	Y	2009-09-01 21:40:08	0	\N	\N	R
363	Form	0	0	104	2000-07-13 19:05:39	0	Special Forms	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	X
387	String	0	0	210	2000-09-15 15:24:47	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	S
388	Number	0	0	210	2000-09-15 15:25:13	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	N
389	Date	0	0	210	2000-09-15 15:25:49	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	D
390	Constant	0	0	210	2000-09-20 23:29:04	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	C
585	Obscure Digits but last 4	0	0	291	2003-09-28 18:20:39	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	904
586	Obscure Digits but first/last 4	0	0	291	2003-09-28 18:24:07	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	944
587	Obscure AlphaNumeric but first/last 4	0	0	291	2003-09-28 18:24:50	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	A44
588	Obscure AlphaNumeric but last 4	0	0	291	2003-09-28 18:25:22	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	A04
591	Accessing	0	0	293	2003-10-21 17:42:21	0	General Access	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	A
592	Reporting	0	0	293	2003-10-21 17:42:37	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	R
593	Exporting	0	0	293	2003-10-21 17:42:51	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	E
594	Client only	0	0	5	2003-10-21 18:19:20	0	\N	Application Dictionary	Y	2007-12-17 03:06:32	0	\N	\N	2
626	Process Created	0	0	306	2003-12-31 15:07:29	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	PC
627	State Changed	0	0	306	2003-12-31 15:07:53	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	SC
667	Yes	0	0	319	2004-03-18 21:36:56	0	\N	Application Dictionary	Y	2019-02-04 13:38:36	0	\N	\N	Y
668	No	0	0	319	2004-03-18 21:37:03	0	\N	Application Dictionary	Y	2019-02-04 13:38:36	0	\N	\N	N
671	Process Completed	0	0	306	2004-03-22 16:02:28	0	\N	Application Dictionary	Y	2000-01-02 00:00:00	0	\N	\N	PX
768	Encrypted	0	0	354	2005-07-20 07:38:22	100	\N	Application Dictionary	Y	2005-07-20 07:38:22	100	\N	\N	Y
769	Not Encrypted	0	0	354	2005-07-20 07:38:35	100	\N	Application Dictionary	Y	2005-07-20 07:38:35	100	\N	\N	N
50012	All Database Types	0	0	50003	2006-12-11 23:46:40	0	Use this when a SQL command syntax is the same between database types	Application Dictionary	Y	2006-12-12 00:13:32	0	\N	\N	ALL
50013	DB2	0	0	50003	2006-12-11 23:46:40	0	DB2 Database	Application Dictionary	Y	2006-12-12 00:13:32	0	\N	\N	DB2
50014	Firebird	0	0	50003	2006-12-11 23:46:40	0	Firebird Database	Application Dictionary	Y	2006-12-12 00:13:33	0	\N	\N	Firebird
50015	MySQL	0	0	50003	2006-12-11 23:46:41	0	MySQL Database	Application Dictionary	Y	2006-12-12 00:13:34	0	\N	\N	MySQL
50016	Oracle	0	0	50003	2006-12-11 23:46:41	0	Oracle Database	Application Dictionary	Y	2006-12-12 00:13:35	0	\N	\N	Oracle
50017	Postgres	0	0	50003	2006-12-11 23:46:41	0	Postgres Database	Application Dictionary	Y	2006-12-12 00:13:37	0	\N	\N	Postgres
50018	SQL Server	0	0	50003	2006-12-11 23:46:41	0	SQL Server Database	Application Dictionary	Y	2006-12-12 00:13:38	0	\N	\N	SQL
50019	Sybase	0	0	50003	2006-12-11 23:46:41	0	Sybase Database	Application Dictionary	Y	2006-12-12 00:13:38	0	\N	\N	Sybase
50037	Ask user (for future use)	0	0	50007	2006-12-19 04:01:20	0	\N	Application Dictionary	N	2006-12-19 04:01:42	0	\N	\N	A
50038	Don't show help	0	0	50007	2006-12-19 04:02:01	0	\N	Application Dictionary	Y	2006-12-19 04:02:01	0	\N	\N	N
50039	Show Help	0	0	50007	2006-12-19 04:02:25	0	\N	Application Dictionary	Y	2006-12-19 04:02:25	0	\N	\N	Y
50040	Run silently - Take Defaults	0	0	50007	2020-06-25 14:43:44.061952	0	\N	Application Dictionary	Y	2020-06-25 14:43:44.061952	0	\N	\N	S
53000	Tab	0	0	53000	2007-07-18 00:00:00	100	\N	Application Dictionary	Y	2007-07-18 00:00:00	100	\N	\N	T
53001	Label	0	0	53000	2007-07-18 00:00:00	100	\N	Application Dictionary	Y	2007-07-18 00:00:00	100	\N	\N	L
53002	Collapse	0	0	53000	2007-07-18 00:00:00	100	\N	Application Dictionary	Y	2007-07-18 00:00:00	100	\N	\N	C
53289	Aspect Orient Program	0	0	53235	2008-01-23 11:51:19	100	\N	Application Dictionary	Y	2008-01-23 11:51:19	100	\N	\N	A
53290	JSR 223 Scripting APIs	0	0	53235	2008-01-23 11:51:28	100	\N	Application Dictionary	Y	2008-01-24 14:26:54	0	\N	\N	S
53291	JSR 94 Rule Engine API	0	0	53235	2008-01-23 11:51:42	100	\N	Application Dictionary	Y	2008-01-23 11:51:42	100	\N	\N	R
53292	SQL	0	0	53235	2008-01-23 11:51:51	100	\N	Application Dictionary	Y	2008-01-24 14:27:04	0	\N	\N	Q
53293	Callout	0	0	53236	2008-01-23 11:55:09	100	\N	Application Dictionary	Y	2008-01-23 11:55:09	100	\N	\N	C
53294	Process	0	0	53236	2008-01-23 11:55:16	100	\N	Application Dictionary	Y	2008-01-23 11:55:16	100	\N	\N	P
53295	Model Validator Table Event	0	0	53236	2008-01-23 11:55:56	100	\N	Application Dictionary	Y	2008-01-23 11:55:56	100	\N	\N	T
53296	Model Validator Document Event	0	0	53236	2008-01-23 11:56:08	100	\N	Application Dictionary	Y	2008-01-23 11:56:26	100	\N	\N	D
53297	Model Validator Login Event	0	0	53236	2008-01-23 11:56:20	100	\N	Application Dictionary	Y	2008-01-23 11:56:20	100	\N	\N	L
53299	Table Before New	0	0	53237	2008-02-01 01:38:15	100	\N	Application Dictionary	Y	2008-02-01 01:38:15	100	\N	\N	TBN
53300	Table Before Change	0	0	53237	2008-02-01 01:38:32	100	\N	Application Dictionary	Y	2008-02-01 01:38:32	100	\N	\N	TBC
53301	Table Before Delete	0	0	53237	2008-02-01 01:38:45	100	\N	Application Dictionary	Y	2008-02-01 01:38:45	100	\N	\N	TBD
53302	Table After New	0	0	53237	2008-02-01 01:39:11	100	\N	Application Dictionary	Y	2008-02-01 01:39:11	100	\N	\N	TAN
53303	Table After Change	0	0	53237	2008-02-01 01:39:27	100	\N	Application Dictionary	Y	2008-02-01 01:39:27	100	\N	\N	TAC
53304	Table After Delete	0	0	53237	2008-02-01 01:39:43	100	\N	Application Dictionary	Y	2008-02-01 01:39:43	100	\N	\N	TAD
53305	Document Before Prepare	0	0	53237	2008-02-01 01:40:06	100	\N	Application Dictionary	Y	2008-02-01 01:40:06	100	\N	\N	DBPR
53306	Document Before Void	0	0	53237	2008-02-01 01:40:20	100	\N	Application Dictionary	Y	2008-02-01 01:40:20	100	\N	\N	DBVO
53307	Document Before Close	0	0	53237	2008-02-01 01:40:45	100	\N	Application Dictionary	Y	2008-02-01 01:40:45	100	\N	\N	DBCL
53308	Document Before Reactivate	0	0	53237	2008-02-01 01:41:05	100	\N	Application Dictionary	Y	2008-02-01 01:41:05	100	\N	\N	DBAC
53309	Document Before Reverse Correct	0	0	53237	2008-02-01 01:41:24	100	\N	Application Dictionary	Y	2008-02-01 01:41:24	100	\N	\N	DBRC
53310	Document Before Reverse Accrual	0	0	53237	2008-02-01 01:41:37	100	\N	Application Dictionary	Y	2008-02-01 01:41:37	100	\N	\N	DBRA
53311	Document Before Complete	0	0	53237	2008-02-01 01:41:55	100	\N	Application Dictionary	Y	2008-02-01 01:41:55	100	\N	\N	DBCO
53312	Document Before Post	0	0	53237	2008-02-01 01:42:08	100	\N	Application Dictionary	Y	2008-02-01 01:42:08	100	\N	\N	DBPO
53313	Document After Prepare	0	0	53237	2008-02-01 01:42:25	100	\N	Application Dictionary	Y	2008-02-01 01:42:25	100	\N	\N	DAPR
53314	Document After Void	0	0	53237	2008-02-01 01:42:43	100	\N	Application Dictionary	Y	2008-02-01 01:42:43	100	\N	\N	DAVO
53315	Document After Close	0	0	53237	2008-02-01 01:42:58	100	\N	Application Dictionary	Y	2008-02-01 01:42:58	100	\N	\N	DACL
53316	Document After Reactivate	0	0	53237	2008-02-01 01:43:11	100	\N	Application Dictionary	Y	2008-02-01 01:43:11	100	\N	\N	DAAC
53317	Document After Reverse Correct	0	0	53237	2008-02-01 01:43:37	100	\N	Application Dictionary	Y	2008-02-01 01:43:37	100	\N	\N	DARC
53318	Document After Reverse Accrual	0	0	53237	2008-02-01 01:43:52	100	\N	Application Dictionary	Y	2008-02-01 01:43:52	100	\N	\N	DARA
53319	Document After Complete	0	0	53237	2008-02-01 01:44:12	100	\N	Application Dictionary	Y	2008-02-01 01:44:12	100	\N	\N	DACO
53320	Document After Post	0	0	53237	2008-02-01 01:44:32	100	\N	Application Dictionary	Y	2008-02-01 01:44:32	100	\N	\N	DAPO
53321	Insert	0	0	53238	2008-02-12 21:28:05	100	\N	Application Dictionary	Y	2008-02-12 21:28:05	100	\N	\N	I
53322	Delete	0	0	53238	2008-02-12 21:28:11	100	\N	Application Dictionary	Y	2008-02-12 21:28:11	100	\N	\N	D
53323	Update	0	0	53238	2008-02-12 21:28:16	100	\N	Application Dictionary	Y	2008-02-12 21:28:16	100	\N	\N	U
53327	In Progress	0	0	53239	2008-02-15 15:10:08	100	In Progress	Application Dictionary	Y	2008-02-15 15:10:08	100	\N	\N	IP
53328	Completed	0	0	53239	2008-02-15 15:10:21	100	Completed	Application Dictionary	Y	2008-02-15 15:10:21	100	\N	\N	CO
53329	Error	0	0	53239	2008-02-15 15:10:32	100	Error	Application Dictionary	Y	2008-02-15 15:10:32	100	\N	\N	ER
53337	Table After New Replication	0	0	53237	2008-03-05 03:39:52	0	\N	Application Dictionary	Y	2008-03-05 03:39:52	0	\N	\N	TANR
53338	Table After Change Replication	0	0	53237	2008-03-05 03:40:43	0	\N	Application Dictionary	Y	2008-03-05 03:40:43	0	\N	\N	TACR
53339	Table Before Delete Replication	0	0	53237	2008-03-05 03:41:36	0	\N	Application Dictionary	Y	2008-03-05 03:41:36	0	\N	\N	TBDR
53454	Query	0	0	53291	2009-02-18 13:39:57	100	\N	Application Dictionary	Y	2009-02-18 13:39:57	100	\N	\N	Q
53455	Table	0	0	53291	2009-02-18 13:40:10	100	\N	Application Dictionary	Y	2009-02-18 13:40:10	100	\N	\N	T
53456	Integer	0	0	53292	2009-02-18 13:44:18	100	\N	Application Dictionary	Y	2009-02-18 13:44:18	100	\N	\N	I
53457	String	0	0	53292	2009-02-18 13:44:36	100	\N	Application Dictionary	Y	2009-02-18 13:44:36	100	\N	\N	S
53488	Application Dictionary	0	0	53313	2009-06-14 01:04:22	100	\N	Application Dictionary	Y	2009-06-14 01:04:22	100	\N	\N	AD
53489	SQL Statement	0	0	53313	2009-06-14 01:04:51	100	\N	Application Dictionary	Y	2009-06-14 01:04:51	100	\N	\N	SQL
53503	Broadcast	0	0	126	2009-08-14 13:26:35	100	Broadcast <<->>	Application Dictionary	Y	2009-08-14 13:38:32	100	\N	\N	B
53551	Human Resource & Payroll	0	0	53236	2009-11-06 17:47:56	0	\N	Application Dictionary	Y	2009-11-06 17:47:56	0	\N	\N	H
53556	Measure for Performance Analysis	0	0	53236	2009-11-28 11:03:06	0	\N	Application Dictionary	Y	2009-11-28 11:03:06	0	\N	\N	M
53627	GL Reconciliation	0	0	53236	2010-09-02 18:03:43	100	SQL subquery used in GL Reconciliation (auto)	Application Dictionary	Y	2010-09-02 18:03:43	100	\N	\N	R
54514	ActiveWorkflows	0	0	53763	2014-12-06 18:18:18	100	Active Workflows	Application Dictionary	Y	2014-12-06 18:18:18	100	\N	\N	ActiveWorkflows
54515	Archive	0	0	53763	2014-12-06 18:18:32	100	Archive	Application Dictionary	Y	2014-12-06 18:18:32	100	\N	\N	Archive
54516	Attachment	0	0	53763	2014-12-06 18:18:43	100	Attachment	Application Dictionary	Y	2014-12-06 18:18:43	100	\N	\N	Attachment
54517	Chat	0	0	53763	2014-12-06 18:18:54	100	Chat	Application Dictionary	Y	2014-12-06 18:18:54	100	\N	\N	Chat
54518	Copy	0	0	53763	2014-12-06 18:19:06	100	Copy	Application Dictionary	Y	2014-12-06 18:19:06	100	\N	\N	Copy
54519	Delete	0	0	53763	2014-12-06 18:19:18	100	Delete	Application Dictionary	Y	2014-12-06 18:19:18	100	\N	\N	Delete
54520	DeleteSelection	0	0	53763	2014-12-06 18:19:37	100	Delete Selection	Application Dictionary	Y	2014-12-06 18:19:37	100	\N	\N	DeleteSelection
54521	DetailRecord	0	0	53763	2014-12-06 18:20:15	100	Detail Record	Application Dictionary	Y	2014-12-06 18:20:15	100	\N	\N	DetailRecord
54522	Find	0	0	53763	2014-12-06 18:20:27	100	Find	Application Dictionary	Y	2014-12-06 18:20:27	100	\N	\N	Find
54523	First	0	0	53763	2014-12-06 18:20:36	100	First	Application Dictionary	Y	2014-12-06 18:20:36	100	\N	\N	First
54524	Help	0	0	53763	2014-12-06 18:20:48	100	Help	Application Dictionary	Y	2014-12-06 18:20:48	100	\N	\N	Help
54525	HistoryRecords	0	0	53763	2014-12-06 18:21:08	100	History Records	Application Dictionary	Y	2014-12-06 18:21:08	100	\N	\N	HistoryRecords
54526	Ignore	0	0	53763	2014-12-06 18:21:20	100	Ignore	Application Dictionary	Y	2014-12-06 18:21:20	100	\N	\N	Ignore
54527	Last	0	0	53763	2014-12-06 18:21:29	100	Last	Application Dictionary	Y	2014-12-06 18:21:29	100	\N	\N	Last
54528	Lock	0	0	53763	2014-12-06 18:21:43	100	Lock	Application Dictionary	Y	2014-12-06 18:21:43	100	\N	\N	Lock
54529	New	0	0	53763	2014-12-06 18:21:55	100	New	Application Dictionary	Y	2014-12-06 18:21:55	100	\N	\N	New
54530	Next	0	0	53763	2014-12-06 18:22:07	100	Next	Application Dictionary	Y	2014-12-06 18:22:07	100	\N	\N	Next
54531	ParentRecord	0	0	53763	2014-12-06 18:22:17	100	Parent Record	Application Dictionary	Y	2014-12-06 18:22:17	100	\N	\N	ParentRecord
54532	Previous	0	0	53763	2014-12-06 18:22:28	100	Previous	Application Dictionary	Y	2014-12-06 18:22:28	100	\N	\N	Previous
54533	Print	0	0	53763	2014-12-06 18:22:43	100	Print	Application Dictionary	Y	2014-12-06 18:22:43	100	\N	\N	Print
54534	ProductInfo	0	0	53763	2014-12-06 18:22:58	100	Product Info	Application Dictionary	Y	2014-12-06 18:22:58	100	\N	\N	ProductInfo
54535	Refresh	0	0	53763	2014-12-06 18:23:09	100	Refresh	Application Dictionary	Y	2014-12-06 18:23:09	100	\N	\N	Refresh
54536	Report	0	0	53763	2014-12-06 18:23:17	100	Report	Application Dictionary	Y	2014-12-06 18:23:17	100	\N	\N	Report
54537	Requests	0	0	53763	2014-12-06 18:23:27	100	Requests	Application Dictionary	Y	2014-12-06 18:23:27	100	\N	\N	Requests
54538	Save	0	0	53763	2014-12-06 18:23:39	100	Save	Application Dictionary	Y	2014-12-06 18:23:39	100	\N	\N	Save
54539	SaveColumnWidth	0	0	53763	2014-12-06 18:23:55	100	Save Column Width	Application Dictionary	Y	2014-12-06 18:23:55	100	\N	\N	SaveColumnWidth
54540	Toggle	0	0	53763	2014-12-06 18:24:06	100	Toggle	Application Dictionary	Y	2014-12-06 18:24:06	100	\N	\N	Toggle
54541	ZoomAcross	0	0	53763	2014-12-06 18:24:16	100	Zoom Across	Application Dictionary	Y	2014-12-06 18:24:16	100	\N	\N	ZoomAcross
54660	Button	0	0	53806	2016-04-20 15:50:04	100	Display as button	Application Dictionary	Y	2016-04-20 15:50:04	100	\N	\N	B
54661	List	0	0	53806	2016-04-20 18:05:23	100	Display as list	Application Dictionary	Y	2016-04-20 18:05:23	100	\N	\N	L
54662	Menu	0	0	53806	2016-04-20 18:05:44	100	Display as menu on a gear button	Application Dictionary	Y	2016-04-20 18:05:44	100	\N	\N	M
54663	!=	0	0	53807	2016-04-21 12:08:04	100	\N	Application Dictionary	Y	2016-04-21 12:08:04	100	\N	\N	!=
54664	<	0	0	53807	2016-04-21 12:08:17	100	\N	Application Dictionary	Y	2016-04-21 12:08:17	100	\N	\N	<
54665	<=	0	0	53807	2016-04-21 12:08:27	100	\N	Application Dictionary	Y	2016-04-21 12:08:27	100	\N	\N	<=
54666	=	0	0	53807	2016-04-21 12:08:38	100	\N	Application Dictionary	Y	2016-04-21 12:08:38	100	\N	\N	=
54667	>	0	0	53807	2016-04-21 12:08:47	100	\N	Application Dictionary	Y	2016-04-21 12:08:47	100	\N	\N	>
54668	>=	0	0	53807	2016-04-21 12:08:56	100	\N	Application Dictionary	Y	2016-04-21 12:08:56	100	\N	\N	>=
54669	Like	0	0	53807	2016-04-21 12:09:07	100	\N	Application Dictionary	Y	2016-04-21 12:09:07	100	\N	\N	Like
\.
