copy "ad_table" ("ad_table_id", "tablename", "name", "accesslevel", "ad_client_id", "ad_org_id", "ad_val_rule_id", "ad_window_id", "copycolumnsfromtable", "created", "createdby", "description", "entitytype", "help", "importtable", "isactive", "iscentrallymaintained", "ischangelog", "isdeleteable", "ishighvolume", "issecurityenabled", "isview", "loadseq", "po_window_id", "replicationtype", "sqlstatement", "updated", "updatedby") from STDIN;
100	AD_Table	Table	4	0	0	\N	100	\N	1999-05-21 00:00:00	0	Table for the Fields	Application Dictionary	\N	\N	Y	Y	N	Y	Y	N	N	55	\N	L	\N	2000-01-02 00:00:00	0
101	AD_Column	Column	4	0	0	\N	100	\N	1999-05-21 00:00:00	0	Column in the table	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	130	\N	L	\N	2000-01-02 00:00:00	0
102	AD_Reference	Reference	4	0	0	\N	101	\N	1999-05-21 00:00:00	0	System Reference (Pick List)	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	125	\N	L	\N	2000-01-02 00:00:00	0
103	AD_Ref_Table	AD_Ref_Table	4	0	0	\N	101	\N	1999-05-21 00:00:00	0	Reference Table Definitions	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
104	AD_Ref_List	Reference List	4	0	0	\N	101	\N	1999-05-21 00:00:00	0	Reference List based on Table	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
105	AD_Window	Window	4	0	0	\N	102	\N	1999-05-21 00:00:00	0	Data entry or display window	Application Dictionary	\N	\N	Y	Y	N	Y	Y	N	N	50	\N	L	\N	2000-01-02 00:00:00	0
106	AD_Tab	Tab	4	0	0	\N	102	\N	1999-05-21 00:00:00	0	Tab within a Window	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2000-01-02 00:00:00	0
107	AD_Field	Field	4	0	0	\N	102	\N	1999-05-21 00:00:00	0	Field on a database table	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
108	AD_Val_Rule	Dynamic Validation	4	0	0	\N	103	\N	1999-05-21 00:00:00	0	Dynamic Validation Rule	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	50	\N	L	\N	2000-01-02 00:00:00	0
109	AD_Message	Message	4	0	0	\N	104	\N	1999-05-21 00:00:00	0	System Message	Application Dictionary	\N	\N	Y	Y	N	Y	Y	N	N	130	\N	L	\N	2005-02-08 18:43:53	100
119	AD_Message_Trl	Message Trl	4	0	0	\N	104	\N	1999-05-21 00:00:00	0	System Message	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
123	AD_Tab_Trl	Tab Trl	4	0	0	\N	102	\N	1999-05-21 00:00:00	0	Tab within a Window	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
126	AD_Reference_Trl	Reference Trl	4	0	0	\N	101	\N	1999-05-21 00:00:00	0	System Reference and Validation	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
127	AD_Field_Trl	Field Trl	4	0	0	\N	102	\N	1999-05-21 00:00:00	0	Field on a database table	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
132	AD_Window_Trl	Window Trl	4	0	0	\N	102	\N	1999-05-21 00:00:00	0	Data entry or display window	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
136	AD_Ref_List_Trl	Reference List Trl	4	0	0	\N	101	\N	1999-05-21 00:00:00	0	Reference List based on Table	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
156	AD_Role	Role	6	0	0	\N	111	\N	1999-05-21 00:00:00	0	Responsibility Role	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	115	\N	L	\N	2000-01-02 00:00:00	0
157	AD_User_Roles	AD_User_Roles	6	0	0	\N	108	\N	1999-05-21 00:00:00	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
197	AD_Process_Access	Process Access	6	0	0	\N	165	\N	1999-06-14 00:00:00	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2006-07-23 15:06:35	100
201	AD_Window_Access	AD_Window_Access	6	0	0	\N	102	\N	1999-06-14 00:00:00	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
202	AD_Workflow_Access	AD_Workflow_Access	6	0	0	\N	113	\N	1999-06-14 00:00:00	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2007-12-17 02:05:31	0
276	AD_Element	System Element	4	0	0	\N	151	\N	1999-09-26 00:00:00	0	System Element enables the central maintenance of column description and help.	Application Dictionary	\N	\N	Y	Y	N	Y	Y	N	N	125	\N	L	\N	2000-01-02 00:00:00	0
277	AD_Element_Trl	System Element Trl	4	0	0	\N	151	\N	1999-09-26 00:00:00	0	System Element enables the central maintenance of column description and help.	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
284	AD_Process	Process	4	0	0	\N	165	\N	1999-11-10 00:00:00	0	Process or Report	Application Dictionary	\N	\N	Y	Y	N	Y	Y	N	N	125	\N	L	\N	2000-01-02 00:00:00	0
285	AD_Process_Para	Process Parameter	4	0	0	\N	165	\N	1999-11-10 00:00:00	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
286	AD_Process_Para_Trl	Process Parameter Trl	4	0	0	\N	165	\N	1999-11-10 00:00:00	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
287	AD_Process_Trl	Process Trl	4	0	0	\N	165	\N	1999-11-10 00:00:00	0	Process or Report	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
361	AD_ReportView	Report View	4	0	0	\N	180	\N	2000-05-11 18:21:30	0	View used to generate this report	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	60	\N	L	\N	2000-01-02 00:00:00	0
376	AD_Form	Special Form	4	0	0	\N	187	\N	2000-07-13 17:31:02	0	Special Form	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	130	\N	L	\N	2000-01-02 00:00:00	0
377	AD_Form_Trl	Special Form Trl	4	0	0	\N	187	\N	2000-07-13 17:31:02	0	Special Form	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
378	AD_Form_Access	AD_Form_Access	6	0	0	\N	187	\N	2000-07-15 10:13:45	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
405	AD_Attribute	System Attribute	7	0	0	\N	196	\N	2000-12-22 22:20:19	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
414	AD_FieldGroup	Field Group	4	0	0	\N	200	\N	2001-01-11 17:01:17	0	Logical grouping of fields	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2000-01-02 00:00:00	0
415	AD_FieldGroup_Trl	Field Group Trl	4	0	0	\N	200	\N	2001-01-11 17:01:17	0	Logical grouping of fields	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
422	AD_Role_OrgAccess	AD_Role_OrgAccess	6	0	0	\N	111	\N	2001-01-20 12:59:11	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
428	AD_ReportView_Col	Report view Column	4	0	0	\N	180	\N	2001-02-25 20:49:34	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
464	AD_UserDef_Field	User defined Field	6	0	0	\N	229	\N	2001-09-05 20:55:18	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2010-04-16 23:05:50	100
466	AD_UserDef_Tab	User defined Tab	6	0	0	\N	229	\N	2001-09-05 20:55:18	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2010-04-16 23:05:17	100
467	AD_UserDef_Win	User defined Window	6	0	0	\N	229	\N	2001-09-05 20:55:18	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2005-04-02 22:54:19	100
565	AD_Table_Access	AD_Table_Access	6	0	0	\N	268	\N	2003-05-28 21:35:14	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
567	AD_Record_Access	AD_Record_Access	6	0	0	\N	268	\N	2003-05-28 21:35:14	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
571	AD_Column_Access	AD_Column_Access	6	0	0	\N	268	\N	2003-05-28 21:35:14	0	\N	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
746	AD_Table_Trl	Table Trl	4	0	0	\N	100	\N	2004-07-07 18:32:24	0	Database Table information	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2005-04-26 20:31:52	100
752	AD_Column_Trl	Column Trl	4	0	0	\N	100	\N	2004-09-06 16:08:44	0	Column in the table	Application Dictionary	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
882	AD_EntityType	Entity Type	4	0	0	\N	381	\N	2006-06-11 11:22:25	100	System Entity Type	Application Dictionary	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2006-06-11 16:32:05	100
883	AD_Modification	Modification	4	0	0	\N	381	\N	2006-06-11 11:46:14	100	\N	Application Dictionary	\N	N	Y	Y	N	N	N	N	N	0	\N	L	\N	2006-06-11 16:32:25	100
895	AD_InfoWindow	Info Window	4	0	0	\N	385	\N	2006-06-17 17:14:58	100	\N	Application Dictionary	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2006-06-17 17:19:19	100
896	AD_InfoWindow_Trl	Info Window Trl	4	0	0	\N	385	\N	2006-06-17 17:23:59	100	Info and search/select Window	Application Dictionary	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2006-06-17 17:23:59	100
897	AD_InfoColumn	Info Column	4	0	0	\N	385	\N	2006-06-17 17:25:41	100	\N	Application Dictionary	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2006-06-17 17:25:41	100
898	AD_InfoColumn_Trl	Info Column Trl	4	0	0	\N	385	\N	2006-06-17 17:32:40	100	Info Window Column	Application Dictionary	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2006-06-17 17:33:35	100
53012	AD_Document_Action_Access	Document Action Access	6	0	0	\N	\N	N	2020-06-25 14:44:27.146181	100	Define access to document type / document action / role combinations.	Application Dictionary	Define access rules (add roles with access) for client/role/doctype/document action combinations. If no rules are defined for a client/doctype/doc action combination all roles can access the document action.	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2020-06-25 14:44:25.511504	100
53014	AD_ModelValidator	Model Validator	4	0	0	\N	53003	N	2007-10-22 00:00:00	100	Global Model Validator	Application Dictionary	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2009-08-31 14:54:45	0
53058	AD_Rule	Rule	4	0	0	\N	53017	N	2008-01-23 11:41:30	100	\N	Application Dictionary	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2008-01-23 12:05:51	100
53059	AD_Table_ScriptValidator	Table Script Validator	4	0	0	\N	100	N	2008-02-01 01:47:37	100	Script model validator for tables	Application Dictionary	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2008-02-01 01:47:37	100
53169	AD_SearchDefinition	Search Definition	4	0	0	\N	\N	N	2009-02-18 12:44:56	100	\N	Application Dictionary	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2009-02-18 12:44:56	100
53222	AD_Role_Included	Included Role	6	0	0	\N	111	N	2009-07-27 19:43:49	0	\N	Application Dictionary	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2009-07-27 19:43:49	0
53400	AD_TableSelection	Selection Table	4	0	0	\N	\N	N	2012-07-02 17:28:03	100	Select records from a table based on criteria	Application Dictionary	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2016-04-11 16:20:28	0
53401	AD_TableSelection_Column	Selection Column ID	4	0	0	\N	\N	N	2012-07-02 17:34:04	100	\N	Application Dictionary	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2012-07-02 17:34:04	100
53895	AD_UIAction	ToolBar Button	4	0	0	\N	\N	N	2014-11-17 10:27:20	100	\N	Application Dictionary	\N	N	Y	Y	Y	Y	N	N	N	0	\N	L	\N	2014-11-17 10:27:20	100
53936	X_RoleAuthority	RoleAuthority	3	0	0	\N	\N	N	2015-01-29 14:00:18	100	\N	Application Dictionary	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2016-04-11 16:44:38	0
54064	AD_ZoomCondition	Zoom Condition	4	0	0	\N	\N	N	2016-04-07 14:38:58	100	\N	Application Dictionary	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2016-04-07 14:38:58	100
54073	AD_InfoProcess	Info Process ID	4	0	0	\N	385	N	2016-04-20 15:18:59	100	\N	Application Dictionary	\N	N	Y	Y	Y	Y	N	N	N	0	\N	L	\N	2016-04-20 15:18:59	100
54081	AD_InfoWindow_Access	Info Window Access	6	0	0	\N	\N	N	2016-04-21 16:11:16	100	\N	Application Dictionary	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2016-04-21 16:11:16	100
54087	AD_ColumnProcess	Column Link Process	4	0	0	\N	\N	N	2016-04-29 10:03:35	100	Process linked to a column	Application Dictionary	Process linked to column when generating report in HTML view.  It allows you to execute another process using the link as parameter	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2016-04-29 10:03:35	100
54088	AD_ColumnProcessPara	Link Process Parameter	4	0	0	\N	\N	N	2016-04-29 10:23:05	100	\N	Application Dictionary	Override Parameter value using the @Link_ID@ from the HTML Report	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2016-04-29 10:23:05	100
\.
