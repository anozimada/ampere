copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
138	Element	W	0	\N	\N	0	\N	151	1999-09-26 00:00:00	0	Maintain System Elements	Application Dictionary	Y	Y	N	Y	N	2000-01-02 00:00:00	0
139	Table and Column	W	0	\N	\N	0	\N	100	1999-05-21 00:00:00	0	Maintain Tables and Columns	Application Dictionary	Y	Y	N	Y	N	2000-01-02 00:00:00	0
140	Reference	W	0	\N	\N	0	\N	101	1999-05-21 00:00:00	0	Maintain System References	Application Dictionary	Y	Y	N	Y	N	2006-05-03 14:40:49	100
141	Window, Tab and Field	W	0	\N	\N	0	\N	102	1999-05-21 00:00:00	0	Maintain Windows, Tabs and Fields	Application Dictionary	Y	Y	N	Y	N	2022-02-17 23:01:36	100
142	Validation Rules	W	0	\N	\N	0	\N	103	1999-05-21 00:00:00	0	Maintain dynamic Validation Rules for columns and fields	Application Dictionary	Y	Y	N	Y	N	2000-01-02 00:00:00	0
143	Message	W	0	\N	\N	0	\N	104	1999-05-21 00:00:00	0	Maintain Information and Error Messages	Application Dictionary	Y	Y	N	Y	N	2000-01-02 00:00:00	0
153	Application Dictionary	\N	0	\N	\N	0	\N	\N	1999-11-27 16:31:34	0	Maintain Application Dictionary	Application Dictionary	Y	N	N	Y	Y	2000-01-02 00:00:00	0
176	Report & Process	W	0	\N	\N	0	\N	165	1999-12-11 20:07:01	0	Maintain Reports & Processes	Application Dictionary	Y	Y	N	Y	N	2000-01-02 00:00:00	0
201	Report View	W	0	\N	\N	0	\N	180	2000-05-15 21:59:58	0	Maintain Report Views	Application Dictionary	Y	Y	N	Y	N	2000-01-02 00:00:00	0
216	Form	W	0	\N	\N	0	\N	187	2000-07-13 19:01:36	0	Special Forms	Application Dictionary	Y	Y	N	N	N	2000-01-02 00:00:00	0
239	Custom Attribute	W	0	\N	\N	0	\N	196	2000-12-22 22:42:25	0	Maintain custom entity attributes	Application Dictionary	N	Y	N	N	N	2005-04-02 23:29:18	100
249	Field Group	W	0	\N	\N	0	\N	200	2001-01-11 17:28:09	0	Define Field Group	Application Dictionary	Y	Y	N	N	N	2000-01-02 00:00:00	0
499	Reapply Customisations	P	0	\N	\N	0	308	\N	2004-09-24 21:36:30	0	If you identified customisations in the Change Log, you can reapply them	Application Dictionary	N	Y	N	N	N	2016-04-11 16:46:00	0
517	Window Customisation	W	0	\N	\N	0	\N	229	2005-04-02 23:09:56	100	Define Window Customisation for Role/User	Application Dictionary	N	Y	N	N	N	2020-10-16 23:33:47	100
586	Entity Type	W	0	\N	\N	0	\N	381	2006-06-11 11:53:08	100	Maintain System Entity Type	Application Dictionary	Y	Y	N	N	N	2006-06-11 11:53:08	100
589	Info Window	W	0	\N	\N	0	\N	385	2006-06-17 17:52:16	100	Define Info and search/select Window	Application Dictionary	Y	Y	N	N	N	2006-06-17 17:52:16	100
53012	Model Validator	W	0	\N	\N	0	\N	53003	2007-10-22 00:00:00	100	\N	Application Dictionary	Y	Y	N	N	N	2007-10-22 00:00:00	100
53086	Rule	W	0	\N	\N	0	\N	53017	2008-01-23 12:06:13	100	\N	Application Dictionary	Y	Y	N	N	N	2008-01-23 12:06:13	100
53203	Search Definition	W	0	\N	\N	0	\N	53069	2009-02-18 13:49:13	100	Define transactioncodes for the QuickSearch bar	Application Dictionary	Y	Y	N	N	N	2009-02-18 13:49:13	100
53429	Selection Table	W	0	\N	\N	0	\N	53186	2012-07-02 17:55:31	100	Table for selecting records	Application Dictionary	Y	Y	N	N	N	2012-07-02 17:55:31	100
\.
