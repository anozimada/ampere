copy "ad_reference" ("ad_reference_id", "name", "validationtype", "ad_client_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "isorderbyvalue", "updated", "updatedby", "vformat") from STDIN;
100	AD_Validation Rules Parent	T	0	0	1999-05-21 00:00:00	0	Validation rule Parent selection	D	\N	Y	N	2000-01-02 00:00:00	0	\N
111	C_Conversion_Rate Types	L	0	0	1999-06-22 00:00:00	0	Conversion Rate Type list	D	\N	Y	N	2000-01-02 00:00:00	0	L
113	AD_Org Parent	T	0	0	1999-06-22 00:00:00	0	Organization Parent selection	D	\N	Y	N	2000-01-02 00:00:00	0	\N
121	X12DE98 Entity Identifier Code	L	0	0	1999-06-23 00:00:00	0	X12DE98 Entity Identifier Code list	D	Identify organizational entity physical location property or individual list	Y	N	2000-01-02 00:00:00	0	AA
145	C_Element	T	0	0	1999-07-07 00:00:00	0	Element selection	D	\N	Y	N	2000-01-02 00:00:00	0	\N
161	M_Product Parent	T	0	0	1999-08-10 00:00:00	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
169	C_Project Parent	T	0	0	1999-08-10 00:00:00	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
175	C_ValidCombination	T	0	0	1999-09-27 00:00:00	0	Valid Account combinations	D	\N	Y	N	2000-01-02 00:00:00	0	\N
179	C_SalesRegion Parent	T	0	0	1999-09-29 00:00:00	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
180	_ErrorType	L	0	0	1999-09-29 00:00:00	0	List of type of errors	D	\N	Y	N	2000-01-02 00:00:00	0	L
193	C_BankAccount for Credit Card	T	0	0	2000-01-25 20:36:42	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
198	M_Inventory ReportType	L	0	0	2000-02-06 21:35:39	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
199	M_PriceList_Version for Client	T	0	0	2000-02-07 16:40:23	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
208	AD_ImpFormat Type	L	0	0	2000-09-15 15:02:42	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	L
218	R_Request Request Type	L	0	0	2001-01-11 18:08:32	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	L
226	AD_Role User Level	L	0	0	2001-04-05 17:51:49	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
228	PA_Goal	T	0	0	2001-04-17 22:27:16	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
229	PA_Achievement Parent	T	0	0	2001-04-17 23:12:28	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
245	_Entity Type	L	0	0	2001-11-18 18:22:35	0	Entity Type (Dictionary, ..)	D	\N	Y	N	2000-01-02 00:00:00	0	AAAA
269	AD_PrintFormat Not TableBased	T	0	0	2002-09-14 15:15:05	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
285	K_Entry Related	T	0	0	2003-07-10 21:23:26	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
294	M_TransactionAllocation Type	L	0	0	2003-12-14 22:58:09	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
297	M_ProductionLine	T	0	0	2003-12-14 23:10:50	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
298	M_Transaction	T	0	0	2003-12-14 23:11:47	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
300	WF_Transition Type	L	0	0	2003-12-31 00:18:20	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
308	WF_LoopType	L	0	0	2003-12-31 18:25:05	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
315	B_TopicType AuctionType	L	0	0	2004-02-19 11:47:30	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
323	C_Allocation	T	0	0	2004-06-10 20:26:38	0	\N	D	\N	Y	N	2000-01-02 00:00:00	0	\N
330	AD_Role PreferenceType	L	0	0	2004-11-27 11:37:22	0	Preference Type	D	\N	Y	N	2000-01-02 00:00:00	0	\N
391	K_IndexLog QuerySource	L	0	0	2006-06-24 12:53:44	100	\N	D	\N	Y	N	2006-06-24 12:53:55	100	\N
53317	AD_Role	T	0	0	2009-07-27 19:49:07	0	\N	D	\N	Y	N	2009-07-27 19:49:07	0	\N
53333	RelType C_Order (Sales) <= C_Invoice_ID (Customer)	T	0	0	2009-11-13 15:26:25	100	Finds C_Order_IDs for a given C_Invoice_ID	D	\N	Y	N	2010-04-08 18:19:22	100	\N
53334	RelType C_Invoice (Customer) <= C_Order_ID (Sales)	T	0	0	2009-11-13 15:27:40	100	Finds C_Invoice_IDs for a given C_C_Order_ID	D	\N	Y	N	2010-04-08 18:20:00	100	\N
53335	C_BPartner -Active,Manufacturer, Non summary	T	0	0	2009-11-29 01:01:09	100	\N	D	\N	Y	N	2016-04-11 16:46:54	0	\N
53345	RelType M_InOut (Shipment) <= C_Invoice_ID (Customer)	T	0	0	2010-03-20 16:57:18	100	\N	D	\N	Y	N	2010-04-08 18:21:57	100	\N
53347	RelType M_InOut (Shipment) <=  C_Order_ID	T	0	0	2010-03-20 16:59:59	100	\N	D	\N	Y	N	2010-04-08 18:02:59	100	\N
53348	RelType C_Invoice (Customer) <= M_InOut_ID (Shipment)	T	0	0	2010-03-20 17:01:13	100	\N	D	\N	Y	N	2010-04-08 18:21:31	100	\N
53349	RelType C_Order (Sales) <=  M_InOut_ID (Shipment)	T	0	0	2010-03-20 17:02:08	100	\N	D	\N	Y	N	2010-04-08 18:17:58	100	\N
53352	RelType C_BPartner <= Vendor/Customer Return	T	0	0	2010-03-22 07:40:39	100	\N	D	\N	Y	N	2010-03-22 13:40:28	100	\N
53353	RelType Customer Return <= C_BPartner_ID	T	0	0	2010-03-22 07:44:29	100	\N	D	\N	Y	N	2010-03-22 13:40:39	100	\N
53354	RelType Vendor Return <= C_BPartner_ID	T	0	0	2010-03-22 07:56:32	100	\N	D	\N	Y	N	2010-03-22 13:37:03	100	\N
53417	C_Order for opportunity	T	0	0	2011-09-02 13:49:26	100	\N	D	\N	Y	N	2011-09-02 13:49:26	100	\N
53562	Aisle (X)	T	0	0	2013-09-23 11:52:47	100	Warehouse locator Aisle (X)	D	\N	Y	N	2013-09-23 11:52:47	100	\N
53714	_TaxRateOverride	L	0	0	2014-08-06 13:48:01	100	\N	D	\N	Y	N	2014-08-06 13:48:01	100	\N
53776	M_InOut_Manifest (Open)	T	0	0	2015-04-24 14:38:30	100	\N	D	\N	Y	N	2015-04-24 14:38:30	100	\N
53782	M_Production_MiniMRP	T	0	0	2015-06-29 17:42:40	100	\N	D	\N	Y	N	2015-06-29 17:42:40	100	\N
53797	Dunning Age Group	L	0	0	2015-09-23 12:19:47	100	\N	D	\N	Y	Y	2016-04-11 16:52:25	0	\N
53839	C_Location_AX	T	0	0	2016-06-20 21:32:19	100	Location Print Out	D	\N	Y	N	2016-06-20 21:32:19	100	\N
\.
