copy "ad_modelvalidator" ("ad_modelvalidator_id", "name", "ad_client_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "modelvalidationclass", "seqno", "updated", "updatedby") from STDIN;
50000	Model Validator to Libero Manufacturing Management	0	0	2008-01-03 14:14:48	0	\N	D	\N	Y	org.eevolution.model.LiberoValidator	1	2009-02-10 00:00:00	0
50006	Sales Management Validator	0	0	2011-11-04 16:01:25	100	\N	D	\N	Y	org.compiere.model.SalesMgmtValidator	0	2011-11-04 00:00:00	100
50019	Model validator for fixed asset	0	0	2014-07-14 15:59:26	100	\N	D	\N	Y	com.astidian.compiere.fa.Validator	0	2014-07-14 00:00:00	100
50023	Product Validator	0	0	2015-09-21 10:25:26	100	Set default replenishment for Product	D	\N	Y	org.compiere.model.ProductValidator	30	2015-09-21 00:00:00	100
50025	InterCompany Due Validator	0	0	2016-09-08 18:52:59	100	\N	D	\N	Y	org.compiere.model.InterCompanyDueValidator	40	2016-09-08 00:00:00	100
\.
