copy "ad_element" ("ad_element_id", "columnname", "ad_client_id", "ad_org_id", "ad_reference_id", "ad_reference_value_id", "created", "createdby", "description", "entitytype", "fieldlength", "help", "isactive", "name", "po_description", "po_help", "po_name", "po_printname", "printname", "updated", "updatedby") from STDIN;
127	AD_TaskInstance_ID	0	0	13	\N	1999-11-19 10:07:43	0	\N	D	22	\N	Y	Task Instance	\N	\N	\N	\N	Task Instance	2000-01-02 00:00:00	0
163	AmtApproval	0	0	12	\N	1999-11-19 10:07:43	0	The approval amount limit for this role	D	22	The Approval Amount field indicates the amount limit this Role has for approval of documents.	Y	Approval Amount	\N	\N	\N	\N	Approval Amt	2000-01-02 00:00:00	0
472	Node_ID	0	0	13	\N	1999-11-19 10:07:43	0	\N	D	22	\N	Y	Node	\N	\N	\N	\N	Node	2010-03-29 16:38:46	100
615	UserLevel	0	0	17	226	1999-11-19 10:07:43	0	System Client Organization	D	3	The User Level field determines if users of this Role will have access to System level data, Organization level data, Client level data or Client and Organization level data.	Y	User Level	\N	\N	\N	\N	User Level	2000-01-02 00:00:00	0
1417	Product	0	0	10	\N	2000-12-17 16:23:13	0	\N	D	40	\N	Y	Product	\N	\N	\N	\N	Product	2000-01-02 00:00:00	0
1433	Session_ID	0	0	13	\N	2000-12-17 16:23:13	0	\N	D	60	\N	Y	Session ID	\N	\N	\N	\N	Session ID	2000-01-02 00:00:00	0
1446	W_Basket_ID	0	0	13	\N	2000-12-17 16:23:13	0	Web Basket	D	22	Temporary Web Basket	Y	Basket	\N	\N	\N	\N	Basket	2010-03-29 16:42:59	100
1484	V_Date	0	0	15	\N	2000-12-22 22:27:31	0	\N	D	7	\N	Y	Date	\N	\N	\N	\N	Date	2010-03-29 16:41:18	100
1485	V_Number	0	0	10	\N	2000-12-22 22:27:31	0	\N	D	22	\N	Y	Number	\N	\N	\N	\N	Number	2010-03-29 16:42:03	100
1486	V_String	0	0	10	\N	2000-12-22 22:27:31	0	\N	D	2000	\N	Y	String	\N	\N	\N	\N	String	2010-03-29 16:42:43	100
1667	IsSimulation	0	0	\N	\N	2001-11-18 16:03:59	0	Performing the function is only simulated	D	\N	\N	Y	Simulation	\N	\N	\N	\N	Simulation	2000-01-02 00:00:00	0
1671	MaxInvWriteOffAmt	0	0	\N	\N	2001-11-18 16:27:47	0	Maximum invoice amount to be written off in invoice currency	D	\N	\N	Y	Maximum write-off per Invoice	\N	\N	\N	\N	Maximum write-off per Invoice	2000-01-02 00:00:00	0
1672	MustBeStocked	0	0	\N	\N	2001-11-18 16:27:47	0	If not sufficient in stock in the warehouse, the BOM is not produced	D	\N	\N	Y	Product quantity must be in stock	\N	\N	\N	\N	Product quantity must be in stock	2005-08-31 18:23:23	100
1677	ShowActualAmt	0	0	\N	\N	2001-11-18 16:27:47	0	\N	D	\N	\N	Y	Show Actual Amount	\N	\N	\N	\N	Show Actual Amount	2000-01-02 00:00:00	0
1678	ShowCommittedAmt	0	0	\N	\N	2001-11-18 16:27:47	0	\N	D	\N	\N	Y	Show Committed Amount	\N	\N	\N	\N	Show Committed Amount	2000-01-02 00:00:00	0
1679	ShowPlannedAmt	0	0	\N	\N	2001-11-18 16:27:47	0	\N	D	\N	\N	Y	Show Planned Amount	\N	\N	\N	\N	Show Planned Amount	2000-01-02 00:00:00	0
1680	ShowPlannedMarginAmt	0	0	\N	\N	2001-11-18 16:27:47	0	\N	D	\N	\N	Y	Show Planned Margin Amount	\N	\N	\N	\N	Show Planned Margin Amount	2000-01-02 00:00:00	0
1681	ShowPlannedQty	0	0	\N	\N	2001-11-18 16:27:47	0	\N	D	\N	\N	Y	Show Planned Quantity	\N	\N	\N	\N	Show Planned Quantity	2000-01-02 00:00:00	0
1692	DeletePosting	0	0	\N	\N	2001-12-02 12:39:41	0	The selected accounting entries will be deleted!  DANGEROUS !!!	D	\N	\N	Y	Delete existing Accounting Entries	\N	\N	\N	\N	Delete existing Accounting Entries	2000-01-02 00:00:00	0
1836	AmtInWords	0	0	12	\N	2002-08-16 18:15:10	0	Amount in words	D	22	Amount in words will be printed.	Y	Amt in Words	\N	\N	\N	\N	Amt in Words	2000-01-02 00:00:00	0
1838	BPGreeting	0	0	10	\N	2002-08-16 18:15:10	0	Greeting for Business Partner	D	60	\N	Y	BP Greeting	\N	\N	\N	\N	BP Greeting	2000-01-02 00:00:00	0
1841	DocumentType	0	0	10	\N	2002-08-16 18:15:10	0	Document Type	D	60	\N	Y	Document Type	\N	\N	\N	\N	Document Type	2000-01-02 00:00:00	0
1842	DocumentTypeNote	0	0	10	\N	2002-08-16 18:15:10	0	Optional note of a document type	D	2000	\N	Y	Document Type Note	\N	\N	\N	\N	Document Type Note	2000-01-02 00:00:00	0
1847	PaymentTerm	0	0	10	\N	2002-08-16 18:15:10	0	Payment Term	D	60	\N	Y	Payment Term	\N	\N	\N	\N	Payment Term	2000-01-02 00:00:00	0
1848	PaymentTermNote	0	0	10	\N	2002-08-16 18:15:10	0	Note of a Payment Term	D	2000	\N	Y	Payment Term Note	\N	\N	\N	\N	Payment Term Note	2000-01-02 00:00:00	0
1849	ResourceDescription	0	0	10	\N	2002-08-16 18:15:10	0	Resource Allocation Description	D	255	\N	Y	Resource Description	\N	\N	\N	\N	Resource Description	2000-01-02 00:00:00	0
1874	Org_Location_ID	0	0	21	\N	2002-08-24 14:31:26	0	Organization Location/Address	D	22	\N	Y	Org Address	\N	\N	\N	\N	Org Address	2000-01-02 00:00:00	0
1875	Warehouse_Location_ID	0	0	21	\N	2002-08-24 14:31:26	0	Warehouse Location/Address	D	22	Address of Warehouse	Y	Warehouse Address	\N	\N	\N	\N	Warehouse Address	2000-01-02 00:00:00	0
1876	BPValue	0	0	10	\N	2002-08-25 11:31:32	0	Business Partner Key Value	D	40	Search Key of Business Partner	Y	BP Search Key	\N	\N	\N	\N	Customer No	2000-01-02 00:00:00	0
2006	W_BasketLine_ID	0	0	13	\N	2003-05-04 00:31:52	0	Web Basket Line	D	22	Temporary Web Basket Line	Y	Basket Line	\N	\N	\N	\N	Basket Line	2000-01-02 00:00:00	0
2139	IsPublic	0	0	20	\N	2003-07-10 20:16:00	0	Public can read entry	D	1	If selected, public users can read/view the entry. Public are users without a Role in the system. Use security rules for more specific access control.	Y	Public	\N	\N	\N	\N	Public	2010-01-13 10:38:15	0
2140	IsPublicWrite	0	0	20	\N	2003-07-10 20:16:00	0	Public can write entries	D	1	If selected, public users can write/create entries. Public are users without a Role in the system. Use security rules for more specific access control.	Y	Public Write	\N	\N	\N	\N	Public Write	2010-01-13 10:38:15	0
2141	K_Category_ID	0	0	19	\N	2003-07-10 20:16:00	0	Knowledge Category	D	22	Set up knowledge categories and values as a search aid. Examples are Release Version, Product Area, etc. Knowledge Category values act like keywords.	Y	Knowledge Category	\N	\N	\N	\N	Category	2010-01-13 10:38:15	0
2142	K_CategoryValue_ID	0	0	19	\N	2003-07-10 20:16:00	0	The value of the category	D	22	The value of the category is a keyword	Y	Category Value	\N	\N	\N	\N	Value	2000-01-02 00:00:00	0
2143	K_Comment_ID	0	0	13	\N	2003-07-10 20:16:00	0	Knowledge Entry Comment	D	22	Comment regarding a knowledge entry	Y	Entry Comment	\N	\N	\N	\N	Comment	2000-01-02 00:00:00	0
2144	K_Entry_ID	0	0	30	\N	2003-07-10 20:16:00	0	Knowledge Entry	D	22	The searchable Knowledge Entry	Y	Entry	\N	\N	\N	\N	Entry	2000-01-02 00:00:00	0
2145	K_EntryRelated_ID	0	0	18	285	2003-07-10 20:16:00	0	Related Entry for this Entry	D	22	Related Knowledge Entry for this Knowledge Entry	Y	Related Entry	\N	\N	\N	\N	Related Entry	2010-01-13 10:38:15	0
2146	K_Source_ID	0	0	13	\N	2003-07-10 20:16:00	0	Source of a Knowledge Entry	D	22	The Source of a Knowledge Entry is a pointer to the originating system. The Knowledge Entry has an additional entry (Description URL)  for more detailed info.	Y	Knowledge Source	\N	\N	\N	\N	Knowledge Source	2010-01-13 10:38:15	0
2147	K_Synonym_ID	0	0	13	\N	2003-07-10 20:16:00	0	Knowledge Keyword Synonym	D	22	Search Synonyms for Knowledge Keywords; Example: Product = Item	Y	Knowledge Synonym	\N	\N	\N	\N	Knowledge Synonym	2010-01-13 10:38:15	0
2148	K_Topic_ID	0	0	13	\N	2003-07-10 20:16:00	0	Knowledge Topic	D	22	Topic or Discussion Thead	Y	Knowledge Topic	\N	\N	\N	\N	Topic	2010-01-13 10:38:15	0
2149	K_Type_ID	0	0	13	\N	2003-07-10 20:16:00	0	Knowledge Type	D	22	Area of knowledge - A Type has multiple Topics	Y	Knowledge Type	\N	\N	\N	\N	Type	2010-01-13 10:38:15	0
2150	Keywords	0	0	10	\N	2003-07-10 20:16:00	0	List of Keywords - separated by space, comma or semicolon	D	255	List if individual keywords for search relevancy. The keywords are separated by space, comma or semicolon. 	Y	Keywords	\N	\N	\N	\N	Keywords	2000-01-02 00:00:00	0
2151	SynonymName	0	0	10	\N	2003-07-10 20:16:00	0	The synonym for the name	D	60	The synonym broadens the search	Y	Synonym Name	\N	\N	\N	\N	Synonym Name	2000-01-02 00:00:00	0
2209	IsPersonalAccess	0	0	20	\N	2003-10-07 15:10:01	0	Allow access to all personal records	D	1	Users of this role have access to all records locked as personal.	Y	Personal Access	\N	\N	\N	\N	Personal Access	2000-01-02 00:00:00	0
2210	IsPersonalLock	0	0	20	\N	2003-10-07 15:10:01	0	Allow users with role to lock access to personal records	D	1	If enabled, the user with the role can prevent access of others to personal records.  If a record is locked, only the user or people who can read personal locked records can see the record.	Y	Personal Lock	\N	\N	\N	\N	Personal Lock	2000-01-02 00:00:00	0
2213	IsShowAcct	0	0	20	\N	2003-10-07 15:10:01	0	Users with this role can see accounting information	D	1	This allows to prevent access to any accounting information.	Y	Show Accounting	\N	\N	\N	\N	Show Accounting	2000-01-02 00:00:00	0
2266	BPTaxID	0	0	10	\N	2003-12-07 20:19:08	0	Tax ID of the Business Partner	D	20	\N	Y	Partner Tax ID	\N	\N	\N	\N	BP Tax TD	2000-01-02 00:00:00	0
2267	QtyBackOrdered	0	0	29	\N	2003-12-07 22:59:51	0	Backordered Quantity	D	22	Calculated: ordered - delivered quantity	Y	Backordered	\N	\N	\N	\N	Backordered	2000-01-02 00:00:00	0
2270	AllocationStrategyType	0	0	17	294	2003-12-14 22:53:05	0	Allocation Strategy	D	1	Allocation from incoming to outgoing transactions	Y	Allocation Strategy	\N	\N	\N	\N	Allocation	2000-01-02 00:00:00	0
2271	Out_M_InOutLine_ID	0	0	18	295	2003-12-14 22:53:06	0	Outgoing Shipment/Receipt	D	22	\N	Y	Out Shipment Line	\N	\N	\N	\N	Out Shipment Line	2000-01-02 00:00:00	0
2272	Out_M_InventoryLine_ID	0	0	18	296	2003-12-14 22:53:06	0	Outgoing Inventory Line	D	22	\N	Y	Out Inventory Line	\N	\N	\N	\N	Out Inventory Line	2000-01-02 00:00:00	0
2273	Out_M_ProductionLine_ID	0	0	18	297	2003-12-14 22:53:06	0	Outgoing Production Line	D	22	\N	Y	Out Production Line	\N	\N	\N	\N	Out Production Line	2000-01-02 00:00:00	0
2274	Out_M_Transaction_ID	0	0	18	298	2003-12-14 22:53:06	0	Outgoing Transaction	D	22	\N	Y	Out Transaction	\N	\N	\N	\N	Out Transaction	2000-01-02 00:00:00	0
2280	WarehouseName	0	0	10	\N	2003-12-22 11:29:44	0	Warehouse Name	D	60	\N	Y	Warehouse	\N	\N	\N	\N	Warehouse	2000-01-02 00:00:00	0
2305	AD_User_Substitute_ID	0	0	13	\N	2004-01-01 17:58:51	0	Substitute of the user	D	22	A user who can act for another user.	Y	User Substitute	\N	\N	\N	\N	User Substitute	2000-01-02 00:00:00	0
2345	MaintenanceMode	0	0	\N	\N	2004-01-16 23:50:14	0	Language Maintenance Mode	D	\N	\N	Y	Maintenance Mode	\N	\N	\N	\N	Maintenance Mode	2000-01-02 00:00:00	0
2467	IsCanApproveOwnDoc	0	0	20	\N	2004-03-18 12:04:05	0	Users with this role can approve their own documents	D	1	If a user cannot approve their own documents (orders, etc.), it needs to be approved by someone else.	Y	Approve own Documents	\N	\N	\N	\N	Approve own	2000-01-02 00:00:00	0
2502	Bill_BPTaxID	0	0	10	\N	2004-04-22 14:57:49	0	\N	D	20	\N	Y	Invoice Tax ID	\N	\N	\N	\N	Invoice Tax ID	2000-01-02 00:00:00	0
2503	Bill_BPValue	0	0	10	\N	2004-04-22 14:57:49	0	\N	D	40	\N	Y	Invoice Partner Key	\N	\N	\N	\N	Invoice Partner Key	2000-01-02 00:00:00	0
2504	Bill_C_Location_ID	0	0	21	\N	2004-04-22 14:57:49	0	Address Used for Invoicing	D	22	\N	Y	Invoice Address	\N	\N	\N	\N	Invoice Address	2000-01-02 00:00:00	0
2505	Bill_ContactName	0	0	10	\N	2004-04-22 14:57:49	0	\N	D	60	\N	Y	Invoice Contact Name	\N	\N	\N	\N	Invoice Contact Name	2000-01-02 00:00:00	0
2506	Bill_Name	0	0	10	\N	2004-04-22 14:57:49	0	\N	D	60	\N	Y	Invoice Name	\N	\N	\N	\N	Invoice Name	2000-01-02 00:00:00	0
2507	Bill_Name2	0	0	10	\N	2004-04-22 14:57:49	0	\N	D	60	\N	Y	Invoice Name2	\N	\N	\N	\N	Invoice Name2	2000-01-02 00:00:00	0
2508	Bill_Phone	0	0	10	\N	2004-04-22 14:57:49	0	\N	D	40	\N	Y	Invoice Phone	\N	\N	\N	\N	Invoice Phone	2000-01-02 00:00:00	0
2509	Bill_Title	0	0	10	\N	2004-04-22 14:57:49	0	\N	D	40	\N	Y	Invoice Title	\N	\N	\N	\N	Invoice Title	2000-01-02 00:00:00	0
2511	BPName2	0	0	10	\N	2004-04-22 14:57:49	0	\N	D	60	\N	Y	BP Name2	\N	\N	\N	\N	BP Name2	2000-01-02 00:00:00	0
2532	ShipDescription	0	0	10	\N	2004-06-09 18:16:55	0	\N	D	255	\N	Y	Ship Description	\N	\N	Receipt Decription	Receipt Description	Ship Description	2000-01-02 00:00:00	0
2573	OnlySOTrx	0	0	\N	\N	2004-07-05 15:13:30	0	Otherwise also Payments and AP Invoices	D	\N	\N	Y	Only Sales Invoices	\N	\N	\N	\N	Only Sales Invoices	2000-01-02 00:00:00	0
2579	ValueNumber	0	0	22	\N	2004-07-07 17:42:41	0	Numeric Value	D	22	\N	Y	Value	\N	\N	\N	\N	Value	2000-01-02 00:00:00	0
2587	IsAccessAllOrgs	0	0	20	\N	2004-07-20 17:03:56	0	Access all Organizations (no org access control) of the client	D	1	When selected, the role has access to all organizations of the client automatically. This also increases performance where you have many organizations.	Y	Access all Orgs	\N	\N	\N	\N	Access all Orgs	2000-01-02 00:00:00	0
2590	PriceEnteredList	0	0	37	\N	2004-07-23 22:13:01	0	Entered List Price	D	22	Price List converted to entered UOM	Y	List Prive	\N	\N	\N	\N	List Price	2000-01-02 00:00:00	0
2628	DetailsSourceFirst	0	0	\N	\N	2004-08-26 01:19:31	0	Details and Sources are printed before the Line	D	\N	\N	Y	Details/Source First	\N	\N	\N	\N	Details/Source First	2000-01-02 00:00:00	0
2646	CheckNewValue	0	0	\N	\N	2004-09-28 11:44:40	0	Ensure that the new value of the change is the current value in the system (i.e. no change since then)	D	\N	\N	Y	Validate current (new) Value	\N	\N	\N	\N	Validate current (new) Value	2000-01-02 00:00:00	0
2647	CheckOldValue	0	0	\N	\N	2004-09-28 11:44:40	0	Ensure that the old value of the change is the current value in the system (i.e. original situation)	D	\N	\N	Y	Validate current (old) Value	\N	\N	\N	\N	Validate current (old) Value	2000-01-02 00:00:00	0
2651	ProductDescription	0	0	10	\N	2004-10-10 21:57:32	0	Product Description	D	255	Description of the product	Y	Product Description	\N	\N	\N	\N	Prod Description	2000-01-02 00:00:00	0
2656	PreferenceType	0	0	17	330	2004-11-27 11:47:12	0	Determines what preferences the user can set	D	1	Preferences allow you to define default values.  If set to None, you cannot set any preference nor value preference. Only if set to Client, you can see the Record Info Change Log.	Y	Preference Level	\N	\N	\N	\N	Preference Level	2000-01-02 00:00:00	0
2657	OverwritePriceLimit	0	0	20	\N	2004-11-27 22:28:40	0	Overwrite Price Limit if the Price List  enforces the Price Limit	D	1	The Price List allows to enforce the Price Limit. If set, a user with this role can overwrite the price limit (i.e. enter any price).	Y	Overwrite Price Limit	\N	\N	\N	\N	Overwrite Price Limit	2000-01-02 00:00:00	0
2662	AllocateOldest	0	0	\N	\N	2004-11-30 01:52:41	0	Allocate payments to the oldest invoice	D	\N	Allocate payments to the oldest invoice. There might be an unallocated amount remaining.	Y	Allocate Oldest First	\N	\N	\N	\N	Allocate Oldest First	2000-01-02 00:00:00	0
2666	IsUnconfirmedInOut	0	0	\N	\N	2005-01-05 21:33:49	0	Generate shipments for Orders with open delivery confirmations?	D	\N	You can also include orders who have outstanding confirmations (e.g. ordered=10 - not confirmed shipments=4 - would create a new shipment of 6 if available).	Y	Orders with unconfirmed Shipments	\N	\N	\N	\N	Orders with unconfirmed Shipments	2005-01-05 21:33:49	0
2671	AD_Archive_ID	0	0	13	\N	2005-01-10 17:52:29	0	Document and Report Archive	D	22	Depending on the Client Automatic Archive Level documents and reports are saved and available for view.	Y	Archive	\N	\N	\N	\N	Archive	2005-01-10 18:23:40	100
2696	IsUseUserOrgAccess	0	0	20	\N	2005-04-21 20:51:19	100	Use Org Access defined by user instead of Role Org Access	D	1	You can define the access to Organization either by Role or by User.  You would select this, if you have many organizations.	Y	Use User Org Access	\N	\N	\N	\N	Use User Org Access	2005-04-21 20:54:33	100
2698	AllTables	0	0	\N	\N	2005-04-21 21:50:30	0	Check not just this table	D	\N	\N	Y	Check all DB Tables	\N	\N	\N	\N	Check all DB Tables	2005-04-21 21:50:30	0
2838	AD_UserQuery_ID	0	0	13	\N	2005-09-09 14:56:03	100	Saved User Query	D	10	\N	Y	User Query	\N	\N	\N	\N	User Query	2005-09-09 14:56:59	100
2853	ConfirmQueryRecords	0	0	11	\N	2005-10-08 12:42:49	100	Require Confirmation if more records will be returned by the query (If not defined 500)	D	10	Enter the number of records the query will return without confirmation to avoid unnecessary system load.  If 0, the system default of 500 is used.	Y	Confirm Query Records	\N	\N	\N	\N	Confirm Query Records	2010-01-13 10:38:15	100
2973	Meta_Copyright	0	0	10	\N	2006-03-26 15:00:23	100	Contains Copyright information for the content	D	2000	This Tag contains detailed information about the content's copyright situation, how holds it for which timeframe etc.	Y	Meta Copyright	\N	\N	\N	\N	Meta Copyright	2006-04-05 09:57:10	100
2974	Meta_Publisher	0	0	10	\N	2006-03-26 15:00:23	100	Meta Publisher defines the publisher of the content	D	2000	As author and publisher must not be the same person this tag saves the responsible publisher for the content	Y	Meta Publisher	\N	\N	\N	\N	Meta Publisher	2006-04-05 09:55:04	100
2975	Meta_RobotsTag	0	0	10	\N	2006-03-26 15:00:23	100	RobotsTag defines how search robots should handle this content	D	2000	The Meta Robots Tag define on how a search engines robot should handle this page and the following ones. It defines two keywords: (NO)INDEX which defines whether or not to index this content and (NO)FOLLOW which defines whether or not to follow links. The most common combination is INDEX,FOLLOW which will force a search robot to index the content and follow links and images.	Y	Meta RobotsTag	\N	\N	\N	\N	Meta RobotsTag	2010-01-13 10:38:15	100
2976	Meta_Author	0	0	10	\N	2006-03-26 15:00:23	100	Author of the content	D	2000	Author of the content for the Containers Meta Data	Y	Meta Author	\N	\N	\N	\N	Meta Author	2006-04-05 09:45:29	100
2977	Meta_Content	0	0	10	\N	2006-03-26 15:00:23	100	Defines the type of content i.e. "text/html; charset=UTF-8"	D	2000	With this tag you can overwrite the type of content and how search engines will interpret it. You should keep in mind that this will not influence how the Server and Client interpret the content.	Y	Meta Content Type	\N	\N	\N	\N	Meta Content Type	2006-04-05 10:04:08	100
2981	IsUseAd	0	0	20	\N	2006-03-26 15:01:55	100	Whether or not this templates uses Ad's	D	1	This describe whether or not this Template will use Ad's	Y	Use Ad	\N	\N	\N	\N	Use Ad	2006-04-05 10:24:46	100
2982	IsNews	0	0	20	\N	2006-03-26 15:01:55	100	Template or container uses news channels	D	1	This content (container or template) uses news channels	Y	Uses News	\N	\N	\N	\N	Uses News	2006-04-05 10:23:52	100
2983	Elements	0	0	14	\N	2006-03-26 15:01:55	100	Contains list of elements separated by CR	D	2000	Contains a list of elements this template uses separated by a Carriage Return. Last line should be empty	Y	Elements	\N	\N	\N	\N	Elements	2010-01-13 10:38:15	100
2984	TemplateXST	0	0	36	\N	2006-03-26 15:01:55	100	Contains the template code itself	D	0	Here we include the template code itself	Y	TemplateXST	\N	\N	\N	\N	TemplateXST	2006-04-05 10:22:45	100
2986	Notice	0	0	14	\N	2006-03-26 15:03:19	100	Contains last write notice	D	2000	Contains info on what changed with the last write	Y	Notice	\N	\N	\N	\N	Notice	2006-04-05 10:41:34	100
2987	ContainerType	0	0	17	385	2006-03-26 15:03:19	100	Web Container Type	D	1	This parameter defines the type of content for this container.	Y	Web Container Type	\N	\N	\N	\N	Container Type	2006-04-16 15:55:09	100
2988	ContainerLinkURL	0	0	40	\N	2006-03-26 15:03:19	100	External Link (URL) for the Container	D	60	External URL for the Container\n	Y	External Link (URL)	\N	\N	\N	\N	External Link	2010-01-13 10:38:15	100
2989	RelativeURL	0	0	10	\N	2006-03-26 15:03:19	100	Contains the relative URL for the container	D	120	The relative URL is used together with the webproject domain to display the content	Y	Relative URL	\N	\N	\N	\N	Relative URL	2006-04-05 10:48:08	100
2992	Meta_Description	0	0	14	\N	2006-03-26 15:03:19	100	Meta info describing the contents of the page	D	2000	The Meta Description tag should contain a short description on the page content	Y	Meta Description	\N	\N	\N	\N	Meta Description	2006-04-05 10:40:58	100
2993	Meta_Keywords	0	0	14	\N	2006-03-26 15:03:19	100	Contains the keywords for the content	D	2000	Contains keyword info on the main search words this content is relevant to	Y	Meta Keywords	\N	\N	\N	\N	Meta Keywords	2006-04-05 10:45:11	100
2994	StructureXML	0	0	14	\N	2006-03-26 15:03:19	100	Autogenerated Containerdefinition as XML Code	D	2000	Autogenerated Containerdefinition as XML Code	Y	StructureXML	\N	\N	\N	\N	StructureXML	2006-04-05 10:48:42	100
2995	ContainerXML	0	0	14	\N	2006-03-26 15:03:19	100	Autogenerated Containerdefinition as XML Code	D	2000	Autogenerated Containerdefinition as XML Code	Y	ContainerXML	\N	\N	\N	\N	ContainerXML	2006-04-05 10:27:26	100
2998	MediaType	0	0	17	388	2006-03-26 15:06:01	100	Defines the media type for the browser	D	3	The browser and the media server need info on the type of content	Y	Media Type	\N	\N	\N	\N	Media Type	2006-05-03 14:31:57	100
3000	Target_Frame	0	0	10	\N	2006-03-26 15:06:51	100	Which target should be used if user clicks?	D	20	Do we want the content to stay in same window, to open up a new one or to place it in a special frame?	Y	Target Frame	\N	\N	\N	\N	Target Frame	2006-04-05 11:48:29	100
3001	ActualClick	0	0	11	\N	2006-03-26 15:06:51	100	How many clicks have been counted	D	10	Contains info on the actual click count until now	Y	Actual Click Count	\N	\N	\N	\N	Actual Click Count	2006-04-05 11:39:04	100
3002	MaxClick	0	0	11	\N	2006-03-26 15:06:51	100	Maximum Click Count until banner is deactivated	D	10	A banner has a maximum number of clicks after which it will get deactivated	Y	Max Click Count	\N	\N	\N	\N	Max Click Count	2006-04-05 11:43:23	100
3003	ActualImpression	0	0	11	\N	2006-03-26 15:06:51	100	How many impressions have been counted	D	10	Contains info on the actual impression count until now	Y	Actual Impression Count	\N	\N	\N	\N	Actual Impression Count	2006-04-05 11:41:01	100
3004	MaxImpression	0	0	11	\N	2006-03-26 15:06:51	100	Maximum Impression Count until banner is deactivated	D	10	A banner has a maximum number of impressions after which it will get deactivated	Y	Max Impression Count	\N	\N	\N	\N	Max Impression Count	2006-04-05 11:44:47	100
3005	StartImpression	0	0	11	\N	2006-03-26 15:06:51	100	For rotation we need a start count	D	10	If we run banners in rotation we always show the one with the min of impressions, so if a new banner is added to impressions we don't want it to show up so often we set a startimpressions value. StartImpression+ActualImpression=CurrentImpression	Y	Start Count Impression	\N	\N	\N	\N	Start Count Impression	2006-04-05 11:47:31	100
3006	ContentHTML	0	0	14	\N	2006-03-26 15:06:51	100	Contains the content itself	D	2000	Contains the content itself as HTML code. Should normally only use basic tags, no real layouting	Y	Content HTML	\N	\N	\N	\N	Content HTML	2006-04-05 10:54:07	100
3007	IsAdFlag	0	0	20	\N	2006-03-26 15:06:51	100	Do we need to specially mention this ad?	D	1	If we have a block in content where announce content and also sponsored links we should mention the sponsored ones	Y	Special AD Flag	\N	\N	\N	\N	Special AD Flag	2010-01-13 10:38:15	100
3008	IsLogged	0	0	20	\N	2006-03-26 15:06:51	100	Do we need to log the banner impressions and clicks? (needs much performance)	D	1	As of performance we should only log banners if really necessary, as this takes a lot of performance	Y	Logging	\N	\N	\N	\N	Logging	2006-04-05 11:45:53	100
3010	IsPassive	0	0	20	\N	2006-03-26 15:07:59	100	FTP passive transfer	D	1	Should the transfer be run in passive mode?	Y	Transfer passive	\N	\N	\N	\N	Transfer passive	2006-04-05 11:19:46	100
3011	IP_Address	0	0	10	\N	2006-03-26 15:07:59	100	Defines the IP address to transfer data to	D	20	Contains info on the IP address to which we will transfer data	Y	IP Address	\N	\N	\N	\N	IP Address	2006-04-05 11:19:05	100
3012	Folder	0	0	10	\N	2006-03-26 15:07:59	100	A folder on a local or remote system to store data into	D	60	We store files in folders, especially media files.	Y	Folder	\N	\N	\N	\N	Folder	2006-04-05 11:18:13	100
3019	Checked	0	0	16	\N	2006-03-26 15:16:08	100	Info when we did the last check	D	7	Info on the last check date	Y	Last Checked	\N	\N	\N	\N	Last Checked	2006-04-05 11:04:21	100
3021	Last_Result	0	0	14	\N	2006-03-26 15:16:08	100	Contains data on the last check result	D	2000	If we ran into errors etc. you will find the details in here	Y	Last Result	\N	\N	\N	\N	Last Result	2006-04-05 11:05:18	100
3025	Link	0	0	10	\N	2006-03-26 15:26:31	100	Contains URL to a target	D	255	A Link should contain info on how to get to more information	Y	Link	\N	\N	\N	\N	Link	2006-04-05 11:24:10	100
3027	LinkURL	0	0	10	\N	2006-03-26 15:27:31	100	Contains URL to a target	D	120	A Link should contain info on how to get to more information	Y	LinkURL	\N	\N	\N	\N	LinkURL	2006-04-05 11:27:02	100
3028	PubDate	0	0	16	\N	2006-03-26 15:27:31	100	Date on which this article will / should get published	D	7	Date on which this article will / should get published	Y	Publication Date	\N	\N	\N	\N	Publication Date	2006-04-05 11:27:36	100
3030	FQDN	0	0	10	\N	2006-03-26 15:29:51	100	Fully Qualified Domain Name i.e. www.comdivision.com	D	120	This field contains the so called fully qualified domain name including host and domain, but not anything protocol specific like http or the document path.	Y	Fully Qualified Domain Name	\N	\N	\N	\N	Fully Qualified Domain Name	2006-04-05 10:10:42	100
3039	IsModified	0	0	20	\N	2006-04-17 16:05:43	100	The record is modified	D	1	Indication that the record is modified	Y	Modified	\N	\N	\N	\N	Modified	2006-04-17 16:09:35	100
3047	Meta_Language	0	0	10	\N	2006-04-20 14:44:24	100	Language HTML Meta Tag	D	2	\N	Y	Meta Language	\N	\N	\N	\N	Meta Language	2006-04-20 14:45:34	100
3058	IsDeployed	0	0	20	\N	2006-06-11 16:50:21	100	Entity is deployed	D	1	\N	Y	Deployed	\N	\N	\N	\N	Deployed	2006-06-11 17:09:36	100
3059	LastSynchronized	0	0	16	\N	2006-06-11 16:50:21	100	Date when last synchronised	D	7	\N	Y	Last Synchronised	\N	\N	\N	\N	Last Synchronised	2016-04-11 16:07:00	0
3062	LogType	0	0	17	390	2006-06-11 17:13:58	100	Web Log Type	D	1	\N	Y	Log Type	\N	\N	\N	\N	Log Type	2006-06-11 17:16:23	100
3063	RequestType	0	0	10	\N	2006-06-11 17:13:58	100	\N	D	4	\N	Y	Request Type	\N	\N	\N	\N	Request Type	2006-06-11 17:19:13	100
3064	Hyphen	0	0	10	\N	2006-06-11 17:13:59	100	\N	D	20	\N	Y	Hyphen	\N	\N	\N	\N	Hyphen	2006-06-11 17:15:50	100
3065	Protocol	0	0	10	\N	2006-06-11 17:13:59	100	Protocol	D	20	\N	Y	Protocol	\N	\N	\N	\N	Protocol	2006-06-11 17:18:43	100
3067	FileSize	0	0	22	\N	2006-06-11 17:13:59	100	Size of the File in bytes	D	22	\N	Y	File Size	\N	\N	\N	\N	File Size	2006-06-11 17:15:22	100
3071	K_IndexLog_ID	0	0	13	\N	2006-06-24 12:15:50	100	Text search log	D	10	\N	Y	Index Log	\N	\N	\N	\N	Log	2006-06-24 12:52:22	100
3072	IndexQuery	0	0	10	\N	2006-06-24 12:15:50	100	Text Search Query 	D	255	Text search query entered	Y	Index Query	\N	\N	\N	\N	Query	2006-06-24 12:50:16	100
3073	IndexQueryResult	0	0	11	\N	2006-06-24 12:15:50	100	Result of the text query	D	10	\N	Y	Query Result	\N	\N	\N	\N	Result	2006-06-24 12:51:24	100
3074	QuerySource	0	0	17	391	2006-06-24 12:15:50	100	Source of the Query	D	1	\N	Y	Query Source	\N	\N	\N	\N	Source	2006-06-24 12:53:11	100
3075	K_INDEX_ID	0	0	13	\N	2006-06-24 12:16:36	100	Text Search Index	D	10	Text search index keyword and excerpt across documents	Y	Index	\N	\N	\N	\N	Index	2006-06-24 12:25:32	100
3076	Excerpt	0	0	14	\N	2006-06-24 12:16:36	100	Surrounding text of the keyword	D	2000	A passage or segment taken from a document,	Y	Excerpt	\N	\N	\N	\N	Excerpt	2006-06-24 12:21:05	100
3077	SourceUpdated	0	0	16	\N	2006-06-24 12:16:36	100	Date the source document was updated	D	7	\N	Y	Source Updated	\N	\N	\N	\N	Source Updated	2006-06-24 12:26:47	100
3078	K_IndexStop_ID	0	0	13	\N	2006-06-24 12:17:20	100	Keyword not to be indexed	D	10	Keyword not to be indexed, optional restricted to specific Document Type, Container or Request Type	Y	Index Stop	\N	\N	\N	\N	Index Stop	2010-01-13 10:38:15	100
3080	ContentText	0	0	36	\N	2006-08-06 21:18:02	100	\N	D	0	\N	Y	Content	\N	\N	\N	\N	Content	2006-08-06 21:18:02	100
3081	DirectDeploy	0	0	28	\N	2006-08-06 21:21:33	100	\N	D	1	\N	Y	Direct Deploy	\N	\N	\N	\N	Direct Deploy	2006-08-06 21:21:33	100
3097	ModerationType	0	0	17	395	2006-10-30 00:00:00	0	Type of moderation	D	1	\N	Y	Moderation Type	\N	\N	\N	\N	Moderation Type	2006-10-30 00:00:00	0
3100	ChatEntryType	0	0	17	398	2006-10-30 00:00:00	0	Type of Chat/Forum Entry	D	1	\N	Y	Chat Entry Type	\N	\N	\N	\N	Chat Entry Type	2006-10-30 00:00:00	0
3101	ModeratorStatus	0	0	17	396	2006-10-30 00:00:00	0	Status of Moderation	D	1	\N	Y	Moderation Status	\N	\N	\N	\N	Moderation Status	2006-10-30 00:00:00	0
3103	TokenType	0	0	17	397	2006-10-30 00:00:00	0	Wiki Token Type	D	1	\N	Y	TokenType	\N	\N	\N	\N	Token Type	2006-10-30 00:00:00	0
3104	Macro	0	0	14	\N	2006-10-30 00:00:00	0	Macro	D	2000	\N	Y	Macro	\N	\N	\N	\N	Macro	2006-10-30 00:00:00	0
50045	Allow_Info_Account	0	0	20	\N	2007-02-28 02:23:55	100	\N	D	1	\N	Y	Allow Info Account	\N	\N	\N	\N	Allow Info Account	2007-02-28 02:24:32	100
50046	Allow_Info_Asset	0	0	20	\N	2007-02-28 02:23:55	100	\N	D	1	\N	Y	Allow Info Asset	\N	\N	\N	\N	Allow Info Asset	2007-02-28 02:27:15	100
50047	Allow_Info_BPartner	0	0	20	\N	2007-02-28 02:23:55	100	\N	D	1	\N	Y	Allow Info BPartner	\N	\N	\N	\N	Allow Info BPartner	2007-02-28 02:29:51	100
50048	Allow_Info_CashJournal	0	0	20	\N	2007-02-28 02:23:56	100	\N	D	1	\N	Y	Allow Info CashJournal	\N	\N	\N	\N	Allow Info CashJournal	2007-02-28 02:30:56	100
50049	Allow_Info_InOut	0	0	20	\N	2007-02-28 02:23:56	100	\N	D	1	\N	Y	Allow Info InOut	\N	\N	\N	\N	Allow Info InOut	2007-02-28 02:31:13	100
50050	Allow_Info_Invoice	0	0	20	\N	2007-02-28 02:23:56	100	\N	D	1	\N	Y	Allow Info Invoice	\N	\N	\N	\N	Allow Info Invoice	2007-02-28 02:31:30	100
50051	Allow_Info_Order	0	0	20	\N	2007-02-28 02:23:56	100	\N	D	1	\N	Y	Allow Info Order	\N	\N	\N	\N	Allow Info Order	2007-02-28 02:31:43	100
50052	Allow_Info_Payment	0	0	20	\N	2007-02-28 02:23:56	100	\N	D	1	\N	Y	Allow Info Payment	\N	\N	\N	\N	Allow Info Payment	2007-02-28 02:32:00	100
50053	Allow_Info_Product	0	0	20	\N	2007-02-28 02:23:56	100	\N	D	1	\N	Y	Allow Info Product	\N	\N	\N	\N	Allow Info Product	2007-02-28 02:32:15	100
50054	Allow_Info_Resource	0	0	20	\N	2007-02-28 02:23:56	100	\N	D	1	\N	Y	Allow Info Resource	\N	\N	\N	\N	Allow Info Resource	2007-02-28 02:32:29	100
50055	Allow_Info_Schedule	0	0	20	\N	2007-02-28 02:23:56	100	\N	D	1	\N	Y	Allow Info Schedule	\N	\N	\N	\N	Allow Info Schedule	2007-02-28 02:32:46	100
50056	p_InboxFolder	0	0	\N	\N	2007-02-28 00:00:00	0	\N	D	\N	\N	Y	Inbox Folder	\N	\N	\N	\N	Inbox Folder	2007-02-28 00:00:00	0
50057	p_DefaultConfidentiality	0	0	\N	\N	2007-02-28 00:00:00	0	Type of Confidentiality	D	\N	\N	Y	Confidentiality	\N	\N	\N	\N	Confidentiality	2007-02-28 00:00:00	0
50058	p_IMAPUser	0	0	\N	\N	2007-02-28 00:00:00	0	\N	D	\N	\N	Y	IMAP User	\N	\N	\N	\N	IMAP User	2007-02-28 00:00:00	0
50059	p_IMAPPwd	0	0	\N	\N	2007-02-28 00:00:00	0	\N	D	\N	\N	Y	IMAP Password	\N	\N	\N	\N	IMAP Password	2007-02-28 00:00:00	0
50060	p_IMAPHost	0	0	\N	\N	2007-02-28 00:00:00	0	\N	D	\N	\N	Y	IMAP Host	\N	\N	\N	\N	IMAP Host	2007-02-28 00:00:00	0
50061	p_DefaultPriority	0	0	\N	\N	2007-02-28 00:00:00	0	Priority of the issue for the User	D	\N	\N	Y	User Importance	\N	\N	\N	\N	User Importance	2007-02-28 00:00:00	0
50062	p_RequestFolder	0	0	\N	\N	2007-02-28 00:00:00	0	\N	D	\N	\N	Y	Request Folder	\N	\N	\N	\N	Request Folder	2007-02-28 00:00:00	0
50063	p_ErrorFolder	0	0	\N	\N	2007-02-28 00:00:00	0	\N	D	\N	\N	Y	Error Folder	\N	\N	\N	\N	Error Folder	2007-02-28 00:00:00	0
52024	UserDiscount	0	0	22	\N	2008-03-26 13:20:01.063	0	\N	D	22	\N	Y	UserDiscount	\N	\N	\N	\N	UserDiscount	2008-03-26 13:20:01.063	0
52051	IsDiscountUptoLimitPrice	0	0	20	\N	2008-12-21 03:58:43.39285	100	\N	D	1	\N	Y	IsDiscountUptoLimitPrice	\N	\N	\N	\N	IsDiscountUptoLimitPrice	2008-12-21 03:58:43.39285	100
52052	IsDiscountAllowedOnTotal	0	0	20	\N	2008-12-21 03:58:43.394049	100	\N	D	1	\N	Y	IsDiscountAllowedOnTotal	\N	\N	\N	\N	IsDiscountAllowedOnTotal	2008-12-21 03:58:43.394049	100
53317	T_MRP_CRP_ID	0	0	13	\N	2007-12-17 08:45:21	0	\N	D	10	\N	Y	Temporal MRP & CRP	\N	\N	\N	\N	Temporal MRP & CRP	2008-08-05 12:13:10	0
53319	T_BOMLine_ID	0	0	13	\N	2007-12-17 08:45:47	0	\N	D	10	\N	Y	Temporal BOM Line	\N	\N	\N	\N	Temporal BOM Line	2008-08-05 12:13:25	0
53468	Allow_Info_MRP	0	0	20	\N	2008-05-17 20:46:13	0	\N	D	1	\N	Y	Allow Info MRP	\N	\N	\N	\N	Allow Info MRP	2008-05-17 20:46:13	0
53469	Allow_Info_CRP	0	0	20	\N	2008-05-17 20:46:44	0	\N	D	1	\N	Y	Allow Info CRP	\N	\N	\N	\N	Allow Info CRP	2008-05-17 20:46:44	0
53502	I_FAJournal_ID	0	0	11	\N	2008-05-30 16:37:07	100	\N	D	22	\N	Y	FA Journal	\N	\N	\N	\N	FA Journal	2010-03-29 15:58:53	100
53503	CurrencyRateType	0	0	17	111	2008-05-30 16:37:26	100	\N	D	1	\N	Y	CurrencyRateType	\N	\N	\N	\N	CurrencyRateType	2008-05-30 16:37:26	100
53760	QtyGrossReq	0	0	\N	\N	2009-01-21 19:40:48	100	\N	D	\N	\N	Y	Gross Requirements Quantity	\N	\N	\N	\N	Gross Req. Qty	2009-01-21 19:40:48	100
53761	QtyScheduledReceipts	0	0	\N	\N	2009-01-21 19:43:09	100	\N	D	\N	\N	Y	Scheduled Receipts Quantity	\N	\N	\N	\N	Scheduled Receipts Qty	2009-01-21 19:43:09	100
53762	QtyOnHandProjected	0	0	\N	\N	2009-01-21 19:47:08	100	On Hand Projected Quantity	D	\N	The On Hand Projected Quantity indicates the quantity of a product that is on hand in time line.	Y	On Hand Projected Quantity	\N	\N	\N	\N	On Hand Projected Qty	2009-01-21 19:47:08	100
53896	Included_Role_ID	0	0	18	53317	2009-07-27 19:47:34	0	\N	D	10	\N	Y	Included Role	\N	\N	\N	\N	Included Role	2009-07-27 19:47:34	0
54079	Manufacturer_ID	0	0	\N	\N	2009-11-29 00:49:37	100	\N	D	\N	\N	N	Manufacturer	\N	\N	\N	\N	Manufacturer	2010-03-24 16:19:25	100
54266	AmtApprovalAccum	0	0	12	\N	2010-08-16 00:00:00	100	The approval amount limit for this role accumulated on a period	D	22	The Approval Amount field indicates the amount limit this Role has for approval of documents within a period limit.	Y	Approval Amount Accumulated	\N	\N	\N	\N	Approval Amt Accumulated	2010-08-16 00:00:00	100
54267	DaysApprovalAccum	0	0	11	\N	2010-08-16 00:00:00	100	The days approval indicates the days to take into account to verify the accumulated approval amount.	D	10	The Days Approval Accumulated field indicates the days to take into account to verify the accumulated approval amount.	Y	Days Approval Accumulated	\N	\N	\N	\N	Days Approval Accumulated	2010-08-16 00:00:00	100
55254	Org_Phone	0	0	10	\N	2011-08-09 12:25:06	100	\N	D	40	\N	Y	Organisation Phone	\N	\N	\N	\N	Organisation Phone	2011-08-09 12:25:06	100
55255	Org_DUNS	0	0	10	\N	2011-08-09 12:33:14	100	\N	D	11	\N	Y	Organisation ABN/ACN	\N	\N	\N	\N	Organisation ABN/ACN	2016-04-11 16:00:50	0
55256	BankAccountDescription	0	0	10	\N	2011-08-10 16:28:38	100	\N	D	255	\N	Y	Bank Account Description	\N	\N	\N	\N	Bank Account Description	2011-08-10 16:28:38	100
55263	Ship_BPartner_ID	0	0	18	138	2011-08-24 16:47:06	100	Business Partner to ship to	D	10	This is the drop ship business partner for drop ship orders, otherwise the order/shipment business partner.	Y	Ship To Business Partner	\N	\N	\N	\N	Ship To Business Partner	2011-09-05 15:31:12	100
55265	Ship_User_id	0	0	18	110	2011-08-24 16:47:12	100	Contact of Ship To party	D	10	\N	Y	Ship To Contact	\N	\N	\N	\N	Ship To Contact	2011-09-05 15:31:14	100
55266	Ship_BPValue	0	0	10	\N	2011-08-24 16:47:16	100	Search Key of the Ship To Business Partner	D	40	\N	Y	Ship To BP Search Key	\N	\N	\N	\N	Ship To BP Search Key	2011-09-05 15:31:12	100
55267	Ship_BPTaxID	0	0	10	\N	2011-08-24 16:47:19	100	Tax ID of the Ship to Business Partner.	D	20	\N	Y	Ship To BP Tax ID	\N	\N	\N	\N	Ship To BP Tax ID	2011-09-05 15:31:12	100
55268	Ship_Name	0	0	10	\N	2011-08-24 16:47:21	100	Name of the Ship To Business Partner	D	60	\N	Y	Ship To BP Name	\N	\N	\N	\N	Ship To BP Name	2011-09-05 15:31:13	100
55269	Ship_Name2	0	0	10	\N	2011-08-24 16:47:24	100	Additional Name of Ship To Business Partner	D	60	\N	Y	Ship To BP Name 2	\N	\N	\N	\N	Ship To BP Name 2	2011-09-05 15:31:13	100
55270	Ship_Title	0	0	10	\N	2011-08-24 16:47:30	100	Title of Ship To Business Partner Contact	D	40	\N	Y	Ship To Contact Title	\N	\N	\N	\N	Ship To Contact Title	2011-09-05 15:31:14	100
55271	Ship_Phone	0	0	10	\N	2011-08-24 16:47:33	100	Phone number of Ship To contact	D	40	\N	Y	Ship To Contact Phone	\N	\N	\N	\N	Ship To Contact Phone	2011-09-05 15:31:14	100
55272	Ship_ContactName	0	0	10	\N	2011-08-24 16:47:36	100	Name of Ship To contact	D	256	\N	Y	Ship To Contact Name	\N	\N	\N	\N	Ship To Contact Name	2011-09-05 15:31:13	100
55291	C_SalesDashboard_ID	0	0	\N	\N	2011-09-02 12:26:28	100	\N	D	\N	\N	Y	Sales Dashboard	\N	\N	\N	\N	Sales Dashboard	2011-09-02 12:26:28	100
55358	TaxBaseAcctAmt	0	0	12	\N	2011-10-31 15:01:48	100	Tax Base Amount in accounting currency	D	10	\N	Y	Tax Base Accounted Amount	\N	\N	\N	\N	Tax Base Accounted Amount	2016-04-11 16:07:19	0
55359	TaxAcctAmt	0	0	12	\N	2011-10-31 15:01:51	100	Tax amount in the accounting currency.	D	10	\N	Y	Tax Accounted Amount	\N	\N	\N	\N	Tax Accounted Amount	2016-04-11 16:07:19	0
55360	TaxLineAcctTotal	0	0	12	\N	2011-10-31 15:01:54	100	Tax Line Total in accounting currency	D	22	\N	Y	Tax Line Accounted Total	\N	\N	\N	\N	Tax Line Accounted Total	2016-04-11 16:07:20	0
55361	AcctCurrency_ID	0	0	18	112	2011-10-31 15:01:58	100	\N	D	10	\N	Y	Accounting Currency	\N	\N	\N	\N	Accounting Currency	2016-04-11 16:07:21	0
55362	ARAP	0	0	10	\N	2011-10-31 15:02:01	100	Receivable/Payable	D	22	\N	Y	AR/AP	\N	\N	\N	\N	AR/AP	2016-04-11 16:07:19	0
55370	Amt_0	0	0	12	\N	2012-01-12 15:51:40	100	\N	D	22	\N	Y	Amount 0	\N	\N	\N	\N	Amount 0	2012-01-12 15:51:40	100
55371	Amt_1	0	0	12	\N	2012-01-12 15:52:18	100	\N	D	22	\N	Y	Amount 1	\N	\N	\N	\N	Amount 1	2012-01-12 15:52:18	100
55372	Amt_2	0	0	12	\N	2012-01-12 15:52:30	100	\N	D	22	\N	Y	Amount 2	\N	\N	\N	\N	Amount 2	2012-01-12 15:52:30	100
55373	Amt_3	0	0	12	\N	2012-01-12 15:52:41	100	\N	D	22	\N	Y	Amount 3	\N	\N	\N	\N	Amount 3	2012-01-12 15:52:41	100
55374	Amt_4	0	0	12	\N	2012-01-12 15:53:12	100	\N	D	22	\N	Y	Amount 4	\N	\N	\N	\N	Amount 4	2012-01-12 15:53:12	100
55375	Text_0	0	0	10	\N	2012-01-12 15:53:30	100	\N	D	255	\N	Y	Text 0	\N	\N	\N	\N	Text 0	2012-01-12 15:53:30	100
55376	Text_1	0	0	10	\N	2012-01-12 15:53:41	100	\N	D	255	\N	Y	Text 1	\N	\N	\N	\N	Text 1	2012-01-12 15:53:41	100
55377	Text_2	0	0	10	\N	2012-01-12 15:54:13	100	\N	D	255	\N	Y	Text 2	\N	\N	\N	\N	Text 2	2012-01-12 15:54:13	100
55378	Text_3	0	0	10	\N	2012-01-12 15:54:27	100	\N	D	255	\N	Y	Text 3	\N	\N	\N	\N	Text 3	2012-01-12 15:54:27	100
55379	Text_4	0	0	10	\N	2012-01-12 15:54:38	100	\N	D	255	\N	Y	Text 4	\N	\N	\N	\N	Text 4	2012-01-12 15:54:38	100
55380	Date_0	0	0	15	\N	2012-01-12 15:55:19	100	\N	D	7	\N	Y	Date 0	\N	\N	\N	\N	Date 0	2012-01-12 15:55:19	100
55381	Qty_0	0	0	29	\N	2012-01-12 15:55:38	100	\N	D	22	\N	Y	Quantity 0	\N	\N	\N	\N	Quantity 0	2012-06-08 15:17:05	0
55382	Qty_1	0	0	29	\N	2012-01-12 15:56:16	100	\N	D	22	\N	Y	Quantity 1	\N	\N	\N	\N	Quantity 1	2012-01-12 15:56:16	100
55383	Date_1	0	0	15	\N	2012-01-12 15:56:43	100	\N	D	7	\N	Y	Date 1	\N	\N	\N	\N	Date 1	2012-01-12 15:56:43	100
55458	I_FreightRegion_ID	0	0	13	\N	2012-03-26 15:20:18	100	\N	D	22	\N	Y	Import Freight Region	\N	\N	\N	\N	Import Freight Region	2012-03-26 15:20:18	100
55459	ShipperName	0	0	10	\N	2012-03-26 15:21:04	100	\N	D	60	\N	Y	ShipperName	\N	\N	\N	\N	ShipperName	2012-03-26 15:21:04	100
57373	T_MiniMRP_ID	0	0	\N	\N	2014-08-01 14:33:49	100	\N	D	\N	\N	Y	T_MiniMRP ID	\N	\N	\N	\N	T_MiniMRP ID	2014-08-01 14:33:49	100
57402	AD_Tree_Favorite_ID	0	0	\N	\N	2014-08-05 16:00:22	100	\N	D	\N	\N	Y	Tree Favorite ID	\N	\N	\N	\N	Tree Favorite ID	2014-08-05 16:00:22	100
57403	AD_Tree_Favorite_Node_ID	0	0	\N	\N	2014-08-05 16:09:13	100	\N	D	\N	\N	Y	Tree Favorite Node ID	\N	\N	\N	\N	Tree Favorite Node ID	2014-08-05 16:09:13	100
57404	NodeName	0	0	\N	\N	2014-08-05 16:12:34	100	\N	D	\N	\N	Y	Node Name	\N	\N	\N	\N	Node Name	2014-08-05 16:12:34	100
57405	Tax_Rate_Override	0	0	\N	\N	2014-08-06 13:47:04	100	\N	D	\N	\N	Y	Tax Rate Override	\N	\N	\N	\N	Tax Rate Override	2016-04-11 16:41:04	0
57480	UnPaidAmt	0	0	12	\N	2014-10-06 12:34:19	100	\N	D	22	\N	Y	UnPaid Amount	\N	\N	\N	\N	Unpaid	2014-10-06 12:34:19	100
57550	AD_Tab_Customization_ID	0	0	\N	\N	2014-11-17 10:17:57	100	\N	D	\N	\N	Y	AD_Tab_Customization_ID	\N	\N	\N	\N	AD_Tab_Customization_ID	2014-11-17 10:17:57	100
57551	custom	0	0	\N	\N	2014-11-17 10:21:09	100	\N	D	\N	\N	Y	custom	\N	\N	\N	\N	custom	2014-11-17 10:21:09	100
57829	IsShowInList	0	0	\N	\N	2015-02-03 09:30:08	100	\N	D	\N	\N	Y	Show In List	\N	\N	\N	\N	Show In List	2015-02-03 09:30:08	100
57913	T_Replenish_PO_ID	0	0	\N	\N	2015-03-23 13:09:06	100	\N	D	\N	\N	Y	Replenish PO	\N	\N	\N	\N	Replenish PO	2016-04-11 16:46:51	0
57914	Current_C_BPartner_ID	0	0	\N	\N	2015-03-23 13:09:26	100	\N	D	\N	\N	Y	Current_C_BPartner_ID	\N	\N	\N	\N	Current_C_BPartner_ID	2015-03-23 13:09:26	100
57915	POPrice	0	0	\N	\N	2015-03-23 13:09:35	100	\N	D	\N	\N	Y	POPrice	\N	\N	\N	\N	PO Price	2015-03-23 13:09:35	100
57916	Remarks	0	0	\N	\N	2015-03-23 13:09:39	100	\N	D	\N	\N	Y	Remarks	\N	\N	\N	\N	Remarks	2015-03-23 13:09:39	100
57917	POQty	0	0	\N	\N	2015-03-23 13:09:49	100	\N	D	\N	\N	Y	POQty	\N	\N	\N	\N	PO Qty	2015-03-23 13:09:49	100
57961	bson_gradelevel_ID	0	0	\N	\N	2015-04-01 12:28:47	100	\N	D	\N	\N	Y	bson_gradelevel ID	\N	\N	\N	\N	bson_gradelevel ID	2015-04-01 12:28:47	100
58005	IsGoodtoShip	0	0	\N	\N	2015-05-26 18:33:44	100	\N	D	\N	\N	Y	Good to Ship	\N	\N	\N	\N	Good to Ship	2016-04-11 16:49:46	0
58111	RV_Fact_Acct_Proj_ID	0	0	\N	\N	2015-08-03 12:35:37	100	\N	D	\N	\N	Y	Project Accounting Fact ID	\N	\N	\N	\N	Project Accounting Fact ID	2015-08-03 12:35:37	100
58260	I_Product_BOM_ID	0	0	\N	\N	2015-08-13 21:49:51	100	\N	D	\N	\N	Y	Product BOM Import ID	\N	\N	\N	\N	Product BOM Import ID	2015-08-13 21:49:51	100
58261	I_AttributeSetInstance_ID	0	0	\N	\N	2015-08-13 23:26:29	100	\N	D	\N	\N	Y	Attribute Set Instance ID	\N	\N	\N	\N	Attribute Set Instance ID	2015-08-13 23:26:29	100
58322	IsOverdue	0	0	\N	\N	2015-09-23 12:18:26	100	\N	D	\N	\N	Y	IsOverdue	\N	\N	\N	\N	IsOverdue	2016-04-11 16:52:25	0
58323	DunningAgeGroup	0	0	\N	\N	2015-09-23 12:18:29	100	\N	D	\N	\N	Y	Dunning Age Group	\N	\N	\N	\N	Dunning Age Group	2016-04-11 16:52:25	0
58385	IsCanSaveColumnWidthEveryone	0	0	\N	\N	2016-03-16 10:44:28	100	Can save column width on included tab or quick entry form for everyone.	D	0	\N	Y	Can Save Column Width to Everyone	\N	\N	\N	\N	Can Save Column Width to Everyone	2016-03-16 10:44:28	100
58386	Allow_XLS_View	0	0	20	\N	2016-03-17 15:30:41	100	\N	D	1	\N	Y	Allow XLS View	\N	\N	\N	\N	Allow XLS View	2016-03-17 15:30:41	100
58387	Allow_HTML_View	0	0	20	\N	2016-03-17 15:31:14	100	\N	D	1	\N	Y	Allow HTML View	\N	\N	\N	\N	Allow HTML View	2016-03-17 15:31:14	100
58405	M_PBatch_Line_ID	0	0	\N	\N	2016-04-12 14:47:16	100	\N	D	\N	\N	Y	Production Batch Line ID	\N	\N	\N	\N	Production Batch Line ID	2016-04-12 14:47:16	100
58774	T_MRP_Run_Capacity_ID	0	0	\N	\N	2016-05-16 10:42:50	100	\N	D	\N	\N	Y	T_MRP_Run_Capacity ID	\N	\N	\N	\N	T_MRP_Run_Capacity ID	2016-07-01 00:51:58	0
58775	TotalCapacity	0	0	29	\N	2016-05-16 12:24:24	100	Total Work Centre capacity	D	\N	\N	Y	Total WC Capacity	\N	\N	\N	\N	Total WC Capacity	2016-07-01 00:51:58	0
58776	TotalProduction	0	0	29	\N	2016-05-16 12:24:26	100	\N	D	\N	\N	Y	Total Production in Queue	\N	\N	\N	\N	Total Production in Queue	2016-07-01 00:51:58	0
58777	TotalMPO	0	0	29	\N	2016-05-16 12:24:28	100	\N	D	\N	\N	Y	Total Planned Mfg Order	\N	\N	\N	\N	Total Planned Mfg Order	2016-07-01 00:51:58	0
58778	TotalMOP	0	0	29	\N	2016-05-16 12:24:31	100	\N	D	\N	\N	Y	Total Mfg Order	\N	\N	\N	\N	Total Mfg Order	2016-07-01 00:51:58	0
58942	oi	0	0	\N	\N	2016-06-17 15:43:44	100	\N	D	\N	\N	Y	oi	\N	\N	\N	\N	oi	2016-06-17 15:43:44	100
59003	IsRefreshGridTimer	0	0	20	\N	2016-09-02 15:03:03	100	Configure specific timer interval to refresh the Quick form data	D	1	\N	Y	Refresh grid data by Timer	\N	\N	\N	\N	Refresh the grid data by Timer interval	2016-09-02 15:03:03	100
59004	RefreshInterval	0	0	11	\N	2016-09-02 15:12:34	100	Set the interval in second to hit refresh event for the quick grid	D	10	\N	Y	Refresh Timer Interval	\N	\N	\N	\N	Refresh Timer Interval [Sec] 	2016-09-02 15:12:34	100
59265	CumulatedCapacity	0	0	29	\N	2017-03-15 16:13:16	100	\N	D	0	\N	Y	Accumulated Capacity	\N	\N	\N	\N	Accumulated Capacity	2017-03-15 16:13:16	100
59333	T_Product_BOMLine_ID	0	0	\N	\N	2017-04-17 19:26:53	100	\N	D	\N	\N	Y	Product BOMLine Temporary ID	\N	\N	\N	\N	Product BOMLine Temporary ID	2017-04-17 19:26:53	100
59334	QtyRequired	0	0	\N	\N	2017-04-17 19:29:35	100	\N	D	0	\N	Y	Qty Required	\N	\N	\N	\N	Qty Required	2017-04-17 19:29:35	100
59346	T_MRP_RUN_Capacity_Summary_ID	0	0	\N	\N	2017-04-20 16:56:35	100	\N	D	\N	\N	Y	MRP RUN Capacity Summary ID	\N	\N	\N	\N	MRP RUN Capacity Summary ID	2017-04-20 16:56:35	100
59347	Week	0	0	10	\N	2017-04-20 17:11:56	100	\N	D	10	\N	Y	Week	\N	\N	\N	\N	Week	2017-04-20 17:11:56	100
59348	SuggestedQty	0	0	29	\N	2017-04-20 17:13:01	100	\N	D	22	\N	Y	Suggested Qty	\N	\N	\N	\N	Suggested Qty	2017-04-20 17:13:01	100
59349	TotalDemandQty	0	0	29	\N	2017-04-20 17:14:48	100	\N	D	22	\N	Y	Total DemandQty	\N	\N	\N	\N	Total Demand Qty	2017-04-20 17:14:48	100
59350	CapacityQty	0	0	29	\N	2017-04-20 17:17:51	100	\N	D	22	\N	Y	Capacity Qty	\N	\N	\N	\N	Capacity Qty	2017-04-20 17:17:51	100
59351	SurplusShortageQty	0	0	29	\N	2017-04-20 17:20:34	100	\N	D	22	\N	Y	Surplus Shortage Qty	\N	\N	\N	\N	Surplus Shortage Qty	2017-04-20 17:20:34	100
59352	CumulatedDemand	0	0	29	\N	2017-04-20 17:24:17	100	\N	D	22	\N	Y	Accumulated Demand	\N	\N	\N	\N	Accumulated Demand	2017-04-20 17:24:17	100
59353	RemainingCumulativeCapacity	0	0	29	\N	2017-04-20 17:26:58	100	\N	D	22	\N	Y	Remaining Cumulative Capacity	\N	\N	\N	\N	Remaining Cumulative Capacity	2017-04-20 17:26:58	100
60924	IsNewPageAfterRH	0	0	20	\N	2019-02-08 12:24:02	100	Print the Header section in new page after Report Header.	D	1	\N	Y	New Page after Report Header	\N	\N	\N	\N	New Page after Report Header	2020-10-19 22:59:45	100
60969	IncludedTabHeight	0	0	22	\N	2019-05-08 18:11:08	100	\N	D	22	\N	Y	Included Tab Height	\N	\N	\N	\N	Included Tab Height	2019-05-08 18:11:08	100
61144	ExpVar	0	0	10	\N	2019-08-16 14:48:42	100	Expression variable assigned to a column to be used for calculation (e.g. a, b)	D	10	Limit characters to minimal to easily read mathematical expression	Y	Exp Var	\N	\N	\N	\N	Exp Var	2020-03-16 13:17:13	0
61145	ExpEval	0	0	14	\N	2019-08-16 14:54:18	100	Value of Print Format Item expressed in mathematical expression (e.g.  $a + $b)	D	500	\N	Y	Exp Eval	\N	\N	\N	\N	Exp Eval	2020-03-16 13:17:13	0
61151	BGColorExpression	0	0	14	\N	2019-08-26 14:52:12	100	Applies simple or conditional background color formatting	D	500	Use to apply background color \nExample #1\n<br> blue\n<br>Example #2\n<br>#if ($value < 0) red\n<br>#else black \n<br>#end\n\n<br>value is a reserve word for the value for the column.	Y	BG Color Expression	\N	\N	\N	\N	BG Color Expression	2020-03-16 13:17:19	0
200000	AD_RecentItem_ID	0	0	13	\N	2012-01-27 15:19:57	100	\N	D	22	\N	Y	Recent Item	\N	\N	\N	\N	Recent Item	2012-01-27 15:19:57	100
200001	Fact_Reconciliation_ID	0	0	13	\N	2012-02-03 22:07:45	100	\N	D	10	\N	Y	Accounting Fact Reconciliation	\N	\N	\N	\N	Accounting Fact Reconciliation	2012-02-03 22:07:45	100
200098	ColumnSpan	0	0	\N	\N	2012-08-23 11:37:32	100	Number of column for a box of field	D	\N	\N	Y	Column Span	\N	\N	\N	\N	Column Span	2012-08-23 11:37:32	100
200099	NumLines	0	0	\N	\N	2012-08-23 11:39:13	100	Number of lines for a field	D	\N	Number of lines for a field	Y	Number of Lines	\N	\N	\N	\N	Number of Lines	2012-08-23 11:39:13	100
200187	IsToolbarButton	0	0	\N	\N	2012-10-19 14:04:01	100	Add the column button to the toolbar	D	\N	There IsToolbarButton check box indicates if this column button is part of the toolbar's process button popup list or render as field.	Y	Toolbar Button	\N	\N	\N	\N	Toolbar Button	2012-10-19 14:04:01	100
1000000	Count	0	0	\N	\N	2011-11-02 16:35:06	0	\N	D	\N	\N	Y	Count	\N	\N	\N	\N	Count	2011-11-02 16:35:06	0
1000001	Logo	0	0	\N	\N	2011-11-02 16:35:06	0	\N	D	\N	\N	Y	Logo	\N	\N	\N	\N	Logo	2011-11-02 16:35:06	0
1000002	HistoryYears	0	0	\N	\N	2011-11-02 16:35:06	0	Number of years prior to current year to create in calendar	D	\N	\N	Y	HistoryYears	\N	\N	\N	\N	History Years	2011-11-02 16:35:06	0
1000003	CharSetName	0	0	\N	\N	2012-06-08 15:19:40	0	\N	D	\N	\N	Y	CharSetName	\N	\N	\N	\N	Character Set	2012-06-08 15:19:40	0
1000004	CreateOpportunity	0	0	\N	\N	2012-06-08 15:19:40	0	Create Opportunity	D	\N	Create a new Sales Opportunity when converting a lead	Y	CreateOpportunity	\N	\N	\N	\N	Create Opportunity	2012-06-08 15:19:40	0
1000005	IsExcludeHeader	0	0	\N	\N	2012-06-08 15:19:40	0	Exclude the first row of the file (e.g. if it contains headers)	D	\N	\N	Y	IsExcludeHeader	\N	\N	\N	\N	Exclude first row	2012-06-08 15:19:40	0
1000007	BookListGradeLevel	0	0	\N	\N	2016-04-12 09:52:19	0	\N	D	\N	\N	Y	BookListGradeLevel	\N	\N	\N	\N	BookListGradeLevel	2016-04-12 09:52:19	0
1000008	EDIProcessor	0	0	\N	\N	2016-04-12 09:52:19	0	\N	D	\N	\t\t\t	Y	EDIProcessor	\N	\N	\N	\N	EDIProcessor	2016-04-12 09:52:19	0
1000009	days_back	0	0	\N	\N	2016-04-12 09:52:19	0	\N	D	\N	\N	Y	days_back	\N	\N	\N	\N	days_back	2016-04-12 09:52:19	0
1000010	FailOnError	0	0	\N	\N	2016-04-12 09:52:19	0	\N	D	\N	\N	Y	FailOnError	\N	\N	\N	\N	Fail on Error	2016-04-12 09:52:19	0
1000011	Directory	0	0	\N	\N	2016-04-12 09:52:19	0	\N	D	\N	\N	Y	Directory	\N	\N	\N	\N	Directory	2016-04-12 09:52:19	0
1000012	Substitute	0	0	\N	\N	2016-04-12 09:52:19	0	Substitute Daily Usage x Lead Time for Maximum	D	\N	\N	Y	Substitute	\N	\N	\N	\N	Substitute	2016-04-12 09:52:19	0
1000013	IsCopyToSystem	0	0	\N	\N	2016-04-12 09:52:19	0	\N	D	\N	\N	Y	IsCopyToSystem	\N	\N	\N	\N	Copy to System	2016-04-12 09:52:19	0
1000014	CurrencyFrom	0	0	\N	\N	2016-04-12 09:52:19	0	\N	D	\N	\N	Y	CurrencyFrom	\N	\N	\N	\N	Currency From	2016-04-12 09:52:19	0
1000015	MagentoPassword	0	0	\N	\N	2016-04-12 09:52:19	0	Magento API password	D	\N	API key as set in Magento	Y	MagentoPassword	\N	\N	\N	\N	Magento API Key	2016-04-12 09:52:19	0
1000016	SUBREPORT_DIR	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\N	Y	SUBREPORT_DIR	\N	\N	\N	\N	SUBREPORT_DIR	2016-04-12 09:52:20	0
1000017	MagentoUser	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\N	Y	MagentoUser	\N	\N	\N	\N	Magento User Name	2016-04-12 09:52:20	0
1000018	IncludeBalancing	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\N	Y	IncludeBalancing	\N	\N	\N	\N	Include Balancing	2016-04-12 09:52:20	0
1000019	DeleteRemoteFile	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\N	Y	DeleteRemoteFile	\N	\N	\N	\N	DeleteRemoteFile	2016-04-12 09:52:20	0
1000020	Date	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\N	Y	Date	\N	\N	\N	\N	Date	2016-04-12 09:52:20	0
1000021	AckProcess	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\N	Y	AckProcess	\N	\N	\N	\N	AckProcess	2016-04-12 09:52:20	0
1000022	ResetAve	0	0	\N	\N	2016-04-12 09:52:20	0	Recalculate Average, Max and Min	D	\N	\N	Y	ResetAve	\N	\N	\N	\N	ResetAve	2016-04-12 09:52:20	0
1000023	Discount_Charge_ID	0	0	\N	\N	2016-04-12 09:52:20	0	Charge to use for Discounts	D	\N	\N	Y	Discount_Charge_ID	\N	\N	\N	\N	Discount Charge	2016-04-12 09:52:20	0
1000024	ConnectionType	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\N	Y	ConnectionType	\N	\N	\N	\N	ConnectionType	2016-04-12 09:52:20	0
1000025	GenerateCodes	0	0	\N	\N	2016-04-12 09:52:20	0	Create new numbers before to print the label.	D	\N	\N	Y	GenerateCodes	\N	\N	\N	\N	GenerateCodes	2016-04-12 09:52:20	0
1000026	MagentoInstance	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\N	Y	MagentoInstance	\N	\N	\N	\N	Magento Instance	2016-04-12 09:52:20	0
1000027	MagentoURL	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\N	Y	MagentoURL	\N	\N	\N	\N	Magento URL	2016-04-12 09:52:20	0
1000029	ABCClass	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	Use to limit on Product Classification	Y	ABCClass	\N	\N	\N	\N	ABCClass	2016-04-12 09:52:20	0
1000030	UpdateExisting	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\N	Y	UpdateExisting	\N	\N	\N	\N	Update Existing	2016-04-12 09:52:20	0
1000031	calc_date	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\t\t\t\t	Y	calc_date	\N	\N	\N	\N	calc_date	2016-04-12 09:52:20	0
1000032	TaxRate	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\N	Y	TaxRate	\N	\N	\N	\N	Tax Rate	2016-04-12 09:52:20	0
1000033	PrintDocuments	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	Print Documents  created	Y	PrintDocuments	\N	\N	\N	\N	PrintDocuments	2016-04-12 09:52:20	0
1000034	Force_Print	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\N	Y	Force_Print	\N	\N	\N	\N	Force_Print	2016-04-12 09:52:20	0
1000035	C_SchoolBP_ID	0	0	\N	\N	2016-04-12 09:52:20	0	Identifies a School Business Partner	D	\N	A Business Partner is anyone with whom you transact.  This can include Vendor, Customer, Employee or Salesperson	Y	C_SchoolBP_ID	\N	\N	\N	\N	School BP 	2016-04-12 09:52:20	0
1000036	isShowRetainedEarnings	0	0	\N	\N	2016-04-12 09:52:20	0	Is Show Retained Earnings	D	\N	\N	Y	isShowRetainedEarnings	\N	\N	\N	\N	Is Show Retained Earnings	2016-04-12 09:52:20	0
1000037	ResetMin	0	0	\N	\N	2016-04-12 09:52:20	0	Reset Minimum based on Average Usage x Days Safety Stock	D	\N	\N	Y	ResetMin	\N	\N	\N	\N	ResetMin	2016-04-12 09:52:20	0
1000038	CreatePayments	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\N	Y	CreatePayments	\N	\N	\N	\N	Create Payments	2016-04-12 09:52:20	0
1000039	Freight_Charge_ID	0	0	\N	\N	2016-04-12 09:52:20	0	Charge to use for Freight	D	\N	\N	Y	Freight_Charge_ID	\N	\N	\N	\N	Freight Charge	2016-04-12 09:52:20	0
1000040	Adjust	0	0	\N	\N	2016-04-12 09:52:20	0	Adjust Replenish Max and Min for Seasonality Factor	D	\N	\N	Y	Adjust	\N	\N	\N	\N	Adjust	2016-04-12 09:52:20	0
1000041	LocalFolder	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\N	Y	LocalFolder	\N	\N	\N	\N	LocalFolder	2016-04-12 09:52:20	0
1000042	AmountFrom	0	0	\N	\N	2016-04-12 09:52:20	0	\N	D	\N	\N	Y	AmountFrom	\N	\N	\N	\N	Amount From	2016-04-12 09:52:20	0
1000043	Destination	0	0	\N	\N	2019-02-04 13:39:33	0	\N	D	\N	\N	Y	Destination	\N	\N	\N	\N	Destination Storage	2019-02-04 13:39:33	0
1000044	UpdateCountQty	0	0	\N	\N	2019-02-04 13:39:33	0	Update count quantity on inventory	D	\N	If selected, the count quantity will be set as well as the book quantity.	Y	UpdateCountQty	\N	\N	\N	\N	Update Count Qty	2019-02-04 13:39:33	0
1000045	Path	0	0	\N	\N	2019-02-04 13:39:33	0	\N	D	\N	\N	Y	Path	\N	\N	\N	\N	Destination Path	2019-02-04 13:39:33	0
\.
