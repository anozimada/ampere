copy "ad_sysconfig" ("ad_sysconfig_id", "name", "ad_client_id", "ad_org_id", "configurationlevel", "created", "createdby", "description", "entitytype", "isactive", "updated", "updatedby", "value") from STDIN;
50000	DICTIONARY_ID_WEBSITE	0	0	S	2007-10-13 00:00:00	100	Website where the system sequences are found (MSequence)	D	Y	2021-04-27 08:56:36	100	http://adempiere.io:7780/cgi-bin/get_ID
50001	DICTIONARY_ID_USER	0	0	S	2007-10-13 00:00:00	100	User (sourceforge developer) to get the system sequences (MSequence)	D	Y	2016-04-11 16:25:20	0	phib
50002	DICTIONARY_ID_PASSWORD	0	0	S	2007-10-13 00:00:00	100	Password to get the system sequences (MSequence)	D	Y	2016-04-11 16:25:20	0	zAcechu7
50003	DICTIONARY_ID_COMMENTS	0	0	S	2007-10-13 00:00:00	100	Comment to reserve the system sequences (MSequence)	D	Y	2008-12-08 21:36:58	100	I forgot to set the DICTIONARY_ID_COMMENTS System Configurator
50004	PROJECT_ID_WEBSITE	0	0	S	2007-10-13 00:00:00	100	Website where the system sequences are found (MSequence)	D	Y	2007-12-15 12:49:42	100	http://developer.adempiere.com/cgi-bin/get_ID
50005	PROJECT_ID_PROJECT	0	0	S	2007-10-13 00:00:00	100	Name of the project you are working on for development	D	Y	2007-12-15 12:49:16	100	TestProject
50006	PROJECT_ID_USER	0	0	S	2007-10-13 00:00:00	100	User (sourceforge developer) to get the system sequences (MSequence)	D	Y	2007-12-15 12:49:49	100	globalqss
50007	PROJECT_ID_PASSWORD	0	0	S	2007-10-13 00:00:00	100	Password to get the system sequences (MSequence)	D	Y	2007-12-15 12:50:06	100	password_inseguro
50008	PROJECT_ID_COMMENTS	0	0	S	2007-10-13 00:00:00	100	Comment to reserve the system sequences (MSequence)	D	Y	2007-12-15 12:50:29	100	Default comment for updating dictionary
50009	DICTIONARY_ID_USE_CENTRALIZED_ID	0	0	S	2007-10-13 00:00:00	100	Flag to indicate if we use centralized ID approach or not	D	Y	2022-03-02 13:57:04	100	N
50010	PROJECT_ID_USE_CENTRALIZED_ID	0	0	S	2007-10-13 00:00:00	100	Flag to indicate if we use centralized ID approach or not	D	Y	2007-12-15 12:49:32	100	N
50011	PAYMENT_OVERWRITE_DOCUMENTNO_WITH_CHECK_ON_PAYMENT	0	0	C	2008-01-21 23:13:07	100	Y/N - Define if the payment document number must be overwritten with the check number	D	Y	2008-01-21 23:13:07	100	Y
50012	PAYMENT_OVERWRITE_DOCUMENTNO_WITH_CREDIT_CARD	0	0	C	2008-01-21 23:13:30	100	Y/N - Define if the payment document number must be overwritten with the credit card	D	Y	2008-01-21 23:13:30	100	Y
50013	PAYMENT_OVERWRITE_DOCUMENTNO_WITH_CHECK_ON_RECEIPT	0	0	C	2008-01-21 23:19:48	100	Y/N - Define if the payment (receipt) document number must be overwritten with the check number	D	Y	2008-01-21 23:19:48	100	Y
50014	ALERT_SEND_ATTACHMENT_AS_XLS	0	0	C	2008-02-15 00:00:00	0	Send alert results as Excel attachments	D	Y	2008-02-15 00:00:00	0	Y
50015	SYSTEM_INSERT_CHANGELOG	0	0	C	2008-03-19 19:17:05	100	Keep change log for inserts: Y - Yes, N - No, K - just the key _ID	D	Y	2008-03-19 19:17:05	100	K
50016	SYSTEM_NATIVE_SEQUENCE	0	0	S	2008-09-08 22:12:21	0	Y - Yes Use Native Sequence , N - No Use Adempiere Sequence (AD_Sequence)	D	Y	2008-09-08 22:12:21	0	N
50017	START_VALUE_BPLOCATION_NAME	0	0	O	2009-02-09 12:43:11	100	Define the start value for C_BPartner_Location.Name (possible values 0 to 4) - complete definition here http://adempiere.com/wiki/index.php/ManPageW_SystemConfigurator	D	Y	2009-02-09 12:43:11	100	0
50018	ZK_PAGING_SIZE	0	0	S	2009-03-17 00:16:11	100	Default paging size for grid view in zk webui	D	Y	2009-03-17 00:16:11	100	100
50019	ZK_GRID_EDIT_MODELESS	0	0	S	2009-03-17 00:19:37	100	Grid view will enter in edit mode	D	Y	2009-11-24 16:20:31	100	N
50020	ZK_DASHBOARD_REFRESH_INTERVAL	0	0	S	2009-03-17 00:20:03	100	Milliseconds of wait to run the dashboard refresh on zk webui client	D	Y	2009-03-17 00:20:03	100	60000
50021	ZK_DESKTOP_CLASS	0	0	S	2009-03-17 00:21:14	100	package+classname of zk desktop class	D	Y	2009-03-17 00:21:14	100	org.adempiere.webui.desktop.DefaultDesktop
50022	ProductUOMConversionUOMValidate	0	0	S	2009-03-17 00:23:20	100	Enable/disable validation -> Select the Product UoM as the From Unit of Measure	D	Y	2009-03-17 00:23:20	100	Y
50023	ProductUOMConversionRateValidate	0	0	S	2009-03-17 00:23:43	100	Enable/disable validation -> The Product UoM needs to be the smallest UoM - Multiplier must be > 0	D	Y	2009-03-17 00:23:43	100	Y
50024	MENU_INFOUPDATER_SLEEP_MS	0	0	S	2009-03-17 00:24:37	100	Milliseconds of wait to run the infoupdater class on the menu window	D	Y	2009-03-17 00:24:37	100	60000
50025	WEBUI_LOGOURL	0	0	S	2009-03-17 00:26:23	100	url for the logo in zkwebui	D	Y	2009-03-17 00:26:23	100	/images/AD10030.png
50029	CHECK_CREDIT_ON_CASH_POS_ORDER	0	0	O	2009-08-19 12:05:21	100	Check credit on Cash POS Order	D	Y	2009-08-19 12:05:21	100	Y
50030	CHECK_CREDIT_ON_PREPAY_ORDER	0	0	O	2009-08-19 12:05:37	100	Check credit on Prepay Order	D	Y	2009-08-19 12:05:37	100	Y
50031	2PACK_HANDLE_TRANSLATIONS	0	0	S	2009-09-03 20:52:17	100	Define if 2pack handle translations or not	D	Y	2009-09-03 20:52:17	100	N
50032	INFO_DEFAULTSELECTED	0	0	C	2009-09-06 09:20:32	0	Info windows - Specify if the records should be checked(selected) by default (multi selection mode only)	D	Y	2009-09-06 09:20:32	0	N
50033	INFO_DOUBLECLICKTOGGLESSELECTION	0	0	C	2009-09-06 09:21:12	0	Info Window - True if double click on a row toggles if row is selected (multi selection mode only)	D	Y	2009-09-06 09:21:22	0	N
50034	CLIENT_ACCOUNTING	0	0	C	2009-09-13 17:37:28	100	Enable client Accounting -> D - Disabled (default) / Q - Queue (enabled to post by hand - queue documents for posterior processing) / I - Immediate (immediate post)	D	Y	2016-04-11 16:50:44	0	I
50035	LOCATION_MAX_CITY_ROWS	0	0	S	2009-09-22 14:07:19	100	Number of rows to show on city autocomplete feature on Location window	D	Y	2009-09-22 14:07:19	100	7
50037	ZK_LOGIN_ALLOW_REMEMBER_ME	0	0	S	2009-12-14 18:16:10	100	Allow remember me on zkwebui - allowed values [U]ser / [P]assword / [N]one	D	Y	2009-12-14 18:16:10	100	U
50038	SWING_LOGIN_ALLOW_REMEMBER_ME	0	0	S	2009-12-14 18:16:23	100	Allow remember me on swing - allowed values [U]ser / [P]assword / [N]one	D	Y	2009-12-14 18:16:23	100	P
50039	REAL_TIME_POS	0	0	O	2009-12-16 10:33:53	100	Enable Overwrite of Date on POS generated Invoice. When a POS Order is not completed immediately then on Completion ADempiere generated Invoice (Customer) with Date from the POS Order. In some cases this is not desired.	D	Y	2009-12-16 10:33:53	100	N
50041	ALogin_ShowOneRole	0	0	S	2010-03-25 10:30:46	100	Show Role List Box when there is just one role to select	D	Y	2010-03-25 10:30:46	100	Y
50043	QTY_TO_SHOW_FOR_SERVICES	0	0	C	2010-04-18 10:38:03	100	Quantity to show for services in InfoProduct window	D	Y	2010-04-18 10:38:03	100	99999
50049	MAIL_SEND_BCC_TO_FROM	0	0	C	2010-11-10 10:55:24	100	When enabled the outgoing mails from Adempiere will be sent BCC to the originating user	D	Y	2010-11-10 10:55:24	100	N
50105	ZK_SHOW_SYSINFO	0	0	S	2014-10-13 16:21:14	100	Show system information at ZK client login	D	Y	2014-10-13 16:21:14	100	Y
50106	ZK_BROWSER_TITLE	0	0	S	2014-10-13 16:22:32	100	Web page title for ZK client	D	Y	2014-10-13 16:22:32	100	Adempiere
50107	ZK_ENABLE_URL	0	0	S	2014-10-13 16:42:48	100	Match client using server url	D	Y	2014-10-13 16:42:48	100	Y
50118	SUPPORT_MULTITAB_BROWSER	0	0	S	2016-06-17 12:59:43	100	Support same browser have multiple tab access	D	Y	2016-06-17 12:59:43	100	N
50121	INTER_COMPANY_DUE_POSTING	0	0	S	2016-09-09 12:16:47	100	Allow to inter-org transaction balance the posting	D	Y	2016-09-09 12:16:47	100	Y
200000	RecentItems_MaxShown	0	0	C	2012-01-27 15:36:23	100	Max number of records to show on recent items dashboard	D	Y	2012-01-27 15:36:23	100	10
200001	RecentItems_MaxSaved	0	0	C	2012-01-27 15:37:11	100	Max number of records to save on recent items table per user/client	D	Y	2012-01-27 15:37:22	100	50
200002	ZK_REPORT_FORM_OUTPUT_TYPE	0	0	O	2012-02-05 12:56:28	0	Type of output in zkwebui for reports of type form, possible values are PDF, HTML, XLS	D	Y	2012-02-05 12:56:28	0	PDF
200003	ZK_REPORT_TABLE_OUTPUT_TYPE	0	0	O	2012-02-05 12:56:54	0	Type of output in zkwebui for reports of type table, possible values are PDF, HTML, XLS	D	Y	2012-02-05 12:56:54	0	PDF
200004	LOCATION_MAPS_URL_PREFIX	0	0	S	2012-02-11 12:12:19	100	URL to indicate the preferred maps service	D	Y	2012-02-11 12:12:19	100	http://local.google.com/maps?q=
200005	LOCATION_MAPS_ROUTE_PREFIX	0	0	S	2012-02-11 12:13:03	100	URL to indicate the preferred maps routing service	D	Y	2012-02-11 12:13:03	100	http://maps.google.com/maps?f=d&geocode=
200006	LOCATION_MAPS_SOURCE_ADDRESS	0	0	S	2012-02-11 12:13:49	100	Prefix for source address used by the preferred maps routing service	D	Y	2012-02-11 12:13:49	100	&saddr=
200007	LOCATION_MAPS_DESTINATION_ADDRESS	0	0	S	2012-02-11 12:14:07	100	Prefix for destination address used by the preferred maps routing service	D	Y	2012-02-11 12:14:07	100	&daddr=
200009	Invoice_ReverseUseNewNumber	0	0	C	2012-02-13 12:36:55	100	Allows you to choose to use a new number for the reversed invoice.	D	Y	2012-02-13 12:36:55	100	Y
\.
