copy "ad_val_rule" ("ad_val_rule_id", "name", "ad_client_id", "ad_org_id", "code", "created", "createdby", "description", "entitytype", "isactive", "type", "updated", "updatedby") from STDIN;
122	C_ElementValue Def.AS Account_ID	0	0	C_ElementValue.C_Element_ID IN \n(SELECT C_Element_ID FROM C_AcctSchema_Element  WHERE C_AcctSchema_ID=@$C_AcctSchema_ID@ AND ElementType='AC')	1999-10-11 00:00:00	0	Default Accounting Schema	D	Y	S	2005-10-27 15:43:31	100
137	C_ElementValue AS Account_ID	0	0	EXISTS (SELECT * FROM C_AcctSchema_Element ae WHERE C_ElementValue.C_Element_ID=ae.C_Element_ID AND ae.ElementType='AC' AND ae.C_AcctSchema_ID=@C_AcctSchema_ID@)	2000-08-06 21:46:47	0	Account based on AcctSchema	D	Y	S	2005-10-27 15:41:14	100
142	No Sales Transactions	0	0	IsSOTrx='N'	2001-01-01 18:19:36	0	\N	D	Y	S	2000-01-02 00:00:00	0
143	Sales Transactions	0	0	IsSOTrx='Y'	2001-01-01 18:20:19	0	\N	D	Y	S	2000-01-02 00:00:00	0
144	C_Payment Not Reconciled	0	0	C_Payment.IsReconciled='N'	2001-01-09 22:08:38	0	\N	D	Y	S	2000-01-02 00:00:00	0
145	C_Invoice of C_BPartner	0	0	C_Invoice.C_BPartner_ID=@C_BPartner_ID@	2001-01-13 19:42:01	0	\N	D	Y	S	2000-01-02 00:00:00	0
146	C_Invoice NotPaid	0	0	C_Invoice.IsPaid<>'Y'	2001-02-01 13:49:33	0	\N	D	Y	S	2000-01-02 00:00:00	0
147	C_Order of C_BPartner	0	0	C_Order.C_BPartner_ID=@C_BPartner_ID@	2001-03-28 22:18:56	0	\N	D	Y	S	2000-01-02 00:00:00	0
150	AD_Tree Menu	0	0	AD_Tree.TreeType='MM'	2001-12-18 23:01:59	0	Only menu items	D	Y	S	2000-01-02 00:00:00	0
151	M_DiscountSchema only PriceList	0	0	M_DiscountSchema.DiscountType='P'	2001-12-28 22:44:52	0	\N	D	Y	S	2000-01-02 00:00:00	0
162	All_Payment Rule - Check/DirectDeposit	0	0	AD_Ref_List.Value IN ('S','T')	2003-03-30 23:04:18	0	\N	D	Y	S	2000-01-02 00:00:00	0
169	K_Category Value	0	0	K_Category_ID=@K_Category_ID@	2003-07-10 21:21:25	0	\N	D	Y	S	2000-01-02 00:00:00	0
183	C_ElementValue of Element	0	0	C_ElementValue.C_Element_ID=@C_Element_ID@	2003-11-28 23:48:37	0	\N	D	Y	S	2000-01-02 00:00:00	0
212	C_Order Waiting Payment	0	0	C_Order.DocStatus='WP'	2003-10-04 10:32:00	0	\N	D	Y	S	2000-01-02 00:00:00	0
234	M_InOutLine Receipts	0	0	EXISTS (SELECT * FROM M_InOut io WHERE M_InOutLine.M_InOut_ID=io.M_InOut_ID AND io.IsSOTrx='N')	2005-07-28 12:47:31	100	\N	D	Y	S	2005-07-28 12:47:31	100
264	CM_CStage node of  WebProject	0	0	CM_CStage.CM_WebProject_ID=@CM_WebProject_ID@  AND CM_CStage.IsSummary='N'	2006-04-16 17:06:33	100	\N	D	Y	S	2006-04-22 10:12:43	100
265	CM_Container node of  WebProject	0	0	CM_Container.CM_WebProject_ID=@CM_WebProject_ID@ AND CM_Container.IsSummary='N'	2006-04-16 17:06:59	100	\N	D	Y	S	2006-04-22 10:14:07	100
266	CM_Template Node of  WebProject	0	0	CM_Template.CM_WebProject_ID=@CM_WebProject_ID@  AND CM_Template.IsSummary='N' AND CM_Template.IsInclude='N' AND CM_Template.IsValid='N'	2006-04-22 10:05:31	100	\N	D	Y	S	2006-08-06 22:19:35	100
267	CM_Media node of Ad_Cat.WebProject	0	0	CM_Media.IsSummary='N'  AND EXISTS (SELECT * FROM CM_Ad_Cat c WHERE c.CM_Ad_Cat_ID=@CM_Ad_Cat_ID@ AND CM_Media.CM_WebProject_ID=c.CM_WebProject_ID)\n	2006-04-22 10:23:07	100	\N	D	Y	S	2006-04-22 10:23:07	100
270	AD_Process Jasper Reports	0	0	AD_Process.JasperReport IS NOT NULL	2007-02-27 00:00:00	0	\N	D	Y	S	2007-02-27 00:00:00	0
51002	AD_Ref_List_ID (Document Actions)	0	0	AD_Ref_List.AD_Reference_ID=135	2020-06-25 14:47:35.353608	100	all document actions	D	Y	S	2020-06-25 14:47:34.17937	100
52060	C_DocType Outbound	0	0	C_DocType.DocBaseType IN ('WMO') AND C_DocType.IsSOTrx='Y'	2009-09-03 20:49:16	0	Document Type Warehouse Management	D	Y	S	2009-09-03 20:49:16	0
52062	C_DocType Inbound	0	0	C_DocType.DocBaseType IN ('WMO') AND C_DocType.IsSOTrx='N'	2009-09-03 20:49:16	0	Document Type Warehouse Management	D	Y	S	2009-09-03 20:49:16	0
52071	C_BPartner - Manufacturer	0	0	C_BPartner.IsActive='Y' AND C_BPartner.IsManufacturer='Y'	2009-11-29 00:52:04	100	\N	D	Y	S	2009-11-29 00:52:26	100
52336	M_Product(Resource)	0	0	M_Product.IsSummary='N' AND M_Product.IsActive='Y' AND M_Product.ProductType='R'	2014-03-18 00:51:21	100	\N	D	Y	S	2014-03-18 00:51:21	100
52432	C_Invoice of BPartner, Not Paid & BankAcct Currency	0	0	C_Invoice.DocStatus IN ('CO', 'CL') AND C_Invoice.IsPaid<>'Y' AND C_Invoice.C_BPartner_ID=C_BPartner_ID AND C_Invoice.C_Currency_ID IN (SELECT C_Currency_ID FROM C_BankAccount WHERE C_BankAccount_ID = C_BankAccount_ID)	2015-04-29 10:52:31	100	\N	D	Y	S	2015-04-29 10:52:31	100
52442	C_DocType Material Production	0	0	C_DocType.DocBaseType='MMP'	2015-11-25 11:07:43	100	\N	D	Y	S	2015-11-25 11:07:43	100
52459	C_BankAccount_ID for SB POS Bank Statement Close	0	0	EXISTS (SELECT 1 FROM C_POS pos WHERE pos.C_POS_ID=@pos.C_POS_ID@ AND pos.C_BankAccount_ID=C_BankAccount.C_BankAccount_ID)	2016-04-22 16:47:55	100	\N	D	Y	S	2016-04-22 16:47:55	100
52460	AD_Ref_List_ID (Tender Type)	0	0	AD_Ref_List.AD_Reference_ID=214	2016-04-22 16:51:18	100	all document actions	D	Y	S	2016-04-22 16:51:18	100
\.
