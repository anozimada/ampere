copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
163	Data Import	\N	0	\N	\N	0	\N	\N	1999-12-02 13:02:27	0	\N	Data Import	Y	N	N	Y	Y	2000-01-02 00:00:00	0
185	Import Business Partner	W	0	\N	\N	0	\N	172	1999-12-29 05:00:02	0	Import Business Partner	Data Import	Y	Y	N	Y	N	2000-01-02 00:00:00	0
222	Import Loader Format	W	0	\N	\N	0	\N	189	2000-09-15 14:54:13	0	Maintain Import Loader Formats	Data Import	Y	Y	N	N	N	2000-01-02 00:00:00	0
223	Import File Loader	X	0	101	\N	0	\N	\N	2000-09-15 16:59:25	0	Load flat Files into import tables	Data Import	Y	Y	N	N	N	2000-01-02 00:00:00	0
338	Import Report Line Set	W	0	\N	\N	0	\N	249	2003-01-11 16:30:16	0	Import Report Line Sets	Data Import	Y	Y	N	Y	N	2000-01-02 00:00:00	0
339	Import Account	W	0	\N	\N	0	\N	248	2003-01-11 16:29:10	0	Import Natural Account Values	Data Import	Y	Y	N	Y	N	2000-01-02 00:00:00	0
340	Import Product	W	0	\N	\N	0	\N	247	2003-01-11 16:25:16	0	Import Products	Data Import	Y	Y	N	N	N	2005-07-24 13:07:39	100
363	Import Inventory	W	0	\N	\N	0	\N	267	2003-05-28 22:43:30	0	Import Inventory Transactions	Data Import	Y	Y	N	Y	N	2000-01-02 00:00:00	0
373	Import Bank Statement	W	0	\N	\N	0	\N	277	2003-06-07 20:39:05	0	Import Bank Statements	Data Import	Y	Y	N	Y	N	2000-01-02 00:00:00	0
374	Import Payment	W	0	\N	\N	0	\N	280	2003-06-07 21:19:14	0	Import Payments	Data Import	Y	Y	N	Y	N	2000-01-02 00:00:00	0
376	Import Order	W	0	\N	\N	0	\N	281	2003-06-07 21:30:29	0	Import Orders	Data Import	Y	Y	N	Y	N	2000-01-02 00:00:00	0
378	Import GL Journal	W	0	\N	\N	0	\N	278	2003-06-07 20:41:39	0	Import General Ledger Journals	Data Import	Y	Y	N	Y	N	2005-10-08 10:25:04	100
382	Import Invoice	W	0	\N	\N	0	\N	279	2003-06-07 21:07:40	0	Import Invoices	Data Import	Y	Y	N	Y	N	2005-04-02 23:33:51	100
423	Load Bank Statement	P	0	\N	\N	0	247	\N	2003-12-25 15:13:33	0	Load Bank Statement	Data Import	Y	Y	N	N	N	2000-01-02 00:00:00	0
424	Delete Import	P	0	\N	\N	0	248	\N	2003-12-26 10:53:17	0	Delete all data in Import Table	Data Import	Y	Y	N	N	N	2000-01-02 00:00:00	0
425	Import Currency Rate	W	0	\N	\N	0	\N	296	2003-12-29 20:42:46	0	Import Currency Conversion Rates	Data Import	Y	Y	N	N	N	2000-01-02 00:00:00	0
486	Import Confirmations	W	0	\N	\N	0	\N	334	2004-07-02 15:16:05	0	Import Receipt/Shipment Confirmation Lines	Data Import	Y	Y	N	N	N	2000-01-02 00:00:00	0
53206	Import Price List	W	0	\N	\N	0	\N	53071	2009-03-18 00:01:33	100	Import Price Lists	Data Import	Y	Y	N	N	N	2009-03-18 00:01:33	100
53406	Data Import	W	0	\N	\N	0	\N	53172	2012-02-28 17:53:10	100	Data import definition	Data Import	Y	Y	N	N	N	2012-02-28 17:53:10	100
53407	Import File Loader	P	0	\N	\N	0	53288	\N	2012-03-09 16:26:42	100	Process to load data from file into import table	Data Import	Y	Y	N	N	N	2012-03-09 16:26:42	100
53408	Import Lead	W	0	\N	\N	0	\N	53173	2012-03-13 16:09:00	100	\N	Data Import	Y	Y	N	N	N	2012-03-13 16:09:00	100
53665	Import Budget	W	0	\N	\N	0	\N	53286	2013-10-24 13:01:13	100	\N	Data Import	Y	Y	N	N	N	2013-10-24 13:01:13	100
53846	Import Invoice/Payment Report	P	0	\N	\N	0	53651	\N	2014-07-04 14:15:08	100	\N	Data Import	Y	Y	N	N	N	2014-07-04 14:15:08	100
53988	Import Orders	P	0	\N	\N	0	53779	\N	2015-06-03 19:34:03	100	Import Payments	Data Import	Y	Y	N	Y	N	2015-06-03 19:34:03	100
54124	Import Column	W	0	\N	\N	0	\N	53499	2016-12-06 16:29:55	100	\N	Data Import	Y	Y	N	N	N	2016-12-06 16:29:55	100
\.
