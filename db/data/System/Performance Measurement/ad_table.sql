copy "ad_table" ("ad_table_id", "tablename", "name", "accesslevel", "ad_client_id", "ad_org_id", "ad_val_rule_id", "ad_window_id", "copycolumnsfromtable", "created", "createdby", "description", "entitytype", "help", "importtable", "isactive", "iscentrallymaintained", "ischangelog", "isdeleteable", "ishighvolume", "issecurityenabled", "isview", "loadseq", "po_window_id", "replicationtype", "sqlstatement", "updated", "updatedby") from STDIN;
438	PA_Achievement	Achievement	6	0	0	\N	215	\N	2001-04-17 21:21:17	0	Performance Achievement	Performance Measurement	\N	\N	Y	Y	N	N	N	N	N	145	\N	L	\N	2005-12-25 17:11:32	100
440	PA_Goal	Goal	6	0	0	\N	212	\N	2001-04-17 21:21:18	0	Performance Goal	Performance Measurement	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2005-12-25 17:10:54	100
441	PA_Measure	Measure	6	0	0	\N	215	\N	2001-04-17 21:21:18	0	Concrete Performance Measurement	Performance Measurement	\N	\N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2005-12-25 14:38:16	100
442	PA_MeasureCalc	Measure Calculation	6	0	0	\N	213	\N	2001-04-17 21:21:18	0	Calculation method for measuring performance	Performance Measurement	\N	\N	Y	Y	N	Y	N	N	N	130	\N	L	\N	2005-12-25 14:37:59	100
592	AD_AlertRecipient	Alert Recipient	6	0	0	\N	276	\N	2003-06-07 19:48:39	0	Recipient of the Alert Notification	Performance Measurement	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
593	AD_AlertRule	Alert Rule	6	0	0	\N	276	\N	2003-06-07 19:48:39	0	Definition of the alert element	Performance Measurement	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
594	AD_Alert	Alert	6	0	0	\N	276	\N	2003-06-07 19:48:39	0	Adempiere Alert	Performance Measurement	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
743	PA_SLA_Measure	SLA Measure	3	0	0	\N	335	\N	2004-07-07 17:42:22	0	Service Level Agreement Measure	Performance Measurement	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2005-04-26 20:43:00	100
744	PA_SLA_Criteria	SLA Criteria	6	0	0	\N	335	\N	2004-07-07 17:42:22	0	Service Level Agreement Criteria	Performance Measurement	\N	\N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2005-04-26 20:42:40	100
745	PA_SLA_Goal	SLA Goal	3	0	0	\N	335	\N	2004-07-07 17:42:22	0	Service Level Agreement Goal	Performance Measurement	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2005-04-26 20:42:52	100
831	PA_ColorSchema	Color Schema	6	0	0	\N	364	\N	2005-12-23 16:39:49	100	Performance Color Schema	Performance Measurement	\N	N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2005-12-25 17:11:47	100
832	PA_GoalRestriction	Goal Restriction	6	0	0	\N	212	\N	2005-12-23 18:10:39	100	Performance Goal Restriction	Performance Measurement	\N	N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2005-12-25 17:11:59	100
833	PA_Benchmark	Benchmark	6	0	0	\N	365	\N	2005-12-26 12:30:07	100	Performance Benchmark	Performance Measurement	\N	N	Y	Y	N	Y	N	N	N	130	\N	L	\N	2005-12-26 12:55:04	100
834	PA_BenchmarkData	Benchmark Data	6	0	0	\N	365	\N	2005-12-26 12:40:43	100	Performance Benchmark Data Point	Performance Measurement	\N	N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2005-12-26 12:55:01	100
835	PA_Ratio	Ratio	2	0	0	\N	366	\N	2005-12-26 12:49:59	100	Performace Ratio	Performance Measurement	\N	N	Y	Y	N	Y	N	N	N	130	\N	L	\N	2005-12-26 12:54:58	100
836	PA_RatioElement	Ratio Element	2	0	0	\N	366	\N	2005-12-26 12:54:16	100	Performance Ratio Element	Performance Measurement	\N	N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2005-12-26 12:54:55	100
53282	AD_ChartDatasource	Chart Datasource	4	0	0	\N	53124	N	2010-08-23 13:23:59	100	Chart data definition	Performance Measurement	Query to retrieve data for chart	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2011-11-02 16:34:58	0
53284	AD_Chart	Chart	4	0	0	\N	53124	N	2010-08-23 13:44:21	100	Chart definition	Performance Measurement	Charts are for the graphical display of information.	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2012-06-08 15:15:31	0
\.
