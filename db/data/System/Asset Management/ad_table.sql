copy "ad_table" ("ad_table_id", "tablename", "name", "accesslevel", "ad_client_id", "ad_org_id", "ad_val_rule_id", "ad_window_id", "copycolumnsfromtable", "created", "createdby", "description", "entitytype", "help", "importtable", "isactive", "iscentrallymaintained", "ischangelog", "isdeleteable", "ishighvolume", "issecurityenabled", "isview", "loadseq", "po_window_id", "replicationtype", "sqlstatement", "updated", "updatedby") from STDIN;
539	A_Asset	Asset	3	0	0	\N	53272	\N	2003-01-23 00:08:48	0	Asset used internally or by customers	Asset Management	\N	\N	Y	Y	N	Y	Y	N	N	120	\N	L	\N	2016-04-11 16:36:20	0
542	A_Asset_Group	Asset Group	3	0	0	\N	53270	\N	2003-01-23 00:08:48	0	Group of Assets	Asset Management	\N	\N	Y	Y	N	Y	N	N	N	70	\N	L	\N	2016-04-11 16:36:20	0
53131	A_Asset_Info_Tax	Asset Info Tax	7	0	0	\N	\N	\N	2008-05-30 16:54:21	100	\N	Asset Management	This table holds tax related settings and information for each individual fixed asset	N	Y	Y	N	N	N	N	N	\N	\N	L	\N	2016-04-11 16:33:54	0
53132	A_Asset_Info_Fin	Asset Info Fin.	7	0	0	\N	\N	\N	2008-05-30 16:54:53	100	\N	Asset Management	This table holds financial related settings and information for each individual fixed asset	N	Y	Y	N	N	N	N	N	\N	\N	L	\N	2016-04-11 16:33:44	0
53134	A_Asset_Info_Lic	Asset Info Lic.	7	0	0	\N	\N	\N	2008-05-30 16:57:50	100	\N	Asset Management	This table holds license related settings and information for each individual fixed asset	N	Y	Y	N	N	N	N	N	\N	\N	L	\N	2016-04-11 16:33:49	0
53135	A_Asset_Info_Ins	Asset Info Ins.	7	0	0	\N	\N	\N	2008-05-30 16:58:15	100	\N	Asset Management	This table holds insurance related settings and information for each individual fixed asset	N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2016-04-11 16:33:52	0
53136	A_Asset_Info_Oth	Asset Info Oth.	7	0	0	\N	\N	\N	2008-05-30 16:58:43	100	\N	Asset Management	This table holds other user defined related settings and information for each individual fixed asset	N	Y	Y	N	N	N	N	N	\N	\N	L	\N	2016-04-11 16:33:57	0
53573	C_ElementValue_FA	Account Elements for Fixed Assets	3	0	0	\N	\N	\N	2014-07-14 15:12:54	100	Account Elements for Fixed Assets	Asset Management	\N	\N	Y	N	N	Y	N	N	N	\N	\N	L	\N	2014-07-14 15:12:54	100
53574	GAAS_Asset_Group_Acct	Asset Group Accounting	3	0	0	\N	53270	\N	2014-07-14 15:08:17	100	\N	Asset Management	\N	N	Y	N	N	Y	N	N	N	\N	\N	L	\N	2016-04-11 16:36:20	0
53575	GAAS_AssetJournal	Asset Journal	3	0	0	\N	53271	\N	2014-07-14 15:05:46	100	\N	Asset Management	\N	N	Y	N	N	Y	N	N	N	\N	\N	L	\N	2014-07-14 15:05:46	100
53576	GAAS_AssetJournalLine	Asset Journal Line	3	0	0	\N	53271	\N	2014-07-14 15:06:23	100	\N	Asset Management	\N	N	Y	N	N	Y	N	N	N	\N	\N	L	\N	2016-04-11 16:37:03	0
53578	GAAS_AssetJournalLine_Det	Asset Journal Line Detail	3	0	0	\N	53271	\N	2014-07-14 15:06:59	100	\N	Asset Management	\N	N	Y	N	N	Y	N	N	N	\N	\N	L	\N	2014-07-14 15:06:59	100
53579	GAAS_Depreciation_Workfile	Depreciation Workfile	3	0	0	\N	53272	\N	2014-07-14 15:09:11	100	\N	Asset Management	\N	N	Y	N	N	Y	N	N	N	\N	\N	L	\N	2016-04-11 16:36:20	0
53580	GAAS_Depreciation_Expense	Depreciation Expense	3	0	0	\N	53272	\N	2014-07-14 15:09:53	100	\N	Asset Management	\N	N	Y	N	N	N	N	N	N	\N	\N	L	\N	2014-07-14 15:09:53	100
53582	I_GAAS_Asset	Import Asset FA	3	0	0	\N	53273	\N	2014-07-14 15:07:08	100	\N	Asset Management	\N	N	Y	N	N	Y	N	N	N	\N	\N	L	\N	2016-04-11 16:36:01	0
53826	RV_Fact_Acct_FA	Accounting Fact View FA	3	0	0	\N	\N	\N	2014-07-14 15:05:39	100	\N	Asset Management	\N	\N	Y	N	N	N	N	N	Y	\N	\N	L	\N	2016-04-11 16:36:20	0
53827	GAAS_Asset_Acct	Asset Acct	3	0	0	\N	53272	\N	2014-07-14 15:06:40	100	\N	Asset Management	\N	N	Y	N	N	Y	N	N	N	\N	\N	L	\N	2016-04-11 16:36:20	0
\.
