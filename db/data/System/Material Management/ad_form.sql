copy "ad_form" ("ad_form_id", "name", "accesslevel", "ad_client_id", "ad_org_id", "classname", "created", "createdby", "description", "entitytype", "help", "isactive", "isbetafunctionality", "jspurl", "updated", "updatedby") from STDIN;
103	Material Transactions	3	0	0	org.compiere.apps.form.VTrxMaterial	2000-10-31 22:19:31	0	Material Transactions	Material Management	\N	Y	N	\N	2000-01-02 00:00:00	0
114	BOM Drop	1	0	0	org.compiere.apps.form.VBOMDrop	2003-12-30 10:42:46	0	Drop (expand) Bill of Materials	Material Management	Drop the extended Bill of Materials into an Order, Invoice, etc.  The documents need to be in a Drafted stage.  Make sure that the items included in the BOM are on the price list of the Order, Invoice, etc. as otherwise the price will be zero!	Y	N	\N	2000-01-02 00:00:00	0
53037	Stocktake	3	0	0	org.compiere.apps.form.VStocktake	2013-09-23 13:54:09	100	Physical Inventory stock count form	Material Management	\N	Y	N	\N	2013-09-23 13:54:09	100
53054	Movement Scanner	3	0	0	org.adaxa.form.VMovementScannerForm	2015-04-08 13:21:36	100	Inventory Move using Barcode Scanner	Material Management	\N	Y	N	\N	2015-04-08 13:21:36	100
\.
