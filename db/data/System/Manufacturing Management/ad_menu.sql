copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
320	Resource Type	W	0	\N	\N	0	\N	237	2002-06-15 22:26:25	0	Maintain Resource Types	Manufacturing Management	N	Y	N	Y	N	2007-12-17 08:50:27	0
478	Forecast	W	0	\N	\N	0	\N	328	2004-04-17 11:11:08	0	Maintain Material Forecast	Manufacturing Management	N	Y	N	N	N	2008-06-25 22:56:40	0
484	Move Confirmation	W	0	\N	\N	0	\N	333	2004-06-17 11:29:43	0	Confirm Inventory Moves	Manufacturing Management	N	Y	N	N	N	2005-07-24 13:09:51	100
53014	Manufacturing Management	\N	0	\N	\N	0	\N	\N	2007-12-17 08:49:35	0	Manufacturing	Manufacturing Management	Y	N	N	N	Y	2007-12-17 08:49:35	0
53016	Engineering Management	\N	0	\N	\N	0	\N	\N	2007-12-17 08:49:38	0	Engineering management involves the overall management of organizations with an orientation to manufacturing, engineering, technology or production.	Manufacturing Management	Y	N	N	N	Y	2007-12-17 08:49:38	0
53017	Resource Manufacturing	\N	0	\N	\N	0	\N	\N	2007-12-17 08:50:14	0	Resource Manufacturing	Manufacturing Management	Y	N	N	N	Y	2007-12-17 08:50:14	0
53029	Planning Management	\N	0	\N	\N	0	\N	\N	2007-12-17 08:51:24	0	Using Planning Management you answer the question: When and How Many products we must get?	Manufacturing Management	Y	N	N	N	Y	2007-12-17 08:51:24	0
53066	Distribution Management	\N	0	\N	\N	0	\N	\N	2007-12-17 08:52:37	0	Distribution Resource Planning (DRP) is a method used in business administration for planning orders within a supply chain. 	Manufacturing Management	Y	N	N	N	Y	2007-12-17 08:52:37	0
53180	Forecast Management	\N	0	\N	\N	0	\N	\N	2008-06-25 22:52:31	0	\N	Manufacturing Management	Y	N	N	N	Y	2008-06-25 22:52:31	0
53181	Forecast Report by Period	R	0	\N	\N	0	53146	\N	2008-06-25 22:52:33	0	Forecast Report by Period	Manufacturing Management	N	Y	N	N	N	2008-06-25 22:52:33	0
53183	Forecast Report	R	0	\N	\N	0	53144	\N	2008-06-25 22:57:55	0	Forecast Report	Manufacturing Management	N	Y	N	N	N	2008-06-25 22:57:55	0
\.
