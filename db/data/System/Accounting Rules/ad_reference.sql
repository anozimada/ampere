copy "ad_reference" ("ad_reference_id", "name", "validationtype", "ad_client_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "isorderbyvalue", "updated", "updatedby", "vformat") from STDIN;
116	C_Element Type	L	0	0	1999-06-23 00:00:00	0	Account Element Types	Accounting Rules	\N	Y	N	2000-01-02 00:00:00	0	L
122	C_AcctSchema Costing Method	L	0	0	1999-06-23 00:00:00	0	Costing Method list	Accounting Rules	\N	Y	N	2007-12-17 05:03:28	0	L
123	C_AcctSchema GAAP	L	0	0	1999-06-23 00:00:00	0	General Accepted Accounting Principle list	Accounting Rules	\N	Y	N	2000-01-02 00:00:00	0	LL
132	Account_ID (Trx)	T	0	0	1999-07-01 00:00:00	0	Account selection based on Client	Accounting Rules	\N	Y	N	2016-04-11 16:36:22	0	\N
133	C_Location	T	0	0	1999-07-01 00:00:00	0	Location selection	Accounting Rules	\N	Y	N	2016-04-11 16:36:23	0	\N
134	Account_ID - User1	T	0	0	1999-07-01 00:00:00	0	User1 selection based on Client	Accounting Rules	\N	Y	N	2019-02-04 13:38:37	0	\N
137	Account_ID - User2	T	0	0	1999-07-07 00:00:00	0	User2 selection based on Client	Accounting Rules	\N	Y	N	2019-02-04 13:38:37	0	\N
141	C_Project (No summary)	T	0	0	1999-07-07 00:00:00	0	Project selection	Accounting Rules	\N	Y	N	2000-01-02 00:00:00	0	\N
142	C_Activity (No summary)	T	0	0	1999-07-07 00:00:00	0	Activity selection	Accounting Rules	\N	Y	N	2016-04-11 16:36:02	0	\N
144	C_Sales Region (No summary)	T	0	0	1999-07-07 00:00:00	0	Sales Region selection (No summary)	Accounting Rules	\N	Y	N	2000-01-02 00:00:00	0	\N
148	C_DocType SubTypeSO	L	0	0	1999-08-05 00:00:00	0	Order Types list	Accounting Rules	\N	Y	N	2000-01-02 00:00:00	0	\N
156	C_Country	T	0	0	1999-08-09 00:00:00	0	Country selection	Accounting Rules	\N	Y	N	2008-03-03 22:13:09	0	\N
178	GL_Budget Status	L	0	0	1999-09-27 00:00:00	0	\N	Accounting Rules	\N	Y	N	2000-01-02 00:00:00	0	L
181	C_AcctSchema ElementType	L	0	0	1999-10-04 00:00:00	0	Element Types for Accounting Elements	Accounting Rules	Hardcoded Element Types	Y	N	2000-01-02 00:00:00	0	AA
206	AD_Table Posting	T	0	0	2000-08-06 23:36:51	0	Posting Tables	Accounting Rules	\N	Y	N	2000-01-02 00:00:00	0	\N
207	GL Category Type	L	0	0	2000-08-19 14:04:33	0	\N	Accounting Rules	\N	Y	N	2000-01-02 00:00:00	0	\N
287	C_Tax SPPOType	L	0	0	2003-08-19 23:21:10	0	\N	Accounting Rules	\N	Y	N	2008-03-03 22:12:56	0	\N
331	Account_ID	T	0	0	2004-11-28 13:42:26	0	Account selection based on Client	Accounting Rules	\N	Y	N	2016-04-11 16:34:04	0	\N
355	C_AcctSchema CostingLevel	L	0	0	2005-07-26 16:31:16	0	\N	Accounting Rules	\N	Y	N	2005-07-26 16:31:16	0	\N
359	C_AcctSchema CommitmentType	L	0	0	2005-10-11 17:10:43	100	\N	Accounting Rules	\N	Y	N	2005-10-11 17:10:43	100	\N
361	GL_BudgetControl Scope	L	0	0	2005-10-25 09:58:06	100	\N	Accounting Rules	\N	Y	N	2005-10-25 09:58:06	100	\N
362	C_ElementValue (trx)	T	0	0	2005-10-27 15:37:02	100	Element Values	Accounting Rules	\N	Y	N	2005-10-27 16:09:53	100	\N
392	C_AcctSchema TaxCorrectionType	L	0	0	2006-07-23 17:16:53	100	\N	Accounting Rules	\N	Y	N	2006-08-10 20:23:01	100	\N
53240	C_TaxBase	L	0	0	2008-03-03 22:16:46	0	\N	Accounting Rules	\N	Y	N	2008-03-03 22:16:46	0	\N
53425	Print Document	L	0	0	2012-01-13 14:09:15	100	\N	Accounting Rules	\N	Y	Y	2012-01-13 14:09:15	100	\N
53770	X_AuthorityType	T	0	0	2015-01-29 14:09:22	100	\N	Accounting Rules	\N	Y	N	2015-01-29 14:09:22	100	\N
53773	IncludeExclude	L	0	0	2015-02-05 10:06:01	100	\N	Accounting Rules	\N	Y	N	2015-02-05 10:06:01	100	\N
\.
