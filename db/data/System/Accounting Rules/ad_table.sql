copy "ad_table" ("ad_table_id", "tablename", "name", "accesslevel", "ad_client_id", "ad_org_id", "ad_val_rule_id", "ad_window_id", "copycolumnsfromtable", "created", "createdby", "description", "entitytype", "help", "importtable", "isactive", "iscentrallymaintained", "ischangelog", "isdeleteable", "ishighvolume", "issecurityenabled", "isview", "loadseq", "po_window_id", "replicationtype", "sqlstatement", "updated", "updatedby") from STDIN;
115	AD_Sequence	Sequence	6	0	0	\N	112	\N	1999-05-21 00:00:00	0	Document Sequence	Accounting Rules	\N	\N	Y	Y	N	N	N	N	N	90	\N	L	\N	2000-01-02 00:00:00	0
140	C_Conversion_Rate	Conversion Rate	6	0	0	\N	116	\N	1999-05-21 00:00:00	0	Rate used for converting currencies	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
141	C_Currency	Currency	6	0	0	\N	115	\N	1999-05-21 00:00:00	0	The Currency for this record	Accounting Rules	\N	\N	Y	Y	N	N	Y	N	N	50	\N	L	\N	2000-01-02 00:00:00	0
142	C_Element	Element	2	0	0	\N	118	\N	1999-05-21 00:00:00	0	Accounting Element	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	95	\N	L	\N	2005-11-20 16:11:42	100
176	C_ValidCombination	Combination	2	0	0	\N	153	\N	1999-06-03 00:00:00	0	Valid Account Combination	Accounting Rules	\N	\N	Y	Y	N	N	N	N	N	130	\N	L	\N	2005-11-23 18:43:17	100
188	C_ElementValue	Account Element	2	0	0	\N	118	\N	1999-06-03 00:00:00	0	Account Element	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	100	\N	L	\N	2005-11-20 16:11:48	100
217	C_DocType	Document Type	6	0	0	\N	135	\N	1999-06-28 00:00:00	0	Document type or rules	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	100	\N	L	\N	2000-01-02 00:00:00	0
218	GL_Category	GL Category	2	0	0	\N	131	\N	1999-06-28 00:00:00	0	General Ledger Category	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	95	\N	L	\N	2000-01-02 00:00:00	0
252	C_TaxCategory	Tax Category	2	0	0	\N	138	\N	1999-08-08 00:00:00	0	Tax Category	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	75	\N	L	\N	2008-03-03 22:11:51	0
261	C_Tax	Tax	2	0	0	\N	137	\N	1999-08-08 00:00:00	0	Tax identifier	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	110	\N	L	\N	2008-03-03 22:12:56	0
265	C_AcctSchema	Accounting Schema	2	0	0	\N	125	\N	1999-09-21 00:00:00	0	Rules for accounting	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	125	\N	L	\N	2000-01-02 00:00:00	0
266	C_AcctSchema_GL	C_AcctSchema_GL	2	0	0	\N	125	\N	1999-09-21 00:00:00	0	\N	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
271	GL_Budget	Budget	3	0	0	\N	154	\N	1999-09-21 00:00:00	0	General Ledger Budget	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	130	\N	L	\N	2000-01-02 00:00:00	0
279	C_AcctSchema_Element	Acct.Schema Element	2	0	0	\N	125	\N	1999-10-05 00:00:00	0	\N	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
300	C_DocType_Trl	Document Type Trl	6	0	0	\N	135	\N	1999-12-04 19:50:17	0	Document type or rules	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
302	C_ElementValue_Trl	Account Element Trl	2	0	0	\N	118	\N	1999-12-04 19:50:17	0	Account Element	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2005-11-20 16:11:55	100
313	C_Charge	Charge	3	0	0	\N	161	\N	1999-12-04 19:50:17	0	Additional document charges	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	100	\N	L	\N	2000-01-02 00:00:00	0
315	C_AcctSchema_Default	C_AcctSchema_Default	2	0	0	\N	125	\N	1999-12-19 20:39:32	0	\N	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2016-04-11 16:36:14	0
316	C_Activity	Activity	2	0	0	\N	134	\N	1999-12-19 20:39:32	0	Business Activity	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	100	\N	L	\N	2000-01-02 00:00:00	0
348	C_TaxCategory_Trl	Tax Category Trl	2	0	0	\N	138	\N	2000-03-19 08:35:31	0	Tax Category	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2008-03-03 22:11:39	0
396	C_Charge_Acct	C_Charge_Acct	3	0	0	\N	161	\N	2000-12-17 16:19:52	0	\N	Accounting Rules	\N	\N	Y	Y	N	N	N	N	N	145	\N	L	\N	2005-09-24 09:24:52	100
399	C_Tax_Acct	C_Tax_Acct	3	0	0	\N	137	\N	2000-12-17 16:19:52	0	\N	Accounting Rules	\N	\N	Y	Y	N	N	N	N	N	145	\N	L	\N	2008-03-03 22:13:46	0
546	C_Tax_Trl	Tax Trl	2	0	0	\N	137	\N	2003-02-17 21:07:16	0	Tax identifier	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2008-03-03 22:13:34	0
617	C_Currency_Trl	Currency Trl	6	0	0	\N	115	\N	2003-08-06 20:02:45	0	The Currency for this record	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
635	RV_UnPosted	Not Posted	3	0	0	\N	294	\N	2003-12-14 01:28:38	0	\N	Accounting Rules	\N	\N	Y	Y	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:58:44	100
637	C_ConversionType	Currency Type	6	0	0	\N	295	\N	2003-12-21 00:09:13	0	Currency Conversion Rate Type	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	100	\N	L	\N	2000-01-02 00:00:00	0
638	C_Currency_Acct	C_Currency_Acct	3	0	0	\N	115	\N	2003-12-21 00:09:13	0	\N	Accounting Rules	\N	\N	Y	Y	N	N	N	N	N	145	\N	L	\N	2005-09-24 09:24:56	100
701	C_TaxPostal	Tax Postcode	2	0	0	\N	137	\N	2004-03-11 23:53:21	0	Tax Postcode	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2008-03-03 22:13:24	0
707	GL_DistributionLine	GL Distribution Line	2	0	0	\N	323	\N	2004-03-19 12:37:22	0	General Ledger Distribution Line	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
708	GL_Distribution	GL Distribution	2	0	0	\N	323	\N	2004-03-19 12:37:24	0	General Ledger Distribution	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
718	C_DocTypeCounter	Counter Document	2	0	0	\N	327	\N	2004-04-14 12:45:12	0	Counter Document Relationship	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2005-04-26 20:37:16	100
818	C_TaxDeclaration	Tax Declaration	3	0	0	\N	359	\N	2005-10-18 11:09:26	100	Define the declaration to the tax authorities	Accounting Rules	The tax declaration allows you to create supporting information and reconcile the documents with the accounting	N	Y	Y	N	N	N	N	N	140	\N	L	\N	2005-10-18 12:01:16	100
819	C_TaxDeclarationLine	Tax Declaration Line	3	0	0	\N	359	\N	2005-10-18 11:24:28	100	Tax Declaration Document Information	Accounting Rules	The lines are created by the create process. You can delete them if you do not want to include them in a particular declaration. 	N	Y	Y	N	N	N	N	N	145	\N	L	\N	2005-11-20 15:31:02	100
820	C_TaxDeclarationAcct	Tax Declaration Accounting	3	0	0	\N	359	\N	2005-10-18 11:33:54	100	Tax Accounting Reconciliation 	Accounting Rules	Displays all accounting related information for reconcilation with documents. It includes all revenue/expense and tax entries as a base for detail reporting	N	Y	Y	N	N	N	N	N	145	\N	L	\N	2005-10-18 12:01:00	100
822	GL_BudgetControl	Budget Control	2	0	0	\N	361	\N	2005-10-25 09:55:23	100	Budget Control	Accounting Rules	\N	N	Y	Y	N	N	N	N	N	145	\N	L	\N	2005-10-25 09:55:23	100
823	GL_Fund	GL Fund	2	0	0	\N	362	\N	2005-10-25 10:33:45	100	General Ledger Funds Control	Accounting Rules	\N	N	Y	Y	N	N	N	N	N	140	\N	L	\N	2005-10-25 10:34:28	100
824	GL_FundRestriction	Fund Restriction	2	0	0	\N	362	\N	2005-10-25 10:48:13	100	Restriction of Funds	Accounting Rules	\N	N	Y	Y	N	N	N	N	N	145	\N	L	\N	2005-10-25 10:48:13	100
825	C_SubAcct	Sub Account	3	0	0	\N	\N	\N	2005-10-31 20:43:53	100	Sub account for Element Value	Accounting Rules	\N	N	Y	Y	N	N	N	N	N	145	\N	L	\N	2005-10-31 20:43:53	100
53066	C_TaxGroup	Tax Group	3	0	0	\N	53020	\N	2008-03-03 22:11:10	0	Tax Group	Accounting Rules	\N	N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2008-03-03 22:11:10	0
53067	C_TaxDefinition	Tax Definition	3	0	0	\N	53021	\N	2008-03-03 22:14:08	0	\N	Accounting Rules	\N	N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2008-03-03 22:14:08	0
53068	C_TaxType	Tax Type	3	0	0	\N	53023	\N	2008-03-03 22:16:21	0	\N	Accounting Rules	\N	N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2008-03-03 22:16:21	0
53069	C_TaxBase	Tax Base	3	0	0	\N	53024	\N	2008-03-03 22:16:43	0	\N	Accounting Rules	\N	N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2008-03-03 22:16:43	0
53145	C_ChargeType	Charge Type	3	0	0	\N	53062	N	2008-08-26 22:38:52	100	Charge Type	Accounting Rules	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2008-08-26 22:59:33	100
53146	C_ChargeType_DocType	Charge Type by Doc Type	3	0	0	\N	53062	N	2008-08-26 22:48:46	100	Configuration table for charges to be used by document type	Accounting Rules	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2008-08-26 22:59:13	100
53187	C_Charge_Trl	Charge Trl	3	0	0	\N	161	N	2009-04-17 15:04:53	0	Additional document charges	Accounting Rules	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2009-04-17 15:44:32	0
53939	C_ElementValidation	Account Element Validation ID	2	0	0	\N	118	\N	2015-02-05 09:50:43	100	\N	Accounting Rules	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2016-04-11 16:39:33	0
53982	X_Conv_Currency	Conversion Currency ID	3	0	0	\N	53422	N	2015-07-24 19:15:30	0	\N	Accounting Rules	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2016-04-11 16:43:55	0
53983	X_Conv_Currency_Rate	Conversion Currency Rate ID	3	0	0	\N	53422	N	2015-07-24 19:28:50	0	\N	Accounting Rules	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2016-04-11 16:43:55	0
\.
