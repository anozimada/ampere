copy "ad_val_rule" ("ad_val_rule_id", "name", "ad_client_id", "ad_org_id", "code", "created", "createdby", "description", "entitytype", "isactive", "type", "updated", "updatedby") from STDIN;
125	C_DocType Material Shipments/Receipts	0	0	C_DocType.DocBaseType IN ('MMR', 'MMS')	2000-01-30 13:55:28	0	Document Type Material Shipments and Receipts	Accounting Rules	Y	S	2009-06-01 00:01:38	100
126	C_DocType AR Pro Forma Invoices	0	0	C_DocType.DocBaseType='ARF'	2000-01-30 14:35:37	0	Document Type AR Pro Forma Invoice	Accounting Rules	Y	S	2000-01-02 00:00:00	0
138	C_Accounts User1	0	0	EXISTS (SELECT * FROM C_AcctSchema_Element ae WHERE C_ElementValue.C_Element_ID=ae.C_Element_ID AND ae.ElementType='U1' AND ae.C_AcctSchema_ID=@C_AcctSchema_ID@)	2000-08-06 21:47:40	0	based on C_AcctSchema_ID	Accounting Rules	Y	S	2000-01-02 00:00:00	0
139	C_Accounts User2	0	0	EXISTS (SELECT * FROM C_AcctSchema_Element ae WHERE C_ElementValue.C_Element_ID=ae.C_Element_ID AND ae.ElementType='U2' AND ae.C_AcctSchema_ID=@C_AcctSchema_ID@)	2000-08-06 21:47:54	0	based on C_AcctSchema_ID	Accounting Rules	Y	S	2000-01-02 00:00:00	0
155	C_Region of To_Country	0	0	C_Region.C_Country_ID=@To_Country_ID@	2003-01-08 13:09:37	0	\N	Accounting Rules	Y	S	2008-03-03 22:13:10	0
204	C_DocType Material Difference	0	0	C_DocType.DocBaseType='@DocBaseType@'	2000-01-30 13:55:28	0	Document Type Material Shipments and Receipts for Split Differnce	Accounting Rules	Y	S	2000-01-02 00:00:00	0
232	C_Project (Trx)	0	0	C_Project.IsSummary='N' AND C_Project.IsActive='Y' AND C_Project.Processed='N'	2005-07-24 14:02:12	100	\N	Accounting Rules	Y	S	2007-12-17 07:46:11	0
235	C_Activity (Trx)	0	0	C_Activity.IsActive='Y' AND C_Activity.IsSummary='N'	2005-08-23 17:56:20	100	\N	Accounting Rules	Y	S	2007-12-17 07:21:49	0
236	C_Campaign (Trx)	0	0	C_Campaign.IsActive='Y' AND C_Campaign.IsSummary='N'	2005-08-23 17:58:02	100	\N	Accounting Rules	Y	S	2016-04-11 16:37:04	0
237	C_ElementValue of Element (Trx)	0	0	C_ElementValue.C_Element_ID=@C_Element_ID@ AND C_ElementValue.IsActive='Y' AND C_ElementValue.IsSummary='N'	2005-08-23 18:00:53	100	\N	Accounting Rules	Y	S	2005-08-23 18:00:53	100
238	C_SalesRegion (Trx)	0	0	C_SalesRegion.IsActive='Y' AND C_SalesRegion.IsSummary='N'	2005-08-23 18:04:23	100	\N	Accounting Rules	Y	S	2016-04-11 16:37:04	0
242	C_AcctSchema CostingMethod	0	0	AD_Ref_List.Value <> 'x'	2005-09-13 21:30:50	100	\N	Accounting Rules	Y	S	2005-09-13 21:30:50	100
244	AD_Tree Element Value Complete	0	0	AD_Tree.TreeType IN ('EV','U1','U2') AND AD_Tree.IsAllNodes='Y'	2005-10-23 18:19:27	100	Only Element Value items	Accounting Rules	Y	S	2006-04-17 10:57:59	100
252	C_ElementValue AS Account_ID (active)	0	0	EXISTS (SELECT * FROM C_AcctSchema_Element ae WHERE C_ElementValue.C_Element_ID=ae.C_Element_ID AND ae.ElementType='AC' AND ae.C_AcctSchema_ID=@C_AcctSchema_ID@) AND C_ElementValue.IsActive='Y'	2005-10-25 10:56:12	100	Selected Accounting Schema	Accounting Rules	Y	S	2005-10-27 15:45:05	100
254	AD_Column Key	0	0	AD_Column.IsKey='Y' AND AD_Column.IsActive='Y'	2005-11-01 03:55:56	100	Key Columns	Accounting Rules	Y	S	2005-11-01 03:55:56	100
255	C_SubAcct of Account	0	0	C_SubAcct.C_ElementValue_ID=@Account_ID@	2005-11-01 08:30:15	100	\N	Accounting Rules	Y	S	2005-11-01 08:30:15	100
\.
