copy "ad_table" ("ad_table_id", "tablename", "name", "accesslevel", "ad_client_id", "ad_org_id", "ad_val_rule_id", "ad_window_id", "copycolumnsfromtable", "created", "createdby", "description", "entitytype", "help", "importtable", "isactive", "iscentrallymaintained", "ischangelog", "isdeleteable", "ishighvolume", "issecurityenabled", "isview", "loadseq", "po_window_id", "replicationtype", "sqlstatement", "updated", "updatedby") from STDIN;
230	C_SalesRegion	Sales Region	3	0	0	\N	152	\N	1999-07-04 00:00:00	0	Sales coverage region	Sales and Marketing	\N	\N	Y	Y	N	Y	N	N	N	70	\N	L	\N	2000-01-02 00:00:00	0
274	C_Campaign	Campaign	3	0	0	\N	149	\N	1999-09-21 00:00:00	0	Marketing Campaign	Sales and Marketing	\N	\N	Y	Y	N	Y	N	N	N	95	\N	L	\N	2000-01-02 00:00:00	0
275	C_Channel	Channel	3	0	0	\N	150	\N	1999-09-21 00:00:00	0	Sales Channel	Sales and Marketing	\N	\N	Y	Y	N	Y	N	N	N	90	\N	L	\N	2000-01-02 00:00:00	0
429	C_Commission	Commission	3	0	0	\N	207	\N	2001-03-11 17:34:41	0	Commission	Sales and Marketing	\N	\N	Y	Y	N	Y	N	N	N	130	\N	L	\N	2000-01-02 00:00:00	0
430	C_CommissionAmt	Commission Amount	1	0	0	\N	210	\N	2001-03-11 17:34:41	0	Generated Commission Amount 	Sales and Marketing	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
431	C_CommissionLine	Commission Line	3	0	0	\N	207	\N	2001-03-11 17:34:41	0	Commission Line	Sales and Marketing	\N	\N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2000-01-02 00:00:00	0
436	C_CommissionRun	Commission Run	1	0	0	\N	210	\N	2001-03-31 11:42:24	0	Commission Run or Process	Sales and Marketing	\N	\N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2000-01-02 00:00:00	0
437	C_CommissionDetail	Commission Detail	1	0	0	\N	210	\N	2001-04-09 14:26:51	0	Supporting information for Commission Amounts	Sales and Marketing	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
757	RV_CommissionRunDetail	Commission Run Detail	3	0	0	\N	\N	\N	2005-02-25 16:12:55	0	\N	Sales and Marketing	\N	\N	Y	Y	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:55:55	100
53337	C_Opportunity	Sales Opportunity	3	0	0	\N	53155	N	2011-09-01 21:53:00	100	\N	Sales and Marketing	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2012-06-08 15:15:29	0
53354	C_ContactActivity	Contact Activity	3	0	0	\N	53152	N	2011-10-13 14:28:49	100	Events, tasks, communications related to a contact	Sales and Marketing	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2012-06-08 15:16:36	0
\.
