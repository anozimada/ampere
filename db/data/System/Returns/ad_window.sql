copy "ad_window" ("ad_window_id", "name", "ad_client_id", "ad_color_id", "ad_image_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "isbetafunctionality", "isdefault", "issotrx", "processing", "updated", "updatedby", "windowtype", "winheight", "winwidth") from STDIN;
320	Customer RMA	0	\N	\N	0	2004-02-19 22:59:58	0	Manage Return Material Authorization	Returns	A Return Material Authorization may be required to accept returns and to create Credit Memos	Y	N	N	Y	N	2009-09-11 00:49:06	100	T	\N	\N
331	RMA Type	0	\N	\N	0	2004-05-16 20:58:04	0	Return Material Authorization Type	Returns	Maintain the types of RMA	Y	N	N	Y	N	2009-09-11 00:48:00	100	M	0	0
53097	Customer Return	0	\N	\N	0	2009-09-11 00:33:23	100	Customer Return (Receipts)	Returns	The Customer Return Window defines the receipt of product or material from a Customer Return.	Y	N	N	Y	N	2009-09-11 00:33:23	100	T	0	0
53098	Return to Vendor	0	\N	\N	0	2009-09-11 00:41:45	100	Vendor Returns	Returns	The Return to Vendor Window defines shipments made or to be made to a vendor as part of a return.	Y	N	N	N	N	2009-09-11 00:41:45	100	T	0	0
53099	Vendor RMA	0	\N	\N	0	2009-09-11 00:52:52	100	Manage Return Material Authorization	Returns	A Return Material Authorization may be required to accept returns and to create Credit Memos	Y	N	N	N	N	2009-09-11 00:52:52	100	T	\N	\N
\.
