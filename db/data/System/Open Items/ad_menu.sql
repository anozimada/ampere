copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
234	Bank Statement	W	0	\N	\N	0	\N	194	2000-12-18 23:37:53	0	Process Bank Statements	Open Items	Y	Y	N	Y	N	2000-01-02 00:00:00	0
235	Payment	W	0	\N	\N	0	\N	195	2000-12-18 23:38:23	0	Process Payments and Receipts	Open Items	Y	Y	N	Y	N	2000-01-02 00:00:00	0
236	Open Items	\N	0	\N	\N	0	\N	\N	2000-12-18 23:41:35	0	\N	Open Items	Y	N	N	N	Y	2000-01-02 00:00:00	0
243	Open Items	R	0	\N	\N	0	145	\N	2001-01-03 23:07:10	0	Open Item (Invoice) List	Open Items	Y	Y	N	Y	N	2005-02-07 21:42:07	100
244	Unreconciled Payments	R	0	\N	\N	0	146	\N	2001-01-03 23:07:42	0	Payments not reconciled with Bank Statement	Open Items	Y	Y	N	Y	N	2020-10-16 23:49:22	100
245	Payment Allocation	X	0	104	\N	0	\N	\N	2001-01-09 16:48:51	0	Allocate invoices and payments	Open Items	Y	Y	N	Y	N	2000-01-02 00:00:00	0
246	Allocation	R	0	\N	\N	0	148	\N	2001-01-09 22:29:01	0	Payment - Invoice - Allocation	Open Items	N	Y	N	Y	N	2022-02-17 22:30:34	100
251	View Allocation	W	0	\N	\N	0	\N	205	2001-01-24 16:16:46	0	View and Reverse Allocations 	Open Items	Y	Y	N	Y	N	2000-01-02 00:00:00	0
255	Payment Selection	W	0	\N	\N	0	\N	206	2001-02-15 17:18:17	0	Select Invoices for Payment	Open Items	Y	Y	N	N	N	2000-01-02 00:00:00	0
286	Payment Selection (manual)	X	0	107	\N	0	\N	\N	2001-07-26 18:34:11	0	Manual Payment Selection	Open Items	Y	Y	N	N	N	2000-01-02 00:00:00	0
287	Payment Print/Export	X	0	106	\N	0	\N	\N	2001-07-26 18:34:40	0	Print or export your payments	Open Items	Y	Y	N	N	N	2000-01-02 00:00:00	0
304	Receivables Write-Off	P	0	\N	\N	0	171	\N	2001-11-14 18:10:47	0	Write off open receivables	Open Items	N	Y	N	Y	N	2022-02-28 00:38:25	100
413	Aging	R	0	\N	\N	0	238	\N	2003-12-05 22:37:19	0	Aging Report	Open Items	N	Y	N	N	N	2022-02-27 23:47:48	100
432	Invoice Tax	R	0	\N	\N	0	251	\N	2004-01-09 14:36:52	0	Invoice Tax Reconciliation	Open Items	Y	Y	N	N	N	2005-11-21 09:30:33	100
438	Payment Batch	W	0	\N	\N	0	\N	303	2004-01-29 14:40:34	0	Process Payment Patches for EFT	Open Items	Y	Y	N	N	N	2000-01-02 00:00:00	0
462	Debt Collection Run	W	0	\N	\N	0	\N	321	2004-03-06 00:59:09	0	Manage Debt Collection cycle	Open Items	Y	Y	N	Y	N	2022-02-28 00:39:16	100
496	Reset Allocation	P	0	\N	\N	0	303	\N	2004-08-15 21:32:07	0	Reset (delete) allocation of invoices to payments	Open Items	Y	Y	N	Y	N	2005-02-21 22:24:11	100
497	Auto Allocation	P	0	\N	\N	0	302	\N	2004-08-15 21:32:46	0	Automatic allocation of invoices to payments	Open Items	N	Y	N	Y	N	2022-02-17 22:31:04	100
505	Print Statements	P	0	\N	\N	0	312	\N	2004-11-30 01:39:09	0	Print Statement/Collection letters to paper or send PDF	Open Items	Y	Y	N	Y	N	2022-02-17 22:46:14	100
509	Unallocated Invoices	R	0	\N	\N	0	316	\N	2005-01-28 00:01:42	100	Invoices not allocated to Payments	Open Items	Y	Y	N	Y	N	2022-02-17 22:30:45	100
510	Unallocated Payments	R	0	\N	\N	0	317	\N	2005-01-28 00:02:12	100	Payments not allocated to Invoices	Open Items	Y	Y	N	Y	N	2022-02-17 22:30:54	100
511	Payment Details	R	0	\N	\N	0	318	\N	2005-02-07 22:23:48	100	Payment Detail Report	Open Items	Y	Y	N	Y	N	2005-02-07 22:23:48	100
538	Unrealised Gain/Loss	R	0	\N	\N	0	326	\N	2005-05-30 15:18:06	100	Unrealised Gain & Loss on Invoices Report	Open Items	Y	Y	N	N	N	2022-02-17 22:28:18	100
53190	Bank Transfer	P	0	\N	\N	0	53153	\N	2008-09-04 19:15:08	100	Record transfer of funds between the selected bank accounts	Open Items	Y	Y	N	N	N	2022-02-28 00:43:09	100
53313	Cash Plan	W	0	\N	\N	0	\N	53134	2010-12-08 21:24:47	100	\N	Open Items	N	Y	N	N	N	2019-12-05 14:00:01	100
53314	Cash Flow Report	R	0	\N	\N	0	53248	\N	2010-12-08 21:24:49	100	\N	Open Items	N	Y	N	N	N	2019-12-05 14:00:03	100
53349	Aging with payments	R	0	\N	\N	0	53260	\N	2011-07-01 16:54:19	100	Aging Report with payments	Open Items	Y	Y	N	N	N	2011-07-07 14:22:53	100
53387	Invoice Tax Accounted	R	0	\N	\N	0	53277	\N	2011-10-31 16:50:27	100	Invoice Tax Reconciliation in accounting currecny	Open Items	Y	Y	N	N	N	2011-10-31 16:50:27	100
53688	AP/AR Restatement	R	0	\N	\N	0	53532	\N	2013-11-20 19:24:15	100	AP/AR Restatement	Open Items	Y	Y	N	N	N	2013-11-20 19:24:15	100
53887	Print Remittance Advice	P	0	\N	\N	0	53712	\N	2014-11-07 17:58:18	100	Print  Remittance Advices to paper or send PDF	Open Items	Y	Y	N	N	N	2022-02-28 00:39:50	100
\.
