copy "ad_table" ("ad_table_id", "tablename", "name", "accesslevel", "ad_client_id", "ad_org_id", "ad_val_rule_id", "ad_window_id", "copycolumnsfromtable", "created", "createdby", "description", "entitytype", "help", "importtable", "isactive", "iscentrallymaintained", "ischangelog", "isdeleteable", "ishighvolume", "issecurityenabled", "isview", "loadseq", "po_window_id", "replicationtype", "sqlstatement", "updated", "updatedby") from STDIN;
335	C_Payment	Payment	1	0	0	\N	195	\N	2000-01-24 17:03:25	0	Payment identifier	Open Items	\N	\N	Y	Y	N	Y	N	N	N	105	\N	L	\N	2000-01-02 00:00:00	0
392	C_BankStatement	Bank Statement	3	0	0	\N	194	\N	2000-12-17 16:19:52	0	Bank Statement of account	Open Items	\N	\N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2000-01-02 00:00:00	0
393	C_BankStatementLine	Bank statement line	3	0	0	\N	194	\N	2000-12-17 16:19:52	0	Line on a statement from this Bank	Open Items	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
411	C_PaymentBatch	Payment Batch	1	0	0	\N	303	\N	2000-12-22 22:20:19	0	Payment batch for EFT	Open Items	\N	\N	Y	Y	N	Y	N	N	N	100	\N	L	\N	2000-01-02 00:00:00	0
426	C_PaySelection	Payment Selection	1	0	0	\N	206	\N	2001-02-15 16:57:30	0	Payment Selection	Open Items	\N	\N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2005-02-12 02:30:12	100
427	C_PaySelectionLine	Payment Selection Line	1	0	0	\N	206	\N	2001-02-15 16:57:30	0	Payment Selection Line	Open Items	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2005-02-12 02:30:30	100
524	C_DunningRunLine	Dunning Run Line	3	0	0	\N	321	\N	2002-08-29 20:40:53	0	Dunning Run Line	Open Items	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
525	C_PaySelectionCheck	Pay Selection Cheque	1	0	0	\N	206	\N	2002-08-29 20:40:53	0	Payment Selection Cheque	Open Items	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
526	C_DunningRun	Dunning Run	3	0	0	\N	321	\N	2002-08-29 20:40:53	0	Dunning Run	Open Items	\N	\N	Y	Y	N	Y	N	N	N	135	\N	L	\N	2000-01-02 00:00:00	0
527	C_DunningRunEntry	Dunning Run Entry	3	0	0	\N	321	\N	2002-08-29 20:40:53	0	Dunning Run Entry	Open Items	\N	\N	Y	Y	N	Y	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
554	C_Payment_v	Payment_v	3	0	0	\N	\N	\N	2003-05-04 01:28:27	0	\N	Open Items	\N	\N	Y	Y	N	N	N	N	Y	150	\N	L	\N	2005-02-25 16:42:56	100
631	T_Aging	T_Aging	3	0	0	\N	\N	\N	2003-12-05 13:37:53	0	\N	Open Items	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2005-09-03 12:06:56	100
654	RV_C_InvoiceTax	InvoiceTax	3	0	0	\N	\N	\N	2004-01-09 13:43:08	0	\N	Open Items	\N	\N	Y	Y	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:54:01	100
736	RV_Allocation	Allocation	3	0	0	\N	\N	\N	2004-06-11 20:03:27	0	\N	Open Items	\N	\N	Y	Y	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:53:13	100
804	T_InvoiceGL_v	T_InvoiceGL_v	3	0	0	\N	\N	\N	2005-05-30 13:30:44	0	Gain/Loss	Open Items	\N	\N	Y	Y	N	N	N	N	Y	150	\N	L	\N	2005-05-31 13:58:22	0
812	C_PaymentAllocate	Allocate Payment	1	0	0	\N	195	\N	2005-09-03 08:25:28	100	Allocate Payment to Invoices	Open Items	\N	N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2005-11-09 14:26:29	100
53297	C_CashPlan	Cash Plan	1	0	0	\N	53134	\N	2010-12-08 21:22:02	100	Cash Plan	Open Items	\N	N	Y	Y	Y	Y	N	N	N	\N	\N	L	\N	2010-12-08 21:22:02	100
53298	C_CashPlanLine	Cash Plan Line	1	0	0	\N	53134	\N	2010-12-08 21:23:00	100	Cash Plan Line	Open Items	\N	N	Y	Y	Y	Y	N	N	N	\N	\N	L	\N	2010-12-08 21:23:00	100
53299	T_CashFlow	T_CashFlow_ID	3	0	0	\N	\N	\N	2010-12-08 21:23:41	100	\N	Open Items	\N	\N	Y	Y	N	Y	N	N	N	\N	\N	L	\N	2010-12-08 21:23:44	100
53323	T_CombinedAging	T_CombinedAging	3	0	0	\N	\N	\N	2011-07-01 16:39:20	100	\N	Open Items	\N	\N	Y	Y	N	Y	N	N	N	145	\N	L	\N	2011-07-01 16:39:20	100
53669	T_CombinedArApRevalue	T_CombinedArApRevalue	3	0	0	\N	\N	N	2013-11-20 17:08:00	100	\N	Open Items	\N	N	Y	Y	N	Y	N	N	N	0	\N	L	\N	2013-11-20 17:08:00	100
\.
