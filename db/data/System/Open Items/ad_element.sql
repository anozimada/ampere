copy "ad_element" ("ad_element_id", "columnname", "ad_client_id", "ad_org_id", "ad_reference_id", "ad_reference_value_id", "created", "createdby", "description", "entitytype", "fieldlength", "help", "isactive", "name", "po_description", "po_help", "po_name", "po_printname", "printname", "updated", "updatedby") from STDIN;
1136	TrxAmt	0	0	12	\N	2000-01-24 18:04:26	0	Amount of a transaction	Open Items	22	The Transaction Amount indicates the amount for a single transaction.	Y	Transaction Amount	\N	\N	\N	\N	Transaction Amt	2000-01-02 00:00:00	0
1378	BeginningBalance	0	0	12	\N	2000-12-17 16:23:12	0	Balance prior to any transactions	Open Items	22	The Beginning Balance is the balance prior to making any adjustments for payments or disbursements.	Y	Beginning Balance	\N	\N	\N	\N	Beginning Balance	2016-04-11 16:31:41	0
1381	C_BankStatement_ID	0	0	13	\N	2000-12-17 16:23:12	0	Bank Statement of account	Open Items	22	The Bank Statement identifies a unique Bank Statement for a defined time period.  The statement defines all transactions that occurred	Y	Bank Statement	\N	\N	\N	\N	Bank Statement	2000-01-02 00:00:00	0
1382	C_BankStatementLine_ID	0	0	13	\N	2000-12-17 16:23:12	0	Line on a statement from this Bank	Open Items	22	The Bank Statement Line identifies a unique transaction (Payment, Withdrawal, Charge) for the defined time period at this Bank.	Y	Bank statement line	\N	\N	\N	\N	Bank statement line	2000-01-02 00:00:00	0
1396	EndingBalance	0	0	12	\N	2000-12-17 16:23:13	0	Ending  or closing balance	Open Items	22	The Ending Balance is the result of adjusting the Beginning Balance by any payments or disbursements.	Y	Ending balance	\N	\N	\N	\N	Ending balance	2016-04-11 16:31:44	0
1434	StatementDate	0	0	15	\N	2000-12-17 16:23:13	0	Date of the statement	Open Items	7	The Statement Date field defines the date of the statement.	Y	Statement date	\N	\N	\N	\N	Statement date	2016-04-11 16:31:47	0
1435	StatementDifference	0	0	12	\N	2000-12-17 16:23:13	0	Difference between statement ending balance and actual ending balance	Open Items	22	The Statement Difference reflects the difference between the Statement Ending Balance and the Actual Ending Balance.	Y	Statement difference	\N	\N	\N	\N	Statement difference	2016-04-11 16:31:47	0
1457	InterestAmt	0	0	12	\N	2000-12-18 21:43:54	0	Interest Amount	Open Items	22	The Interest Amount indicates any interest charged or received on a Bank Statement.	Y	Interest Amount	\N	\N	\N	\N	Interest	2000-01-02 00:00:00	0
1462	C_Cash_ID	0	0	13	\N	2000-12-22 22:27:31	0	Cash Journal	Open Items	22	The Cash Journal uniquely identifies a Cash Journal.  The Cash Journal will record transactions for the cash bank account	Y	Cash Journal	\N	\N	\N	\N	Cash Journal	2000-01-02 00:00:00	0
1466	CashType	0	0	17	217	2000-12-22 22:27:31	0	Source of Cash	Open Items	1	The Cash Type indicates the source for this Cash Journal Line.	Y	Cash Type	\N	\N	\N	\N	Cash Type	2000-01-02 00:00:00	0
1476	IsReversal	0	0	20	\N	2000-12-22 22:27:31	0	This is a reversing transaction	Open Items	1	The Reversal check box indicates if this is a reversal of a prior transaction.	Y	Reversal	\N	\N	\N	\N	Reversal	2000-01-02 00:00:00	0
1479	ProcessingDate	0	0	15	\N	2000-12-22 22:27:31	0	\N	Open Items	7	\N	Y	Processing date	\N	\N	\N	\N	Processing date	2000-01-02 00:00:00	0
1482	StmtAmt	0	0	12	\N	2000-12-22 22:27:31	0	Statement Amount	Open Items	22	The Statement Amount indicates the amount of a single statement line.	Y	Statement amount	\N	\N	\N	\N	Statement Amt	2000-01-02 00:00:00	0
1487	ValutaDate	0	0	15	\N	2000-12-22 22:27:31	0	Date when money is available	Open Items	7	The Effective Date indicates the date that money is available from the bank.	Y	Effective date	\N	\N	\N	\N	Effective date	2000-01-02 00:00:00	0
1532	C_PaySelection_ID	0	0	13	\N	2001-02-15 16:59:42	0	Payment Selection	Open Items	22	The Payment Selection identifies a unique Payment	Y	Payment Selection	\N	\N	\N	\N	Payment Selection	2008-03-23 20:56:54	100
1533	C_PaySelectionLine_ID	0	0	13	\N	2001-02-15 16:59:42	0	Payment Selection Line	Open Items	22	The Payment Selection Line identifies a unique line in a payment	Y	Payment Selection Line	\N	\N	\N	\N	Payment Selection Line	2000-01-02 00:00:00	0
1538	PayDate	0	0	15	\N	2001-02-15 16:59:42	0	Date Payment made	Open Items	7	The Payment Date indicates the date the payment was made.	Y	Payment date	\N	\N	\N	\N	Payment date	2000-01-02 00:00:00	0
1635	DifferenceAmt	0	0	12	\N	2001-07-31 16:47:45	0	Difference Amount	Open Items	22	\N	Y	Difference	\N	\N	\N	\N	Difference	2000-01-02 00:00:00	0
1673	OnlyDiscount	0	0	\N	\N	2001-11-18 16:27:47	0	Include only invoices where we would get payment discount	Open Items	\N	\N	Y	Only Discount	\N	\N	\N	\N	Only Discount	2000-01-02 00:00:00	0
1674	OnlyDue	0	0	\N	\N	2001-11-18 16:27:47	0	Include only due invoices	Open Items	\N	\N	Y	Only Due	\N	\N	\N	\N	Only Due	2000-01-02 00:00:00	0
1879	C_DunningRun_ID	0	0	13	\N	2002-08-29 20:46:51	0	Dunning Run	Open Items	22	\N	Y	Dunning Run	\N	\N	\N	\N	Dunning Run	2000-01-02 00:00:00	0
1880	C_DunningRunEntry_ID	0	0	13	\N	2002-08-29 20:46:52	0	Dunning Run Entry	Open Items	22	\N	Y	Dunning Run Entry	\N	\N	\N	\N	Dunning Run Entry	2000-01-02 00:00:00	0
1881	C_DunningRunLine_ID	0	0	13	\N	2002-08-29 20:46:52	0	Dunning Run Line	Open Items	22	\N	Y	Dunning Run Line	\N	\N	\N	\N	Dunning Run Line	2000-01-02 00:00:00	0
1882	C_PaySelectionCheck_ID	0	0	13	\N	2002-08-29 20:46:52	0	Payment Selection Cheque	Open Items	22	\N	Y	Pay Selection Cheque	\N	\N	\N	\N	Pay Selection Cheque	2016-04-11 16:07:04	0
1883	DunningDate	0	0	15	\N	2002-08-29 20:46:52	0	Date of Statement	Open Items	7	\N	Y	Statement Date	\N	\N	\N	\N	Statement Date	2016-04-11 16:07:04	0
2107	IsDelayedCapture	0	0	20	\N	2003-06-07 19:49:51	0	Charge after Shipment	Open Items	1	Delayed Capture is required, if you ship products.  The first credit card transaction is the Authorization, the second is the actual transaction after the shipment of the product.	Y	Delayed Capture	\N	\N	\N	\N	Delayed Capture	2000-01-02 00:00:00	0
2112	Memo	0	0	10	\N	2003-06-07 19:49:51	0	Memo Text	Open Items	255	\N	Y	Memo	\N	\N	\N	\N	Memo	2000-01-02 00:00:00	0
2119	R_AuthCode_DC	0	0	10	\N	2003-06-07 19:49:51	0	Authorisation Code Delayed Capture returned	Open Items	20	The Authorisation Code indicates the code returned from the electronic transmission.	Y	Authorisation Code (DC)	\N	\N	\N	\N	Authorisation Code (DC)	2016-04-11 16:07:00	0
2120	R_CVV2Match	0	0	20	\N	2003-06-07 19:49:51	0	Credit Card Verification Code Match	Open Items	1	The Credit Card Verification Code was matched	Y	CVV Match	\N	\N	\N	\N	CVV Match	2000-01-02 00:00:00	0
2121	R_PnRef_DC	0	0	10	\N	2003-06-07 19:49:51	0	Payment Reference Delayed Capture	Open Items	20	The Payment Reference indicates the reference returned from the Credit Card Company for a payment	Y	Reference (DC)	\N	\N	\N	\N	Reference (DC)	2000-01-02 00:00:00	0
2125	Swipe	0	0	10	\N	2003-06-07 19:49:51	0	Track 1 and 2 of the Credit Card	Open Items	80	Swiped information for Credit Card Presence Transactions	Y	Swipe	\N	\N	\N	\N	Swipe	2000-01-02 00:00:00	0
2229	CreatePayment	0	0	28	\N	2003-11-20 15:22:16	0	\N	Open Items	1	\N	Y	Create Payment	\N	\N	\N	\N	Create Payment	2000-01-02 00:00:00	0
2231	EftMemo	0	0	10	\N	2003-11-20 15:22:16	0	Electronic Funds Transfer Memo	Open Items	2000	Information from EFT media	Y	EFT Memo	\N	\N	\N	\N	EFT Memo	2000-01-02 00:00:00	0
2232	EftPayee	0	0	10	\N	2003-11-20 15:22:16	0	Electronic Funds Transfer Payee information	Open Items	255	Information from EFT media	Y	EFT Payee	\N	\N	\N	\N	EFT Payee	2000-01-02 00:00:00	0
2233	EftPayeeAccount	0	0	10	\N	2003-11-20 15:22:16	0	Electronic Funds Transfer Payee Account Information	Open Items	40	Information from EFT media	Y	EFT Payee Account	\N	\N	\N	\N	EFT Payee Account	2010-01-13 10:38:15	0
2234	EftReference	0	0	10	\N	2003-11-20 15:22:16	0	Electronic Funds Transfer Reference	Open Items	60	Information from EFT media	Y	EFT Reference	\N	\N	\N	\N	EFT Reference	2000-01-02 00:00:00	0
2235	EftTrxID	0	0	10	\N	2003-11-20 15:22:16	0	Electronic Funds Transfer Transaction ID	Open Items	40	Information from EFT media	Y	EFT Trx ID	\N	\N	\N	\N	EFT Trx ID	2000-01-02 00:00:00	0
2236	EftTrxType	0	0	10	\N	2003-11-20 15:22:16	0	Electronic Funds Transfer Transaction Type	Open Items	20	Information from EFT media	Y	EFT Trx Type	\N	\N	\N	\N	EFT Trx Type	2000-01-02 00:00:00	0
2241	Due0	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Due Today	\N	\N	\N	\N	Due Today	2000-01-02 00:00:00	0
2242	Due0_30	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Due Today-30	\N	\N	\N	\N	Due Today-30	2000-01-02 00:00:00	0
2243	Due0_7	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Due Today-7	\N	\N	\N	\N	Due Today-7	2000-01-02 00:00:00	0
2244	Due1_7	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Due 1-7	\N	\N	\N	\N	Due 1-7	2000-01-02 00:00:00	0
2245	Due31_60	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Due 31-60	\N	\N	\N	\N	Due 31-60	2000-01-02 00:00:00	0
2246	Due31_Plus	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Due > 31	\N	\N	\N	\N	Due > 31	2000-01-02 00:00:00	0
2247	Due61_90	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Due 61-90	\N	\N	\N	\N	Due 61-90	2000-01-02 00:00:00	0
2248	Due61_Plus	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Due > 61	\N	\N	\N	\N	Due > 61	2000-01-02 00:00:00	0
2249	Due8_30	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Due 8-30	\N	\N	\N	\N	Due 8-30	2000-01-02 00:00:00	0
2250	Due91_Plus	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Due > 91	\N	\N	\N	\N	Due > 91	2000-01-02 00:00:00	0
2251	PastDue1_30	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Past Due 1-30	\N	\N	\N	\N	Past Due 1-30	2000-01-02 00:00:00	0
2252	PastDue1_7	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Past Due 1-7	\N	\N	\N	\N	Past Due 1-7	2000-01-02 00:00:00	0
2253	PastDue31_60	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Past Due 31-60	\N	\N	\N	\N	Past Due 31-60	2000-01-02 00:00:00	0
2254	PastDue31_Plus	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Past Due > 31	\N	\N	\N	\N	Past Due > 31	2000-01-02 00:00:00	0
2255	PastDue61_90	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Past Due 61-90	\N	\N	\N	\N	Past Due 61-90	2000-01-02 00:00:00	0
2256	PastDue61_Plus	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Past Due > 61	\N	\N	\N	\N	Past Due > 61	2000-01-02 00:00:00	0
2257	PastDue8_30	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Past Due 8-30	\N	\N	\N	\N	Past Due 8-30	2000-01-02 00:00:00	0
2258	PastDue91_Plus	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Past Due > 91	\N	\N	\N	\N	Past Due > 91	2000-01-02 00:00:00	0
2259	PastDueAmt	0	0	12	\N	2003-12-05 13:38:20	0	\N	Open Items	22	\N	Y	Past Due	\N	\N	\N	\N	Past Due	2000-01-02 00:00:00	0
2260	IsListInvoices	0	0	20	\N	2003-12-05 14:31:40	0	Include List of Invoices	Open Items	1	\N	Y	List Invoices	\N	\N	\N	\N	List Invoices	2000-01-02 00:00:00	0
2287	EftAmt	0	0	12	\N	2003-12-25 14:02:45	0	Electronic Funds Transfer Amount	Open Items	22	\N	Y	EFT Amount	\N	\N	\N	\N	EFT Amount	2000-01-02 00:00:00	0
2289	EftCheckNo	0	0	10	\N	2003-12-25 14:02:45	0	Electronic Funds Transfer Check No	Open Items	20	Information from EFT media	Y	EFT Check No	\N	\N	\N	\N	EFT Check No	2000-01-02 00:00:00	0
2290	EftCurrency	0	0	10	\N	2003-12-25 14:02:45	0	Electronic Funds Transfer Currency	Open Items	20	Information from EFT media	Y	EFT Currency	\N	\N	\N	\N	EFT Currency	2000-01-02 00:00:00	0
2291	EftStatementDate	0	0	15	\N	2003-12-25 14:02:45	0	Electronic Funds Transfer Statement Date	Open Items	7	Information from EFT media	Y	EFT Statement Date	\N	\N	\N	\N	EFT Statement Date	2000-01-02 00:00:00	0
2292	EftStatementLineDate	0	0	15	\N	2003-12-25 14:02:45	0	Electronic Funds Transfer Statement Line Date	Open Items	7	Information from EFT media	Y	EFT Statement Line Date	\N	\N	\N	\N	EFT Statement Line Date	2000-01-02 00:00:00	0
2293	EftStatementReference	0	0	10	\N	2003-12-25 14:02:45	0	Electronic Funds Transfer Statement Reference	Open Items	60	Information from EFT media	Y	EFT Statement Reference	\N	\N	\N	\N	EFT Statement Reference	2000-01-02 00:00:00	0
2294	EftValutaDate	0	0	15	\N	2003-12-25 14:02:45	0	Electronic Funds Transfer Valuta (effective) Date	Open Items	7	Information from EFT media	Y	EFT Effective Date	\N	\N	\N	\N	EFT Effective Date	2000-01-02 00:00:00	0
2300	StatementLineDate	0	0	15	\N	2003-12-25 14:02:45	0	Date of the Statement Line	Open Items	7	\N	Y	Statement Line Date	\N	\N	\N	\N	Statement Line Date	2000-01-02 00:00:00	0
2341	TaxLineTotal	0	0	12	\N	2004-01-09 14:06:11	0	Tax Line Total Amount	Open Items	22	\N	Y	Tax Line Total	\N	\N	\N	\N	Line Total	2000-01-02 00:00:00	0
2348	MatchStatement	0	0	28	\N	2004-01-25 13:10:04	0	\N	Open Items	1	\N	Y	Match Statement	\N	\N	\N	\N	Match Statement	2000-01-02 00:00:00	0
2564	SendIt	0	0	28	\N	2004-07-04 00:40:28	0	\N	Open Items	1	\N	Y	Send	\N	\N	\N	\N	Send	2000-01-02 00:00:00	0
2565	TimesDunned	0	0	11	\N	2004-07-04 00:40:28	0	Number of times dunned previously	Open Items	22	\N	Y	Times Dunned	\N	\N	\N	\N	# Dunned	2000-01-02 00:00:00	0
2572	IncludeInDispute	0	0	\N	\N	2004-07-04 18:26:14	0	Include disputed Invoices	Open Items	\N	\N	Y	Include Disputed	\N	\N	\N	\N	Include Disputed	2000-01-02 00:00:00	0
2661	APAR	0	0	17	332	2004-11-30 01:52:41	0	Include Receivables and/or Payables transactions	Open Items	1	\N	Y	AP - AR	\N	\N	\N	\N	AP - AR	2000-01-02 00:00:00	0
2753	Ref_Payment_ID	0	0	18	343	2005-05-08 21:05:45	100	\N	Open Items	10	\N	Y	Referenced Payment	\N	\N	\N	\N	Ref Payment	2005-05-08 21:10:58	100
2798	IsOnlyIfBPBalance	0	0	\N	\N	2005-05-22 02:14:02	100	Include only if Business Partner has outstanding Balance	Open Items	\N	\N	Y	Only If BP has Balance	\N	\N	\N	\N	Only If BP has Balance	2005-05-22 02:14:02	100
2800	AmtRevalCr	0	0	12	\N	2005-05-30 13:31:57	0	Restated Cr Amount	Open Items	22	\N	Y	Restated Amount Cr	\N	\N	\N	\N	Restated Amt Cr	2016-04-11 16:07:04	0
2801	AmtRevalCrDiff	0	0	12	\N	2005-05-30 13:31:57	0	Restated Cr Amount Difference	Open Items	22	\N	Y	Restated Difference Cr	\N	\N	\N	\N	Difference Cr	2016-04-11 16:07:04	0
2802	AmtRevalDr	0	0	12	\N	2005-05-30 13:31:57	0	Restated Dr Amount	Open Items	22	\N	Y	Restated Amount Dr	\N	\N	\N	\N	Restated Amt Dr	2016-04-11 16:07:04	0
2803	AmtRevalDrDiff	0	0	12	\N	2005-05-30 13:31:57	0	Restated Dr Amount Difference	Open Items	22	\N	Y	Restated Difference Dr	\N	\N	\N	\N	Difference Dr	2016-04-11 16:07:04	0
2804	C_ConversionTypeReval_ID	0	0	18	352	2005-05-30 13:31:57	0	Revaluation Currency Conversion Type	Open Items	22	\N	Y	Revaluation Conversion Type	\N	\N	\N	\N	Reval Conversion Type	2005-05-30 14:37:58	100
2805	DateReval	0	0	15	\N	2005-05-30 13:31:57	0	Date of Revaluation	Open Items	7	\N	Y	Revaluation Date	\N	\N	\N	\N	Reval Date	2005-05-30 14:43:45	100
2832	C_PaymentAllocate_ID	0	0	13	\N	2005-09-03 08:25:35	100	Allocate Payment to Invoices	Open Items	10	You can directly allocate payments to invoices when creating the Payment. \nNote that you can over- or under-allocate the payment.  When processing the payment, the allocation is created.	Y	Allocate Payment	\N	\N	\N	\N	Allocate Payment	2005-09-03 08:33:43	100
2833	InvoiceAmt	0	0	12	\N	2005-09-03 08:57:39	100	\N	Open Items	22	\N	Y	Invoice Amt	\N	\N	\N	\N	Invoice Amt	2005-09-03 08:58:17	100
2834	RemainingAmt	0	0	12	\N	2005-09-03 09:25:39	100	Remaining Amount	Open Items	22	\N	Y	Remaining Amt	\N	\N	\N	\N	Remaining Amt	2016-04-11 16:33:05	0
2866	MatchRequirement	0	0	\N	\N	2005-10-19 21:12:45	100	Matching Requirement for Invoice	Open Items	\N	\N	Y	Match Requirement	\N	\N	\N	\N	Match	2005-10-19 21:35:01	100
2896	C_DocTypeReval_ID	0	0	18	170	2005-12-18 15:37:15	100	Document Type for Revaluation Journal	Open Items	10	\N	Y	Revaluation Document Type	\N	\N	\N	\N	Doc Type Reval	2005-12-18 15:45:01	100
2897	IsAllCurrencies	0	0	20	\N	2005-12-19 18:06:47	100	Report not just foreign currency Invoices	Open Items	1	\N	Y	Include All Currencies	\N	\N	\N	\N	All Currencies	2005-12-19 18:06:47	100
3083	AmtAcctOpenDr	0	0	12	\N	2006-10-25 00:00:00	0	Open Debit in document currency & rate	Open Items	22	\N	Y	Acct Open Dr	\N	\N	\N	\N	Acct Open Dr	2010-01-13 10:38:15	0
3084	AmtAcctOpenCr	0	0	12	\N	2006-10-25 00:00:00	0	Open Credit in document currency & rate	Open Items	22	\N	Y	Acct Open Cr	\N	\N	\N	\N	Acct Open Cr	2010-01-13 10:38:15	0
3085	AmtAcctOpenBalance	0	0	12	\N	2006-10-25 00:00:00	0	Open Balance in document currency & rate	Open Items	22	\N	Y	Acct Open Balance	\N	\N	\N	\N	Acct Open Balance	2010-01-13 10:38:15	0
53224	PrintUnprocessedOnly	0	0	\N	\N	2007-09-28 00:00:00	100	\N	Open Items	\N	\N	Y	Print Unprocessed Entries Only	\N	\N	\N	\N	Print Unprocessed Entries Only	2007-09-28 00:00:00	100
53334	IsGeneratedDraft	0	0	20	\N	2008-01-24 17:45:27	100	\N	Open Items	1	\N	Y	Generated Draft	\N	\N	\N	\N	Generated Draft	2008-01-24 17:45:27	100
54099	AllAllocations	0	0	\N	\N	2009-12-14 17:43:06	100	\N	Open Items	\N	\N	Y	All Allocations	\N	\N	\N	\N	All Allocations	2009-12-14 17:43:06	100
54393	C_CashPlan_ID	0	0	13	\N	2010-12-08 21:22:04	100	\N	Open Items	22	\N	Y	Cash Plan	\N	\N	\N	\N	Cash Plan	2010-12-08 21:22:04	100
54394	CashFlowType	0	0	17	53385	2010-12-08 21:22:08	100	\N	Open Items	1	\N	Y	Cash Flow Type	\N	\N	\N	\N	Cash Flow Type	2010-12-08 21:22:08	100
54395	GeneratePeriodic	0	0	28	\N	2010-12-08 21:22:21	100	\N	Open Items	1	\N	Y	Generate Periodic Plan	\N	\N	\N	\N	Generate Periodic Plan	2010-12-08 21:22:21	100
54398	T_CashFlow_ID	0	0	13	\N	2010-12-08 21:23:44	100	\N	Open Items	10	\N	Y	T_CashFlow_ID	\N	\N	\N	\N	T_CashFlow_ID	2010-12-08 21:23:44	100
54399	CashFlowSource	0	0	17	53387	2010-12-08 21:23:49	100	\N	Open Items	1	\N	Y	Cash Flow Source	\N	\N	\N	\N	Cash Flow Source	2010-12-08 21:23:49	100
55220	IsIncludePayments	0	0	20	\N	2011-07-01 16:44:04	100	Include payments in the aging report	Open Items	1	\N	Y	Include Payments	\N	\N	\N	\N	Include Payments	2011-07-01 16:44:04	100
55506	IncludeFutureDocs	0	0	\N	\N	2012-06-15 14:21:58	100	Include payments and invoices later than the dunning date	Open Items	\N	\N	Y	Include Future Documents	\N	\N	\N	\N	Include Future Documents	2012-06-15 14:21:58	100
56613	AmtAcctOpenPosted	0	0	\N	\N	2013-11-21 10:59:28	100	\N	Open Items	\N	\N	Y	Open Posted Amount	\N	\N	\N	\N	Open Posted Amount	2013-10-18 16:42:39	100
56614	AmtAcctOpenSource	0	0	\N	\N	2013-11-21 11:01:04	100	\N	Open Items	\N	\N	Y	Open Source Amount	\N	\N	\N	\N	Open Source Amount	2013-11-21 11:01:04	100
56615	AmtRevalDiff	0	0	\N	\N	2013-11-21 11:01:50	100	Revaluation difference	Open Items	\N	\N	Y	Revalue Diff	\N	\N	\N	\N	Revalue Diff	2013-11-21 11:01:50	100
57093	Delete	0	0	\N	\N	2014-03-19 17:13:03	100	Delete the current item	Open Items	\N	\N	Y	Delete	\N	\N	\N	\N	Delete	2016-04-11 16:40:04	0
57538	IsDebtorStatement	0	0	\N	\N	2014-11-07 17:40:12	100	\N	Open Items	\N	\N	Y	IsDebtorStatement	\N	\N	\N	\N	IsDebtorStatement	2014-11-07 17:40:12	100
57541	Remittance_Printed	0	0	\N	\N	2014-11-07 18:07:41	100	\N	Open Items	\N	\N	Y	Remittance_Printed	\N	\N	\N	\N	Remittance_Printed	2014-11-07 18:07:41	100
58027	CreateAllocation	0	0	\N	\N	2015-07-17 17:50:02	100	\N	Open Items	\N	\N	Y	Create Allocation	\N	\N	\N	\N	Create Allocation	2015-07-17 17:50:02	100
\.
