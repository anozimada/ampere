copy "ad_ref_list" ("ad_ref_list_id", "name", "ad_client_id", "ad_org_id", "ad_reference_id", "created", "createdby", "description", "entitytype", "isactive", "updated", "updatedby", "validfrom", "validto", "value") from STDIN;
411	Bank Account Transfer	0	0	217	2000-12-22 23:05:38	0	\N	Open Items	Y	2000-01-02 00:00:00	0	\N	\N	T
412	Invoice	0	0	217	2000-12-22 23:06:14	0	\N	Open Items	Y	2000-01-02 00:00:00	0	\N	\N	I
413	General Expense	0	0	217	2000-12-22 23:06:47	0	\N	Open Items	Y	2000-01-02 00:00:00	0	\N	\N	E
414	General Receipts	0	0	217	2000-12-22 23:07:25	0	\N	Open Items	Y	2000-01-02 00:00:00	0	\N	\N	R
415	Charge	0	0	217	2000-12-22 23:07:49	0	\N	Open Items	Y	2000-01-02 00:00:00	0	\N	\N	C
416	Difference	0	0	217	2000-12-22 23:08:15	0	\N	Open Items	Y	2000-01-02 00:00:00	0	\N	\N	D
699	Receivables & Payables	0	0	332	2004-11-29 23:34:41	0	\N	Open Items	Y	2000-01-02 00:00:00	0	\N	\N	A
700	Receivables only	0	0	332	2004-11-29 23:35:06	0	\N	Open Items	Y	2000-01-02 00:00:00	0	\N	\N	R
701	Payables only	0	0	332	2004-11-29 23:35:20	0	\N	Open Items	Y	2000-01-02 00:00:00	0	\N	\N	P
787	None	0	0	360	2005-10-19 21:14:06	100	\N	Open Items	Y	2005-10-19 21:14:06	100	\N	\N	N
788	Receipt	0	0	360	2005-10-19 21:14:37	100	\N	Open Items	Y	2005-10-19 21:14:37	100	\N	\N	R
789	Purchase Order	0	0	360	2005-10-19 21:14:53	100	\N	Open Items	Y	2005-10-19 21:14:53	100	\N	\N	P
790	Purchase Order and Receipt	0	0	360	2005-10-19 21:15:15	100	\N	Open Items	Y	2005-10-19 21:15:15	100	\N	\N	B
53663	Financing	0	0	53385	2010-12-08 21:22:09	100	\N	Open Items	Y	2010-12-08 21:22:09	100	\N	\N	F
53664	Investment	0	0	53385	2010-12-08 21:22:10	100	\N	Open Items	Y	2010-12-08 21:22:10	100	\N	\N	I
53665	Operational	0	0	53385	2010-12-08 21:22:10	100	\N	Open Items	Y	2010-12-08 21:22:10	100	\N	\N	O
53666	Daily	0	0	53386	2010-12-08 21:22:28	100	\N	Open Items	Y	2010-12-08 21:22:28	100	\N	\N	001_D
53667	Weekly	0	0	53386	2010-12-08 21:22:28	100	\N	Open Items	Y	2010-12-08 21:22:28	100	\N	\N	007_W
53668	Fortnightly	0	0	53386	2010-12-08 21:22:29	100	\N	Open Items	Y	2010-12-08 21:22:29	100	\N	\N	014_F
53669	Biweekly	0	0	53386	2010-12-08 21:22:30	100	\N	Open Items	Y	2010-12-08 21:22:30	100	\N	\N	015_B
53670	Monthly	0	0	53386	2010-12-08 21:22:30	100	\N	Open Items	Y	2010-12-08 21:22:30	100	\N	\N	030_M
53671	Bimonthly	0	0	53386	2010-12-08 21:22:31	100	\N	Open Items	Y	2010-12-08 21:22:31	100	\N	\N	060_2
53672	Quarterly	0	0	53386	2010-12-08 21:22:31	100	\N	Open Items	Y	2010-12-08 21:22:31	100	\N	\N	090_Q
53673	Semiannual	0	0	53386	2010-12-08 21:22:32	100	\N	Open Items	Y	2010-12-08 21:22:32	100	\N	\N	180_S
53674	Yearly	0	0	53386	2010-12-08 21:22:33	100	\N	Open Items	Y	2010-12-08 21:22:33	100	\N	\N	365_Y
53675	1_Initial Balance	0	0	53387	2010-12-08 21:23:50	100	\N	Open Items	Y	2010-12-08 21:23:50	100	\N	\N	1
53676	2_Plan	0	0	53387	2010-12-08 21:23:51	100	\N	Open Items	Y	2010-12-08 21:23:51	100	\N	\N	2
53677	3_Commitments (Orders)	0	0	53387	2010-12-08 21:23:51	100	\N	Open Items	Y	2010-12-08 21:23:51	100	\N	\N	3
53678	4_Actual Debt (Invoices)	0	0	53387	2010-12-08 21:23:52	100	\N	Open Items	Y	2010-12-08 21:23:52	100	\N	\N	4
\.
