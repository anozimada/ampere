copy "ad_reference" ("ad_reference_id", "name", "validationtype", "ad_client_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "isorderbyvalue", "updated", "updatedby", "vformat") from STDIN;
155	M_DiscountPriceList RoundingRule	L	0	0	1999-08-06 00:00:00	0	Price Rounding Rule list	Material Management Rules	\N	Y	N	2000-01-02 00:00:00	0	\N
164	M_Replenish Type	L	0	0	1999-08-10 00:00:00	0	\N	Material Management Rules	\N	Y	N	2016-04-11 16:47:03	0	\N
188	M_PriceList Version	T	0	0	1999-12-15 15:01:02	0	\N	Material Management Rules	\N	Y	N	2000-01-02 00:00:00	0	\N
189	M_Transaction Movement Type	L	0	0	1999-12-15 19:04:12	0	\N	Material Management Rules	\N	Y	N	2007-12-17 04:17:03	0	 
194	M_DiscountPriceList Base	L	0	0	2000-01-28 09:36:27	0	\N	Material Management Rules	\N	Y	N	2000-01-02 00:00:00	0	\N
246	M_Discount CumulativeLevel	L	0	0	2001-12-28 20:07:12	0	\N	Material Management Rules	\N	Y	N	2000-01-02 00:00:00	0	L
247	M_Discount Type	L	0	0	2001-12-28 20:09:14	0	\N	Material Management Rules	\N	Y	N	2000-01-02 00:00:00	0	L
270	M_Product_ProductType	L	0	0	2002-10-25 22:46:22	0	\N	Material Management Rules	\N	Y	N	2016-04-11 16:47:12	0	\N
279	M_Product BOM Product TypeX	L	0	0	2003-05-21 17:48:56	0	Old	Material Management Rules	\N	Y	N	2005-05-15 14:28:44	100	\N
288	C_ProjectType Category	L	0	0	2003-09-02 18:12:24	0	\N	Material Management Rules	\N	Y	N	2000-01-02 00:00:00	0	\N
290	C_Order	T	0	0	2003-09-03 12:24:54	0	Order	Material Management Rules	\N	Y	N	2000-01-02 00:00:00	0	\N
313	M_RelatedProduct Type	L	0	0	2004-02-07 17:50:22	0	\N	Material Management Rules	\N	Y	N	2000-01-02 00:00:00	0	\N
347	M_BOM Type	L	0	0	2005-05-15 14:29:06	100	\N	Material Management Rules	\N	Y	N	2007-12-17 01:10:37	0	\N
348	M_BOM Use	L	0	0	2005-05-15 14:36:55	100	\N	Material Management Rules	\N	Y	N	2007-12-17 01:12:25	0	\N
349	M_BOMProduct Type	L	0	0	2005-05-15 15:53:12	100	\N	Material Management Rules	\N	Y	N	2005-05-15 15:53:12	100	\N
353	C_BPartner SalesRep	T	0	0	2005-07-13 15:42:53	100	BP SalesRep selection	Material Management Rules	\N	Y	N	2005-07-13 15:42:53	100	\N
383	C_Project InvoiceRule	L	0	0	2006-03-26 18:40:23	100	\N	Material Management Rules	\N	Y	N	2006-03-26 18:40:23	100	\N
384	C_Project LineLevel	L	0	0	2006-03-26 20:31:24	100	\N	Material Management Rules	\N	Y	N	2006-03-26 20:31:24	100	\N
53225	PP_ComponentType	L	0	0	2007-12-17 03:26:21	0	\N	Material Management Rules	\N	Y	N	2008-02-12 13:45:33	0	\N
53226	PP_Product_BOM IssueMethod	L	0	0	2007-12-17 03:26:45	0	Issue Method	Material Management Rules	\N	Y	N	2008-02-12 13:45:38	0	\N
53294	M_PromotionDistribution Operation	L	0	0	2009-04-07 16:55:02	100	\N	Material Management Rules	\N	Y	N	2009-04-07 16:55:02	100	CC
53295	M_PromotionDistrition Type	L	0	0	2009-04-07 17:15:07	100	Type of quantity distribution	Material Management Rules	\N	Y	N	2009-04-07 17:15:07	100	C
53296	M_PromotionDistribution Sorting	L	0	0	2009-04-07 17:20:22	100	Quantity distribution sorting direction	Material Management Rules	\N	Y	N	2009-04-07 17:20:22	100	C
53297	M_PromotionDistribution	T	0	0	2009-04-09 16:20:34	100	\N	Material Management Rules	\N	Y	N	2009-04-09 16:20:34	100	\N
53298	M_PromotionReward Type	L	0	0	2009-04-09 16:26:11	100	\N	Material Management Rules	\N	Y	N	2009-04-09 16:26:11	100	L
53323	UOM Type	L	0	0	2009-09-12 15:00:00	100	\N	Material Management Rules	\N	Y	N	2009-09-12 15:00:00	100	\N
53343	M_DiscountSchema PL	T	0	0	2010-03-18 22:26:41	100	Price List Discount Schema	Material Management Rules	\N	Y	N	2010-03-18 22:26:41	100	\N
53429	M_FreightRegion	T	0	0	2012-03-26 15:18:48	100	\N	Material Management Rules	\N	Y	N	2012-03-26 15:18:48	100	\N
\.
