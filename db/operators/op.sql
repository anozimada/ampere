DROP OPERATOR IF EXISTS + (timestamp with time zone, numeric) CASCADE; 

CREATE OPERATOR + (
    FUNCTION = adddays,
    LEFTARG = timestamp with time zone,
    RIGHTARG = numeric,
    COMMUTATOR = OPERATOR(+)
);

DROP OPERATOR IF EXISTS + (interval, numeric) CASCADE;

CREATE OPERATOR + (
    FUNCTION = adddays,
    LEFTARG = interval,
    RIGHTARG = numeric,
    COMMUTATOR = OPERATOR(+)
);

DROP OPERATOR IF EXISTS - (timestamp with time zone, numeric) CASCADE;

CREATE OPERATOR - (
    FUNCTION = subtractdays,
    LEFTARG = timestamp with time zone,
    RIGHTARG = numeric
);


DROP OPERATOR IF EXISTS - (interval, numeric) CASCADE;

CREATE OPERATOR - (
    FUNCTION = subtractdays,
    LEFTARG = interval,
    RIGHTARG = numeric
);


