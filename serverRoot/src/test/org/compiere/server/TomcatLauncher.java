package org.compiere.server;

import java.io.File;
import java.io.IOException;
import org.apache.catalina.Valve;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.valves.RemoteIpValve;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;
import org.apache.tomcat.util.scan.StandardJarScanner;
import org.keycloak.adapters.tomcat.KeycloakAuthenticatorValve;

public class TomcatLauncher {

    public static void main(String[] args) throws Exception {

        String webappDirLocation = "src/web/";
        Tomcat tomcat = new Tomcat();

        String webPort = System.getenv("PORT");
        if(webPort == null || webPort.isEmpty()) {
            webPort = "8081";
        }

        tomcat.setPort(Integer.valueOf(webPort));
        tomcat.setBaseDir(createTempDir());
        
        /*  experiment to try to resolve web-app within fat jar
         * 
        URL webRootLocation = TomcatLauncher.class.getResource("/adempiere.html");
        if (webRootLocation == null)
        {
            throw new IllegalStateException("Unable to determine webroot URL location");
        }

        URI webRootUri = URI.create(webRootLocation.toURI().toASCIIString().replaceFirst("/adempiere.html$","/"));
       
        
        String appBase = webRootUri.toString();
        System.err.printf("Web Root URI: %s%n",appBase);
        
        Host host = tomcat.getHost();
        host.setAppBase(appBase);      
        StandardContext ctx = (StandardContext) tomcat.addWebapp("", appBase);

        */

        StandardContext ctx = (StandardContext) tomcat.addWebapp("", new File(webappDirLocation).getAbsolutePath());

        String kccf = System.getenv("KEYCLOAK_JSON_FILE");
        if ( kccf == null || kccf.isEmpty() )
        	kccf = "keycloak.json";

        ctx.addParameter("keycloak.config.file", kccf);
        
        Valve valve = new KeycloakAuthenticatorValve();
        ctx.getPipeline().addValve(valve);
        
        RemoteIpValve ipValve = new RemoteIpValve();
        ipValve.setProtocolHeader("X-Forwarded-Proto");
        ipValve.setRemoteIpHeader("X-Forwarded-For");
        ctx.getPipeline().addValve(ipValve);
        
        File additionWebInfClasses = new File("bin/main");  // eclipse build output path
        WebResourceRoot resources = new StandardRoot(ctx);
        resources.addPreResources(new DirResourceSet(resources, "/WEB-INF/classes",  // standard location
                additionWebInfClasses.getAbsolutePath(), "/"));
        ctx.setResources(resources);
        StandardJarScanner js = new StandardJarScanner();
        js.setScanClassPath(false);
        js.setScanManifest(false);
        ctx.setJarScanner(js);

        tomcat.start();
        tomcat.getServer().await();
    }
    
    // based on AbstractEmbeddedServletContainerFactory
    private static String createTempDir() {
        try {
            File tempDir = File.createTempFile("tomcat.", "monitor");
            tempDir.delete();
            tempDir.mkdir();
            tempDir.deleteOnExit();
            return tempDir.getAbsolutePath();
        } catch (IOException ex) {
            throw new RuntimeException(
                    "Unable to create tempDir. java.io.tmpdir is set to " + System.getProperty("java.io.tmpdir"),
                    ex
            );
        }
    }
}