/******************************************************************************
 * Copyright (C) 2009 Low Heng Sin                                            *
 * Copyright (C) 2009 Idalica Corporation                                     *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.compiere.apps.form;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;

import org.compiere.apps.IStatusBar;
import org.compiere.minigrid.IDColumn;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.MOrder;
import org.compiere.model.MPInstance;
import org.compiere.model.MPInstancePara;
import org.compiere.model.MPrivateAccess;
import org.compiere.model.MRMA;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.print.ReportEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Trx;

/**
 * Generate Shipment (manual) controller class
 * 
 */
public class InOutGen extends InOutGenForm
{
	private static final String USE_MANIFEST_ON_SHIPPING = "UseManifestOnShipping";

	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(InOutGen.class);
	//
	
	public Object 			m_M_Warehouse_ID = null;
	public Object 			m_C_BPartner_ID = null;
	public int 			m_Good_To_Ship = 0;
	public Date 			m_DatePromised = null;
	
	public void dynInit() throws Exception
	{
		setTitle("InOutGenerateInfo");
		setReportEngineType(ReportEngine.SHIPMENT);
		setAskPrintMsg("PrintShipments");
	}
	
	public void configureMiniTable(IMiniTable miniTable)
	{
		//  create Columns
		miniTable.addColumn("C_Order_ID");
		miniTable.addColumn("AD_Org_ID");
		miniTable.addColumn("C_DocType_ID");
		miniTable.addColumn("DocumentNo");
		miniTable.addColumn("C_BPartner_ID");
		miniTable.addColumn("DateOrdered");
		miniTable.addColumn("TotalLines");
//		miniTable.addColumn("ShipmenNo");
		miniTable.addColumn("GoodToShip");
		miniTable.addColumn("DatePromised");
		//
		miniTable.setMultiSelection(true);

		//  set details
		miniTable.setColumnClass(0, IDColumn.class, false, " ");
		miniTable.setColumnClass(1, String.class, true, Msg.translate(Env.getCtx(), "AD_Org_ID"));
		miniTable.setColumnClass(2, String.class, true, Msg.translate(Env.getCtx(), "C_DocType_ID"));
		miniTable.setColumnClass(3, String.class, true, Msg.translate(Env.getCtx(), "DocumentNo"));
		miniTable.setColumnClass(4, String.class, true, Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		miniTable.setColumnClass(5, Timestamp.class, true, Msg.translate(Env.getCtx(), "DateOrdered"));
		miniTable.setColumnClass(6, BigDecimal.class, true, Msg.translate(Env.getCtx(), "TotalLines"));
//		miniTable.setColumnClass(7, String.class, true, "ShipmentNo");
		miniTable.setColumnClass(7, Boolean.class, true, "Good To Ship");
		miniTable.setColumnClass(8, String.class, true, "Date Promised");
		//
		miniTable.autoSize();
	}
	
	public void configureRequiredProductTable(IMiniTable miniTable)
	{
		miniTable.addColumn("Value");
		miniTable.addColumn("Name");
		miniTable.addColumn("Orders");
		miniTable.addColumn("OnHand");
		miniTable.addColumn("OtherWarehouse");
		miniTable.addColumn("QtyOpen");
		miniTable.addColumn("QtyRequired");
		miniTable.addColumn("QtyToDeliver");
		
		miniTable.setMultiSelection(false);
		
		miniTable.setColumnClass(0, String.class, true, Msg.translate(Env.getCtx(), "Value"));
		miniTable.setColumnClass(1, String.class, true, Msg.translate(Env.getCtx(), "Name"));
		miniTable.setColumnClass(2, String.class, true, "Orders");
		miniTable.setColumnClass(3, BigDecimal.class, true, Msg.translate(Env.getCtx(), "QtyOnHand"));
		miniTable.setColumnClass(4, BigDecimal.class, true, "Other Warehouse");
		miniTable.setColumnClass(5, BigDecimal.class, true, "Open Ship Qty");
		miniTable.setColumnClass(6, BigDecimal.class, true, "Total Qty to Fulfill ");
		miniTable.setColumnClass(7, BigDecimal.class, true, Msg.translate(Env.getCtx(), "QtyToDeliver"));
		
		//
		miniTable.autoSize();
	}
	
	
	private String getOrderProductRequiredSQL()
	{
		if (getSelection() == null || getSelection().size() == 0) {
			return null;
		}
		boolean commaRequired = false;
		String OrderList = "";
		for (int c_order_id : getSelection())
		{
			if (commaRequired) {
				OrderList = OrderList + ", ";
			}
			
			OrderList = OrderList + c_order_id;
			commaRequired = true;
		}
		
		String sql = "SELECT p.value, p.name, req.orders" +
				 ", SUM(CASE WHEN req.m_warehouse_id = l.m_warehouse_id then COALESCE(s.qtyonhand, 0) else 0 end) AS qtyonhand " +
			     ", SUM(CASE WHEN req.m_warehouse_id <> l.m_warehouse_id then COALESCE(s.qtyonhand,0) else 0 end) AS otherwarehouse" +		
			     ", MAX(COALESCE(inout.qtyopen, 0)) AS qtyopen " +
			     ", MAX(req.qtyrequired) AS qtyrequired " +
				 ", MAX(req.qtytodeliver) AS qtytodeliver " +
	             "FROM " +
	             "  (SELECT o.m_warehouse_id, ol.m_product_id, SUM(ol.qtyentered - ol.qtydelivered) AS qtyrequired " +
	             ", SUM(CASE WHEN o.c_order_id IN (" + OrderList + ") THEN ol.qtyentered - ol.qtydelivered ELSE 0 END) AS qtytodeliver " +
	             ", string_agg(o.documentno, ', ') as orders " +
	             "              FROM c_order o  " +
	             "                INNER JOIN c_orderline ol ON o.c_order_id = ol.c_order_id " +
	             "              WHERE o.c_order_id IN (SELECT C_Order_ID FROM m_inout_candidate_v) AND ol.qtyentered <> ol.qtydelivered" +
	             "              GROUP BY o.m_warehouse_id, ol.m_product_id " +
	             "              HAVING SUM(ol.qtyentered - ol.qtydelivered) <> 0) req" +
	             "  INNER JOIN m_product p ON  p.m_product_id = req.m_product_id" +
	             "  LEFT JOIN m_storage s ON p.m_product_id = s.m_product_id  " +
	             "  LEFT JOIN m_locator l ON l.m_locator_id = s.m_locator_id   " +
	             "  LEFT JOIN (SELECT io.m_warehouse_id, iol.M_Product_ID, sum(iol.qtyentered) as qtyopen " +
	             "  	FROM m_inout io INNER JOIN m_inoutline iol ON io.m_inout_id = iol.m_inout_id " +
	             "  	WHERE io.docstatus IN ('IP', 'DR') AND io.issotrx = 'Y' GROUP BY io.m_warehouse_id, iol.M_Product_ID " +
	             "  	) inout ON req.m_warehouse_id = inout.m_warehouse_id AND req.m_product_id = inout.m_product_id " +	             
	             "WHERE l.ad_client_id = ? " +
	             "GROUP BY p.value, p.name, req.orders " +
	             "ORDER BY 1";
		
		return sql;
	}
	
	/**
	 * Get SQL for Orders that needs to be shipped
	 * @return sql
	 */
	private String getOrderSQL()
	{
	//  Create SQL
        StringBuffer sql = new StringBuffer(
            "SELECT ic.C_Order_ID, o.Name, dt.Name, ic.DocumentNo, bp.Name, ic.DateOrdered, ic.TotalLines, ic.isGoodToShip "
   	        + ", to_char(ic.datepromised, 'dd/MM/yyyy HH24:MM') as datepromised "
        	+ "FROM M_InOut_Candidate_v ic  " 
            + "INNER JOIN AD_Org o ON ic.AD_Org_ID = o.AD_Org_ID "
        	+ "INNER JOIN C_BPartner bp ON ic.C_BPartner_ID = bp.C_BPartner_ID " 
            + "INNER JOIN C_DocType dt ON ic.C_DocType_ID = dt.C_DocType_ID " 
            + " WHERE ic.AD_Client_ID=? AND dt.isAllowGenerateShipment='Y'");

        if (m_M_Warehouse_ID != null)
            sql.append(" AND ic.M_Warehouse_ID=").append(m_M_Warehouse_ID);
        if (m_C_BPartner_ID != null)
            sql.append(" AND ic.C_BPartner_ID=").append(m_C_BPartner_ID);
        if (m_Good_To_Ship != 0)
            sql.append(" AND ic.isGoodToShip='").append(m_Good_To_Ship == 1 ? "Y" : "N").append("'");

        if (m_DatePromised != null) {
        	sql.append(" AND ic.datepromised <= ? ");
        }
        
        
        // bug - [ 1713317 ] Generate Shipments (manual) show locked records
        /* begin - Exclude locked records; @Trifon */
        int AD_User_ID = Env.getContextAsInt(Env.getCtx(), "#AD_User_ID");
        String lockedIDs = MPrivateAccess.getLockedRecordWhere(MOrder.Table_ID, AD_User_ID);
        if (lockedIDs != null)
        {
            if (sql.length() > 0)
                sql.append(" AND ");
            sql.append("ic.C_Order_ID").append(lockedIDs);
        }
        /* eng - Exclude locked records; @Trifon */
          
        //
        sql.append(" ORDER BY o.Name,bp.Name,DateOrdered");
        
        return sql.toString();
	}
	
	/**
	 * Get SQL for Vendor RMA that need to be shipped
	 * @return sql
	 */
	private String getRMASql()
	{
	    StringBuffer sql = new StringBuffer();
	    
	    sql.append("SELECT rma.M_RMA_ID, org.Name, dt.Name, rma.DocumentNo, bp.Name, rma.Created, rma.Amt ");
	    sql.append(", '' ");
	    sql.append("FROM M_RMA rma INNER JOIN AD_Org org ON rma.AD_Org_ID=org.AD_Org_ID ");
	    sql.append("INNER JOIN C_DocType dt ON rma.C_DocType_ID=dt.C_DocType_ID ");
	    sql.append("INNER JOIN C_BPartner bp ON rma.C_BPartner_ID=bp.C_BPartner_ID ");
	    sql.append("INNER JOIN M_InOut io ON rma.InOut_ID=io.M_InOut_ID ");
	    sql.append("WHERE rma.DocStatus='CO' ");
	    sql.append("AND dt.DocBaseType = 'POO' ");
	    sql.append("AND EXISTS (SELECT * FROM M_RMA r INNER JOIN M_RMALine rl ");
	    sql.append("ON r.M_RMA_ID=rl.M_RMA_ID WHERE r.M_RMA_ID=rma.M_RMA_ID ");
	    sql.append("AND rl.IsActive='Y' AND rl.M_InOutLine_ID > 0 AND rl.QtyDelivered < rl.Qty) ");
	    sql.append("AND NOT EXISTS (SELECT * FROM M_InOut oio WHERE oio.M_RMA_ID=rma.M_RMA_ID ");
	    sql.append("AND oio.DocStatus IN ('IP', 'CO', 'CL')) " );
	    sql.append("AND rma.AD_Client_ID=?");
	    
	    if (m_M_Warehouse_ID != null)
            sql.append(" AND io.M_Warehouse_ID=").append(m_M_Warehouse_ID);
        if (m_C_BPartner_ID != null)
            sql.append(" AND bp.C_BPartner_ID=").append(m_C_BPartner_ID);
        
        int AD_User_ID = Env.getContextAsInt(Env.getCtx(), "#AD_User_ID");
        String lockedIDs = MPrivateAccess.getLockedRecordWhere(MRMA.Table_ID, AD_User_ID);
        if (lockedIDs != null)
        {
            sql.append(" AND rma.M_RMA_ID").append(lockedIDs);
        }
	    
	    sql.append(" ORDER BY org.Name, bp.Name, rma.Created ");

	    return sql.toString();
	}
	
	/**
	 *  Query Info
	 */
	public void executeQuery(KeyNamePair docTypeKNPair, IMiniTable miniTable)
	{
		log.info("");
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		
		String sql = "";
		
		if (docTypeKNPair.getKey() == MRMA.Table_ID)
		{
		    sql = getRMASql();
		}
		else
		{
		    sql = getOrderSQL();
		}

		log.fine(sql);
		//  reset table
		int row = 0;
		miniTable.setRowCount(row);
		//  Execute
		try
		{
			PreparedStatement pstmt = DB.prepareStatement(sql.toString(), null);
			pstmt.setInt(1, AD_Client_ID);
			if (m_DatePromised != null) {
				pstmt.setTimestamp(2,new Timestamp(m_DatePromised.getTime()));
			}
			ResultSet rs = pstmt.executeQuery();
			//
			while (rs.next())
			{
				//  extend table
				miniTable.setRowCount(row+1);
				//  set values
				miniTable.setValueAt(new IDColumn(rs.getInt(1)), row, 0);   //  C_Order_ID
				miniTable.setValueAt(rs.getString(2), row, 1);              //  Org
				miniTable.setValueAt(rs.getString(3), row, 2);              //  DocType
				miniTable.setValueAt(rs.getString(4), row, 3);              //  Doc No
				miniTable.setValueAt(rs.getString(5), row, 4);              //  BPartner
				miniTable.setValueAt(rs.getTimestamp(6), row, 5);           //  DateOrdered
				miniTable.setValueAt(rs.getBigDecimal(7), row, 6);          //  TotalLines
//				miniTable.setValueAt(rs.getString(8), row, 7);              //  Shipment No
				miniTable.setValueAt(rs.getString(8).equals("Y"), row, 7);              //  Good to Ship
				miniTable.setValueAt(rs.getString(9), row, 8);              //  Date Promised

				//  prepare next
				row++;
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		//
		miniTable.autoSize();
	//	statusBar.setStatusDB(String.valueOf(miniTable.getRowCount()));
	}   //  executeQuery
	
	/**
	 *	Save Selection & return selection Query or ""
	 *  @return where clause like C_Order_ID IN (...)
	 */
	public void saveSelection(IMiniTable miniTable)
	{
		log.info("");

		//  Array of Integers
		ArrayList<Integer> results = new ArrayList<Integer>();
		setSelection(null);

		//	Get selected entries
		int rows = miniTable.getRowCount();
		for (int i = 0; i < rows; i++)
		{
			IDColumn id = (IDColumn)miniTable.getValueAt(i, 0);     //  ID in column 0
		//	log.fine( "Row=" + i + " - " + id);
			if (id != null && id.isSelected())
				results.add(id.getRecord_ID());
		}

		if (results.size() == 0)
			return;
		log.config("Selected #" + results.size());
		setSelection(results);
	}	//	saveSelection

	
	public void resetMinitable(IMiniTable miniTable)
	{
		miniTable.setRowCount(0);
		miniTable.autoSize();
		miniTable.repaint();
	}
	public void updateProductRequired(IMiniTable miniTable) 
	{
		log.info("");
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		String sql = "";

		sql = getOrderProductRequiredSQL();
		log.fine(sql);
		//  reset table
		int row = 0;
		miniTable.setRowCount(row);
		if (sql == null) {
			miniTable.autoSize();
			miniTable.repaint();
			return;
		}

		//  Execute
		try
		{
			PreparedStatement pstmt = DB.prepareStatement(sql.toString(), null);
			pstmt.setInt(1, AD_Client_ID);
			ResultSet rs = pstmt.executeQuery();
			//
			while (rs.next())
			{
				//  extend table
				miniTable.setRowCount(row+1);
				//  set values
				miniTable.setValueAt(rs.getString(1), row, 0);              //  PValue
				miniTable.setValueAt(rs.getString(2), row, 1);              //  PName
				miniTable.setValueAt(rs.getString(3), row, 2);              //  Orders
				miniTable.setValueAt(rs.getBigDecimal(4), row, 3);          //  QOH
				miniTable.setValueAt(rs.getBigDecimal(5), row, 4);          //  Other Warehouse
				miniTable.setValueAt(rs.getBigDecimal(6), row, 5);          //  QtyOpen
				miniTable.setValueAt(rs.getBigDecimal(7), row, 6);          //  Total Qty to Fulfil
				miniTable.setValueAt(rs.getBigDecimal(8), row, 7);          //  QtyRequired

				//  prepare next
				row++;
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		//
		miniTable.autoSize();
		miniTable.repaint();

	}
	
	/**************************************************************************
	 *	Generate Shipments
	 */
	public String generate(IStatusBar statusBar, KeyNamePair docTypeKNPair, String docActionSelected)
	{
		String info = "";
		MWarehouse wh = new MWarehouse(Env.getCtx(), Integer.parseInt(m_M_Warehouse_ID.toString()), null);
		
		if (MSysConfig.getValue(USE_MANIFEST_ON_SHIPPING, 
					wh.getAD_Client_ID(),
					wh.getAD_Org_ID()) == null) {
			MSysConfig def = new MSysConfig(Env.getCtx(), 0, null);
			def.setAD_Client_ID(wh.getAD_Client_ID());
			def.setAD_Org_ID(wh.getAD_Org_ID());
			def.setName(USE_MANIFEST_ON_SHIPPING);
			def.setValue("N");
			def.setConfigurationLevel(MSysConfig.CONFIGURATIONLEVEL_Organization);
			def.setDescription("Creates shipment manifest if configured to Y");
			def.setEntityType("U");
			def.save();
			MSysConfig.resetCache();
		}
		boolean isUseManifest = MSysConfig.getBooleanValue(USE_MANIFEST_ON_SHIPPING, false
				, wh.getAD_Client_ID(), wh.getAD_Org_ID());
		
		log.info("M_Warehouse_ID=" + m_M_Warehouse_ID);
		String trxName = Trx.createTrxName("IOG");	
		Trx trx = Trx.get(trxName, true);	//trx needs to be committed too
		
		setSelectionActive(false);  //  prevents from being called twice
		statusBar.setStatusLine(Msg.getMsg(Env.getCtx(), "InOutGenerateGen"));
		statusBar.setStatusDB(String.valueOf(getSelection().size()));

		//	Prepare Process
		int AD_Process_ID = 0;	  
        
        if (docTypeKNPair.getKey() == MRMA.Table_ID)
        {
            AD_Process_ID = 52001; // M_InOut_GenerateRMA - org.adempiere.process.InOutGenerateRMA
        }
        else
        {
            AD_Process_ID = 199;      // M_InOut_Generate - org.compiere.process.InOutGenerate
        }
		
		MPInstance instance = new MPInstance(Env.getCtx(), AD_Process_ID, 0);
		if (!instance.save())
		{
			info = Msg.getMsg(Env.getCtx(), "ProcessNoInstance");
			return info;
		}
		
		//insert selection
		StringBuffer insert = new StringBuffer();
		insert.append("INSERT INTO T_SELECTION(AD_PINSTANCE_ID, T_SELECTION_ID) ");
		int counter = 0;
		for(Integer selectedId : getSelection())
		{
			counter++;
			if (counter > 1)
				insert.append(" UNION ");
			insert.append("SELECT ");
			insert.append(instance.getAD_PInstance_ID());
			insert.append(", ");
			insert.append(selectedId);
			insert.append(" ");
			
			if (counter == 1000) 
			{
				if ( DB.executeUpdate(insert.toString(), trxName) < 0 )
				{
					String msg = "No Shipments";     //  not translated!
					log.config(msg);
					info = msg;
					trx.rollback();
					return info;
				}
				insert = new StringBuffer();
				insert.append("INSERT INTO T_SELECTION(AD_PINSTANCE_ID, T_SELECTION_ID) ");
				counter = 0;
			}
		}
		if (counter > 0)
		{
			if ( DB.executeUpdate(insert.toString(), trxName) < 0 )
			{
				String msg = "No Shipments";     //  not translated!
				log.config(msg);
				info = msg;
				trx.rollback();
				return info;
			}
		}
		
		
		//call process
		ProcessInfo pi = new ProcessInfo ("VInOutGen", AD_Process_ID);
		pi.setAD_PInstance_ID (instance.getAD_PInstance_ID());

		//	Add Parameter - Selection=Y
		MPInstancePara ip = new MPInstancePara(instance, 10);
		ip.setParameter("Selection","Y");
		if (!ip.save())
		{
			String msg = "No Parameter added";  //  not translated
			info = msg;
			log.log(Level.SEVERE, msg);
			return info;
		}
		//Add Document action parameter
		ip = new MPInstancePara(instance, 20);
//		String docActionSelected = (String)docAction.getValue();
		ip.setParameter("DocAction", docActionSelected);
		if(!ip.save())
		{
			String msg = "No DocACtion Parameter added";
			info = msg;
			log.log(Level.SEVERE, msg);
			return info;
		}
		//	Add Parameter - M_Warehouse_ID=x
		ip = new MPInstancePara(instance, 30);
		ip.setParameter("M_Warehouse_ID", Integer.parseInt(m_M_Warehouse_ID.toString()));
		if(!ip.save())
		{
			String msg = "No Parameter added";  //  not translated
			info = msg;
			log.log(Level.SEVERE, msg);
			return info;
		}
		ip = new MPInstancePara(instance, 40);
		if (isUseManifest) {
			ip.setParameter("IsUseManifest","Y");
			if (!ip.save())
			{
				String msg = "No Parameter added";  //  not translated
				info = msg;
				log.log(Level.SEVERE, msg);
				return info;
			}
		}
		
		setTrx(trx);
		setProcessInfo(pi);
		
		return info;
	}	//	generateShipments
	
	public void setM_Warehouse_ID(Object value)
	{
		this.m_M_Warehouse_ID = value;
	}
	
	public int getM_Warehouse_ID()
	{
		if (m_M_Warehouse_ID == null)
			return -1;
		return ((Integer)m_M_Warehouse_ID);
	}
}