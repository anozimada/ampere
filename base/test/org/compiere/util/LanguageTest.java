package org.compiere.util;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Locale;

import javax.print.attribute.standard.MediaSize;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LanguageTest {

	static Language lang_en_US;
	static Language lang_en_AU;
	static Language lang_en_ZZ;
	static Language lang_es_MX;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		org.compiere.Adempiere.startupEnvironment(true);
		
		lang_en_US = Language.getLanguage("en_US");
		lang_en_AU = Language.getLanguage("en_AU");
		lang_es_MX = Language.getLanguage("es_MX");
		lang_en_ZZ = Language.getLanguage("en_ZZ");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testGetLanguageString() {
		assertEquals("Language=[English (Australia),Locale=en_AU,AD_Language=en,DatePattern=DD/MM/YYYY,DecimalPoint=true]",
				Language.getLanguage("en_AU").toString());
	}
	
	@Test
	void testGetBaseLanguage() {
		assertEquals("Language=[English,Locale=en,AD_Language=en,DatePattern=YYYY-MM-DD,DecimalPoint=true]",
				Language.getBaseLanguage().toString());
	}
	
	@Test
	void testGetBaseAD_Language() {
		assertEquals("en", Language.getBaseAD_Language());
	}

	@Test
	void testGetNames() {
		assertEquals("English", Language.getNames()[0]);
	}

	@Test
	void testLanguageStringStringLocaleBooleanStringMediaSize() {
		Language lang = new Language("Test Language", "tt_TT", Locale.ROOT, true, "dd/MM/yyyy", MediaSize.ISO.A4);
		
		assertEquals("Language=[Test Language,Locale=,AD_Language=tt_TT,DatePattern=DD/MM/YYYY,DecimalPoint=true]", 
				lang.toString());
	}

	@Test
	void testGetLanguageCode() {
		assertEquals("en", lang_en_US.getLanguageCode());
	}

	@Test
	void testGetDateFormat() {
		assertEquals("MM/dd/yyyy", lang_en_US.getDateFormat().toPattern());
		assertEquals("dd/MM/yyyy", lang_en_AU.getDateFormat().toPattern());
	}

	@Test
	void testGetDateTimeFormat() {
		assertEquals("MMM d, y, h:mm:ss a z", lang_en_US.getDateTimeFormat().toPattern());
	}

	@Test
	void testGetDBdatePattern() {
		assertEquals("MM/DD/YYYY", lang_en_US.getDBdatePattern());
	}

	@Test
	void testEqualsObject() {
		//  weird idea of equality
		// "Two languages are equal, if they have the same AD_Language"
		assertTrue( lang_en_US.equals(lang_en_AU));
		assertTrue( lang_en_US.equals(lang_es_MX));
	}

}
