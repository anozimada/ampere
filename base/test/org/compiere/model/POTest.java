package org.compiere.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class POTest {

	private static MClient client = null;
	private Trx trx = null;
	
	@BeforeAll
	private static void setUpClass() throws Exception {

		org.compiere.Adempiere.startupEnvironment(true);
		client = MClient.get(Env.getCtx(), 0);
		client.set_Value("Name", "Test System");
	}
	
	@BeforeEach
	void setUp() throws Exception {
		
		trx = Trx.get(Trx.createTrxName("POTest"), true);
	}

	@AfterEach
	void tearDown() throws Exception {
		trx.rollback();
		trx.close();
	}

	@Test
	void testPOPropertiesIntString() {
		MClient c = new MClient(Env.getCtx(), 0, "test_trx");
		assertEquals(0, c.get_ID());
	}

	@Test
	void testToString() {
		assertEquals("MClient[0-SYSTEM]", client.toString());
	}

	@Test
	void testGet_ValueString() {
		assertEquals("Test System", client.get_Value("Name"));
	}
	

	@Test
	void testGet_ValueInt() {
		assertEquals(0, client.get_Value(0));
	}

	@Test
	void testGet_ValueAsString() {
		assertEquals("0", client.get_ValueAsString("AD_Client_ID"));
	}

	@Test
	void testGet_ValueOld() {
		assertEquals("System", client.get_ValueOld("Name"));
	}

	@Test
	void testGet_ValueOldAsInt() {
		assertEquals(0, client.get_ValueOldAsInt("AD_Client_ID"));
	}

	@Test
	void testIs_ValueChanged() {
		client.set_Value("Value", "new_value");
		assertTrue(client.is_ValueChanged("Name"));
	}

	@Test
	void testSet_ValueStringObject() {
		assertTrue(client.set_Value("IsActive", true));
	}
	

	@Test
	void testUpdateAndSave() {
		
		MTest test = new MTest(Env.getCtx(), 0, trx.getTrxName());
		test.setName("Test 1");
		test.setC_Currency_ID(100);
		test.saveEx();
		
		assertNotEquals(0, test.getTest_ID());
		
		test.setC_Currency_ID(-1);
		
		test.saveEx();
		assertNotEquals(100, test.getC_Currency_ID());
		
	}

	@Test
	void testSet_ValueIntObject() {
		// not updateable
		assertFalse(client.set_Value(client.get_ColumnIndex("AD_Client_ID"), 0));
		// updateable
		assertTrue(client.set_Value(client.get_ColumnIndex("Description"), "Test name"));
	}
	

	@Test
	void testCreateNewZeroId() {
		
		// Org table has zero ID record
		MOrg org = new MOrg(Env.getCtx(), -1, trx.getTrxName());
		org.setName("Zero test");
		org.saveEx();
		
		assertTrue(org.getAD_Org_ID() > 0);
	}

	@Disabled
	@Test 
	void testLoading10000Columns() {
		
		long start = System.nanoTime();
		
		int[] ids = DB.getIDsEx(null, "SELECT AD_Column_ID FROM AD_Column");
		int counter = 1;
		for (int i = 0; i < 10000; i++)
		{
			MColumn col = new MColumn(Env.getCtx(), ids[i], null);
			String s = col.getColumnName() + col.getValueMax() + col.getFormatPattern();
			System.out.println(s);
			counter++;
		}
		 long elapsed = System.nanoTime() - start;
		 
		 System.out.println( "Elapsed (ms):" + elapsed/1000000 + " Loaded: " + counter);
	}
	
	@Disabled
	@Test 
	void testCreating100Orders() {
		
		String trxName = Trx.createTrxName();

		Trx trx = Trx.get(trxName, true);

		Properties ctx = Env.getCtx();
		Env.setContext(ctx, "#AD_Client_ID", 11);
		int salesRep_ID = 101;
		Env.setContext(ctx, "#SalesRep_ID", 101);
		
		
		long start = System.nanoTime();
		try {
			for (int i = 0; i < 100; i++)
			{
				MOrder order = new MOrder(ctx, 0, trxName);
				MBPartner bp = new MBPartner(ctx, 112, null);
				order.setAD_Client_ID(11);
				order.setAD_Org_ID(11);
				order.setBPartner(bp);
				order.setM_PriceList_ID(101);
				order.setM_Warehouse_ID(103);
				order.setSalesRep_ID(salesRep_ID);  
				order.setC_DocTypeTarget_ID(135);
				order.saveEx(trxName);
				System.out.println("Created order: " + order.getDocumentNo());
			}
		}
		finally {
			trx.rollback();
			trx.close();
		}

		 long elapsed = System.nanoTime() - start;
		 System.out.println( "Elapsed (ms):" + elapsed/1000000 );
	}

}
