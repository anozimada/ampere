/**
 * 
 */
package org.compiere.acct;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MGAASAssetJournalLine;
import org.compiere.model.MInvoice;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

/**
 * @author Ashley G Ramdass
 * 
 */
public class DocLine_AssetJournal extends DocLine
{
    private static final CLogger logger = CLogger
            .getCLogger(DocLine_AssetJournal.class);
    private MGAASAssetJournalLine poLine;

    public DocLine_AssetJournal(MGAASAssetJournalLine line,
            Doc_GAASAssetJournal doc)
    {
        super(line, doc);
        this.poLine = line;
    }

    public boolean isMatchedInvoice()
    {
        return (poLine.getC_InvoiceLine_ID() != 0);
    }

    public boolean hasDetails()
    {
        return poLine.hasDetails();
    }

    /**
     * @return the line
     */
    public MGAASAssetJournalLine getPOLine()
    {
        return poLine;
    }

    public BigDecimal getPostedAmount(MAcctSchema schema, String trxName)
    {
        BigDecimal postedAmount = Env.ZERO;

        if (!isMatchedInvoice())
        {
            return postedAmount;
        }

        String sql = "SELECT AmtAcctDr, AmtAcctCr FROM Fact_Acct fa "
                + "INNER JOIN C_ElementValue ev "
                + "ON (fa.Account_ID=ev.C_ElementValue_ID) "
                + "WHERE fa.AD_Table_ID=? AND fa.Line_ID=? "
                + "AND fa.C_AcctSchema_ID=? AND ev.AccountType='A'";

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, trxName);
            pstmt.setInt(1, MInvoice.Table_ID);
            pstmt.setInt(2, poLine.getC_InvoiceLine_ID());
            pstmt.setInt(3, schema.get_ID());

            rs = pstmt.executeQuery();
            if (rs.next())
            {
                BigDecimal amtDebit = rs.getBigDecimal(1);
                BigDecimal amtCredit = rs.getBigDecimal(2);

                postedAmount = amtDebit.subtract(amtCredit);
            }
        }
        catch (Exception ex)
        {
            logger.log(Level.SEVERE, "Could not get posted amount", ex);
            throw new IllegalStateException("Could not get posted amount");
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        return postedAmount;
    }

    public MAccount getAccount(String account, MAcctSchema as, String trxName)
    {
        return getAccount(account, as, poLine.getA_Asset_ID(), 0, trxName);
    }

    public MAccount getAccount(String account, MAcctSchema as, int assetId,
            int assetAcctId, String trxName)
    {
        String sql = "SELECT " + account + " FROM GAAS_Asset_Acct"
                + " WHERE A_Asset_ID=? AND C_AcctSchema_ID=?";

        if (assetAcctId > 0)
        {
            sql += " AND GAAS_Asset_Acct_ID=?";
        }
        else
        {
            sql += " AND IsActive='Y'";
        }

        int validCombination_ID = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try
        {
            pstmt = DB.prepareStatement(sql, trxName);
            pstmt.setInt(1, assetId);
            pstmt.setInt(2, as.getC_AcctSchema_ID());
            if (assetAcctId > 0)
            {
                pstmt.setInt(3, assetAcctId);
            }
            rs = pstmt.executeQuery();
            if (rs.next())
            {
                validCombination_ID = rs.getInt(1);
            }
        }
        catch (SQLException e)
        {
            log.log(Level.SEVERE, sql, e);
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        if (validCombination_ID == 0)
        {
            throw new IllegalStateException("No Account defined for: "
                    + Msg.getMsg(as.getCtx(), account));
        }

        return new MAccount(as.getCtx(), validCombination_ID, trxName);
    }

    public FactLine createFactLine(Fact fact, MAccount account, int currencyId,
            BigDecimal debitAmount, BigDecimal creditAmount)
    {
        FactLine factLine = fact.createLine(this, account, currencyId,
                debitAmount, creditAmount);
        factLine.setA_Asset_ID(poLine.getA_Asset_ID());

        return factLine;
    }

    public FactLine createFactLine(Fact fact, MAccount account, int currencyId,
            BigDecimal debitAmount, BigDecimal creditAmount, int assetId)
    {
        FactLine factLine = fact.createLine(this, account, currencyId,
                debitAmount, creditAmount);
        
        if (factLine != null)
        {
            factLine.setA_Asset_ID(assetId);
        }

        return factLine;
    }
}
