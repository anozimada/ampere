package org.compiere.acct;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.compiere.model.MAcctSchema;
import org.compiere.model.MProduct;
import org.compiere.model.MProject;
import org.compiere.model.MTimesheetEntry;
import org.compiere.model.ProductCost;
import org.compiere.util.Env;

/**
 *	Timesheet Entry.
 *	Note:
 *		Will load the default GL Category. 
 *		Set up a document type to set the GL Category. 
 *	
 */
public class Doc_TimesheetEntry extends Doc
{
	/**
	 *  Constructor
	 * 	@param ass accounting schemata
	 * 	@param rs record
	 * 	@param trxName trx
	 */
	public Doc_TimesheetEntry (MAcctSchema[] ass, ResultSet rs, String trxName)
	{
		super (ass, MTimesheetEntry.class, rs, DOCTYPE_TimesheetEntry, trxName);
	}   //  

	/**	Pseudo Line								*/
	private DocLine				m_line = null;
	/** Issue									*/
	private MTimesheetEntry		m_timesheetEntry = null;

	/**
	 *  Load Document Details
	 *  @return error message or null
	 */
	protected String loadDocumentDetails()
	{
		setC_Currency_ID(NO_CURRENCY);
		m_timesheetEntry = (MTimesheetEntry)getPO();
		setDateDoc (m_timesheetEntry.getStartDate());
		setDateAcct(m_timesheetEntry.getDateAcct());
			
		//	Pseudo Line
		m_line = new DocLine (m_timesheetEntry, this); 
		m_line.setQty (m_timesheetEntry.getQtyEntered(), true);    //  sets Trx and Storage Qty
		
		//	Pseudo Line Check
		if (m_line.getM_Product_ID() == 0)
			log.warning(m_line.toString() + " - No Product");
		log.fine(m_line.toString());
		return null;
	}   //  loadDocumentDetails

	/**
	 * 	Get DocumentNo
	 *	@return document no
	 */
	public String getDocumentNo ()
	{
		MProject p = m_timesheetEntry.getProject();
		if (p != null)
			return p.getValue() + " #" + m_timesheetEntry.get_ID();
		return "(" + m_timesheetEntry.get_ID() + ")";
	}	//	getDocumentNo

	/**
	 *  Get Balance
	 *  @return Zero (always balanced)
	 */
	public BigDecimal getBalance()
	{
		BigDecimal retValue = Env.ZERO;
		return retValue;
	}   //  getBalance

	/**
	 *  Create Facts (the accounting logic) for
	 *  PJI
	 *  <pre>
	 *  Issue
	 *      ProjectWIP      DR
	 *      Inventory               CR
	 *  </pre>
	 *  Project Account is either Asset or WIP depending on Project Type
	 *  @param as accounting schema
	 *  @return Fact
	 */
	public ArrayList<Fact> createFacts (MAcctSchema as)
	{
		/*  don't use project accounting or product service accounting
		//  create Fact Header
		Fact fact = new Fact(this, as, Fact.POST_Actual);
		setC_Currency_ID (as.getC_Currency_ID());

		MProject project = new MProject (getCtx(), m_timesheetEntry.getC_Project_ID(), getTrxName());
		String ProjectCategory = project.getProjectCategory();
		MProduct product = MProduct.get(getCtx(), m_timesheetEntry.getM_Product_ID());
			
		//  Line pointers
		FactLine dr = null;
		FactLine cr = null;

		//  Issue Cost
		BigDecimal cost = null;
		if (cost == null)	//	standard Product Costs
			cost = m_line.getProductCosts(as, getAD_Org_ID(), false);
		
		//  Project         DR
		int acctType = ACCTTYPE_ProjectWIP;
		if (MProject.PROJECTCATEGORY_ServiceChargeProject.equals(ProjectCategory) || MProject.PROJECTCATEGORY_General.equals(ProjectCategory))
		{
			dr = fact.createLine(m_line,
					m_line.getAccount(ProductCost.ACCTTYPE_P_Cogs, as), as.getC_Currency_ID(), cost, null);
			dr.setQty(m_line.getQty().negate());
		}
		else {
			if (MProject.PROJECTCATEGORY_AssetProject.equals(ProjectCategory))
				acctType = ACCTTYPE_ProjectAsset;
			dr = fact.createLine(m_line,
					getAccount(acctType, as), as.getC_Currency_ID(), cost, null);
			dr.setQty(m_line.getQty().negate());
		}
		
		//  Inventory               CR
		acctType = ProductCost.ACCTTYPE_P_Asset;
		if (product.isService())
			acctType = ProductCost.ACCTTYPE_P_Expense;
		cr = fact.createLine(m_line,
			m_line.getAccount(acctType, as),
			as.getC_Currency_ID(), null, cost);
		//
		ArrayList<Fact> facts = new ArrayList<Fact>();
		facts.add(fact);
		return facts;
	}   //  createFact
	*/
		//  create Fact Header
		Fact fact = new Fact(this, as, Fact.POST_Actual);
		setC_Currency_ID (as.getC_Currency_ID());

		MProduct product = MProduct.get(getCtx(), m_timesheetEntry.getM_Product_ID());
			
		//  Line pointers
		FactLine dr = null;
		FactLine cr = null;

		//  Issue Cost
		BigDecimal cost = m_line.getProductCosts(as, getAD_Org_ID(), false);


		dr = fact.createLine(m_line,
				m_line.getAccount(ProductCost.ACCTTYPE_P_Cogs, as), as.getC_Currency_ID(), cost, null);
		if ( dr != null )
			dr.setQty(m_line.getQty().negate());


		//  Inventory               CR
		cr = fact.createLine(m_line,
				m_line.getAccount(ProductCost.ACCTTYPE_P_Asset, as),
				as.getC_Currency_ID(), null, cost);
		//
		ArrayList<Fact> facts = new ArrayList<Fact>();
		facts.add(fact);
		return facts;
	}   //  createFact

}	//	DocTimesheetEntry