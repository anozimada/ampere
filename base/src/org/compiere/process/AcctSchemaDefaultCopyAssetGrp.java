/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.math.BigDecimal;
import java.util.logging.Level;

import org.compiere.model.MAcctSchema;
import org.compiere.model.MAcctSchemaDefault;
import org.compiere.model.MSequence;
import org.compiere.util.AdempiereSystemError;
import org.compiere.util.DB;

/**
 * 	Add or Copy Acct Schema Default Accounts
 *	
 *  @author Jorg Janke
 *  @version $Id: AcctSchemaDefaultCopy.java,v 1.2 2006/07/30 00:51:01 jjanke Exp $
 */
public class AcctSchemaDefaultCopyAssetGrp extends SvrProcess
{
	/**	Acct Schema					*/
	private int			p_C_AcctSchema_ID = 0;
	/** Copy & Overwrite			*/
	private boolean 	p_CopyOverwriteAcct = false;
	
	
	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare ()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("C_AcctSchema_ID"))
				p_C_AcctSchema_ID = para[i].getParameterAsInt();
			else if (name.equals("CopyOverwriteAcct"))
				p_CopyOverwriteAcct = "Y".equals(para[i].getParameter());
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}	//	prepare
		
	/**
	 * 	Process
	 *	@return message
	 *	@throws Exception
	 */
	protected String doIt () throws Exception
	{
		log.info("C_AcctSchema_ID=" + p_C_AcctSchema_ID
			+ ", CopyOverwriteAcct=" + p_CopyOverwriteAcct);
		if (p_C_AcctSchema_ID == 0)
			throw new AdempiereSystemError("C_AcctSchema_ID=0");
		MAcctSchema as = MAcctSchema.get(getCtx(), p_C_AcctSchema_ID);
		if (as.get_ID() == 0)
			throw new AdempiereSystemError("Not Found - C_AcctSchema_ID=" + p_C_AcctSchema_ID);
		MAcctSchemaDefault acct = MAcctSchemaDefault.get (getCtx(), p_C_AcctSchema_ID);
		if (acct == null || acct.get_ID() == 0)
			throw new AdempiereSystemError("Default Not Found - C_AcctSchema_ID=" + p_C_AcctSchema_ID);
		
		String sql = null;
		int updated = 0;
		int created = 0;
		int updatedTotal = 0;
		int createdTotal = 0;
		
		
		//--> YAST_Ashley (Fixed Asset)
		// Update Fixed Asset Group
        if (p_CopyOverwriteAcct)
        {
            sql = "UPDATE GAAS_Asset_Group_Acct a "
                + "SET GAAS_Accum_Depr_Acct=" + acct.get_ValueAsInt("GAAS_Accum_Depr_Acct")
                + ", GAAS_Accum_Depr_OffSet_Acct=" + acct.get_ValueAsInt("GAAS_Accum_Depr_OffSet_Acct")
                + ", GAAS_Asset_Cost_Acct=" + acct.get_ValueAsInt("GAAS_Asset_Cost_Acct")
                + ", GAAS_Depreciation_Expense_Acct=" + acct.get_ValueAsInt("GAAS_Depreciation_Expense_Acct")
                + ", GAAS_Disposal_Gain_Acct=" + acct.get_ValueAsInt("GAAS_Disposal_Gain_Acct")
                + ", GAAS_Disposal_Loss_Acct=" + acct.get_ValueAsInt("GAAS_Disposal_Loss_Acct")
                + ", GAAS_InTransfer_Incoming_Acct=" + acct.get_ValueAsInt("GAAS_InTransfer_Incoming_Acct")
                + ", GAAS_InTransfer_Outgoing_Acct=" + acct.get_ValueAsInt("GAAS_InTransfer_Outgoing_Acct")
                + ", GAAS_Revaluation_Expense_Acct=" + acct.get_ValueAsInt("GAAS_Revaluation_Expense_Acct")
                + ", GAAS_Revaluation_Gain_Acct=" + acct.get_ValueAsInt("GAAS_Revaluation_Gain_Acct")
                + ", GAAS_Revaluation_Loss_Acct=" + acct.get_ValueAsInt("GAAS_Revaluation_Loss_Acct")
                + ", GAAS_Revaluation_OffSet_Acct=" + acct.get_ValueAsInt("GAAS_Revaluation_OffSet_Acct")
                + ", GAAS_Revaluation_Reserve_Acct=" + acct.get_ValueAsInt("GAAS_Revaluation_Reserve_Acct")
                + ", GAAS_Unidentified_Add_Acct=" + acct.get_ValueAsInt("GAAS_Unidentified_Add_Acct")
                + ", Updated=CURRENT_TIMESTAMP, UpdatedBy=0 "
                + " WHERE a.C_AcctSchema_ID=" + p_C_AcctSchema_ID
                + " AND EXISTS (SELECT * FROM GAAS_Asset_Group_Acct x "
                + " WHERE x.A_Asset_Group_ID=a.A_Asset_Group_ID)"
                + " AND a.A_Asset_Group_ID = ? ";
            updated = DB.executeUpdate(sql, getRecord_ID(), get_TrxName());
            addLog(0, null, new BigDecimal(updated), "@Updated@ @A_Asset_Group_ID@");
            updatedTotal += updated;
        }
        
        MSequence assetGroupAcctSeq = MSequence.get(getCtx(), "GAAS_Asset_Group_Acct");
        // Insert Asset Group
        sql = "INSERT INTO GAAS_Asset_Group_Acct "
            + "(GAAS_Asset_Group_Acct_ID, A_Asset_Group_ID, C_AcctSchema_ID,"
            + " AD_Client_ID, AD_Org_ID, IsActive, Created, CreatedBy, Updated, UpdatedBy,"
            + " GAAS_Accum_Depr_Acct, GAAS_Accum_Depr_OffSet_Acct, GAAS_Asset_Cost_Acct,"
            + " GAAS_Depreciation_Expense_Acct, GAAS_Disposal_Gain_Acct, GAAS_Disposal_Loss_Acct,"
            + " GAAS_InTransfer_Incoming_Acct, GAAS_InTransfer_Outgoing_Acct, GAAS_Revaluation_Expense_Acct,"
            + " GAAS_Revaluation_Gain_Acct, GAAS_Revaluation_Loss_Acct,"
            + " GAAS_Revaluation_OffSet_Acct, GAAS_Revaluation_Reserve_Acct, GAAS_Unidentified_Add_Acct) "
            + "SELECT nextID(" + assetGroupAcctSeq.getAD_Sequence_ID() + ", 'N'), "
            + " x.A_Asset_Group_ID, acct.C_AcctSchema_ID,"
            + " x.AD_Client_ID, x.AD_Org_ID, 'Y', CURRENT_TIMESTAMP, 0, CURRENT_TIMESTAMP, 0,"
            + " acct.GAAS_Accum_Depr_Acct, acct.GAAS_Accum_Depr_OffSet_Acct, acct.GAAS_Asset_Cost_Acct,"
            + " acct.GAAS_Depreciation_Expense_Acct, acct.GAAS_Disposal_Gain_Acct, acct.GAAS_Disposal_Loss_Acct,"
            + " acct.GAAS_InTransfer_Incoming_Acct, acct.GAAS_InTransfer_Outgoing_Acct, acct.GAAS_Revaluation_Expense_Acct,"
            + " acct.GAAS_Revaluation_Gain_Acct, acct.GAAS_Revaluation_Loss_Acct,"
            + " acct.GAAS_Revaluation_OffSet_Acct, acct.GAAS_Revaluation_Reserve_Acct, acct.GAAS_Unidentified_Add_Acct "
            + "FROM A_Asset_Group x"
            + " INNER JOIN C_AcctSchema_Default acct ON (x.AD_Client_ID=acct.AD_Client_ID) "
            + "WHERE acct.C_AcctSchema_ID=" + p_C_AcctSchema_ID 
            + " AND NOT EXISTS (SELECT * FROM GAAS_Asset_Group_Acct a "
            + " WHERE a.A_Asset_Group_ID=x.A_Asset_Group_ID"
            + " AND a.C_AcctSchema_ID=acct.C_AcctSchema_ID)"
            + " AND x.A_Asset_Group_ID = ? ";
         
        created = DB.executeUpdate(sql, getRecord_ID() ,get_TrxName());
        addLog(0, null, new BigDecimal(created), "@Created@ @A_Asset_Group_ID@");
        createdTotal += created;      
      
		
		return "@Created@=" + createdTotal + ", @Updated@=" + updatedTotal;
	}	//	doIt
	
}	//	AcctSchemaDefaultCopy
