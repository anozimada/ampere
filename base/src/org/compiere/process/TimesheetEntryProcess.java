/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.compiere.model.MTimesheetEntry;
import org.compiere.model.Query;

/**
 *	Process/delete timesheet entry
 */

public class TimesheetEntryProcess extends SvrProcess
{
	int timesheetId;
	int projectId;
	int resourceId;
	Timestamp fromDate = null;
	Timestamp toDate = null;

	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if ( "C_Project_ID".equals(name) )
				projectId = para[i].getParameterAsInt();
			else if ( "S_Resource_ID".equals(name) )
				resourceId = para[i].getParameterAsInt();
			else if ( "Date".equals(name) )
			{
				fromDate = para[i].getParameterAsTimestamp();
				toDate = para[i].getParameterToAsTimestamp();
			}
			
		}
		
		if ( getTable_ID() == MTimesheetEntry.Table_ID)
			timesheetId = getRecord_ID();
		
	}	//	prepare


	/**
	 *  Perform process.
	 *  @return clear Message
	 *  @throws Exception
	 */
	protected String doIt() throws java.lang.Exception
	{
		
		String where = "Processed='N' ";
		ArrayList<Object> params = new ArrayList<Object>(5);
		if ( timesheetId > 0 )
		{
			where += " AND S_TimesheetEntry_ID = ? ";
			params.add(timesheetId);
		}
		if ( projectId > 0 )
		{
			where += " AND C_Project_ID = ? ";
			params.add(projectId);
		}
		if ( resourceId > 0 )
		{
			where += " AND S_Resource_ID = ? ";
			params.add(resourceId);
		}
		
		if ( fromDate != null )
		{
			where += " AND StartDate >= ? ";
			params.add(fromDate);
		}
		
		if ( toDate != null )
		{
			where += " AND StartDate <= ? ";
			params.add(toDate);
		}
		
		
		Query query = new Query(getCtx(), MTimesheetEntry.Table_Name, where, get_TrxName())
		.setParameters(params)
		.setOnlyActiveRecords(true)
		.setApplyAccessFilter(true);
		
		List<MTimesheetEntry> entries = query.list();
		
		int count = 0;
		for (MTimesheetEntry entry : entries )
		{
			entry.process();
			count++;
		}
		
		return "@Processed@ " + count;
	}	//	doIt

}	//	TimesheetEntryProcess