/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.I_M_Product;
import org.compiere.model.I_M_Product_BOM;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MLocation;
import org.compiere.model.MLocator;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProduct;
import org.compiere.model.MProductBOM;
import org.compiere.model.MProductPrice;
import org.compiere.model.MUOM;
import org.compiere.model.MUOMConversion;
import org.compiere.model.MUser;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.model.X_I_Order;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.compiere.util.Util;

/**
 *	Import Order from I_Order
 *  @author Oscar Gomez
 * 			<li>BF [ 2936629 ] Error when creating bpartner in the importation order
 * 			<li>https://sourceforge.net/tracker/?func=detail&aid=2936629&group_id=176962&atid=879332
 * 	@author 	Jorg Janke
 * 	@version 	$Id: ImportOrder.java,v 1.2 2006/07/30 00:51:02 jjanke Exp $
 */
public class ImportOrder extends SvrProcess
{
	//private static final int DELETED_PRODUCT_CATEGORY = 1000016;
	/**	Client to be imported to		*/
	private int				m_AD_Client_ID = 0;
	/**	Organization to be imported to		*/
	private int				m_AD_Org_ID = 0;
	/**	Delete old Imported				*/
	private boolean			m_deleteOldImported = false;
	/**	Document Action					*/
	private String			m_docAction = MOrder.DOCACTION_Prepare;

	private static CLogger s_log = CLogger.getCLogger(ImportOrder.class);


	/** Effective						*/
	private Timestamp		m_DateValue = null;
	private static int def_Product_Category_ID = 0;

	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("AD_Client_ID"))
				m_AD_Client_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else if (name.equals("AD_Org_ID"))
				m_AD_Org_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else if (name.equals("DeleteOldImported"))
				m_deleteOldImported = "Y".equals(para[i].getParameter());
			else if (name.equals("DocAction"))
				m_docAction = (String)para[i].getParameter();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		if (m_DateValue == null)
			m_DateValue = new Timestamp (System.currentTimeMillis());
	}	//	prepare


	/**
	 *  Perform process.
	 *  @return Message
	 *  @throws Exception
	 */
	protected String doIt() throws java.lang.Exception
	{
		StringBuffer sql = null;
		int no = 0;
		String clientCheck = " AND AD_Client_ID=" + m_AD_Client_ID;

		//	****	Prepare	****

		//	Delete Old Imported
		if (m_deleteOldImported)
		{
			sql = new StringBuffer ("DELETE FROM I_Order "
				  + "WHERE I_IsImported='Y'").append (clientCheck);
			no = DB.executeUpdate(sql.toString(), get_TrxName());
			log.fine("Delete Old Impored =" + no);
		}

		
		prepareImportOrder(getCtx(), m_AD_Client_ID, m_AD_Org_ID, get_TrxName());
		commitEx();
		
		//	-- New BPartner ---------------------------------------------------
		//	Go through Order Records w/o C_BPartner_ID
		assignBusinessPartner(getCtx(), m_AD_Client_ID, get_TrxName());

		commitEx();
		
		//	-- New Orders -----------------------------------------------------
		String msg = processImportOrder(getCtx(), getProcessInfo(), m_AD_Client_ID, m_docAction, get_TrxName());
		return "#" + msg;
	}	//	doIt





	/**
	 * @param clientCheck
	 */
	public static void prepareImportOrder(Properties p_ctx, int AD_Client_ID, int AD_Org_ID, String trxName) {
		StringBuffer sql;
		
		String clientCheck = " AND AD_Client_ID=" + AD_Client_ID;

		sql = new StringBuffer("SELECT max(m_product_category_id) " +
				"FROM m_product_category " +
				"WHERE isdefault = 'Y' " +
				"AND   ad_org_id =  ? " +
				"AND   ad_client_id = ?");
		def_Product_Category_ID = DB.getSQLValue(null, sql.toString(), AD_Client_ID, AD_Org_ID);

		int no;
		//	Set Client, Org, IsActive, Created/Updated
		sql = new StringBuffer ("UPDATE I_Order "
			  + "SET AD_Client_ID = COALESCE (AD_Client_ID,").append (AD_Client_ID).append ("),"
			  + " AD_Org_ID = COALESCE (AD_Org_ID,").append (AD_Org_ID).append ("),"
			  + " IsActive = COALESCE (IsActive, 'Y'),"
			  + " Created = COALESCE (Created, CURRENT_TIMESTAMP),"
			  + " CreatedBy = COALESCE (CreatedBy, 0),"
			  + " Updated = COALESCE (Updated, CURRENT_TIMESTAMP),"
			  + " UpdatedBy = COALESCE (UpdatedBy, 0),"
			  + " I_ErrorMsg = ' ',"
			  + " I_IsImported = 'N' "
			  + "WHERE I_IsImported<>'Y' OR I_IsImported IS NULL");
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.info("Reset=" + no);

		sql = new StringBuffer ("UPDATE I_Order o "
			+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Org, '"
			+ "WHERE (AD_Org_ID IS NULL OR AD_Org_ID=0"
			+ " OR EXISTS (SELECT * FROM AD_Org oo WHERE o.AD_Org_ID=oo.AD_Org_ID AND (oo.IsSummary='Y' OR oo.IsActive='N')))"
			+ " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("Invalid Org=" + no);

		//	Document Type - PO - SO
		sql = new StringBuffer ("UPDATE I_Order o "	//	PO Document Type Name
			  + "SET C_DocType_ID=(SELECT C_DocType_ID FROM C_DocType d WHERE d.Name=o.DocTypeName"
			  + " AND d.DocBaseType='POO' AND o.AD_Client_ID=d.AD_Client_ID) "
			  + "WHERE C_DocType_ID IS NULL AND IsSOTrx='N' AND DocTypeName IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set PO DocType=" + no);
		sql = new StringBuffer ("UPDATE I_Order o "	//	SO Document Type Name
			  + "SET C_DocType_ID=(SELECT C_DocType_ID FROM C_DocType d WHERE d.Name=o.DocTypeName"
			  + " AND d.DocBaseType='SOO' AND o.AD_Client_ID=d.AD_Client_ID) "
			  + "WHERE C_DocType_ID IS NULL AND IsSOTrx='Y' AND DocTypeName IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set SO DocType=" + no);
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET C_DocType_ID=(SELECT C_DocType_ID FROM C_DocType d WHERE d.Name=o.DocTypeName"
			  + " AND d.DocBaseType IN ('SOO','POO') AND o.AD_Client_ID=d.AD_Client_ID) "
			//+ "WHERE C_DocType_ID IS NULL AND IsSOTrx IS NULL AND DocTypeName IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
			  + "WHERE C_DocType_ID IS NULL AND DocTypeName IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set DocType=" + no);
		sql = new StringBuffer ("UPDATE I_Order "	//	Error Invalid Doc Type Name
			  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid DocTypeName, ' "
			  + "WHERE C_DocType_ID IS NULL AND DocTypeName IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("Invalid DocTypeName=" + no);
		//	DocType Default
		sql = new StringBuffer ("UPDATE I_Order o "	//	Default PO
			  + "SET C_DocType_ID=(SELECT MAX(C_DocType_ID) FROM C_DocType d WHERE d.IsDefault='Y'"
			  + " AND d.DocBaseType='POO' AND o.AD_Client_ID=d.AD_Client_ID) "
			  + "WHERE C_DocType_ID IS NULL AND IsSOTrx='N' AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set PO Default DocType=" + no);
		sql = new StringBuffer ("UPDATE I_Order o "	//	Default SO
			  + "SET C_DocType_ID=(SELECT MAX(C_DocType_ID) FROM C_DocType d WHERE d.IsDefault='Y'"
			  + " AND d.DocBaseType='SOO' AND o.AD_Client_ID=d.AD_Client_ID) "
			  + "WHERE C_DocType_ID IS NULL AND IsSOTrx='Y' AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set SO Default DocType=" + no);
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET C_DocType_ID=(SELECT MAX(C_DocType_ID) FROM C_DocType d WHERE d.IsDefault='Y'"
			  + " AND d.DocBaseType IN('SOO','POO') AND o.AD_Client_ID=d.AD_Client_ID) "
			  + "WHERE C_DocType_ID IS NULL AND IsSOTrx IS NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Default DocType=" + no);
		sql = new StringBuffer ("UPDATE I_Order "	// No DocType
			  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No DocType, ' "
			  + "WHERE C_DocType_ID IS NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("No DocType=" + no);

		//	Set IsSOTrx
		sql = new StringBuffer ("UPDATE I_Order o SET IsSOTrx='Y' "
			  + "WHERE EXISTS (SELECT * FROM C_DocType d WHERE o.C_DocType_ID=d.C_DocType_ID AND d.DocBaseType='SOO' AND o.AD_Client_ID=d.AD_Client_ID)"
			  + " AND C_DocType_ID IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set IsSOTrx=Y=" + no);
		sql = new StringBuffer ("UPDATE I_Order o SET IsSOTrx='N' "
			  + "WHERE EXISTS (SELECT * FROM C_DocType d WHERE o.C_DocType_ID=d.C_DocType_ID AND d.DocBaseType='POO' AND o.AD_Client_ID=d.AD_Client_ID)"
			  + " AND C_DocType_ID IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set IsSOTrx=N=" + no);

		//	Price List
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET M_PriceList_ID=(SELECT MAX(M_PriceList_ID) FROM M_PriceList p WHERE p.IsDefault='Y'"
			  + " AND p.C_Currency_ID=o.C_Currency_ID AND p.IsSOPriceList=o.IsSOTrx AND o.AD_Client_ID=p.AD_Client_ID) "
			  + "WHERE M_PriceList_ID IS NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Default Currency PriceList=" + no);
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET M_PriceList_ID=(SELECT MAX(M_PriceList_ID) FROM M_PriceList p WHERE p.IsDefault='Y'"
			  + " AND p.IsSOPriceList=o.IsSOTrx AND o.AD_Client_ID=p.AD_Client_ID) "
			  + "WHERE M_PriceList_ID IS NULL AND C_Currency_ID IS NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Default PriceList=" + no);
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET M_PriceList_ID=(SELECT MAX(M_PriceList_ID) FROM M_PriceList p "
			  + " WHERE p.C_Currency_ID=o.C_Currency_ID AND p.IsSOPriceList=o.IsSOTrx AND o.AD_Client_ID=p.AD_Client_ID) "
			  + "WHERE M_PriceList_ID IS NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Currency PriceList=" + no);
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET M_PriceList_ID=(SELECT MAX(M_PriceList_ID) FROM M_PriceList p "
			  + " WHERE p.IsSOPriceList=o.IsSOTrx AND o.AD_Client_ID=p.AD_Client_ID) "
			  + "WHERE M_PriceList_ID IS NULL AND C_Currency_ID IS NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set PriceList=" + no);
		//
		sql = new StringBuffer ("UPDATE I_Order "
			  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No PriceList, ' "
			  + "WHERE M_PriceList_ID IS NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning("No PriceList=" + no);

		// @Trifon - Import Order Source
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET C_OrderSource_ID=(SELECT C_OrderSource_ID FROM C_OrderSource p"
			  + " WHERE o.C_OrderSourceValue=p.Value AND o.AD_Client_ID=p.AD_Client_ID) "
			  + "WHERE C_OrderSource_ID IS NULL AND C_OrderSourceValue IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Order Source=" + no);
		// Set proper error message
		sql = new StringBuffer ("UPDATE I_Order "
			  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Not Found Order Source, ' "
			  + "WHERE C_OrderSource_ID IS NULL AND C_OrderSourceValue IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning("No OrderSource=" + no);
		
		//	Payment Term
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET C_PaymentTerm_ID=(SELECT C_PaymentTerm_ID FROM C_PaymentTerm p"
			  + " WHERE o.PaymentTermValue=p.Value AND o.AD_Client_ID=p.AD_Client_ID) "
			  + "WHERE C_PaymentTerm_ID IS NULL AND PaymentTermValue IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set PaymentTerm=" + no);
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET C_PaymentTerm_ID=(SELECT MAX(C_PaymentTerm_ID) FROM C_PaymentTerm p"
			  + " WHERE p.IsDefault='Y' AND o.AD_Client_ID=p.AD_Client_ID) "
			  + "WHERE C_PaymentTerm_ID IS NULL AND o.PaymentTermValue IS NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Default PaymentTerm=" + no);
		//
		sql = new StringBuffer ("UPDATE I_Order "
			  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No PaymentTerm, ' "
			  + "WHERE C_PaymentTerm_ID IS NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("No PaymentTerm=" + no);

		//	Warehouse
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET M_Warehouse_ID=(SELECT MAX(M_Warehouse_ID) FROM M_Warehouse w"
			  + " WHERE o.AD_Client_ID=w.AD_Client_ID AND o.AD_Org_ID=w.AD_Org_ID) "
			  + "WHERE M_Warehouse_ID IS NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);	//	Warehouse for Org
		if (no != 0)
			s_log.fine("Set Warehouse=" + no);
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET M_Warehouse_ID=(SELECT M_Warehouse_ID FROM M_Warehouse w"
			  + " WHERE o.AD_Client_ID=w.AD_Client_ID) "
			  + "WHERE M_Warehouse_ID IS NULL"
			  + " AND EXISTS (SELECT AD_Client_ID FROM M_Warehouse w WHERE w.AD_Client_ID=o.AD_Client_ID GROUP BY AD_Client_ID HAVING COUNT(*)=1)"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.fine("Set Only Client Warehouse=" + no);
		//
		sql = new StringBuffer ("UPDATE I_Order "
			  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No Warehouse, ' "
			  + "WHERE M_Warehouse_ID IS NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("No Warehouse=" + no);

		//	BP from EMail
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET (C_BPartner_ID,AD_User_ID)=(SELECT C_BPartner_ID,AD_User_ID FROM AD_User u"
			  + " WHERE o.EMail=u.EMail AND o.AD_Client_ID=u.AD_Client_ID AND u.C_BPartner_ID IS NOT NULL) "
			  + "WHERE C_BPartner_ID IS NULL AND EMail IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set BP from EMail=" + no);
		//	BP from ContactName
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET (C_BPartner_ID,AD_User_ID)=(SELECT C_BPartner_ID,AD_User_ID FROM AD_User u"
			  + " WHERE o.ContactName=u.Name AND o.AD_Client_ID=u.AD_Client_ID AND u.C_BPartner_ID IS NOT NULL) "
			  + "WHERE C_BPartner_ID IS NULL AND ContactName IS NOT NULL"
			  + " AND EXISTS (SELECT Name FROM AD_User u WHERE o.ContactName=u.Name AND o.AD_Client_ID=u.AD_Client_ID AND u.C_BPartner_ID IS NOT NULL GROUP BY Name HAVING COUNT(*)=1)"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set BP from ContactName=" + no);
		//	BP from Value
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET C_BPartner_ID=(SELECT MAX(C_BPartner_ID) FROM C_BPartner bp"
			  + " WHERE o.BPartnerValue=bp.Value AND o.AD_Client_ID=bp.AD_Client_ID) "
			  + "WHERE C_BPartner_ID IS NULL AND BPartnerValue IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set BP from Value=" + no);
		//	Default BP
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET C_BPartner_ID=(SELECT C_BPartnerCashTrx_ID FROM AD_ClientInfo c"
			  + " WHERE o.AD_Client_ID=c.AD_Client_ID) "
			  + "WHERE C_BPartner_ID IS NULL AND BPartnerValue IS NULL AND Name IS NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Default BP=" + no);

		//	Existing Location ? Exact Match
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET (BillTo_ID,C_BPartner_Location_ID)=(SELECT C_BPartner_Location_ID,C_BPartner_Location_ID"
			  + " FROM C_BPartner_Location bpl INNER JOIN C_Location l ON (bpl.C_Location_ID=l.C_Location_ID)"
			  + " WHERE o.C_BPartner_ID=bpl.C_BPartner_ID AND bpl.AD_Client_ID=o.AD_Client_ID"
			  + " AND MD5(o.Address1)=MD5(l.Address1) AND MD5(o.Address2)=MD5(l.Address2)"
			  + " AND MD5(o.City)=MD5(l.City) AND MD5(o.Postal)=MD5(l.Postal)"
			  + " AND o.C_Region_ID=l.C_Region_ID AND o.C_Country_ID=l.C_Country_ID) "
			  + "WHERE C_BPartner_ID IS NOT NULL AND C_BPartner_Location_ID IS NULL"
			  + " AND I_IsImported='N'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Found Location=" + no);
		//	Set Bill Location from BPartner
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET BillTo_ID=(SELECT MAX(C_BPartner_Location_ID) FROM C_BPartner_Location l"
			  + " WHERE l.C_BPartner_ID=o.C_BPartner_ID AND o.AD_Client_ID=l.AD_Client_ID"
			  + " AND ((l.IsBillTo='Y' AND o.IsSOTrx='Y') OR (l.IsPayFrom='Y' AND o.IsSOTrx='N'))"
			  + ") "
			  + "WHERE C_BPartner_ID IS NOT NULL AND BillTo_ID IS NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set BP BillTo from BP=" + no);
		//	Set Location from BPartner
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET C_BPartner_Location_ID=(SELECT MAX(C_BPartner_Location_ID) FROM C_BPartner_Location l"
			  + " WHERE l.C_BPartner_ID=o.C_BPartner_ID AND o.AD_Client_ID=l.AD_Client_ID"
			  + " AND ((l.IsShipTo='Y' AND o.IsSOTrx='Y') OR o.IsSOTrx='N')"
			  + ") "
			  + "WHERE C_BPartner_ID IS NOT NULL AND C_BPartner_Location_ID IS NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set BP Location from BP=" + no);
		//
		sql = new StringBuffer ("UPDATE I_Order "
			  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No BP Location, ' "
			  + "WHERE C_BPartner_ID IS NOT NULL AND (BillTo_ID IS NULL OR C_BPartner_Location_ID IS NULL)"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("No BP Location=" + no);

		//	Set Country
		/**
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET CountryCode=(SELECT MAX(CountryCode) FROM C_Country c WHERE c.IsDefault='Y'"
			  + " AND c.AD_Client_ID IN (0, o.AD_Client_ID)) "
			  + "WHERE C_BPartner_ID IS NULL AND CountryCode IS NULL AND C_Country_ID IS NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Country Default=" + no);
		**/
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET C_Country_ID=(SELECT C_Country_ID FROM C_Country c"
			  + " WHERE o.CountryCode=c.CountryCode AND c.AD_Client_ID IN (0, o.AD_Client_ID)) "
			  + "WHERE C_BPartner_ID IS NULL AND C_Country_ID IS NULL AND CountryCode IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Country=" + no);
		//
		sql = new StringBuffer ("UPDATE I_Order "
			  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Country, ' "
			  + "WHERE C_BPartner_ID IS NULL AND C_Country_ID IS NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("Invalid Country=" + no);

		//	Set Region
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "Set RegionName=(SELECT MAX(Name) FROM C_Region r"
			  + " WHERE r.IsDefault='Y' AND r.C_Country_ID=o.C_Country_ID"
			  + " AND r.AD_Client_ID IN (0, o.AD_Client_ID)) "
			  + "WHERE C_BPartner_ID IS NULL AND C_Region_ID IS NULL AND RegionName IS NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Region Default=" + no);
		//
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "Set C_Region_ID=(SELECT C_Region_ID FROM C_Region r"
			  + " WHERE r.Name=o.RegionName AND r.C_Country_ID=o.C_Country_ID"
			  + " AND r.AD_Client_ID IN (0, o.AD_Client_ID)) "
			  + "WHERE C_BPartner_ID IS NULL AND C_Region_ID IS NULL AND RegionName IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Region=" + no);
		//
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Region, ' "
			  + "WHERE C_BPartner_ID IS NULL AND C_Region_ID IS NULL "
			  + " AND EXISTS (SELECT * FROM C_Country c"
			  + " WHERE c.C_Country_ID=o.C_Country_ID AND c.HasRegion='Y')"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("Invalid Region=" + no);

		//	for Set Ship Country
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET Ship_C_Country_ID=(SELECT C_Country_ID FROM C_Country c"
			  + " WHERE o.ShipCountryCode=c.CountryCode AND c.AD_Client_ID IN (0, o.AD_Client_ID)) "
			  + " WHERE Ship_C_Country_ID IS NULL AND ShipCountryCode IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Ship Country=" + no);
		//
		sql = new StringBuffer ("UPDATE I_Order "
			  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Ship Country, ' "
			  + " WHERE Ship_C_Country_ID IS NULL AND ShipCountryCode IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("Invalid Ship Country=" + no);

		// for Set Ship Region
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "Set Ship_C_Region_ID=(SELECT MAX(C_Region_ID) FROM C_Region r"
			  + " WHERE r.IsDefault='Y' AND r.C_Country_ID=o.Ship_C_Country_ID"
			  + " AND r.AD_Client_ID IN (0, o.AD_Client_ID)) "
			  + " WHERE Ship_C_Country_ID IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Ship Region Default=" + no);
		//
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "Set Ship_C_Region_ID=(SELECT C_Region_ID FROM C_Region r"
			  + " WHERE r.Name=o.ShipRegionName AND r.C_Country_ID=o.Ship_C_Country_ID"
			  + " AND r.AD_Client_ID IN (0, o.AD_Client_ID)) "
			  + " WHERE ShipRegionName IS NOT NULL AND Ship_C_Country_ID IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Ship Region=" + no);
		//
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Ship Region, ' "
			  + "WHERE Ship_C_Region_ID IS NULL "
			  + " AND EXISTS (SELECT * FROM C_Country c"
			  + " WHERE c.C_Country_ID=o.Ship_C_Country_ID AND c.HasRegion='Y')"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("Invalid Ship Region=" + no);
		
		//	for Ship Existing Location ? Exact Match
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET Ship_Location_ID=(SELECT MAX(bpl.C_BPartner_Location_ID) "
			  + " FROM C_BPartner_Location bpl INNER JOIN C_Location l ON (bpl.C_Location_ID=l.C_Location_ID)"
			  + " WHERE o.C_BPartner_ID=bpl.C_BPartner_ID AND bpl.AD_Client_ID=o.AD_Client_ID"
			  + " AND MD5(o.ShipAddress1)=MD5(l.Address1) AND MD5(o.ShipAddress2)=MD5(l.Address2) "
//			  + " AND MD5(o.ShipCity)=MD5(l.City) AND MD5(o.ShipPostCode)=MD5(l.Postal) "
			  + ") "
			  + "WHERE C_BPartner_ID IS NOT NULL AND Ship_Location_ID IS NULL"
			  + " AND I_IsImported='N'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Found Ship BP Location=" + no);

		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET Ship_C_Location_ID=(SELECT MAX(bpl.C_Location_ID) "
			  + " FROM C_BPartner_Location bpl INNER JOIN C_Location l ON (bpl.C_Location_ID=l.C_Location_ID)"
			  + " WHERE o.C_BPartner_ID=bpl.C_BPartner_ID AND bpl.AD_Client_ID=o.AD_Client_ID"
			  + " AND MD5(o.ShipAddress1)=MD5(l.Address1))"
			  + " WHERE C_BPartner_ID IS NOT NULL AND Ship_C_Location_ID IS NULL AND ShipAddress1 IS NOT NULL"
			  + " AND I_IsImported='N'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Found Ship Location=" + no);

		//	Ship ContactName from BP
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET ShipAD_User_ID = (SELECT AD_User_ID FROM AD_User u"
			  + " WHERE o.ShipContactName=u.Name AND o.AD_Client_ID=u.AD_Client_ID AND u.C_BPartner_ID=o.C_BPartner_ID AND o.C_BPartner_ID IS NOT NULL) "
			  + " WHERE I_IsImported<>'Y' AND ShipAD_User_ID IS NULL ").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Ship ContactName from BP=" + no);

		sql = new StringBuffer ("UPDATE I_Order o "
				  + "SET C_Location_ID=(SELECT MAX(bpl.C_Location_ID) "
				  + " FROM C_BPartner_Location bpl INNER JOIN C_Location l ON (bpl.C_Location_ID=l.C_Location_ID)"
				  + " WHERE o.C_BPartner_ID=bpl.C_BPartner_ID AND bpl.AD_Client_ID=o.AD_Client_ID"
				  + " AND MD5(o.Address1)=MD5(l.Address1))"
				  + " WHERE C_BPartner_ID IS NOT NULL AND C_Location_ID IS NULL AND Address1 IS NOT NULL"
				  + " AND I_IsImported='N'").append (clientCheck);
			no = DB.executeUpdate(sql.toString(), trxName);
			s_log.fine("Found Ship Location=" + no);
			
		//	Product
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET M_Product_ID=(SELECT MAX(M_Product_ID) FROM M_Product p"
			  + " WHERE o.ProductValue=p.Value AND o.AD_Client_ID=p.AD_Client_ID) "
			  + "WHERE M_Product_ID IS NULL AND ProductValue IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Product from Value=" + no);
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET M_Product_ID=(SELECT MAX(M_Product_ID) FROM M_Product p"
			  + " WHERE o.UPC=p.UPC AND o.AD_Client_ID=p.AD_Client_ID) "
			  + "WHERE M_Product_ID IS NULL AND UPC IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Product from UPC=" + no);
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET M_Product_ID=(SELECT MAX(M_Product_ID) FROM M_Product p"
			  + " WHERE o.SKU=p.SKU AND o.AD_Client_ID=p.AD_Client_ID) "
			  + "WHERE M_Product_ID IS NULL AND SKU IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Product from SKU=" + no);
		
		
		sql = new StringBuffer ("UPDATE I_Order o "
				  + "SET M_Product_ID=(SELECT MAX(M_Product_ID) FROM M_Product p"
				  + " WHERE o.Ext_Product_ID=p.Ext_Product_ID AND o.AD_Client_ID=p.AD_Client_ID) "
				  + "WHERE M_Product_ID IS NULL AND Ext_Product_ID IS NOT NULL"
				  + " AND I_IsImported<>'Y'").append (clientCheck);
			no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Product from External Product ID =" + no);
			
		assignProduct(p_ctx, AD_Client_ID, trxName);
		assignProductBOM(p_ctx, AD_Client_ID, trxName);
		
		sql = new StringBuffer ("UPDATE I_Order "
			  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Product, ' "
			  + "WHERE M_Product_ID IS NULL AND (ProductValue IS NOT NULL OR UPC IS NOT NULL OR SKU IS NOT NULL)"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("Invalid Product=" + no);

		//	Charge
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET C_Charge_ID=(SELECT C_Charge_ID FROM C_Charge c"
			  + " WHERE o.ChargeName=c.Name AND o.AD_Client_ID=c.AD_Client_ID) "
			  + "WHERE C_Charge_ID IS NULL AND ChargeName IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Charge=" + no);
		sql = new StringBuffer ("UPDATE I_Order "
				  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Charge, ' "
				  + "WHERE C_Charge_ID IS NULL AND (ChargeName IS NOT NULL)"
				  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("Invalid Charge=" + no);
		//
		
		sql = new StringBuffer ("UPDATE I_Order "
				  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Product and Charge, ' "
				  + "WHERE M_Product_ID IS NOT NULL AND C_Charge_ID IS NOT NULL "
				  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("Invalid Product and Charge exclusive=" + no);

		//	Tax
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET C_Tax_ID=(SELECT MAX(C_Tax_ID) FROM C_Tax t"
			  + " WHERE o.TaxIndicator=t.TaxIndicator AND o.AD_Client_ID=t.AD_Client_ID) "
			  + "WHERE C_Tax_ID IS NULL AND TaxIndicator IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		s_log.fine("Set Tax=" + no);
		sql = new StringBuffer ("UPDATE I_Order "
			  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Tax, ' "
			  + "WHERE C_Tax_ID IS NULL AND TaxIndicator IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("Invalid Tax=" + no);

		// duplicate documentno
		sql = new StringBuffer ("UPDATE I_Order o " +
				"SET I_IsImported = 'E', I_ErrorMsg=I_ErrorMsg||'ERR=Duplicate documentno, ' " +
				"WHERE o.DocumentNo IS NOT NULL " +
				"AND o.C_BPartner_ID IS NOT NULL " +
				"AND EXISTS (SELECT 1 FROM C_Order co " +
				"	WHERE co.C_DocType_ID=o.C_DocType_ID " +
				"	AND co.DocumentNo=o.DocumentNo " +
				"	AND co.C_BPartner_ID=o.C_BPartner_ID) " +
				"AND o.I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("Duplicate document no =" + no);
		


		//error on at least 1 line
		sql = new StringBuffer ("UPDATE I_Order o " +
				"SET I_IsImported = 'E', I_ErrorMsg=I_ErrorMsg||'ERR=Error on at least 1 line, ' " +
				"WHERE o.DocumentNo IS NOT NULL " +
//				"AND o.C_BPartner_ID IS NOT NULL " +
				"AND EXISTS (SELECT 1 FROM I_Order ox " +
				"	WHERE ox.I_IsImported = 'E' " +
				"	AND o.DocumentNo=ox.DocumentNo " +
//				"	AND o.C_BPartner_ID=ox.C_BPartner_ID) " + 				
				"	) " + 				
				"AND o.I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		if (no != 0)
			s_log.warning ("Error on at least 1 line =" + no);
	}


	public static void assignProduct(Properties p_ctx, int AD_Client_ID, String trxName) {
		String clientCheck = " AND AD_Client_ID=" + AD_Client_ID;

		StringBuffer sql = new StringBuffer ("SELECT * FROM I_Order "
//				  + "WHERE I_IsImported<>'Y' AND M_Product_ID IS NULL").append (clientCheck);
				  + "WHERE I_IsImported<>'Y' AND C_Charge_ID IS NULL ").append (clientCheck);

		int no = 0;
		
		try
		{
			
			int C_TaxCategory_ID = DB.getSQLValue(trxName, "SELECT MAX(C_TaxCategory_ID) FROM C_TaxCategory WHERE IsDefault='Y'" + clientCheck);
			
			PreparedStatement pstmt = DB.prepareStatement (sql.toString(), trxName);
			ResultSet rs = pstmt.executeQuery ();
			while (rs.next ()) {
				X_I_Order imp = new X_I_Order (p_ctx, rs, trxName);
				if (imp.getProductValue() != null) {
					
					MProduct[] products = MProduct.get(p_ctx, "value = '" + imp.getProductValue() +"' ", trxName);
					MProduct product = null;
					if (products.length > 0) {
						product = products[0];
						//reset Product Price based on Web Order pricelist
						int M_PriceList_Version_ID = DB.getSQLValue(trxName, "SELECT MAX(M_PriceList_Version_ID) FROM M_PriceList_Version WHERE M_PriceList_ID=?", imp.getM_PriceList_ID());
						MProductPrice pp = MProductPrice.get(p_ctx, M_PriceList_Version_ID, product.getM_Product_ID(), trxName);
						if (pp == null) {
							pp = new MProductPrice(p_ctx, M_PriceList_Version_ID, product.getM_Product_ID(), imp.getPriceActual()
									,   imp.getPriceActual(),  imp.getPriceActual(), trxName);
							pp.saveEx();
						} else {
							//do not override existing price
//							pp.setPrices(imp.getPriceActual(), imp.getPriceActual(),imp.getPriceActual());
//							pp.saveEx();
						}
					} else {
						product = new MProduct(p_ctx, 0, trxName);
						product.setAD_Client_ID(AD_Client_ID);
						product.setValue(imp.getProductValue());
						product.setName((imp.getProductValue()));
						product.setC_UOM_ID(MUOM.getDefault_UOM_ID(p_ctx));
						product.setM_Product_Category_ID(def_Product_Category_ID);
						MLocator defLocator = MLocator.getDefault((MWarehouse) imp.getM_Warehouse());
						if (defLocator == null) {
							s_log.warning("No Default Locator set for Warehouse " + imp.getM_Warehouse().getName());
							throw new AdempiereException("No Default Locator set for Warehouse " + imp.getM_Warehouse().getName());
						}
						product.setM_Locator_ID(defLocator.getM_Locator_ID());
						product.setC_TaxCategory_ID(C_TaxCategory_ID);
						product.setDescription("Not found in MProduct. Default Product created");
						product.set_ValueOfColumn("Ext_Product_ID", imp.get_ValueAsString("Ext_Product_ID"));
						product.saveEx();
						int M_PriceList_Version_ID = DB.getSQLValue(trxName, "SELECT MAX(M_PriceList_Version_ID) FROM M_PriceList_Version WHERE M_PriceList_ID=?", imp.getM_PriceList_ID());
						MProductPrice pp = new MProductPrice(p_ctx, M_PriceList_Version_ID, product.getM_Product_ID(), imp.getPriceActual()
								,   imp.getPriceActual(),  imp.getPriceActual(), trxName);
						pp.saveEx();
					}
					imp.setM_Product_ID(product.getM_Product_ID());
					imp.save();
				}
			}
		
			rs.close ();
			pstmt.close ();
		} catch(SQLException e) {
			s_log.log(Level.SEVERE, "Product - " + sql.toString(), e);
		}

	}
	
	public static void assignProductBOM(Properties p_ctx, int AD_Client_ID, String trxName) {
		String clientCheck = " AND AD_Client_ID=" + AD_Client_ID;

		StringBuffer sql = new StringBuffer ("SELECT * FROM I_Order "
//				  + "WHERE I_IsImported<>'Y' AND M_Product_ID IS NULL").append (clientCheck);
				  + "WHERE I_IsImported<>'Y' AND Ext_Product_BOM_ID IS NOT NULL ").append (clientCheck);

		sql.append(" ORDER BY Ext_Product_BOM_ID ");
		int no = 0;
		
		try
		{
			
			int C_TaxCategory_ID = DB.getSQLValue(trxName, "SELECT MAX(C_TaxCategory_ID) FROM C_TaxCategory WHERE IsDefault='Y'" + clientCheck);
			
			PreparedStatement pstmt = DB.prepareStatement (sql.toString(), trxName);
			ResultSet rs = pstmt.executeQuery ();
			MProduct bom = null; 
			List <MProductBOM> bomLines = null;
			while (rs.next ()) {
				X_I_Order imp = new X_I_Order (p_ctx, rs, trxName);
				if (bom == null || !bom.get_ValueAsString("Ext_Product_ID").equals(imp.get_ValueAsString("Ext_Product_BOM_ID")))
				{
					bom = new Query(p_ctx, I_M_Product.Table_Name, "Ext_Product_ID=?", trxName)
			    	.setParameters(imp.get_ValueAsString("Ext_Product_BOM_ID"))
			    	.setClient_ID()
			    	.setOrderBy("AD_Org_ID")
			    	.setOnlyActiveRecords(true)
			    	.firstOnly();

					bom.setProductType(MProduct.PRODUCTTYPE_Service);
					bom.setIsBOM(true);
					bom.save();
				}
				final String whereClause = "M_Product_ID=? AND Ext_BOM_Option_ID=? AND Ext_BOM_Option_Value=?";
				bomLines = new Query(p_ctx, I_M_Product_BOM.Table_Name, whereClause, trxName)
				.setParameters(bom.getM_Product_ID()
						, imp.get_ValueAsString("Ext_BOM_Option_ID")
						, imp.get_ValueAsString("Ext_BOM_Option_Value"))
				.setOrderBy("Line")
				.list();
				
				if (bomLines.size() == 0) 
				{
					MProductBOM bomLine = new MProductBOM(p_ctx, 0, trxName);
					bomLine.setM_Product_ID(bom.getM_Product_ID());
					bomLine.setBOMQty((BigDecimal)imp.get_Value("BOMQty"));
					bomLine.setM_ProductBOM_ID(imp.getM_Product_ID());
					bomLine.set_ValueOfColumn("Ext_BOM_Option_ID", imp.get_ValueAsString("Ext_BOM_Option_ID"));
					bomLine.set_ValueOfColumn("Ext_BOM_Option_Value", imp.get_ValueAsString("Ext_BOM_Option_Value"));
					bomLine.setBOMType(imp.get_ValueAsString("BOMType"));
					String sql0 = "SELECT MAX(line) FROM M_Product_BOM WHERE M_Product_ID=?";
					int lineNo = DB.getSQLValue(trxName, sql0, bom.getM_Product_ID()) ;
					bomLine.setLine(lineNo + 10);
					bomLine.save();
				}
			}
		
			rs.close ();
			pstmt.close ();
		} catch(SQLException e) {
			s_log.log(Level.SEVERE, "Product - " + sql.toString(), e);
		}

	}
	public static void assignBusinessPartner(Properties p_ctx, int AD_Client_ID, String trxName) {
		
		String clientCheck = " AND AD_Client_ID=" + AD_Client_ID;
		//	Go through Order Records w/o C_BPartner_ID
		StringBuffer sql = new StringBuffer ("SELECT * FROM I_Order "
			  + "WHERE I_IsImported='N' AND C_BPartner_ID IS NULL").append (clientCheck);
		
		int no = 0;
		try
		{
			PreparedStatement pstmt = DB.prepareStatement (sql.toString(), trxName);
			ResultSet rs = pstmt.executeQuery ();
			while (rs.next ()) {
				X_I_Order imp = new X_I_Order (p_ctx, rs, trxName);
				if (imp.getBPartnerValue () == null)
				{
					if (imp.getEMail () != null)
						imp.setBPartnerValue (imp.getEMail ());
					else if (imp.getName () != null)
						imp.setBPartnerValue (imp.getName ());
					else
						continue;
				}
				if (imp.getName () == null)
				{
					if (imp.getContactName () != null)
						imp.setName (imp.getContactName ());
					else
						imp.setName (imp.getBPartnerValue ());
				}
				//	BPartner
				MBPartner bp = MBPartner.get (p_ctx, imp.getBPartnerValue(), trxName);
				if (bp == null)
				{
					bp = new MBPartner (p_ctx, -1, trxName);
					bp.setClientOrg (imp.getAD_Client_ID (), imp.getAD_Org_ID ());
					bp.setValue (imp.getBPartnerValue ());
					bp.setC_BP_Group_ID(imp.get_ValueAsInt("C_BP_Group_ID"));
					bp.setName (imp.getName ());
					if (!bp.save ())
						continue;
				}
				imp.setC_BPartner_ID (bp.getC_BPartner_ID ());
				
				//	BP Location
				MBPartnerLocation bpl = null; 
				MBPartnerLocation[] bpls = bp.getLocations(true);
				for (int i = 0; bpl == null && i < bpls.length; i++)
				{
					if (imp.getC_BPartner_Location_ID() == bpls[i].getC_BPartner_Location_ID())
						bpl = bpls[i];
					//	Same Location ID
					else if (imp.getC_Location_ID() == bpls[i].getC_Location_ID())
						bpl = bpls[i];
					//	Same Location Info
					else if (imp.getC_Location_ID() == 0)
					{
						MLocation loc = bpls[i].getLocation(false);
						if (loc.equals(imp.getC_Country_ID(), imp.getC_Region_ID(), 
								imp.getPostal(), "", imp.getCity(), 
								imp.getAddress1(), imp.getAddress2()))
							bpl = bpls[i];
					}
				}
				if (bpl == null)
				{
					//	New Location
					MLocation loc = new MLocation (p_ctx, 0, trxName);
					loc.setAddress1 (imp.getAddress1 ());
					loc.setAddress2 (imp.getAddress2 ());
					loc.setAddress3 (imp.get_ValueAsString("Address3"));
					loc.setAddress4 (imp.get_ValueAsString("Address4"));
					loc.setCity (imp.getCity ());
					loc.setPostal (imp.getPostal ());
					if (imp.getC_Region_ID () != 0)
						loc.setC_Region_ID (imp.getC_Region_ID ());
					loc.setC_Country_ID (imp.getC_Country_ID ());
					if (!loc.save ())
						continue;
					//
					bpl = new MBPartnerLocation (bp);
					bpl.setC_Location_ID (loc.getC_Location_ID ());
					if (!bpl.save ())
						continue;
				}
				imp.setC_Location_ID (bpl.getC_Location_ID ());
				imp.setBillTo_ID (bpl.getC_BPartner_Location_ID ());
				imp.setC_BPartner_Location_ID (bpl.getC_BPartner_Location_ID ());
				
				createShipBPLocation(p_ctx, imp, trxName);
				

				//	User/Contact
				if (imp.getContactName () != null 
					|| imp.getEMail () != null 
					|| imp.getPhone () != null)
				{
					MUser[] users = bp.getContacts(true);
					MUser user = null;
					MUser shipper = null;
					for (int i = 0; user == null && i < users.length;  i++)
					{
						String name = users[i].getName();
						if (name.equals(imp.getContactName()) 
							|| name.equals(imp.getName()))
						{
							user = users[i];
							imp.setAD_User_ID (user.getAD_User_ID ());
						}
						if (imp.get_ValueAsString("ShipContactName") != null
							&&	name.equals(imp.get_ValueAsString("ShipContactName")))
						{
							shipper = users[i];
							imp.set_ValueOfColumn("ShipAD_User_ID", shipper.getAD_User_ID ());
						}
					}
					if (user == null)
					{
						user = new MUser (bp);
						if (imp.getContactName () == null)
							user.setName (imp.getName ());
						else
							user.setName (imp.getContactName ());
						user.setEMail (imp.getEMail ());
						user.setPhone (imp.getPhone ());
						if (user.save ())
							imp.setAD_User_ID (user.getAD_User_ID ());
					}
					if (shipper == null) {
						createShipUser(p_ctx, imp, trxName);
					}
				}
				imp.save ();

			}
			rs.close ();
			pstmt.close ();
		} catch(SQLException e) {
			s_log.log(Level.SEVERE, "BP - " + sql.toString(), e);
		}
		sql = new StringBuffer ("UPDATE I_Order "
				  + "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No BPartner, ' "
				  + "WHERE C_BPartner_ID IS NULL"
				  + " AND I_IsImported<>'Y'").append (clientCheck);
			no = DB.executeUpdate(sql.toString(), trxName);
			if (no != 0)
				s_log.warning ("No BPartner=" + no);
			
	}
	
	
	/**
	 * Create order in one block
	 * make sure all lines are included, otherwise throw an error for the whole order
	 * @param p_ctx
	 * @param importLines
	 * @param p_docAction
	 * @return
	 */
	private static int createOrder(Properties p_ctx, ArrayList<X_I_Order> importLines, String p_docAction, String trxName) {

		X_I_Order imp = importLines.get(0);
		Trx trx = Trx.get(trxName, true);
		MOrder order = new MOrder (p_ctx, 0, trx.getTrxName());
		
		int noInsertLine = 0;
		int counter = 0;

		try
		{
					createShipBPLocation(p_ctx, imp, trx.getTrxName());
					createShipUser(p_ctx, imp, trx.getTrxName());

					order.setClientOrg (imp.getAD_Client_ID(), imp.getAD_Org_ID());
					order.setC_DocTypeTarget_ID(imp.getC_DocType_ID());
					order.setIsSOTrx(imp.isSOTrx());
					if (imp.getDeliveryRule() != null ) {
						order.setDeliveryRule(imp.getDeliveryRule());
					}
					if (imp.getDocumentNo() != null)
						order.setDocumentNo(imp.getDocumentNo());
					//	Ship Partner
					order.setC_BPartner_ID(imp.getC_BPartner_ID());
					order.setC_BPartner_Location_ID(imp.getC_BPartner_Location_ID());
					if (imp.getAD_User_ID() != 0)
						order.setAD_User_ID(imp.getAD_User_ID());
					if (imp.get_ValueAsInt("ShipAD_User_ID") != 0)
						order.setAD_User_ID(imp.get_ValueAsInt("ShipAD_User_ID"));
					if (imp.getAD_User_ID() != 0)
						order.setBill_User_ID(imp.getAD_User_ID());
					
					//	Bill Partner
					order.setBill_BPartner_ID(imp.getC_BPartner_ID());
					order.setBill_Location_ID(imp.getBillTo_ID());
					//
					if (imp.getDescription() != null)
						order.setDescription(imp.getDescription());
					order.setC_PaymentTerm_ID(imp.getC_PaymentTerm_ID());
					order.setM_PriceList_ID(imp.getM_PriceList_ID());
					order.setM_Warehouse_ID(imp.getM_Warehouse_ID());
					if (imp.getM_Shipper_ID() != 0) {
						order.setM_Shipper_ID(imp.getM_Shipper_ID());
						order.setDeliveryViaRule(MOrder.DELIVERYVIARULE_Shipper);
					}
					//	SalesRep from Import or the person running the import
					if (imp.getSalesRep_ID() != 0)
						order.setSalesRep_ID(imp.getSalesRep_ID());
					if (order.getSalesRep_ID() == 0)
						order.setSalesRep_ID(Env.getAD_User_ID(p_ctx)); //TODO - create Env.getSalesRep_ID()
					//
					if (imp.getAD_OrgTrx_ID() != 0)
						order.setAD_OrgTrx_ID(imp.getAD_OrgTrx_ID());
					if (imp.getC_Activity_ID() != 0)
						order.setC_Activity_ID(imp.getC_Activity_ID());
					if (imp.getC_Campaign_ID() != 0)
						order.setC_Campaign_ID(imp.getC_Campaign_ID());
					if (imp.getC_Project_ID() != 0)
						order.setC_Project_ID(imp.getC_Project_ID());
					//
					if (imp.getDateOrdered() != null)
						order.setDateOrdered(imp.getDateOrdered());
					if (imp.getDateAcct() != null)
						order.setDateAcct(imp.getDateAcct());
					
					// Set Order Source
					if (imp.getC_OrderSource() != null)
						order.setC_OrderSource_ID(imp.getC_OrderSource_ID());
					
					if (imp.get_ValueAsString("HeaderRef") != null) {
						order.set_ValueOfColumn("HeaderRef", imp.get_ValueAsString("HeaderRef"));
					}
					//
					order.saveEx();
			int lineNo = 10;
			counter = 0;

			for ( X_I_Order iLine : importLines )
			{
				//	New OrderLine
				MOrderLine line = new MOrderLine (order);
				line.setLine(lineNo);
				lineNo += 10;
				if (iLine.getM_Product_ID() != 0)
					line.setM_Product_ID(iLine.getM_Product_ID(), true);
				if (iLine.getC_Charge_ID() != 0)
					line.setC_Charge_ID(iLine.getC_Charge_ID());
				line.setC_UOM_ID(iLine.getC_UOM_ID());
				line.setQtyEntered(iLine.getQtyOrdered());
				if (iLine.get_ValueAsBoolean("IsBOM")) {
					line.set_ValueOfColumn("IsBOM", "Y");
					line.setQtyOrdered(Env.ZERO);
				} else {
					BigDecimal ordered = MUOMConversion.convertProductFrom(iLine.getCtx(), iLine.getM_Product_ID(), iLine.getC_UOM_ID(), iLine.getQtyOrdered());
					line.setQtyOrdered(ordered);
					line.set_ValueOfColumn("IsBOM", "N");
				}
					
				line.setPrice();
				if (iLine.getPriceActual().compareTo(Env.ZERO) != 0)
					line.setPrice(iLine.getPriceActual());
				if (iLine.getC_Tax_ID() != 0)
					line.setC_Tax_ID(iLine.getC_Tax_ID());
				else
				{
					line.setTax();
					iLine.setC_Tax_ID(line.getC_Tax_ID());
				}
				if (iLine.getFreightAmt() != null)
					line.setFreightAmt(iLine.getFreightAmt());
				if (iLine.getLineDescription() != null)
					line.setDescription(iLine.getLineDescription());
				if (iLine.get_ValueAsString("LineRef") != null) {
					line.set_ValueOfColumn("LineRef", iLine.get_ValueAsString("LineRef"));
				}				
				line.saveEx();
				
					noInsertLine++;
				counter++;

				iLine.setC_Order_ID(line.getC_Order_ID());
				iLine.setC_OrderLine_ID(line.getC_OrderLine_ID());
				iLine.setI_IsImported(true);
				iLine.setProcessed(true);

				}
			
				if (p_docAction != null && p_docAction.length() > 0)
				{
					order.setDocAction(p_docAction);
					order.processIt (p_docAction);
				}
				order.saveEx();


//			trx.commit();
			}
		catch (Exception e)
		{
			for (int i = 0; i < importLines.size() ; i++ )
			{
				
				X_I_Order iLine = importLines.get(i);
				noInsertLine = 0;
				iLine.setC_Order_ID(0);
				iLine.setC_OrderLine_ID(0);
				iLine.setI_IsImported(false);
				iLine.setProcessed(false);
				if ( i == counter )				
					iLine.setI_ErrorMsg(e.getLocalizedMessage());
				else
					iLine.setI_ErrorMsg("Related order line failed.");
			}
//			if ( trx != null )
//			{
//				trx.rollback();
//			}
		}
//		finally
//		{ 
//			if ( trx != null )
//			{
//				trx.close();	
//				trx = null;
//			}
//		}

		for (X_I_Order iLine : importLines )
		{
			iLine.save();
		}

		return noInsertLine;
		}

	/**
	 * Check Ship Address1 if not present then create new 
	 * 
	 * @param imp
	 * @param trx
	 */
	private static void createShipBPLocation(Properties p_ctx, X_I_Order imp, String trxName)
	{
		if (!(imp.get_ValueAsInt("Ship_Location_ID") != 0 || imp.get_ValueAsInt("Ship_C_Location_ID") != 0 || !Util.isEmpty(imp.get_ValueAsString("ShipAddress1"))))
			return;
		
		// BP Location
		MBPartner bp = new MBPartner(p_ctx, imp.getC_BPartner_ID(), trxName);
		MBPartnerLocation bpl = null;
		MBPartnerLocation[] bpls = bp.getLocations(true);
		for (int i = 0; bpl == null && i < bpls.length; i++)
		{
			if (imp.get_ValueAsInt("Ship_Location_ID") == bpls[i].getC_BPartner_Location_ID())
				bpl = bpls[i];
			// Same Location ID
			else if (imp.get_ValueAsInt("Ship_C_Location_ID") == bpls[i].getC_Location_ID())
				bpl = bpls[i];
			// Same Location Info
			else if (imp.get_ValueAsInt("Ship_C_Location_ID") == 0)
			{
				MLocation loc = bpls[i].getLocation(false);
				if (loc.getAddress1() != null && loc.getAddress1().equals(imp.get_ValueAsString("ShipAddress1")))
					bpl = bpls[i];
			}
		}
		if (bpl == null)
		{
			// New Location
			MLocation loc = new MLocation(p_ctx, 0, trxName);
			loc.setAddress1(imp.get_ValueAsString("ShipAddress1"));
			loc.setAddress2(imp.get_ValueAsString("ShipAddress2"));
			loc.setAddress3(imp.get_ValueAsString("ShipAddress3"));
			loc.setAddress4(imp.get_ValueAsString("ShipAddress4"));
			loc.setCity(imp.get_ValueAsString("ShipCity"));
			loc.setPostal(imp.get_ValueAsString("ShipPostCode"));
			if (imp.get_ValueAsInt("Ship_C_Region_ID") != 0)
				loc.setC_Region_ID(imp.get_ValueAsInt("Ship_C_Region_ID"));
			loc.setC_Country_ID(imp.get_ValueAsInt("Ship_C_Country_ID"));
			loc.save();
			//
			bpl = new MBPartnerLocation(bp);
			bpl.setC_Location_ID(loc.getC_Location_ID());
			bpl.setIsShipTo(true);
			bpl.setIsRemitTo(false);
			bpl.setIsPayFrom(false);
			bpl.setIsBillTo(false);
			bpl.save();
		}
		imp.set_ValueOfColumn("Ship_C_Location_ID", bpl.getC_Location_ID());
		imp.set_ValueOfColumn("Ship_Location_ID", bpl.getC_BPartner_Location_ID());
		imp.setC_BPartner_Location_ID(bpl.getC_BPartner_Location_ID());
		imp.save();
	} // createBPLocation
	
	/**
	 * Create or Set Ship Contact Name
	 * @param imp
	 * @param trxName
	 */
	private static void createShipUser(Properties p_ctx, X_I_Order imp, String trxName)
	{
		if (imp.get_ValueAsInt("ShipAD_User_ID") == 0 && Util.isEmpty(imp.get_ValueAsString("ShipContactName")))
			return;
		else if (imp.get_ValueAsInt("ShipAD_User_ID") != 0)
		{
		}
		else
		{
			int user_id = DB.getSQLValue(trxName, "SELECT AD_User_ID FROM AD_User where name = ? AND C_BPartner_ID= ?", imp.get_ValueAsString("ShipContactName"), imp.getC_BPartner_ID() );
			if (user_id <= 0) {
				MUser newUser = new MUser(p_ctx, -1, trxName);
				newUser.setName(imp.get_ValueAsString("ShipContactName"));
				newUser.setC_BPartner_ID(imp.getC_BPartner_ID());
				newUser.save();
				imp.set_ValueOfColumn("ShipAD_User_ID", newUser.getAD_User_ID());
			} else {
				imp.set_ValueOfColumn("ShipAD_User_ID", user_id);
			}
			imp.save();
		}
	} // createShipUser

	
	public static String processImportOrder(Properties p_ctx, ProcessInfo pi, int AD_Client_ID, String p_DocAction, String trxName) {
		
		String clientCheck = " AND AD_Client_ID=" + AD_Client_ID;
		//	Go through Order Records w/o C_BPartner_ID
		StringBuffer sql = null;

		int noInsert = 0;
		int noInsertLine = 0;
		int no = 0;

		StringBuffer error = new StringBuffer();
		
		//	Go through Order Records w/o Error
		sql = new StringBuffer ("SELECT * FROM I_Order "
			  + "WHERE I_IsImported='N'").append (clientCheck)
			.append(" ORDER BY C_BPartner_ID, BillTo_ID, C_BPartner_Location_ID, DocumentNo, I_Order_ID");
		try
		{
			PreparedStatement pstmt = DB.prepareStatement (sql.toString(), trxName);
			ResultSet rs = pstmt.executeQuery ();
			//
			int oldC_BPartner_ID = 0;
			int oldBillTo_ID = 0;
			int oldC_BPartner_Location_ID = 0;
			String oldDocumentNo = "";
			//
			ArrayList<X_I_Order> importLines = new ArrayList<X_I_Order>();  // accumulator for import lines related to one order
			while (rs.next ())
			{
				X_I_Order imp = new X_I_Order (p_ctx, rs, trxName);
				String cmpDocumentNo = imp.getDocumentNo();
				if (cmpDocumentNo == null)
					cmpDocumentNo = "";
				//	New Order
				if (oldC_BPartner_ID != imp.getC_BPartner_ID() 
					|| oldC_BPartner_Location_ID != imp.getC_BPartner_Location_ID()
					|| oldBillTo_ID != imp.getBillTo_ID() 
					|| !oldDocumentNo.equals(cmpDocumentNo))
				{
					if ( !importLines.isEmpty() )
					{
						int insertedLines = createOrder(Env.getCtx(), importLines, p_DocAction, trxName);
						if ( insertedLines > 0 )
						{
							noInsert++;
							noInsertLine += insertedLines;
						} else {
							//failed to create record
							String docNo = "";
							for ( X_I_Order iLine : importLines ) {
								if (!iLine.getDocumentNo().equalsIgnoreCase(docNo)) {
									docNo = iLine.getDocumentNo();
									error.append(docNo + ", ");
								}
							}
						}
						importLines.clear();
					}
				}
				
				importLines.add(imp);
					oldC_BPartner_ID = imp.getC_BPartner_ID();
					oldC_BPartner_Location_ID = imp.getC_BPartner_Location_ID();
					oldBillTo_ID = imp.getBillTo_ID();
					oldDocumentNo = imp.getDocumentNo();
					if (oldDocumentNo == null)
						oldDocumentNo = "";
			}
			rs.close();
			pstmt.close();
			
			if ( !importLines.isEmpty() )
			{
				int insertedLines = createOrder(Env.getCtx(), importLines, p_DocAction, trxName);
				if ( insertedLines > 0 )
				{
					noInsert++;
					noInsertLine += insertedLines;
				}
				importLines.clear();
			}
		}
		catch (Exception e)
		{
			s_log.log(Level.SEVERE, "Order - " + sql.toString(), e);

		}

		//	Set Error to indicator to not imported
		sql = new StringBuffer ("UPDATE I_Order "
			+ "SET I_IsImported='N', Updated=CURRENT_TIMESTAMP "
			+ "WHERE I_IsImported<>'Y'").append(clientCheck);
		no = DB.executeUpdate(sql.toString(), trxName);
		pi.addLog (0, null, new BigDecimal (no), "Import Order @Errors@");
					//
		pi.addLog (0, null, new BigDecimal (noInsert), "@C_Order_ID@: @Inserted@");
		pi.addLog (0, null, new BigDecimal (noInsertLine), "@C_OrderLine_ID@: @Inserted@");

		return error.toString();
	}
}	//	ImportOrder
