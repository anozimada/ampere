/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.logging.Level;

import org.compiere.model.MProduct;
import org.compiere.print.MPrintFormat;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Ini;

/**
  * Simple production plan for manufacturing light 
 */
public class ProductionPlan extends SvrProcess
{
	/** Print Format Parameter			*/
	private int					p_AD_PrintFormat_ID = 50089;
	/**	Product Parameter				*/
	private int					p_M_Product_ID = 0;
	private Timestamp			p_Date_From = null;
	private Timestamp			p_Date_To = null;
	private int					seqNo = 0;
	private MProduct			product = null;
	private BigDecimal 			unitsperlift = Env.ZERO;
	private String				insertString = null;
	private BigDecimal totalDemand = null;
	private BigDecimal openingTotal = null;
	private BigDecimal closingTotal = null;
	private BigDecimal totalSupply = null;
	
	/**
	 *  Prepare - e.g., get Parameters.
	 */
	
	protected void prepare()
	{
		//	Parameter
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Product_ID"))
				p_M_Product_ID = para[i].getParameterAsInt();
			else if (name.equals("AD_PrintFormat_ID"))
				p_AD_PrintFormat_ID = para[i].getParameterAsInt();
			else if (name.equals("Date"))
			{
				p_Date_From = (Timestamp)para[i].getParameter();
				p_Date_To = (Timestamp)para[i].getParameter_To();
			}
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		insertString = "INSERT INTO T_ReportData "
			+ "(AD_PInstance_ID, SeqNo, Text_0, "
			+ "Qty_0, Qty_1, Date_0, Text_1, LevelNo ) "
			+ "VALUES (" + getAD_PInstance_ID() + ", ?, ?, ?, ?, ?, ?, ?)";
		
	}	//	prepare

	
	/**************************************************************************
	 *  Perform process.
	 *  @return Message to be translated
	 */
	protected String doIt()
	{
		product = MProduct.get(getCtx(), p_M_Product_ID);
		unitsperlift = product.getUnitsPerPallet();
		if (unitsperlift.compareTo(Env.ZERO) == 0)
			unitsperlift = null;
		
		createOpenInventory();
		createDemand();
		createSupply();
		createSummary();

		if (Ini.isClient())
			getProcessInfo().setTransientObject (MPrintFormat.get (getCtx(), p_AD_PrintFormat_ID, false));
		else
			getProcessInfo().setSerializableObject(MPrintFormat.get (getCtx(), p_AD_PrintFormat_ID, false));

		return "";
	}	//	doIt

	
	private void createOpenInventory()
	{

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		
		// header line 1
		DB.executeUpdate(insertString, new Object[] {seqNo++, product.getValue() + " - "  + product.getName(),
				null, null, null, null, 0}, false, get_TrxName());
		// header line 2
		DB.executeUpdate(insertString, new Object[] {seqNo++, format.format(p_Date_From) + " - " + format.format(p_Date_To),
				null, null, null, null, 0}, false, get_TrxName());
		
		
		// blank line
		DB.executeUpdate(insertString, new Object[] {seqNo++, null, null, null, null, null,0}, false, get_TrxName());
		
		StringBuffer sb = new StringBuffer ("INSERT INTO T_ReportData "
			+ "(AD_PInstance_ID, SeqNo, Text_0, "
			+ "Qty_0, Qty_1, Date_0, LevelNo ) ");
		sb.append("SELECT ").append(getAD_PInstance_ID()).append("," + seqNo +", 'Opening Inventory', ")
			.append("SUM(s.QtyOnHand),CASE WHEN p.UnitsPerPallet<>0 THEN SUM(s.QtyOnHand)/p.UnitsPerPallet END, ?, 1 ")
			.append("FROM M_Storage s "
			+ "INNER JOIN M_Product p ON (p.M_Product_ID=s.M_Product_ID) "
			+ "WHERE p.M_Product_ID = " + p_M_Product_ID)
			.append(" GROUP BY p.UnitsPerPallet ");
			
		//
		DB.executeUpdate(sb.toString(), new Object[] {p_Date_From}, false, get_TrxName());
		log.finest(sb.toString());
		
		String sql = "SELECT SUM(Qty_0) FROM T_ReportData WHERE AD_PInstance_ID = ? AND SeqNo = ? ";
		openingTotal = DB.getSQLValueBD(get_TrxName(), sql, getAD_PInstance_ID(), seqNo++);
		if (openingTotal == null)
			openingTotal = Env.ZERO;

		
		// blank line
		DB.executeUpdate(insertString, new Object[] {seqNo++, null, null, null, null, null, 0}, false, get_TrxName());
	}	//	

	/**
	 */
	private void createDemand()
	{
		
		// section header
		DB.executeUpdate(insertString, new Object[] {seqNo++, "Demand", null, null, null, null, 0}, false, get_TrxName());
		// section header
		DB.executeUpdate(insertString, new Object[] {seqNo++, "Customer Orders", null, null, null, null, 1}, false, get_TrxName());
		

		// order details
		StringBuffer sb = new StringBuffer ("INSERT INTO T_ReportData "
				+ "(AD_PInstance_ID, SeqNo, Text_0, "
				+ "Qty_0, Qty_1, Date_0, Text_1, LevelNo ) ");
		sb.append("SELECT ").append(getAD_PInstance_ID()).append("," + seqNo +", bp.Name || ' - ' || o.DocumentNo," +
				" ol.QtyReserved, CASE WHEN p.UnitsPerPallet<>0 THEN ol.QtyReserved/p.UnitsPerPallet END, ol.DatePromised, w.Name, 2 ")
		.append("FROM C_Order o "
				+ "INNER JOIN C_OrderLine ol ON (o.C_Order_ID=ol.C_Order_ID) "
				+ "INNER JOIN C_BPartner bp ON (o.C_BPartner_ID=bp.C_BPartner_ID) "
				+ "INNER JOIN M_Product p ON (ol.M_Product_ID=p.M_Product_ID) "
				+ "INNER JOIN M_Warehouse w ON (o.M_Warehouse_ID=w.M_Warehouse_ID) "
				+ "WHERE p.M_Product_ID = " + p_M_Product_ID 
				+ " AND ol.DatePromised >= " + DB.TO_DATE(p_Date_From)
				+ " AND ol.DatePromised <= " + DB.TO_DATE(p_Date_To)
				+ " AND o.DocStatus IN ('CO', 'CL') AND o.IsSOTrx = 'Y' AND ol.QtyReserved <> 0 ");

		//
		DB.executeUpdate(sb.toString(), get_TrxName());
		log.finest(sb.toString());
		
		String sql = "SELECT SUM(Qty_0) FROM T_ReportData WHERE AD_PInstance_ID = ? AND SeqNo = ? ";
		BigDecimal orderedTotal = DB.getSQLValueBD(get_TrxName(), sql, getAD_PInstance_ID(), seqNo++);
		if (orderedTotal == null)
			orderedTotal = Env.ZERO;
		BigDecimal orderedTotalLifts = Env.ZERO;
		if ( unitsperlift != null )
			orderedTotalLifts = orderedTotal.divide(unitsperlift, RoundingMode.HALF_UP);
		
		// section header
		DB.executeUpdate(insertString, new Object[] {seqNo++, "Production Orders", null, null, null, null, 1}, false, get_TrxName());
		

		// order details
		sb = new StringBuffer ("INSERT INTO T_ReportData "
				+ "(AD_PInstance_ID, SeqNo, Text_0, "
				+ "Qty_0, Qty_1, Date_0, Text_1, LevelNo ) ");
		sb.append("SELECT ").append(getAD_PInstance_ID()).append("," + seqNo +", 'Production Order - ' || p.DocumentNo," +
				" pl.QtyUsed, CASE WHEN prod.UnitsPerPallet<>0 THEN pl.QtyUsed/prod.UnitsPerPallet END, p.MovementDate, w.Name, 2 ")
		.append("FROM M_Production p "
				+ "INNER JOIN M_ProductionLine pl ON (p.M_Production_ID=pl.M_Production_ID) "
				+ "INNER JOIN M_Locator l ON (l.M_Locator_ID=pl.M_Locator_ID) "
				+ "INNER JOIN M_Warehouse w ON (l.M_Warehouse_ID=w.M_Warehouse_ID) "
				+ "INNER JOIN M_Product prod ON (pl.M_Product_ID=prod.M_Product_ID) "
				+ "WHERE prod.M_Product_ID = " + p_M_Product_ID 
				+ " AND p.MovementDate >= " + DB.TO_DATE(p_Date_From)
				+ " AND p.MovementDate <= " + DB.TO_DATE(p_Date_To)
				+ " AND p.Processed = 'N' AND pl.IsEndProduct = 'N' ");

		//
		DB.executeUpdate(sb.toString(), get_TrxName());
		log.finest(sb.toString());
		
		sql = "SELECT SUM(Qty_0) FROM T_ReportData WHERE AD_PInstance_ID = ? AND SeqNo = ? ";
		BigDecimal reqTotal = DB.getSQLValueBD(get_TrxName(), sql, getAD_PInstance_ID(), seqNo++);
		if (reqTotal == null )
			reqTotal = Env.ZERO;
		BigDecimal reqTotalLifts = Env.ZERO;
		if ( unitsperlift != null )
			reqTotalLifts = reqTotal.divide(unitsperlift, RoundingMode.HALF_UP);


		totalDemand = orderedTotal.add(reqTotal);
		BigDecimal totalDemandLifts = Env.ZERO;
		if ( unitsperlift != null )
			totalDemandLifts = (orderedTotal.add(reqTotal)).divide(unitsperlift, RoundingMode.HALF_UP);
		
		// summary
		DB.executeUpdate(insertString, new Object[] {seqNo++, "*** Total Demand", totalDemand, totalDemandLifts, null, null, 1}, false, get_TrxName());

		// blank line
		DB.executeUpdate(insertString, new Object[] {seqNo++, null, null, null, null, null, 0}, false, get_TrxName());
		
	}	//	

	/**
	 */
	private void createSupply()
	{
		
		// section header
		DB.executeUpdate(insertString, new Object[] {seqNo++, "Supply", null, null, null, null, 0}, false, get_TrxName());
			
		// order details

		// section header
		DB.executeUpdate(insertString, new Object[] {seqNo++, "Purchase Orders", null, null, null, null, 1}, false, get_TrxName());
		
		StringBuffer sb = new StringBuffer ("INSERT INTO T_ReportData "
				+ "(AD_PInstance_ID, SeqNo, Text_0, "
				+ "Qty_0, Qty_1, Date_0, Text_1, LevelNo ) ");
		sb.append("SELECT ").append(getAD_PInstance_ID()).append("," + seqNo +", bp.Name || ' - ' || o.DocumentNo," +
				" ol.QtyReserved, CASE WHEN p.UnitsPerPallet<>0 THEN ol.QtyReserved/p.UnitsPerPallet END, ol.DatePromised, w.Name, 2 ")
		.append("FROM C_Order o "
				+ "INNER JOIN C_OrderLine ol ON (o.C_Order_ID=ol.C_Order_ID) "
				+ "INNER JOIN C_BPartner bp ON (o.C_BPartner_ID=bp.C_BPartner_ID) "
				+ "INNER JOIN M_Product p ON (ol.M_Product_ID=p.M_Product_ID) "
				+ "INNER JOIN M_Warehouse w ON (o.M_Warehouse_ID=w.M_Warehouse_ID) "
				+ "WHERE p.M_Product_ID = " + p_M_Product_ID 
				+ " AND ol.DatePromised >= " + DB.TO_DATE(p_Date_From)
				+ " AND ol.DatePromised <= " + DB.TO_DATE(p_Date_To)
				+ " AND o.DocStatus IN ('CO', 'CL') AND o.IsSOTrx = 'N' AND ol.QtyReserved <> 0 ");

		//
		DB.executeUpdate(sb.toString(), get_TrxName());
		log.finest(sb.toString());
		
		String sql = "SELECT SUM(Qty_0) FROM T_ReportData WHERE AD_PInstance_ID = ? AND SeqNo = ? ";
		BigDecimal purchaseTotal = DB.getSQLValueBD(get_TrxName(), sql, getAD_PInstance_ID(), seqNo++);
		if (purchaseTotal == null)
			purchaseTotal = Env.ZERO;
		BigDecimal purchaseTotalLifts = Env.ZERO;
		if ( unitsperlift != null )
			purchaseTotalLifts = purchaseTotal.divide(unitsperlift, RoundingMode.HALF_UP);
		
		// section header
		DB.executeUpdate(insertString, new Object[] {seqNo++, "Production Orders", null, null, null, null, 1}, false, get_TrxName());
		

		// order details
		sb = new StringBuffer ("INSERT INTO T_ReportData "
				+ "(AD_PInstance_ID, SeqNo, Text_0, "
				+ "Qty_0, Qty_1, Date_0, Text_1, LevelNo ) ");
		sb.append("SELECT ").append(getAD_PInstance_ID()).append("," + seqNo +", 'Production Order - ' || p.DocumentNo," +
				" pl.MovementQty, CASE WHEN prod.UnitsPerPallet<>0 THEN pl.MovementQty/prod.UnitsPerPallet END, p.MovementDate, w.Name, 2 ")
		.append("FROM M_Production p "
				+ "INNER JOIN M_ProductionLine pl ON (p.M_Production_ID=pl.M_Production_ID) "
				+ "INNER JOIN M_Locator l ON (l.M_Locator_ID=pl.M_Locator_ID) "
				+ "INNER JOIN M_Warehouse w ON (l.M_Warehouse_ID=w.M_Warehouse_ID) "
				+ "INNER JOIN M_Product prod ON (pl.M_Product_ID=prod.M_Product_ID) "
				+ "WHERE prod.M_Product_ID = " + p_M_Product_ID 
				+ " AND p.MovementDate >= " + DB.TO_DATE(p_Date_From)
				+ " AND p.MovementDate <= " + DB.TO_DATE(p_Date_To)
				+ " AND p.Processed = 'N' AND pl.IsEndProduct = 'Y' ");

		//
		DB.executeUpdate(sb.toString(), get_TrxName());
		log.finest(sb.toString());
		
		sql = "SELECT SUM(Qty_0) FROM T_ReportData WHERE AD_PInstance_ID = ? AND SeqNo = ? ";
		BigDecimal productionTotal = DB.getSQLValueBD(get_TrxName(), sql, getAD_PInstance_ID(), seqNo++);
		if (productionTotal == null )
			productionTotal = Env.ZERO;
		BigDecimal productionTotalLifts = Env.ZERO;
		if ( unitsperlift != null )
			productionTotalLifts = productionTotal.divide(unitsperlift, RoundingMode.HALF_UP);

		totalSupply = productionTotal.add(purchaseTotal);
		BigDecimal totalSupplyLifts = Env.ZERO;
		if ( unitsperlift != null )
			totalSupplyLifts = (productionTotal.add(purchaseTotal)).divide(unitsperlift, RoundingMode.HALF_UP);
		
		// summary
		DB.executeUpdate(insertString, new Object[] {seqNo++, "*** Total Supply", totalSupply, totalSupplyLifts, null, null, 1}, false, get_TrxName());


		// blank line
		DB.executeUpdate(insertString, new Object[] {seqNo++, null, null, null, null, null, 0}, false, get_TrxName());
		
	}	//	

	/**
	 */
	private void createSummary()
	{
		// ending balance
		closingTotal = openingTotal.subtract(totalDemand).add(totalSupply);
		BigDecimal closingTotalLifts = Env.ZERO;
		if ( unitsperlift != null )
			closingTotalLifts = closingTotal.divide(unitsperlift, RoundingMode.HALF_UP);
		
		DB.executeUpdate(insertString, new Object[] {seqNo++, "Closing Inventory", closingTotal, closingTotalLifts, p_Date_To, null, 1}, false, get_TrxName());
		
		// blank line
		DB.executeUpdate(insertString, new Object[] {seqNo++, null, null, null, null, null, 0}, false, get_TrxName());
		
		// replenishment
		DB.executeUpdate(insertString, new Object[] {seqNo++, "Reorder Parameters", null, null, null, null, 1}, false, get_TrxName());

		// replenishment details
		StringBuffer sb = new StringBuffer ("INSERT INTO T_ReportData "
				+ "(AD_PInstance_ID, SeqNo, Text_0, "
				+ "Qty_0, Qty_1, Date_0, Text_1, LevelNo ) ");
		sb.append("SELECT ").append(getAD_PInstance_ID()).append("," + seqNo +", w.Name," +
				" r.Level_Min, CASE WHEN prod.UnitsPerPallet<>0 THEN r.Level_Min/prod.UnitsPerPallet END, null, w.Name, 2 ")
		.append("FROM M_Replenish r "
				+ "INNER JOIN M_Warehouse w ON (r.M_Warehouse_ID=w.M_Warehouse_ID) "
				+ "INNER JOIN M_Product prod ON (prod.M_Product_ID=r.M_Product_ID) "
				+ "WHERE r.M_Product_ID = " + p_M_Product_ID 
				+ " AND r.IsActive = 'Y' ");

		//
		DB.executeUpdate(sb.toString(), get_TrxName());
		log.finest(sb.toString());
		
		String sql = "SELECT SUM(Qty_0) FROM T_ReportData WHERE AD_PInstance_ID = ? AND SeqNo = ? ";
		BigDecimal targetTotal = DB.getSQLValueBD(get_TrxName(), sql, getAD_PInstance_ID(), seqNo++);
		if (targetTotal == null)
			targetTotal = Env.ZERO;
		BigDecimal targetTotalLifts = null;
		if ( unitsperlift != null)
			targetTotalLifts = targetTotal.divide(unitsperlift, RoundingMode.HALF_UP);

		// summary
		DB.executeUpdate(insertString, new Object[] {seqNo++, "*** Total Target Inventory", targetTotal, targetTotalLifts, null, null, 1}, false, get_TrxName());


		// blank line
		DB.executeUpdate(insertString, new Object[] {seqNo++, null, null, null, null, null, 0}, false, get_TrxName());
		
		// suggested level

		BigDecimal suggestedTotal = targetTotal.subtract(closingTotal);
		if (suggestedTotal == null || suggestedTotal.compareTo(Env.ZERO) < 0)
			suggestedTotal = Env.ZERO;
		BigDecimal suggestedTotalLifts = Env.ZERO;
		if ( unitsperlift != null)
			suggestedTotalLifts = suggestedTotal.divide(unitsperlift, RoundingMode.HALF_UP);

		// summary
		DB.executeUpdate(insertString, new Object[] {seqNo++, "Suggested Production", suggestedTotal, suggestedTotalLifts, null, null, 0}, false, get_TrxName());
		
	}	//	
}	//	
