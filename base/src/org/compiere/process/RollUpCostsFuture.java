package org.compiere.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Level;

import org.compiere.model.MCostElement;
import org.compiere.util.DB;
import org.compiere.util.Env;

public class RollUpCostsFuture extends SvrProcess {


	int category = 0;
	int product_id = 0;
	int client_id = 0; 
	int org_id = 0; 
	int user_id = 0;
	int costelement_id = 0;
	private HashSet<Integer> processed;
	
	protected void prepare() 
	{
	
		int chosen_id = 0;
				
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
		//	log.fine("prepare - " + para[i]);
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Product_Category_ID"))
				category = para[i].getParameterAsInt();
			else if (name.equals("M_Product_ID"))
				chosen_id = para[i].getParameterAsInt();
			else if (name.equals("M_CostElement_ID"))
				costelement_id = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);		
		}
		
	
		product_id = getRecord_ID();
		if (product_id == 0)
		{
			product_id = chosen_id;
		}

		
	}
	
	protected String doIt() throws Exception
	{
		client_id = Env.getAD_Client_ID(getCtx());
		org_id = Env.getAD_Org_ID(getCtx());
		user_id = Env.getAD_User_ID(getCtx());
		
		//make sure futurecostprice is not empty or zero when current cost price has value
		String sql = "UPDATE m_cost \n" +
	             "SET futurecostprice = currentcostprice \n" +
	             "WHERE COALESCE(futurecostprice,0) = 0 \n" +
	             "AND   currentcostprice <> 0 \n" +
	             "AND   ad_client_id = ?";
		
		DB.executeUpdate(sql, client_id, null);
		
		
		Collection<MCostElement>  elements = new HashSet<MCostElement>();
		String result = "";
				
		if (costelement_id == 0)
			elements = MCostElement.getActiveElements(getCtx(), get_TrxName());
		else
		{
			elements.add(new MCostElement(getCtx(), costelement_id, get_TrxName()));
		}
			
		for(MCostElement element : elements)
		{
			costelement_id = element.get_ID();
			createView();
			result = rollUp();
			addLog(0, null, null, result + element.toString());
			deleteView();
		}
		return result;
	}
	


	protected String rollUp() throws Exception {
		
		
		if (product_id != 0) //only for the product
		{
			String sql = "SELECT M_PRODUCT_ID FROM M_PRODUCT WHERE M_PRODUCT_ID = " + 
				product_id + " AND AD_CLIENT_ID = " + Env.getAD_Client_ID(getCtx()) + 
		    	" AND IsManufactured = 'Y' " +
		    	" AND M_PRODUCT_ID IN (SELECT M_PRODUCT_ID FROM M_PRODUCT_BOM)";
			//System.err.println(sql);
			PreparedStatement pstmt = DB.prepareStatement (sql, get_TrxName());
		    ResultSet results = pstmt.getResultSet();
			while (results.next())
			{
				rollUpCosts(product_id);
			}
		}
		else if (category != 0) //roll up for all categories
		{
			String sql = "SELECT M_PRODUCT_ID FROM M_PRODUCT WHERE M_PRODUCT_CATEGORY_ID = " + 
			    category + " AND AD_CLIENT_ID = " + Env.getAD_Client_ID(getCtx()) + 
			    " AND IsManufactured = 'Y' " +
			    " AND M_PRODUCT_ID IN (SELECT M_PRODUCT_ID FROM M_PRODUCT_BOM)";
			//System.err.println(sql);
			PreparedStatement pstmt = DB.prepareStatement (sql, get_TrxName());
		    ResultSet results = pstmt.getResultSet();
			while (results.next())
			{
				rollUpCosts(results.getInt(1));
			}
		}
		else //do it for all products 
		{
			String sql = "SELECT M_PRODUCT_ID FROM M_PRODUCT WHERE AD_CLIENT_ID = " + Env.getAD_Client_ID(getCtx()) + 
			   " AND IsManufactured = 'Y' " +
			   " AND M_PRODUCT_ID IN (SELECT M_PRODUCT_ID FROM M_PRODUCT_BOM)";
		    //System.err.println(sql);
			PreparedStatement pstmt = DB.prepareStatement (sql, get_TrxName());
		    ResultSet results = pstmt.getResultSet();
		    while (results.next())
		    {
			    rollUpCosts(results.getInt(1));
		    }
	    }
		
		return "Roll Up Complete: ";
	}
    
	protected void createView() throws Exception
	{
		
		processed = new HashSet<Integer>();
		
	}
	
	protected void deleteView()
	{
		processed.clear();
	}
	
	protected void rollUpCosts(int p_id) throws Exception 
	{
		String sql = "SELECT pb.M_ProductBOM_ID FROM M_Product_BOM pb " +
				"JOIN M_Product p ON pb.M_Product_ID = p.M_Product_ID " +
				"WHERE p.IsManufactured = 'Y' AND pb.M_Product_ID = " + 
		    p_id + " AND pb.AD_Client_ID = " + Env.getAD_Client_ID(getCtx());
		//System.err.println(sql);
		PreparedStatement pstmt = DB.prepareStatement (sql, get_TrxName());
	    ResultSet results = pstmt.getResultSet();
		
		while (results.next())
		{
			if ( !processed.contains(p_id)) {
				rollUpCosts(results.getInt(1));
			}
        }
		results.close();
			
		//once the subproducts costs are accurate, calculate the costs for this product
		String update = "UPDATE M_Cost set FutureCostPrice = " + 
		   " COALESCE((select Sum(b.BOMQty * COALESCE(NULLIF(c.futurecostprice,0),c.currentcostprice)) " + 
		   " FROM M_Product_BOM b " + 
           " INNER JOIN M_Cost c ON (b.M_PRODUCTBOM_ID = c.M_Product_ID) " + 
           " WHERE b.M_Product_ID = " + p_id + " AND M_CostElement_ID = " + costelement_id + "),0)" +
           " WHERE M_Product_ID = " + p_id + " AND AD_Client_ID = " + Env.getAD_Client_ID(getCtx()) +
           " AND M_CostElement_ID = " + costelement_id +
           " AND M_PRODUCT_ID IN (SELECT M_PRODUCT_ID FROM M_PRODUCT_BOM)" + 
           " AND M_PRODUCT_ID IN (SELECT M_PRODUCT_ID FROM M_PRODUCT WHERE IsManufactured = 'Y')";;
        
		//System.err.println(sql);
		DB.executeUpdate(update, get_TrxName());

		processed.add(p_id);
		
	}
}
