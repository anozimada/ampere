package org.compiere.process;

import org.compiere.model.MBPartner;
import org.compiere.model.MOrder;


/**
 * Process to temporarily disable credit check
 * in order to proceed with the Order completion
 * @author jtrinidad
 *
 */
public class CompleteOrderOnHold extends SvrProcess {

	
	int p_C_Order_ID = 0;
	
	@Override
	protected void prepare() {
		p_C_Order_ID = getRecord_ID();
	}

	@Override
	protected String doIt() throws Exception {
		if (p_C_Order_ID == 0) {
			return "No selected order to complete.";
		}
		
		MOrder order = new MOrder(getCtx(), p_C_Order_ID, get_TrxName());
		
		if (!order.isSOTrx()) {
			return "Order is not a Sales Transaction.";
		}
		if (!order.isCreditHold()) {
			return "Order is On Hold.";
		}
		
		MBPartner bp = (MBPartner) order.getBill_BPartner();
		
		String lastCreditStatus = bp.getSOCreditStatus();
		//temporarily allow for no credit check
		bp.setSOCreditStatus(MBPartner.SOCREDITSTATUS_NoCreditCheckManual);
		bp.saveEx();
		
		boolean sucess = order.processIt(DocAction.ACTION_Complete);
		if (sucess) {
			addLog("Customer Order manually released from Credit Hold. DocumentNo=" + order.getDocumentNo() + ".");
			bp.setSOCreditStatus(lastCreditStatus);
			//bp.setTotalOpenBalance(); might not be necessary
			bp.save();
		}
		
		return null;
		
	}

}
