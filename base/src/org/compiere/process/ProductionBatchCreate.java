package org.compiere.process;

import java.math.BigDecimal;
import java.util.logging.Level;

import org.compiere.model.MProduction;
import org.compiere.model.MProductionBatch;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.Env;


/**
 * 
 * Process to create productions based on the batch
 * defined for a particular production header
 * @author JobrianT
 *
 */
public class ProductionBatchCreate extends SvrProcess {

	private int p_M_Production_Batch_ID=0;
	private MProductionBatch pBatch = null;
	
	private boolean m_CreateProductionLine = true;
	private BigDecimal productionQty = Env.ZERO;
	
	protected void prepare() {
		
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("ProductionQty"))
				productionQty = para[i].getParameterAsBigDecimal();
			log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		p_M_Production_Batch_ID = getRecord_ID();
		pBatch = new MProductionBatch(getCtx(), p_M_Production_Batch_ID, get_TrxName());

	}	//prepare

	@Override
	protected String doIt() throws Exception
	{
		if (pBatch.get_ID() == 0)
			throw new AdempiereUserError("Could not load production batch");

		if (pBatch.isProcessed())
		{
			if (pBatch.getDocStatus().equals(MProductionBatch.DOCACTION_Close))
				return "Can not process, Production Batch is closed";
			else if (pBatch.getDocStatus().equals(MProductionBatch.DOCACTION_Void))
				return "Can not process, Production Batch is voided";
			else if (pBatch.getDocStatus().equals(MProductionBatch.DOCACTION_Complete))
			{
				if (productionQty.signum() == 0)
					return "Invalid Production Qty.";
				return createProduction();
			}
		}
		else
		{
			return "Please first confirm/complete the batch";
		}

		return null;
	}

	protected String createProduction() throws Exception {
		
		if (pBatch.createProductionHeader(productionQty) == null) {
			throw new AdempiereUserError("There is an Open Production Order");
		}
		//create Production lines
		MProduction current = null;
		if (m_CreateProductionLine) {
			current = getCurrent(true);
			if (current == null) {
				throw new AdempiereUserError("Unexpected error!  There must be an open Production order.");
			}
			if (!current.costsOK())
				throw new AdempiereUserError("Excessive difference in standard costs");
			
			if (current.createLines(false) > 0) {
				current.setIsCreated(true);
				current.saveEx();
			}
				
		}
		//create movement
		if (pBatch.isCreateMove()) {
			if (current == null) {
				current = getCurrent(true);
			}
			current.createMovement();
		}
		
		return "Production Order created.";
	}
	
	private MProduction getCurrent(boolean requery) {
		MProduction[] productions = pBatch.getHeaders(requery);
		for (MProduction p : productions) {
			if (!p.isProcessed()) {
				return p;
			}
		}
		
		return null;
	}

}
