package org.compiere.process;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.adempiere.exceptions.AdempiereException;

/**
 * @author Nikunj Panelia
 */
public class DistributeCost extends SvrProcess
{

	@Override
	protected String doIt() throws Exception
	{
		try
		{
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			if (loader == null)
				loader = DistributeCost.class.getClassLoader();

			Class<?> aClass = loader.loadClass("org.adempiere.webui.process.DistributeCost");
			Constructor<?> constructor = aClass.getConstructor(Integer.TYPE);
			constructor.newInstance(getRecord_ID());
		}
		catch (InvocationTargetException e)
		{
			throw new AdempiereException(e);
		}

		return "";
	}

	@Override
	protected void prepare()
	{

	}

}
