/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                        *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAttachment;
import org.compiere.model.MBPBankAccount;
import org.compiere.model.MBank;
import org.compiere.model.MBankAccount;
import org.compiere.model.MPaySelection;
import org.compiere.model.MPaySelectionCheck;
import org.compiere.model.MPaymentBatch;
import org.compiere.util.DB;
import org.compiere.util.Util;

public class ExportABA extends SvrProcess {


	private String p_userName;
	private String p_userNumber;
	private int count = 0;
	private int p_C_PaySelection_ID = 0;
	private MPaySelection paySelection;
	private MBankAccount ownBankAcct;
	private int nextDocNo;
	private MBank ownBank;
	private String p_directoryName;
	private String p_remitterName;
	private String trace;
	private MPaySelectionCheck[] payments;
	private boolean p_createPayments = false;
	private boolean p_includeBalancing = true;
	private String p_description = null;
	
	@Override
	protected void prepare() {

		for ( ProcessInfoParameter para : getParameter())
		{
			if ( para.getParameterName().equals("UserName") )
				p_userName = (String) para.getParameter();
			else if ( para.getParameterName().equals("UserNumber"))
				p_userNumber = para.getParameterAsString();
			else if ( para.getParameterName().equals("C_PaySelection_ID") )
				p_C_PaySelection_ID = para.getParameterAsInt();
			else if ( para.getParameterName().equals("Directory"))
				p_directoryName = (String) para.getParameter();
			else if ( para.getParameterName().equals("CreatePayments"))
				p_createPayments  = "Y".equals(para.getParameter());
			else if ( para.getParameterName().equals("IncludeBalancing"))
				p_includeBalancing = para.getParameterAsBoolean();
			else if ( para.getParameterName().equals("Description"))
				p_description = para.getParameterAsString();
			else 
				log.info("Parameter not found " + para.getParameterName());
		}
		
		if ( p_C_PaySelection_ID == 0 && getRecord_ID() > 0 )
			p_C_PaySelection_ID = getRecord_ID();

		
		paySelection = new MPaySelection(getCtx(), p_C_PaySelection_ID, get_TrxName());
	}
	
	/**
	 * Creates a basic ABA compliant file for vendor payment
	 */
	@Override
	protected String doIt() throws Exception {
		ownBankAcct = (MBankAccount) paySelection.getC_BankAccount();
		if (Util.isEmpty(p_userName, true) )
			p_userName = ownBankAcct.get_ValueAsString("UserNameABA");
		
		if ( Util.isEmpty(p_userNumber, true) )
			p_userNumber = ownBankAcct.get_ValueAsString("UserNumberABA");
		
		if ( Util.isEmpty(p_userName, true) || Util.isEmpty(p_userNumber, true))
			return "ABA user name and number required on bank account " + ownBankAcct.getName();

		p_remitterName = ownBankAcct.get_ValueAsString("RemitterName");
		if ( Util.isEmpty(p_remitterName, true))
			return "No remitter name on bank account " + ownBankAcct.getName();
		
		if ( Util.isEmpty(ownBankAcct.getAccountNo() ))
			return "No account number on bank account " + ownBankAcct.getName();

		if ( Util.isEmpty(p_userName, true) || Util.isEmpty(p_userNumber, true) )
			return "User name and number required on bank account " + ownBankAcct.getName();
		
		if ( paySelection == null || paySelection.is_new() )
			return "Payment Selection required.";
		
		if ( Util.isEmpty(p_description))
			p_description = paySelection.getDescription();
		if (Util.isEmpty(p_description))
			p_description = paySelection.getName();
		
		ownBank = (MBank) ownBankAcct.getC_Bank();
		String ownbsb = ownBank.getRoutingNo();
		
		if ( Util.isEmpty(ownbsb, true) || ownbsb.length() < 6 || ownbsb.length() > 7 )
			return "Invalid routing (BSB) number on bank " + ownBank;
		
		ownbsb = ownbsb.replace(" ", "");
		if ( ownbsb.length() == 6 )
			ownbsb = ownbsb.substring(0,3) + "-" + ownbsb.substring(3);
		trace = ownbsb + leftPad(ownBankAcct.getAccountNo(),9);
		
		//  DocumentNo
		String sql = "SELECT CurrentNext "
			+ "FROM C_BankAccountDoc "
			+ "WHERE C_BankAccount_ID=? AND PaymentRule IN (?,?) AND IsActive='Y'";
		nextDocNo = DB.getSQLValue(get_TrxName(), sql, ownBankAcct.getC_BankAccount_ID(),
				MPaySelectionCheck.PAYMENTRULE_EFTAR, MPaySelectionCheck.PAYMENTRULE_EFTAP);
		
		ArrayList<MPaySelectionCheck> checks = new ArrayList<MPaySelectionCheck>();
		checks.addAll(Arrays.asList(MPaySelectionCheck.get(paySelection.getC_PaySelection_ID(), MPaySelectionCheck.PAYMENTRULE_EFTAP,
				nextDocNo, get_TrxName())));
		checks.addAll(Arrays.asList(MPaySelectionCheck.get(paySelection.getC_PaySelection_ID(), MPaySelectionCheck.PAYMENTRULE_EFTAR,
				nextDocNo, get_TrxName())));
		payments = checks.toArray(new MPaySelectionCheck[] {});
		
		File file = new File(p_directoryName, "ABA_" + paySelection.getC_PaySelection_ID() + new SimpleDateFormat("yyyyMMdd").format(new Date()) + ".txt");
		
		StringBuilder result = new StringBuilder();
		
		getType0(result);
		getVendorType1(result);
		if (p_includeBalancing)
			getOwnType1(result);
		getType7(result);

		PrintWriter writer = null;
		try
		{
			writer = new PrintWriter(new BufferedWriter(new FileWriter(file)), true);
			writer.write(result.toString());
			writer.flush();
			writer.close();
		}
		catch (IOException ioe)
		{
			ioe.printStackTrace();
			return "Failure writing file.";
		}			

		MAttachment attach = paySelection.createAttachment();
		attach.addEntry(file);
		attach.saveEx();

		if ( p_createPayments )
			MPaySelectionCheck.confirmPrint (payments, MPaymentBatch.getForPaySelection(getCtx(), paySelection.getC_PaySelection_ID(), get_TrxName()));
		

		return "Exported " + (count-1) + " payments to file: " + file.getName();
	}
	
	/**
	 * Create the type 0 record which contains information about the organisation
	 * submitting the file to their bank.
	 * 
	 * @param result
	 * @return
	 */
	private boolean getType0(StringBuilder result) {
		
		String date = new SimpleDateFormat("ddMMyy").format(paySelection.getPayDate());
		String char1 = "0";
		String char2_18 = leftPad("",17);
		String char19_20 = "01";
		String char21_23 = ownBank.getName().substring(0,3);
		String char24_30 = leftPad("",7);
		String char31_56 = rightPad(p_userName, 26);
		String char57_62 = leftPad(p_userNumber, 6); 
		String char63_74 = rightPad(p_description, 12);
		String char75_80 = date;
		String char81_120 = leftPad("",40);
		result.append(char1 + char2_18 + char19_20 + char21_23 + char24_30 + char31_56 + char57_62 + char63_74 + char75_80 + char81_120 + "\r\n");
		
		return true;
		
		
	}

	/**
	 * Create a type 7 record with summary information to validate
	 * @param result
	 * @return
	 */
	private void getType7(StringBuilder result) {
		BigDecimal total = paySelection.getTotalAmt().multiply(new BigDecimal("100").setScale(0));
				
		String char1 = "7";
		String char2_8 = "999-999"; 
		String char9_20 = leftPad("",12);
		String char21_30 = leftPad(p_includeBalancing ? 0 : total, 10);
		String char31_40 = leftPad(total, 10);
		String char41_50 = leftPad(p_includeBalancing ? total : 0, 10);
		String char51_74 = leftPad("",24); 
		String char75_80 = leftPad(count, 6);
		String char81_120 = leftPad("",40);
		
		result.append(char1 + char2_8 + char9_20 + char21_30 + char31_40 + char41_50 + char51_74 + char75_80 + char81_120 + "\r\n");
	}
    
	/**
	 * Create the type 1 lines for each payment selection check to be issued
	 * @param result
	 * @return
	 * @throws Exception 
	 */
    private boolean getVendorType1(StringBuilder result) throws Exception
    {
	    	for (MPaySelectionCheck payment : payments )
	    	{
	    		MBPBankAccount vendorBA = (MBPBankAccount) payment.getC_BP_BankAccount();
	    		log.log(Level.FINE, vendorBA.getA_Name() + vendorBA.getRoutingNo() + vendorBA.getAccountNo()) ;
	    		if ( vendorBA == null || vendorBA.is_new() 
	    				|| Util.isEmpty(vendorBA.getA_Name(),true) 
	    				|| Util.isEmpty(vendorBA.getRoutingNo(), true)
	    				|| Util.isEmpty(vendorBA.getAccountNo(), true) )
	    			throw new AdempiereException("Invalid bank account info for vendor " + payment.getC_BPartner().getName() );
	    		
	    		String remitterName = Util.isEmpty(vendorBA.get_ValueAsString("RemitterName"), true) ? p_remitterName : vendorBA.get_ValueAsString("RemitterName");
	    		
	    		String reference = vendorBA.get_ValueAsString("LodgementReference");
	    		if ( Util.isEmpty(reference, true) )
	    			reference = p_description;
	    		
	    		String bsb = vendorBA.getRoutingNo();
	    		bsb = bsb.replace(" ", "");
	    		if ( bsb.length() == 6 )
	    			bsb = bsb.substring(0,3) + "-" + bsb.substring(3);
	    		
	    		String char1 = "1";
				String char2_8 = leftPad(bsb,7); 
				String char9_17 = leftPad(vendorBA.getAccountNo(), 9);
				String char18 = " ";
				String char19_20 = "50";
				String char21_30 = leftPad(payment.getPayAmt().multiply(new BigDecimal("100")).setScale(0), 10);
				String char31_62 = rightPad(vendorBA.getA_Name(),32); 
				String char63_80 = rightPad(reference,18);
				String char81_96 = leftPad(trace, 16);
				String char97_112 = rightPad(remitterName,16);
				String char113_120 = leftPad(0, 8);
			
				result.append(char1 + char2_8 + char9_17 + char18 + char19_20 + char21_30 + char31_62 + char63_80 + char81_96 + char97_112 + char113_120 + "\r\n");
				
				count++;
				
	    	}	
		return true;
	
    }
    
    private boolean getOwnType1(StringBuilder result)
    {	
		String char1 = "1";
		String char2_17 = leftPad(trace, 16);
		String char18 = " ";
		String char19_20 = "13";
		String char21_30 = leftPad(paySelection.getTotalAmt().multiply(new BigDecimal("100")), 10);
		String char31_62 = rightPad(ownBankAcct.getName(),32); 
		String char63_80 = rightPad(p_description,18);
		String char81_96 = leftPad(trace, 16);
		String char97_112 = rightPad(p_remitterName,16);
		String char113_120 = leftPad(0, 8);
	
		result.append(char1 + char2_17 + char18 + char19_20 + char21_30 + char31_62 + char63_80 + char81_96 + char97_112 + char113_120 + "\r\n");
		
	    
		return true;
    }

    private String leftPad(String string, int length){
    	if ( string.length() > length )
    		string = string.substring(0, length);
    	return String.format("%1$" + length + "s", string);  
    }

    private String rightPad(String string, int length) {
    	if ( string.length() > length )
    		string = string.substring(0, length);
    	return String.format("%1$-" + length + "s", string);  
    }

    private String leftPad(Number value, int length) {
    	return String.format("%1$0" + length + "d", value.intValue());
    }
}
