package org.compiere.process;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.compiere.model.CallOutImportInvoice;
import org.compiere.model.MCurrency;
import org.compiere.model.MTax;
import org.compiere.model.Tax;
import org.compiere.model.X_I_Invoice;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Nikunj Panelia
 *
 */
public class SimulateImport extends SvrProcess 
{

	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare(){
	
	}

	/**
	 *  Perform process.
	 *  @return clear Message
	 *  @throws Exception
	 */
	protected String doIt() throws Exception
	{
		int no=0;
		String unImportCheck = " C_Payment_ID IS null AND (BankAccountNo IS NOT null OR C_BankAccount_ID IS NOT null) AND AD_Client_ID=" + getAD_Client_ID() ;
		StringBuffer sql=new StringBuffer();
		
		//Init
		sql.append("UPDATE I_Invoice i SET I_ErrorMsg = '',I_IsImported = 'N',DateInvoiced=i.DateAcct WHERE (I_IsImported<>'Y' OR I_IsImported IS NULL) AND ").append(unImportCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("Set Dates =" + no);
	
		//Error Date Invoiced
		sql = new StringBuffer ("UPDATE I_Invoice "
				+ " SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No Date Invoiced/Date Acct, ' "
				+ " WHERE (DateInvoiced IS NULL OR DateAcct Is Null) AND").append(unImportCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("No Date Invoiced/Date Acct =" + no);
				
		//Map Business Partner
		sql = new StringBuffer ("UPDATE I_Invoice i SET C_BPartner_ID=(SELECT MIN(C_BPartner_ID) "
				+ " FROM C_BPartner WHERE i.Description LIKE TRIM(Name2)) WHERE C_BPartner_ID is null AND ").append(unImportCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("Set Business Partner =" + no);
		
		//Error for Business partner
		sql = new StringBuffer ("UPDATE I_Invoice "
				  + " SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No Business Partner, ' "
				  + " WHERE C_BPartner_ID IS NULL AND").append(unImportCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("No Business Partner =" + no);
			
		//Payment Document Type
		sql = new StringBuffer ("UPDATE I_Invoice i SET  C_DocType_Payment_ID= (CASE WHEN AmtAcctDr > 0 then (SELECT C_DocType_ID from C_DocType WHERE Name='AP Payment' AND AD_Client_ID=?) "
				+ "  WHEN  AmtAcctCr > 0 then (SELECT C_DocType_ID from C_DocType WHERE Name='AR Receipt' AND AD_Client_ID=?) END ),"
				+ " IsSOTrx=(CASE WHEN AmtAcctDr > 0 then 'N'  WHEN   AmtAcctCr > 0 then  'Y' END )"
				+ " Where C_DocType_Payment_ID is null AND ").append(unImportCheck);
		no = DB.executeUpdateEx(sql.toString(),new Object[]{getAD_Client_ID(),getAD_Client_ID()}, get_TrxName());
		if (no != 0)
			log.warning ("Set Payment Document Type =" + no);
			
		//Error document type
		sql = new StringBuffer ("UPDATE I_Invoice SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No Payment DocType, ' "
				  + "WHERE C_DocType_Payment_ID IS NULL AND ").append (unImportCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("No Payment DocType=" + no);
		
		//Bank account
		sql = new StringBuffer("UPDATE I_Invoice i SET C_BankAccount_ID=(SELECT C_BankAccount_ID from C_BankAccount where AccountNo=i.BankAccountNo)"
				+ " WHERE C_BankAccount_ID is null and ").append(unImportCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());	
		if (no != 0)
			log.warning ("Set Bank Acct=" + no);
		
		//Error Bank Account
		sql = new StringBuffer ("UPDATE I_Invoice SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR= No Bank Account, ' WHERE C_BankAccount_ID IS null AND ").append(unImportCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());	
		if (no != 0)
			log.warning ("No Bank Acct=" + no);		
		
		//Currency
		sql= new StringBuffer("UPDATE I_Invoice i SET C_Currency_ID=(SELECT C_Currency_ID FROM  C_BankAccount WHERE C_BankAccount_ID=i.C_BankAccount_ID) WHERE").append(unImportCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("Set Currency=" + no);
		
		//Error Currency
		sql = new StringBuffer ("UPDATE I_Invoice SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR= No Currency, ' WHERE C_Currency_ID IS null AND ").append(unImportCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());	
		if (no != 0)
			log.warning ("No Currency=" + no);
		
		//check for cr amt/dr amt
		sql = new StringBuffer ("UPDATE I_Invoice SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR= No amount, ' WHERE AmtAcctCr IS null AND  AmtAcctDr IS null AND ").append(unImportCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());	
		if (no != 0)
			log.warning ("No Amount=" + no);		
		
		//Product/Charge
		String productCheck=" AND M_Product_ID is null AND  C_Charge_ID is null";
		
		sql = new StringBuffer ("UPDATE I_Invoice i SET "
				+ " M_Product_ID=(SELECT M_Product_ID FROM C_BPartner where C_BPartner_ID=i.C_BPartner_ID  AND M_Product_ID IS NOT null),"
				+ " C_Charge_ID =(SELECT InvoiceCharge_ID FROM C_BPartner where C_BPartner_ID=i.C_BPartner_ID AND InvoiceCharge_ID IS NOT null ) WHERE")
				.append(unImportCheck).append(productCheck); 
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("Set Product/Charge=" + no);		

		sql = new StringBuffer ("UPDATE I_Invoice i SET "
				+ " C_Charge_ID =(SELECT PaymentCharge_ID FROM C_BPartner where C_BPartner_ID=i.C_BPartner_ID AND PaymentCharge_ID IS NOT null) ,"
				+ " IsChargeOnPayment=(Case when (SELECT PaymentCharge_ID FROM C_BPartner where C_BPartner_ID=i.C_BPartner_ID AND PaymentCharge_ID IS NOT null) >0 then 'Y' end) WHERE")
		.append(unImportCheck).append(productCheck); 
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("Set Product/Charge=" + no);	
		
		//Error Charge/Product
		sql = new StringBuffer ("UPDATE I_Invoice SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No Product/Charge, '  WHERE M_Product_ID IS null  "
				+ " AND C_Charge_ID IS null AND ").append(unImportCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("No Product/Charge=" + no);	
		
		//BPartner Location
		sql = new StringBuffer ("UPDATE I_Invoice i SET "
		+" C_BPartner_Location_ID=(SELECT MIN(C_BPartner_Location_ID) FROM C_BPartner_Location  WHERE C_BPartner_ID=i.C_BPartner_ID AND "
		+ " IsBillTo='Y' AND C_BPartner_Location.IsActive='Y' ) WHERE C_BPartner_Location_ID IS null AND C_BPartner_ID IS NOT null AND" ).append(unImportCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("Set BPartner Location=" + no);
		
		//Error Charge/Product
		sql = new StringBuffer ("UPDATE I_Invoice SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No BPartner Location, '  WHERE C_BPartner_Location_ID IS null AND C_BPartner_ID IS NOT null "
				+ " AND ").append(unImportCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("No BPartner Location=" + no);	

		//Invoice Document Type
		sql = new StringBuffer ("UPDATE I_Invoice i SET C_DocType_ID= (CASE WHEN AmtAcctDr > 0 then "
				+ " (SELECT C_DocType_ID from C_DocType WHERE Name='AP Invoice' AND AD_Client_ID=?) "
				+ "  WHEN AmtAcctCr > 0   THEN (SELECT C_DocType_ID from C_DocType WHERE Name='AR Invoice' AND AD_Client_ID=?) END ) Where ").append(unImportCheck);
		no=DB.executeUpdateEx(sql.toString(),new Object[]{getAD_Client_ID(),getAD_Client_ID()}, get_TrxName());
		if (no != 0)
			log.fine("Set DocType=" + no);
		
		//Error Invoice document type
		sql = new StringBuffer ("UPDATE I_Invoice SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No Invoice DocType, ' "
				  + "WHERE C_DocType_ID IS NULL AND ").append (unImportCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("No Invoice DocType=" + no);
		
		//Error organization
		sql = new StringBuffer ("UPDATE I_Invoice SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No organization, ' "
				  + "WHERE AD_Org_ID IS NULL AND ").append (unImportCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("No Organization=" + no);
		
		commitEx();
		
		sql = new StringBuffer ("SELECT * FROM I_Invoice WHERE I_IsImported<>'E' AND ").append(unImportCheck);
		try
		{
			PreparedStatement pstmt = DB.prepareStatement (sql.toString(), get_TrxName());
			ResultSet rs = pstmt.executeQuery ();
			while (rs.next ())
			{
				X_I_Invoice imp = new X_I_Invoice (getCtx (), rs,  get_TrxName());			
				boolean isError=false;
				if(imp.get_ValueAsBoolean("IsChargeOnPayment") && imp.getC_Charge_ID() == 0)
				{
					imp.setI_ErrorMsg(imp.getI_ErrorMsg()+"Charge is mandatory when 'Charge On Payment' is true,");					
					isError=true;
				}
				if (checkForDuplicatePayment(imp.getC_BPartner_ID(), imp.get_Value("AmtAcctCr"), imp.get_Value("AmtAcctDr"),imp.getDateInvoiced()))
				{
					imp.setI_ErrorMsg(imp.getI_ErrorMsg()+"Payment is present with same data,");
					isError=true;
				}
				if (checkForDuplicateInvoice(imp.getC_BPartner_ID(), imp.get_Value("AmtAcctCr"), imp.get_Value("AmtAcctDr"),imp.getDateInvoiced()))
				{
					imp.setI_ErrorMsg(imp.getI_ErrorMsg()+"Invoice is present with same data,");					
					isError=true;
				}
				if(isError)
				{
					sql=new StringBuffer("UPDATE I_Invoice SET I_IsImported='E' WHERE I_Invoice_ID=?");
					DB.executeUpdate(sql.toString(), imp.get_ID() ,get_TrxName());
					imp.saveEx();
					continue;
				}
					
				setAmt(imp);
			}
		}
		catch (SQLException e)
		{
					log.log(Level.SEVERE, "Problem when simulating import", e);
		}

		return "";
	}

	/**
	 * Set amounts on Import Record 
	 * 
	 * @param imp
	 */
	private void setAmt(X_I_Invoice imp)
	{
		int c_Tax_ID=0;
		int stdPrecision = MCurrency.getStdPrecision(getCtx(), imp.getC_Currency_ID());
		BigDecimal TaxAmt=BigDecimal.ZERO;
		c_Tax_ID = Tax.get(getCtx(), imp.getM_Product_ID(),imp.getC_Charge_ID(), imp.getDateAcct(), imp.getDateAcct(),
				Env.getAD_Org_ID(getCtx()), Env.getContextAsInt(getCtx(), "#M_Warehouse_ID"), imp.getC_BPartner_Location_ID(), imp.getC_BPartner_Location_ID(),
				imp.isSOTrx());
		if(c_Tax_ID==0 && imp.getC_Tax_ID() > 0)
		{
			c_Tax_ID=imp.getC_Tax_ID();			
		}
		if(c_Tax_ID==0)
		{
			imp.setI_ErrorMsg(imp.getI_ErrorMsg()+"No Tax,");
			imp.saveEx();
			return;
		}
		imp.setC_Tax_ID(c_Tax_ID);		
		BigDecimal rate=BigDecimal.ZERO;
		BigDecimal amt=BigDecimal.ZERO;
			
		MTax tax=new MTax(Env.getCtx(),c_Tax_ID, get_TrxName());
		rate=tax.getRate();
		if(imp.get_Value("AmtAcctCr") !=null && ((BigDecimal)imp.get_Value("AmtAcctCr")).compareTo(BigDecimal.ZERO)>0)
			amt=(BigDecimal)imp.get_Value("AmtAcctCr");
		else if(imp.get_Value("AmtAcctDr") !=null && ((BigDecimal)imp.get_Value("AmtAcctDr")).compareTo(BigDecimal.ZERO)>0)
			amt=(BigDecimal)imp.get_Value("AmtAcctDr");
			
		TaxAmt=CallOutImportInvoice.calculateTaxAmt(amt,rate);
		TaxAmt = TaxAmt.setScale(stdPrecision, RoundingMode.HALF_UP);
		imp.setTaxAmt(TaxAmt);
		BigDecimal unitAmt=amt.subtract(TaxAmt);
		imp.setPriceActual(unitAmt);
		imp.saveEx();
				
	}
	
	
	/**
	 * @param c_BPartner_ID Business Partner
	 * @param crAmt Credit Amount
	 * @param drAmt debit Amount
	 * @param dateInvoiced  Date Invoice
	 * @return true is Duplicate payment
	 */
	private boolean checkForDuplicatePayment(int c_BPartner_ID, Object crAmt,Object drAmt, Timestamp dateInvoiced) 
	{
		boolean isDuplicate=false;
		
		StringBuffer sql=new StringBuffer("SELECT * From C_Payment where C_BPartner_ID=? AND PayAmt=? AND DateAcct=? AND AD_Client_ID=?");
		PreparedStatement pstm=null;
		ResultSet rs=null;
		try
		{
			pstm=DB.prepareStatement(sql.toString(), get_TrxName());
			pstm.setInt(1, c_BPartner_ID);
			if(crAmt!=null && ((BigDecimal)crAmt).compareTo(BigDecimal.ZERO) > 0)
				pstm.setBigDecimal(2, (BigDecimal)crAmt);
			if(drAmt!=null && ((BigDecimal)drAmt).compareTo(BigDecimal.ZERO) > 0)
				pstm.setBigDecimal(2, (BigDecimal)drAmt);
			pstm.setTimestamp(3, dateInvoiced);
			pstm.setInt(4, getAD_Client_ID());
			rs=pstm.executeQuery();
			if(rs.next())
			{
				isDuplicate=true;
			}
		}
		catch(SQLException e)
		{
			log.log(Level.SEVERE,e.getLocalizedMessage());
			
		}
		return isDuplicate;
	}
	
	/**
	 * @param c_BPartner_ID Business Partner
	 * @param crAmt Credit Amount
	 * @param drAmt debit Amount
	 * @param dateInvoiced  Date Invoice
	 * @return true is Duplicate Invoice
	 */
	private boolean checkForDuplicateInvoice(int c_BPartner_ID, Object crAmt,Object drAmt, Timestamp dateInvoiced) 
	{
		boolean isDuplicate=false;
		
		StringBuffer sql=new StringBuffer("SELECT * From C_Invoice where C_BPartner_ID=? AND GrandTotal=? AND DateInvoiced=? AND AD_Client_ID=?");
		PreparedStatement pstm=null;
		ResultSet rs=null;
		try
		{
			pstm=DB.prepareStatement(sql.toString(), get_TrxName());
			pstm.setInt(1, c_BPartner_ID);
			if(crAmt!=null && ((BigDecimal)crAmt).compareTo(BigDecimal.ZERO) > 0)
				pstm.setBigDecimal(2, (BigDecimal)crAmt);
			if(drAmt!=null && ((BigDecimal)drAmt).compareTo(BigDecimal.ZERO) > 0)
				pstm.setBigDecimal(2, (BigDecimal)drAmt);
			pstm.setTimestamp(3, dateInvoiced);
			pstm.setInt(4, getAD_Client_ID());
			rs=pstm.executeQuery();
			if(rs.next())
			{
				isDuplicate=true;
			}
		}
		catch(SQLException e)
		{
			log.log(Level.SEVERE,e.getLocalizedMessage());
			
		}
		return isDuplicate;
	}
}