/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.compiere.model.MColumn;
import org.compiere.model.MField;
import org.compiere.model.MTab;
import org.compiere.model.MTable;
import org.compiere.model.M_Element;
import org.compiere.model.PO;
import org.compiere.model.X_AD_FieldGroup;
import org.compiere.model.X_I_Column;
import org.compiere.util.DB;
import org.compiere.util.Util;

/**
 *	Import Columns from I_Column
 *
 */
public class ImportColumn extends SvrProcess
{
	/**	Client to be imported to		*/
	private int				m_AD_Client_ID = 0;
	/**	Delete old Imported				*/
	private boolean			m_deleteOldImported = false;

	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("IsDeleteImported"))
				m_deleteOldImported = "Y".equals(para[i].getParameter());
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}	//	prepare


	/**
	 *  Perform process.
	 *  @return Message
	 *  @throws Exception
	 */
	protected String doIt() throws java.lang.Exception
	{
		StringBuffer sql = null;
		int no = 0;
		String clientCheck = " AND AD_Client_ID=" + m_AD_Client_ID;

		//	****	Prepare	****

		//	Delete Old Imported
		if (m_deleteOldImported)
		{
			sql = new StringBuffer ("DELETE FROM I_Column "
				+ "WHERE I_IsImported='Y'").append(clientCheck);
			no = DB.executeUpdateEx(sql.toString(), get_TrxName());
			log.fine("Delete Old Impored =" + no);
		}

		//	Set Client, Org, IsActive, Created/Updated
		sql = new StringBuffer ("UPDATE I_Column "
			+ "SET AD_Client_ID = COALESCE (AD_Client_ID, ").append(m_AD_Client_ID).append("),"
			+ " AD_Org_ID = COALESCE (AD_Org_ID, 0),"
			+ " IsActive = COALESCE (IsActive, 'Y'),"
			+ " Created = COALESCE (Created, CURRENT_TIMESTAMP),"
			+ " CreatedBy = COALESCE (CreatedBy, 0),"
			+ " Updated = COALESCE (Updated, CURRENT_TIMESTAMP),"
			+ " UpdatedBy = COALESCE (UpdatedBy, 0),"
			+ " I_ErrorMsg = ' ',"
			+ " I_IsImported = 'N' "
			+ "WHERE I_IsImported<>'Y' OR I_IsImported IS NULL");
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Reset=" + no);

		// set AD_Table_ID
		sql = new StringBuffer ("UPDATE I_Column i "
				+ "SET AD_Table_ID=(SELECT AD_Table_ID FROM AD_Table WHERE AD_Table.TableName=i.TableName) "
						+ "WHERE (i.AD_Table_ID IS NULL OR i.AD_Table_ID <= 0) "
						+ " AND I_IsImported<>'Y' AND i.TableName IS NOT NULL "
						+ " AND EXISTS (SELECT AD_Table_ID FROM AD_Table WHERE AD_Table.TableName=i.TableName) ").append(clientCheck);
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Set Table ID=" + no);

		// set AD_Element_ID
		sql = new StringBuffer ("UPDATE I_Column i "
				+ "SET AD_Element_ID=(SELECT AD_Element_ID FROM AD_Element WHERE AD_Element.ColumnName=i.ColumnName) "
				+ "WHERE (i.AD_Element_ID IS NULL OR i.AD_Element_ID <= 0) "
				+ " AND I_IsImported<>'Y' AND i.ColumnName IS NOT NULL "
				+ " AND EXISTS (SELECT AD_Element_ID FROM AD_Element WHERE AD_Element.ColumnName=i.ColumnName) ").append(clientCheck);
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Set Element ID=" + no);

		// set AD_Reference_ID
		sql = new StringBuffer ("UPDATE I_Column i "
				+ "SET AD_Reference_ID=(SELECT AD_Reference_ID FROM AD_Reference WHERE AD_Reference.Name=i.ReferenceName) "
				+ "WHERE (i.AD_Reference_ID IS NULL OR i.AD_Reference_ID <= 0) "
				+ " AND I_IsImported<>'Y' AND i.ReferenceName IS NOT NULL "
				+ " AND EXISTS (SELECT AD_Reference_ID FROM AD_Reference WHERE AD_Reference.Name=i.ReferenceName) ").append(clientCheck);
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Set Reference ID=" + no);

		// set AD_Reference_Value_ID
		sql = new StringBuffer ("UPDATE I_Column i "
				+ "SET AD_Reference_Value_ID=(SELECT AD_Reference_ID FROM AD_Reference WHERE AD_Reference.Name=i.ReferenceValueName) "
				+ "WHERE (i.AD_Reference_Value_ID IS NULL OR i.AD_Reference_Value_ID <= 0) "
				+ " AND I_IsImported<>'Y' AND i.ReferenceValueName IS NOT NULL "
				+ " AND EXISTS (SELECT AD_Reference_ID FROM AD_Reference WHERE AD_Reference.Name=i.ReferenceValueName) ").append(clientCheck);
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Set Reference Value ID=" + no);
		
		// set Name from AD_Element
		sql = new StringBuffer ("UPDATE I_Column i "
				+ "SET Name=(SELECT Name FROM AD_Element WHERE AD_Element.AD_Element_ID=i.AD_Element_ID) "
				+ "WHERE (i.AD_Element_ID IS NOT NULL) "
				+ " AND I_IsImported<>'Y' AND i.Name IS NULL "
				+ " AND EXISTS (SELECT Name FROM AD_Element WHERE AD_Element.AD_Element_ID=i.AD_Element_ID) ").append(clientCheck);
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Set Name from Element = " + no);

		// set ColumnName from AD_Element
		sql = new StringBuffer ("UPDATE I_Column i "
				+ "SET ColumnName=(SELECT ColumnName FROM AD_Element WHERE AD_Element.AD_Element_ID=i.AD_Element_ID) "
				+ "WHERE (i.AD_Element_ID IS NOT NULL) "
				+ " AND I_IsImported<>'Y' AND i.ColumnName IS NULL "
				+ " AND EXISTS (SELECT ColumnName FROM AD_Element WHERE AD_Element.AD_Element_ID=i.AD_Element_ID) ").append(clientCheck);
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Set ColumnName from Element = " + no);
		
		// set AD_Reference_ID from AD_Element
		sql = new StringBuffer ("UPDATE I_Column i "
				+ "SET AD_Reference_ID=(SELECT AD_Reference_ID FROM AD_Element WHERE AD_Element.AD_Element_ID=i.AD_Element_ID) "
				+ "WHERE (i.AD_Element_ID IS NOT NULL) "
				+ " AND I_IsImported<>'Y' AND i.AD_Reference_ID IS NULL "
				+ " AND EXISTS (SELECT AD_Reference_ID FROM AD_Element WHERE AD_Element.AD_Element_ID=i.AD_Element_ID) ").append(clientCheck);
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Set AD_Reference_ID from Element = " + no);

		// set AD_Reference_Value_ID from AD_Element
		sql = new StringBuffer ("UPDATE I_Column i "
				+ "SET AD_Reference_Value_ID=(SELECT AD_Reference_Value_ID FROM AD_Element WHERE AD_Element.AD_Element_ID=i.AD_Element_ID) "
				+ "WHERE (i.AD_Element_ID IS NOT NULL) "
				+ " AND I_IsImported<>'Y' AND i.AD_Reference_Value_ID IS NULL "
				+ " AND EXISTS (SELECT AD_Reference_Value_ID FROM AD_Element WHERE AD_Element.AD_Element_ID=i.AD_Element_ID) ").append(clientCheck);
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Set AD_Reference_Value_ID from Element = " + no);
		
		// set FieldLength from AD_Element
		sql = new StringBuffer ("UPDATE I_Column i "
				+ "SET FieldLength=(SELECT FieldLength FROM AD_Element WHERE AD_Element.AD_Element_ID=i.AD_Element_ID) "
				+ "WHERE (i.AD_Element_ID IS NOT NULL) "
				+ " AND I_IsImported<>'Y' AND (i.FieldLength IS NULL OR i.FieldLength <= 0) "
				+ " AND EXISTS (SELECT FieldLength FROM AD_Element WHERE AD_Element.AD_Element_ID=i.AD_Element_ID) ").append(clientCheck);
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Set FieldLength from Element = " + no);

		// set Description from AD_Element
		sql = new StringBuffer ("UPDATE I_Column i "
				+ "SET Description=(SELECT Description FROM AD_Element WHERE AD_Element.AD_Element_ID=i.AD_Element_ID) "
				+ "WHERE (i.AD_Element_ID IS NOT NULL) "
				+ " AND I_IsImported<>'Y' AND i.Description IS NULL "
				+ " AND EXISTS (SELECT Description FROM AD_Element WHERE AD_Element.AD_Element_ID=i.AD_Element_ID) ").append(clientCheck);
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Set Description from Element = " + no);

		// set Help from AD_Element
		sql = new StringBuffer ("UPDATE I_Column i "
				+ "SET Help=(SELECT Help FROM AD_Element WHERE AD_Element.AD_Element_ID=i.AD_Element_ID) "
				+ "WHERE (i.AD_Element_ID IS NOT NULL) "
				+ " AND I_IsImported<>'Y' AND i.Help IS NULL "
				+ " AND EXISTS (SELECT Help FROM AD_Element WHERE AD_Element.AD_Element_ID=i.AD_Element_ID) ").append(clientCheck);
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Set Help from Element = " + no);

		// set AD_Window_ID
		sql = new StringBuffer ("UPDATE I_Column i "
				+ "SET AD_Window_ID=(SELECT AD_Window_ID FROM AD_Window WHERE AD_Window.Name=i.WindowName) "
				+ "WHERE (i.AD_Window_ID IS NULL OR i.AD_Window_ID <= 0) "
				+ " AND I_IsImported<>'Y' AND i.WindowName IS NOT NULL "
				+ " AND EXISTS (SELECT AD_Window_ID FROM AD_Window WHERE AD_Window.Name=i.WindowName) ").append(clientCheck);
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Set AD_Window_ID=" + no);
		
		// set AD_Tab_ID
		sql = new StringBuffer ("UPDATE I_Column i "
				+ "SET AD_Tab_ID=(SELECT AD_Tab_ID FROM AD_Tab WHERE AD_Tab.Name=i.TabName "
				+ " AND AD_Tab.AD_Window_ID=i.AD_Window_ID) "
				+ "WHERE (i.AD_Tab_ID IS NULL OR i.AD_Tab_ID <= 0) "
				+ " AND I_IsImported<>'Y' AND i.TabName IS NOT NULL "
				+ " AND EXISTS (SELECT AD_Tab_ID FROM AD_Tab WHERE AD_Tab.Name=i.TabName "
				+ " AND AD_Tab.AD_Window_ID=i.AD_Window_ID) ").append(clientCheck);
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Set AD_Tab_ID=" + no);
		
		// set AD_Field_ID
		sql = new StringBuffer ("UPDATE I_Column i "
				+ "SET AD_Field_ID=(SELECT AD_Field_ID FROM AD_Field WHERE AD_Field.AD_Tab_ID=i.AD_Tab_ID "
				+ " AND AD_Field.Name=i.Name) "
				+ "WHERE (i.AD_Field_ID IS NULL OR i.AD_Field_ID <= 0) "
				+ " AND I_IsImported<>'Y' "
				+ " AND EXISTS (SELECT AD_Field_ID FROM AD_Field WHERE AD_Field.AD_Tab_ID=i.AD_Tab_ID "
				+ " AND AD_Field.Name=i.Name) ").append(clientCheck);
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Set AD_Field_ID=" + no);
		
		// set AD_FieldGroup_ID
		sql = new StringBuffer ("UPDATE I_Column i "
				+ "SET AD_FieldGroup_ID=(SELECT MAX(AD_FieldGroup_ID) FROM AD_FieldGroup WHERE AD_FieldGroup.Name=i.FieldGroupName) "
				+ "WHERE (i.AD_FieldGroup_ID IS NULL OR i.AD_FieldGroup_ID <= 0) "
				+ " AND I_IsImported<>'Y' AND i.FieldGroupName IS NOT NULL "
				+ " AND EXISTS (SELECT AD_FieldGroup_ID FROM AD_FieldGroup WHERE AD_FieldGroup.Name=i.FieldGroupName) ").append(clientCheck);
		no = DB.executeUpdateEx(sql.toString(), get_TrxName());
		log.fine("Set AD_FieldGroup_ID=" + no);
		
		// Validations
		sql = new StringBuffer ("UPDATE I_Column "
			+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Table, ' "
			+ "WHERE (AD_Table_ID IS NULL "
			+ "OR NOT EXISTS (SELECT * FROM AD_Table WHERE AD_Table.AD_Table_ID=I_Column.AD_Table_ID))"
			+ " AND I_IsImported<>'Y'").append(clientCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		log.config("Invalid Table=" + no);
		
		sql = new StringBuffer ("UPDATE I_Column "
				+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid AD_Column_ID, ' "
				+ "WHERE (AD_Column_ID IS NOT NULL "
				+ "AND NOT EXISTS (SELECT * FROM AD_Column WHERE AD_Column.AD_Column_ID=I_Column.AD_Column_ID))"
				+ " AND I_IsImported<>'Y'").append(clientCheck);
			no = DB.executeUpdate(sql.toString(), get_TrxName());
			log.config("Invalid AD_Column_ID = " + no);

		sql = new StringBuffer ("UPDATE I_Column "
				+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Reference, ' "
				+ "WHERE (AD_Reference_ID IS NULL "
				+ "OR NOT EXISTS (SELECT * FROM AD_Reference WHERE AD_Reference.AD_Reference_ID=I_Column.AD_Reference_ID)) "
				+ " AND I_IsImported<>'Y'").append(clientCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		log.config("Invalid Reference=" + no);	
		
		sql = new StringBuffer ("UPDATE I_Column "
				+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Required Reference Value, ' "
				+ "WHERE AD_Reference_ID IN (17,18,28,30) AND (AD_Reference_Value_ID IS NULL "
				+ "OR NOT EXISTS (SELECT * FROM AD_Reference WHERE AD_Reference.AD_Reference_ID=I_Column.AD_Reference_Value_ID)) "
				+ " AND I_IsImported<>'Y'").append(clientCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		log.config("Invalid Reference Value=" + no);	
		
		sql = new StringBuffer ("UPDATE I_Column "
				+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid FieldLength, ' "
				+ "WHERE (FieldLength IS NULL "
				+ "OR FieldLength <= 0) "
				+ " AND I_IsImported<>'Y'").append(clientCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		log.config("Invalid FieldLength=" + no);	
		
		sql = new StringBuffer ("UPDATE I_Column "
				+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Name, ' "
				+ "WHERE (Name IS NULL "
				+ "OR Name = '') "
				+ " AND I_IsImported<>'Y'").append(clientCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		log.config("Invalid Name=" + no);
		
		sql = new StringBuffer ("UPDATE I_Column "
				+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid ColumnName, ' "
				+ "WHERE (ColumnName IS NULL "
				+ "OR ColumnName = '') "
				+ " AND I_IsImported<>'Y'").append(clientCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		log.config("Invalid ColumnName=" + no);
		
		sql = new StringBuffer ("UPDATE I_Column "
				+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Window, ' "
				+ "WHERE WindowName IS NOT NULL "
				+ "AND WindowName != '' AND COALESCE(AD_Window_ID,0) <= 0 "
				+ " AND I_IsImported<>'Y'").append(clientCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		log.config("Invalid Window=" + no);
		
		sql = new StringBuffer ("UPDATE I_Column "
				+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Tab, ' "
				+ "WHERE TabName IS NOT NULL "
				+ "AND TabName != '' AND COALESCE(AD_Tab_ID,0) <= 0 "
				+ " AND I_IsImported<>'Y'").append(clientCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		log.config("Invalid Tab=" + no);

		int noInsert = 0;
		int noUpdate = 0;
		
		Map<String, Integer> newFieldGroups = new HashMap<String, Integer>();
		int currentFieldGroupId = 0;

		//	Go through Records
		sql = new StringBuffer ("SELECT * FROM I_Column "
				+ "WHERE I_IsImported='N'").append(clientCheck);
		sql.append(" ORDER BY AD_Table_ID, TableName, I_Column_ID");
		PreparedStatement pstmt =  null;
		ResultSet rs = null;
		X_I_Column impCol = null;
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), get_TrxName());
			rs = pstmt.executeQuery();

			MTable table = null;

			while (rs.next())
			{	

				impCol = new X_I_Column (getCtx(), rs, get_TrxName());
				log.fine("I_Column_ID=" + impCol.getI_Column_ID()
						+ ", ColumnName=" + impCol.getColumnName()
						+ ", AD_Table_ID=" + impCol.getAD_Table_ID());

				if ( table ==null || impCol.getAD_Table_ID() != table.getAD_Table_ID() )
				{
					table = MTable.get(getCtx(), impCol.getAD_Table_ID());
					table.set_TrxName(get_TrxName());
				}

				// create element
				if ( impCol.getAD_Element_ID() == 0 )
				{
					M_Element el = M_Element.get(getCtx(), impCol.getColumnName(), get_TrxName());
					if ( el == null )
					{
						el = new M_Element(getCtx(), 0, get_TrxName());
						el.setAD_Client_ID(impCol.getAD_Client_ID());
						el.setAD_Org_ID(impCol.getAD_Org_ID());
						el.setColumnName(impCol.getColumnName());
						el.setName(impCol.getName());
						el.setPrintName(impCol.getName());
						el.setDescription(impCol.getDescription());
						el.setHelp(impCol.getHelp());
						el.setAD_Reference_ID(impCol.getAD_Reference_ID());
						el.setAD_Reference_Value_ID(impCol.getAD_Reference_Value_ID());
						el.setFieldLength(impCol.getFieldLength());
						el.saveEx();
					}
					
					impCol.setAD_Element_ID(el.getAD_Element_ID());
					impCol.saveEx();
				}

				MColumn col = new MColumn (getCtx(), impCol.getAD_Column_ID(), get_TrxName());
				if (impCol.getAD_Column_ID() == 0)	//	Insert new Column
				{
					PO.copyValues(impCol, col, true);
					
					col.saveEx();
					//col.syncDatabase();
					noInsert++;
					
				}
				else  // Update existing columns
				{
					PO.copyValues(impCol, col, true);
					
					col.saveEx();
					noUpdate++;
				}
				

				if ( col != null )
				{
					impCol.setAD_Column_ID(col.getAD_Column_ID());
					impCol.setI_ErrorMsg(null);
					impCol.setI_IsImported(true);
					impCol.setProcessed(true);
					impCol.saveEx();
					
					
					if ( impCol.getAD_Window_ID() != 0 && impCol.getAD_Tab_ID() != 0 && impCol.getAD_Field_ID() == 0 )
					{
						MTab tab = new MTab(getCtx(), impCol.getAD_Tab_ID(), get_TrxName());
						MField field = new MField(tab);
						
						field.setColumn(col);
						if ( impCol.getDisplayLength() > 0 )
							field.setDisplayLength(impCol.getDisplayLength());
						field.setIsDisplayed(impCol.isDisplayed());
						if ( impCol.getSeqNoField() > 0 )
							field.setSeqNo(impCol.getSeqNoField());
						field.setIsDisplayedGrid(impCol.isDisplayedGrid()); 
						if ( impCol.getSeqNoGrid() > 0 )
							field.setSeqNoGrid(impCol.getSeqNoGrid());
						field.setIsSameLine(impCol.isSameLine());
						
						if (col.isKey()) {
							field.setIsDisplayed(false);
							field.setIsDisplayedGrid(false); 
						}
						
						int fieldGroupId = impCol.getAD_FieldGroup_ID();
						if ( fieldGroupId == 0 && !Util.isEmpty(impCol.getFieldGroupName()))
						{
							if ( newFieldGroups.containsKey(impCol.getFieldGroupName()))
								fieldGroupId = newFieldGroups.get(impCol.getFieldGroupName());
							else {
								X_AD_FieldGroup group = new X_AD_FieldGroup(getCtx(), 0, get_TrxName());
								group.setName(impCol.getFieldGroupName());
								group.setEntityType(impCol.getEntityType());
								group.setFieldGroupType(X_AD_FieldGroup.FIELDGROUPTYPE_Collapse);
								group.saveEx();
								fieldGroupId = group.getAD_FieldGroup_ID();
								newFieldGroups.put(impCol.getFieldGroupName(), fieldGroupId);
							}
						}

						if (fieldGroupId > 0 )
						{
							field.setAD_FieldGroup_ID(fieldGroupId);
							impCol.setAD_FieldGroup_ID(fieldGroupId);
						}
						
						field.saveEx();
						impCol.setAD_Field_ID(field.getAD_Field_ID());
						impCol.saveEx();

					}	

				}
			}
		}
		catch (Exception e) {
			
			impCol.setI_ErrorMsg("Error saving column, review log");
			impCol.saveEx();
			
			log.log(Level.SEVERE, "Failed processing import records", e);
		}
		finally {
			DB.close(rs,pstmt);
		}

		return "Inserted: " + noInsert + " / Updated: " + noUpdate;
	}	//	doIt

}	//	ImportColumn
