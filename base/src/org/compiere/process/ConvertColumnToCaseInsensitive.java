package org.compiere.process;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.adempiere.exceptions.AdempiereException;
import org.apache.commons.lang.StringUtils;
import org.compiere.util.DB;
import org.compiere.util.Trx;


/**
 * 
 * Used Stored Procedure as provided in this link
 * http://pretius.com/postgresql-stop-worrying-about-table-and-view-dependencies/
 * to drop and restore dependendent views
 * 
 * @author jtrinidad
 *
 */
public class ConvertColumnToCaseInsensitive extends SvrProcess {

	private static final String[] TARGET_COLUMNS = {"value"};
	
	private static String SQL_CREATE_EXTENSION = "CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA adempiere;";
	
	private static final String SQL_AFFECTED_TABLES = "select table_name, " +
			        "  (SELECT i.relname " +
			        "   FROM pg_class t, pg_class i, pg_index ix, pg_attribute a " +
			        "   WHERE t.oid = ix.indrelid " +
			        "   AND   i.oid = ix.indexrelid " +
			        "   AND   a.attrelid = t.oid " +
			        "   AND   a.attnum = ANY (ix.indkey) " +
			        "   AND   t.relkind = 'r' " +
			        "   AND   a.attname = column_name " +
			        "   AND   t.relname = table_name)  " +
			        "AS index_name " +
			        "FROM information_schema.columns " +
			        "WHERE UPPER(column_name)= UPPER(?) " +
			        "AND data_type='character varying' " +
			        "AND   table_name IN (SELECT tablename " +
			        "                     FROM pg_catalog.pg_tables " +
			        "                     WHERE schemaname = 'adempiere') " +
			        "ORDER BY table_name";			
			
	private int nTables = 0;
	private int nReIndex = 0;
	private ArrayList<String> tblAffected = new ArrayList<String>();
	private ArrayList<String> tblFailedToConvert = new ArrayList<String>();

	private String trxName;
			
	@Override
	protected void prepare() {

	}

	@Override
	protected String doIt() throws Exception {
		trxName = Trx.createTrxName("CITEXT");

		try {
			DB.executeUpdate(SQL_CREATE_EXTENSION, trxName);
			DB.commit(true, trxName);
		} catch (Exception e) {
			try {
				DB.rollback(false, trxName);
			} catch (SQLException e1) {
				
			}
			return "Unable to create citext extension. Please check if you installed in OS";
		}
		
		dropAllDependencies();
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			for (String targetColumn : TARGET_COLUMNS)
			{
				pstmt = DB.prepareStatement(SQL_AFFECTED_TABLES, trxName);
				pstmt.setString(1, targetColumn);
				rs = pstmt.executeQuery();
				
				nTables = 0;
				nReIndex = 0;
				while (rs.next())
				{
					String tableName = rs.getString("table_name");
					String indexName = rs.getString("index_name");
					convertColumn(tableName, targetColumn, indexName);
					
				}
				addLog(0, null, new BigDecimal(nTables), "Tables updated for Column=" + targetColumn);
				addLog(0, null, new BigDecimal(nReIndex), "Indexes rebuilt for Column=" + targetColumn);
			}
		} catch (Exception e) {
			throw new AdempiereException(e.getMessage());
			
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
		restoreAllDependencies();
		
		if (tblFailedToConvert.size() > 0) {
			addLog(0, null, new BigDecimal(tblFailedToConvert.size()), "Failed to convert due to duplicate records.");
			int n = 0;
			for (String t : tblFailedToConvert) {
				n++;
				addLog(0, null, new BigDecimal(n), t);
			}
			
		}
		
		return "";
	}
	
	
	private void convertColumn(String tableName, String columnName, String indexName) 
	{
		try {
			String sql = "alter table " + tableName + " alter column " + columnName + " type citext;";
			DB.executeUpdateEx(sql, trxName);
			nTables++;
			
			if (indexName != null && indexName.length() > 0) {
				sql = "reindex index " + indexName + ";";
				DB.executeUpdateEx(sql, trxName);
				nReIndex++;
			}
			
			DB.commit(true, trxName);
		} catch (Exception e) {
			try {
				DB.rollback(false, trxName);
			} catch (SQLException e1) {
				
			}
			tblFailedToConvert.add(tableName);
			getDuplicates(tableName, columnName);
			log.severe(e.getMessage());
		}
	}	
	
	private void dropAllDependencies()
	{
		String cols = "('";
		int n = 0;
		for (String targetColumn : TARGET_COLUMNS) {
			cols = cols + targetColumn + "'";
			n++;
			if (n == TARGET_COLUMNS.length) {
				cols = cols + ") ";
			} else {
				cols = cols + ", ";
				
			}
		}
		
		String sql = "SELECT table_name " +
	             "FROM information_schema.columns " +
	             "WHERE lower(column_name) IN " + cols + 
	             "AND   table_name IN (SELECT tablename " +
	             "                     FROM pg_catalog.pg_tables " +
	             "                     WHERE schemaname = 'adempiere') " +
	             "ORDER BY table_name";
		
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, trxName);
			rs = pstmt.executeQuery();
			
			while (rs.next())
			{
				String tableName = rs.getString("table_name");
				log.fine("Affected table added. Table Name = " + tableName);
				tblAffected.add(tableName);
				
			}

			for (String t: tblAffected) {
				dropDependencies(t, pstmt);
			}
		} catch (Exception e) {
			throw new AdempiereException(e.getMessage());
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
	}
	
	private void restoreAllDependencies()
	{
		PreparedStatement pstmt = null;
		for (String t: tblAffected) {
			restoreDependencies(t, pstmt);
		}
		pstmt = null;
	}
	
	private void dropDependencies(String tableName, PreparedStatement pstmt)
	{
		String sql = "select deps_save_and_drop_dependencies('adempiere', '" + tableName + "')";
		try {
			pstmt = DB.prepareStatement(sql, trxName);
			pstmt.execute();
			DB.commit(true, trxName);		
		} catch (Exception e) {
			throw new AdempiereException(e.getMessage());
		}
	}
	
	private void restoreDependencies(String tableName, PreparedStatement pstmt)
	{
		String sql = "select deps_restore_dependencies('adempiere', '" + tableName + "')";
		try {
			pstmt = DB.prepareStatement(sql, trxName);
			pstmt.execute();
			DB.commit(true, trxName);
		} catch (Exception e) {
			throw new AdempiereException(e.getMessage());
		}
		
	}
	
	private void getDuplicates(String tableName, String columnName)
	{
		ArrayList<String> duplicates = new ArrayList<String>();
		
		String sql = "select " + columnName + " from " + tableName  
				+ " where upper(" + columnName + ") IN (select UPPER(" 
				+ columnName + ") from " + tableName
				+ " group by upper(" + columnName + ") having count(*) >1);";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, trxName);
			rs = pstmt.executeQuery();
			
			while (rs.next())
			{
				String s = rs.getString("value");
				duplicates.add(s);
			}

		} catch (Exception e) {
			log.severe(e.getMessage());
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		if (duplicates.size() == 0) {
			return;
		}
		addLog(0, null, new BigDecimal(duplicates.size()), "Duplicates Table=" + tableName + "->" +  StringUtils.abbreviate(StringUtils.join(duplicates.iterator(), ","), 300));

	}

}
