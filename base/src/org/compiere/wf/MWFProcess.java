/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.wf;

import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MTable;
import org.compiere.model.PO;
import org.compiere.model.X_AD_WF_Process;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfo;
import org.compiere.process.StateEngine;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;


/**
 *	Workflow Process
 *	
 *  @author Jorg Janke
 *  @version $Id: MWFProcess.java,v 1.2 2006/07/30 00:51:05 jjanke Exp $
 */
public class MWFProcess extends X_AD_WF_Process
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4447369291008183913L;

	/**
	 * 	Standard Constructor
	 *	@param ctx context
	 *	@param AD_WF_Process_ID process
	 *	@param trxName transaction
	 */
	public MWFProcess (Properties ctx, int AD_WF_Process_ID, String trxName)
	{
		super (ctx, AD_WF_Process_ID, trxName);
		if (AD_WF_Process_ID == 0)
			throw new IllegalArgumentException ("Cannot create new WF Process directly");
	}	//	MWFProcess

	/**
	 * 	Load Constructor
	 *	@param ctx context
	 *	@param rs result set
	 *	@param trxName transaction
	 */
	public MWFProcess (Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}	//	MWFProcess
	
	/**
	 * 	New Constructor
	 *	@param wf workflow
	 *	@param pi Process Info (Record_ID)
	 *  @param trxName 
	 *	@throws Exception
	 */
	public MWFProcess (MWorkflow wf, ProcessInfo pi, String trxName) throws Exception
	{
		super (wf.getCtx(), 0, trxName);
		if (!TimeUtil.isValid(wf.getValidFrom(), wf.getValidTo()))
			throw new IllegalStateException("Workflow not valid");
		m_wf = wf;
		setAD_Workflow_ID (wf.getAD_Workflow_ID());
		setPriority(wf.getPriority());
		super.setWFState (WFSTATE_NotStarted);
		
		//	Document
		setAD_Table_ID(wf.getAD_Table_ID());
		setRecord_ID(pi.getRecord_ID());
		if (getPO() == null)
		{
			setTextMsg("No PO with ID=" + pi.getRecord_ID());
			addTextMsg(new Exception(""));
			super.setWFState (WFSTATE_Terminated);
		}
		else
			setTextMsg(getPO());
		
		setProcessed (false);
		//	Lock Entity
		getPO();
	}	//	MWFProcess

	/**	Workflow					*/
	private MWorkflow			m_wf = null;
	/**	Persistent Object			*/
	private PO					m_po = null;
	/** Message from Activity		*/
	private String				m_processMsg = null;
	

	/**
	 * 	Get Workflow
	 *	@return workflow
	 */
	private MWorkflow getWorkflow()
	{
		if (m_wf == null)
			m_wf = MWorkflow.get (getCtx(), getAD_Workflow_ID());
		if (m_wf.get_ID() == 0)
			throw new IllegalStateException("Not found - AD_Workflow_ID=" + getAD_Workflow_ID());
		return m_wf;
	}	//	getWorkflow
	
	/**
	 * 	Start "WorkFlow" Execution
	 *  Workflow engine deprecated, replace with hard coded Document Process
	 *	@return true if success
	 */
	public boolean startWork()
	{
		log.fine("AD_Workflow_ID=" + getWorkflow().getAD_Workflow_ID());
		setWFState(WFSTATE_Running);
		try
		{
			performWork(DocAction.ACTION_Prepare);  // throws on error
			performWork(DocAction.ACTION_Complete); 

		}
		catch (Throwable e)
		{
			log.log(Level.SEVERE, "AD_Workflow_ID=" + getWorkflow().getAD_Workflow_ID(), e);
			setTextMsg(e.toString());
			addTextMsg(e);
			setWFState(StateEngine.STATE_Terminated);
			return false;
		}

		setWFState(WFSTATE_Completed);
		return true;
	}	//	performStart
	
	/**
	 * 	Perform Work.
	 * 	Set Text Msg.
	 * 	@param trx transaction
	 *	@return true if completed, false otherwise
	 *	@throws Exception if error
	 *  Moved from MWFActivity
	 */
	private boolean performWork (String action) throws Exception
	{
		log.info ("MWFProcess: " + this.toString() + " [" + get_TrxName() + "]");
		String docStatus = null;
		
			log.fine("DocumentAction=" + action);
			getPO();
			if (m_po == null)
				throw new Exception("Persistent Object not found - AD_Table_ID=" 
					+ getAD_Table_ID() + ", Record_ID=" + getRecord_ID());
			boolean success = false;
			String processMsg = null;
			DocAction doc = null;
			if (m_po instanceof DocAction)
			{
				doc = (DocAction)m_po;
				//
				try {
					success = doc.processIt (action);	//	** Do the work
					setTextMsg(doc.getSummary());
					processMsg = doc.getProcessMsg();
					// Bug 1904717 - Invoice reversing has incorrect doc status
					// Just prepare and complete return a doc status to take into account
					// the rest of methods return boolean, so doc status must not be taken into account when not successful
					if (   DocAction.ACTION_Prepare.equals(action)
						|| DocAction.ACTION_Complete.equals(action) 
						|| success)
						docStatus = doc.getDocStatus();
				}
				catch (Exception e) {
					setProcessMsg(e.getLocalizedMessage());
					throw e;
				}
				
				setProcessMsg(processMsg);
			}
			else
				throw new IllegalStateException("Persistent Object not DocAction - "
					+ m_po.getClass().getName()
					+ " - AD_Table_ID=" + getAD_Table_ID() + ", Record_ID=" + getRecord_ID());
			//
			if (!m_po.save())
			{
				success = false;
				processMsg = "SaveError";
			}
			if (!success)
			{
				if (processMsg == null || processMsg.length() == 0)
				{
					processMsg = "PerformWork Error - " + this.toString();
					if (doc != null)	//	problem: status will be rolled back
						processMsg += " - DocStatus=" + doc.getDocStatus();
				}
				throw new Exception(processMsg);
			}
			return success;
		}	//	DocumentAction	
	

	/**************************************************************************
	 * 	Get Persistent Object
	 *	@return po
	 */
	public PO getPO()
	{
		if (m_po != null)
			return m_po;
		if (getRecord_ID() == 0)
			return null;
		
		MTable table = MTable.get (getCtx(), getAD_Table_ID());
		m_po = table.getPO(getRecord_ID(), get_TrxName());
		return m_po;
	}	//	getPO

	/**
	 * 	Set Text Msg (add to existing)
	 *	@param po base object
	 */
	public void setTextMsg (PO po)
	{
		if (po != null && po instanceof DocAction)
			setTextMsg(((DocAction)po).getSummary());
	}	//	setTextMsg	

	/**
	 * 	Set Text Msg (add to existing)
	 *	@param TextMsg msg
	 */
	public void setTextMsg (String TextMsg)
	{
		String oldText = getTextMsg();
		if (oldText == null || oldText.length() == 0)
			super.setTextMsg (TextMsg);
		else if (TextMsg != null && TextMsg.length() > 0)
			super.setTextMsg (oldText + "\n - " + TextMsg);
	}	//	setTextMsg	

	/**
	 * 	Add to Text Msg
	 *	@param obj some object
	 */
	public void addTextMsg (Object obj)
	{
		if (obj == null)
			return;
		//
		StringBuffer TextMsg = new StringBuffer ();
		if (obj instanceof Exception)
		{
			Exception ex = (Exception)obj;
			if (ex.getMessage() != null && ex.getMessage().trim().length() > 0)
			{
				TextMsg.append(ex.toString());
			}
			else if (ex instanceof NullPointerException)
			{
				TextMsg.append(ex.getClass().getName());
			}
			while (ex != null)
			{
				StackTraceElement[] st = ex.getStackTrace();
				for (int i = 0; i < st.length; i++)
				{
					StackTraceElement ste = st[i];
					if (i == 0 || ste.getClassName().startsWith("org.compiere") || ste.getClassName().startsWith("org.adempiere"))
						TextMsg.append(" (").append(i).append("): ")
							.append(ste.toString())
							.append("\n");
				}
				if (ex.getCause() instanceof Exception)
					ex = (Exception)ex.getCause();
				else
					ex = null;
			}
		}
		else
		{
			TextMsg.append(obj.toString());
		}
		//
		String oldText = getTextMsg();
		if (oldText == null || oldText.length() == 0)
			super.setTextMsg(Util.trimSize(TextMsg.toString(),1000));
		else if (TextMsg != null && TextMsg.length() > 0)
			super.setTextMsg(Util.trimSize(oldText + "\n - " + TextMsg.toString(),1000));
	}	//	addTextMsg
	
	/**
	 * 	Set Runtime (Error) Message
	 *	@param msg message
	 */
	public void setProcessMsg (String msg)
	{
		m_processMsg = msg;
		if (msg != null && msg.length() > 0)
			setTextMsg(msg);
	}	//	setProcessMsg
	
	/**
	 * 	Get Runtime (Error) Message
	 *	@return msg
	 */
	public String getProcessMsg()
	{
		return m_processMsg;
	}	//	getProcessMsg
	
}	//	MWFProcess
