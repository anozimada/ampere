/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.util;

import java.awt.Dimension;
import java.awt.Point;
import java.io.File;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.adempiere.exceptions.AdempiereException;

/**
 *	Load & Save INI Settings from property file
 *	Initiated in Adempiere.startup
 *	Settings activated in ALogin.getIni
 * 
 *  @author     Jorg Janke
 *  @version    $Id$
 * 
 * @author Teo Sarca, www.arhipac.ro
 * 			<li>FR [ 1658127 ] Select charset encoding on import
 * 			<li>FR [ 2406123 ] Ini.saveProperties fails if target directory does not exist
 */
public final class Ini implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3666529972922769528L;

	/** Property file name				*/
	public static final String	ADEMPIERE_PROPERTY_FILE = "Adempiere.properties";

	/** Trace Level			*/
	public static final String	P_TRACELEVEL = 		"TraceLevel";
	private static final String DEFAULT_TRACELEVEL = "WARNING";
	/** Auto Save			*/
	public static final String  P_A_COMMIT =		"AutoCommit";
	private static final boolean DEFAULT_A_COMMIT =	true;
	/** Auto New Record		*/
	public static final String	P_A_NEW =			"AutoNew";
	private static final boolean DEFAULT_A_NEW =	false;
	/** Dictionary Maintenance	*/
	public static final String  P_ADEMPIERESYS =		"AdempiereSys";	//	Save system records
	private static final boolean DEFAULT_ADEMPIERESYS = false;
	/** Log Migration Script	*/
	public static final String  P_LOGMIGRATIONSCRIPT =		"LogMigrationScript";	//	Log migration script
	private static final boolean DEFAULT_LOGMIGRATIONSCRIPT = false;
	/** Show Acct Tabs			*/
	public static final String  P_SHOW_ACCT =		"ShowAcct";
	private static final boolean DEFAULT_SHOW_ACCT = true;
	/** Show Advanced Tabs		*/
	public static final String  P_SHOW_ADVANCED =	"ShowAdvanced";
	private static final boolean DEFAULT_SHOW_ADVANCED = true;
	/** Show Translation Tabs	*/
	public static final String  P_SHOW_TRL =		"ShowTrl";
	private static final boolean DEFAULT_SHOW_TRL =	false;
	/** Cache Windows			*/
	public static final String  P_CACHE_WINDOW =	"CacheWindow";
	private static final boolean DEFAULT_CACHE_WINDOW = true;
	/** Printer Name			*/
	public static final String  P_PRINTER =			"Printer";
	private static final String  DEFAULT_PRINTER =	"";
	/** Current Date			*/
	public static final String  P_TODAY =       	"CDate";
	private static final Timestamp DEFAULT_TODAY =	new Timestamp(System.currentTimeMillis());
	
	
	public static final String P_HEADLINE_TEXT = "Headline.Text";
	public static final String P_HEADLINE_FGCOLOR = "Headline.FGColor";
	public static final String DEFAULT_HEADLINE_FGCOLOR = "blue";
	public static final String P_HEADLINE_BGCOLOR = "Headline.BGColor";
	public static final String DEFAULT_HEADLINE_BGCOLOR = "yellow";
	
	
	/** Ini Properties		*/
	private static final String[]   PROPERTIES = new String[] {
		P_TRACELEVEL,
		P_A_COMMIT, P_A_NEW, 
		P_ADEMPIERESYS, P_LOGMIGRATIONSCRIPT, P_SHOW_ACCT, P_SHOW_TRL, 
		P_SHOW_ADVANCED, P_CACHE_WINDOW,
		P_PRINTER, P_TODAY,
		P_HEADLINE_TEXT, P_HEADLINE_FGCOLOR, P_HEADLINE_BGCOLOR
		
	};
	/** Ini Property Values	*/
	private static final String[]   VALUES = new String[] {
		DEFAULT_TRACELEVEL,
		DEFAULT_A_COMMIT?"Y":"N", DEFAULT_A_NEW?"Y":"N",
		DEFAULT_ADEMPIERESYS?"Y":"N", DEFAULT_LOGMIGRATIONSCRIPT?"Y":"N", DEFAULT_SHOW_ACCT?"Y":"N", DEFAULT_SHOW_TRL?"Y":"N", 
		DEFAULT_SHOW_ADVANCED?"Y":"N", DEFAULT_CACHE_WINDOW?"Y":"N",
		DEFAULT_PRINTER, DEFAULT_TODAY.toString(),
		"", DEFAULT_HEADLINE_FGCOLOR, DEFAULT_HEADLINE_BGCOLOR
	};

	/**	Container for Properties    */
	private static Properties 		s_prop = new Properties();
	
	private static String s_propertyFileName = null;
	
	/**	Logger						*/
	private static Logger			log = null;

	/**
	 *	Load INI parameters from disk
	 *  @param reload reload
	 */
	public static void loadProperties (boolean reload)
	{
		if (reload || s_prop.size() == 0)
		{
			loadProperties(getFileName(s_client));
		}
	}	//	loadProperties

	
	/**
	 *  Load INI parameters from filename.
	 *  Logger is on default level (INFO)
	 *	@param filename to load
	 *	@return true if first time
	 */
	public static boolean loadProperties (String filename)
	{
		checkProperties();
		
		s_loaded = true;
		s_propertyFileName = filename;
		
		return true;
	}	//	loadProperties

	private static void checkProperties() {
		//	Check/set properties	defaults
		for (int i = 0; i < PROPERTIES.length; i++)
		{
			if (VALUES[i].length() > 0)
				checkProperty(PROPERTIES[i], VALUES[i]);
		}

		//
		String tempDir = System.getProperty("java.io.tmpdir");
		if (tempDir == null || tempDir.length() == 1)
			tempDir = getAdempiereHome();
		if (tempDir == null)
			tempDir = "";
	}
	
	/**
	 *	Load property and set to default, if not existing
	 *
	 * 	@param key   Key
	 * 	@param defaultValue   Default Value
	 * 	@return Property
	 */
	private static String checkProperty (String key, String defaultValue)
	{
		String result = null;
		if (!isClient())
			result = s_prop.getProperty (key, SecureInterface.CLEARVALUE_START + defaultValue + SecureInterface.CLEARVALUE_END);
		else
			result = s_prop.getProperty (key, SecureEngine.encrypt(defaultValue));
		s_prop.setProperty (key, result);
		return result;
	}	//	checkProperty

	/**
	 *	Return File Name of INI file
	 *  <pre>
	 *  Examples:
	 *	    C:\WinNT\Profiles\jjanke\Adempiere.properties
	 *      D:\Adempiere\Adempiere.properties
	 *      Adempiere.properties
	 *  </pre>
	 *  Can be overwritten by -DPropertyFile=myFile allowing multiple
	 *  configurations / property files.
	 *  @param tryUserHome get user home first
	 *  @return file name
	 */
	private static String getFileName (boolean tryUserHome)
	{
		if (System.getProperty("PropertyFile") != null)
			return System.getProperty("PropertyFile");
		//
		String base = null;
		if (tryUserHome && s_client)
			base = System.getProperty("user.home");
		//  Server
		if (!s_client || base == null || base.length() == 0)
		{
			String home = getAdempiereHome();
			if (home != null)
				base = home;
		}
		if (base != null && !base.endsWith(File.separator))
			base += File.separator;
		if (base == null)
			base = "";
		//
		return base + ADEMPIERE_PROPERTY_FILE;
	}	//	getFileName

	
	/**************************************************************************
	 *	Set Property
	 *  @param key   Key
	 *  @param value Value
	 */
	public static void setProperty (String key, String value)
	{
	//	log.finer(key + "=" + value);
		if (s_prop == null)
			s_prop = new Properties();
		if (!isClient())
			s_prop.setProperty(key, SecureInterface.CLEARVALUE_START + value + SecureInterface.CLEARVALUE_END);
		else
		{
			if (value == null)
				s_prop.setProperty(key, "");
			else
			{
				String eValue = SecureEngine.encrypt(value);
				if (eValue == null)
					s_prop.setProperty(key, "");
				else
					s_prop.setProperty(key, eValue);
			}
		}
	}	//	setProperty

	/**
	 *	Set Property
	 *  @param key   Key
	 *  @param value Value
	 */
	public static void setProperty (String key, boolean value)
	{
		setProperty (key, value ? "Y" : "N");
	}   //  setProperty

	/**
	 *	Set Property
	 *  @param key   Key
	 *  @param value Value
	 */
	public static void setProperty (String key, int value)
	{
		setProperty (key, String.valueOf(value));
	}   //  setProperty

	/**
	 *	Get Property
	 *  @param key  Key
	 *  @return     Value
	 */
	public static String getProperty (String key)
	{
		if (key == null)
			return "";
		String retStr = s_prop.getProperty(key, "");
		if (retStr == null || retStr.length() == 0)
			return "";
		//
		String value = SecureEngine.decrypt(retStr); 
	//	log.finer(key + "=" + value);
		if (value == null)
			return "";
		return value;
	}	//	getProperty

	/**
	 *	Get Property as Boolean
	 *  @param key  Key
	 *  @return     Value
	 */
	public static boolean isPropertyBool (String key)
	{
		return getProperty (key).equals("Y");
	}	//	getProperty

	/**
	 * 	Cache Windows
	 *	@return true if windows are cached
	 */
	public static boolean isCacheWindow()
	{
		return getProperty (P_CACHE_WINDOW).equals("Y");
	}	//	isCacheWindow
	
	/**************************************************************************
	 *  Get Properties
	 *
	 * @return Ini properties
	 */
	public static Properties getProperties()
	{
		return s_prop;
	}   //  getProperties

	/**
	 *  toString
	 *  @return String representation
	 */
	public static String getAsString()
	{
		StringBuffer buf = new StringBuffer ("Ini[");
		Enumeration e = s_prop.keys();
		while (e.hasMoreElements())
		{
			String key = (String)e.nextElement();
			buf.append(key).append("=");
			buf.append(getProperty(key)).append("; ");
		}
		buf.append("]");
		return buf.toString();
	}   //  toString

	
	/*************************************************************************/

	/** System environment prefix                                       */
	public static final String  ENV_PREFIX = "env.";
	/** System Property Value of ADEMPIERE_HOME                          */
	public static final String  ADEMPIERE_HOME = "ADEMPIERE_HOME";

	/** IsClient Internal marker            */
	private static boolean      s_client = false;
	/** IsClient Internal marker            */
	private static boolean      s_loaded = false;
	/** Show license dialog for first time **/
	private static boolean		s_license_dialog = true;

	/**
	 *  Are we in Client Mode ?
	 *  @return true if client
	 */
	public static boolean isClient()
	{
		return s_client;
	}   //  isClient

	/**
	 *  Set Client Mode
	 *  @param client client
	 */
	public static void setClient (boolean client)
	{
		if (log != null) //already initialized
			return;
		s_client = client;
		CLogMgt.initialize(client);
		log = Logger.getLogger(Ini.class.getName());
	}   //  setClient
	
	/**
	 * Set show license dialog for new setup
	 * @param b
	 */
	public static void setShowLicenseDialog(boolean b)
	{
		s_license_dialog = b;
	}
	
	/**
	 * Is show license dialog for new setup
	 * @return boolean
	 */
	public static boolean isShowLicenseDialog()
	{
		return s_license_dialog;
	}	
	
	/**
	 *  Are the properties loaded?
	 *  @return true if properties loaded.
	 */
	public static boolean isLoaded()
	{
		return s_loaded;
	}   //  isLoaded

	/**
	 *  Get Adempiere Home from Environment
	 *  @return AdempiereHome or null
	 */
	public static String getAdempiereHome()
	{
		String env = System.getProperty (ENV_PREFIX + ADEMPIERE_HOME);
		if (env == null)
			env = System.getProperty (ADEMPIERE_HOME);
		if (env == null)
			env = System.getenv(ADEMPIERE_HOME);
		if (env == null && ! isClient()) {
			InitialContext context;
			try {
				context = new InitialContext();
				env = (String) context.lookup("java:comp/env/"+ADEMPIERE_HOME);
			} catch (NamingException e) {
				// teo_sarca: if you uncomment the line below you will get an NPE when generating models
				//log.fine( "Not found 'java:comp/env/"+ADEMPIERE_HOME+"' in Initial Context. " +e.getMessage());
			}
			
		}
		if (env == null || "".equals(env) )	//	Fallback
			env = File.separator + "Adempiere";
		return env;
	}   //  getAdempiereHome

	/**
	 *  Set Adempiere Home
	 *  @param AdempiereHome ADEMPIERE_HOME
	 */
	public static void setAdempiereHome (String AdempiereHome)
	{
		if (AdempiereHome != null && AdempiereHome.length() > 0)
			System.setProperty (ADEMPIERE_HOME, AdempiereHome);
	}   //  setAdempiereHome

	/**
	 * 	Find Adempiere Home
	 *	@return adempiere home or null
	 */
	public static String findAdempiereHome()
	{
		String ch = getAdempiereHome();
		if (ch != null)
			return ch;
		
		File[] roots = File.listRoots();
		for (int i = 0; i < roots.length; i++)
		{
			if (roots[i].getAbsolutePath().startsWith("A:"))
				continue;
			File[] subs = roots[i].listFiles();
			if (subs == null)
				continue;
			for (int j = 0; j < subs.length; j++) 
			{
				if (!subs[j].isDirectory())
					continue;
				String fileName = subs[j].getAbsolutePath();
				// globalqss, it's leaving log in first directory with lib subdirectory. i.e. Oracle
				// if (fileName.indexOf("Adempiere") != 1)
				if (fileName.indexOf("Adempiere") != -1)
				{
					String libDir = fileName + File.separator + "lib";
					File lib = new File(libDir);
					if (lib.exists() && lib.isDirectory())
						return fileName;
				}
			}
		}
		return ch;
	}	//	findAdempiereHome

	
	//jobriant - Headline - to identify environment (e.g. Production, Test, Dev)
	
	public static boolean isHeadlineSet() {
		
		String headLine = getProperty(P_HEADLINE_TEXT);  
		if (headLine == null || headLine.length() == 0) {
			return false;
		}
		return true;
	}
	
	public static String getHeadLineText() {
		String headLine = getProperty(P_HEADLINE_TEXT);  
		if (headLine == null || headLine.length() == 0) {
			throw new AdempiereException("Please run isHeadlineSet() check before using HeadLine feature");
		}
		return headLine;
	}
	
	public static String getFGColor() {
		return getProperty(P_HEADLINE_FGCOLOR);
	}
	
	public static String getBGColor() {
		return getProperty(P_HEADLINE_BGCOLOR);
	}
		
	/**************************************************************************
	 * 	Get Window Dimension
	 *	@param AD_Window_ID window no
	 *	@return dimension or null
	 */
	public static Dimension getWindowDimension(int AD_Window_ID)
	{
		String key = "WindowDim" + AD_Window_ID;
		String value = (String)s_prop.get(key);
		if (value == null || value.length() == 0)
			return null;
		int index = value.indexOf('|');
		if (index == -1)
			return null;
		try
		{
			String w = value.substring(0, index);
			String h = value.substring(index+1);
			return new Dimension(Integer.parseInt(w),Integer.parseInt(h));
		}
		catch (Exception e)
		{
		}
		return null;
	}	//	getWindowDimension
	
	/**
	 * 	Set Window Dimension 
	 *	@param AD_Window_ID window
	 *	@param windowDimension dimension - null to remove
	 */
	public static void setWindowDimension(int AD_Window_ID, Dimension windowDimension)
	{
		String key = "WindowDim" + AD_Window_ID;
		if (windowDimension != null)
		{
			String value = windowDimension.width + "|" + windowDimension.height;
			s_prop.put(key, value);
		}
		else
			s_prop.remove(key);
	}	//	setWindowDimension
	
	/**
	 * 	Get Window Location
	 *	@param AD_Window_ID window id
	 *	@return location or null
	 */
	public static Point getWindowLocation(int AD_Window_ID)
	{
		String key = "WindowLoc" + AD_Window_ID;
		String value = (String)s_prop.get(key);
		if (value == null || value.length() == 0)
			return null;
		int index = value.indexOf('|');
		if (index == -1)
			return null;
		try
		{
			String x = value.substring(0, index);
			String y = value.substring(index+1);
			return new Point(Integer.parseInt(x),Integer.parseInt(y));
		}
		catch (Exception e)
		{
		}
		return null;
	}	//	getWindowLocation
	
	/**
	 * 	Set Window Location
	 *	@param AD_Window_ID window
	 *	@param windowLocation location - null to remove
	 */
	public static void setWindowLocation(int AD_Window_ID, Point windowLocation)
	{
		String key = "WindowLoc" + AD_Window_ID;
		if (windowLocation != null)
		{
			String value = windowLocation.x + "|" + windowLocation.y;
			s_prop.put(key, value);
		}
		else
			s_prop.remove(key);
	}	//	setWindowLocation
	
	/**
	 * 	Get Divider Location
	 *	@return location
	 */
	public static int getDividerLocation()
	{
		String key = "Divider";
		String value = (String)s_prop.get(key);
		if (value == null || value.length() == 0)
			return 0;
		try
		{
			return Integer.parseInt(value);
		}
		catch (Exception e)
		{
		}
		return 0;
	}	//	getDividerLocation
	
	/**
	 * 	Set Divider Location
	 *	@param dividerLocation location
	 */
	public static void setDividerLocation(int dividerLocation)
	{
		String key = "Divider";
		String value = String.valueOf(dividerLocation);
		s_prop.put(key, value);
	}	//	setDividerLocation

	/**
	 * Get Available Encoding Charsets
	 * @return array of available encoding charsets
	 * @since 3.1.4
	 */
	public static Charset[] getAvailableCharsets() {
		Collection<Charset> col = Charset.availableCharsets().values();
		Charset[] arr = new Charset[col.size()];
		col.toArray(arr);
		return arr;
	}
	
	/**
	 * Get current charset
	 * @return current charset
	 * @since 3.1.4
	 */
	public static Charset getCharset() {
		return Charset.defaultCharset();
	}
	
	public static String getPropertyFileName() 
	{
		return s_propertyFileName;
	}
}	//	Ini
