/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.util;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.logging.Level;

import org.adempiere.exceptions.DBException;

/**
 * Wrapper for PreparedStatement that closes connection automatically on close
 * @author Low Heng Sin
 *
 */
public class CPreparedStatement extends CStatement implements PreparedStatement
{
	public CPreparedStatement(int resultSetType, int resultSetConcurrency,
			String sql0, String trxName) {
		if (sql0 == null || sql0.length() == 0)
			throw new IllegalArgumentException("sql required");

		p_vo = new CStatementVO(resultSetType, resultSetConcurrency,sql0);

		p_vo.setTrxName(trxName);

		init();
	} // PreparedStatementProxy
	
	public CPreparedStatement(CStatementVO vo)
	{
		super(vo);
	}	//	PreparedStatementProxy

	/**
	 * Initialise the prepared statement wrapper object
	 */
	protected void init() {
		try {
			Connection conn = null;
			Trx trx = p_vo.getTrxName() == null ? null : Trx.get(p_vo
					.getTrxName(), false);
			if (trx != null) {
				conn = trx.getConnection();
			} else {
				if (p_vo.getResultSetConcurrency() == ResultSet.CONCUR_UPDATABLE)
					m_conn = DB.getConnectionRW();
				else
					m_conn = DB.getConnectionRO();
				conn = m_conn;
			}
			if (conn == null)
				throw new DBException("No Connection");
			p_stmt = conn.prepareStatement(p_vo.getSql(), p_vo
					.getResultSetType(), p_vo.getResultSetConcurrency());
		} catch (Exception e) {
			log.log(Level.SEVERE, p_vo.getSql(), e);
			throw new DBException(e);
		}
	}

	/**
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#addBatch()
	 */
	public void addBatch() throws SQLException {
		((PreparedStatement) p_stmt).addBatch();
	}

	/**
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#clearParameters()
	 */
	public void clearParameters() throws SQLException {
		((PreparedStatement) p_stmt).clearParameters();
	}

	/**
	 * @return
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#execute()
	 */
	public boolean execute() throws SQLException {
		return ((PreparedStatement) p_stmt).execute();
	}

	/**
	 * @return
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#executeUpdate()
	 */
	public int executeUpdate() throws SQLException {
		return ((PreparedStatement) p_stmt).executeUpdate();
	}
	
	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setDate(int, java.sql.Date, java.util.Calendar)
	 */
	public void setDate(int arg0, Date arg1, Calendar arg2) throws SQLException {
		((PreparedStatement) p_stmt).setDate(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setDate(int, java.sql.Date)
	 */
	public void setDate(int arg0, Date arg1) throws SQLException {
		((PreparedStatement) p_stmt).setDate(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setDouble(int, double)
	 */
	public void setDouble(int arg0, double arg1) throws SQLException {
		((PreparedStatement) p_stmt).setDouble(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setFloat(int, float)
	 */
	public void setFloat(int arg0, float arg1) throws SQLException {
		((PreparedStatement) p_stmt).setFloat(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setInt(int, int)
	 */
	public void setInt(int arg0, int arg1) throws SQLException {
		((PreparedStatement) p_stmt).setInt(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setLong(int, long)
	 */
	public void setLong(int arg0, long arg1) throws SQLException {
		((PreparedStatement) p_stmt).setLong(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNCharacterStream(int, java.io.Reader, long)
	 */
	public void setNCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		((PreparedStatement) p_stmt).setNCharacterStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNCharacterStream(int, java.io.Reader)
	 */
	public void setNCharacterStream(int arg0, Reader arg1) throws SQLException {
		((PreparedStatement) p_stmt).setNCharacterStream(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNClob(int, java.sql.NClob)
	 */
	public void setNClob(int arg0, NClob arg1) throws SQLException {
		((PreparedStatement) p_stmt).setNClob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNClob(int, java.io.Reader, long)
	 */
	public void setNClob(int arg0, Reader arg1, long arg2) throws SQLException {
		((PreparedStatement) p_stmt).setNClob(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNClob(int, java.io.Reader)
	 */
	public void setNClob(int arg0, Reader arg1) throws SQLException {
		((PreparedStatement) p_stmt).setNClob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNString(int, java.lang.String)
	 */
	public void setNString(int arg0, String arg1) throws SQLException {
		((PreparedStatement) p_stmt).setNString(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNull(int, int, java.lang.String)
	 */
	public void setNull(int arg0, int arg1, String arg2) throws SQLException {
		((PreparedStatement) p_stmt).setNull(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNull(int, int)
	 */
	public void setNull(int arg0, int arg1) throws SQLException {
		((PreparedStatement) p_stmt).setNull(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setObject(int, java.lang.Object, int, int)
	 */
	public void setObject(int arg0, Object arg1, int arg2, int arg3) throws SQLException {
		((PreparedStatement) p_stmt).setObject(arg0, arg1, arg2, arg3);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setObject(int, java.lang.Object, int)
	 */
	public void setObject(int arg0, Object arg1, int arg2) throws SQLException {
		((PreparedStatement) p_stmt).setObject(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setObject(int, java.lang.Object)
	 */
	public void setObject(int arg0, Object arg1) throws SQLException {
		((PreparedStatement) p_stmt).setObject(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setRef(int, java.sql.Ref)
	 */
	public void setRef(int arg0, Ref arg1) throws SQLException {
		((PreparedStatement) p_stmt).setRef(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setRowId(int, java.sql.RowId)
	 */
	public void setRowId(int arg0, RowId arg1) throws SQLException {
		((PreparedStatement) p_stmt).setRowId(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setSQLXML(int, java.sql.SQLXML)
	 */
	public void setSQLXML(int arg0, SQLXML arg1) throws SQLException {
		((PreparedStatement) p_stmt).setSQLXML(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setShort(int, short)
	 */
	public void setShort(int arg0, short arg1) throws SQLException {
		((PreparedStatement) p_stmt).setShort(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setString(int, java.lang.String)
	 */
	public void setString(int arg0, String arg1) throws SQLException {
		((PreparedStatement) p_stmt).setString(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setTime(int, java.sql.Time, java.util.Calendar)
	 */
	public void setTime(int arg0, Time arg1, Calendar arg2) throws SQLException {
		((PreparedStatement) p_stmt).setTime(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setTime(int, java.sql.Time)
	 */
	public void setTime(int arg0, Time arg1) throws SQLException {
		((PreparedStatement) p_stmt).setTime(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setTimestamp(int, java.sql.Timestamp, java.util.Calendar)
	 */
	public void setTimestamp(int arg0, Timestamp arg1, Calendar arg2) throws SQLException {
		((PreparedStatement) p_stmt).setTimestamp(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setTimestamp(int, java.sql.Timestamp)
	 */
	public void setTimestamp(int arg0, Timestamp arg1) throws SQLException {
		((PreparedStatement) p_stmt).setTimestamp(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setURL(int, java.net.URL)
	 */
	public void setURL(int arg0, URL arg1) throws SQLException {
		((PreparedStatement) p_stmt).setURL(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @deprecated
	 * @see java.sql.PreparedStatement#setUnicodeStream(int, java.io.InputStream, int)
	 */
	public void setUnicodeStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		((PreparedStatement) p_stmt).setUnicodeStream(arg0, arg1, arg2);
	}

	/**
	 * @return
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#executeQuery()
	 */
	public ResultSet executeQuery() throws SQLException {
		return ((PreparedStatement) p_stmt).executeQuery();
	}

	/**
	 * @param arg0
	 * @return
	 * @throws SQLException
	 * @see java.sql.Statement#executeUpdate(java.lang.String)
	 */
	public int executeUpdate(String arg0) throws SQLException {
		return p_stmt.executeUpdate(arg0);
	}

	/**
	 * @return
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#getMetaData()
	 */
	public ResultSetMetaData getMetaData() throws SQLException {
		return ((PreparedStatement) p_stmt).getMetaData();
	}

	/**
	 * @return
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#getParameterMetaData()
	 */
	public ParameterMetaData getParameterMetaData() throws SQLException {
		return ((PreparedStatement) p_stmt).getParameterMetaData();
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setArray(int, java.sql.Array)
	 */
	public void setArray(int arg0, Array arg1) throws SQLException {
		((PreparedStatement) p_stmt).setArray(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setAsciiStream(int, java.io.InputStream, int)
	 */
	public void setAsciiStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		((PreparedStatement) p_stmt).setAsciiStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setAsciiStream(int, java.io.InputStream, long)
	 */
	public void setAsciiStream(int arg0, InputStream arg1, long arg2) throws SQLException {
		((PreparedStatement) p_stmt).setAsciiStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setAsciiStream(int, java.io.InputStream)
	 */
	public void setAsciiStream(int arg0, InputStream arg1) throws SQLException {
		((PreparedStatement) p_stmt).setAsciiStream(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBigDecimal(int, java.math.BigDecimal)
	 */
	public void setBigDecimal(int arg0, BigDecimal arg1) throws SQLException {
		((PreparedStatement) p_stmt).setBigDecimal(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBinaryStream(int, java.io.InputStream, int)
	 */
	public void setBinaryStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		((PreparedStatement) p_stmt).setBinaryStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBinaryStream(int, java.io.InputStream, long)
	 */
	public void setBinaryStream(int arg0, InputStream arg1, long arg2) throws SQLException {
		((PreparedStatement) p_stmt).setBinaryStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBinaryStream(int, java.io.InputStream)
	 */
	public void setBinaryStream(int arg0, InputStream arg1) throws SQLException {
		((PreparedStatement) p_stmt).setBinaryStream(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBlob(int, java.sql.Blob)
	 */
	public void setBlob(int arg0, Blob arg1) throws SQLException {
		((PreparedStatement) p_stmt).setBlob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBlob(int, java.io.InputStream, long)
	 */
	public void setBlob(int arg0, InputStream arg1, long arg2) throws SQLException {
		((PreparedStatement) p_stmt).setBlob(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBlob(int, java.io.InputStream)
	 */
	public void setBlob(int arg0, InputStream arg1) throws SQLException {
		((PreparedStatement) p_stmt).setBlob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBoolean(int, boolean)
	 */
	public void setBoolean(int arg0, boolean arg1) throws SQLException {
		((PreparedStatement) p_stmt).setBoolean(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setByte(int, byte)
	 */
	public void setByte(int arg0, byte arg1) throws SQLException {
		((PreparedStatement) p_stmt).setByte(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBytes(int, byte[])
	 */
	public void setBytes(int arg0, byte[] arg1) throws SQLException {
		((PreparedStatement) p_stmt).setBytes(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setCharacterStream(int, java.io.Reader, int)
	 */
	public void setCharacterStream(int arg0, Reader arg1, int arg2) throws SQLException {
		((PreparedStatement) p_stmt).setCharacterStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setCharacterStream(int, java.io.Reader, long)
	 */
	public void setCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		((PreparedStatement) p_stmt).setCharacterStream(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setCharacterStream(int, java.io.Reader)
	 */
	public void setCharacterStream(int arg0, Reader arg1) throws SQLException {
		((PreparedStatement) p_stmt).setCharacterStream(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setClob(int, java.sql.Clob)
	 */
	public void setClob(int arg0, Clob arg1) throws SQLException {
		((PreparedStatement) p_stmt).setClob(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setClob(int, java.io.Reader, long)
	 */
	public void setClob(int arg0, Reader arg1, long arg2) throws SQLException {
		((PreparedStatement) p_stmt).setClob(arg0, arg1, arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setClob(int, java.io.Reader)
	 */
	public void setClob(int arg0, Reader arg1) throws SQLException {
		((PreparedStatement) p_stmt).setClob(arg0, arg1);
	}


}	//	CPreparedStatement

