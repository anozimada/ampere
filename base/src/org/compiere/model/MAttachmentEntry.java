/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.CLogger;
import org.compiere.util.Env;


/**
 *	Individual Attachment Entry of MAttachment
 *	
 *  @author Jorg Janke
 *  @version $Id: MAttachmentEntry.java,v 1.2 2006/07/30 00:58:18 jjanke Exp $
 */
public class MAttachmentEntry extends X_AD_AttachmentEntry
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8523974575170241795L;


	/**
	 * 	Attachment Entry from parent
	 *  @param parent MAttachment
	 * 	@param name name
	 * 	@param data binary data
	 */
	public MAttachmentEntry (MAttachment parent, String name, byte[] data)
	{
		super (Env.getCtx(), 0, parent.get_TrxName());
		if (parent.is_new())
			parent.saveEx();
		
		setParent(parent);
		setAD_Attachment_ID(parent.getAD_Attachment_ID());
		setFileName(name);
		setFileSize(data.length);
		setData(data);
	}	//	MAttachmentItem
	
	
	public MAttachmentEntry(Properties ctx, int AD_AttachmentEntry_ID, String trxName) {
		super(ctx, AD_AttachmentEntry_ID, trxName);

		if ( getAD_Attachment_ID() > 0 ) {
			MAttachment parent = (MAttachment) getAD_Attachment();
			setParent(parent);
		}
		
	}
	public MAttachmentEntry(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
		if ( getAD_Attachment_ID() > 0 ) {
			MAttachment parent = (MAttachment) getAD_Attachment();
			setParent(parent);
		}
	}

	/** The Data				*/
	private byte[] 	m_data = null; 
	
	/** The Parent attachment    */
	private MAttachment parent = null;

	/**	Logger			*/
	protected CLogger	log = CLogger.getCLogger(getClass());
	
	/**
	 * @return Returns the data.
	 */
	public byte[] getData ()
	{
		if ( m_data == null )
			loadData();
		
		return m_data;
	}
	/**
	 * @param data The data to set.
	 */
	public void setData (byte[] data)
	{
		m_data = data;
	}
	
	public MAttachment getParent() {
		return parent;
	}
	public void setParent(MAttachment parent) {
		this.parent = parent;
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		boolean success = true;
		
		if ( newRecord )
			success = saveFile();
		
		return success;
	}
	
	@Override
	protected boolean beforeDelete() {
		return deleteFile();
	}
	/**
	 * 	To String
	 *	@return name
	 */
	public String toString ()
	{
		return getFileName();
	}	//	toString

	/**
	 * 	To String Extended
	 *	@return name (length)
	 */
	public String toStringX ()
	{
		if ( getFileName() == null )
			return null;
		
		StringBuffer sb = new StringBuffer (getFileName());
		if (m_data != null)
		{
			sb.append(" (");
			//
			float size = m_data.length;
			if (size <= 1024)
				sb.append(m_data.length).append(" B");
			else
			{
				size /= 1024;
				if (size > 1024)
				{
					size /= 1024;
					sb.append(size).append(" MB");
				}
				else
					sb.append(size).append(" kB");
			}
			//
			sb.append(")");
		}
		sb.append(" - ").append(getContentType());
		return sb.toString();
	}	//	toStringX

	
	/**
	 * 	Dump Data
	 */
	public void dump ()
	{
		String hdr = "----- " + getFileName() + " -----";
		System.out.println (hdr);
		if (m_data == null)
		{
			System.out.println ("----- no data -----");
			return;
		}
		//	raw data
		for (int i = 0; i < m_data.length; i++)
		{
			char data = (char)m_data[i];
			System.out.print(data);
		}
			
		System.out.println ();
		System.out.println (hdr);
		//	Count nulls at end
		int ii = m_data.length -1;
		int nullCount = 0;
		while (m_data[ii--] == 0)
			nullCount++;
		System.out.println("----- Length=" + m_data.length + ", EndNulls=" + nullCount 
			+ ", RealLength=" + (m_data.length-nullCount));
		/**
		//	Dump w/o nulls
		if (nullCount > 0)
		{
			for (int i = 0; i < m_data.length-nullCount; i++)
				System.out.print((char)m_data[i]);
			System.out.println ();
			System.out.println (hdr);
		}
		/** **/
	}	//	dump

	/**
	 * 	Get File with default name
	 *	@return File
	 */
	public File getFile ()
	{
		return getFile ("");
	}	//	getFile

	/**
	 * 	Get File with name
	 *	@param fileName optional file name
	 *	@return file
	 */	
	public File getFile (String fileName)
	{
		if (fileName == null || fileName.length() == 0)
			fileName = parent.getAttachmentPath()+ File.separator + getFileName();
		return new File(fileName);
	}	//	getFile

	public String getFilePath()
	{
		return parent.getAttachmentPath() + File.separator + getFileName();
	}
	
	/**
	 * 	Get Content (Mime) Type
	 *	@return content type
	 */
	public String getContentType()
	{
		String mime = "application/octet-stream";
		try {
			mime = Files.probeContentType(Path.of(getFileName()));
		} catch (IOException e) {
		}
		return mime;
	}	//	getContentType
	
	/**
	 * 	Get Data as Input Stream
	 *	@return input stream
	 */
	public InputStream getInputStream()
	{
		if (m_data == null)
			return null;
		return new ByteArrayInputStream(m_data);
	}	//	getInputStream
	
	public void loadData()
	{
		final File file = getFile();
		if (file.exists()) {
			// read files into byte[]
			final byte[] dataEntry = new byte[(int) file.length()];
			try {
				final FileInputStream fileInputStream = new FileInputStream(file);
				fileInputStream.read(dataEntry);
				fileInputStream.close();
			} catch (FileNotFoundException e) {
				log.severe("File Not Found.");
				e.printStackTrace();
			} catch (IOException e1) {
				log.severe("Error Reading The File.");
				e1.printStackTrace();
			}

			setData (dataEntry);
			
		} else {
			log.severe("file not found: " + file.getAbsolutePath());
		}
	}
	
	public boolean saveFile() {
		
		if ( m_data == null )
			return false;

		log.fine(toString());
		final String path = getFilePath();

		final File destFile = new File(path);

		if ( !destFile.exists() )
		{
			destFile.getParentFile().mkdirs();
			try {
				destFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				log.severe("unable to create file: " + path);
				return false;
			}
		}
		try (FileOutputStream out = new FileOutputStream(destFile))
		{
			out.write(m_data);
		} catch (IOException e) {
			e.printStackTrace();
			log.severe("unable to write file: " + path);
			return false;
		} 

		return true;
	}

	public boolean deleteFile() {
		final File file = getFile();
		if(file !=null && file.exists()){
			if(!file.delete()){
				log.warning("unable to delete " + file.getAbsolutePath());
				return false;
			}
		}
		return true;
	}
	
}	//	MAttachmentItem
