package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.Env;

/**
 * Asset Group Accounting Model
 */
public class MGAASAssetGroupAcct extends X_GAAS_Asset_Group_Acct
{
	private static final long serialVersionUID = 1L;
	public static final String COLUMNNAME_A_Asset_Group_ID = "A_Asset_Group_ID";
	public static final String COLUMNNAME_PostingType = "PostingType";

	/**
	 *	@param ctx context
	 *	@param X_GAAS_Asset_Group_Acct_ID id
	 */
	public MGAASAssetGroupAcct (Properties ctx, int X_GAAS_Asset_Group_Acct_ID, String trxName)
	{
		super (ctx,X_GAAS_Asset_Group_Acct_ID, trxName);
	}	//	MGAASAssetGroupAcct
	
	/**
	 * 	Load Constructor
	 *	@param ctx context
	 *	@param rs result set
	 */
	public MGAASAssetGroupAcct (Properties ctx, ResultSet rs, String trxName)
	{
		super (ctx, rs,  trxName);
	}	//	MGAASAssetGroupAcct
	
	/**	Asset Group	*/
	private MAssetGroup m_parent = null;
	
	/**
	 * Get Asset Group
	 */
	public MAssetGroup getParent()
	{
		if (m_parent == null)
		{
			int A_Asset_Group_ID = getA_Asset_Group_ID();
			if (is_new())
			{
				m_parent = new MAssetGroup(getCtx(), A_Asset_Group_ID, get_TrxName());
			}
			else
			{
				m_parent = MAssetGroup.get(getCtx(), A_Asset_Group_ID);
			}
		}
		return m_parent;
	}
	
	public int getGAAS_Asset_Class_ID()
	{
		return getParent().get_ValueAsInt("GAAS_Asset_Class_ID");
	}
	
	/**
	 * Clone this object, using specified group
	 * @param grp	the new asset group
	 * @return new asset group accounting (NOTE: it's not saved)
	 */
	public MGAASAssetGroupAcct copy(MAssetGroup grp)
	{
		MGAASAssetGroupAcct newAcct = new MGAASAssetGroupAcct(grp.getCtx(), 0, grp.get_TrxName());
		copyValues(this, newAcct, grp.getAD_Client_ID(), grp.getAD_Org_ID());
		newAcct.setA_Asset_Group_ID(grp.getA_Asset_Group_ID());
		return newAcct;
	}

	public boolean beforeSave(boolean newRecord)
	{
	    if (getDepreciationRate() == null)
        {
            setDepreciationRate(Env.ZERO);
        }
	    
	    if (getDepreciationRate().compareTo(Env.ZERO) < 0
                || getDepreciationRate().compareTo(new BigDecimal("1")) >= 0)
        {
            log.saveError("Error", "Invalid Depreciation Rate");
            return false;
        }
		return true;
	}
}	//	MGAASAssetGroupAcct
