/**
 * 
 */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;

/**
 * @author Ashley G Ramdass
 * 
 */
public class MGAASDepreciationWorkfile extends X_GAAS_Depreciation_Workfile
{
    private static final long serialVersionUID = -4947555095739716149L;

    public MGAASDepreciationWorkfile(Properties ctx,
            int GAAS_Depreciation_Workfile_ID, String trxName)
    {
        super(ctx, GAAS_Depreciation_Workfile_ID, trxName);
    }

    public MGAASDepreciationWorkfile(Properties ctx, ResultSet rs, String trxName)
    {
        super(ctx, rs, trxName);
    }

    public static MGAASDepreciationWorkfile get(MAsset asset)
    {
        return get(asset.getCtx(), asset.getA_Asset_ID(), asset.get_TrxName());
    }

    /**
     * Returns the depreciation workfile associated to an asset
     * 
     * @param assetId
     *            Asset id
     * @return Depreciation workfile record or null
     */
    public static MGAASDepreciationWorkfile get(Properties ctx, int assetId,
            String trxName)
    {
        String sql = "SELECT GAAS_Depreciation_Workfile_ID FROM "
                + "GAAS_Depreciation_Workfile WHERE A_Asset_ID=? AND IsActive='Y'";

        int workfileId = DB.getSQLValue(trxName, sql, assetId);

        if (workfileId <= 0)
        {
            return null;
        }

        return new MGAASDepreciationWorkfile(ctx, workfileId, trxName);
    }
    
    @Override
    protected boolean beforeSave(boolean newRecord)
    {
        String sql = "SELECT COUNT(*) FROM GAAS_Depreciation_Workfile"
                + " WHERE IsActive='Y' AND AD_Client_ID=" + getAD_Client_ID()
                + " AND A_Asset_ID=? AND GAAS_Depreciation_Workfile_ID<>?";

        int count = DB.getSQLValue(get_TrxName(), sql, getA_Asset_ID(), getGAAS_Depreciation_Workfile_ID());
        
        if (count > 0)
        {
            log.saveError("Error", "Another active record already present");
            return false;
        }
        
        return super.beforeSave(newRecord);
    }
}
