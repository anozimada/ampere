package org.compiere.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class MManifest extends X_M_InOut_Manifest {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6518051991602120741L;
	private List<MManifestLine> m_lines = null;
	private int lastSeqNo = 0;
	private int lastLineNo = 0;



	public MManifest(Properties ctx, int M_InOut_Manifest_ID, String trxName) {
		super(ctx, M_InOut_Manifest_ID, trxName);
		
		if (M_InOut_Manifest_ID == 0) {
			super.setProcessed (false);
			setProcessing(false);
			setIsComplete("N");
		}

	}
	
	public List<MManifestLine> getLines (boolean requery)
	{
		if (m_lines  != null && !requery) {
			for (MManifestLine line : m_lines) {
				line.set_TrxName(get_TrxName());
				if (lastSeqNo < line.getSeqNo()) {
					lastSeqNo = line.getSeqNo();
				}
				if (lastLineNo < line.getLine()) {
					lastLineNo = line.getLine();
				}
			}
			return m_lines;
		}
		List<MManifestLine> list = new Query(getCtx(), I_M_InOut_ManifestLine.Table_Name, "M_InOut_Manifest_ID=?", get_TrxName())
		.setParameters(getM_InOut_Manifest_ID())
		.setOrderBy(MInOutLine.COLUMNNAME_Line)
		.list();
		//
		m_lines = list;
		for (MManifestLine line : m_lines) {
			if (lastSeqNo < line.getSeqNo()) {
				lastSeqNo = line.getSeqNo();
			}
			if (lastLineNo < line.getLine()) {
				lastLineNo = line.getLine();
			}
		}
		return m_lines;
	}	//	getMInOutLines

	/**
	 * 	Get Lines of Manifest
	 * 	@return lines
	 */
	public List<MManifestLine> getLines()
	{
		return getLines(false);
	}	

	public MManifestLine add(int M_InOut_ID)
	{
		MManifestLine line = new MManifestLine(getCtx(), 0, get_TrxName());
	
		line.setM_InOut_ID(M_InOut_ID);
		line.setM_InOut_Manifest_ID(getM_InOut_Manifest_ID());
		if (m_lines == null) {
			m_lines= new ArrayList<MManifestLine>();
			lastSeqNo+= 10;
			lastLineNo+= 10;
		}
		line.setLine(lastLineNo);
		line.setSeqNo(lastSeqNo);
		line.save();
		m_lines.add(line);
		return line;
	}
	
	public int getLastSeqNo() {
		return lastSeqNo;
	}

	public int getLastLineNo() {
		return lastLineNo;
	}
}
