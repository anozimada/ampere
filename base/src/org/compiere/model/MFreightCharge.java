package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MFreightCharge extends X_M_FreightCharge {

	/**
	 * 
	 */
	private static final long serialVersionUID = -978289524409623040L;

	public MFreightCharge(Properties ctx, int M_FreightCharge_ID, String trxName) {
		super(ctx, M_FreightCharge_ID, trxName);
	}

	public MFreightCharge(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
}
