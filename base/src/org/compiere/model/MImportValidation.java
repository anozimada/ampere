/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/

package org.compiere.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.DB;

public class MImportValidation extends X_AD_ImportValidation {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9052544341602655427L;

	public MImportValidation(Properties ctx, int AD_ImportValidation_ID, String trxName) {
		super(ctx, AD_ImportValidation_ID, trxName);
	}

	public MImportValidation(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		return true;
	}

	public void validate() {
		
		String sql = getSQLStatement();
		
		if ( sql == null || sql.trim().length() == 0 || sql.equals(";"))
		{
			log.log(Level.WARNING, "Validation step " + getSeqNo() + " (" + getName() + ") skipped: No SQL");
			return;
		}
		
		sql = sql.trim();
		if (sql.endsWith(";"))
			sql = sql.substring(0, sql.length() - 1);		

		Connection conn = DB.getConnectionRW();
		Statement stmt = null;
		try {

			conn.setAutoCommit(false);
			stmt = conn.createStatement();
			int count = stmt.executeUpdate(sql);
			conn.commit();
			conn.close();
			log.log(Level.CONFIG, "Validation step " + getSeqNo() + " (" + getName() + ") updated: " + count);
		} 
		catch (SQLException e) {
			log.log(Level.SEVERE, "SQL statement failed: " + sql, e);
			try {
				conn.rollback();
				conn.close();
			} catch (SQLException se) {
				;  // all out of luck
			}
		} 
		finally {
			DB.close(stmt);
			saveEx(null);
		}
		
	}

}
