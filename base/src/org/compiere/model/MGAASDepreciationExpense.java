/**
 * 
 */
package org.compiere.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.CLogger;
import org.compiere.util.DB;

/**
 * @author Ashley G Ramdass
 * 
 */
public class MGAASDepreciationExpense extends X_GAAS_Depreciation_Expense
{
    private static final long serialVersionUID = -3330900083340612682L;
    private static final CLogger logger = CLogger
            .getCLogger(MGAASDepreciationExpense.class);

    public MGAASDepreciationExpense(Properties ctx, int GAAS_Depreciation_Expense_ID,
            String trxName)
    {
        super(ctx, GAAS_Depreciation_Expense_ID, trxName);
    }

    public MGAASDepreciationExpense(Properties ctx, ResultSet rs, String trxName)
    {
        super(ctx, rs, trxName);
    }

    public static List<MGAASDepreciationExpense> getProcessedExpenses(Properties ctx,
            int assetId, Timestamp toDate, String trxName)
    {
        String sql = "SELECT GAAS_Depreciation_Expense_ID"
                + " FROM GAAS_Depreciation_Expense WHERE A_Asset_ID=?"
                + " AND AssetDepreciationDate<? AND Processed='Y'"
                + " ORDER BY AssetDepreciationDate";

        ArrayList<MGAASDepreciationExpense> expenses = new ArrayList<MGAASDepreciationExpense>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, trxName);
            pstmt.setInt(1, assetId);
            pstmt.setTimestamp(2, toDate);
            rs = pstmt.executeQuery();

            while (rs.next())
            {
                expenses.add(new MGAASDepreciationExpense(ctx, rs.getInt(1),
                        trxName));
            }
        }
        catch (Exception ex)
        {
            logger.log(Level.SEVERE, "Could not get Depreciation Expenses", ex);
            throw new IllegalStateException(
                    "Could not get data for Depreciation Expenses");
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        return expenses;
    }

    public static List<MGAASDepreciationExpense> getProcessedExpenses(Properties ctx,
            int assetId, Timestamp fromDate, Timestamp toDate, String trxName)
    {
        String sql = "SELECT GAAS_Depreciation_Expense_ID"
                + " FROM GAAS_Depreciation_Expense WHERE A_Asset_ID=?"
                + " AND AssetDepreciationDate>=? AND AssetDepreciationDate<?"
                + " AND Processed='Y' AND PeriodNo <> 0 ORDER BY AssetDepreciationDate";

        ArrayList<MGAASDepreciationExpense> expenses = new ArrayList<MGAASDepreciationExpense>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, trxName);
            pstmt.setInt(1, assetId);
            pstmt.setTimestamp(2, fromDate);
            pstmt.setTimestamp(3, toDate);
            rs = pstmt.executeQuery();

            while (rs.next())
            {
                expenses.add(new MGAASDepreciationExpense(ctx, rs.getInt(1),
                        trxName));
            }
        }
        catch (Exception ex)
        {
            logger.log(Level.SEVERE, "Could not get Depreciation Expenses", ex);
            throw new IllegalStateException(
                    "Could not get data for Depreciation Expenses");
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        return expenses;
    }

    public static MGAASDepreciationExpense getProcessedExpense(Properties ctx,
            int assetId, int periodNo, String trxName)
    {
        MGAASDepreciationExpense retValue = null;
        String sql = "SELECT GAAS_Depreciation_Expense_ID"
                + " FROM GAAS_Depreciation_Expense WHERE A_Asset_ID=?"
                + " AND PeriodNo=? AND Processed='Y'";

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, trxName);
            pstmt.setInt(1, assetId);
            pstmt.setInt(2, periodNo);
            rs = pstmt.executeQuery();

            if (rs.next())
            {
                retValue = new MGAASDepreciationExpense(ctx, rs.getInt(1),
                        trxName);
            }
        }
        catch (Exception ex)
        {
            logger.log(Level.SEVERE, "Could not get Depreciation Expense", ex);
            throw new IllegalStateException(
                    "Could not get data for Depreciation Expense");
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        return retValue;
    }
}
