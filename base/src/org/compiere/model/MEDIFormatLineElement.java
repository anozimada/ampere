package org.compiere.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.CLogger;
import org.compiere.util.DB;

public class MEDIFormatLineElement extends X_C_EDIFormat_LineElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**	Static Logger	*/
	private static CLogger	s_log	= CLogger.getCLogger (X_C_EDIFormat_LineElement.class);
	
	
	public MEDIFormatLineElement(Properties ctx, int C_EDIFormat_LineElement_ID, String trxName) {
		super(ctx, C_EDIFormat_LineElement_ID, trxName);
	}
	
	public MEDIFormatLineElement (Properties ctx, ResultSet rs, String trxName) {
		super (ctx, rs, trxName);
	}
	
	public X_C_EDIFormat_LineElementComp[] getEDIFormat_LineElementComps(String trxName) {
		List<X_C_EDIFormat_LineElementComp> resultList = new ArrayList<X_C_EDIFormat_LineElementComp>();
		                   
		StringBuffer sql = new StringBuffer("SELECT * ")
			.append(" FROM ").append(X_C_EDIFormat_LineElementComp.Table_Name)
			.append(" WHERE ").append(X_C_EDIFormat_LineElementComp.COLUMNNAME_C_EDIFormat_LineElement_ID).append("=?")
			.append(" AND IsActive=?")
			.append(" ORDER BY ").append(X_C_EDIFormat_LineElementComp.COLUMNNAME_Position);
		PreparedStatement pstmt = null;
		X_C_EDIFormat_LineElementComp ediLine = null;
		try {
			pstmt = DB.prepareStatement (sql.toString(), trxName);
			pstmt.setInt(1, getC_EDIFormat_LineElement_ID());
			pstmt.setString(2, "Y");
			ResultSet rs = pstmt.executeQuery ();
			while ( rs.next() ) {
				ediLine = new X_C_EDIFormat_LineElementComp (getCtx(), rs, trxName);
				resultList.add(ediLine);
			}
			rs.close ();
			pstmt.close ();
			pstmt = null;
		} catch (SQLException e) {
			s_log.log(Level.SEVERE, sql.toString(), e);
		} finally {
			try	{
				if (pstmt != null) pstmt.close ();
				pstmt = null;
			} catch (Exception e) {	pstmt = null; }
		}
		
		X_C_EDIFormat_LineElementComp[] result = (X_C_EDIFormat_LineElementComp[])resultList.toArray( new X_C_EDIFormat_LineElementComp[0]);
		return result;
	}
	
	/**
	 * 	Copy EDI Format Lines From other EDI Format
	 *	@param from source of EDI Format
	 *	@param to   target of EDI Format
	 *	@return number of lines copied
	 */
	public int copyLinesElementCompFrom (MEDIFormatLineElement from)
	{
		if (from == null)
			return 0;
		X_C_EDIFormat_LineElementComp[] linesElement = from.getEDIFormat_LineElementComps(get_TrxName());
	
		int count = 0;
		
		for (X_C_EDIFormat_LineElementComp fromLineElement : linesElement){
			X_C_EDIFormat_LineElementComp toLineElement = new X_C_EDIFormat_LineElementComp(getCtx(), 0, get_TrxName());
			PO.copyValues(fromLineElement, toLineElement, getAD_Client_ID(), getAD_Org_ID());
			toLineElement.setC_EDIFormat_LineElement_ID(get_ID());
			
			if (toLineElement.save(get_TrxName()))
				count++;
			else
				log.log(Level.SEVERE, "Record not saved!"+toLineElement);
			
		}
		if (linesElement.length != count)
			log.log(Level.SEVERE, "Line difference - From=" + linesElement.length + " <> Saved=" + count);
		
		return count;
	}	//	copyLinesFrom
	
}
