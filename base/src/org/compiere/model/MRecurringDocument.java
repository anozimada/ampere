package org.compiere.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;


public class MRecurringDocument extends X_C_Recurring_Document {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MRecurringDocument (Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}	//	MRecurringDocument

	public MRecurringDocument(Properties ctx, int C_Recurring_Document_ID,
			String trxName) {
		super(ctx, C_Recurring_Document_ID, trxName);
	}

	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return true
	 */
	protected boolean beforeSave (boolean newRecord)
	{
		String rt = getRecurringType();
		if (rt == null)
		{
			log.saveError("FillMandatory", Msg.getElement(getCtx(), "RecurringType"));
			return false;
		}
		if (rt.equals(MRecurring.RECURRINGTYPE_Order)
			&& getC_Order_ID() == 0)
		{
			log.saveError("FillMandatory", Msg.getElement(getCtx(), "C_Order_ID"));
			return false;
		}
		if (rt.equals(MRecurring.RECURRINGTYPE_Invoice)
			&& getC_Invoice_ID() == 0)
		{
			log.saveError("FillMandatory", Msg.getElement(getCtx(), "C_Invoice_ID"));
			return false;
		}
		if (rt.equals(MRecurring.RECURRINGTYPE_GLJournalBatch)
			&& getGL_JournalBatch_ID() == 0)
		{
			log.saveError("FillMandatory", Msg.getElement(getCtx(), "GL_JournalBatch_ID"));
			return false;
		}
		if (rt.equals(MRecurring.RECURRINGTYPE_Project)
			&& getC_Project_ID() == 0)
		{
			log.saveError("FillMandatory", Msg.getElement(getCtx(), "C_Project_ID"));
			return false;
		}
		if (rt.equals(MRecurring.RECURRINGTYPE_GLJournal)
				&& get_ValueAsInt("GL_Journal_ID") == 0)
		{
			log.saveError("FillMandatory", Msg.getElement(getCtx(), "GL_Journal_ID"));
			return false;
		}
		
//		setRunsRemaining(Env.getContextAsInt(Env.getCtx(), "@RunsMax@") - getCurrentRun());
		
		if (rt.equals(MRecurring.RECURRINGTYPE_Order))
		{
			MOrder order = new MOrder(Env.getCtx(), getC_Order_ID(), null);
			setC_BPartner_ID(order.getC_BPartner_ID());
			//check if exists on other recurring documents
			//check if child of other recurring document
			setStatus(checkOrderStatus(getC_Order_ID()));
			return true;
		}
		if (rt.equals(MRecurring.RECURRINGTYPE_Invoice))
		{
			MInvoice invoice = new MInvoice(Env.getCtx(), getC_Invoice_ID(), null);
			setC_BPartner_ID(invoice.getC_BPartner_ID());
			setStatus(checkInvoiceStatus(getC_Invoice_ID()));
			return true;
		}
		if (rt.equals(MRecurring.RECURRINGTYPE_GLJournalBatch))
		{
			//MJournal journal = new MJournal(Env.getCtx(), getC_Invoice_ID(), null);
			setStatus(checkJournalBatchStatus(getGL_JournalBatch_ID()));
			return true;
		}
		if (rt.equals(MRecurring.RECURRINGTYPE_Project))
		{
			MProject project = new MProject(Env.getCtx(), getC_Project_ID(), null);
			setC_BPartner_ID(project.getC_BPartner_ID());
			setStatus(checkProjectStatus(getC_Project_ID()));
			return true;
		}
		if (rt.equals(MRecurring.RECURRINGTYPE_GLJournal))
		{
			MJournal journal= new MJournal(Env.getCtx(),get_ValueAsInt("GL_Journal_ID"), null);
			//setC_BPartner_ID(journal.getC_BPartner_ID());
			setStatus(checkJournalStatus(get_ValueAsInt("GL_Journal_ID")));
			return true;
		}


		return true;
	}	//	beforeSave
	
	
	private String checkOrderStatus(int c_order_id)
	{
		StringBuilder sb = new StringBuilder();
		
		MRecurringDocument[] rds = getOtherRecurringDocuments("C_Order_ID = ? AND isActive = 'Y'", "C_Recurring_Document_ID", c_order_id);
		if (rds.length > 1) {
			sb.append("Exists in Recurring Document:");
			for (MRecurringDocument rd: rds) {
				if (rd.getC_Recurring_Document_ID() == getC_Recurring_Document_ID()) continue;
				
				sb.append(";");
			}
		}
		if (isRecurringDocumentChild("C_Order_ID = ?", c_order_id)) {
			if (sb.length() > 0) sb.append(" | ");
			sb.append("Not Seed Document");
		}
		
		if (sb.length() == 0)
			return "OK";
		
		return sb.toString();
	}

	private String checkInvoiceStatus(int c_invoice_id)
	{
		StringBuilder sb = new StringBuilder();
		
		MRecurringDocument[] rds = getOtherRecurringDocuments("C_Invoice_ID = ? AND isActive = 'Y'", "C_Recurring_Document_ID", c_invoice_id);
		if (rds.length > 1) {
			sb.append("Exists in Recurring Document:");
			for (MRecurringDocument rd: rds) {
				if (rd.getC_Recurring_Document_ID() == getC_Recurring_Document_ID()) continue;
				
				sb.append(";");
			}
		}
		if (isRecurringDocumentChild("C_Invoice_ID = ?", c_invoice_id)) {
			if (sb.length() > 0) sb.append(" | ");
			sb.append("Not Seed Document");
		}
		
		if (sb.length() == 0)
			return "OK";
		
		return sb.toString();
	}

	private String checkJournalBatchStatus(int gl_journalbatch_id)
	{
		StringBuilder sb = new StringBuilder();
		
		MRecurringDocument[] rds = getOtherRecurringDocuments("GL_JournalBatch_ID = ? AND isActive = 'Y'", "C_Recurring_Document_ID", gl_journalbatch_id);
		if (rds.length > 1) {
			sb.append("Exists in Recurring Document:");
			for (MRecurringDocument rd: rds) {
				if (rd.getC_Recurring_Document_ID() == getC_Recurring_Document_ID()) continue;
				
				sb.append(";");
			}
		}
		if (isRecurringDocumentChild("GL_JournalBatch_ID = ?", gl_journalbatch_id)) {
			if (sb.length() > 0) sb.append(" | ");
			sb.append("Not Seed Document");
		}
		
		if (sb.length() == 0)
			return "OK";
		
		return sb.toString();
	}
	private String checkJournalStatus(int gl_journal_id)
	{
		StringBuilder sb = new StringBuilder();
		
		MRecurringDocument[] rds = getOtherRecurringDocuments("GL_Journal_ID = ? AND isActive = 'Y'", "C_Recurring_Document_ID",gl_journal_id);
		if (rds.length > 1) {
			sb.append("Exists in Recurring Document:");
			for (MRecurringDocument rd: rds) {
				if (rd.getC_Recurring_Document_ID() == getC_Recurring_Document_ID()) continue;
				
				sb.append(";");
			}
		}
		if (isRecurringDocumentChild("GL_Journal_ID = ?",  gl_journal_id)) {
			if (sb.length() > 0) sb.append(" | ");
			sb.append("Not Seed Document");
		}
		
		if (sb.length() == 0)
			return "OK";
		
		return sb.toString();
	}

	private String checkProjectStatus(int c_project_id)
	{
		StringBuilder sb = new StringBuilder();
		
		MRecurringDocument[] rds = getOtherRecurringDocuments("C_Project_ID = ? AND isActive = 'Y'", "C_Recurring_Document_ID", c_project_id);
		if (rds.length > 1) {
			sb.append("Exists in Recurring Document:");
			for (MRecurringDocument rd: rds) {
				if (rd.getC_Recurring_Document_ID() == getC_Recurring_Document_ID()) continue;
				
				sb.append(";");
			}
		}
		if (isRecurringDocumentChild("C_Project_ID = ?", c_project_id)) {
			if (sb.length() > 0) sb.append(" | ");
			sb.append("Not Seed Document");
		}
		
		if (sb.length() == 0)
			return "OK";
		
		return sb.toString();
	}
	
	private MRecurringDocument[] getOtherRecurringDocuments(String whereClause, String orderClause, int ID)
	{
		List<MRecurringDocument> list = new Query(getCtx(), I_C_Recurring_Document.Table_Name, whereClause, get_TrxName())
										.setParameters(ID)
										.setOrderBy(orderClause)
										.list();
		return list.toArray(new MRecurringDocument[list.size()]);		
	}	//	getLines
	

	private boolean isRecurringDocumentChild(String whereClause, int ID)
	{
		String sql = "SELECT COUNT(*) FROM C_Recurring_Run WHERE " + whereClause;
		return DB.getSQLValue(get_TrxName(), sql, ID) > 0;	
	}
	
	//	/**
//	 * 	After Save
//	 *	@param newRecord new
//	 *	@param success success
//	 *	@return true if can be saved
//	 */
//	protected boolean afterSave (boolean newRecord, boolean success)
//	{
//		if (!success || newRecord)
//			return success;
//
//		String rt = getRecurringType();
//		if (rt.equals(MRecurring.RECURRINGTYPE_Order))
//		{
//			MOrder order = new MOrder(Env.getCtx(), getC_Order_ID(), null);
//			setC_BPartner_ID(order.getC_BPartner_ID());
//			return true;
//		}
//		if (rt.equals(MRecurring.RECURRINGTYPE_Invoice))
//		{
//			MInvoice invoice = new MInvoice(Env.getCtx(), getC_Invoice_ID(), null);
//			setC_BPartner_ID(invoice.getC_BPartner_ID());
//			return true;
//		}
//		if (rt.equals(MRecurring.RECURRINGTYPE_GLJournal))
//		{
//			//MJournal journal = new MJournal(Env.getCtx(), getC_Invoice_ID(), null);
//			return true;
//		}
//		if (rt.equals(MRecurring.RECURRINGTYPE_Project))
//		{
//			MProject project = new MProject(Env.getCtx(), getC_Invoice_ID(), null);
//			setC_BPartner_ID(project.getC_BPartner_ID());
//			return true;
//		}
//		
//		
//	
//		return true;
//	}

	
/*	public int[] getOrder(int C_Recurring_ID) {
		List<Integer> id = new ArrayList<Integer>();
		
		String sql = "SELECT C_Order_ID"  
		+ " FROM C_Recurring_Document"
		+ " WHERE C_Recurring_ID = ?";

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try
        {
            pstmt = DB.prepareStatement(sql, null);
            pstmt.setInt(1, C_Recurring_ID);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	id.add(rs.getInt(1));
            }

        }
        catch (SQLException e)
        {
        	return null;
        }
        finally
        {
            try {
				rs.close();
				pstmt.close();
			} catch (SQLException e) {
				
			}
            rs= null;
            pstmt = null;
        }
		return id.toArray(int);
		
	}
*/	
	/**
	 * Return the number of runs
	 */
	public int getCurrentRun()
	{

		String sql = "SELECT COUNT(*)+1 FROM C_Recurring_Run WHERE C_Recurring_Document_ID=?";
		return DB.getSQLValue(get_TrxName(), sql, getC_Recurring_Document_ID());		
	}
}

