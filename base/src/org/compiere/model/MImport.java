/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/

package org.compiere.model;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

public class MImport extends X_AD_Import {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9052544341602655427L;

	public MImport(Properties ctx, int AD_Import_ID, String trxName) {
		super(ctx, AD_Import_ID, trxName);
	}

	public MImport(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		return true;
	}

	public String runImport() {
		
		runValidation();
		
		String message = importRecords();
		
		return message;
	}

	private String importRecords() {
		int added = 0, updated = 0;
		
		MTable sourceTable = MTable.get(getCtx(), getSource_Table_ID());
		MTable targetTable = MTable.get(getCtx(), getTarget_Table_ID());
		
		List<PO> sourceData = sourceTable.createQuery("I_IsImported='N'", get_TrxName())
							.setOnlyActiveRecords(true)
							.setClient_ID()
							.setOrderBy(getOrderBy())
							.list();
		
		for (PO source : sourceData)
		{
			boolean newrecord = false;
			PO target = null;
			String where = "";
			ArrayList<Object> params = new ArrayList<Object>();
			boolean first = true;

			for ( MImportColumn key : getKeyColumns() )
			{ 
				if (key.getTarget_Column_ID() <= 0 || key.getSource_Column_ID() <= 0)
					continue;

				if ( first )
					first = false;
				else
					where += " AND ";

				MColumn sourceColumn = (MColumn) key.getSource_Column();
				MColumn targetColumn = (MColumn) key.getTarget_Column();

				where += targetTable.getTableName() + "." + targetColumn.getColumnName() + " = ? ";
				params.add(source.get_Value(sourceColumn.getColumnName()));

			}

			target = targetTable.createQuery(where, get_TrxName())
			.setClient_ID()
			.setParameters(params)
			.firstOnly();
			
			if ( target == null )
			{
				target = targetTable.getPO(0, get_TrxName());
				newrecord = true;
			}

			for (MImportColumn column : getColumns())
			{
				Object obj = source.get_Value(column.getSource_Column().getColumnName());
				if (obj != null)
					target.set_Value(column.getTarget_Column().getColumnName(), obj);
			}
			
			try
			{
				target.saveEx();
				source.set_Value("I_IsImported", true);
				source.set_Value("Processed", true);
				
				if (newrecord)
				{
					for ( MImportColumn key : getKeyColumns() )
					{ 
						if (key.getTarget_Column_ID() <= 0 || key.getSource_Column_ID() <= 0)
							continue;

						MColumn sourceColumn = (MColumn) key.getSource_Column();
						MColumn targetColumn = (MColumn) key.getTarget_Column();

						source.set_Value(sourceColumn.getColumnName(), target.get_Value(targetColumn.getColumnName()));
					}
				}
				source.save();
			}
			catch (Exception e) 
			{
				source.set_Value("I_IsImported", false);
				log.log(Level.WARNING,e.getLocalizedMessage(), e);
				source.set_Value("I_ErrorMsg", e.getLocalizedMessage());
				source.save();
			}
			
			
		
			if (newrecord)
				added++;
			else
				updated++;
		}
		return "New records: " + added + ", Updated records: " + updated;
	}

	private String getOrderBy() {
		String orderBy = "";
		boolean first = true;
		for (MImportColumn sortColumn : getSortColumns())
		{
			if (sortColumn.getSource_Column_ID() > 0)
			{
				if (first)
					first = false;
				else
					orderBy += ", ";
						
				orderBy += sortColumn.getSource_Column().getColumnName();
				
			}
		}
		return orderBy;
	}
			

	private List<MImportColumn> getColumns() {
		Query query = MTable.get(getCtx(), MImportColumn.Table_ID).createQuery("AD_Import_ID=?", get_TrxName())
		.setOnlyActiveRecords(true)
		.setParameters(getAD_Import_ID());
	return query.list();
	}
	
	private List<MImportColumn> getSortColumns() {
		Query query = MTable.get(getCtx(), MImportColumn.Table_ID).createQuery("AD_Import_ID=? AND IsOrderBy = 'Y'", get_TrxName())
		.setOnlyActiveRecords(true)
		.setOrderBy("SortNo")
		.setParameters(getAD_Import_ID());
	return query.list();
	}
	
	private List<MImportColumn> getKeyColumns() {
		Query query = MTable.get(getCtx(), MImportColumn.Table_ID).createQuery("AD_Import_ID=? AND IsKey='Y'", get_TrxName())
		.setOnlyActiveRecords(true)
		.setOrderBy("SortNo")
		.setParameters(getAD_Import_ID());
		return query.list();
	}

	private void runValidation() {
		
		for (MImportValidation validation : getValidations())
		{
			validation.validate();
		}
		
	}

	private List<MImportValidation> getValidations() {
		Query query = MTable.get(getCtx(), MImportValidation.Table_ID).createQuery("AD_Import_ID=?", get_TrxName())
			.setOnlyActiveRecords(true)
			.setOrderBy("SeqNo")
			.setParameters(getAD_Import_ID());
		return query.list();
	}

}
