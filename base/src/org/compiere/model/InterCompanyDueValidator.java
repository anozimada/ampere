package org.compiere.model;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import org.compiere.acct.Doc;
import org.compiere.acct.Fact;
import org.compiere.acct.FactLine;
import org.compiere.util.Env;
import org.compiere.util.Util;

/**
 * Validator for Posting of Inter-organization transaction
 * 
 * @author <a href="mailto:sbhimani@logilite.com">Sachin Bhimani</a>
 */
public class InterCompanyDueValidator implements FactsValidator, ModelValidator
{

	public static final String	INTER_COMPANY_DUE_POSTING	= "INTER_COMPANY_DUE_POSTING";
	private int					m_AD_Client_ID				= -1;

	@Override
	public void initialize(ModelValidationEngine engine, MClient client)
	{
		engine.addFactsValidate(MAllocationHdr.Table_Name, this);
		engine.addFactsValidate(MProduction.Table_Name, this);
		engine.addFactsValidate(MInventory.Table_Name, this);
		engine.addFactsValidate(MMatchInv.Table_Name, this);
		engine.addFactsValidate(MMovement.Table_Name, this);
		engine.addFactsValidate(MInvoice.Table_Name, this);
		engine.addFactsValidate(MPayment.Table_Name, this);
		engine.addFactsValidate(MJournal.Table_Name, this);
		engine.addFactsValidate(MInOut.Table_Name, this);

		if (client != null)
			m_AD_Client_ID = client.getAD_Client_ID();
	}

	@Override
	public int getAD_Client_ID()
	{
		return m_AD_Client_ID;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID)
	{
		return null;
	}

	@Override
	public String modelChange(PO po, int type) throws Exception
	{
		return null;
	}

	@Override
	public String docValidate(PO po, int timing)
	{
		return null;
	}

	@Override
	public String factsValidate(MAcctSchema schema, List<Fact> facts, PO po)
	{
		boolean isInterOrgPosting = MSysConfig.getBooleanValue(INTER_COMPANY_DUE_POSTING, false);
		if (!isInterOrgPosting)
			return null;

		if (MAllocationHdr.Table_Name.equals(po.get_TableName()))
		{
			String errorMsg = postAllocationHdr(schema, facts);
			if (!Util.isEmpty(errorMsg, true))
				return errorMsg;
		}
		else if (MInvoice.Table_Name.equals(po.get_TableName()))
		{
			postInvoice(schema, facts, po);
		}
		else if (MMatchInv.Table_Name.equals(po.get_TableName()))
		{
			postMatchInv(schema, facts, po);
		}
		else if (MProduction.Table_Name.equals(po.get_TableName()))
		{
			postProduction(schema, facts, po);
		}

		return null;
	}

	private String postAllocationHdr(MAcctSchema schema, List<Fact> facts)
	{
		for (Fact fact : facts)
		{
			HashMap<Integer, BigDecimal> orgBalance = new HashMap<Integer, BigDecimal>();
			HashMap<Integer, BigDecimal> drOrgList = new HashMap<Integer, BigDecimal>();
			HashMap<Integer, BigDecimal> crOrgList = new HashMap<Integer, BigDecimal>();
			BigDecimal validBalance = Env.ZERO;
			FactLine refFactline = null;

			for (FactLine factLine : fact.getLines())
			{
				MOrg flOrg = new MOrg(factLine.getCtx(), factLine.getAD_Org_ID(), factLine.get_TrxName());
				if (flOrg.get_ValueAsBoolean("IsConsolAdjustOrg"))
					continue;
				BigDecimal drAmt = orgBalance.get(factLine.getAD_Org_ID());
				if (drAmt == null)
					drAmt = Env.ZERO;

				drAmt = drAmt.add(factLine.getAmtAcctDr().subtract(factLine.getAmtAcctCr()));
				orgBalance.put(factLine.getAD_Org_ID(), drAmt);
				refFactline = factLine;
			}

			// Create Org wise list for Credit and Debit
			for (Integer orgID : orgBalance.keySet())
			{
				BigDecimal amt = orgBalance.get(orgID);
				if (amt.signum() == 0)
					continue;
				else if (amt.signum() > 0)
					crOrgList.put(orgID, amt);
				else
					drOrgList.put(orgID, amt);

				validBalance.add(amt);
			}

			// Check is valid balance for inter org posting
			if (validBalance.signum() != 0)
			{
				return "Inter org amt not balanced";
			}

			for (Integer drOrgID : drOrgList.keySet())
			{
				BigDecimal drAmt = drOrgList.get(drOrgID).abs();

				if (drAmt.signum() == 0)
					continue;

				for (Integer crOrgID : crOrgList.keySet())
				{
					BigDecimal crAmt = crOrgList.get(crOrgID);
					if (crAmt.signum() == 0)
						continue;

					BigDecimal amount = crAmt;
					// if there's more balance on Credit than left in Debit
					if (amount.abs().compareTo(drAmt.abs()) > 0)
						amount = drAmt;

					FactLine dueFromLine = new FactLine(fact.getDoc().getCtx(), fact.getDoc().get_Table_ID(),
							fact.getDoc().get_ID(), 0, fact.get_TrxName());
					dueFromLine.setDocumentInfo(fact.getDoc(), null);
					dueFromLine.setPostingType(refFactline.getPostingType());

					dueFromLine.setAD_Org_ID(drOrgID);
					dueFromLine.setAD_OrgTrx_ID(crOrgID);
					dueFromLine.setAccount(schema, schema.getDueFrom_Acct(""));
					dueFromLine.setAmtSource(schema.getC_Currency_ID(), amount.abs(), Env.ZERO);
					dueFromLine.convert();
					fact.add(dueFromLine);

					FactLine dueToLine = new FactLine(fact.getDoc().getCtx(), fact.getDoc().get_Table_ID(),
							fact.getDoc().get_ID(), 0, fact.get_TrxName());
					dueToLine.setDocumentInfo(fact.getDoc(), null);
					dueToLine.setAccount(schema, schema.getDueTo_Acct(""));
					dueToLine.setPostingType(refFactline.getPostingType());
					dueToLine.setAmtSource(schema.getC_Currency_ID(), Env.ZERO, amount.abs());
					dueToLine.setAD_Org_ID(crOrgID);
					dueToLine.setAD_OrgTrx_ID(drOrgID);
					dueToLine.convert();
					fact.add(dueToLine);

					// subtract amount from Debit/Credit
					crAmt = crAmt.subtract(amount);
					drAmt = drAmt.subtract(amount);

					// Update Map Amt values
					crOrgList.put(crOrgID, crAmt);
					drOrgList.put(drOrgID, drAmt);
				}
			}
		}

		return null;
	}

	private void postInvoice(MAcctSchema schema, List<Fact> facts, PO po)
	{
		int doc_org_ID = po.getAD_Org_ID();
		for (Fact fact : facts)
		{
			for (FactLine factLine : fact.getLines())
			{
				MOrg flOrg = new MOrg(po.getCtx(), factLine.getAD_Org_ID(), po.get_TrxName());
				if (!flOrg.get_ValueAsBoolean("IsConsolAdjustOrg") && factLine.getAD_Org_ID() != doc_org_ID)
				{
					BigDecimal amtDr = factLine.getAmtSourceDr().subtract(factLine.getAmtSourceCr());
					int dueFromOrg = 0;
					int dueToOrg = 0;
					if (amtDr.compareTo(Env.ZERO) > 0)
					{
						dueFromOrg = doc_org_ID;
						dueToOrg = factLine.getAD_Org_ID();
					}
					else
					{
						dueFromOrg = factLine.getAD_Org_ID();
						dueToOrg = doc_org_ID;
					}
					FactLine dueFromLine = fact.createLine(factLine.getDocLine(), schema.getDueFrom_Acct(""),
							factLine.getC_Currency_ID(), amtDr.abs(), Env.ZERO);

					dueFromLine.setAD_Org_ID(dueFromOrg);
					dueFromLine.setAD_OrgTrx_ID(dueToOrg);

					FactLine dueToLine = fact.createLine(factLine.getDocLine(), schema.getDueTo_Acct(""),
							factLine.getC_Currency_ID(), Env.ZERO, amtDr.abs());
					dueToLine.setAD_Org_ID(dueToOrg);
					dueToLine.setAD_OrgTrx_ID(dueFromOrg);
				}
			}
		}
	}

	private void postMatchInv(MAcctSchema schema, List<Fact> facts, PO po)
	{
		MMatchInv matchInv = (MMatchInv) po;
		int receiptOrgID = 0;
		int invoiceOrgID = 0;

		if (matchInv.getM_InOutLine_ID() > 0 && matchInv.getC_InvoiceLine_ID() > 0)
		{
			receiptOrgID = matchInv.getM_InOutLine().getAD_Org_ID();
			invoiceOrgID = matchInv.getC_InvoiceLine().getAD_Org_ID();
		}

		if (receiptOrgID != 0 && invoiceOrgID != 0 && receiptOrgID != invoiceOrgID)
		{
			for (Fact fact : facts)
			{
				MAccount NIR = fact.getDoc().getAccount(Doc.ACCTTYPE_NotInvoicedReceipts, schema);
				for (FactLine factLine : fact.getLines())
				{
					if (NIR.getAccount_ID() != factLine.getAccount_ID())
					{
						BigDecimal drAmt = factLine.getAmtSourceDr().subtract(factLine.getAmtSourceCr());
						int orgDueTo;
						int orgDueFrom;
						if (drAmt.compareTo(Env.ZERO) < 0)
						{
							orgDueTo = invoiceOrgID;
							orgDueFrom = receiptOrgID;
						}
						else
						{
							orgDueTo = receiptOrgID;
							orgDueFrom = invoiceOrgID;
						}

						FactLine dueFromLine = fact.createLine(factLine.getDocLine(), schema.getDueFrom_Acct(""),
								factLine.getC_Currency_ID(), drAmt.abs(), Env.ZERO);
						dueFromLine.setAD_Org_ID(orgDueTo);
						dueFromLine.setAD_OrgTrx_ID(orgDueFrom);

						FactLine dueToLine = fact.createLine(factLine.getDocLine(), schema.getDueTo_Acct(""),
								factLine.getC_Currency_ID(), Env.ZERO, drAmt.abs());
						dueToLine.setAD_Org_ID(orgDueFrom);
						dueToLine.setAD_OrgTrx_ID(orgDueTo);
					}
				}
			}
		}
	}

	private String postProduction(MAcctSchema schema, List<Fact> facts, PO po)
	{
		return postAllocationHdr(schema, facts);
	}
}
