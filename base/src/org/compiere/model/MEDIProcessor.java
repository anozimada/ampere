/**********************************************************************
 * This file is part of Adempiere ERP Bazaar                          * 
 * http://www.adempiere.org                                           * 
 *                                                                    * 
 * Copyright (C) Trifon Trifonov.                                     * 
 * Copyright (C) Contributors                                         * 
 *                                                                    * 
 * This program is free software; you can redistribute it and/or      * 
 * modify it under the terms of the GNU General Public License        * 
 * as published by the Free Software Foundation; either version 2     * 
 * of the License, or (at your option) any later version.             * 
 *                                                                    * 
 * This program is distributed in the hope that it will be useful,    * 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     * 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the       * 
 * GNU General Public License for more details.                       * 
 *                                                                    * 
 * You should have received a copy of the GNU General Public License  * 
 * along with this program; if not, write to the Free Software        * 
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,         * 
 * MA 02110-1301, USA.                                                * 
 *                                                                    * 
 * Contributors:                                                      * 
 *  - Trifon Trifonov (trifonnt@users.sourceforge.net)                *
 *                                                                    *
 * Sponsors:                                                          *
 *  - Idalica (http://www.idalica.com/)                               *
 **********************************************************************/

package org.compiere.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.CLogger;
import org.compiere.util.DB;

public class MEDIProcessor extends X_C_EDIProcessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**	Static Logger	*/
	private static CLogger	s_log	= CLogger.getCLogger (MEDIProcessor.class);
	
	
	public MEDIProcessor(Properties ctx, int C_EDIProcessor_ID, String trxName) {
		super(ctx, C_EDIProcessor_ID, trxName);
	}
	
	public MEDIProcessor(Properties ctx, ResultSet rs, String trxName) {
		super (ctx, rs, trxName);
	}
	
	public X_C_EDIProcessor_Parameter[] getEDIProcessorParameters(String trxName) {
		List<X_C_EDIProcessor_Parameter> resultList = new ArrayList<X_C_EDIProcessor_Parameter>();
		                   
		StringBuffer sql = new StringBuffer("SELECT * ")
			.append(" FROM ").append(X_C_EDIProcessor_Parameter.Table_Name)
			.append(" WHERE ").append(X_C_EDIProcessor_Parameter.COLUMNNAME_C_EDIProcessor_ID).append("=?") // # 1
			.append(" AND IsActive = ?")  // # 2
			//.append(" ORDER BY ").append(X_C_EDIProcessor_Parameter.COLUMNNAME_)
		;
		PreparedStatement pstmt = null;
		X_C_EDIProcessor_Parameter processorParameter = null;
		try {
			pstmt = DB.prepareStatement (sql.toString(), trxName);
			pstmt.setInt(1, getC_EDIProcessor_ID());
			pstmt.setString(2, "Y");
			ResultSet rs = pstmt.executeQuery ();
			while ( rs.next() ) {
				processorParameter = new X_C_EDIProcessor_Parameter (getCtx(), rs, trxName);
				resultList.add(processorParameter);
			}
			rs.close ();
			pstmt.close ();
			pstmt = null;
		} catch (SQLException e) {
			s_log.log(Level.SEVERE, sql.toString(), e);
		} finally {
			try	{
				if (pstmt != null) pstmt.close ();
				pstmt = null;
			} catch (Exception e) {	pstmt = null; }
		}
		X_C_EDIProcessor_Parameter[] result = (X_C_EDIProcessor_Parameter[])resultList.toArray( new X_C_EDIProcessor_Parameter[0]);
		return result;
	}
	
}
