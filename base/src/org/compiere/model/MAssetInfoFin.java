/*******************************************************************************
 * (C) Astidian Systems 2012                                                   *
 *                                                                             *
 ******************************************************************************/

package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;

public class MAssetInfoFin extends X_A_Asset_Info_Fin 
{
	private static final long serialVersionUID = 5038524599974599940L;
	
	public MAssetInfoFin(Properties ctx, int A_Asset_Info_Fin_ID, String trxName) 
	{
		super(ctx, A_Asset_Info_Fin_ID, trxName);
	}
	
	public MAssetInfoFin(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}

	public static MAssetInfoFin get(MAsset asset)
	{
		if (asset == null || asset.getA_Asset_ID() <= 0)
		{
			throw new IllegalArgumentException("Asset Mandatory");
		}
		
		String sql = "SELECT A_Asset_Info_Fin_ID FROM A_Asset_Info_Fin WHERE A_Asset_ID=?";
		
		int infoFinId = DB.getSQLValue(asset.get_TrxName(), sql, asset.getA_Asset_ID());
		
		if (infoFinId <= 0)
		{
			return null;
		}
		
		return new MAssetInfoFin(asset.getCtx(), infoFinId, asset.get_TrxName());
	}
}
