/*******************************************************************************
 * (C) Astidian Systems 2012                                                   *
 *                                                                             *
 ******************************************************************************/
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;



/**
 * @author Ashley G Ramdass
 *
 */
public class MAssetGAAS extends MAsset
{
    private static final long serialVersionUID = -4716395227951394901L;

    public MAssetGAAS(Properties ctx, int A_Asset_ID, String trxName)
    {
        super(ctx, A_Asset_ID, trxName);
    }
    
    public MAssetGAAS(Properties ctx, ResultSet rs, String trxName)
    {
        super(ctx, rs, trxName);
    }
}
