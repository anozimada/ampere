package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.CCache;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * Asset Acct Model
 */
public class MGAASAssetAcct extends X_GAAS_Asset_Acct
{
    private static final long serialVersionUID = 1L;
    private static final CLogger logger = CLogger
            .getCLogger(MGAASAssetAcct.class);

    public MGAASAssetAcct(Properties ctx, int X_GAAS_Asset_Acct_ID, String trxName)
    {
        super(ctx, X_GAAS_Asset_Acct_ID, trxName);
    }

    public MGAASAssetAcct(Properties ctx, ResultSet rs, String trxName)
    {
        super(ctx, rs, trxName);
    }

    /** Static Cache: A_Asset_Acct_ID -> MAssetAcct */
    private static CCache<Integer, MGAASAssetAcct> s_cache = new CCache<Integer, MGAASAssetAcct>(
            Table_Name, 5);

    /**
     * Get Asset Accounting (from cache)
     * 
     * @param ctx
     *            context
     * @param GAAS_Asset_Acct_ID
     *            asset accounting id
     * @return asset accounting or null if not found
     */
    public static MGAASAssetAcct get(Properties ctx, int GAAS_Asset_Acct_ID)
    {
        MGAASAssetAcct acct = s_cache.get(GAAS_Asset_Acct_ID);
        if (acct != null)
        {
            return acct;
        }
        acct = new MGAASAssetAcct(ctx, GAAS_Asset_Acct_ID, null);
        if (acct.get_ID() > 0)
        {
            s_cache.put(GAAS_Asset_Acct_ID, null);
        }
        else
        {
            acct = null;
        }
        return acct;
    }

    public static int getDefaultAssetAcctId(Properties ctx, int assetId, String trxName)
    {
        String sql = "SELECT GAAS_Asset_Acct_ID FROM GAAS_Asset_Acct"
                + " WHERE A_Asset_ID=? AND C_AcctSchema_ID=? AND IsActive='Y'";
        
        int schemaId = MClientInfo.get(ctx).getC_AcctSchema1_ID();
        
        int recordId = DB.getSQLValue(trxName, sql, assetId, schemaId);
        
        if (recordId <= 0)
        {
            throw new IllegalStateException("No default asset account");
        }
            
        return recordId;
    }

    @Override
    protected boolean beforeSave(boolean newRecord)
    {
        if (getDepreciationRate() == null)
        {
            setDepreciationRate(Env.ZERO);
        }

        if (getDepreciationRate().compareTo(Env.ZERO) < 0
                || getDepreciationRate().compareTo(new BigDecimal("1")) >= 0)
        {
            logger.saveError("Error", "@InvalidDepreciationRate@");
            return false;
        }
        
        String sql = "SELECT COUNT(GAAS_Asset_Acct_ID) FROM GAAS_Asset_Acct"
                + " WHERE C_AcctSchema_ID=? AND A_Asset_ID=?"
                + " AND GAAS_Asset_Acct_ID<>? AND IsActive='Y'";
        
        int count = DB.getSQLValue(get_TrxName(), sql, getC_AcctSchema_ID(), getA_Asset_ID(), getGAAS_Asset_Acct_ID());
        
        if (count > 0)
        {
            logger.saveError("Error", "@DuplicateAcctData@");
            return false;
        }
        
        return true;
    }
} // class MGAASAssetAcct
