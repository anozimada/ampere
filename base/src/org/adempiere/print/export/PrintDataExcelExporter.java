/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2008 SC ARHIPAC SERVICE SRL. All Rights Reserved.            *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.print.export;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.print.attribute.standard.MediaSizeName;

import org.adempiere.impexp.AbstractExcelExporter;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.velocity.app.Velocity;
import org.compiere.print.MPrintFormat;
import org.compiere.print.MPrintFormatItem;
import org.compiere.print.MPrintPaper;
import org.compiere.print.PrintData;
import org.compiere.print.PrintDataElement;
import org.compiere.print.PrintDataFunction;
import org.compiere.print.PrintDataGroup;
import org.compiere.print.ReportEngine;
import org.compiere.util.DisplayType;
import org.mariuszgromada.math.mxparser.Expression;

/**
 * Export PrintData to Excel (XLS) file
 * @author Teo Sarca, SC ARHIPAC SERVICE SRL
 * 			<li>BF [ 1939010 ] Excel Export ERROR - java.sql.Date - integrated Mario Grigioni's fix
 * 			<li>BF [ 1974309 ] Exporting a report to XLS is not setting page format
 */
public class PrintDataExcelExporter
extends AbstractExcelExporter
{
	private PrintData m_printData;
	private MPrintFormat m_printFormat;

	public PrintDataExcelExporter(PrintData printData, MPrintFormat printFormat, PrintDataGroup printDataGroup) {
		super();
		this.m_printData = printData;
		this.m_printFormat = printFormat;
		this.m_pdg = printDataGroup;
	}
	public PrintDataExcelExporter(PrintData printData, MPrintFormat printFormat, boolean isXLSX, PrintDataGroup printDataGroup) {
		this(printData, printFormat, printDataGroup);
		this.isXLSX = isXLSX;
	}

	
	@Override
	public int getColumnCount() {
		return m_printFormat.getItemCount();
	}

	private PrintDataElement getPDE(int row, int col) {
		if (m_printData.getRowIndex() != row)
			m_printData.setRowIndex(row);
		//
		MPrintFormatItem item = m_printFormat.getItem(col);
		int AD_Column_ID = item.getAD_Column_ID();
		Object obj = null;
		boolean isSummary = m_printData.isFunctionRow();
		if (AD_Column_ID > 0 && !item.getPrintFormatType().equals(MPrintFormatItem.PRINTFORMATTYPE_Text)) {
			obj = m_printData.getNode(Integer.valueOf(AD_Column_ID));
			//retrieve grouping info here
			if (obj != null && isSummary) {
				boolean parseFunction = false;
				if (!item.isGroupBy() && !item.isSummarized() && !item.isCounted()) {
					rowGroupKey = PrintDataGroup.TOTAL;
					parseFunction = true;
				} else if (item.isGroupBy()) {
					rowGroupKey = item.getAD_Column().getColumnName();
					parseFunction = true;
				}
				if (parseFunction) {
					String val = "";
					if (obj instanceof PrintDataElement)
						val = ((PrintDataElement) obj).getValueDisplay(m_printFormat.getLanguage());
					for (char c : PrintDataFunction.FUNCTIONS) {
						if (val.toUpperCase().contains(PrintDataFunction.getFunctionName(c).toUpperCase()) ||
								val.toUpperCase().contains(PrintDataFunction.getFunctionSymbol((c)).toUpperCase())) {
							rowFunctionKey = c;
							break;
						}
					}
					//colFunction 
				}
			}

		}
		if (obj != null && obj instanceof PrintDataElement) {
			return (PrintDataElement)obj;
		}
		return null;
	}
	@Override
	public int getDisplayType(int row, int col) {
		PrintDataElement pde = getPDE(row, col);
		if (pde != null) {
			return pde.getDisplayType();
		} else if (m_printFormat.getItem(col).getExpEval() != null) {
			return DisplayType.Amount;  // for now All returned value are numeric
		}
		
		return -1;
		//
	}
	
	@Override
	public Object getValueAt(int row, int col) {
		PrintDataElement pde = getPDE(row, col);
		Object value = null;
		MPrintFormatItem item = m_printFormat.getItem(col);
		if ((pde == null && item.getExpEval() == null) || !ReportEngine.isDisplayed(item, m_printData))
			;
		else if (pde == null && item.getExpEval() != null) {
//			DecimalFormat df = null;
//			if (item.getFormatPattern() != null) {
//				df = new DecimalFormat(item.getFormatPattern());
//			} else {
//				df = DisplayType.getNumberFormat(DisplayType.Amount);
//			}
			boolean isSummary = m_printData.isFunctionRow(row);
			if (!isSummary ) {
				StringWriter expWriter = new StringWriter();
				if (m_context != null && m_context.getKeys().length > 0) {
					
					Velocity.evaluate(m_context, expWriter, "expression", item.getExpEval());
					Expression ab = new Expression(expWriter.toString());
					Double result = ab.calculate();
					if (Double.isNaN(result))
						result = 0.0;
					value = result;	
					//add to grouping
					if (item.isSummarized() || item.isCounted() || item.isAveraged() || 
							item.isMaxCalc() || item.isMinCalc() || item.isDeviationCalc() || item.isVarianceCalc()) {
						//for loop to all group columns
						for (String g : m_pdg.getM_groups()) {
							PrintDataFunction cpdf =  m_colFunctionGroup.get(g + PrintDataGroup.DELIMITER + item.getAD_PrintFormatItem_ID());
							if (cpdf == null) {
								cpdf = new PrintDataFunction();
								m_colFunctionGroup.put(g + PrintDataGroup.DELIMITER + item.getAD_PrintFormatItem_ID(), cpdf);
							}
							cpdf.addValue(new BigDecimal(result));
						}
					}
				}	
			} else {
				//retrieve applicable pdf 
				PrintDataFunction activepdf = m_colFunctionGroup.get(rowGroupKey + PrintDataGroup.DELIMITER + item.getAD_PrintFormatItem_ID());
				
				if (activepdf != null) {
					if ((item.isSummarized() && rowFunctionKey == PrintDataFunction.F_SUM) ||
						(item.isCounted() && rowFunctionKey == PrintDataFunction.F_COUNT) ||
						(item.isDeviationCalc() && rowFunctionKey == PrintDataFunction.F_DEVIATION) ||
						(item.isMaxCalc() && rowFunctionKey == PrintDataFunction.F_MAX) ||
						(item.isMinCalc() && rowFunctionKey == PrintDataFunction.F_MIN) ||
						(item.isAveraged() && rowFunctionKey == PrintDataFunction.F_MEAN) ||
						(item.isVarianceCalc() && rowFunctionKey == PrintDataFunction.F_VARIANCE)) {
							value = activepdf.getValue(rowFunctionKey);
						}
					else
						value = null;
					m_needsReset.put(rowGroupKey + PrintDataGroup.DELIMITER + item.getAD_PrintFormatItem_ID(), activepdf);	
				}
			}
		}
		else if (pde.isDate()) {
			Object o = pde.getValue();
			if (o instanceof Date)
				value = new Timestamp(((Date)o).getTime());
			else
				value = (Timestamp)pde.getValue();
		}
		else if (pde.isNumeric()) {
			Object o = pde.getValue();
			if (o instanceof Number) {
				value = ((Number)o).doubleValue();
			}
		}
		else if (pde.isYesNo()) {
			value = pde.getValue();
		}
		else if (pde.isPKey()) {
			value = pde.getValueAsString();
		}
		else {
			value = pde.getValueDisplay(getLanguage());
		}
		//
		return value;
	}

	@Override
	public String getHeaderName(int col) {
		return m_printFormat.getItem(col).getPrintName(getLanguage());
	}

	@Override
	public String getFormatPattern(int col) {
		return m_printFormat.getItem(col).getFormatPattern();
	}

	public int getAD_PrintColor_ID(int col) {
		int id = m_printFormat.getItem(col).getAD_PrintColor_ID();
		if (id == 0)
			return m_printFormat.getAD_PrintColor_ID();
		
		return id;
	}
	

	@Override
	public int getRowCount() {
		return m_printData.getRowCount();
	}

	@Override
	public boolean isColumnPrinted(int col) {
		MPrintFormatItem item = m_printFormat.getItem(col);
		return item.isPrinted();
	}

	@Override
	public boolean isPageBreak(int row, int col) {
		PrintDataElement pde = getPDE(row, col);
		return pde != null ? pde.isPageBreak() : false;
	}

	@Override
	protected void setCurrentRow(int row) {
		m_printData.setRowIndex(row);
	}

	@Override
	public boolean isFunctionRow() {
		return m_printData.isFunctionRow();
	}

	@Override
	protected void formatPage(Sheet sheet) {
		super.formatPage(sheet);
		MPrintPaper paper = MPrintPaper.get(this.m_printFormat.getAD_PrintPaper_ID());
		//
		// Set paper size:
		short paperSize = -1;
		MediaSizeName mediaSizeName = paper.getMediaSize().getMediaSizeName();
		if (MediaSizeName.NA_LETTER.equals(mediaSizeName)) {
			paperSize = PrintSetup.LETTER_PAPERSIZE;
		}
		else if (MediaSizeName.NA_LEGAL.equals(mediaSizeName)) {
		  	paperSize = PrintSetup.LEGAL_PAPERSIZE;
		}
		else if (MediaSizeName.EXECUTIVE.equals(mediaSizeName)) {
			paperSize = PrintSetup.EXECUTIVE_PAPERSIZE;
		}
		else if (MediaSizeName.ISO_A4.equals(mediaSizeName)) {
			paperSize = PrintSetup.A4_PAPERSIZE;
		}
		else if (MediaSizeName.ISO_A5.equals(mediaSizeName)) {
			paperSize = PrintSetup.A5_PAPERSIZE;
		}
		else if (MediaSizeName.NA_NUMBER_10_ENVELOPE.equals(mediaSizeName)) {
			paperSize = PrintSetup.ENVELOPE_10_PAPERSIZE;
		}
//		else if (MediaSizeName..equals(mediaSizeName)) {
//			paperSize = HSSFPrintSetup.ENVELOPE_DL_PAPERSIZE;
//		}
//		else if (MediaSizeName..equals(mediaSizeName)) {
//			paperSize = HSSFPrintSetup.ENVELOPE_CS_PAPERSIZE;
//		}
		else if (MediaSizeName.MONARCH_ENVELOPE.equals(mediaSizeName)) {
			paperSize = HSSFPrintSetup.ENVELOPE_MONARCH_PAPERSIZE;
		}
		if (paperSize != -1) {
			sheet.getPrintSetup().setPaperSize(paperSize);
		}
		//
		// Set Landscape/Portrait:
		sheet.getPrintSetup().setLandscape(paper.isLandscape());
		//
		// Set Paper Margin:
		sheet.setMargin(Sheet.TopMargin, ((double)paper.getMarginTop()) /72);
		sheet.setMargin(Sheet.RightMargin, ((double)paper.getMarginRight()) / 72);
		sheet.setMargin(Sheet.LeftMargin, ((double)paper.getMarginLeft()) / 72);
		sheet.setMargin(Sheet.BottomMargin, ((double)paper.getMarginBottom()) / 72);
		//
	}
	@Override
	protected int getCurrentRow()
	{
		return m_printData.getRowIndex();
	}
	@Override
	public void saveValueToContext(int col) {
		MPrintFormatItem item = m_printFormat.getItem(col);
		
		if (item.getExpVar() != null && item.getExpVar().length() > 0 ) {
			Object obj = null;
			if (item.getAD_Column_ID() > 0 && !item.getPrintFormatType().equals(MPrintFormatItem.PRINTFORMATTYPE_Text))
				obj = m_printData.getNode(Integer.valueOf(item.getAD_Column_ID()));

			if (obj != null && obj instanceof PrintDataElement) {
				PrintDataElement pde = (PrintDataElement) obj;
				m_context.put(item.getExpVar(), pde.getValue());
			}
			if (!m_context.containsKey(item.getExpVar()))
				m_context.put(item.getExpVar(), 0.0);
		}		
	}
}
