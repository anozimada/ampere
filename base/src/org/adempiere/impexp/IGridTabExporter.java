package org.adempiere.impexp;

import java.io.File;
import java.util.List;

import org.compiere.model.GridTab;

public interface IGridTabExporter
{
	/**
	 * export gridTab data to file
	 * @param gridTab
	 * @param childs
	 * @param isCurrentRowOnly
	 * @param file
	 */
	public void export(GridTab gridTab, List<GridTab> childs, boolean isCurrentRowOnly, File file, int indxDetailSelected);

	/**
	 * @return file extension
	 */
	public String getFileExtension();

	/**
	 * @return description for file extension
	 */
	public String getFileExtensionLabel();

	/**
	 * @return mime type
	 */
	public String getContentType();

	/**
	 * @return suggested file name
	 */
	public String getSuggestedFileName(GridTab gridTab);
	
	/**
	 * Check a tab (detail tab) is support to export in this exporter
	 * @param gridTab
	 * @return
	 */
	public boolean isExportableTab (GridTab gridTab);
}
