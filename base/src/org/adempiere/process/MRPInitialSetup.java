package org.adempiere.process;

import java.util.logging.Level;

import org.compiere.model.MDocType;
import org.compiere.model.MSequence;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereSystemError;
import org.compiere.util.DB;
import org.compiere.util.Env;

public class MRPInitialSetup extends SvrProcess {

	
	private static final String TEXT_PLANNED_MFG_ORDER = "MRP - Planned Mfg Order";
	private static final String TEXT_CONFIRMED_MFG_ORDER = "MRP - Confirmed Mfg Order";
	private static final String TEXT_MRP_REQUISITION = "MRP Requisition";
	private static final String TEXT_MRP_PURCHASE_ORDER = "MRP Purchase Order";
	private static final String TAG_MFG_SETUP = "Created by Manufacturing Setup. Please update as needed.";
	private boolean isCreateDocTypePlannedOrder = false;
	private boolean isCreateDocTypeConfirmedOrder = false;
	private boolean isCreateDocTypeMRPRequisition = false;
	private boolean isCreateDocTypePurchaseOrder = false;
	private int GL_Category_Default = 0;
	
	private int m_AD_Client_ID;
	private int m_AD_Org_ID;
	
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("isCreateDocTypePlannedOrder"))
				isCreateDocTypePlannedOrder = para[i].getParameterAsBoolean();
			else if (name.equals("isCreateDocTypeConfirmedOrder"))
				isCreateDocTypeConfirmedOrder = para[i].getParameterAsBoolean();
			else if (name.equals("isCreateDocTypeMRPRequisition"))
				isCreateDocTypeMRPRequisition = para[i].getParameterAsBoolean();
			else if (name.equals("isCreateDocTypePurchaseOrder"))
				isCreateDocTypePurchaseOrder = para[i].getParameterAsBoolean();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}


		m_AD_Client_ID = Env.getAD_Client_ID(getCtx());
		m_AD_Org_ID = Env.getAD_Org_ID(getCtx());
	}

	@Override
	protected String doIt() throws Exception {
		GL_Category_Default = getGLCategory_ID("None");
		if (isCreateDocTypePlannedOrder) {
			createDocTypePlannedOrder();
		}
		if (isCreateDocTypeConfirmedOrder) {
			createDocTypeConfirmedOrder();
		}
		if (isCreateDocTypeMRPRequisition) {
			createDocTypeMRPRequisition();
		}
		if (isCreateDocTypePurchaseOrder) {
			createDocTypePurchaseOrder();
		}
		return null;
	}
	
	private void createDocTypePlannedOrder() throws AdempiereSystemError
	{
		MSequence seq = new MSequence(getCtx(), 0, get_TrxName());
		seq.setName(TEXT_PLANNED_MFG_ORDER);
		seq.setDescription(TEXT_PLANNED_MFG_ORDER);
		seq.setAD_Client_ID(m_AD_Client_ID);
		seq.setAD_Org_ID(m_AD_Org_ID);
		seq.saveEx();
		
		MDocType dt = new MDocType(getCtx(), -1, get_TrxName());
		dt.setDocBaseType(MDocType.DOCBASETYPE_ManufacturingPlannedOrder);
		dt.setGL_Category_ID(getGLCategory_ID("Manufacturing"));
		dt.setName(TEXT_PLANNED_MFG_ORDER);
		dt.setDescription(TEXT_PLANNED_MFG_ORDER);
		dt.setPrintName(TEXT_PLANNED_MFG_ORDER);
		dt.setDocumentNote(TAG_MFG_SETUP);
		dt.setDocNoSequence_ID(seq.getAD_Sequence_ID());
		dt.setIsDocNoControlled(true);
		dt.setAD_Client_ID(m_AD_Client_ID);
		dt.setAD_Org_ID(m_AD_Org_ID);
		//TODO - create default print format
		dt.saveEx();
		
		addLog(TEXT_PLANNED_MFG_ORDER + " Created");
	}

	private void createDocTypeConfirmedOrder() throws AdempiereSystemError
	{
		MSequence seq = new MSequence(getCtx(), 0, get_TrxName());
		seq.setName(TEXT_CONFIRMED_MFG_ORDER);
		seq.setDescription(TEXT_CONFIRMED_MFG_ORDER);
		seq.setAD_Client_ID(m_AD_Client_ID);
		seq.setAD_Org_ID(m_AD_Org_ID);
		seq.saveEx();
		
		MDocType dt = new MDocType(getCtx(), -1, get_TrxName());
		dt.setDocBaseType(MDocType.DOCBASETYPE_ManufacturingOrder);
		dt.setGL_Category_ID(getGLCategory_ID("Manufacturing"));
		dt.setName(TEXT_CONFIRMED_MFG_ORDER);
		dt.setDescription(TEXT_CONFIRMED_MFG_ORDER);
		dt.setPrintName(TEXT_CONFIRMED_MFG_ORDER);
		dt.setDocumentNote(TAG_MFG_SETUP);
		dt.setDocNoSequence_ID(seq.getAD_Sequence_ID());
		dt.setIsDocNoControlled(true);
		dt.setAD_Client_ID(m_AD_Client_ID);
		dt.setAD_Org_ID(m_AD_Org_ID);
		//TODO - create default print format
		dt.saveEx();
		
		addLog(TEXT_CONFIRMED_MFG_ORDER + " Created");
	}
	
	private void createDocTypeMRPRequisition() throws AdempiereSystemError
	{
		MSequence seq = new MSequence(getCtx(), 0, get_TrxName());
		seq.setName(TEXT_MRP_REQUISITION);
		seq.setDescription(TEXT_MRP_REQUISITION);
		seq.setAD_Client_ID(m_AD_Client_ID);
		seq.setAD_Org_ID(m_AD_Org_ID);
		seq.saveEx();
		
		MDocType dt = new MDocType(getCtx(), -1, get_TrxName());
		dt.setDocBaseType(MDocType.DOCBASETYPE_ManufacturingPORequisition);
		dt.setGL_Category_ID(getGLCategory_ID("Standard"));
		dt.setName(TEXT_MRP_REQUISITION);
		dt.setDescription(TEXT_MRP_REQUISITION);
		dt.setPrintName(TEXT_MRP_REQUISITION);
		dt.setDocumentNote(TAG_MFG_SETUP);
		dt.setDocNoSequence_ID(seq.getAD_Sequence_ID());
		dt.setIsDocNoControlled(true);
		dt.setAD_Client_ID(m_AD_Client_ID);
		dt.setAD_Org_ID(m_AD_Org_ID);
		//TODO - create default print format
		dt.saveEx();
		
		addLog(TEXT_MRP_REQUISITION + " Created");
	}
	
	private void createDocTypePurchaseOrder() throws AdempiereSystemError
	{
		MSequence seq = new MSequence(getCtx(), 0, get_TrxName());
		seq.setName(TEXT_MRP_PURCHASE_ORDER);
		seq.setDescription(TEXT_MRP_PURCHASE_ORDER);
		seq.setAD_Client_ID(m_AD_Client_ID);
		seq.setAD_Org_ID(m_AD_Org_ID);
		seq.saveEx();
		
		MDocType dt = new MDocType(getCtx(), -1, get_TrxName());
		dt.setDocBaseType(MDocType.DOCBASETYPE_PurchaseOrder);
		dt.setGL_Category_ID(getGLCategory_ID("Standard"));
		dt.setName(TEXT_MRP_PURCHASE_ORDER);
		dt.setDescription(TEXT_MRP_PURCHASE_ORDER);
		dt.setPrintName(TEXT_MRP_PURCHASE_ORDER);
		dt.setDocumentNote(TAG_MFG_SETUP);
		dt.setDocNoSequence_ID(seq.getAD_Sequence_ID());
		dt.setIsDocNoControlled(true);
		dt.setAD_Client_ID(m_AD_Client_ID);
		dt.setAD_Org_ID(m_AD_Org_ID);
		//TODO - create default print format
		dt.saveEx();
		
		addLog(TEXT_MRP_PURCHASE_ORDER + " Created");
	}
	
	private int getGLCategory_ID(String name) throws AdempiereSystemError 
	{
		int GL_Category_ID = 0;
		
		String sql = "SELECT gl_category_id FROM gl_category WHERE name = ? AND ad_client_id = ?";
		GL_Category_ID = DB.getSQLValue(null, sql, name, m_AD_Client_ID );
		if (GL_Category_ID <= 0) {
			if (GL_Category_Default == 0) {
				sql =  "SELECT MIN(gl_category_id) FROM gl_category WHERE isDefault='Y' AND ad_client_id = ?";
				GL_Category_Default = DB.getSQLValue(null, sql, m_AD_Client_ID );
			}
			log.info("No GL Category found for " + name + " will use default category = None");
			GL_Category_ID = GL_Category_Default;
		}

		if (GL_Category_ID <= 0) {
			throw new AdempiereSystemError("Can't set default GL Category.");
		}
		return GL_Category_ID;
	}
	

}
