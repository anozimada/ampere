/**
 * 
 */
package com.astidian.compiere.fa;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.logging.Level;

import org.compiere.model.MAcctSchema;
import org.compiere.model.MAcctSchemaDefault;
import org.compiere.model.MAsset;
import org.compiere.model.MAssetGroup;
import org.compiere.model.MAssetInfoFin;
import org.compiere.model.MClient;
import org.compiere.model.MGAASAssetAcct;
import org.compiere.model.MGAASAssetGroupAcct;
import org.compiere.model.MGAASDepreciationWorkfile;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MOrderLine;
import org.compiere.model.MSequence;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Ashley G Ramdass
 * 
 */
public class Validator implements ModelValidator
{

    /** Logger */
    private static CLogger log = CLogger.getCLogger(Validator.class);
    /** Client */
    private int m_AD_Client_ID = -1;

    @Override
    public void initialize(ModelValidationEngine engine, MClient client)
    {
        if (client != null)
        {
            m_AD_Client_ID = client.getAD_Client_ID();
            log.info(client.toString());
        }
        else
        {
            log.info("Initializing global validator: " + this.toString());
        }
        
        engine.addModelChange(MOrderLine.Table_Name, this);
        engine.addModelChange(MInvoiceLine.Table_Name, this);
        engine.addModelChange(MAsset.Table_Name, this);
        engine.addModelChange(MAssetGroup.Table_Name, this);
    }

    @Override
    public int getAD_Client_ID()
    {
        return m_AD_Client_ID;
    }

    @Override
    public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID)
    {
        return null;
    }

    @Override
    public String modelChange(PO po, int type) throws Exception
    {
        if (po instanceof MAsset)
        {
            if (type == TYPE_NEW || type == TYPE_CHANGE)
            {
                updateGroupDefaults((MAsset) po);
            }
            else if (type == TYPE_AFTER_NEW || type == TYPE_AFTER_CHANGE)
            {
                copyAccountingForAsset((MAsset) po);
                updateDepreciationWorkfile((MAsset) po);
                updateAssetInfoFin((MAsset) po);
            }
            
        }
        else if (po instanceof MOrderLine || po instanceof MInvoiceLine)
        {
//            if ((type == TYPE_NEW || type == TYPE_CHANGE) && po.get_ValueAsBoolean("GAAS_IsAsset"))
//            {
//                po.set_ValueOfColumn("M_Product_ID", 0);
//                po.set_ValueOfColumn("C_Charge_ID", 0);
//            }
        }
        else if (po instanceof MAssetGroup)
        {
            if (type == TYPE_AFTER_NEW || type == TYPE_AFTER_CHANGE)
            {
                copyAccountingForAssetGroup((MAssetGroup) po);
            }
        }
        
        return null;
    }

    @Override
    public String docValidate(PO po, int timing)
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    
    private void copyAccountingForAssetGroup(MAssetGroup assetGroup)
    {
        String sqlCheck = "SELECT GAAS_Asset_Group_Acct_ID "
                + "FROM GAAS_Asset_Group_Acct WHERE A_Asset_Group_ID=?";

        int groupAcctId = DB.getSQLValue(assetGroup.get_TrxName(), sqlCheck,
                assetGroup.getA_Asset_Group_ID());

        if (groupAcctId > 0)
        {
            return;
        }

        String sql = "SELECT ColumnName FROM AD_Table t"
                + " INNER JOIN AD_Column c ON c.AD_Table_ID=t.AD_Table_ID"
                + " WHERE t.TableName=?"
                + " AND EXISTS (SELECT * FROM AD_Table t1"
                + " INNER JOIN AD_Column c1 on c1.AD_Table_ID=t1.AD_Table_ID"
                + " WHERE c1.ColumnName=c.ColumnName AND t1.TableName=?)"
                + " AND c.ColumnName LIKE  'GAAS%'";

        StringBuilder updateSql = new StringBuilder();
        updateSql.append("UPDATE ");
        updateSql.append(MGAASAssetGroupAcct.Table_Name);
        updateSql.append(" SET ");

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, assetGroup.get_TrxName());
            pstmt.setString(1, MGAASAssetGroupAcct.Table_Name);
            pstmt.setString(2, MAcctSchemaDefault.Table_Name);
            rs = pstmt.executeQuery();

            while (rs.next())
            {
                String columnName = rs.getString(1);
                updateSql.append(columnName).append("=");
                updateSql.append("asd.").append(columnName);
                updateSql.append(",");
            }

            updateSql = new StringBuilder(updateSql.substring(0,
                    updateSql.length() - 1));
            updateSql.append(" FROM ").append(MAcctSchemaDefault.Table_Name);
            updateSql.append(" asd ");
            updateSql.append(" WHERE asd.C_AcctSchema_ID=");
            updateSql.append(MGAASAssetGroupAcct.Table_Name).append(
                    ".C_AcctSchema_ID");
            updateSql.append(" AND asd.C_AcctSchema_ID=?");
            updateSql.append(" AND GAAS_Asset_Group_Acct_ID=?");
        }
        catch (Exception ex)
        {
            log.log(Level.SEVERE, "Could not query data", ex);
            throw new IllegalStateException(
                    "Could not copy accounting for Asset");
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        MAcctSchema[] schemas = MAcctSchema.getClientAcctSchema(
                assetGroup.getCtx(), assetGroup.getAD_Client_ID());

        for (MAcctSchema schema : schemas)
        {
            MGAASAssetGroupAcct assetGroupAcct = new MGAASAssetGroupAcct(
                    assetGroup.getCtx(), 0, assetGroup.get_TrxName());
            assetGroupAcct.setC_AcctSchema_ID(schema.get_ID());
            assetGroupAcct.setA_Asset_Group_ID(assetGroup.get_ID());
            assetGroupAcct.setAD_Org_ID(assetGroup.getAD_Org_ID());
            assetGroupAcct.saveEx();

            Object[] params = new Object[2];
            params[0] = schema.get_ID();
            params[1] = assetGroupAcct.get_ID();

            int no = DB.executeUpdate(updateSql.toString(), params, false,
                    assetGroup.get_TrxName());

            if (no > 0)
            {
                log.fine("# of accounts copied: " + no);
            }
            else
            {
                log.warning("#" + no + " - Table="
                        + MGAASAssetGroupAcct.Table_Name + " from "
                        + MAcctSchemaDefault.Table_Name);
            }
        }
    }
    
    // TODO should be added to MSequence
    private String getSequenceValue(MSequence sequence, PO po)
    {
        int next = sequence.getNextID();
        String prefix = sequence.getPrefix();
        String suffix = sequence.getSuffix();
        String decimalPattern = sequence.getDecimalPattern();
        
        StringBuffer doc = new StringBuffer();
        if (prefix != null && prefix.length() > 0)
        {
            doc.append(Env.parseVariable(prefix, po, po.get_TrxName(), false));
        }
        
        if (decimalPattern != null && decimalPattern.length() > 0)
        {
            doc.append(new DecimalFormat(decimalPattern).format(next));
        }
        else
        {
            doc.append(next);
        }
        
        if (suffix != null && suffix.length() > 0)
        {
            doc.append(Env.parseVariable(suffix, po, po.get_TrxName(), false));
        }
        
        return doc.toString();
    }
    
    private void updateGroupDefaults(MAsset asset)
    {
        if (asset.is_new())
        {
            MAssetGroup assetGroup = new MAssetGroup(asset.getCtx(), asset.getA_Asset_Group_ID(), asset.get_TrxName());
            asset.setIsOwned(assetGroup.isOwned());
            asset.setIsDepreciated(assetGroup.isDepreciated());
            int sequenceId = assetGroup.get_ValueAsInt("GAAS_Sequence_ID");

            if (sequenceId > 0)
            {
                MSequence sequence = new MSequence(asset.getCtx(), sequenceId,
                        asset.get_TrxName());
                
                asset.setValue(getSequenceValue(sequence, asset));
                sequence.saveEx();
            }
        }
    }
    
    private void updateDepreciationWorkfile(MAsset asset)
    {
        MGAASDepreciationWorkfile workfile = MGAASDepreciationWorkfile
                .get(asset);

        if (workfile == null)
        {
            workfile = new MGAASDepreciationWorkfile(asset.getCtx(), 0,
                    asset.get_TrxName());
            workfile.setA_Asset_ID(asset.getA_Asset_ID());
            workfile.setAD_Org_ID(asset.getAD_Org_ID());
            workfile.setIsActive(true);
            workfile.setStartPeriod(1);
            int defaultPeriods = DB.getSQLValueEx(asset.get_TrxName(), "SELECT Default_Depn_Periods FROM GAAS_Asset_Group_Acct WHERE A_Asset_Group_ID=?", asset.getA_Asset_Group_ID());
            workfile.setUseLifeMonths(defaultPeriods);
         	workfile.saveEx();
         	
        }
    }

    private void updateAssetInfoFin(MAsset asset)
    {
        MAssetInfoFin assetInfo = MAssetInfoFin.get(asset);

        if (assetInfo == null)
        {
            assetInfo = new MAssetInfoFin(asset.getCtx(), 0,
                    asset.get_TrxName());
            assetInfo.setA_Asset_ID(asset.getA_Asset_ID());
            assetInfo.setAD_Org_ID(asset.getAD_Org_ID());
            assetInfo.setIsActive(true);
            assetInfo.saveEx();
        }
    }
    
    private void copyAccountingForAsset(MAsset asset)
    {
        String sqlCheck = "SELECT GAAS_Asset_Acct_ID "
                + "FROM GAAS_Asset_Acct WHERE A_Asset_ID=? AND IsActive='Y'";

        int assetAcctId = DB.getSQLValue(asset.get_TrxName(), sqlCheck,
                asset.getA_Asset_ID());

        if (assetAcctId > 0)
        {
            return;
        }

        String sql = "SELECT ColumnName FROM AD_Table t"
                + " INNER JOIN AD_Column c ON c.AD_Table_ID=t.AD_Table_ID"
                + " WHERE t.TableName=?"
                + " AND EXISTS (SELECT * FROM AD_Table t1"
                + " INNER JOIN AD_Column c1 on c1.AD_Table_ID=t1.AD_Table_ID"
                + " WHERE c1.ColumnName=c.ColumnName AND t1.TableName=?)"
                + " AND c.IsActive='Y'";

        StringBuilder updateSql = new StringBuilder();
        updateSql.append("UPDATE ");
        updateSql.append(MGAASAssetAcct.Table_Name);
        updateSql.append(" SET ");

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, asset.get_TrxName());
            pstmt.setString(1, MGAASAssetAcct.Table_Name);
            pstmt.setString(2, MGAASAssetGroupAcct.Table_Name);
            rs = pstmt.executeQuery();

            while (rs.next())
            {
                String columnName = rs.getString(1);
                updateSql.append(columnName).append("=");
                updateSql.append("t.").append(columnName);
                updateSql.append(",");
            }

            updateSql = new StringBuilder(updateSql.substring(0,
                    updateSql.length() - 1));
            updateSql.append(" FROM ").append(MGAASAssetGroupAcct.Table_Name);
            updateSql.append(" t ");
            updateSql.append(" WHERE t.GAAS_Asset_Group_Acct_ID=?");
            updateSql.append(" AND GAAS_Asset_Acct_ID=?");
        }
        catch (Exception ex)
        {
            log.log(Level.SEVERE, "Could not query data", ex);
            throw new IllegalStateException(
                    "Could not copy accounting for Asset");
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        String groupAcctSql = "SELECT GAAS_Asset_Group_Acct_ID, C_AcctSchema_ID "
                + "FROM GAAS_Asset_Group_Acct WHERE A_Asset_Group_ID=?";

        try
        {
            pstmt = DB.prepareStatement(groupAcctSql, asset.get_TrxName());
            pstmt.setInt(1, asset.getA_Asset_Group_ID());
            rs = pstmt.executeQuery();

            while (rs.next())
            {
                MGAASAssetAcct assetAcct = new MGAASAssetAcct(asset.getCtx(),
                        0, asset.get_TrxName());
                assetAcct.setC_AcctSchema_ID(rs.getInt(2));
                assetAcct.setA_Asset_ID(asset.get_ID());
                assetAcct.setAD_Org_ID(asset.getAD_Org_ID());
                assetAcct.saveEx();

                Object[] params = new Object[2];
                params[0] = rs.getInt(1);
                params[1] = assetAcct.get_ID();

                int no = DB.executeUpdate(updateSql.toString(), params, false,
                        asset.get_TrxName());

                if (no > 0)
                {
                    log.fine("# of accounts copied: " + no);
                }
                else
                {
                    log.warning("#" + no + " - Table="
                            + MGAASAssetAcct.Table_Name + " from "
                            + MGAASAssetGroupAcct.Table_Name);
                }
            }
        }
        catch (Exception ex)
        {
            log.log(Level.SEVERE, "Could not query data", ex);
            throw new IllegalStateException(
                    "Could not copy accounting for Asset");
        }
        finally
        {
            DB.close(rs, pstmt);
        }
    }
}
