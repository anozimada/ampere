/**
 * 
 */
package com.astidian.compiere.fa;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;

import org.compiere.model.MAsset;
import org.compiere.model.MGAASAssetJournal;
import org.compiere.model.MGAASAssetJournalLine;
import org.compiere.model.X_GAAS_AssetJournal;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;


/**
 * @author Ashley G Ramdass
 * 
 */
public class ImportAsset extends SvrProcess
{
    private static final CLogger logger = CLogger.getCLogger(ImportAsset.class);
    private boolean p_isDeleteImported;
    private HashMap<String, String> mandatoryColumns = new HashMap<String, String>();
    private HashMap<String, String> infoFinColumns = new HashMap<String, String>();
    private HashMap<String, String> infoInsColumns = new HashMap<String, String>();

    private String assetColumnSql = "SELECT c.ColumnName FROM AD_Column c"
            + " INNER JOIN AD_Table t ON c.AD_Table_ID=t.AD_Table_ID"
            + " WHERE t.TableName='I_GAAS_Asset'"
            + " AND EXISTS (SELECT * FROM AD_Column"
            + " c1 WHERE c1.AD_Table_ID=539 AND c1.ColumnName=c.ColumnName)"
            + " AND c.ColumnName NOT IN ('Created', 'CreatedBy',"
            + " 'IsActive', 'Processed', 'Processing', 'Updated', 'UpdatedBy')";

    private String workfileColumnSql = "SELECT c.ColumnName FROM AD_Column c"
            + " INNER JOIN AD_Table t ON c.AD_Table_ID=t.AD_Table_ID"
            + " WHERE t.TableName='I_GAAS_Asset'"
            + " AND EXISTS (SELECT * FROM AD_Column"
            + " c1 INNER JOIN AD_Table t1 ON c1.AD_Table_ID=t1.AD_Table_ID"
            + " WHERE t1.TableName='GAAS_Depreciation_Workfile' AND c1.ColumnName=c.ColumnName AND c1.ColumnSql IS NULL)"
            + " AND c.ColumnName NOT IN ('Created', 'CreatedBy',"
            + " 'IsActive', 'Processed', 'Processing', 'Updated', 'UpdatedBy')";

    @Override
    protected void prepare()
    {
        ProcessInfoParameter[] para = getParameter();
        for (int i = 0; i < para.length; i++)
        {
            String name = para[i].getParameterName();
            if (para[i].getParameter() == null)
            {
                ;
            }
            else if (name.equals("IsDeleteImported"))
            {
                p_isDeleteImported = "Y"
                        .equals((String) para[i].getParameter());
                logger.info("Delete info lines: " + p_isDeleteImported);
            }
            else
            {
                log.warning("Unknown parameter: " + name);
            }
        }

        mandatoryColumns.put("Name", "Name");
        mandatoryColumns.put("Value", "Search Key");
        mandatoryColumns.put("A_Asset_Group_ID", "Asset Group");

        infoFinColumns.put("BPName", "BPName");
        infoFinColumns.put("InvoiceNo", "InvoiceNo");
        infoFinColumns.put("Fin_Comment", "TextMsg");
        infoInsColumns.put("A_Ins_Value", "A_Ins_Value");
    }

    private void updateImportRecordsColumn(String columnName)
    {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE I_GAAS_Asset ");
        sql.append("SET ").append(columnName).append("=");
        sql.append("(SELECT a.").append(columnName).append(" FROM A_Asset a ");
        sql.append("WHERE a.A_Asset_ID=I_GAAS_Asset.A_Asset_ID) ");
        sql.append("WHERE I_IsImported<>'Y' AND IsActive='Y' ");
        sql.append("AND ").append(columnName).append(" IS NULL ");
        sql.append("AND AD_Client_ID=?");

        int count = DB.executeUpdate(sql.toString(), getAD_Client_ID(),
                get_TrxName());

        logger.info("Count: " + count + ", SQL: " + sql.toString());

        if (count < 0)
        {
            logger.severe("SQL: " + sql.toString());
            throw new IllegalStateException(
                    "Could not update Import Asset record");
        }
    }

    private int updateImportRecords()
    {
        String sql = "UPDATE I_GAAS_Asset SET"
                + " A_Asset_ID=(SELECT a.A_Asset_ID"
                + " FROM A_Asset a WHERE a.Value=I_GAAS_Asset.Value)"
                + " WHERE AD_Client_ID=? AND I_IsImported<>'Y' AND IsActive='Y'";

        int count = DB.executeUpdate(sql, getAD_Client_ID(), get_TrxName());

        logger.info("Assets updated: " + count);

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(assetColumnSql, get_TrxName());
            rs = pstmt.executeQuery();

            while (rs.next())
            {
                updateImportRecordsColumn(rs.getString(1));
            }
        }
        catch (SQLException ex)
        {
            logger.log(Level.SEVERE, "Could not query columns", ex);
            throw new IllegalStateException("Could not query columms");
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        return count;
    }

    private void deleteImportedRecords()
    {
        String sql = "DELETE FROM I_GAAS_Asset WHERE AD_Client_ID=?"
                + " AND I_IsImported='Y'";

        int count = DB.executeUpdate(sql, getAD_Client_ID(), get_TrxName());
        logger.info("Deleted #" + count);

        if (count < 0)
        {
            logger.severe("SQL: " + sql);
            throw new IllegalStateException(
                    "Could not delete old imported records");
        }

    }

    private boolean validateRecords()
    {
        int invalidRecordsCount = 0;
        String updateSql = "UPDATE I_GAAS_Asset SET I_ErrorMsg=NULL"
                + " WHERE AD_Client_ID=? AND IsActive='Y' AND I_IsImported<>'Y'";

        int updateCount = DB.executeUpdate(updateSql, getAD_Client_ID(),
                get_TrxName());

        if (updateCount < 0)
        {
            logger.severe("SQL: " + updateSql);
            throw new IllegalStateException("Could not reset Error Msg");
        }

        for (String column : mandatoryColumns.keySet())
        {
            String sql = "UPDATE I_GAAS_Asset SET"
                    + " I_ErrorMsg=(CASE WHEN I_ErrorMsg IS NOT NULL"
                    + " THEN I_ErrorMsg || ', " + mandatoryColumns.get(column)
                    + "' ELSE 'Invalid Field: " + column + "' END)"
                    + " WHERE AD_Client_ID=? AND IsActive='Y'"
                    + " AND I_IsImported<>'Y' AND " + column + " IS NULL";

            int count = DB.executeUpdate(sql, getAD_Client_ID(), get_TrxName());
            if (count < 0)
            {
                logger.severe("SQL: " + sql);
                throw new IllegalStateException("Could not set Error Msg");
            }

            invalidRecordsCount += count;
        }

        return (invalidRecordsCount == 0);
    }

    private void updatePeriods()
    {
      
		String sql = "UPDATE I_GAAS_Asset SET"
				+ " UseLifeMonths = DepreciationPeriods "
				+ " WHERE AD_Client_ID=? AND I_IsImported<>'Y'"
				+ " AND IsActive='Y' AND DepreciationPeriods IS NOT NULL  ";

        int count = DB.executeUpdate(sql, getAD_Client_ID(), get_TrxName());
        logger.info("Use life information updated: " + count);

        if (count < 0)
        {
            logger.severe("SQL: " + sql.toString());
            throw new IllegalStateException(
                    "Could not update Use life information");
        }

        sql = "UPDATE I_GAAS_Asset SET"
                + " StartPeriod=(CASE WHEN (DepreciatedPeriods + 1) >="
                + " DepreciationPeriods THEN DepreciationPeriods"
                + " ELSE (DepreciatedPeriods + 1) END)"
                + " WHERE AD_Client_ID=? AND I_IsImported<>'Y'"
                + " AND IsActive='Y' AND DepreciationPeriods IS NOT NULL AND DepreciationPeriods <> 0 " 
                + " AND DepreciatedPeriods IS NOT NULL";

        count = DB.executeUpdate(sql, getAD_Client_ID(), get_TrxName());
        logger.info("Start Period information updated: " + count);

        if (count < 0)
        {
            logger.severe("SQL: " + sql.toString());
            throw new IllegalStateException("Could not update Start Period");
        }

        sql = "UPDATE I_GAAS_Asset SET"
                + " AssetDepreciationDate=(LastDepreciationDate + INTERVAL '1 month')"
                + " WHERE AD_Client_ID=? AND I_IsImported<>'Y'"
                + " AND IsActive='Y' AND LastDepreciationDate IS NOT NULL";

        count = DB.executeUpdate(sql, getAD_Client_ID(), get_TrxName());
        logger.info("Depreciation date information updated: " + count);

        if (count < 0)
        {
            logger.severe("SQL: " + sql.toString());
            throw new IllegalStateException(
                    "Could not update depreciation date");
        }
    }

    private void updateAssetGroup()
    {
        String sql = "UPDATE I_GAAS_Asset"
                + " SET A_Asset_Group_ID=(SELECT ag.A_Asset_Group_ID"
                + " FROM A_Asset_Group ag"
                + " WHERE ag.Name=I_GAAS_Asset.A_Asset_Group_Value"
                + " AND ag.AD_Client_ID=I_GAAS_Asset.AD_Client_ID) "
                + " WHERE AD_Client_ID=? AND I_IsImported<>'Y' AND I_GAAS_Asset.A_Asset_Group_ID IS NULL"
                + " AND IsActive='Y'";

        int count = DB.executeUpdate(sql, getAD_Client_ID(), get_TrxName());
        logger.info("Asset group updated: " + count);

        if (count < 0)
        {
            logger.severe("SQL: " + sql.toString());
            throw new IllegalStateException("Could not update Asset group");
        }
    }

    private void updateDepartment()
    {
        String sql = "UPDATE I_GAAS_Asset"
                + " SET GAAS_Department_ID=(SELECT o.AD_Org_ID"
                + " FROM AD_Org o"
                + " WHERE UPPER(o.Value)=UPPER(I_GAAS_Asset.GAAS_Department_Key)"
                + " AND o.AD_Client_ID=I_GAAS_Asset.AD_Client_ID)"
                + " WHERE AD_Client_ID=? AND I_IsImported<>'Y' AND IsActive='Y' AND I_GAAS_Asset.GAAS_Department_ID IS NULL";

        int count = DB.executeUpdate(sql, getAD_Client_ID(), get_TrxName());
        logger.info("Department updated: " + count);

        if (count < 0)
        {
            logger.severe("SQL: " + sql.toString());
            throw new IllegalStateException("Could not update department");
        }
    }

    private void setAssetId(int importRecordId, int assetId)
    {
        String sql = "UPDATE I_GAAS_Asset SET A_Asset_ID=? WHERE I_GAAS_Asset_ID=?";
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(assetId);
        params.add(importRecordId);

        int count = DB.executeUpdate(sql, params.toArray(), false, get_TrxName());

        if (count != 1)
        {
            logger.severe("SQL: " + sql.toString());
            throw new IllegalStateException(
                    "Could not update Asset on the import record");
        }
    }

    private void updateAssetColumnInformation(String column)
    {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE A_Asset SET ");
        sql.append(column);
        sql.append("=(SELECT ");
        sql.append(column);
        sql.append(" FROM I_GAAS_Asset iga WHERE");
        sql.append(" iga.A_Asset_ID=A_Asset.A_Asset_ID) WHERE AD_Client_ID=? AND");
        sql.append(" EXISTS (SELECT * FROM I_GAAS_Asset iga");
        sql.append(" WHERE iga.A_Asset_ID=A_Asset.A_Asset_ID AND iga.");
        sql.append(column).append(" IS NOT NULL AND iga.I_IsImported<>'Y')");

        int count = DB.executeUpdate(sql.toString(), getAD_Client_ID(),
                get_TrxName());

        if (count < 0)
        {
            logger.severe("SQL: " + sql.toString());
            throw new IllegalStateException("Could not update Asset");
        }
    }
    
    private void updateAssetProcessing(boolean processing)
    {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        String sql = "SELECT A_Asset_ID FROM I_GAAS_Asset WHERE AD_Client_ID=?" +
                " AND I_IsImported<>'Y' AND A_Asset_ID IS NOT NULL";
        
        try
        {
            pstmt = DB.prepareStatement(sql, get_TrxName());
            pstmt.setInt(1, getAD_Client_ID());
            rs = pstmt.executeQuery();
            while (rs.next())
            {
                MAsset asset = new MAsset(getCtx(), rs.getInt(1), get_TrxName());
                asset.set_ValueOfColumn("Processing", processing);
                asset.saveEx();
            }
        }
        catch (SQLException ex)
        {
            logger.log(Level.SEVERE, "Could not query assets", ex);
        }
        finally
        {
            DB.close(rs, pstmt);
        }
    }
            
            

    private void updateAssetInformation()
    {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(assetColumnSql, get_TrxName());
            rs = pstmt.executeQuery();

            while (rs.next())
            {
                updateAssetColumnInformation(rs.getString(1));
            }
        }
        catch (SQLException ex)
        {
            logger.log(Level.SEVERE, "Could not query columns", ex);
            throw new IllegalStateException("Could not query columms");
        }
        finally
        {
            DB.close(rs, pstmt);
        }
    }

    private void updateTableColumn(String table, String fromColumn,
            String toColumn)
    {
    	int count=0;
    	if(table.equals("GAAS_Depreciation_Workfile") && fromColumn.equals("UseLifeMonths"))
    	{
    		String sql="UPDATE GAAS_Depreciation_Workfile SET UseLifeMonths=(SELECT UseLifeMonths FROM I_GAAS_Asset iga " +
    				"WHERE iga.A_Asset_ID=GAAS_Depreciation_Workfile.A_Asset_ID) WHERE AD_Client_ID=? " +
    				"AND EXISTS (SELECT * FROM I_GAAS_Asset iga WHERE iga.A_Asset_ID=GAAS_Depreciation_Workfile.A_Asset_ID" +
    				" AND iga.UseLifeMonths IS NOT NULL AND iga.UseLifeMonths <> 0  AND iga.I_IsImported<>'Y')";
    		
			count = DB.executeUpdate(sql.toString(), getAD_Client_ID(),
					get_TrxName());
			return;
    	}
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ").append(table).append(" SET ");
        sql.append(toColumn);
        sql.append("=(SELECT ");
        sql.append(fromColumn);
        sql.append(" FROM I_GAAS_Asset iga WHERE");
        sql.append(" iga.A_Asset_ID=").append(table)
                .append(".A_Asset_ID) WHERE AD_Client_ID=? AND");
        sql.append(" EXISTS (SELECT * FROM I_GAAS_Asset iga");
        sql.append(" WHERE iga.A_Asset_ID=").append(table)
                .append(".A_Asset_ID AND iga.");
        sql.append(fromColumn)
                .append(" IS NOT NULL AND iga.I_IsImported<>'Y')");

        count = DB.executeUpdate(sql.toString(), getAD_Client_ID(),
                get_TrxName());

        if (count < 0)
        {
            logger.severe("SQL: " + sql.toString());
            throw new IllegalStateException("Could not update table:" + table
                    + ", column: " + toColumn);
        }
    }

    private void updateWorkfileInformation()
    {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(workfileColumnSql, get_TrxName());
            rs = pstmt.executeQuery();

            while (rs.next())
            {
                updateTableColumn("GAAS_Depreciation_Workfile", rs.getString(1),
                        rs.getString(1));
            }
        }
        catch (SQLException ex)
        {
            logger.log(Level.SEVERE, "Could not query columns", ex);
            throw new IllegalStateException("Could not query columms");
        }
        finally
        {
            DB.close(rs, pstmt);
        }
    }

    private void updateAccountInformation()
    {

    	String sql="UPDATE GAAS_Asset_Acct SET DepreciationRate=(SELECT iga.DepreciationRate FROM I_GAAS_Asset iga " +
    			"WHERE iga.A_Asset_ID=GAAS_Asset_Acct.A_Asset_ID ) WHERE AD_Client_ID=? " +
    			"AND EXISTS (SELECT * FROM I_GAAS_Asset iga WHERE iga.A_Asset_ID=GAAS_Asset_Acct.A_Asset_ID" +
    			" AND iga.DepreciationRate IS NOT NULL AND iga.DepreciationRate > 0   AND iga.I_IsImported<>'Y')";
    	DB.executeUpdate(sql.toString(), getAD_Client_ID(),
    			get_TrxName());


    }

    private void updateFinancialInformation()
    {
        for (String column : infoFinColumns.keySet())
        {
            updateTableColumn("GAAS_Asset_Info_Fin", column,
                    infoFinColumns.get(column));
        }
    }

    private void updateInsuranceInformation()
    {
        for (String column : infoInsColumns.keySet())
        {
            updateTableColumn("GAAS_Asset_Info_Ins", column,
                    infoInsColumns.get(column));
        }
    }

    private void createAssets()
    {
        String sql = "SELECT I_GAAS_Asset_ID, Name, Value, A_Asset_Group_ID, IsOwned "
                + " FROM I_GAAS_Asset WHERE AD_Client_ID=? AND IsActive='Y'"
                + " AND I_IsImported<>'Y' AND A_Asset_ID IS NULL";

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, get_TrxName());
            pstmt.setInt(1, getAD_Client_ID());
            rs = pstmt.executeQuery();

            while (rs.next())
            {
                MAsset asset = new MAsset(getCtx(), 0, get_TrxName());
                asset.setName(rs.getString(2));
                asset.setValue(rs.getString(3));
                asset.setA_Asset_Group_ID(rs.getInt(4));
                String isOwned=rs.getString(5);
                if(isOwned.equals("Y"))
                	asset.set_ValueOfColumn("GAAS_Asset_Type", "FA"); // Fixed Asset
                asset.saveEx();
                setAssetId(rs.getInt(1), asset.get_ID());
            }
        }
        catch (SQLException ex)
        {
            logger.log(Level.SEVERE, "Could not create assets", ex);
            throw new IllegalStateException("Could not create assets");
        }
        finally
        {
            DB.close(rs, pstmt);
        }

    }
    
    private void createDepreciations()
    {
    	String sql = "SELECT A_Asset_ID ,IsOwned "
                + " FROM I_GAAS_Asset WHERE AD_Client_ID=? AND IsActive='Y'"
                + " AND I_IsImported<>'Y' AND A_Asset_ID IS NOT NULL";
    	
    	 PreparedStatement pstmt = null;
         ResultSet rs = null;
         int assetID=0;
         String isOwned=null;
         try
         {
             pstmt = DB.prepareStatement(sql, get_TrxName());
             pstmt.setInt(1, getAD_Client_ID());
             rs = pstmt.executeQuery();

             while (rs.next())
             {
            	 assetID=rs.getInt(1);
            	 isOwned=rs.getString(2);
                 if(isOwned.equals("N"))
            		 continue;
            	 else
            		createDepreciation(assetID);
            	 
             }
         }
         catch (SQLException ex)
         {
             logger.log(Level.SEVERE, "Could not create  depreciations.", ex);
         }
         finally
         {
             DB.close(rs, pstmt);
         }    	
    }
    
    private void createDepreciation(int assetID) 
    {

        if (assetID <= 0)
        {
            throw new IllegalStateException("No Asset");
        }
        
        try 
        {
			Depreciation.updateDepreciationPeriods(getCtx(), assetID, null);
		}
        catch (Exception e) 
        {
			
			logger.log(Level.SEVERE, "Could not update periods", e);
		}

        String sql = "SELECT aa.GAAS_Asset_Acct_ID, aa.GAAS_Depreciation_Type"
                + " FROM GAAS_Asset_Acct aa "
                + " WHERE aa.A_Asset_ID=? AND aa.IsActive='Y'";

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, get_TrxName());
            pstmt.setInt(1, assetID);
            rs = pstmt.executeQuery();

            while (rs.next())
            {
                int assetAcctId = rs.getInt(1);
                String depreciationType = rs.getString(2);
                Depreciation depreciation;

                if ("SL".equals(depreciationType))
                {
                    depreciation = new SLDepreciation(getCtx(), assetID,
                            get_TrxName());
                }
                else if ("RB".equals(depreciationType))
                {
                    depreciation = new RBDepreciation(getCtx(), assetID,
                            get_TrxName());
                }
                else
                {
                    throw new IllegalStateException(
                            "Depreciation type not yet implemented");
                }
                depreciation.updateDepreciationExpenses(assetAcctId);
            }
        }
        catch (SQLException ex)
        {
            logger.log(Level.SEVERE, "Could not asset account information", ex);
        }
        finally
        {
            DB.close(rs, pstmt);
        }

    }
    
    private void createAssetJournal()
    {
    	
    	String sql = "SELECT A_Asset_ID , AssetCost , InitialAccumulatedDepr , A_Asset_Group_ID , Name, I_GAAS_Asset_ID , IsOwned, AD_Org_ID "
                + " FROM I_GAAS_Asset WHERE AD_Client_ID=? AND IsActive='Y'"
                + " AND I_IsImported<>'Y' AND A_Asset_ID IS NOT NULL";
    	
    	 PreparedStatement pstmt = null;
         ResultSet rs = null;
         int assetID=0;
         BigDecimal assetCost=BigDecimal.ZERO;
         BigDecimal initialAccumulatedDepr=BigDecimal.ZERO;
         int assetGrpID=0;
         String discription=null;
         String isOwned=null;
         try
         {
			MGAASAssetJournal assetJournal = new MGAASAssetJournal(getCtx(), 0,
					get_TrxName());
			assetJournal.setEntryType(X_GAAS_AssetJournal.ENTRYTYPE_New);
              
        	 pstmt = DB.prepareStatement(sql, get_TrxName());
             pstmt.setInt(1, getAD_Client_ID());
             rs = pstmt.executeQuery();
             
			while (rs.next())
			{

				assetID = rs.getInt(1);
				assetCost = rs.getBigDecimal(2);
				initialAccumulatedDepr = rs.getBigDecimal(3);
				assetGrpID = rs.getInt(4);
				discription = rs.getString(5);
				isOwned=rs.getString("IsOwned");
				if(isOwned.equals("N"))
            		 continue;
				
				assetJournal.setAD_Org_ID(rs.getInt("AD_Org_ID"));
				assetJournal.saveEx();
				MGAASAssetJournalLine assetJournalLine = new MGAASAssetJournalLine(getCtx(),
						0, get_TrxName());
				assetJournalLine.setParentAsset_ID(assetID);
				assetJournalLine.setAccumulatedDepreciationAmt(initialAccumulatedDepr);
				assetJournalLine.setCostAmt(assetCost);
				assetJournalLine.setGAAS_AssetJournal_ID(assetJournal.getGAAS_AssetJournal_ID());
				assetJournalLine.setA_Asset_ID(assetID);
				assetJournalLine.setA_Asset_Group_ID(assetGrpID);
				assetJournalLine.setDescription(discription);
				assetJournalLine.saveEx();
				setAssetJournalLine(rs.getInt(6), assetJournalLine.getGAAS_AssetJournalLine_ID());
				
			
			}
			if (assetJournal.getGAAS_AssetJournal_ID() != 0) 
			{
				try 
				{
					assetJournal.processIt(DocAction.STATUS_Completed);
				} 
				catch (Exception e) 
				{
				
					logger.log(Level.SEVERE, "Could not create journal", e);
				}
				assetJournal.saveEx();
			}
         }
         catch (SQLException ex)
         {
             logger.log(Level.SEVERE, "Could not create journal", ex);
            
         }
         finally
         {
             DB.close(rs, pstmt);
         }   	
    	
    }
    
    private void setAssetJournalLine(int importRecordId, int assetJournalLineId)
    {
        String sql = "UPDATE I_GAAS_Asset SET GAAS_AssetJournalLine_ID=? WHERE I_GAAS_Asset_ID=?";
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(assetJournalLineId);
        params.add(importRecordId);

        int count = DB.executeUpdate(sql, params.toArray(), false, get_TrxName());

        if (count != 1)
        {
            logger.severe("SQL: " + sql.toString());
            throw new IllegalStateException(
                    "Could not update Asset Journal Line on the import record");
        }
    }

    @Override
    protected String doIt() throws Exception
    {
        if (p_isDeleteImported)
        {
            deleteImportedRecords();
        }

        // Check for duplicates
        String sql = "SELECT COUNT(Value) FROM I_GAAS_Asset"
                + " WHERE AD_Client_ID=? GROUP BY Value HAVING COUNT(Value) > 1";

        int count = DB.getSQLValue(get_TrxName(), sql, getAD_Client_ID());

        if (count > 0)
        {
            throw new IllegalStateException(
                    "More than 1 records found with same value, try delete old imported records");
        }

        updatePeriods();
        int updateCount = updateImportRecords();
        updateAssetGroup();
        updateDepartment();

        if (!validateRecords())
        {
            return "Invalid records found!!!";
        }

        createAssets();
        updateAssetProcessing(true);
        createAssetJournal();
        updateAssetInformation();
        updateWorkfileInformation();
        updateAccountInformation();
        //updateFinancialInformation();
        //updateInsuranceInformation();        
        createDepreciations();
        updateAssetProcessing(false);

        sql = "UPDATE I_GAAS_Asset SET I_IsImported='Y', Processed='Y'"
                + " WHERE AD_Client_ID=? AND I_IsImported<>'Y'"
                + " AND A_Asset_ID IS NOT NULL";

        count = DB.executeUpdate(sql, getAD_Client_ID(), get_TrxName());

        return "Assets Imported: " + count + ", Updated: " + updateCount;
    }

}
