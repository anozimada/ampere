/*******************************************************************************
 * (C) Astidian Systems 2012                                                   *
 *                                                                             *
 ******************************************************************************/
package com.astidian.compiere.fa;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MAcctSchema;
import org.compiere.model.MAsset;
import org.compiere.model.MCurrency;
import org.compiere.model.MGAASAssetJournal;
import org.compiere.model.MGAASDepreciationExpense;
import org.compiere.model.MGAASDepreciationWorkfile;
import org.compiere.model.MPeriod;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Ashley G Ramdass
 * 
 */
public abstract class Depreciation
{
    private static final CLogger logger = CLogger
            .getCLogger(Depreciation.class);

    protected Properties ctx;
    protected String trxName;
    protected MAsset asset;
    protected int assetId;
    private MGAASDepreciationWorkfile workfile;

    public Depreciation(Properties ctx, int assetId, String trxName)
    {
        this.ctx = ctx;
        this.assetId = assetId;
        this.trxName = trxName;
        workfile = MGAASDepreciationWorkfile.get(ctx, assetId, trxName);

        if (getAssetCost() == null || getAssetCost().compareTo(Env.ZERO) <= 0)
        {
            throw new IllegalStateException("Invalid Asset Cost: "
                    + getAssetCost());
        }

        if (getAssetDepreciationDate() == null)
        {
            throw new IllegalStateException("Invalid Asset Depreciation Date");
        }
    }

    protected MGAASAssetJournal[] getLinkedAssetJournals(int assetAcctId)
    {
        String sql = "SELECT ajl.GAAS_AssetJournal_ID"
                + " FROM GAAS_AssetJournalLine ajl"
                + " INNER JOIN GAAS_Depreciation_Expense de"
                + " ON de.GAAS_Depreciation_Expense_ID=ajl.GAAS_Depreciation_Expense_ID"
                + " WHERE de.GAAS_Asset_Acct_ID=?";

        ArrayList<MGAASAssetJournal> journals = new ArrayList<MGAASAssetJournal>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, getTrxName());
            pstmt.setInt(1, assetAcctId);
            rs = pstmt.executeQuery();

            while (rs.next())
            {
                MGAASAssetJournal assetJournal = new MGAASAssetJournal(
                        getCtx(), rs.getInt(1), getTrxName());
                journals.add(assetJournal);
            }
        }
        catch (Exception ex)
        {
            logger.log(Level.SEVERE, "Could not query data", ex);
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        MGAASAssetJournal[] lines = new MGAASAssetJournal[journals.size()];
        journals.toArray(lines);
        return lines;
    }

    protected void deleteNotProcessedDepreciations(int assetAcctId)
    {
        String sql = "DELETE FROM GAAS_Depreciation_Expense "
                + "WHERE GAAS_Asset_Acct_ID=? AND Processed='N' AND PeriodNo <> 0";

        ArrayList<Object> params = new ArrayList<Object>();
        params.add(assetAcctId);

        int no = DB.executeUpdate(sql, params.toArray(), false, trxName);
        logger.fine("Deleted not processed depreciations #" + no);
    }

    protected void deleteDeprecatedDepreciations()
    {
        String sql = "DELETE FROM GAAS_Depreciation_Expense "
                + "WHERE A_Asset_ID=? AND Processed='N' AND "
                + "EXISTS (SELECT * FROM GAAS_Asset_Acct aa "
                + "WHERE aa.GAAS_Asset_Acct_ID=GAAS_Depreciation_Expense.GAAS_Asset_Acct_ID "
                + "AND aa.IsActive<>'Y')";

        ArrayList<Object> params = new ArrayList<Object>();
        params.add(assetId);

        int no = DB.executeUpdate(sql, params.toArray(), false, trxName);
        logger.fine("Deleted deprecated depreciations #" + no);
    }

    public abstract void updateDepreciationExpenses(int assetAcctId);

    public int getPrecision(int assetAcctId)
    {
        String sql = "SELECT C_AcctSchema_ID FROM GAAS_Asset_Acct "
                + "WHERE GAAS_Asset_Acct_ID=?";

        int acctSchemaId = DB.getSQLValue(getTrxName(), sql, assetAcctId);

        MAcctSchema acctSchema = MAcctSchema.get(ctx, acctSchemaId);
        int currencyId = acctSchema.getC_Currency_ID();
        return MCurrency.getStdPrecision(ctx, currencyId);
    }

    public BigDecimal getDepreciationRate(int assetAcctId)
    {
        String sql = "SELECT DepreciationRate "
                + "FROM GAAS_Asset_Acct WHERE GAAS_Asset_Acct_ID=?";

        BigDecimal rate = DB.getSQLValueBD(getTrxName(), sql, assetAcctId);

        if (rate == null || rate.compareTo(Env.ZERO) < 0
                || rate.compareTo(Env.ONE) >= 0)
        {
            throw new IllegalStateException("Depreciation rate is invalid");
        }

        return rate;
    }

    public static void updateDepreciationPeriods(Properties ctx, int assetId,
            String trxName) throws Exception
    {
        String sql = "SELECT GAAS_Depreciation_Expense_ID FROM "
                + "GAAS_Depreciation_Expense WHERE A_Asset_ID=? "
                + "AND C_Period_ID IS NULL";

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, trxName);
            pstmt.setInt(1, assetId);
            rs = pstmt.executeQuery();

            while (rs.next())
            {
                int expenseId = rs.getInt(1);
                MGAASDepreciationExpense expense = new MGAASDepreciationExpense(
                        ctx, expenseId, null);
                MPeriod acctPeriod = MPeriod.get(ctx, expense.getAssetDepreciationDate(), 
                		expense.getAD_Org_ID());
                if (acctPeriod != null)
                {
                    expense.setC_Period_ID(acctPeriod.getC_Period_ID());
                    expense.saveEx();
                }
            }
        }
        finally
        {
            DB.close(rs, pstmt);
        }
    }

    public int getProcessedDepreciationsCount(Properties ctx, int assetId,
            int assetAcctId, String trxName)
    {
        int count = 0;

        String sql = "SELECT COUNT(*)"
                + " FROM GAAS_Depreciation_Expense WHERE A_Asset_ID=?"
//                + " AND GAAS_Asset_Acct_ID=?"
				+ " AND PeriodNo <> 0"
                + " AND Processed='Y'";

        count = DB.getSQLValue(trxName, sql, assetId);

        if (count < 0)
        {
            logger.severe("Could not get number of depreciation periods");
            throw new IllegalStateException(
                    "Could not get number of depreciation periods");
        }

        return count;
    }

    public BigDecimal getAccumulatedDeprAmt(Properties ctx, int assetId,
            int assetAcctId, String trxName)
    {
        BigDecimal retAmount = Env.ZERO;

        String sql = "SELECT COALESCE(SUM(DepreciationExpenseAmt),0)"
                + " FROM GAAS_Depreciation_Expense WHERE A_Asset_ID=?"
                //+ " AND GAAS_Asset_Acct_ID=?"
            	+ " AND PeriodNo <> 0"
                + " AND Processed='Y'";

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, trxName);
            pstmt.setInt(1, assetId);
//            pstmt.setInt(2, assetAcctId);
            rs = pstmt.executeQuery();

            if (rs.next())
            {
                retAmount = rs.getBigDecimal(1);
            }
        }
        catch (Exception ex)
        {
            logger.log(Level.SEVERE,
                    "Could not get Accumulated Depreciation Amount", ex);
            throw new IllegalStateException(
                    "Could not get Accumulated Depreciation Amount");
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        return retAmount;
    }

    /**
     * @return the ctx
     */
    public Properties getCtx()
    {
        return ctx;
    }

    /**
     * @return the trxName
     */
    public String getTrxName()
    {
        return trxName;
    }

    /**
     * @return the assetId
     */
    public int getAssetId()
    {
        return assetId;
    }

    public MAsset getAsset()
    {
        if (asset == null)
        {
            asset = new MAsset(this.ctx, getAssetId(), trxName);
        }

        return asset;
    }

    public MGAASDepreciationWorkfile getWorkfile()
    {
        return workfile;
    }

    public BigDecimal getAssetCost()
    {
        return workfile.getAssetCost();
    }

    public int getUselifeYears()
    {
        return workfile.getUseLifeYears();
    }

    public int getTotalUselifeMonths()
    {
        int totalLifeMonths = (12 * workfile.getUseLifeYears());
        totalLifeMonths += workfile.getUseLifeMonths();

        return totalLifeMonths;
    }

    public Timestamp getAssetDepreciationDate()
    {
        return workfile.getAssetDepreciationDate();
    }

    public BigDecimal getSalvageValue()
    {
        return workfile.getSalvageValue();
    }

    public BigDecimal getInitialAccumulatedDepr()
    {
        return workfile.getInitialAccumulatedDepr();
    }

    public int getStartPeriod()
    {
        int startPeriod = workfile.getStartPeriod();

        if (startPeriod <= 0)
        {
            logger.severe("Invalid starting period: " + startPeriod
                    + " for asset: " + getAsset().getValue());
            throw new IllegalStateException("Invalid starting period: "
                    + startPeriod + " for asset: " + getAsset().getValue());
        }

        return startPeriod;
    }
}
