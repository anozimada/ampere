/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for GAAS_AssetJournalLine
 *  @author Adempiere (generated) 
 *  @version $
{
project.version}

 */
public interface I_GAAS_AssetJournalLine 
{

    /** TableName=GAAS_AssetJournalLine */
    public static final String Table_Name = "GAAS_AssetJournalLine";

    /** AD_Table_ID=53576 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name A_Asset_Group_ID */
    public static final String COLUMNNAME_A_Asset_Group_ID = "A_Asset_Group_ID";

	/** Set Asset Group.
	  * Group of Assets
	  */
	public void setA_Asset_Group_ID (int A_Asset_Group_ID);

	/** Get Asset Group.
	  * Group of Assets
	  */
	public int getA_Asset_Group_ID();

	public I_A_Asset_Group getA_Asset_Group() throws RuntimeException;

    /** Column name A_Asset_ID */
    public static final String COLUMNNAME_A_Asset_ID = "A_Asset_ID";

	/** Set Asset.
	  * Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID);

	/** Get Asset.
	  * Asset used internally or by customers
	  */
	public int getA_Asset_ID();

	public I_A_Asset getA_Asset() throws RuntimeException;

    /** Column name AccumulatedDepreciationAmt */
    public static final String COLUMNNAME_AccumulatedDepreciationAmt = "AccumulatedDepreciationAmt";

	/** Set Accumulated Depreciation Amount	  */
	public void setAccumulatedDepreciationAmt (BigDecimal AccumulatedDepreciationAmt);

	/** Get Accumulated Depreciation Amount	  */
	public BigDecimal getAccumulatedDepreciationAmt();

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AssetCost */
    public static final String COLUMNNAME_AssetCost = "AssetCost";

	/** Set Asset Cost	  */
	public void setAssetCost (BigDecimal AssetCost);

	/** Get Asset Cost	  */
	public BigDecimal getAssetCost();

    /** Column name AssetDepreciationDate */
    public static final String COLUMNNAME_AssetDepreciationDate = "AssetDepreciationDate";

	/** Set Asset Depreciation Date.
	  * Date of last depreciation
	  */
	public void setAssetDepreciationDate (Timestamp AssetDepreciationDate);

	/** Get Asset Depreciation Date.
	  * Date of last depreciation
	  */
	public Timestamp getAssetDepreciationDate();

    /** Column name Bill_BPartner_ID */
    public static final String COLUMNNAME_Bill_BPartner_ID = "Bill_BPartner_ID";

	/** Set Invoice Partner.
	  * Business Partner to be invoiced
	  */
	public void setBill_BPartner_ID (int Bill_BPartner_ID);

	/** Get Invoice Partner.
	  * Business Partner to be invoiced
	  */
	public int getBill_BPartner_ID();

	public I_C_BPartner getBill_BPartner() throws RuntimeException;

    /** Column name BookAmt */
    public static final String COLUMNNAME_BookAmt = "BookAmt";

	/** Set Book Amount	  */
	public void setBookAmt (BigDecimal BookAmt);

	/** Get Book Amount	  */
	public BigDecimal getBookAmt();

    /** Column name C_Activity_ID */
    public static final String COLUMNNAME_C_Activity_ID = "C_Activity_ID";

	/** Set Activity.
	  * Business Activity
	  */
	public void setC_Activity_ID (int C_Activity_ID);

	/** Get Activity.
	  * Business Activity
	  */
	public int getC_Activity_ID();

	public I_C_Activity getC_Activity() throws RuntimeException;

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name C_Campaign_ID */
    public static final String COLUMNNAME_C_Campaign_ID = "C_Campaign_ID";

	/** Set Campaign.
	  * Marketing Campaign
	  */
	public void setC_Campaign_ID (int C_Campaign_ID);

	/** Get Campaign.
	  * Marketing Campaign
	  */
	public int getC_Campaign_ID();

	public I_C_Campaign getC_Campaign() throws RuntimeException;

    /** Column name C_InvoiceLine_ID */
    public static final String COLUMNNAME_C_InvoiceLine_ID = "C_InvoiceLine_ID";

	/** Set Invoice Line.
	  * Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID);

	/** Get Invoice Line.
	  * Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID();

	public I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException;

    /** Column name ClosingBalance */
    public static final String COLUMNNAME_ClosingBalance = "ClosingBalance";

	/** Set Closing Balance	  */
	public void setClosingBalance (BigDecimal ClosingBalance);

	/** Get Closing Balance	  */
	public BigDecimal getClosingBalance();

    /** Column name CostAmt */
    public static final String COLUMNNAME_CostAmt = "CostAmt";

	/** Set Cost Value.
	  * Value with Cost
	  */
	public void setCostAmt (BigDecimal CostAmt);

	/** Get Cost Value.
	  * Value with Cost
	  */
	public BigDecimal getCostAmt();

    /** Column name C_Project_ID */
    public static final String COLUMNNAME_C_Project_ID = "C_Project_ID";

	/** Set Project.
	  * Financial Project
	  */
	public void setC_Project_ID (int C_Project_ID);

	/** Get Project.
	  * Financial Project
	  */
	public int getC_Project_ID();

	public I_C_Project getC_Project() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name C_SalesRegion_ID */
    public static final String COLUMNNAME_C_SalesRegion_ID = "C_SalesRegion_ID";

	/** Set Sales Region.
	  * Sales coverage region
	  */
	public void setC_SalesRegion_ID (int C_SalesRegion_ID);

	/** Get Sales Region.
	  * Sales coverage region
	  */
	public int getC_SalesRegion_ID();

	public I_C_SalesRegion getC_SalesRegion() throws RuntimeException;

    /** Column name C_UOM_ID */
    public static final String COLUMNNAME_C_UOM_ID = "C_UOM_ID";

	/** Set UOM.
	  * Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID);

	/** Get UOM.
	  * Unit of Measure
	  */
	public int getC_UOM_ID();

    /** Column name DateInvoiced */
    public static final String COLUMNNAME_DateInvoiced = "DateInvoiced";

	/** Set Date Invoiced.
	  * Date printed on Invoice
	  */
	public void setDateInvoiced (Timestamp DateInvoiced);

	/** Get Date Invoiced.
	  * Date printed on Invoice
	  */
	public Timestamp getDateInvoiced();

    /** Column name DepreciationExpenseAmt */
    public static final String COLUMNNAME_DepreciationExpenseAmt = "DepreciationExpenseAmt";

	/** Set Depreciation Expense Amount	  */
	public void setDepreciationExpenseAmt (BigDecimal DepreciationExpenseAmt);

	/** Get Depreciation Expense Amount	  */
	public BigDecimal getDepreciationExpenseAmt();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DisposalAmt */
    public static final String COLUMNNAME_DisposalAmt = "DisposalAmt";

	/** Set Disposal Amount	  */
	public void setDisposalAmt (BigDecimal DisposalAmt);

	/** Get Disposal Amount	  */
	public BigDecimal getDisposalAmt();

    /** Column name GAAS_AssetJournal_ID */
    public static final String COLUMNNAME_GAAS_AssetJournal_ID = "GAAS_AssetJournal_ID";

	/** Set Asset Journal	  */
	public void setGAAS_AssetJournal_ID (int GAAS_AssetJournal_ID);

	/** Get Asset Journal	  */
	public int getGAAS_AssetJournal_ID();

	public I_GAAS_AssetJournal getGAAS_AssetJournal() throws RuntimeException;

    /** Column name GAAS_AssetJournalLine_ID */
    public static final String COLUMNNAME_GAAS_AssetJournalLine_ID = "GAAS_AssetJournalLine_ID";

	/** Set Asset Journal Line	  */
	public void setGAAS_AssetJournalLine_ID (int GAAS_AssetJournalLine_ID);

	/** Get Asset Journal Line	  */
	public int getGAAS_AssetJournalLine_ID();

    /** Column name GAAS_Depreciation_Expense_ID */
    public static final String COLUMNNAME_GAAS_Depreciation_Expense_ID = "GAAS_Depreciation_Expense_ID";

	/** Set Depreciation Expense	  */
	public void setGAAS_Depreciation_Expense_ID (int GAAS_Depreciation_Expense_ID);

	/** Get Depreciation Expense	  */
	public int getGAAS_Depreciation_Expense_ID();

    /** Column name InitialCost */
    public static final String COLUMNNAME_InitialCost = "InitialCost";

	/** Set Initial Cost	  */
	public void setInitialCost (BigDecimal InitialCost);

	/** Get Initial Cost	  */
	public BigDecimal getInitialCost();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name LineNetAmt */
    public static final String COLUMNNAME_LineNetAmt = "LineNetAmt";

	/** Set Line Amount.
	  * Line Extended Amount (Quantity * Actual Price) without Freight and Charges
	  */
	public void setLineNetAmt (BigDecimal LineNetAmt);

	/** Get Line Amount.
	  * Line Extended Amount (Quantity * Actual Price) without Freight and Charges
	  */
	public BigDecimal getLineNetAmt();

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public I_M_Product getM_Product() throws RuntimeException;

    /** Column name New_Asset_Acct_ID */
    public static final String COLUMNNAME_New_Asset_Acct_ID = "New_Asset_Acct_ID";

	/** Set New Asset Acct	  */
	public void setNew_Asset_Acct_ID (int New_Asset_Acct_ID);

	/** Get New Asset Acct	  */
	public int getNew_Asset_Acct_ID();

	public I_GAAS_Asset_Acct getNew_Asset_Acct() throws RuntimeException;

    /** Column name New_Asset_Group_ID */
    public static final String COLUMNNAME_New_Asset_Group_ID = "New_Asset_Group_ID";

	/** Set New Asset Group	  */
	public void setNew_Asset_Group_ID (int New_Asset_Group_ID);

	/** Get New Asset Group	  */
	public int getNew_Asset_Group_ID();

	public I_A_Asset_Group getNew_Asset_Group() throws RuntimeException;

    /** Column name Old_Asset_Acct_ID */
    public static final String COLUMNNAME_Old_Asset_Acct_ID = "Old_Asset_Acct_ID";

	/** Set Old Asset Acct	  */
	public void setOld_Asset_Acct_ID (int Old_Asset_Acct_ID);

	/** Get Old Asset Acct	  */
	public int getOld_Asset_Acct_ID();

	public I_GAAS_Asset_Acct getOld_Asset_Acct() throws RuntimeException;

    /** Column name Old_Asset_Group_ID */
    public static final String COLUMNNAME_Old_Asset_Group_ID = "Old_Asset_Group_ID";

	/** Set Old Asset Group	  */
	public void setOld_Asset_Group_ID (int Old_Asset_Group_ID);

	/** Get Old Asset Group	  */
	public int getOld_Asset_Group_ID();

	public I_A_Asset_Group getOld_Asset_Group() throws RuntimeException;

    /** Column name OpeningBalance */
    public static final String COLUMNNAME_OpeningBalance = "OpeningBalance";

	/** Set Opening Balance	  */
	public void setOpeningBalance (BigDecimal OpeningBalance);

	/** Get Opening Balance	  */
	public BigDecimal getOpeningBalance();

    /** Column name ParentAsset_ID */
    public static final String COLUMNNAME_ParentAsset_ID = "ParentAsset_ID";

	/** Set Parent Asset	  */
	public void setParentAsset_ID (int ParentAsset_ID);

	/** Get Parent Asset	  */
	public int getParentAsset_ID();

	public I_A_Asset getParentAsset() throws RuntimeException;

    /** Column name PeriodNo */
    public static final String COLUMNNAME_PeriodNo = "PeriodNo";

	/** Set Period No.
	  * Unique Period Number
	  */
	public void setPeriodNo (int PeriodNo);

	/** Get Period No.
	  * Unique Period Number
	  */
	public int getPeriodNo();

    /** Column name PostedAmt */
    public static final String COLUMNNAME_PostedAmt = "PostedAmt";

	/** Set Posted Amount	  */
	public void setPostedAmt (BigDecimal PostedAmt);

	/** Get Posted Amount	  */
	public BigDecimal getPostedAmt();

    /** Column name PriceEntered */
    public static final String COLUMNNAME_PriceEntered = "PriceEntered";

	/** Set Price.
	  * Price Entered - the price based on the selected/base UoM
	  */
	public void setPriceEntered (BigDecimal PriceEntered);

	/** Get Price.
	  * Price Entered - the price based on the selected/base UoM
	  */
	public BigDecimal getPriceEntered();

    /** Column name Qty */
    public static final String COLUMNNAME_Qty = "Qty";

	/** Set Quantity.
	  * Quantity
	  */
	public void setQty (BigDecimal Qty);

	/** Get Quantity.
	  * Quantity
	  */
	public BigDecimal getQty();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name User1_ID */
    public static final String COLUMNNAME_User1_ID = "User1_ID";

	/** Set User List 1.
	  * User defined list element #1
	  */
	public void setUser1_ID (int User1_ID);

	/** Get User List 1.
	  * User defined list element #1
	  */
	public int getUser1_ID();

	public I_C_ElementValue getUser1() throws RuntimeException;

    /** Column name User2_ID */
    public static final String COLUMNNAME_User2_ID = "User2_ID";

	/** Set User List 2.
	  * User defined list element #2
	  */
	public void setUser2_ID (int User2_ID);

	/** Get User List 2.
	  * User defined list element #2
	  */
	public int getUser2_ID();

	public I_C_ElementValue getUser2() throws RuntimeException;
}
