/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for C_ElementValue_FA
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_C_ElementValue_FA extends PO implements I_C_ElementValue_FA, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20220307L;

    /** Standard Constructor */
    public X_C_ElementValue_FA (Properties ctx, int C_ElementValue_FA_ID, String trxName)
    {
      super (ctx, C_ElementValue_FA_ID, trxName);
      /** if (C_ElementValue_FA_ID == 0)
        {
			setC_ElementValue_FA_ID (0);
			setC_ElementValue_ID (0);
			setFAAccountType (null);
			setName (null);
// unused
        } */
    }

    /** Load Constructor */
    public X_C_ElementValue_FA (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_C_ElementValue_FA[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Account Elements for Fixed Assets.
		@param C_ElementValue_FA_ID Account Elements for Fixed Assets	  */
	public void setC_ElementValue_FA_ID (int C_ElementValue_FA_ID)
	{
		if (C_ElementValue_FA_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_ElementValue_FA_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_ElementValue_FA_ID, Integer.valueOf(C_ElementValue_FA_ID));
	}

	/** Get Account Elements for Fixed Assets.
		@return Account Elements for Fixed Assets	  */
	public int getC_ElementValue_FA_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_ElementValue_FA_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ElementValue getC_ElementValue() throws RuntimeException
    {
		return (I_C_ElementValue)MTable.get(getCtx(), I_C_ElementValue.Table_Name)
			.getPO(getC_ElementValue_ID(), get_TrxName());	}

	/** Set Account Element.
		@param C_ElementValue_ID 
		Account Element
	  */
	public void setC_ElementValue_ID (int C_ElementValue_ID)
	{
		if (C_ElementValue_ID < 1) 
			set_Value (COLUMNNAME_C_ElementValue_ID, null);
		else 
			set_Value (COLUMNNAME_C_ElementValue_ID, Integer.valueOf(C_ElementValue_ID));
	}

	/** Get Account Element.
		@return Account Element
	  */
	public int getC_ElementValue_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_ElementValue_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** FAAccountType AD_Reference_ID=53540 */
	public static final int FAACCOUNTTYPE_AD_Reference_ID=53540;
	/** 1 Fixed Asset = 1 */
	public static final String FAACCOUNTTYPE_1FixedAsset = "1";
	/** 2 Cum Depreciation = 2 */
	public static final String FAACCOUNTTYPE_2CumDepreciation = "2";
	/** Set FA Account Type.
		@param FAAccountType FA Account Type	  */
	public void setFAAccountType (String FAAccountType)
	{

		set_Value (COLUMNNAME_FAAccountType, FAAccountType);
	}

	/** Get FA Account Type.
		@return FA Account Type	  */
	public String getFAAccountType () 
	{
		return (String)get_Value(COLUMNNAME_FAAccountType);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }
}