/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.Env;

/** Generated Model for X_Conv_Currency_Rate
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_X_Conv_Currency_Rate extends PO implements I_X_Conv_Currency_Rate, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20220307L;

    /** Standard Constructor */
    public X_X_Conv_Currency_Rate (Properties ctx, int X_Conv_Currency_Rate_ID, String trxName)
    {
      super (ctx, X_Conv_Currency_Rate_ID, trxName);
      /** if (X_Conv_Currency_Rate_ID == 0)
        {
			setC_Currency_ID (0);
			setMultiplyRate (Env.ZERO);
// 1.00000
			setX_Conv_Currency_ID (0);
			setX_Conv_Currency_Rate_ID (0);
        } */
    }

    /** Load Constructor */
    public X_X_Conv_Currency_Rate (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_X_Conv_Currency_Rate[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_Currency getC_Currency() throws RuntimeException
    {
		return (I_C_Currency)MTable.get(getCtx(), I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Currency_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Multiply Rate.
		@param MultiplyRate 
		Rate to multiple the source by to calculate the target.
	  */
	public void setMultiplyRate (BigDecimal MultiplyRate)
	{
		set_Value (COLUMNNAME_MultiplyRate, MultiplyRate);
	}

	/** Get Multiply Rate.
		@return Rate to multiple the source by to calculate the target.
	  */
	public BigDecimal getMultiplyRate () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MultiplyRate);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_X_Conv_Currency getX_Conv_Currency() throws RuntimeException
    {
		return (I_X_Conv_Currency)MTable.get(getCtx(), I_X_Conv_Currency.Table_Name)
			.getPO(getX_Conv_Currency_ID(), get_TrxName());	}

	/** Set Conversion Currency ID.
		@param X_Conv_Currency_ID Conversion Currency ID	  */
	public void setX_Conv_Currency_ID (int X_Conv_Currency_ID)
	{
		if (X_Conv_Currency_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_X_Conv_Currency_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_X_Conv_Currency_ID, Integer.valueOf(X_Conv_Currency_ID));
	}

	/** Get Conversion Currency ID.
		@return Conversion Currency ID	  */
	public int getX_Conv_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_X_Conv_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Conversion Currency Rate ID.
		@param X_Conv_Currency_Rate_ID Conversion Currency Rate ID	  */
	public void setX_Conv_Currency_Rate_ID (int X_Conv_Currency_Rate_ID)
	{
		if (X_Conv_Currency_Rate_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_X_Conv_Currency_Rate_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_X_Conv_Currency_Rate_ID, Integer.valueOf(X_Conv_Currency_Rate_ID));
	}

	/** Get Conversion Currency Rate ID.
		@return Conversion Currency Rate ID	  */
	public int getX_Conv_Currency_Rate_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_X_Conv_Currency_Rate_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}