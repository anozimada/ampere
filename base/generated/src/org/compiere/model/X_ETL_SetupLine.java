/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

/** Generated Model for ETL_SetupLine
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_ETL_SetupLine extends PO implements I_ETL_SetupLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20220307L;

    /** Standard Constructor */
    public X_ETL_SetupLine (Properties ctx, int ETL_SetupLine_ID, String trxName)
    {
      super (ctx, ETL_SetupLine_ID, trxName);
      /** if (ETL_SetupLine_ID == 0)
        {
			setETL_Setup_ID (0);
			setETL_SetupLine_ID (0);
        } */
    }

    /** Load Constructor */
    public X_ETL_SetupLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_ETL_SetupLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_ETL_Setup getETL_Setup() throws RuntimeException
    {
		return (I_ETL_Setup)MTable.get(getCtx(), I_ETL_Setup.Table_Name)
			.getPO(getETL_Setup_ID(), get_TrxName());	}

	/** Set ETL Setup ID.
		@param ETL_Setup_ID ETL Setup ID	  */
	public void setETL_Setup_ID (int ETL_Setup_ID)
	{
		if (ETL_Setup_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_ETL_Setup_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_ETL_Setup_ID, Integer.valueOf(ETL_Setup_ID));
	}

	/** Get ETL Setup ID.
		@return ETL Setup ID	  */
	public int getETL_Setup_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_ETL_Setup_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set ETL Setup Line ID.
		@param ETL_SetupLine_ID ETL Setup Line ID	  */
	public void setETL_SetupLine_ID (int ETL_SetupLine_ID)
	{
		if (ETL_SetupLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_ETL_SetupLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_ETL_SetupLine_ID, Integer.valueOf(ETL_SetupLine_ID));
	}

	/** Get ETL Setup Line ID.
		@return ETL Setup Line ID	  */
	public int getETL_SetupLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_ETL_SetupLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}