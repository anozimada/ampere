/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for C_ElementValidation
 *  @author Adempiere (generated) 
 *  @version $
{
project.version}

 */
public interface I_C_ElementValidation 
{

    /** TableName=C_ElementValidation */
    public static final String Table_Name = "C_ElementValidation";

    /** AD_Table_ID=53939 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 2 - Client 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(2);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_AcctSchema_Element_ID */
    public static final String COLUMNNAME_C_AcctSchema_Element_ID = "C_AcctSchema_Element_ID";

	/** Set Acct.Schema Element	  */
	public void setC_AcctSchema_Element_ID (int C_AcctSchema_Element_ID);

	/** Get Acct.Schema Element	  */
	public int getC_AcctSchema_Element_ID();

	public I_C_AcctSchema_Element getC_AcctSchema_Element() throws RuntimeException;

    /** Column name C_ElementValidation_ID */
    public static final String COLUMNNAME_C_ElementValidation_ID = "C_ElementValidation_ID";

	/** Set Account Element Validation ID	  */
	public void setC_ElementValidation_ID (int C_ElementValidation_ID);

	/** Get Account Element Validation ID	  */
	public int getC_ElementValidation_ID();

    /** Column name C_ElementValue_From_ID */
    public static final String COLUMNNAME_C_ElementValue_From_ID = "C_ElementValue_From_ID";

	/** Set Account Element.
	  * Account Element
	  */
	public void setC_ElementValue_From_ID (int C_ElementValue_From_ID);

	/** Get Account Element.
	  * Account Element
	  */
	public int getC_ElementValue_From_ID();

	public I_C_ElementValue getC_ElementValue_From() throws RuntimeException;

    /** Column name C_ElementValue_ID */
    public static final String COLUMNNAME_C_ElementValue_ID = "C_ElementValue_ID";

	/** Set Account Element.
	  * Account Element
	  */
	public void setC_ElementValue_ID (int C_ElementValue_ID);

	/** Get Account Element.
	  * Account Element
	  */
	public int getC_ElementValue_ID();

	public I_C_ElementValue getC_ElementValue() throws RuntimeException;

    /** Column name C_ElementValue_To_ID */
    public static final String COLUMNNAME_C_ElementValue_To_ID = "C_ElementValue_To_ID";

	/** Set Account Element.
	  * Account Element
	  */
	public void setC_ElementValue_To_ID (int C_ElementValue_To_ID);

	/** Get Account Element.
	  * Account Element
	  */
	public int getC_ElementValue_To_ID();

	public I_C_ElementValue getC_ElementValue_To() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IncludeExclude */
    public static final String COLUMNNAME_IncludeExclude = "IncludeExclude";

	/** Set Include/Exclude	  */
	public void setIncludeExclude (String IncludeExclude);

	/** Get Include/Exclude	  */
	public String getIncludeExclude();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
