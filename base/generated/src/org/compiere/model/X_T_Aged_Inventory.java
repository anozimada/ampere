/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.Env;

/** Generated Model for T_Aged_Inventory
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_T_Aged_Inventory extends PO implements I_T_Aged_Inventory, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20220307L;

    /** Standard Constructor */
    public X_T_Aged_Inventory (Properties ctx, int T_Aged_Inventory_ID, String trxName)
    {
      super (ctx, T_Aged_Inventory_ID, trxName);
      /** if (T_Aged_Inventory_ID == 0)
        {
			setAD_PInstance_ID (0);
        } */
    }

    /** Load Constructor */
    public X_T_Aged_Inventory (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_T_Aged_Inventory[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_AD_PInstance getAD_PInstance() throws RuntimeException
    {
		return (I_AD_PInstance)MTable.get(getCtx(), I_AD_PInstance.Table_Name)
			.getPO(getAD_PInstance_ID(), get_TrxName());	}

	/** Set Process Instance.
		@param AD_PInstance_ID 
		Instance of the process
	  */
	public void setAD_PInstance_ID (int AD_PInstance_ID)
	{
		if (AD_PInstance_ID < 1) 
			set_Value (COLUMNNAME_AD_PInstance_ID, null);
		else 
			set_Value (COLUMNNAME_AD_PInstance_ID, Integer.valueOf(AD_PInstance_ID));
	}

	/** Get Process Instance.
		@return Instance of the process
	  */
	public int getAD_PInstance_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_PInstance_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Cost.
		@param Cost 
		Cost information
	  */
	public void setCost (BigDecimal Cost)
	{
		set_Value (COLUMNNAME_Cost, Cost);
	}

	/** Get Cost.
		@return Cost information
	  */
	public BigDecimal getCost () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Cost);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Month_0.
		@param Month_0 Month_0	  */
	public void setMonth_0 (BigDecimal Month_0)
	{
		set_Value (COLUMNNAME_Month_0, Month_0);
	}

	/** Get Month_0.
		@return Month_0	  */
	public BigDecimal getMonth_0 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Month_0);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Month_1.
		@param Month_1 Month_1	  */
	public void setMonth_1 (BigDecimal Month_1)
	{
		set_Value (COLUMNNAME_Month_1, Month_1);
	}

	/** Get Month_1.
		@return Month_1	  */
	public BigDecimal getMonth_1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Month_1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Month_10.
		@param Month_10 Month_10	  */
	public void setMonth_10 (BigDecimal Month_10)
	{
		set_Value (COLUMNNAME_Month_10, Month_10);
	}

	/** Get Month_10.
		@return Month_10	  */
	public BigDecimal getMonth_10 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Month_10);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Month_11.
		@param Month_11 Month_11	  */
	public void setMonth_11 (BigDecimal Month_11)
	{
		set_Value (COLUMNNAME_Month_11, Month_11);
	}

	/** Get Month_11.
		@return Month_11	  */
	public BigDecimal getMonth_11 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Month_11);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Month_2.
		@param Month_2 Month_2	  */
	public void setMonth_2 (BigDecimal Month_2)
	{
		set_Value (COLUMNNAME_Month_2, Month_2);
	}

	/** Get Month_2.
		@return Month_2	  */
	public BigDecimal getMonth_2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Month_2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Month_3.
		@param Month_3 Month_3	  */
	public void setMonth_3 (BigDecimal Month_3)
	{
		set_Value (COLUMNNAME_Month_3, Month_3);
	}

	/** Get Month_3.
		@return Month_3	  */
	public BigDecimal getMonth_3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Month_3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Month_4.
		@param Month_4 Month_4	  */
	public void setMonth_4 (BigDecimal Month_4)
	{
		set_Value (COLUMNNAME_Month_4, Month_4);
	}

	/** Get Month_4.
		@return Month_4	  */
	public BigDecimal getMonth_4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Month_4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Month_5.
		@param Month_5 Month_5	  */
	public void setMonth_5 (BigDecimal Month_5)
	{
		set_Value (COLUMNNAME_Month_5, Month_5);
	}

	/** Get Month_5.
		@return Month_5	  */
	public BigDecimal getMonth_5 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Month_5);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Month_6.
		@param Month_6 Month_6	  */
	public void setMonth_6 (BigDecimal Month_6)
	{
		set_Value (COLUMNNAME_Month_6, Month_6);
	}

	/** Get Month_6.
		@return Month_6	  */
	public BigDecimal getMonth_6 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Month_6);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Month_7.
		@param Month_7 Month_7	  */
	public void setMonth_7 (BigDecimal Month_7)
	{
		set_Value (COLUMNNAME_Month_7, Month_7);
	}

	/** Get Month_7.
		@return Month_7	  */
	public BigDecimal getMonth_7 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Month_7);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Month_8.
		@param Month_8 Month_8	  */
	public void setMonth_8 (BigDecimal Month_8)
	{
		set_Value (COLUMNNAME_Month_8, Month_8);
	}

	/** Get Month_8.
		@return Month_8	  */
	public BigDecimal getMonth_8 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Month_8);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Month_9.
		@param Month_9 Month_9	  */
	public void setMonth_9 (BigDecimal Month_9)
	{
		set_Value (COLUMNNAME_Month_9, Month_9);
	}

	/** Get Month_9.
		@return Month_9	  */
	public BigDecimal getMonth_9 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Month_9);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_M_Product getM_Product() throws RuntimeException
    {
		return (I_M_Product)MTable.get(getCtx(), I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total.
		@param Total Total	  */
	public void setTotal (BigDecimal Total)
	{
		set_Value (COLUMNNAME_Total, Total);
	}

	/** Get Total.
		@return Total	  */
	public BigDecimal getTotal () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Total);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set TotalValue.
		@param TotalValue TotalValue	  */
	public void setTotalValue (BigDecimal TotalValue)
	{
		set_Value (COLUMNNAME_TotalValue, TotalValue);
	}

	/** Get TotalValue.
		@return TotalValue	  */
	public BigDecimal getTotalValue () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalValue);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Usage_12_Months.
		@param Usage_12_Months Usage_12_Months	  */
	public void setUsage_12_Months (BigDecimal Usage_12_Months)
	{
		set_Value (COLUMNNAME_Usage_12_Months, Usage_12_Months);
	}

	/** Get Usage_12_Months.
		@return Usage_12_Months	  */
	public BigDecimal getUsage_12_Months () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Usage_12_Months);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Usage_18_Months.
		@param Usage_18_Months Usage_18_Months	  */
	public void setUsage_18_Months (BigDecimal Usage_18_Months)
	{
		set_Value (COLUMNNAME_Usage_18_Months, Usage_18_Months);
	}

	/** Get Usage_18_Months.
		@return Usage_18_Months	  */
	public BigDecimal getUsage_18_Months () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Usage_18_Months);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Usage_24_Months.
		@param Usage_24_Months Usage_24_Months	  */
	public void setUsage_24_Months (BigDecimal Usage_24_Months)
	{
		set_Value (COLUMNNAME_Usage_24_Months, Usage_24_Months);
	}

	/** Get Usage_24_Months.
		@return Usage_24_Months	  */
	public BigDecimal getUsage_24_Months () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Usage_24_Months);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Usage_3_Months.
		@param Usage_3_Months Usage_3_Months	  */
	public void setUsage_3_Months (BigDecimal Usage_3_Months)
	{
		set_Value (COLUMNNAME_Usage_3_Months, Usage_3_Months);
	}

	/** Get Usage_3_Months.
		@return Usage_3_Months	  */
	public BigDecimal getUsage_3_Months () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Usage_3_Months);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Usage_6_Months.
		@param Usage_6_Months Usage_6_Months	  */
	public void setUsage_6_Months (BigDecimal Usage_6_Months)
	{
		set_Value (COLUMNNAME_Usage_6_Months, Usage_6_Months);
	}

	/** Get Usage_6_Months.
		@return Usage_6_Months	  */
	public BigDecimal getUsage_6_Months () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Usage_6_Months);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}