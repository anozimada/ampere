/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.Env;

/** Generated Model for C_EDI_AckLine
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_C_EDI_AckLine extends PO implements I_C_EDI_AckLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20220307L;

    /** Standard Constructor */
    public X_C_EDI_AckLine (Properties ctx, int C_EDI_AckLine_ID, String trxName)
    {
      super (ctx, C_EDI_AckLine_ID, trxName);
      /** if (C_EDI_AckLine_ID == 0)
        {
			setC_EDI_Ack_ID (0);
			setC_EDI_AckLine_ID (0);
			setLineNo (Env.ZERO);
			setTransactionIdentifier (null);
        } */
    }

    /** Load Constructor */
    public X_C_EDI_AckLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_C_EDI_AckLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_EDI_Ack getC_EDI_Ack() throws RuntimeException
    {
		return (I_C_EDI_Ack)MTable.get(getCtx(), I_C_EDI_Ack.Table_Name)
			.getPO(getC_EDI_Ack_ID(), get_TrxName());	}

	/** Set EDI Acknowledgement.
		@param C_EDI_Ack_ID EDI Acknowledgement	  */
	public void setC_EDI_Ack_ID (int C_EDI_Ack_ID)
	{
		if (C_EDI_Ack_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_EDI_Ack_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_EDI_Ack_ID, Integer.valueOf(C_EDI_Ack_ID));
	}

	/** Get EDI Acknowledgement.
		@return EDI Acknowledgement	  */
	public int getC_EDI_Ack_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDI_Ack_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set EDI Acknowledgement Line.
		@param C_EDI_AckLine_ID EDI Acknowledgement Line	  */
	public void setC_EDI_AckLine_ID (int C_EDI_AckLine_ID)
	{
		if (C_EDI_AckLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_EDI_AckLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_EDI_AckLine_ID, Integer.valueOf(C_EDI_AckLine_ID));
	}

	/** Get EDI Acknowledgement Line.
		@return EDI Acknowledgement Line	  */
	public int getC_EDI_AckLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDI_AckLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Line.
		@param LineNo 
		Line No
	  */
	public void setLineNo (BigDecimal LineNo)
	{
		set_Value (COLUMNNAME_LineNo, LineNo);
	}

	/** Get Line.
		@return Line No
	  */
	public BigDecimal getLineNo () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LineNo);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Transaction Set Identifier.
		@param TransactionIdentifier Transaction Set Identifier	  */
	public void setTransactionIdentifier (String TransactionIdentifier)
	{
		set_Value (COLUMNNAME_TransactionIdentifier, TransactionIdentifier);
	}

	/** Get Transaction Set Identifier.
		@return Transaction Set Identifier	  */
	public String getTransactionIdentifier () 
	{
		return (String)get_Value(COLUMNNAME_TransactionIdentifier);
	}
}