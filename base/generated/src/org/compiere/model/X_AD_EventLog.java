/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;
import org.json.JSONObject;

/** Generated Model for AD_EventLog
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_AD_EventLog extends PO implements I_AD_EventLog, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20220308L;

    /** Standard Constructor */
    public X_AD_EventLog (Properties ctx, int AD_EventLog_ID, String trxName)
    {
      super (ctx, AD_EventLog_ID, trxName);
      /** if (AD_EventLog_ID == 0)
        {
			setAD_EventLog_ID (0);
			setAD_Session_ID (0);
			setAD_Table_ID (0);
			setRecord_ID (0);
        } */
    }

    /** Load Constructor */
    public X_AD_EventLog (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 6 - System - Client 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_AD_EventLog[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set EventLog ID.
		@param AD_EventLog_ID EventLog ID	  */
	public void setAD_EventLog_ID (int AD_EventLog_ID)
	{
		if (AD_EventLog_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_EventLog_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_EventLog_ID, Integer.valueOf(AD_EventLog_ID));
	}

	/** Get EventLog ID.
		@return EventLog ID	  */
	public int getAD_EventLog_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_EventLog_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_AD_Session getAD_Session() throws RuntimeException
    {
		return (I_AD_Session)MTable.get(getCtx(), I_AD_Session.Table_Name)
			.getPO(getAD_Session_ID(), get_TrxName());	}

	/** Set Session.
		@param AD_Session_ID 
		User Session Online or Web
	  */
	public void setAD_Session_ID (int AD_Session_ID)
	{
		if (AD_Session_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_Session_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_Session_ID, Integer.valueOf(AD_Session_ID));
	}

	/** Get Session.
		@return User Session Online or Web
	  */
	public int getAD_Session_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Session_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getAD_Session_ID()));
    }

	public I_AD_Table getAD_Table() throws RuntimeException
    {
		return (I_AD_Table)MTable.get(getCtx(), I_AD_Table.Table_Name)
			.getPO(getAD_Table_ID(), get_TrxName());	}

	/** Set Table.
		@param AD_Table_ID 
		Database Table information
	  */
	public void setAD_Table_ID (int AD_Table_ID)
	{
		if (AD_Table_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_Table_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_Table_ID, Integer.valueOf(AD_Table_ID));
	}

	/** Get Table.
		@return Database Table information
	  */
	public int getAD_Table_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Table_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Event Data.
		@param EventData 
		Data related to the event
	  */
	public void setEventData (JSONObject EventData)
	{
		set_Value (COLUMNNAME_EventData, EventData);
	}

	/** Get Event Data.
		@return Data related to the event
	  */
	public JSONObject getEventData () 
	{
		return (JSONObject)get_Value(COLUMNNAME_EventData);
	}

	/** EventType AD_Reference_ID=400001 */
	public static final int EVENTTYPE_AD_Reference_ID=400001;
	/** Insert PO = po.insert */
	public static final String EVENTTYPE_InsertPO = "po.insert";
	/** Update PO = po.update */
	public static final String EVENTTYPE_UpdatePO = "po.update";
	/** Delete PO = po.delete */
	public static final String EVENTTYPE_DeletePO = "po.delete";
	/** Set EventType.
		@param EventType 
		Type of Event
	  */
	public void setEventType (String EventType)
	{

		set_Value (COLUMNNAME_EventType, EventType);
	}

	/** Get EventType.
		@return Type of Event
	  */
	public String getEventType () 
	{
		return (String)get_Value(COLUMNNAME_EventType);
	}

	/** Set Record ID.
		@param Record_ID 
		Direct internal record ID
	  */
	public void setRecord_ID (int Record_ID)
	{
		if (Record_ID < 0) 
			set_ValueNoCheck (COLUMNNAME_Record_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Record_ID, Integer.valueOf(Record_ID));
	}

	/** Get Record ID.
		@return Direct internal record ID
	  */
	public int getRecord_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Record_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Transaction.
		@param TrxName 
		Name of the transaction
	  */
	public void setTrxName (String TrxName)
	{
		set_ValueNoCheck (COLUMNNAME_TrxName, TrxName);
	}

	/** Get Transaction.
		@return Name of the transaction
	  */
	public String getTrxName () 
	{
		return (String)get_Value(COLUMNNAME_TrxName);
	}
}