/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for GAAS_AssetJournalLine
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_GAAS_AssetJournalLine extends PO implements I_GAAS_AssetJournalLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20220307L;

    /** Standard Constructor */
    public X_GAAS_AssetJournalLine (Properties ctx, int GAAS_AssetJournalLine_ID, String trxName)
    {
      super (ctx, GAAS_AssetJournalLine_ID, trxName);
      /** if (GAAS_AssetJournalLine_ID == 0)
        {
			setCostAmt (Env.ZERO);
// 0
			setGAAS_AssetJournal_ID (0);
			setGAAS_AssetJournalLine_ID (0);
        } */
    }

    /** Load Constructor */
    public X_GAAS_AssetJournalLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_GAAS_AssetJournalLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_A_Asset_Group getA_Asset_Group() throws RuntimeException
    {
		return (I_A_Asset_Group)MTable.get(getCtx(), I_A_Asset_Group.Table_Name)
			.getPO(getA_Asset_Group_ID(), get_TrxName());	}

	/** Set Asset Group.
		@param A_Asset_Group_ID 
		Group of Assets
	  */
	public void setA_Asset_Group_ID (int A_Asset_Group_ID)
	{
		if (A_Asset_Group_ID < 1) 
			set_Value (COLUMNNAME_A_Asset_Group_ID, null);
		else 
			set_Value (COLUMNNAME_A_Asset_Group_ID, Integer.valueOf(A_Asset_Group_ID));
	}

	/** Get Asset Group.
		@return Group of Assets
	  */
	public int getA_Asset_Group_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_A_Asset_Group_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_A_Asset getA_Asset() throws RuntimeException
    {
		return (I_A_Asset)MTable.get(getCtx(), I_A_Asset.Table_Name)
			.getPO(getA_Asset_ID(), get_TrxName());	}

	/** Set Asset.
		@param A_Asset_ID 
		Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID)
	{
		if (A_Asset_ID < 1) 
			set_Value (COLUMNNAME_A_Asset_ID, null);
		else 
			set_Value (COLUMNNAME_A_Asset_ID, Integer.valueOf(A_Asset_ID));
	}

	/** Get Asset.
		@return Asset used internally or by customers
	  */
	public int getA_Asset_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_A_Asset_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Accumulated Depreciation Amount.
		@param AccumulatedDepreciationAmt Accumulated Depreciation Amount	  */
	public void setAccumulatedDepreciationAmt (BigDecimal AccumulatedDepreciationAmt)
	{
		set_Value (COLUMNNAME_AccumulatedDepreciationAmt, AccumulatedDepreciationAmt);
	}

	/** Get Accumulated Depreciation Amount.
		@return Accumulated Depreciation Amount	  */
	public BigDecimal getAccumulatedDepreciationAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AccumulatedDepreciationAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Asset Cost.
		@param AssetCost Asset Cost	  */
	public void setAssetCost (BigDecimal AssetCost)
	{
		throw new IllegalArgumentException ("AssetCost is virtual column");	}

	/** Get Asset Cost.
		@return Asset Cost	  */
	public BigDecimal getAssetCost () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AssetCost);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Asset Depreciation Date.
		@param AssetDepreciationDate 
		Date of last depreciation
	  */
	public void setAssetDepreciationDate (Timestamp AssetDepreciationDate)
	{
		throw new IllegalArgumentException ("AssetDepreciationDate is virtual column");	}

	/** Get Asset Depreciation Date.
		@return Date of last depreciation
	  */
	public Timestamp getAssetDepreciationDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_AssetDepreciationDate);
	}

	public I_C_BPartner getBill_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getBill_BPartner_ID(), get_TrxName());	}

	/** Set Invoice Partner.
		@param Bill_BPartner_ID 
		Business Partner to be invoiced
	  */
	public void setBill_BPartner_ID (int Bill_BPartner_ID)
	{
		throw new IllegalArgumentException ("Bill_BPartner_ID is virtual column");	}

	/** Get Invoice Partner.
		@return Business Partner to be invoiced
	  */
	public int getBill_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Bill_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Book Amount.
		@param BookAmt Book Amount	  */
	public void setBookAmt (BigDecimal BookAmt)
	{
		throw new IllegalArgumentException ("BookAmt is virtual column");	}

	/** Get Book Amount.
		@return Book Amount	  */
	public BigDecimal getBookAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BookAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_C_Activity getC_Activity() throws RuntimeException
    {
		return (I_C_Activity)MTable.get(getCtx(), I_C_Activity.Table_Name)
			.getPO(getC_Activity_ID(), get_TrxName());	}

	/** Set Activity.
		@param C_Activity_ID 
		Business Activity
	  */
	public void setC_Activity_ID (int C_Activity_ID)
	{
		if (C_Activity_ID < 1) 
			set_Value (COLUMNNAME_C_Activity_ID, null);
		else 
			set_Value (COLUMNNAME_C_Activity_ID, Integer.valueOf(C_Activity_ID));
	}

	/** Get Activity.
		@return Business Activity
	  */
	public int getC_Activity_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Activity_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_Campaign getC_Campaign() throws RuntimeException
    {
		return (I_C_Campaign)MTable.get(getCtx(), I_C_Campaign.Table_Name)
			.getPO(getC_Campaign_ID(), get_TrxName());	}

	/** Set Campaign.
		@param C_Campaign_ID 
		Marketing Campaign
	  */
	public void setC_Campaign_ID (int C_Campaign_ID)
	{
		if (C_Campaign_ID < 1) 
			set_Value (COLUMNNAME_C_Campaign_ID, null);
		else 
			set_Value (COLUMNNAME_C_Campaign_ID, Integer.valueOf(C_Campaign_ID));
	}

	/** Get Campaign.
		@return Marketing Campaign
	  */
	public int getC_Campaign_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Campaign_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException
    {
		return (I_C_InvoiceLine)MTable.get(getCtx(), I_C_InvoiceLine.Table_Name)
			.getPO(getC_InvoiceLine_ID(), get_TrxName());	}

	/** Set Invoice Line.
		@param C_InvoiceLine_ID 
		Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID)
	{
		if (C_InvoiceLine_ID < 1) 
			set_Value (COLUMNNAME_C_InvoiceLine_ID, null);
		else 
			set_Value (COLUMNNAME_C_InvoiceLine_ID, Integer.valueOf(C_InvoiceLine_ID));
	}

	/** Get Invoice Line.
		@return Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_InvoiceLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Closing Balance.
		@param ClosingBalance Closing Balance	  */
	public void setClosingBalance (BigDecimal ClosingBalance)
	{
		throw new IllegalArgumentException ("ClosingBalance is virtual column");	}

	/** Get Closing Balance.
		@return Closing Balance	  */
	public BigDecimal getClosingBalance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ClosingBalance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cost Value.
		@param CostAmt 
		Value with Cost
	  */
	public void setCostAmt (BigDecimal CostAmt)
	{
		set_Value (COLUMNNAME_CostAmt, CostAmt);
	}

	/** Get Cost Value.
		@return Value with Cost
	  */
	public BigDecimal getCostAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CostAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_C_Project getC_Project() throws RuntimeException
    {
		return (I_C_Project)MTable.get(getCtx(), I_C_Project.Table_Name)
			.getPO(getC_Project_ID(), get_TrxName());	}

	/** Set Project.
		@param C_Project_ID 
		Financial Project
	  */
	public void setC_Project_ID (int C_Project_ID)
	{
		if (C_Project_ID < 1) 
			set_Value (COLUMNNAME_C_Project_ID, null);
		else 
			set_Value (COLUMNNAME_C_Project_ID, Integer.valueOf(C_Project_ID));
	}

	/** Get Project.
		@return Financial Project
	  */
	public int getC_Project_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Project_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_SalesRegion getC_SalesRegion() throws RuntimeException
    {
		return (I_C_SalesRegion)MTable.get(getCtx(), I_C_SalesRegion.Table_Name)
			.getPO(getC_SalesRegion_ID(), get_TrxName());	}

	/** Set Sales Region.
		@param C_SalesRegion_ID 
		Sales coverage region
	  */
	public void setC_SalesRegion_ID (int C_SalesRegion_ID)
	{
		if (C_SalesRegion_ID < 1) 
			set_Value (COLUMNNAME_C_SalesRegion_ID, null);
		else 
			set_Value (COLUMNNAME_C_SalesRegion_ID, Integer.valueOf(C_SalesRegion_ID));
	}

	/** Get Sales Region.
		@return Sales coverage region
	  */
	public int getC_SalesRegion_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_SalesRegion_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		throw new IllegalArgumentException ("C_UOM_ID is virtual column");	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Date Invoiced.
		@param DateInvoiced 
		Date printed on Invoice
	  */
	public void setDateInvoiced (Timestamp DateInvoiced)
	{
		set_Value (COLUMNNAME_DateInvoiced, DateInvoiced);
	}

	/** Get Date Invoiced.
		@return Date printed on Invoice
	  */
	public Timestamp getDateInvoiced () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateInvoiced);
	}

	/** Set Depreciation Expense Amount.
		@param DepreciationExpenseAmt Depreciation Expense Amount	  */
	public void setDepreciationExpenseAmt (BigDecimal DepreciationExpenseAmt)
	{
		throw new IllegalArgumentException ("DepreciationExpenseAmt is virtual column");	}

	/** Get Depreciation Expense Amount.
		@return Depreciation Expense Amount	  */
	public BigDecimal getDepreciationExpenseAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DepreciationExpenseAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Disposal Amount.
		@param DisposalAmt Disposal Amount	  */
	public void setDisposalAmt (BigDecimal DisposalAmt)
	{
		set_Value (COLUMNNAME_DisposalAmt, DisposalAmt);
	}

	/** Get Disposal Amount.
		@return Disposal Amount	  */
	public BigDecimal getDisposalAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DisposalAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_GAAS_AssetJournal getGAAS_AssetJournal() throws RuntimeException
    {
		return (I_GAAS_AssetJournal)MTable.get(getCtx(), I_GAAS_AssetJournal.Table_Name)
			.getPO(getGAAS_AssetJournal_ID(), get_TrxName());	}

	/** Set Asset Journal.
		@param GAAS_AssetJournal_ID Asset Journal	  */
	public void setGAAS_AssetJournal_ID (int GAAS_AssetJournal_ID)
	{
		if (GAAS_AssetJournal_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_GAAS_AssetJournal_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_GAAS_AssetJournal_ID, Integer.valueOf(GAAS_AssetJournal_ID));
	}

	/** Get Asset Journal.
		@return Asset Journal	  */
	public int getGAAS_AssetJournal_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_AssetJournal_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getGAAS_AssetJournal_ID()));
    }

	/** Set Asset Journal Line.
		@param GAAS_AssetJournalLine_ID Asset Journal Line	  */
	public void setGAAS_AssetJournalLine_ID (int GAAS_AssetJournalLine_ID)
	{
		if (GAAS_AssetJournalLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_GAAS_AssetJournalLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_GAAS_AssetJournalLine_ID, Integer.valueOf(GAAS_AssetJournalLine_ID));
	}

	/** Get Asset Journal Line.
		@return Asset Journal Line	  */
	public int getGAAS_AssetJournalLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_AssetJournalLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Depreciation Expense.
		@param GAAS_Depreciation_Expense_ID Depreciation Expense	  */
	public void setGAAS_Depreciation_Expense_ID (int GAAS_Depreciation_Expense_ID)
	{
		if (GAAS_Depreciation_Expense_ID < 1) 
			set_Value (COLUMNNAME_GAAS_Depreciation_Expense_ID, null);
		else 
			set_Value (COLUMNNAME_GAAS_Depreciation_Expense_ID, Integer.valueOf(GAAS_Depreciation_Expense_ID));
	}

	/** Get Depreciation Expense.
		@return Depreciation Expense	  */
	public int getGAAS_Depreciation_Expense_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Depreciation_Expense_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Initial Cost.
		@param InitialCost Initial Cost	  */
	public void setInitialCost (BigDecimal InitialCost)
	{
		set_Value (COLUMNNAME_InitialCost, InitialCost);
	}

	/** Get Initial Cost.
		@return Initial Cost	  */
	public BigDecimal getInitialCost () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_InitialCost);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Line Amount.
		@param LineNetAmt 
		Line Extended Amount (Quantity * Actual Price) without Freight and Charges
	  */
	public void setLineNetAmt (BigDecimal LineNetAmt)
	{
		throw new IllegalArgumentException ("LineNetAmt is virtual column");	}

	/** Get Line Amount.
		@return Line Extended Amount (Quantity * Actual Price) without Freight and Charges
	  */
	public BigDecimal getLineNetAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LineNetAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_M_Product getM_Product() throws RuntimeException
    {
		return (I_M_Product)MTable.get(getCtx(), I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_GAAS_Asset_Acct getNew_Asset_Acct() throws RuntimeException
    {
		return (I_GAAS_Asset_Acct)MTable.get(getCtx(), I_GAAS_Asset_Acct.Table_Name)
			.getPO(getNew_Asset_Acct_ID(), get_TrxName());	}

	/** Set New Asset Acct.
		@param New_Asset_Acct_ID New Asset Acct	  */
	public void setNew_Asset_Acct_ID (int New_Asset_Acct_ID)
	{
		if (New_Asset_Acct_ID < 1) 
			set_Value (COLUMNNAME_New_Asset_Acct_ID, null);
		else 
			set_Value (COLUMNNAME_New_Asset_Acct_ID, Integer.valueOf(New_Asset_Acct_ID));
	}

	/** Get New Asset Acct.
		@return New Asset Acct	  */
	public int getNew_Asset_Acct_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_New_Asset_Acct_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_A_Asset_Group getNew_Asset_Group() throws RuntimeException
    {
		return (I_A_Asset_Group)MTable.get(getCtx(), I_A_Asset_Group.Table_Name)
			.getPO(getNew_Asset_Group_ID(), get_TrxName());	}

	/** Set New Asset Group.
		@param New_Asset_Group_ID New Asset Group	  */
	public void setNew_Asset_Group_ID (int New_Asset_Group_ID)
	{
		if (New_Asset_Group_ID < 1) 
			set_Value (COLUMNNAME_New_Asset_Group_ID, null);
		else 
			set_Value (COLUMNNAME_New_Asset_Group_ID, Integer.valueOf(New_Asset_Group_ID));
	}

	/** Get New Asset Group.
		@return New Asset Group	  */
	public int getNew_Asset_Group_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_New_Asset_Group_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_GAAS_Asset_Acct getOld_Asset_Acct() throws RuntimeException
    {
		return (I_GAAS_Asset_Acct)MTable.get(getCtx(), I_GAAS_Asset_Acct.Table_Name)
			.getPO(getOld_Asset_Acct_ID(), get_TrxName());	}

	/** Set Old Asset Acct.
		@param Old_Asset_Acct_ID Old Asset Acct	  */
	public void setOld_Asset_Acct_ID (int Old_Asset_Acct_ID)
	{
		if (Old_Asset_Acct_ID < 1) 
			set_Value (COLUMNNAME_Old_Asset_Acct_ID, null);
		else 
			set_Value (COLUMNNAME_Old_Asset_Acct_ID, Integer.valueOf(Old_Asset_Acct_ID));
	}

	/** Get Old Asset Acct.
		@return Old Asset Acct	  */
	public int getOld_Asset_Acct_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Old_Asset_Acct_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_A_Asset_Group getOld_Asset_Group() throws RuntimeException
    {
		return (I_A_Asset_Group)MTable.get(getCtx(), I_A_Asset_Group.Table_Name)
			.getPO(getOld_Asset_Group_ID(), get_TrxName());	}

	/** Set Old Asset Group.
		@param Old_Asset_Group_ID Old Asset Group	  */
	public void setOld_Asset_Group_ID (int Old_Asset_Group_ID)
	{
		if (Old_Asset_Group_ID < 1) 
			set_Value (COLUMNNAME_Old_Asset_Group_ID, null);
		else 
			set_Value (COLUMNNAME_Old_Asset_Group_ID, Integer.valueOf(Old_Asset_Group_ID));
	}

	/** Get Old Asset Group.
		@return Old Asset Group	  */
	public int getOld_Asset_Group_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Old_Asset_Group_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Opening Balance.
		@param OpeningBalance Opening Balance	  */
	public void setOpeningBalance (BigDecimal OpeningBalance)
	{
		throw new IllegalArgumentException ("OpeningBalance is virtual column");	}

	/** Get Opening Balance.
		@return Opening Balance	  */
	public BigDecimal getOpeningBalance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OpeningBalance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_A_Asset getParentAsset() throws RuntimeException
    {
		return (I_A_Asset)MTable.get(getCtx(), I_A_Asset.Table_Name)
			.getPO(getParentAsset_ID(), get_TrxName());	}

	/** Set Parent Asset.
		@param ParentAsset_ID Parent Asset	  */
	public void setParentAsset_ID (int ParentAsset_ID)
	{
		if (ParentAsset_ID < 1) 
			set_Value (COLUMNNAME_ParentAsset_ID, null);
		else 
			set_Value (COLUMNNAME_ParentAsset_ID, Integer.valueOf(ParentAsset_ID));
	}

	/** Get Parent Asset.
		@return Parent Asset	  */
	public int getParentAsset_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_ParentAsset_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Period No.
		@param PeriodNo 
		Unique Period Number
	  */
	public void setPeriodNo (int PeriodNo)
	{
		throw new IllegalArgumentException ("PeriodNo is virtual column");	}

	/** Get Period No.
		@return Unique Period Number
	  */
	public int getPeriodNo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_PeriodNo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Posted Amount.
		@param PostedAmt Posted Amount	  */
	public void setPostedAmt (BigDecimal PostedAmt)
	{
		throw new IllegalArgumentException ("PostedAmt is virtual column");	}

	/** Get Posted Amount.
		@return Posted Amount	  */
	public BigDecimal getPostedAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PostedAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Price.
		@param PriceEntered 
		Price Entered - the price based on the selected/base UoM
	  */
	public void setPriceEntered (BigDecimal PriceEntered)
	{
		throw new IllegalArgumentException ("PriceEntered is virtual column");	}

	/** Get Price.
		@return Price Entered - the price based on the selected/base UoM
	  */
	public BigDecimal getPriceEntered () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceEntered);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		throw new IllegalArgumentException ("Qty is virtual column");	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_C_ElementValue getUser1() throws RuntimeException
    {
		return (I_C_ElementValue)MTable.get(getCtx(), I_C_ElementValue.Table_Name)
			.getPO(getUser1_ID(), get_TrxName());	}

	/** Set User List 1.
		@param User1_ID 
		User defined list element #1
	  */
	public void setUser1_ID (int User1_ID)
	{
		if (User1_ID < 1) 
			set_Value (COLUMNNAME_User1_ID, null);
		else 
			set_Value (COLUMNNAME_User1_ID, Integer.valueOf(User1_ID));
	}

	/** Get User List 1.
		@return User defined list element #1
	  */
	public int getUser1_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_User1_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ElementValue getUser2() throws RuntimeException
    {
		return (I_C_ElementValue)MTable.get(getCtx(), I_C_ElementValue.Table_Name)
			.getPO(getUser2_ID(), get_TrxName());	}

	/** Set User List 2.
		@param User2_ID 
		User defined list element #2
	  */
	public void setUser2_ID (int User2_ID)
	{
		if (User2_ID < 1) 
			set_Value (COLUMNNAME_User2_ID, null);
		else 
			set_Value (COLUMNNAME_User2_ID, Integer.valueOf(User2_ID));
	}

	/** Get User List 2.
		@return User defined list element #2
	  */
	public int getUser2_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_User2_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}