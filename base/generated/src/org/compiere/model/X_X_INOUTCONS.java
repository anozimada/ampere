/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;

/** Generated Model for X_INOUTCONS
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_X_INOUTCONS extends PO implements I_X_INOUTCONS, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20220307L;

    /** Standard Constructor */
    public X_X_INOUTCONS (Properties ctx, int X_INOUTCONS_ID, String trxName)
    {
      super (ctx, X_INOUTCONS_ID, trxName);
      /** if (X_INOUTCONS_ID == 0)
        {
			setC_BPartner_ID (0);
			setC_BPartner_Location_ID (0);
			setDocumentNo (null);
			setMovementDate (new Timestamp( System.currentTimeMillis() ));
// @#Date@
			setPOReference (null);
			setX_INOUTCONS_ID (0);
        } */
    }

    /** Load Constructor */
    public X_X_INOUTCONS (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_X_INOUTCONS[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_BPartner_Location getC_BPartner_Location() throws RuntimeException
    {
		return (I_C_BPartner_Location)MTable.get(getCtx(), I_C_BPartner_Location.Table_Name)
			.getPO(getC_BPartner_Location_ID(), get_TrxName());	}

	/** Set Partner Location.
		@param C_BPartner_Location_ID 
		Identifies the (ship to) address for this Business Partner
	  */
	public void setC_BPartner_Location_ID (int C_BPartner_Location_ID)
	{
		if (C_BPartner_Location_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_Location_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_Location_ID, Integer.valueOf(C_BPartner_Location_ID));
	}

	/** Get Partner Location.
		@return Identifies the (ship to) address for this Business Partner
	  */
	public int getC_BPartner_Location_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_Location_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Create lines from.
		@param CreateFrom 
		Process which will generate a new document lines based on an existing document
	  */
	public void setCreateFrom (String CreateFrom)
	{
		set_Value (COLUMNNAME_CreateFrom, CreateFrom);
	}

	/** Get Create lines from.
		@return Process which will generate a new document lines based on an existing document
	  */
	public String getCreateFrom () 
	{
		return (String)get_Value(COLUMNNAME_CreateFrom);
	}

	/** Set CreateUCC128.
		@param CreateUCC128 CreateUCC128	  */
	public void setCreateUCC128 (String CreateUCC128)
	{
		set_Value (COLUMNNAME_CreateUCC128, CreateUCC128);
	}

	/** Get CreateUCC128.
		@return CreateUCC128	  */
	public String getCreateUCC128 () 
	{
		return (String)get_Value(COLUMNNAME_CreateUCC128);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_Value (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set EDI Acknowledged.
		@param EDIAcknowledged EDI Acknowledged	  */
	public void setEDIAcknowledged (boolean EDIAcknowledged)
	{
		set_Value (COLUMNNAME_EDIAcknowledged, Boolean.valueOf(EDIAcknowledged));
	}

	/** Get EDI Acknowledged.
		@return EDI Acknowledged	  */
	public boolean isEDIAcknowledged () 
	{
		Object oo = get_Value(COLUMNNAME_EDIAcknowledged);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Submitted via EDI.
		@param IsEDISubmitted Submitted via EDI	  */
	public void setIsEDISubmitted (boolean IsEDISubmitted)
	{
		set_Value (COLUMNNAME_IsEDISubmitted, Boolean.valueOf(IsEDISubmitted));
	}

	/** Get Submitted via EDI.
		@return Submitted via EDI	  */
	public boolean isEDISubmitted () 
	{
		Object oo = get_Value(COLUMNNAME_IsEDISubmitted);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Movement Date.
		@param MovementDate 
		Date a product was moved in or out of inventory
	  */
	public void setMovementDate (Timestamp MovementDate)
	{
		set_Value (COLUMNNAME_MovementDate, MovementDate);
	}

	/** Get Movement Date.
		@return Date a product was moved in or out of inventory
	  */
	public Timestamp getMovementDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_MovementDate);
	}

	public I_M_Shipper getM_Shipper() throws RuntimeException
    {
		return (I_M_Shipper)MTable.get(getCtx(), I_M_Shipper.Table_Name)
			.getPO(getM_Shipper_ID(), get_TrxName());	}

	/** Set Shipper.
		@param M_Shipper_ID 
		Method or manner of product delivery
	  */
	public void setM_Shipper_ID (int M_Shipper_ID)
	{
		if (M_Shipper_ID < 1) 
			set_Value (COLUMNNAME_M_Shipper_ID, null);
		else 
			set_Value (COLUMNNAME_M_Shipper_ID, Integer.valueOf(M_Shipper_ID));
	}

	/** Get Shipper.
		@return Method or manner of product delivery
	  */
	public int getM_Shipper_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Shipper_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set No Packages.
		@param NoPackages 
		Number of packages shipped
	  */
	public void setNoPackages (int NoPackages)
	{
		set_Value (COLUMNNAME_NoPackages, Integer.valueOf(NoPackages));
	}

	/** Get No Packages.
		@return Number of packages shipped
	  */
	public int getNoPackages () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_NoPackages);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Order Reference.
		@param POReference 
		Transaction Reference Number (Sales Order, Purchase Order) of your Business Partner
	  */
	public void setPOReference (String POReference)
	{
		set_Value (COLUMNNAME_POReference, POReference);
	}

	/** Get Order Reference.
		@return Transaction Reference Number (Sales Order, Purchase Order) of your Business Partner
	  */
	public String getPOReference () 
	{
		return (String)get_Value(COLUMNNAME_POReference);
	}

	/** Set Submit EDI.
		@param SubmitEDI Submit EDI	  */
	public void setSubmitEDI (String SubmitEDI)
	{
		set_Value (COLUMNNAME_SubmitEDI, SubmitEDI);
	}

	/** Get Submit EDI.
		@return Submit EDI	  */
	public String getSubmitEDI () 
	{
		return (String)get_Value(COLUMNNAME_SubmitEDI);
	}

	/** Set Tracking No.
		@param TrackingNo 
		Number to track the shipment
	  */
	public void setTrackingNo (String TrackingNo)
	{
		set_Value (COLUMNNAME_TrackingNo, TrackingNo);
	}

	/** Get Tracking No.
		@return Number to track the shipment
	  */
	public String getTrackingNo () 
	{
		return (String)get_Value(COLUMNNAME_TrackingNo);
	}

	/** Set EDI Shipments.
		@param X_INOUTCONS_ID EDI Shipments	  */
	public void setX_INOUTCONS_ID (int X_INOUTCONS_ID)
	{
		if (X_INOUTCONS_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_X_INOUTCONS_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_X_INOUTCONS_ID, Integer.valueOf(X_INOUTCONS_ID));
	}

	/** Get EDI Shipments.
		@return EDI Shipments	  */
	public int getX_INOUTCONS_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_X_INOUTCONS_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set BOL #.
		@param X_LABELBOL BOL #	  */
	public void setX_LABELBOL (String X_LABELBOL)
	{
		set_Value (COLUMNNAME_X_LABELBOL, X_LABELBOL);
	}

	/** Get BOL #.
		@return BOL #	  */
	public String getX_LABELBOL () 
	{
		return (String)get_Value(COLUMNNAME_X_LABELBOL);
	}
}