/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for M_FreightCharge
 *  @author Adempiere (generated) 
 *  @version $
{
project.version}

 */
public interface I_M_FreightCharge 
{

    /** TableName=M_FreightCharge */
    public static final String Table_Name = "M_FreightCharge";

    /** AD_Table_ID=53373 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AdditionalPackageCharge */
    public static final String COLUMNNAME_AdditionalPackageCharge = "AdditionalPackageCharge";

	/** Set Additional Package Charge.
	  * Charge for each additional package.
	  */
	public void setAdditionalPackageCharge (BigDecimal AdditionalPackageCharge);

	/** Get Additional Package Charge.
	  * Charge for each additional package.
	  */
	public BigDecimal getAdditionalPackageCharge();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name BaseCharge */
    public static final String COLUMNNAME_BaseCharge = "BaseCharge";

	/** Set Base Charge.
	  * Charge for initial package
	  */
	public void setBaseCharge (BigDecimal BaseCharge);

	/** Get Base Charge.
	  * Charge for initial package
	  */
	public BigDecimal getBaseCharge();

    /** Column name C_Currency_ID */
    public static final String COLUMNNAME_C_Currency_ID = "C_Currency_ID";

	/** Set Currency.
	  * The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID);

	/** Get Currency.
	  * The Currency for this record
	  */
	public int getC_Currency_ID();

	public I_C_Currency getC_Currency() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name CubicFactor */
    public static final String COLUMNNAME_CubicFactor = "CubicFactor";

	/** Set Cubic Factor.
	  * Factor to multiply weight by when calculating charges
	  */
	public void setCubicFactor (BigDecimal CubicFactor);

	/** Get Cubic Factor.
	  * Factor to multiply weight by when calculating charges
	  */
	public BigDecimal getCubicFactor();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsTaxIncluded */
    public static final String COLUMNNAME_IsTaxIncluded = "IsTaxIncluded";

	/** Set Price includes Tax.
	  * Tax is included in the price 
	  */
	public void setIsTaxIncluded (boolean IsTaxIncluded);

	/** Get Price includes Tax.
	  * Tax is included in the price 
	  */
	public boolean isTaxIncluded();

    /** Column name M_FreightCategory_ID */
    public static final String COLUMNNAME_M_FreightCategory_ID = "M_FreightCategory_ID";

	/** Set Freight Category.
	  * Category of the Freight
	  */
	public void setM_FreightCategory_ID (int M_FreightCategory_ID);

	/** Get Freight Category.
	  * Category of the Freight
	  */
	public int getM_FreightCategory_ID();

	public I_M_FreightCategory getM_FreightCategory() throws RuntimeException;

    /** Column name M_FreightCharge_ID */
    public static final String COLUMNNAME_M_FreightCharge_ID = "M_FreightCharge_ID";

	/** Set Freight Charge	  */
	public void setM_FreightCharge_ID (int M_FreightCharge_ID);

	/** Get Freight Charge	  */
	public int getM_FreightCharge_ID();

    /** Column name M_FreightRegion_ID */
    public static final String COLUMNNAME_M_FreightRegion_ID = "M_FreightRegion_ID";

	/** Set Freight Region	  */
	public void setM_FreightRegion_ID (int M_FreightRegion_ID);

	/** Get Freight Region	  */
	public int getM_FreightRegion_ID();

	public I_M_FreightRegion getM_FreightRegion() throws RuntimeException;

    /** Column name MinimumCharge */
    public static final String COLUMNNAME_MinimumCharge = "MinimumCharge";

	/** Set Minimum Charge.
	  * The minimum charge amount
	  */
	public void setMinimumCharge (BigDecimal MinimumCharge);

	/** Get Minimum Charge.
	  * The minimum charge amount
	  */
	public BigDecimal getMinimumCharge();

    /** Column name M_Shipper_ID */
    public static final String COLUMNNAME_M_Shipper_ID = "M_Shipper_ID";

	/** Set Shipper.
	  * Method or manner of product delivery
	  */
	public void setM_Shipper_ID (int M_Shipper_ID);

	/** Get Shipper.
	  * Method or manner of product delivery
	  */
	public int getM_Shipper_ID();

	public I_M_Shipper getM_Shipper() throws RuntimeException;

    /** Column name Surcharge */
    public static final String COLUMNNAME_Surcharge = "Surcharge";

	/** Set Surcharge %.
	  * Percentage charge on total
	  */
	public void setSurcharge (BigDecimal Surcharge);

	/** Get Surcharge %.
	  * Percentage charge on total
	  */
	public BigDecimal getSurcharge();

    /** Column name To_Region_ID */
    public static final String COLUMNNAME_To_Region_ID = "To_Region_ID";

	/** Set To.
	  * Receiving Region
	  */
	public void setTo_Region_ID (int To_Region_ID);

	/** Get To.
	  * Receiving Region
	  */
	public int getTo_Region_ID();

	public I_M_FreightRegion getTo_Region() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name ValidFrom */
    public static final String COLUMNNAME_ValidFrom = "ValidFrom";

	/** Set Valid from.
	  * Valid from including this date (first day)
	  */
	public void setValidFrom (Timestamp ValidFrom);

	/** Get Valid from.
	  * Valid from including this date (first day)
	  */
	public Timestamp getValidFrom();

    /** Column name WeightMultiple */
    public static final String COLUMNNAME_WeightMultiple = "WeightMultiple";

	/** Set Weight Multiple.
	  * Weight charged in multiples of this number of kilograms
	  */
	public void setWeightMultiple (BigDecimal WeightMultiple);

	/** Get Weight Multiple.
	  * Weight charged in multiples of this number of kilograms
	  */
	public BigDecimal getWeightMultiple();

    /** Column name WeightMultipleRate */
    public static final String COLUMNNAME_WeightMultipleRate = "WeightMultipleRate";

	/** Set Weight Multiple Rate.
	  * Additional amount charged per weight multiple
	  */
	public void setWeightMultipleRate (BigDecimal WeightMultipleRate);

	/** Get Weight Multiple Rate.
	  * Additional amount charged per weight multiple
	  */
	public BigDecimal getWeightMultipleRate();

    /** Column name WeightUnitRate */
    public static final String COLUMNNAME_WeightUnitRate = "WeightUnitRate";

	/** Set Weight Unit Rate.
	  * Additional amount charged per weight unit
	  */
	public void setWeightUnitRate (BigDecimal WeightUnitRate);

	/** Get Weight Unit Rate.
	  * Additional amount charged per weight unit
	  */
	public BigDecimal getWeightUnitRate();
}
