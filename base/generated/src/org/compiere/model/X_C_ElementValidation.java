/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for C_ElementValidation
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_C_ElementValidation extends PO implements I_C_ElementValidation, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20220307L;

    /** Standard Constructor */
    public X_C_ElementValidation (Properties ctx, int C_ElementValidation_ID, String trxName)
    {
      super (ctx, C_ElementValidation_ID, trxName);
      /** if (C_ElementValidation_ID == 0)
        {
			setC_ElementValidation_ID (0);
			setC_ElementValue_ID (0);
			setName (null);
        } */
    }

    /** Load Constructor */
    public X_C_ElementValidation (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 2 - Client 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_C_ElementValidation[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_AcctSchema_Element getC_AcctSchema_Element() throws RuntimeException
    {
		return (I_C_AcctSchema_Element)MTable.get(getCtx(), I_C_AcctSchema_Element.Table_Name)
			.getPO(getC_AcctSchema_Element_ID(), get_TrxName());	}

	/** Set Acct.Schema Element.
		@param C_AcctSchema_Element_ID Acct.Schema Element	  */
	public void setC_AcctSchema_Element_ID (int C_AcctSchema_Element_ID)
	{
		if (C_AcctSchema_Element_ID < 1) 
			set_Value (COLUMNNAME_C_AcctSchema_Element_ID, null);
		else 
			set_Value (COLUMNNAME_C_AcctSchema_Element_ID, Integer.valueOf(C_AcctSchema_Element_ID));
	}

	/** Get Acct.Schema Element.
		@return Acct.Schema Element	  */
	public int getC_AcctSchema_Element_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_AcctSchema_Element_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Account Element Validation ID.
		@param C_ElementValidation_ID Account Element Validation ID	  */
	public void setC_ElementValidation_ID (int C_ElementValidation_ID)
	{
		if (C_ElementValidation_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_ElementValidation_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_ElementValidation_ID, Integer.valueOf(C_ElementValidation_ID));
	}

	/** Get Account Element Validation ID.
		@return Account Element Validation ID	  */
	public int getC_ElementValidation_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_ElementValidation_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ElementValue getC_ElementValue_From() throws RuntimeException
    {
		return (I_C_ElementValue)MTable.get(getCtx(), I_C_ElementValue.Table_Name)
			.getPO(getC_ElementValue_From_ID(), get_TrxName());	}

	/** Set Account Element.
		@param C_ElementValue_From_ID 
		Account Element
	  */
	public void setC_ElementValue_From_ID (int C_ElementValue_From_ID)
	{
		if (C_ElementValue_From_ID < 1) 
			set_Value (COLUMNNAME_C_ElementValue_From_ID, null);
		else 
			set_Value (COLUMNNAME_C_ElementValue_From_ID, Integer.valueOf(C_ElementValue_From_ID));
	}

	/** Get Account Element.
		@return Account Element
	  */
	public int getC_ElementValue_From_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_ElementValue_From_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ElementValue getC_ElementValue() throws RuntimeException
    {
		return (I_C_ElementValue)MTable.get(getCtx(), I_C_ElementValue.Table_Name)
			.getPO(getC_ElementValue_ID(), get_TrxName());	}

	/** Set Account Element.
		@param C_ElementValue_ID 
		Account Element
	  */
	public void setC_ElementValue_ID (int C_ElementValue_ID)
	{
		if (C_ElementValue_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_ElementValue_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_ElementValue_ID, Integer.valueOf(C_ElementValue_ID));
	}

	/** Get Account Element.
		@return Account Element
	  */
	public int getC_ElementValue_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_ElementValue_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ElementValue getC_ElementValue_To() throws RuntimeException
    {
		return (I_C_ElementValue)MTable.get(getCtx(), I_C_ElementValue.Table_Name)
			.getPO(getC_ElementValue_To_ID(), get_TrxName());	}

	/** Set Account Element.
		@param C_ElementValue_To_ID 
		Account Element
	  */
	public void setC_ElementValue_To_ID (int C_ElementValue_To_ID)
	{
		if (C_ElementValue_To_ID < 1) 
			set_Value (COLUMNNAME_C_ElementValue_To_ID, null);
		else 
			set_Value (COLUMNNAME_C_ElementValue_To_ID, Integer.valueOf(C_ElementValue_To_ID));
	}

	/** Get Account Element.
		@return Account Element
	  */
	public int getC_ElementValue_To_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_ElementValue_To_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** IncludeExclude AD_Reference_ID=53773 */
	public static final int INCLUDEEXCLUDE_AD_Reference_ID=53773;
	/** Include = Include */
	public static final String INCLUDEEXCLUDE_Include = "Include";
	/** Exclude = Exclude */
	public static final String INCLUDEEXCLUDE_Exclude = "Exclude";
	/** Set Include/Exclude.
		@param IncludeExclude Include/Exclude	  */
	public void setIncludeExclude (String IncludeExclude)
	{

		set_Value (COLUMNNAME_IncludeExclude, IncludeExclude);
	}

	/** Get Include/Exclude.
		@return Include/Exclude	  */
	public String getIncludeExclude () 
	{
		return (String)get_Value(COLUMNNAME_IncludeExclude);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }
}