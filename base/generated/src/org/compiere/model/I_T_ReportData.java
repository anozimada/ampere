/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for T_ReportData
 *  @author Adempiere (generated) 
 *  @version $
{
project.version}

 */
public interface I_T_ReportData 
{

    /** TableName=T_ReportData */
    public static final String Table_Name = "T_ReportData";

    /** AD_Table_ID=53357 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 4 - System 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(4);

    /** Load Meta Data */

    /** Column name AD_PInstance_ID */
    public static final String COLUMNNAME_AD_PInstance_ID = "AD_PInstance_ID";

	/** Set Process Instance.
	  * Instance of the process
	  */
	public void setAD_PInstance_ID (int AD_PInstance_ID);

	/** Get Process Instance.
	  * Instance of the process
	  */
	public int getAD_PInstance_ID();

	public I_AD_PInstance getAD_PInstance() throws RuntimeException;

    /** Column name Amt_0 */
    public static final String COLUMNNAME_Amt_0 = "Amt_0";

	/** Set Amount 0	  */
	public void setAmt_0 (BigDecimal Amt_0);

	/** Get Amount 0	  */
	public BigDecimal getAmt_0();

    /** Column name Amt_1 */
    public static final String COLUMNNAME_Amt_1 = "Amt_1";

	/** Set Amount 1	  */
	public void setAmt_1 (BigDecimal Amt_1);

	/** Get Amount 1	  */
	public BigDecimal getAmt_1();

    /** Column name Amt_2 */
    public static final String COLUMNNAME_Amt_2 = "Amt_2";

	/** Set Amount 2	  */
	public void setAmt_2 (BigDecimal Amt_2);

	/** Get Amount 2	  */
	public BigDecimal getAmt_2();

    /** Column name Amt_3 */
    public static final String COLUMNNAME_Amt_3 = "Amt_3";

	/** Set Amount 3	  */
	public void setAmt_3 (BigDecimal Amt_3);

	/** Get Amount 3	  */
	public BigDecimal getAmt_3();

    /** Column name Amt_4 */
    public static final String COLUMNNAME_Amt_4 = "Amt_4";

	/** Set Amount 4	  */
	public void setAmt_4 (BigDecimal Amt_4);

	/** Get Amount 4	  */
	public BigDecimal getAmt_4();

    /** Column name Date_0 */
    public static final String COLUMNNAME_Date_0 = "Date_0";

	/** Set Date 0	  */
	public void setDate_0 (Timestamp Date_0);

	/** Get Date 0	  */
	public Timestamp getDate_0();

    /** Column name Date_1 */
    public static final String COLUMNNAME_Date_1 = "Date_1";

	/** Set Date 1	  */
	public void setDate_1 (Timestamp Date_1);

	/** Get Date 1	  */
	public Timestamp getDate_1();

    /** Column name LevelNo */
    public static final String COLUMNNAME_LevelNo = "LevelNo";

	/** Set Level no	  */
	public void setLevelNo (int LevelNo);

	/** Get Level no	  */
	public int getLevelNo();

    /** Column name Qty_0 */
    public static final String COLUMNNAME_Qty_0 = "Qty_0";

	/** Set Quantity 0	  */
	public void setQty_0 (BigDecimal Qty_0);

	/** Get Quantity 0	  */
	public BigDecimal getQty_0();

    /** Column name Qty_1 */
    public static final String COLUMNNAME_Qty_1 = "Qty_1";

	/** Set Quantity 1	  */
	public void setQty_1 (BigDecimal Qty_1);

	/** Get Quantity 1	  */
	public BigDecimal getQty_1();

    /** Column name SeqNo */
    public static final String COLUMNNAME_SeqNo = "SeqNo";

	/** Set Sequence.
	  * Method of ordering records;
 lowest number comes first
	  */
	public void setSeqNo (int SeqNo);

	/** Get Sequence.
	  * Method of ordering records;
 lowest number comes first
	  */
	public int getSeqNo();

    /** Column name Text_0 */
    public static final String COLUMNNAME_Text_0 = "Text_0";

	/** Set Text 0	  */
	public void setText_0 (String Text_0);

	/** Get Text 0	  */
	public String getText_0();

    /** Column name Text_1 */
    public static final String COLUMNNAME_Text_1 = "Text_1";

	/** Set Text 1	  */
	public void setText_1 (String Text_1);

	/** Get Text 1	  */
	public String getText_1();

    /** Column name Text_2 */
    public static final String COLUMNNAME_Text_2 = "Text_2";

	/** Set Text 2	  */
	public void setText_2 (String Text_2);

	/** Get Text 2	  */
	public String getText_2();

    /** Column name Text_3 */
    public static final String COLUMNNAME_Text_3 = "Text_3";

	/** Set Text 3	  */
	public void setText_3 (String Text_3);

	/** Get Text 3	  */
	public String getText_3();

    /** Column name Text_4 */
    public static final String COLUMNNAME_Text_4 = "Text_4";

	/** Set Text 4	  */
	public void setText_4 (String Text_4);

	/** Get Text 4	  */
	public String getText_4();
}
