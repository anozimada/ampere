/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for AD_Tab_Customization
 *  @author Adempiere (generated) 
 *  @version $
{
project.version}

 */
public interface I_AD_Tab_Customization 
{

    /** TableName=AD_Tab_Customization */
    public static final String Table_Name = "AD_Tab_Customization";

    /** AD_Table_ID=53894 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_InfoWindow_ID */
    public static final String COLUMNNAME_AD_InfoWindow_ID = "AD_InfoWindow_ID";

	/** Set Info Window.
	  * Info and search/select Window
	  */
	public void setAD_InfoWindow_ID (int AD_InfoWindow_ID);

	/** Get Info Window.
	  * Info and search/select Window
	  */
	public int getAD_InfoWindow_ID();

	public I_AD_InfoWindow getAD_InfoWindow() throws RuntimeException;

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AD_Tab_Customization_ID */
    public static final String COLUMNNAME_AD_Tab_Customization_ID = "AD_Tab_Customization_ID";

	/** Set AD_Tab_Customization_ID	  */
	public void setAD_Tab_Customization_ID (int AD_Tab_Customization_ID);

	/** Get AD_Tab_Customization_ID	  */
	public int getAD_Tab_Customization_ID();

    /** Column name AD_Tab_ID */
    public static final String COLUMNNAME_AD_Tab_ID = "AD_Tab_ID";

	/** Set Tab.
	  * Tab within a Window
	  */
	public void setAD_Tab_ID (int AD_Tab_ID);

	/** Get Tab.
	  * Tab within a Window
	  */
	public int getAD_Tab_ID();

	public I_AD_Tab getAD_Tab() throws RuntimeException;

    /** Column name AD_User_ID */
    public static final String COLUMNNAME_AD_User_ID = "AD_User_ID";

	/** Set User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID);

	/** Get User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID();

	public I_AD_User getAD_User() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name custom */
    public static final String COLUMNNAME_custom = "custom";

	/** Set custom	  */
	public void setcustom (String custom);

	/** Get custom	  */
	public String getcustom();

    /** Column name IncludedTabHeight */
    public static final String COLUMNNAME_IncludedTabHeight = "IncludedTabHeight";

	/** Set Included Tab Height	  */
	public void setIncludedTabHeight (BigDecimal IncludedTabHeight);

	/** Get Included Tab Height	  */
	public BigDecimal getIncludedTabHeight();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsDisplayedGrid */
    public static final String COLUMNNAME_IsDisplayedGrid = "IsDisplayedGrid";

	/** Set Displayed in Grid.
	  * Determines, if this field is displayed in grid view
	  */
	public void setIsDisplayedGrid (String IsDisplayedGrid);

	/** Get Displayed in Grid.
	  * Determines, if this field is displayed in grid view
	  */
	public String getIsDisplayedGrid();

    /** Column name isQuickEntry */
    public static final String COLUMNNAME_isQuickEntry = "isQuickEntry";

	/** Set Quick Entry.
	  * Display in Quick Entry Form
	  */
	public void setisQuickEntry (boolean isQuickEntry);

	/** Get Quick Entry.
	  * Display in Quick Entry Form
	  */
	public boolean isQuickEntry();

    /** Column name IsRefreshGridTimer */
    public static final String COLUMNNAME_IsRefreshGridTimer = "IsRefreshGridTimer";

	/** Set Refresh grid data by Timer.
	  * Configure specific timer interval to refresh the Quick form data
	  */
	public void setIsRefreshGridTimer (boolean IsRefreshGridTimer);

	/** Get Refresh grid data by Timer.
	  * Configure specific timer interval to refresh the Quick form data
	  */
	public boolean isRefreshGridTimer();

    /** Column name RefreshInterval */
    public static final String COLUMNNAME_RefreshInterval = "RefreshInterval";

	/** Set Refresh Timer Interval.
	  * Set the interval in second to hit refresh event for the quick grid
	  */
	public void setRefreshInterval (int RefreshInterval);

	/** Get Refresh Timer Interval.
	  * Set the interval in second to hit refresh event for the quick grid
	  */
	public int getRefreshInterval();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
